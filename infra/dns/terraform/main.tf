terraform {
  required_version = "~> 0.13.0"

  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 2.20.0"
    }
  }

  backend "s3" {
    bucket         = "autonomicjump-tfstates"
    key            = "autonomicjump-dns.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraform_state_lock"
  }
}

provider "google" {
  credentials = file("account.json")
  project     = "autonomic-web"
  region      = "us-east1"
}

provider "cloudflare" {
  account_id = var.cloudflare_account_id
  api_key = var.cloudflare_api_key
}
