variable "cloudflare_account_id" {}
variable "cloudflare_api_key" {}
data "cloudflare_zones" "autonomicjump_com" {
  filter {
    name = "autonomicjump.com"
  }
}
