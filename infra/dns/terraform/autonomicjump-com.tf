resource "cloudflare_zone" "autonomicjump_com" {
  zone = "autonomicjump.com"
}

resource "cloudflare_zone_settings_override" "autonomicjump_com" {
  zone_id = cloudflare_zone.autonomicjump_com.id

  settings {
    always_online               = "on"
    always_use_https            = "on"
    automatic_https_rewrites    = "on"
    brotli                      = "on"
    browser_cache_ttl           = 1800
    browser_check               = "on"
    cache_level                 = "aggressive"
    challenge_ttl               = 1800
    development_mode            = "off"
    email_obfuscation           = "on"
    h2_prioritization           = "on"
    hotlink_protection          = "on"
    http3                       = "on"
    ip_geolocation              = "on"
    ipv6                        = "on"
    max_upload                  = 100
    min_tls_version             = "1.2"
    opportunistic_encryption    = "on"
    opportunistic_onion         = "on"
    pseudo_ipv4                 = "off"
    privacy_pass                = "on"
    rocket_loader               = "off"
    security_level              = "medium"
    server_side_exclude         = "on"
    ssl                         = "flexible"
    tls_1_3                     = "zrt"
    tls_client_auth             = "off"
    universal_ssl               = "on"
    websockets                  = "on"
    zero_rtt                    = "on"

    minify {
      css  = "on"
      html = "on"
      js   = "on"
    }

    security_header {
      enabled            = true
      preload            = false
      include_subdomains = false
      nosniff            = false
      max_age            = 31536000
    }
  }
}

resource "cloudflare_record" "help" {
  zone_id = cloudflare_zone.autonomicjump_com.id
  name    = "help.${cloudflare_zone.autonomicjump_com.zone}"
  type    = "CNAME"
  value   = "fluidattacks.hosted-by-discourse.com"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "mailchimp_domainkey_2" {
  zone_id = cloudflare_zone.autonomicjump_com.id
  name    = "k2._domainkey.${cloudflare_zone.autonomicjump_com.zone}"
  type    = "CNAME"
  value   = "dkim2.mcsv.net"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "mailchimp_domainkey_3" {
  zone_id = cloudflare_zone.autonomicjump_com.id
  name    = "k3._domainkey.${cloudflare_zone.autonomicjump_com.zone}"
  type    = "CNAME"
  value   = "dkim3.mcsv.net"
  proxied = false
  ttl     = 1
}

resource "cloudflare_record" "autonomicjump_com_email_1" {
  zone_id = cloudflare_zone.autonomicjump_com.id
  name     = cloudflare_zone.autonomicjump_com.zone
  type     = "MX"
  priority = 1
  value    = "aspmx.l.google.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicjump_com_email_2" {
  zone_id = cloudflare_zone.autonomicjump_com.id
  name     = cloudflare_zone.autonomicjump_com.zone
  type     = "MX"
  priority = 5
  value    = "alt1.aspmx.l.google.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicjump_com_email_3" {
  zone_id = cloudflare_zone.autonomicjump_com.id
  name     = cloudflare_zone.autonomicjump_com.zone
  type     = "MX"
  priority = 5
  value    = "alt2.aspmx.l.google.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicjump_com_email_4" {
  zone_id = cloudflare_zone.autonomicjump_com.id
  name     = cloudflare_zone.autonomicjump_com.zone
  type     = "MX"
  priority = 10
  value    = "aspmx2.googlemail.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicjump_com_email_5" {
  zone_id = cloudflare_zone.autonomicjump_com.id
  name     = cloudflare_zone.autonomicjump_com.zone
  type     = "MX"
  priority = 10
  value    = "aspmx3.googlemail.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicjump_com_email_authentication" {
  zone_id = cloudflare_zone.autonomicjump_com.id
  name   = "google._domainkey.${cloudflare_zone.autonomicjump_com.zone}"
  type   = "TXT"
  value  = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhfFuh/eyFUJjqrOAlEEKQn8i+v8Cdu4zYcOROxbyFcr7gPBidkoh+MQPyrrCSJWVBISwxYXTqqs/+Uag+W/vBhTQWrioao7DDUPBGaxeiW1xehLbYc0LkkMeRp3dNy78CqWYkjn16It6gGLLNQzEkZTwSHkBBHVszY5dWyuhjWPU8Uv1itfkVIllTf7uNzrtogTXToinPj9icskTX+srRHvYuiu9dY8EGzI5ttxNytgvmvqNlsdBOD+3RCmkaA2Peph2ovRmG6XjopfvJWOKeCTGK+IJKAAVA+G9i8KaY321HqixVx6eo7jgLafvbr+hpGbSbaoqqagquRGTjs59eQIDAQAB"
  ttl    = 300
}

resource "cloudflare_record" "mandrill_dkim" {
  zone_id = cloudflare_zone.autonomicjump_com.id
  name    = "mandrill._domainkey.${cloudflare_zone.autonomicjump_com.zone}"
  type    = "TXT"
  value   = "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCrLHiExVd55zd/IQ/J/mRwSRMAocV/hMB3jXwaHH36d9NaVynQFYV8NaWi69c1veUtRzGt7yAioXqLj7Z4TeEUoOLgrKsn8YnckGs9i3B3tVFB+Ch/4mPhXWiNfNdynHWBcPcbJ8kjEQ2U8y78dHZj1YeRXXVvWob2OaKynO8/lQIDAQAB;"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "spf_allowed" {
  zone_id = cloudflare_zone.autonomicjump_com.id
  name    = cloudflare_zone.autonomicjump_com.zone
  type    = "TXT"
  value   = "v=spf1 include:_spf.google.com include:mail.zendesk.com include:spf.mandrillapp.com include:servers.mcsv.net include:transmail.net -all"
  ttl     = 1
  proxied = false
}
