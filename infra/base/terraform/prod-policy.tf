data "aws_iam_policy_document" "prod" {

  statement {
    effect = "Allow"
    actions = [
      "access-analyzer:*",
      "batch:*",
      "sns:*",
      "rds:*",
      "s3:*",
      "aws-portal:*",
      "ce:*",
      "cur:*",
      "savingsplans:*",
      "dynamodb:*",
      "elasticloadbalancing:*",
      "autoscaling:*",
      "sqs:*",
      "sts:*",
      "iam:*",
      "secretsmanager:*",
      "cloudwatch:*",
      "kms:*",
      "lambda:*",
      "route53:*",
      "ec2:*",
      "ecr:*",
      "ecs:*",
      "eks:*",
      "elasticache:*",
      "acm:*",
      "events:*",
      "logs:*",
    ]
    resources = ["*"]
  }

}

resource "aws_iam_policy" "prod" {
  description = "Autonomicjump production policy"
  name        = "autonomicjump-prod"
  policy      = data.aws_iam_policy_document.prod.json
}

resource "aws_iam_user_policy_attachment" "prod" {
  user       = "production"
  policy_arn = aws_iam_policy.prod.arn
}
