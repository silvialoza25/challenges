resource "aws_s3_bucket" "states" {
  bucket        = "autonomicjump-tfstates"
  acl           = "private"
  force_destroy = true

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    "Name"               = "autonomicjump-tfstates"
    "management:type"    = "production"
    "management:product" = "autonomicjump"
  }
}
