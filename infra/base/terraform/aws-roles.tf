resource "aws_iam_role" "dev" {
  name                 = "dev"
  assume_role_policy   = data.aws_iam_policy_document.okta_assume_role.json
  max_session_duration = "32400"

  tags = {
    "Name"               = "dev"
    "management:type"    = "production"
    "management:product" = "autonomicjump"
  }
}

resource "aws_iam_role_policy_attachment" "dev" {
  role       = aws_iam_role.dev.name
  policy_arn = aws_iam_policy.dev.arn
}

resource "aws_iam_role" "prod" {
  name                 = "prod"
  assume_role_policy   = data.aws_iam_policy_document.okta_assume_role.json
  max_session_duration = "32400"

  tags = {
    "Name"               = "prod"
    "management:type"    = "production"
    "management:product" = "autonomicjump"
  }
}

resource "aws_iam_role_policy_attachment" "prod" {
  role       = aws_iam_role.prod.name
  policy_arn = aws_iam_policy.prod.arn
}
