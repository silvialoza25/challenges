resource "aws_iam_user" "dev" {
  name = "development"
  force_destroy = true

  tags = {
    "Name"               = "development"
    "management:type"    = "production"
    "management:product" = "autonomicjump"
  }
}

resource "aws_iam_access_key" "dev_key_1" {
  user = "development"
}
