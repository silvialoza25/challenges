resource "aws_iam_user" "prod" {
  name = "production"
  force_destroy = true

  tags = {
    "Name"               = "production"
    "management:type"    = "production"
    "management:product" = "autonomicjump"
  }
}

resource "aws_iam_access_key" "prod_key_1" {
  user = "production"
}
