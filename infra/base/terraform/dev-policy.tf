data "aws_iam_policy_document" "dev" {

  # S3 ephemeral bucket
  statement {
    effect = "Allow"
    actions = [
      "s3:ListBucket",
      "s3:Get*",
      "s3:PutObject",
      "s3:PutObjectAcl",
      "s3:DeleteObject*",
    ]
    resources = [
      "arn:aws:s3:::docs-dev.autonomicjump.com/*",
      "arn:aws:s3:::docs-dev.autonomicjump.com",
      "arn:aws:s3:::forms-dev.autonomicjump.com/*",
      "arn:aws:s3:::forms-dev.autonomicjump.com",
      "arn:aws:s3:::app-dev.autonomicjump.com/*",
      "arn:aws:s3:::app-dev.autonomicjump.com",
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "autoscaling:Describe*",
      "autoscaling:Get*",
      "access-analyzer:List*",
      "access-analyzer:Get*",
      "access-analyzer:Validate*",
      "batch:Describe*",
      "batch:Get*",
      "s3:List*",
      "s3:Get*",
      "iam:List*",
      "iam:Get*",
      "route53:List*",
      "route53:Get*",
      "acm:Describe*",
      "acm:List*",
      "lambda:Get*",
      "lambda:List*",
      "kms:List*",
      "kms:Get*",
      "kms:Describe*",
      "dynamodb:Describe*",
      "dynamodb:List*",
      "events:Describe*",
      "events:List*",
      "elasticloadbalancing:Describe*",
      "cloudwatch:List*",
      "cloudwatch:Describe*",
      "ec2:Describe*",
      "ec2:Get*",
      "eks:Describe*",
      "eks:List*",
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "dev" {
  description = "Autonomicjump development policy"
  name        = "autonomicjump-dev"
  policy      = data.aws_iam_policy_document.dev.json
}

resource "aws_iam_user_policy_attachment" "dev" {
  user       = "development"
  policy_arn = aws_iam_policy.dev.arn
}
