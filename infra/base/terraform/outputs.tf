output "dev_secret_key_id_1" {
  sensitive = true
  value     = aws_iam_access_key.dev_key_1.id
}

output "dev_secret_key_1" {
  sensitive = true
  value     = aws_iam_access_key.dev_key_1.secret
}

output "prod_secret_key_id_1" {
  sensitive = true
  value     = aws_iam_access_key.prod_key_1.id
}

output "prod_secret_key_1" {
  sensitive = true
  value     = aws_iam_access_key.prod_key_1.secret
}
