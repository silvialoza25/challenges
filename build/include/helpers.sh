# shellcheck shell=bash

function helper_use_pristine_workdir {
  export WORKDIR
  export STARTDIR

  function helper_teardown_workdir {
    echo "[INFO] Deleting: ${WORKDIR}" \
      && rm -rf "${WORKDIR}"
  }
  trap 'helper_teardown_workdir' 'EXIT'

  echo '[INFO] Creating a pristine workdir' \
    && rm -rf "${WORKDIR}" \
    && mkdir -p "${WORKDIR}" \
    && echo '[INFO] Copying files to workdir' \
    && cp -r "${STARTDIR}/." "${WORKDIR}" \
    && echo '[INFO] Entering the workdir' \
    && pushd "${WORKDIR}" \
    && echo '[INFO] Running: git clean -xdf' \
    && git clean -xdf \
    || return 1
}

function helper_docker_build_and_push {
  local tag="${1}"
  local context="${2}"
  local dockerfile="${3}"
  local build_arg_1_key="${4:-build_arg_1_key}"
  local build_arg_1_val="${5:-build_arg_1_val}"
  local build_arg_2_key="${6:-build_arg_2_key}"
  local build_arg_2_val="${7:-build_arg_2_val}"
  local build_arg_3_key="${8:-build_arg_3_key}"
  local build_arg_3_val="${9:-build_arg_3_val}"
  local build_arg_4_key="${10:-build_arg_4_key}"
  local build_arg_4_val="${11:-build_arg_4_val}"
  local build_arg_5_key="${12:-build_arg_5_key}"
  local build_arg_5_val="${13:-build_arg_5_val}"
  local build_arg_6_key="${14:-build_arg_6_key}"
  local build_arg_6_val="${15:-build_arg_6_val}"
  local build_arg_7_key="${16:-build_arg_7_key}"
  local build_arg_7_val="${17:-build_arg_7_val}"
  local build_arg_8_key="${18:-build_arg_8_key}"
  local build_arg_8_val="${19:-build_arg_8_val}"
  local build_arg_9_key="${20:-build_arg_9_key}"
  local build_arg_9_val="${21:-build_arg_9_val}"
  local build_args=(
    --tag "${tag}"
    --file "${dockerfile}"
    --build-arg "${build_arg_1_key}=${build_arg_1_val}"
    --build-arg "${build_arg_2_key}=${build_arg_2_val}"
    --build-arg "${build_arg_3_key}=${build_arg_3_val}"
    --build-arg "${build_arg_4_key}=${build_arg_4_val}"
    --build-arg "${build_arg_5_key}=${build_arg_5_val}"
    --build-arg "${build_arg_6_key}=${build_arg_6_val}"
    --build-arg "${build_arg_7_key}=${build_arg_7_val}"
    --build-arg "${build_arg_8_key}=${build_arg_8_val}"
    --build-arg "${build_arg_9_key}=${build_arg_9_val}"
  )

  echo "[INFO] Logging into: ${CI_REGISTRY}" \
    && docker login \
      --username "${CI_REGISTRY_USER}" \
      --password "${CI_REGISTRY_PASSWORD}" \
      "${CI_REGISTRY}" \
    && echo "[INFO] Pulling: ${tag}" \
    && if docker pull "${tag}"; then
      build_args+=(--cache-from "${tag}")
    fi \
    && echo "[INFO] Building: ${tag}" \
    && docker build "${build_args[@]}" "${context}" \
    && echo "[INFO] Pushing: ${tag}" \
    && docker push "${tag}" \
    && echo "[INFO] Deleting local copy of: ${tag}" \
    && docker image remove "${tag}"
}

function helper_list_declared_jobs {
  declare -F \
    | sed 's/declare -f //' \
    | grep -P '^job_[a-z_]+' \
    | sed 's/job_//' \
    | sort
}

function helper_commit_summary {
  git log --max-count=1 --format=%s
}

function helper_is_solution_commit {
  if helper_commit_summary | grep -Pq '^sol*'; then
    return 0
  else
    return 1
  fi
}

function helper_display_array {
  local elem
  local array

  for elem in "$@"; do
    echo "${elem}"
  done
}

function helper_is_elem_in_array {
  local elem
  local array
  local string

  elem="${1}"
  shift 1
  array=("$@")

  for string in "${array[@]}"; do
    if test "${string}" == "${elem}"; then
      return 0
    fi
  done

  return 1
}

function helper_list_touched_files {
  local path

  git show --format= --name-only HEAD | while read -r path; do
    if test -e "${path}"; then
      echo "${path}"
    fi
  done
}

function helper_file_exists {
  local path="${1}"

  if [ -f "${path}" ]; then
    return 0
  else
    echo "[ERROR] ${path} does not exist" \
      && return 1
  fi
}

function helper_build_nix_caches_parallel {
  local n_provisioners
  local n_provisioners_per_group
  local n_provisioners_remaining
  export lower_limit
  export upper_limit

  n_provisioners=$(find build/provisioners/ -type f | wc -l) \
    && n_provisioners_per_group=$((n_provisioners / CI_NODE_TOTAL)) \
    && n_provisioners_remaining=$((n_provisioners % CI_NODE_TOTAL)) \
    && if [ "${n_provisioners_remaining}" -gt '0' ]; then
      n_provisioners_per_group=$((n_provisioners_per_group += 1))
    fi \
    && lower_limit=$(((CI_NODE_INDEX - 1) * n_provisioners_per_group)) \
    && upper_limit=$((CI_NODE_INDEX * n_provisioners_per_group - 1)) \
    && upper_limit=$((\
      upper_limit > n_provisioners - 1 ? n_provisioners - 1 : upper_limit))
}

function helper_test_pykwalify {
  local data="${1}"
  local schema="${2}"
  local tmp_file

  tmp_file="$(mktemp)" \
    && if pykwalify \
      --data-file "${data}" \
      --schema-file "${schema}" \
      &> "${tmp_file}"; then
      echo "[INFO] ${data} complies with the schema ${schema}"
      return 0
    else
      cat "${tmp_file}"
      echo "[ERROR] ${data} does not comply with the schema ${schema}"
      return 1
    fi
}

function helper_test_schemas_site_data {
  local base
  local target
  local schema
  local tmp_file

  tmp_file="$(mktemp)" \
    && find ./code ./hack ./vbd -mindepth 1 -maxdepth 1 -type d \
    | sort \
      | while read -r base; do
        target="${base}/site-data.yml" \
          && schema="${base}/../site-schema.yml" \
          && if pykwalify \
            --data-file "${target}" \
            --schema-file "${schema}" \
            &> "${tmp_file}"; then
            echo "[INFO] ${target} complies with ${schema}"
          else
            cat "${tmp_file}" \
              && echo "[ERROR] ${target} does not comply with ${schema}" \
              && return 1
          fi
      done
}

function helper_test_schemas_user_yaml {
  local base
  local target
  local schema
  local tmp_file
  local IFS
  local split_path

  tmp_file="$(mktemp)" \
    && find ./code ./hack ./vbd -mindepth 3 -maxdepth 3 -type f -name "*.yml" \
    | sort \
      | while read -r target; do
        IFS='/' read -a split_path <<< "$target" \
          && schema="./${split_path[1]}/${split_path[2]}/user-schema.yml" \
          && if pykwalify \
            --data-file "${target}" \
            --schema-file "${schema}" \
            &> "${tmp_file}"; then
            echo "[INFO] ${target} complies with ${schema}"
          else
            cat "${tmp_file}" \
              && echo "[ERROR] ${target} does not comply with ${schema}" \
              && return 1
          fi
      done
}

function helper_build_solution {
  local solution="${1}"
  local builder="${2}"
  local solution_path_var_name='solutionPath'

  nix-build \
    --cores 1 \
    --max-jobs auto \
    --no-out-link \
    --show-trace \
    --option sandbox false \
    "${builder}" \
    --argstr "${solution_path_var_name}" "/${solution}"
}

function helper_build_lang_solutions {
  local builder="${1}"
  local extensions="${2}"
  shift 2
  local folders=("${@}")
  local files
  export -f helper_build_solution

  files="$(find "${folders[@]}" -type f -name "${extensions}")" \
    && if test -n "${files}"; then
      echo "${files}" | parallel \
        --halt-on-error now,fail=1 \
        "helper_build_solution {} ${builder}"
    else
      return 0
    fi
}

function helper_test_generic_dir_depth {
  local bad_folders

  find code -mindepth 3 -type d >&"${TEMP_FD}"
  mapfile -t bad_folders <&"${TEMP_FD}"

  if test "${#bad_folders[@]}" -eq 0; then
    echo '[INFO] Directory depth is below threshold'
    return 0
  else
    echo "${bad_folders[@]}"
    echo '[ERROR] Directory depth is limited to 3, example: code/a/b'
    return 1
  fi
}

function helper_test_generic_others_duplicates {
  if ./build/modules/others_duplicates/main.py; then
    echo '[INFO] OTHERS.lst seem to contain unique urls'
    return 0
  else
    echo '[ERROR] OTHERS.lst contain duplicated urls'
    return 1
  fi
}

function helper_test_generic_others_code_has_unique_ext {
  chmod +x ./build/modules/others_code_has_unique_ext/main.py
  if ./build/modules/others_code_has_unique_ext/main.py; then
    echo '[INFO] OTHERS.lst seem to contain unique extensions in urls'
    return 0
  else
    echo '[ERROR] OTHERS.lst contain duplicated extensions in urls'
    return 1
  fi
}

function helper_test_generic_others_code_raw_only {
  chmod +x ./build/modules/others_code_raw_only/main.py
  if ./build/modules/others_code_raw_only/main.py; then
    echo '[INFO] OTHERS.lst seem to contain raw version urls'
    return 0
  else
    echo '[ERROR] OTHERS.lst contain urls without raw version'
    return 1
  fi
}

function helper_test_generic_others_code_sort_by_ext {
  chmod +x ./build/modules/others_code_sort_by_ext/main.py
  if ./build/modules/others_code_sort_by_ext/main.py; then
    echo '[INFO] OTHERS.lst seem to contain urls sorted by extension and url'
    return 0
  else
    echo '[ERROR] OTHERS.lst contain unsorted urls'
    echo '        Run it locally with:'
    echo '        ./build/modules/others_code_sort_by_ext/main.py'
    return 1
  fi
}

function helper_test_generic_allowed_evidences {
  local mime
  local path
  local success

  find {hack,vbd} -iwholename '*/evidences/*' | while read -r path; do
    is_allowed='False'
    mime=$(file --mime-type --brief "${path}")
    allowed_mimes=(
      'image/png'
      'text/x-python'
      'text/x-script.python'
    )
    for allowed in "${allowed_mimes[@]}"; do
      if test "${mime}" = "$allowed"; then
        is_allowed='True'
      fi
    done
    if test "$is_allowed" != 'True'; then
      echo "(${mime}) ${path}"
      echo "[ERROR] Evidences must be one of the following mime types:"
      helper_display_array "${allowed_mimes[@]}"
      return 1
    fi
  done
}

function helper_test_generic_misplaced_evidences {
  local misplaced_images

  find {hack,vbd}/*/*/*.{bmp,gif,jp*g,png,tiff} 2> /dev/null >&"${TEMP_FD}"
  mapfile -t misplaced_images <&"${TEMP_FD}"

  if test "${#misplaced_images[@]}" -eq 0; then
    echo '[INFO] Evidences are located under evidences/ folder'
    return 0
  else
    for file in "${misplaced_images[@]}"; do
      echo "${file}"
    done
    echo "[ERROR] Evidences must be located under evidences/ folder"
    return 1
  fi
}

function helper_test_generic_allowed_mimes {
  local mime_encoding
  local mime_type
  local path
  local valid_mime_encodings=(
    'binary'
    'us-ascii'
    'utf-8'
  )
  local valid_mime_types=(
    'application/json'
    'application/octet-stream'
    'image/png'
    'inode/x-empty'
    'text/xml'
    'text/html'
    'text/plain'
    'text/troff'
    'text/x-c'
    'text/x-c++'
    'text/x-java'
    'text/x-objective-c'
    'text/x-pascal'
    'text/x-perl'
    'text/x-php'
    'text/x-python'
    'text/x-script.python'
    'text/x-ruby'
    'text/x-shellscript'
    'text/x-clojure'
    'text/x-lisp'
    'text/x-Algol68'
  )

  helper_list_touched_files | while read -r path; do
    mime_encoding=$(file --brief --mime-encoding "${path}")
    mime_type=$(file --brief --mime-type "${path}")

    if helper_is_elem_in_array "${mime_encoding}" "${valid_mime_encodings[@]}"; then
      echo "[INFO] Mime-encoding ${mime_encoding} at: ${path}"
    else
      echo "[ERROR] Mime-encoding ${mime_encoding} at: ${path}"
      echo '[INFO] Valid mime-encodings are:'
      helper_display_array "${valid_mime_encodings[@]}"
      return 1
    fi

    if helper_is_elem_in_array "${mime_type}" "${valid_mime_types[@]}"; then
      echo "[INFO] Mime-type ${mime_type} at: ${path}"
    else
      echo "[ERROR] Mime-type ${mime_type} at: ${path}"
      echo '[INFO] Valid mime-types are:'
      helper_display_array "${valid_mime_types[@]}"
      return 1
    fi
  done
}

function helper_test_generic_atfluid_account {
  if helper_is_solution_commit; then
    if helper_list_touched_files | grep 'atfluid'; then
      echo '[ERROR] Do not commit solutions with an *atfluid account'
      echo '        Use your personal GitLab account'
      return 1
    else
      echo '[INFO] Author is using his/her personal GitLab account'
      return 0
    fi
  else
    echo '[SKIPPED] Not a solution commit'
    return 0
  fi
}

function helper_test_generic_short_filenames {
  if find code | sed 's_^.*/__g' | grep -P '.{36,}'; then
    echo '[ERROR] All filenames must be less than or equal to 36 characters'
    return 1
  else
    echo '[INFO] Filenames are less than or equal to 36 characters'
    return 0
  fi
}

function helper_test_generic_raw_github_urls {
  if grep -r --include=OTHERS.lst --exclude-dir=.git github \
    code/4clojure \
    code/cod* \
    code/exercism \
    code/hackerearth \
    code/hackerrank \
    code/projecteuler \
    code/rosecode \
    code/solveet \
    code/spoj \
    | grep -v 'raw'; then
    echo '[ERROR] GitHub URLs in programming challenges must be raw links'
    return 1
  else
    echo '[INFO] GitHub URLs in programming challenges are raw links'
    return 0
  fi
}

function helper_test_generic_no_asc_extension {
  if find code articles | grep '\.asc$'; then
    echo '[ERROR] ASCIIDoc extensions must be .adoc, not .asc'
    return 1
  else
    echo '[INFO] ASCIIDoc extensions are .adoc'
    return 0
  fi
}

function helper_test_generic_no_tabs {
  # Go linter asks for tabs
  if grep -nrIP --exclude-dir=.git* --exclude=*.go '\t'; then
    echo '[ERROR] Indentation with [TAB]'
    return 1
  else
    echo '[INFO] Indentation with spaces'
    return 0
  fi
}

function helper_test_generic_only_allowed_characters_in_paths {
  local allowed_pattern='^[a-z0-9.\-/]*$'
  if find code hack vbd \
    | sed -E 's/^.*(LINK|DATA|OTHERS).lst$//g' \
    | sed -E 's/^.*(SPEC).txt$//g' \
    | sed -E 's/^.*(README).md$//g' \
    | grep -vP "${allowed_pattern}"; then
    echo "[ERROR] Paths contain characters outside of ${allowed_pattern}"
    return 1
  else
    echo "[INFO] Paths contain characters only in ${allowed_pattern}"
    return 0
  fi
}

function helper_test_generic_80_columns {
  local path

  helper_list_touched_files | while read -r path; do
    if grep \
      --binary-files=without-match \
      --exclude={.mailmap,.gitlab-ci.yml,*.lst,.envrc.public,build/**/*,**/README.md,docs/src/*,.commitlintrc.js,infra/*} \
      --exclude-dir=.git \
      --perl-regex \
      --quiet \
      '^.{81,}$' \
      "${path}"; then
      echo '[ERROR] Wrap your code at column 80'
      echo "  ${path}"
      return 1
    else
      echo "[INFO] Code is under 80 columns: ${path}"
    fi
  done
}

function helper_test_generic_pre_commit {
  helper_list_touched_files | xargs pre-commit run -v --files
}

function helper_threads_files_for_status_code {
  local file="${1}"
  sed 's/\r$//' $file > out.lst
  local content="$(cat out.lst)"
  local url

  while IFS= read -r url; do
    helper_threads_urls_for_status_code "$url" &
  done <<< "$(echo -e "$content")"
}

function helper_threads_urls_for_status_code {
  local url="${1}"
  local status_code

  status_code=$(curl -o /dev/null -s -w '%{http_code}' "$url")
  if test "${status_code}" = '200'; then
    echo "[INFO] HTTP ${status_code} in ${file_name}, url: '${url}'"
  else
    echo "[ERROR] HTTP ${status_code} in ${file_name}, url: '${url}'"
  fi
}

function helper_test_others_urls_status_code {
  local file_name="${1}"
  local touched="$(helper_list_touched_files | grep "${file_name}")"
  local content
  local errors

  if [[ $touched == *"${file_name}"* ]]; then
    while IFS= read -r file; do
      errors+="$(helper_threads_files_for_status_code "$file" &)"
      errors+="$(echo "\n")"
    done <<< "$(echo -e "$touched")"

    errors="$(echo -e "$errors")"
    echo "$errors" | tr "\n" "\n" | sort -r | tr "\n" "\n"

    if [[ $errors == *"[ERROR]"* ]]; then
      return 1
    fi
  fi
}

function helper_test_commit_msg_commitlint {
  local commit_diff
  local commit_hashes

  helper_use_pristine_workdir \
    && env_prepare_node_modules \
    && git fetch --prune > /dev/null \
    && if [ "${IS_LOCAL_BUILD}" = "${TRUE}" ]; then
      commit_diff="origin/master..${CI_COMMIT_REF_NAME}"
    else
      commit_diff="origin/master..origin/${CI_COMMIT_REF_NAME}"
    fi \
    && commit_hashes="$(git log --pretty=%h "${commit_diff}")" \
    && for commit_hash in ${commit_hashes}; do
      echo '[INFO] Running Commitlint' \
        && git log -1 --pretty=%B "${commit_hash}" | commitlint \
        || return 1
    done
}

function helper_pages_compile {
  mv app/src/* . \
    && nanoc
}

function terraform_login_dev {
  export TF_VAR_aws_access_key
  export TF_VAR_aws_secret_key
  export AWS_ACCESS_KEY_ID
  export AWS_SECRET_ACCESS_KEY

  AWS_ACCESS_KEY_ID="${DEV_AWS_ACCESS_KEY_ID}" \
    && AWS_SECRET_ACCESS_KEY="${DEV_AWS_SECRET_ACCESS_KEY}" \
    && sops_export_vars '../../../infra/base/secrets/dev.yml' \
      'CLOUDFLARE_API_KEY' \
      'CLOUDFLARE_ACCOUNT_ID' \
      'CLOUDFLARE_EMAIL' \
    && export TF_VAR_cloudflare_email="${CLOUDFLARE_EMAIL}" \
    && export TF_VAR_cloudflare_api_key="${CLOUDFLARE_API_KEY}" \
    && export TF_VAR_cloudflare_account_id="${CLOUDFLARE_ACCOUNT_ID}" \
    && TF_VAR_aws_access_key="${DEV_AWS_ACCESS_KEY_ID}" \
    && TF_VAR_aws_secret_key="${DEV_AWS_SECRET_ACCESS_KEY}"
}

function terraform_login_prod {
  export TF_VAR_aws_access_key
  export TF_VAR_aws_secret_key
  export AWS_ACCESS_KEY_ID
  export AWS_SECRET_ACCESS_KEY

  AWS_ACCESS_KEY_ID="${PROD_AWS_ACCESS_KEY_ID}" \
    && AWS_SECRET_ACCESS_KEY="${PROD_AWS_SECRET_ACCESS_KEY}" \
    && sops_export_vars '../../../infra/base/secrets/prod.yml' \
      'CLOUDFLARE_API_KEY' \
      'CLOUDFLARE_ACCOUNT_ID' \
      'CLOUDFLARE_EMAIL' \
    && export TF_VAR_cloudflare_email="${CLOUDFLARE_EMAIL}" \
    && export TF_VAR_cloudflare_api_key="${CLOUDFLARE_API_KEY}" \
    && export TF_VAR_cloudflare_account_id="${CLOUDFLARE_ACCOUNT_ID}" \
    && TF_VAR_aws_access_key="${PROD_AWS_ACCESS_KEY_ID}" \
    && TF_VAR_aws_secret_key="${PROD_AWS_SECRET_ACCESS_KEY}"
}

function sops_export_vars {
  sops_export_vars_by_profile "${1}" 'default' "${@:2}"
}

function sops_export_vars_by_profile {
  local manifest="${1}"
  local profile="${2}"

  echo "[INFO] Decrypting ${manifest} with profile ${profile}" \
    && json=$(
      sops \
        --aws-profile "${profile}" \
        --decrypt \
        --output-type json \
        "${manifest}"
    ) \
    && for var in "${@:3}"; do
      echo "[INFO] Exported: ${var}" \
        && export "${var//./__}=$(echo "${json}" | jq -erc ".${var}")" \
        || return 1
    done
}

function deploy_docs {
  local src='docs/src'
  local bucket
  export env="${1}"
  export CI_COMMIT_REF_NAME

  if [ "${env}" = 'prod' ]; then
    bucket='s3://docs.autonomicjump.com/'
  elif [ "${env}" = 'dev' ]; then
    if echo ${CI_COMMIT_REF_NAME} | grep "atfluid"; then
      bucket="s3://docs-dev.autonomicjump.com/${CI_COMMIT_REF_NAME}/"
    else
      echo '[INFO] Only mantainers can create ephemeral environments' \
        && return 0
    fi
  else
    echo '[ERROR] Either "prod" or "dev" must be passed as arg' \
      && return 1
  fi \
    && pushd "${src}" \
    && "aws_login_${env}" \
    && npm install \
    && npm run build \
    && aws s3 sync build "${bucket}" --delete \
    && rm -rf node_modules build .docusaurus yarn.lock \
    && popd \
    || return 1
}

function deploy_forms {
  local src='forms/src'
  local bucket
  export env="${1}"
  export CI_COMMIT_REF_NAME

  if [ "${env}" = 'prod' ]; then
    bucket='s3://forms.autonomicjump.com/'
  elif [ "${env}" = 'dev' ]; then
    if echo ${CI_COMMIT_REF_NAME} | grep "atfluid"; then
      bucket="s3://forms-dev.autonomicjump.com/${CI_COMMIT_REF_NAME}/"
    else
      echo '[INFO] Only mantainers can create ephemeral environments' \
        && return 0
    fi
  else
    echo '[ERROR] Either "prod" or "dev" must be passed as arg' \
      && return 1
  fi \
    && "aws_login_${env}" \
    && aws s3 sync "${src}" "${bucket}" --delete \
    && popd \
    || return 1
}

function deploy_app {
  local bucket
  export env="${1}"
  export CI_COMMIT_REF_NAME

  if [ "${env}" = 'prod' ]; then
    bucket='s3://app.autonomicjump.com/'
  elif [ "${env}" = 'dev' ]; then
    if echo ${CI_COMMIT_REF_NAME} | grep "atfluid"; then
      bucket="s3://app-dev.autonomicjump.com/${CI_COMMIT_REF_NAME}/"
    else
      echo '[INFO] Only mantainers can create ephemeral environments' \
        && return 0
    fi
  else
    echo '[ERROR] Either "prod" or "dev" must be passed as arg' \
      && return 1
  fi \
    && mv app/src/* . \
    && nanoc \
    && "aws_login_${env}" \
    && aws s3 sync public "${bucket}" --delete \
    && popd \
    || return 1
}

function aws_login_dev {
  echo '[INFO] Logging into AWS with development credentials' \
    && aws configure set 'aws_access_key_id' "${DEV_AWS_ACCESS_KEY_ID}" \
    && aws configure set 'aws_secret_access_key' "${DEV_AWS_SECRET_ACCESS_KEY}"
}

function aws_login_prod {
  echo '[INFO] Logging into AWS with production credentials' \
    && aws configure set 'aws_access_key_id' "${PROD_AWS_ACCESS_KEY_ID}" \
    && aws configure set 'aws_secret_access_key' "${PROD_AWS_SECRET_ACCESS_KEY}"
}
