# shellcheck shell=bash

source "${srcIncludeHelpers}"
source "${srcEnv}"

function job_build_nix_caches {
  local provisioners
  local dockerfile
  local context='.'
  local dockerfile_generic='build/Dockerfile.generic'
  local dockerfile_solutions='build/Dockerfile.solutions'

  helper_use_pristine_workdir \
    && provisioners=(./build/provisioners/*) \
    && helper_build_nix_caches_parallel \
    && for ((i = "${lower_limit}"; i <= "${upper_limit}"; i++)); do
      provisioner=$(basename "${provisioners[${i}]}") \
        && provisioner="${provisioner%.*}" \
        && if echo "${provisioner}" | grep -q 'build_solutions_'; then
          dockerfile="${dockerfile_solutions}"
        else
          dockerfile="${dockerfile_generic}"
        fi \
        && helper_docker_build_and_push \
          "${CI_REGISTRY_IMAGE}/nix:${provisioner}" \
          "${context}" \
          "${dockerfile}" \
          'PROVISIONER' "${provisioner}" \
        || return 1
    done
}

function job_build_solutions_gherkin {
  local builder='build/builders/solutions/gherkin'
  local extensions='*.feature'
  local folders=(
    'code'
    'hack'
    'vbd'
  )
  if helper_is_solution_commit; then
    helper_use_pristine_workdir \
      && helper_build_lang_solutions \
        "${builder}" \
        "${extensions}" \
        "${folders[@]}"
  else
    return 0
  fi
}

function job_build_solutions_lobster {
  local builder='build/builders/solutions/lobster'
  local extensions='*.lobster'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_dale {
  local builder='build/builders/solutions/dale'
  local extensions='*.dt'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_colm {
  local builder='build/builders/solutions/colm'
  local extensions='*.lm'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_clean {
  local builder='build/builders/solutions/ats'
  local extensions='*.icl'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_ats {
  local builder='build/builders/solutions/ats'
  local extensions='*.dats'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_tcsh {
  local builder='build/builders/solutions/tcsh'
  local extensions='*.csh'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_rc {
  local builder='build/builders/solutions/rc'
  local extensions='*.rc'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_ion {
  local builder='build/builders/solutions/ion'
  local extensions='*.ion'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_fish {
  local builder='build/builders/solutions/fish'
  local extensions='*.fish'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_elvish {
  local builder='build/builders/solutions/elvish'
  local extensions='*.elv'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_standardml {
  local builder='build/builders/solutions/standardml'
  local extensions='*.sml'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_racket {
  local builder='build/builders/solutions/racket'
  local extensions='*.rkt'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_idris2 {
  local builder='build/builders/solutions/idris2'
  local extensions='*.idr'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_joker {
  local builder='build/builders/solutions/joker'
  local extensions='*.joke'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_janet {
  local builder='build/builders/solutions/janet'
  local extensions='*.janet'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_falcon {
  local builder='build/builders/solutions/falcon'
  local extensions='*.fal'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_java {
  local builder='build/builders/solutions/java'
  local extensions='*.java'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_shell {
  local builder='build/builders/solutions/shell'
  local extensions='*.sh'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_nix {
  local builder='build/builders/solutions/nix'
  local extensions='*.nix'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_python {
  local builder='build/builders/solutions/python'
  local extensions='*.py'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_csharp {
  local builder='build/builders/solutions/csharp'
  local extensions='*.cs'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_javascript {
  local builder='build/builders/solutions/javascript'
  local extensions='*.js'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_ruby {
  local builder='build/builders/solutions/ruby'
  local extensions='*.rb'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_clojure {
  local builder='build/builders/solutions/clojure'
  local extensions='*.clj'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_cpp {
  local builder='build/builders/solutions/cpp'
  local extensions='*.cpp'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_scala {
  local builder='build/builders/solutions/scala'
  local extensions='*.scala'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_php {
  local builder='build/builders/solutions/php'
  local extensions='*.php'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_rust {
  local builder='build/builders/solutions/rust'
  local extensions='*.rs'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_d {
  local builder='build/builders/solutions/d'
  local extensions='*.d'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_crystal {
  local builder='build/builders/solutions/crystal'
  local extensions='*.cr'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_ocaml {
  local builder='build/builders/solutions/ocaml'
  local extensions='*.ml'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_haskell {
  local builder='build/builders/solutions/haskell'
  local extensions='*.hs'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_go {
  local builder='build/builders/solutions/go'
  local extensions='*.go'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_c {
  local builder='build/builders/solutions/c'
  local extensions='*.c'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_lua {
  local builder='build/builders/solutions/lua'
  local extensions='*.lua'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_r {
  local builder='build/builders/solutions/r'
  local extensions='*.r'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_erlang {
  local builder='build/builders/solutions/erlang'
  local extensions='*.erl'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_dart {
  local builder='build/builders/solutions/dart'
  local extensions='*.dart'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_coffeescript {
  local builder='build/builders/solutions/coffeescript'
  local extensions='*.coffee'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_elixir {
  local builder='build/builders/solutions/elixir'
  local extensions='*.exs'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_mercury {
  local builder='build/builders/solutions/mercury'
  local extensions='*.mc'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_groovy {
  local builder='build/builders/solutions/groovy'
  local extensions='*.groovy'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_icon {
  local builder='build/builders/solutions/icon'
  local extensions='*.icn'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_lfe {
  local builder='build/builders/solutions/lfe'
  local extensions='*.lfe'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_raku {
  local builder='build/builders/solutions/raku'
  local extensions='*.raku'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_rexx {
  local builder='build/builders/solutions/rexx'
  local extensions='*.rx'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_red {
  local builder='build/builders/solutions/red'
  local extensions='*.red'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_curry {
  local builder='build/builders/solutions/curry'
  local extensions='*.curry'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_pony {
  local builder='build/builders/solutions/pony'
  local extensions='*.pony'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_reason {
  local builder='build/builders/solutions/reason'
  local extensions='*.re'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_hy {
  local builder='build/builders/solutions/hy'
  local extensions='*.hy'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_neko {
  local builder='build/builders/solutions/neko'
  local extensions='*.neko'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_nim {
  local builder='build/builders/solutions/nim'
  local extensions='*.nim'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_genie {
  local builder='build/builders/solutions/genie'
  local extensions='*.gs'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_picat {
  local builder='build/builders/solutions/picat'
  local extensions='*.pi'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_jq {
  local builder='build/builders/solutions/jq'
  local extensions='*.jq'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_nasm_x64 {
  local builder='build/builders/solutions/nasm_x64'
  local extensions='*.asm'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_vhdl {
  local builder='build/builders/solutions/vhdl'
  local extensions='*.vhdl'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_nasm_x32 {
  local builder='build/builders/solutions/nasm_x32'
  local extensions='*.s'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_verilog {
  local builder='build/builders/solutions/verilog'
  local extensions='*.vl'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_oz {
  local builder='build/builders/solutions/oz'
  local extensions='*.oz'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_guile {
  local builder='build/builders/solutions/guile'
  local extensions='*.scm'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_vala {
  local builder='build/builders/solutions/vala'
  local extensions='*.vala'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_fsharp {
  local builder='build/builders/solutions/fsharp'
  local extensions='*.fs'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_haxe {
  local builder='build/builders/solutions/haxe'
  local extensions='*.hx'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_octave {
  local builder='build/builders/solutions/octave'
  local extensions='*.m'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_perl {
  local builder='build/builders/solutions/perl'
  local extensions='*.pl'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_pascal {
  local builder='build/builders/solutions/pascal'
  local extensions='*.pas'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_awk {
  local builder='build/builders/solutions/awk'
  local extensions='*.awk'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_cobol {
  local builder='build/builders/solutions/cobol'
  local extensions='*.cbl'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_fortran {
  local builder='build/builders/solutions/fortran'
  local extensions='*.f90'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_prolog {
  local builder='build/builders/solutions/prolog'
  local extensions='*.pro'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_kotlin {
  local builder='build/builders/solutions/kotlin'
  local extensions='*.kt'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_typescript {
  local builder='build/builders/solutions/typescript'
  local extensions='*.ts'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_lisp {
  local builder='build/builders/solutions/lisp'
  local extensions='*.lsp'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_build_solutions_clojurescript {
  local builder='build/builders/solutions/clojurescript'
  local extensions='*.cljs'
  local folders=(
    'code'
  )

  helper_use_pristine_workdir \
    && helper_build_lang_solutions \
      "${builder}" \
      "${extensions}" \
      "${folders[@]}"
}

function job_infra_base_test {

  helper_use_pristine_workdir \
    && pushd infra/base/terraform || return 1 \
    && terraform_login_dev \
    && terraform init \
    && terraform plan -lock=false -refresh=true \
    && tflint \
    && popd || return 1
}

function job_infra_dns_test {

  helper_use_pristine_workdir \
    && pushd infra/dns/terraform || return 1 \
    && terraform_login_dev \
    && terraform init \
    && terraform plan -lock=false -refresh=true \
    && tflint \
    && popd || return 1
}

function job_infra_docs_test {

  helper_use_pristine_workdir \
    && pushd docs/infra/terraform || return 1 \
    && terraform_login_dev \
    && terraform init \
    && terraform plan -lock=false -refresh=true \
    && tflint \
    && popd || return 1
}

function job_infra_forms_test {

  helper_use_pristine_workdir \
    && pushd forms/infra/terraform || return 1 \
    && terraform_login_dev \
    && terraform init \
    && terraform plan -lock=false -refresh=true \
    && tflint \
    && popd || return 1
}

function job_infra_app_test {

  helper_use_pristine_workdir \
    && pushd app/infra/terraform || return 1 \
    && terraform_login_dev \
    && terraform init \
    && terraform plan -lock=false -refresh=true \
    && tflint \
    && popd || return 1
}

function job_test_schemas {
  local lang_data_supported='code/lang-data-supported.yml'
  local lang_schema_supported='code/lang-schema-supported.yml'
  local lang_data_dropped='code/lang-data-dropped.yml'
  local lang_schema_dropped='code/lang-schema-dropped.yml'
  local policies_data='policies/data.yaml'
  local policies_schema='policies/schema.yaml'

  helper_use_pristine_workdir \
    && env_prepare_python_packages \
    && helper_test_pykwalify \
      "${lang_data_supported}" \
      "${lang_schema_supported}" \
    && helper_test_pykwalify \
      "${lang_data_dropped}" \
      "${lang_schema_dropped}" \
    && helper_test_pykwalify \
      "${policies_data}" \
      "${policies_schema}" \
    && helper_test_schemas_site_data
}

function job_test_generic {
  helper_use_pristine_workdir \
    && env_prepare_python_packages \
    && env_prepare_ruby_modules \
    && helper_test_generic_dir_depth \
    && helper_test_generic_allowed_evidences \
    && helper_test_generic_misplaced_evidences \
    && helper_test_generic_allowed_mimes \
    && helper_test_generic_atfluid_account \
    && helper_test_generic_short_filenames \
    && helper_test_generic_raw_github_urls \
    && helper_test_generic_no_asc_extension \
    && helper_test_generic_no_tabs \
    && helper_test_generic_only_allowed_characters_in_paths \
    && helper_test_generic_80_columns \
    && helper_test_generic_pre_commit \
    && prospector --profile build/configs/prospector.yaml build
}

function job_test_others {
  helper_use_pristine_workdir \
    && helper_test_generic_others_duplicates \
    && helper_test_generic_others_code_has_unique_ext \
    && helper_test_generic_others_code_raw_only \
    && helper_test_generic_others_code_sort_by_ext \
    && helper_test_others_urls_status_code 'LINK.lst' \
    && helper_test_others_urls_status_code 'OTHERS.lst'
}

function job_test_policy {
  if helper_is_solution_commit; then
    helper_use_pristine_workdir \
      && env_prepare_python_packages \
      && helper_list_touched_files | python3 build/modules/test-policy/main.py
  else
    return 0
  fi
}

function job_test_commit_msg {
  helper_use_pristine_workdir \
    && env_prepare_python_packages \
    && helper_test_commit_msg_commitlint \
    && pushd build/modules/parse-commit-msg \
    && pushd tests \
    && echo '[INFO] Testing the parser' \
    && ./test-commit-msg-parser.sh \
    && popd || return 1 \
    && echo '[INFO] Using the parser to check your commit message' \
    && ./commit_msg_parser.py \
    && popd || return 1
}

function job_test_markdown_docs {
  helper_use_pristine_workdir \
    && mdl --style 'build/modules/test-markdown-docs/style.rb' 'docs'
}

function job_test_user_yaml {
  if helper_is_solution_commit; then
    helper_use_pristine_workdir \
      && env_prepare_python_packages \
      && helper_list_touched_files | python3 build/modules/test-user-yaml/main.py \
      && helper_test_schemas_user_yaml
  else
    return 0
  fi
}

function job_infra_base_deploy {

  helper_use_pristine_workdir \
    && pushd infra/base/terraform || return 1 \
    && terraform_login_prod \
    && terraform init \
    && terraform apply -auto-approve -refresh=true \
    && popd || return 1
}

function job_infra_dns_deploy {

  helper_use_pristine_workdir \
    && pushd infra/dns/terraform || return 1 \
    && terraform_login_prod \
    && terraform init \
    && terraform apply -auto-approve -refresh=true \
    && popd || return 1
}

function job_infra_docs_deploy {

  helper_use_pristine_workdir \
    && pushd docs/infra/terraform || return 1 \
    && terraform_login_prod \
    && terraform init \
    && terraform apply -auto-approve -refresh=true \
    && popd || return 1
}

function job_infra_forms_deploy {

  helper_use_pristine_workdir \
    && pushd forms/infra/terraform || return 1 \
    && terraform_login_prod \
    && terraform init \
    && terraform apply -auto-approve -refresh=true \
    && popd || return 1
}

function job_infra_app_deploy {

  helper_use_pristine_workdir \
    && pushd app/infra/terraform || return 1 \
    && terraform_login_prod \
    && terraform init \
    && terraform apply -auto-approve -refresh=true \
    && popd || return 1
}

function job_deploy_docs_prod {
  helper_use_pristine_workdir \
    && deploy_docs 'prod'
}

function job_deploy_docs_dev {
  helper_use_pristine_workdir \
    && deploy_docs 'dev'
}

function job_deploy_forms_prod {
  helper_use_pristine_workdir \
    && deploy_forms 'prod'
}

function job_deploy_forms_dev {
  helper_use_pristine_workdir \
    && deploy_forms 'dev'
}

function job_deploy_app_prod {
  helper_use_pristine_workdir \
    && env_set_utf8_encoding \
    && env_prepare_ruby_modules \
    && deploy_app 'prod'
}

function job_deploy_app_dev {
  helper_use_pristine_workdir \
    && env_set_utf8_encoding \
    && env_prepare_ruby_modules \
    && deploy_app 'dev'
}

function job_pages_local {
  helper_use_pristine_workdir \
    && env_set_utf8_encoding \
    && env_prepare_ruby_modules \
    && helper_pages_compile \
    && nanoc view
}

function job_reviews {
  reviews .reviews.toml
}
