let
  pkgs = import (import ../pkgs/src/fetch-src.nix {
    repo = "https://github.com/NixOS/nixpkgs";
    commit = "31ba37e111b3a5fef691aac95c4dcc19724d6aa8";
    digest = "0lmghwhky1v7wvl9imsi5x4zm2rgbjmq58spnqw82f5g8mzz1mcm";
  }) { };
  builders.rubyGem = import ../builders/ruby-gem pkgs;
in
  pkgs.stdenv.mkDerivation (
        (import ../src/basic.nix)
    //  (rec {
          name = "builder";

          buildInputs = [
            pkgs.git
            pkgs.coreutils
            pkgs.awscli
            pkgs.git
            pkgs.cacert
            pkgs.glibcLocales
            pkgs.ruby
            pkgs.rubyPackages.nokogiri
          ];

          rubyGemNanoc = builders.rubyGem "nanoc:4.7.4";
          rubyGemSlim = builders.rubyGem "slim:4.0.1";
          rubyGemAdsf = builders.rubyGem "adsf:1.2.1";
        })
  )
