let
  pkgs = import ../pkgs/stable.nix;

  srcProduct = import (pkgs.fetchzip {
    url = "https://gitlab.com/fluidattacks/product/-/archive/b08f1faa003570f3acda49cefd2fdf7770d429a0.zip";
    sha256 = "17mfil4dbaana9ri0vqs12jjfdjkq55q25ngspxc6jjrc2jfmv8h";
  });

in
  pkgs.stdenv.mkDerivation (
       (import ../src/basic.nix)
    // (import ../src/external.nix pkgs)
    // (rec {
      name = "builder";

      buildInputs = [
        pkgs.git
        pkgs.nix
        srcProduct.reviews
      ];
    })
  )
