{ pkgs, inputs }:

let
  base = [
    pkgs.git
    pkgs.nix
    pkgs.parallel
  ];
in
  pkgs.stdenv.mkDerivation (
        (import ../src/basic.nix)
    //  (import ../src/external.nix pkgs)
    //  (rec {
          name = "builder";

          buildInputs = base ++ inputs;
        })
  )
