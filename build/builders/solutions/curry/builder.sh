source "${srcGeneric}"

function compile {
  local solution="${1}"
  local HOME='.'

  pakcs \
    :load \
    "${solution}" \
    :quit >&output.log \
    && cat output.log \
    && if egrep -q 'Warning|Error' output.log; then
      rm output.log \
        && return 1
    else
      rm output.log \
        && return 0
    fi
}

function build {
  generic_set_utf_8 \
    && generic_get_solution \
    && compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
