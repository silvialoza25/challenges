{ solutionPath }:

let
  pkgs = import (import ../../../pkgs/src/fetch-src.nix {
    repo = "https://github.com/NixOS/nixpkgs";
    commit = "acc5f7b18a60bc9b1024e5e1882bf7362e6492e6";
    digest = "193zwmyji0qp31n8faqvl58xcp2kjaip5718y10hynggijh72srb";
  }) { };
  inputs = [
    pkgs.pakcs
  ];
in
  pkgs.stdenv.mkDerivation (
        (import ../generic { inherit solutionPath pkgs inputs; })
    //  (rec {
          builder = ./builder.sh;
        })
  )
