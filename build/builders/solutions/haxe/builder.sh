source "${srcGeneric}"

function lint {
  local solution="${1}"
  local HOME='.'

  haxelib setup . \
    && haxelib install checkstyle \
    && haxelib run checkstyle \
      --exitcode \
      --show-parser-errors \
      --nothreads \
      --default-config \
      -s "${solution}"
}

function compile {
  local solution="${1}"
  local langs=(
    'lua'
    'neko'
    'php'
    'python'
    'hl'
  )

  cp "${solution}" Main.hx \
    && for lang in "${langs[@]}"; do
      haxe \
        -"${lang}" \
        "output.${lang}" \
        -main Main \
        || return 1
    done
}

function build {
  generic_set_utf_8 \
    && generic_get_solution \
    && lint "root/src/${solutionFileName}" \
    && compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
