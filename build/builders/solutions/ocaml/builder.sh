source "${srcGeneric}"

function compile {
  local solution="${1}"

  ocamlc \
    -w @0..1000 \
    "${solution}"
}

function build {
  generic_set_utf_8 \
    && generic_get_solution \
    && compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
