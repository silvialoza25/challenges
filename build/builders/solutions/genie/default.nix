{ solutionPath }:

let
  pkgs = import (import ../../../pkgs/src/fetch-src.nix {
    repo = "https://github.com/NixOS/nixpkgs";
    commit = "519606eb119aa38845ba1c10bd0a225a4a343d00";
    digest = "1nzjrl1cakr9b4z0f4zjd20ndx5yci068kgliybkl9nhm6qxk6nl";
  }) { };
  inputs = [
    pkgs.vala
    pkgs.glib
    pkgs.pkg-config
  ];
in
  pkgs.stdenv.mkDerivation (
        (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
    //  (rec {
          builder = ./builder.sh;
        })
  )
