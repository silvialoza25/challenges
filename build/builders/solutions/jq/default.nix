{ solutionPath }:

let
  pkgs = import (import ../../../pkgs/src/fetch-src.nix {
    repo = "https://github.com/NixOS/nixpkgs";
    commit = "2e6bf42a2207d5ecfe6e67de2def6e004a0eb1f1";
    digest = "1a260v085gzdfy76xggsaip7gfm54xyb5j62nk29i08pm045vzlp";
  }) { };
  inputs = [
    pkgs.jq
  ];
in
  pkgs.stdenv.mkDerivation (
        (import ../generic { inherit solutionPath pkgs inputs; })
    //  (rec {
          builder = ./builder.sh;
        })
  )
