{ solutionPath }:

let
  pkgs = import ../../../pkgs/old.nix;
  inputs = [
    pkgs.nix-linter
  ];
in
  pkgs.stdenv.mkDerivation (
        (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
    //  (rec {
          builder = ./builder.sh;
        })
  )
