source "${stdenv}/setup"
source "${srcShellOptions}"
source "${srcDirStructure}"
source "${srcEnv}"

function generic_set_utf_8 {
  export LC_ALL='en_US.UTF-8'
}

function generic_get_solution {
  cp -r --no-preserve=ownership \
    "${srcSolution}" "root/src/${solutionFileName}"
}
