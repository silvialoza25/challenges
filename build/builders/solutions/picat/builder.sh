source "${srcGeneric}"

function compile {
  local solution="${1}"

  echo "main => compile(read_line())" \
    > compiler.pi \
    && echo \
      "${solution%.*}" \
    | picat \
      -log \
      compiler.pi
}

function build {
  generic_set_utf_8 \
    && generic_get_solution \
    && compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
