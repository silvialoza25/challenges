{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  inputs = [
    pkgs.erlang
    pkgs.lfe
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            builder = ./builder.sh;
          })
    )
