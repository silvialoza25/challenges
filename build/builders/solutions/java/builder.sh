source "${srcGeneric}"

function helper_lint_prettier {
  local solution="${1}"

  prettier \
    --plugin="${nodeJsModulePrettierjava}/node_modules/prettier-plugin-java" \
    --check \
    --config "${srcPrettierConfig}" \
    "${solution}"
}

function lint {
  local solution="${1}"
  local solution_lint_log
  local regex_linter='(ERROR|WARN|Checkstyle ends with [0-9]* errors.)'
  local lizard_max_warns='0'
  local lizard_max_func_length='30'
  local lizard_max_ccn='10'

  solution_lint_log=$(
    java \
      -jar "${srcJavaCheckstyle}" \
      -c "${srcJavaCheckstyleCofig}" \
      "${solution}" \
      2>&1
  ) || true \
    && if echo "${solution_lint_log}" | grep -P "${regex_linter}"; then
      return 1
    fi \
    && if helper_lint_prettier "${solution}"; then
      continue
    else
      echo '[ERROR] Please run prettier on your solution with config:' \
        && echo "${srcPrettierConfig}" \
        && return 1
    fi \
    && lizard \
      --ignore_warnings "${lizard_max_warns}" \
      --length "${lizard_max_func_length}" \
      --CCN "${lizard_max_ccn}" \
      "${solution}"
}

function compile {
  local solution="${1}"

  javac "${solution}"
}

function build {
  env_prepare_ephemeral_vars \
    && env_prepare_node_modules \
    && env_prepare_python_packages \
    && generic_set_utf_8 \
    && generic_get_solution \
    && lint "root/src/${solutionFileName}" \
    && compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
