source "${srcGeneric}"

function lint {
  local solution="${1}"
  local line_length='80'

  fortran-linter \
    --syntax-only \
    --linelength "${line_length}" \
    --verbose \
    "${solution}"
}

function compile {
  local solution="${1}"

  gfortran \
    -Wall \
    -pedantic-errors \
    -Werror \
    "${solution}" \
    -o "${solution%.*}"
}

function build {
  env_prepare_ephemeral_vars \
    && env_prepare_python_packages \
    && generic_set_utf_8 \
    && generic_get_solution \
    && lint "root/src/${solutionFileName}" \
    && compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
