source "${srcGeneric}"

function lint {
  local solution="${1}"

  ktlint \
    --experimental \
    --verbose \
    --disabled_rules=experimental:indent,indent \
    "${solution}"
}

function compile {
  local solution="${1}"

  kotlinc \
    -Werror \
    "${solution}" \
    -include-runtime \
    -d output.jar
}

function build {
  generic_set_utf_8 \
    && generic_get_solution \
    && lint "root/src/${solutionFileName}" \
    && compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
