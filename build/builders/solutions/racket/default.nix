{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  inputs = [
    pkgs.racket
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            builder = ./builder.sh;
          })
    )
