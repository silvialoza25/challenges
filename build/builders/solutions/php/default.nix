{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  builders.nodePackage = import ../../../builders/nodejs-module pkgs;
  builders.composerPackage = import ../../../builders/composer-module pkgs;
  builders.pythonPackage = import ../../../builders/python-package pkgs;
  inputs = [
    pkgs.nodejs
    pkgs.php74
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            srcPrettierConfig = ../../../configs/prettier-php.yaml;

            builder = ./builder.sh;

            composerModulePhplint = builders.composerPackage "overtrue/phplint:2.0.2";

            nodeJsModulePrettier = builders.nodePackage "prettier@2.0.5";
            nodeJsModulePrettierphp = builders.nodePackage "@prettier/plugin-php@0.14.2";

            pyPkgLizard = builders.pythonPackage "lizard==1.17.3";
          })
    )
