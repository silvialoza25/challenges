source "${srcGeneric}"

function compile {
  local solution="${1}"

  if echo $(
    guile-tools compile -O3 \
      -W unsupported-warning \
      -W unused-variable \
      -W unused-toplevel \
      -W shadowed-toplevel \
      -W unbound-variable \
      -W macro-use-before-definition \
      -W arity-mismatch \
      -W duplicate-case-datum \
      -W bad-case-datum \
      -W format \
      -o "${solution}.go" \
      "${solution}" 2>&1
  ) | egrep 'warning|error'; then
    exit 1
  fi
}

function build {
  export GUILE_AUTO_COMPILE=0 \
    && generic_set_utf_8 \
    && generic_get_solution \
    && compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
