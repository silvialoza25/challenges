{ solutionPath }:

let
  pkgs = import (import ../../../pkgs/src/fetch-src.nix {
    repo = "https://github.com/NixOS/nixpkgs";
    commit = "3cccf9088168fb71cfd66eaadfad136fa03bb05a";
    digest = "1qmfl5f4pwhgklcjzrdxidihsm66h3ryv18p2msggbq7bzanc5g8";
  }) { };
  inputs = [
    pkgs.lobster
  ];
in
  pkgs.stdenv.mkDerivation (
        (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
    //  (rec {
          builder = ./builder.sh;
        })
  )
