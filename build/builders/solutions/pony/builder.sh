source "${srcGeneric}"

function compile {
  local solution="${1}"

  mkdir "${solutionFileName%.*}" \
    && mv "${solution}" "${solutionFileName%.*}" \
    && ponyc "${solutionFileName%.*}"
}

function build {
  generic_set_utf_8 \
    && generic_get_solution \
    && compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
