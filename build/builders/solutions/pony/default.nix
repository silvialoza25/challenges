{ solutionPath }:

let
  pkgs = import (import ../../../pkgs/src/fetch-src.nix {
    repo = "https://github.com/NixOS/nixpkgs";
    commit = "f5ccc3e5fe6a06f6c6f71f58e02f1928ce9ee2e4";
    digest = "01ig0dk2bra8zf9g1lyz4429r4j3gm14lh2hbyykfmcn01588lyj";
  }) { };
  inputs = [
    pkgs.ponyc
  ];
in
  pkgs.stdenv.mkDerivation (
        (import ../generic { inherit solutionPath pkgs inputs; })
    //  (rec {
          builder = ./builder.sh;
        })
  )
