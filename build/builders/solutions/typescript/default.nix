{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  builders.nodePackage = import ../../../builders/nodejs-module pkgs;
  inputs = [
    pkgs.nodejs
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            srcEslintConfig = ../../../configs/eslint-typescript.json;
            srcPrettierConfig = ../../../configs/prettier-generic.yaml;

            builder = ./builder.sh;

            nodeJsModuleTypescript = builders.nodePackage "typescript@3.9.5";
            nodeJsModuleTypes = builders.nodePackage "@types/node@14.0.13";
            nodeJsModuleYargs = builders.nodePackage "yargs@15.3.1";

            nodeJsModulePrettier =
              builders.nodePackage "prettier@2.0.5";
            nodeJsModuleEslint =
              builders.nodePackage "eslint@7.1.0";
            nodeJsModuleEslintstrict =
              builders.nodePackage "eslint-config-strict@14.0.1";
            nodeJsModuleEslintTypescriptParser =
              builders.nodePackage "@typescript-eslint/parser@3.2.0";
            nodeJsModuleEslintTypescriptPlugin =
              builders.nodePackage "@typescript-eslint/eslint-plugin@3.2.0";
            nodeJsModuleEslintpluginfs =
              builders.nodePackage "eslint-plugin-fp@2.3.0";
          })
    )
