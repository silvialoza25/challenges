source "${srcGeneric}"

function compile {
  local solution="${1}"

  red "${solution}" | tee text.log \
    && if egrep "Error|Warning" text.log; then
      exit 1
    fi
}

function build {
  generic_set_utf_8 \
    && generic_get_solution \
    && compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
