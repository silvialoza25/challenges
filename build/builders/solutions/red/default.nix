{ solutionPath }:

let
  pkgs = import (import ../../../pkgs/src/fetch-src.nix {
    repo = "https://github.com/NixOS/nixpkgs";
    commit = "001c0cbe54228f88d5634f431fcaf460b8ff4590";
    digest = "14blxg17xl32z0byx1sw6s552r4gghpixcw6380m142r8dd24g30";
  }) { };
  inputs = [
    pkgs.red
  ];
in
  pkgs.stdenv.mkDerivation (
        (import ../generic { inherit solutionPath pkgs inputs; })
    //  (rec {
          builder = ./builder.sh;
        })
  )
