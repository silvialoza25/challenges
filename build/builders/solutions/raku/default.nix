{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  inputs = [
    pkgs.rakudo
  ];
in
  pkgs.stdenv.mkDerivation (
    (import ../generic { inherit solutionPath pkgs inputs; })
//  (rec {
      builder = ./builder.sh;
    })
  )
