source "${srcGeneric}"

function compile {
  local solution="${1}"

  mmc -f "${solution}" \
    && mmc \
      --make \
      --halt-at-warn \
      --warn-unused-imports \
      --warn-unused-args \
      "${solutionFileName%.*}"
}

function build {
  generic_set_utf_8 \
    && generic_get_solution \
    && compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
