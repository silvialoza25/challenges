source "${srcGeneric}"

function compile {
  local solution="${1}"
  local HOME='.'

  ghdl \
    -a \
    "${solution}" \
    && ghdl \
      -e \
      "${solutionFileName%.*}"

}

function build {
  generic_set_utf_8 \
    && generic_get_solution \
    && compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
