{ solutionPath }:

let
  pkgs = import (import ../../../pkgs/src/fetch-src.nix {
    repo = "https://github.com/NixOS/nixpkgs";
    commit = "9a87970b7db99c92df01c4650e8e9f40ba672483";
    digest = "1rv7m4y0fbpdlal2qd1r55fwsrl2nm3wg2m72vh05fmvsvsbqmxl";
  }) { };
  inputs = [
    pkgs.elvish
  ];
in
  pkgs.stdenv.mkDerivation (
        (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
    //  (rec {
          builder = ./builder.sh;
        })
  )
