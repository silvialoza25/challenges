source "${srcGeneric}"

function compile {
  clm \
    "${solutionFileName}" \
    -o \
    "${solutionFileName}"
}

function build {
  generic_set_utf_8 \
    && generic_get_solution \
    && compile
}

build || exit 1
echo > "${out}"
