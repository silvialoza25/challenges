{ solutionPath }:

let
  pkgs = import ../../../pkgs/stable.nix;
  inputs = [
    pkgs.R
    pkgs.rPackages.lintr
  ];
in
    pkgs.stdenv.mkDerivation (
          (import ../generic { inherit solutionPath; inherit pkgs; inherit inputs; })
      //  (rec {
            builder = ./builder.sh;
          })
    )
