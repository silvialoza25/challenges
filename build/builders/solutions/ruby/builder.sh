source "${srcGeneric}"

function lint {
  local solution="${1}"

  rubocop \
    "${solution}"
}

function build {
  generic_set_utf_8 \
    && generic_get_solution \
    && lint "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
