# shellcheck shell=bash

source "${stdenv}/setup"
source "${srcIncludeGenericShellOptions}"
source "${srcIncludeGenericDirStructure}"

export USER="projects"

pushd root/cargo || exit 1 \
  && export HOME="${PWD}" \
  && cargo new project \
  && cat "${path}" > project/Cargo.toml \
  && mkdir project/src/bin \
  && pushd project/ \
  && cargo build \
  && popd \
  && mkdir "${out}" \
  && mv ./* "${out}"
