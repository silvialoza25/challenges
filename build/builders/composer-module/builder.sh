# shellcheck shell=bash

source "${stdenv}/setup"
source "${srcIncludeGenericShellOptions}"
source "${srcIncludeGenericDirStructure}"

pushd root/composer || exit 1

HOME=. composer require "${requirement}"

mkdir "${out}"
mv ./* "${out}"
