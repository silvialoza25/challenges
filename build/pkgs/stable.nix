let
  src = import ./src/fetch-src.nix {
    repo = "https://github.com/NixOS/nixpkgs";
    commit = "074c9cbe1cd42301c142401e0604d520bf3dae9f";
    digest = "1q027y218k0riw6gddlnsh2yd2f747fdia4gyh6gd0pnajzxc3px";
  };
in
  import src { }
