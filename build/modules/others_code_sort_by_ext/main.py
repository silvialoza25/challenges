#!/usr/bin/env python3

"""
This script checks sorted urls in OTHERS.lst files
"""

import os
import sys
import re
from typing import List, Iterable


def ext(url) -> str:
    """Take the url an return the possible extension"""
    return url.split('.')[-1]


def has_extension(url) -> bool:
    """
    Take url eval if you have a recognizable extension with 2 criteria,
    the first if the extension is less than 7 characters long and
    the second if the extension does not have any special character
    """
    has_ext_len = len(ext(url)) <= 7
    especial_chars = bool(re.search("[^A-z0-9]", ext(url)))
    condition = has_ext_len and not especial_chars
    return condition


def sort_by_ext(urls) -> List[str]:
    """
    Take the urls, make a list with urls with recognizable extension
    and a list with urls without recognizable extension then sort all
    list and make a new list with sorted urls
    """
    ext_urls = [[ext(url), url] for url in urls if has_extension(url)]
    no_ext_urls = [url for url in urls if not has_extension(url)]
    ext_urls.sort()
    no_ext_urls.sort()
    sorted_urls = [url[1] for url in ext_urls] + no_ext_urls
    return sorted_urls


def normalize_others_file(file_path):
    """Extract urls from a OTHERS.lst file, filter them, and return them."""
    with open(file_path) as file_handle:
        urls = file_handle.read().splitlines()
        return tuple(sort_by_ext(urls)), tuple(urls)


def yield_others(starting_folder) -> Iterable[str]:
    """Recursively yield paths to OTHERS.lst files starting from folder."""
    for dirpath, _, filenames in os.walk(starting_folder):
        for filename in filter('OTHERS.lst'.__eq__, filenames):
            file_path = os.path.join(dirpath, filename)
            yield file_path


def check_for_sorted_urls(starting_folder) -> bool:
    """Parse every OTHER.lst file and check if it has sorted urls."""
    for others_file_path in yield_others(starting_folder):
        normalized_urls, entry_urls = normalize_others_file(others_file_path)
        if entry_urls != normalized_urls:
            print('[ERROR] URLs must be ordered alphabetically', end=' ')
            print('by extension and url.')
            print('Example to follow:')
            for url in normalized_urls:
                print('  ', url)
            print('  ', others_file_path)
            return True
    return False


def main():
    """Usual entrypoint."""
    folder = 'code'
    if check_for_sorted_urls(folder):
        sys.exit(1)
    sys.exit(0)


if __name__ == '__main__':
    main()
