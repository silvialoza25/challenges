#!/usr/bin/env python3

import os
import sys
import re
import glob
from typing import Any, List, Dict
from ruamel.yaml import YAML


def log_info(*args: Any, **kwargs: Any) -> None:
    """Logger for info category."""
    print('[INFO]', *args, **kwargs)


def log_err(*args: Any, **kwargs: Any) -> None:
    """Logger for error category."""
    print('[ERROR]', *args, **kwargs)


def get_yaml_list(
        valid_solutions: List[str],
        yaml_list: List[Dict[str, Any]]) -> Any:
    if not valid_solutions:
        return yaml_list

    head, *tail = valid_solutions
    yaml: Any = YAML(typ='safe')
    with open(head, 'r') as solution_path:
        dict_list = dict(yaml.load(solution_path))
        dict_list['root-path'] = head
        return \
            get_yaml_list(tail, yaml_list + [dict_list])


def get_date(item: Dict[str, Any]) -> Any:
    return item['date']


def get_previous_codeabbey_commit(username: str) -> Any:
    valid_solutions: List[str] = \
        glob.glob(f'./code/codeabbey/**/{username}.yml')
    yaml_list: List[Dict[str, Any]] = get_yaml_list(valid_solutions, [])

    sorted_by_date: List[Dict[str, Any]] = \
        sorted(yaml_list, key=get_date, reverse=True)

    if len(sorted_by_date) > 1:
        return sorted_by_date[1]
    return None


def get_previous_codeabbey_complexity(username: str) -> float:
    complexity: float = 0
    previous_commit: Any = get_previous_codeabbey_commit(username)
    if previous_commit is not None:
        complexity = get_solution_complexity(previous_commit)
    return complexity


def get_next_complexity(
        c_current: float,
        c_min: float,
        c_goal: float,
        c_min_step: float,
        c_max_step: float) -> Dict[str, float]:
    next_min: float = c_min
    next_max: float = c_min + c_max_step
    if c_current < c_goal:
        next_min = c_current + c_min_step
        next_max = c_current + c_max_step
    return {
        'min': next_min,
        'max': next_max,
    }


def get_policy_complexity(
        policy: Any,
        solution: Dict[str, Any]) -> Dict[str, float]:
    return {
        'min': policy[solution['type']]['complexity']['min'],
        'goal': policy[solution['type']]['complexity']['goal'],
        'min_step': policy[solution['type']]['complexity']['min_step'],
        'max_step': policy[solution['type']]['complexity']['max_step'],
    }


def get_solution_yaml(files: List[str], username: str) -> Any:
    pattern: str = r'''^(code|hack|vbd)/[a-zA-Z0-9-]+/[a-zA-Z0-9-]+/
                    {}\.yml+$'''.format(username)
    patt: str = "".join(line.strip() for line in pattern.splitlines())
    try:
        solution: str = [x for x in files if re.match(patt, x)][0]
    except IndexError as error:
        submission_url: str = \
            'https://gitlab.com/autonomicjump/challenges/-/wikis/Submission'
        log_err('Valid YAML file not found. '
                f'Touched files in the last commit for the user {username} '
                f'are: {files}\n'
                'Seems like your YAML file is non compliant. '
                'Please read '
                f'{submission_url}')
        raise error
    return solution


def get_solution_complexity(solution_yaml: Dict[str, Any]) -> float:
    if 'complexity' in solution_yaml:
        return float(solution_yaml['complexity'])
    return 0


def get_solution_others(solution_path: str) -> List[str]:
    solution_ext: str = solution_path.split(sep='.')[-1]
    username: str = solution_path.split(sep='/')[-1].split(sep='.')[0]
    others_path: str = \
        solution_path.replace(f'/{username}.{solution_ext}', '/OTHERS.lst')
    others_not_empty: bool = os.path.getsize(others_path) > 0 \
        if os.path.exists(others_path) else False
    if others_not_empty:
        with open(others_path, 'r') as externals:
            return externals.readlines()
    return []


def get_solution_unique(s_type: str, s_ext: str, s_others: List[str]) -> bool:
    if s_type == 'code':
        pattern: str = r'^.+\.{}$'.format(s_ext)
        lang_externals: List[str] = \
            [v for v in s_others if re.match(pattern, v)]
        return len(lang_externals) == 0
    return len(s_others) == 0


def get_solution(solution_yaml: Dict[str, Any]) -> Dict[str, Any]:

    solution_path: str = solution_yaml['root-path']
    solution_type: str = solution_yaml['type']
    solution_site: str = solution_path.split(sep='/')[1]
    solution_ext: str = solution_yaml['path'].split(sep='.')[-1]
    solution_others: List[str] = get_solution_others(solution_path)
    solution_complexity: float = get_solution_complexity(solution_yaml)
    solution_unique: bool = \
        get_solution_unique(solution_type, solution_ext, solution_others)

    return {
        'path': solution_path,
        'type': solution_type,
        'site': solution_site,
        'ext': solution_ext,
        'others': solution_others,
        'complexity': solution_complexity,
        'unique': solution_unique,
    }


def get_lang_by_ext(ext: str, data: Any) -> Any:
    lang: Any = [v for (k, v) in data.items() if ext in v['ext']]
    if bool(lang):
        return lang[0]
    return {}


def get_policy_by_user(user: str, data: Any) -> Any:
    policy: Any = [v for (k, v) in data.items() if user in v['users']]
    if bool(policy):
        return policy[0]
    return data['default']


def get_user_unique_solutions(user_solutions: List[str]) -> Dict[str, int]:
    unique_solutions: Dict[str, int] = {
        'code': 0, 'hack': 0, 'vbd': 0
    }
    for solution_path in user_solutions:
        solution_type: str = solution_path.split(sep='/')[0]
        solution_ext: str = solution_path.split(sep='.')[-1]
        others: List[str] = get_solution_others(solution_path)
        solution_unique: bool = \
            get_solution_unique(solution_type, solution_ext, others)
        if solution_unique:
            unique_solutions[solution_type] += 1
    return unique_solutions


def get_deviation(solutions: Dict[str, int], policy: Any) -> int:
    code_active: bool = policy['code']['active']
    hack_active: bool = policy['hack']['active']
    vbd_active: bool = policy['vbd']['active']

    code_to_hack: int = abs(solutions['code'] - solutions['hack']) \
        if code_active and hack_active else 0
    code_to_vbd: int = abs(solutions['code'] - solutions['vbd']) \
        if code_active and vbd_active else 0
    hack_to_vbd: int = abs(solutions['hack'] - solutions['vbd']) \
        if hack_active and vbd_active else 0

    return max(code_to_hack, code_to_vbd, hack_to_vbd)


def get_root_path(user_yaml_path: str, solution_yaml: Dict[str, Any]) -> None:
    root_solution_file: str = \
        f"{'/'.join(user_yaml_path.split('/')[:3])}/{solution_yaml['path']}"
    solution_yaml['root-path'] = root_solution_file


def is_solution_type_valid(
        solution_yaml: Dict[str, Any],
        policies: str, username: str) -> bool:
    with open(policies, 'r') as raw_policies:
        yaml: Any = YAML()
        data_policies: Any = yaml.load(raw_policies)
        user_policy: Any = get_policy_by_user(username, data_policies)
        solution: Dict[str, Any] = get_solution(solution_yaml)
        section_active: bool = \
            user_policy[solution['type']]['active']
        if section_active:
            return True
        log_err(f'{solution["type"]} solutions not allowed in your policy.')
        log_err(f'Your policy is: {user_policy["name"]}')
        log_err(f'Find information about your policy in: {policies}')
    return False


def is_solution_lang_valid(
        solution_yaml: Dict[str, Any],
        langs_supported: str,
        langs_dropped: str) -> bool:
    with open(langs_supported, 'r') as raw_langs_supported:
        yaml: Any = YAML()
        data_langs_supported: Any = yaml.load(raw_langs_supported)
        solution: Dict[str, Any] = get_solution(solution_yaml)
        lang: Any = get_lang_by_ext(solution['ext'], data_langs_supported)
        lang_exists: bool = bool(lang)
        if lang_exists:
            return True
        log_err(f'Valid language for {solution["ext"]} extension not found')
        log_err(f'You can find supported langs in: {langs_supported}')
        log_err(f'You can find dropped langs in: {langs_dropped}')
        log_err(f'Please consider using a supported language')
        log_err('or creating an issue for supporting '
                f'{solution["ext"]} solutions.')
    return False


def is_solution_site_valid(
        solution_yaml: Dict[str, Any],
        policies: str, username: str) -> bool:
    with open(policies, 'r') as raw_policies:
        yaml: Any = YAML()
        data_policies: Any = yaml.load(raw_policies)
        user_policy: Any = get_policy_by_user(username, data_policies)
        solution: Dict[str, Any] = get_solution(solution_yaml)
        if user_policy[solution['type']]['active'] \
                and user_policy[solution['type']]['sites']['active']:
            allowed_sites: List[str] = \
                user_policy[solution['type']]['sites']['list']
            site_valid: bool = solution['site'] in allowed_sites
            if site_valid:
                return True
            log_err(f'Site {solution["site"]} not valid in your policy.')
            log_err(f'Your policy is: {user_policy["name"]}')
            log_err(f'Find more info about your policy in: {policies}')
            return False
    return True


def is_solution_code_lang_valid(
        solution_yaml: Dict[str, Any],
        policies: str,
        langs: str,
        username: str) -> bool:
    with open(policies, 'r') as raw_policies, open(langs, 'r') as raw_langs:
        yaml: Any = YAML()
        data_policies: Any = yaml.load(raw_policies)
        user_policy: Any = get_policy_by_user(username, data_policies)
        solution: Dict[str, Any] = get_solution(solution_yaml)
        if solution['type'] == 'code' \
                and user_policy[solution['type']]['active'] \
                and user_policy[solution['type']]['langs']['active']:
            data_langs: Any = yaml.load(raw_langs)
            allowed_langs: List[str] = \
                user_policy[solution['type']]['langs']['list']
            for allowed_lang in allowed_langs:
                if allowed_lang in data_langs.keys():
                    if solution['ext'] in data_langs[allowed_lang]['ext']:
                        return True
            log_err(f'Language not valid in your policy.')
            log_err(f'Your policy is: {user_policy["name"]}')
            log_err(f'Find more info about your policy in: {policies}')
            log_err('Find more info about supported '
                    f'language extensions in: {langs}')
            return False
    return True


def is_solution_code_complexity_valid(
        solution_yaml: Dict[str, Any],
        policies: str,
        username: str) -> bool:
    with open(policies, 'r') as raw_policies:
        yaml: Any = YAML()
        data_policies: Any = yaml.load(raw_policies)
        user_policy: Any = get_policy_by_user(username, data_policies)
        solution: Dict[str, Any] = get_solution(solution_yaml)
        if solution['type'] == 'code' \
                and user_policy[solution['type']]['active'] \
                and user_policy[solution['type']]['complexity']['active'] \
                and solution['site'] == 'codeabbey':
            c_current: float = solution['complexity']
            c_policy: Dict[str, float] = \
                get_policy_complexity(user_policy, solution)
            c_previous: float = get_previous_codeabbey_complexity(username)
            c_previous = 0 if c_previous >= c_policy['goal'] else c_previous
            # -1 in c_policy['min'] means there's no min restriction
            c_min_expected: float = c_previous + c_policy['min_step'] \
                if c_policy['goal'] >= c_previous > 0 \
                else c_current if c_policy['min'] == -1 else c_policy['min']
            c_max_expected: float = c_previous + c_policy['max_step'] \
                if c_policy['goal'] >= c_previous > 0 \
                else c_current + c_policy['max_step'] \
                if c_policy['min'] == -1 \
                else c_policy['min'] + c_policy['max_step']
            if c_max_expected >= c_current >= c_min_expected or \
                    c_current >= c_policy['goal'] <= c_min_expected or \
                    c_max_expected >= c_policy['goal'] and \
                    c_current >= c_policy['goal']:
                c_expected_next: Dict[str, float] = get_next_complexity(
                    c_current, c_policy['min'],
                    c_policy['goal'], c_policy['min_step'],
                    c_policy['max_step']
                )
                log_info(f'(Next complexity: <value>) > (Min complexity: '
                         f'{c_expected_next["min"]})')
                log_info(f'(Next complexity: <value>) <= (Max complexity: '
                         f'{c_expected_next["max"]})')
                return True
            if c_previous < c_policy['goal']:
                log_info(f'(Current complexity: {c_current}) '
                         f'> (Previous complexity: {c_previous})')
            if c_current < c_min_expected:
                log_err(f'(Current complexity: {c_current}) '
                        f'< (Expected min complexity: {c_min_expected})')
            else:
                log_err(f'(Current complexity: {c_current}) '
                        f'> (Expected max complexity: {c_max_expected})')
            return False
    return True


def is_deviation_valid(
        solution_yaml: Dict[str, Any],
        policies: str, username: str) -> bool:
    with open(policies, 'r') as raw_policies:
        pattern: str = r'''^(code|hack|vbd)/[a-zA-Z0-9-]+/[a-zA-Z0-9-]+/
                        [a-z0-9]+.yml+$'''
        patt: str = "".join(line.strip() for line in pattern.splitlines())
        yaml: Any = YAML()
        data_policies: Any = yaml.load(raw_policies)
        user_policy: Any = get_policy_by_user(username, data_policies)
        last_solution_path: str = solution_yaml['root-path']

        files: List[str] = glob.glob(f'**/**/**/{username}.*')
        new_solutions: List[str] = [x for x in files if not re.match(patt, x)]

        old_solutions: List[str] = \
            [x for x in new_solutions if x != last_solution_path]
        new_unique_solutions: Dict[str, int] = \
            get_user_unique_solutions(new_solutions)
        old_unique_solutions: Dict[str, int] = \
            get_user_unique_solutions(old_solutions)
        new_deviation: int = get_deviation(new_unique_solutions, user_policy)
        old_deviation: int = get_deviation(old_unique_solutions, user_policy)
        exp_deviation: int = user_policy["deviation"]

        if exp_deviation == -1:  # -1 means that deviation isn't evaluated
            return True

        log_info(new_unique_solutions)
        if abs(new_deviation - exp_deviation) \
                > abs(old_deviation - exp_deviation) \
                and old_deviation != exp_deviation:
            log_err(f'Your old deviation was: {old_deviation}')
            log_err(f'Your new deviation is: {new_deviation}')
            log_err(f'Your expected deviation is: {exp_deviation}')
            log_err(f'Your unique solutions are: {new_unique_solutions}')
            log_err(f'Your new deviation should be closer to your '
                    'expected deviation')
            return False
    return True


def is_structure_valid(
        solution_yaml: Dict[str, Any],
        username: str) -> bool:
    solution: Dict[str, Any] = get_solution(solution_yaml)
    folder_title = solution['path'].split('/')[2]
    structure_url: str = \
        'https://gitlab.com/autonomicjump/challenges/-/wikis/structure'
    bad_msg: str = '''Valid solution structure not found.
        Seems like your solution structure is non compliant.
        Please read {}'''.format(structure_url)
    passed: bool = True

    if solution['type'] == 'code':
        pattern_cod: str = r'''^code/[a-zA-Z0-9-]+/[a-z0-9-]+/
                        {}\.[a-zA-Z0-9]+$'''.format(username)
        pattc: str = ''.join(line.strip() for line in pattern_cod.splitlines())
        if not bool(re.match(pattc, solution['path'])):
            log_err(bad_msg)
            passed = False

    if solution['type'] == 'hack':
        pattern_hack: str = r'''^hack/[a-zA-Z0-9-]+/[a-z0-9-]+/
                        {}\.feature$'''.format(username)
        patt: str = ''.join(line.strip() for line in pattern_hack.splitlines())
        if not bool(re.match(patt, solution['path'])):
            log_err(bad_msg)
            passed = False

    if solution['type'] == 'vbd':
        read_path = open(solution['path'], 'r').read()
        find_cwe = re.search("CWE-(.*?): ", read_path)
        if find_cwe:
            cwe = find_cwe.group(1).rjust(4, '0')
            if cwe != folder_title[:4]:
                log_err(
                    bad_msg,
                    f'CWE from commit {cwe} != '
                    f'CWE from file {folder_title[:4]}'
                )
                passed = False
            else:
                pattern_vbd: str = \
                    r'''^vbd/[a-zA-Z0-9-]+/{}-[a-z0-9-]+/{}
                    \.feature$'''.format(cwe, username)
                pattv: str = \
                    ''.join(line.strip() for line in pattern_vbd.splitlines())
                if not bool(re.match(pattv, solution['path'])):
                    log_err(bad_msg)
                    passed = False
        else:
            log_err(bad_msg, 'File has not valid CWE')
            passed = False
    return passed


def main() -> None:
    branch_name: str = os.environ['CI_COMMIT_REF_NAME']
    touched_files: List[str] = \
        list(map(lambda path: path.rstrip(), sys.stdin.readlines()))
    user_yaml_path: str = get_solution_yaml(touched_files, branch_name)

    with open(user_yaml_path, 'r') as solution_path:
        yaml: Any = YAML(typ='safe')
        solution_yaml: Dict[str, Any] = yaml.load(solution_path)

    get_root_path(user_yaml_path, solution_yaml)

    policies: str = 'policies/data.yaml'
    supported_langs: str = 'code/lang-data-supported.yml'
    dropped_langs: str = 'code/lang-data-dropped.yml'

    if not is_solution_type_valid(solution_yaml, policies, branch_name):
        sys.exit(1)
    if not is_solution_lang_valid(solution_yaml, supported_langs,
                                  dropped_langs):
        sys.exit(1)
    if not is_solution_site_valid(solution_yaml, policies, branch_name):
        sys.exit(1)
    if not is_solution_code_lang_valid(solution_yaml, policies,
                                       supported_langs, branch_name):
        sys.exit(1)
    if not is_solution_code_complexity_valid(solution_yaml, policies,
                                             branch_name):
        sys.exit(1)
    if not is_deviation_valid(solution_yaml, policies, branch_name):
        sys.exit(1)
    if not is_structure_valid(solution_yaml, branch_name):
        sys.exit(1)


if __name__ == '__main__':
    main()
