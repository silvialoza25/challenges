#!/usr/bin/env python3

# pylint: disable= superfluous-parens, pointless-string-statement
# pylint: disable= unreachable, not-callable, bad-continuation

"""
------------------------------------------------------------------------------
                       Pyparsing Commit Message Linter
------------------------------------------------------------------------------
This script checks ONLY the last commit.
In particular, it does not check for multi-commit Merge Requests,
nor for branches that have not been rebased,
which are also a rejection reason.
A valid commit message must conform to one of
https://docs.autonomicjump.com/templates/commit/hack-code
and in that case we call it a "solution-type" commit,
or start with one of the keywords
feat: fix: style: refac: test:
for internal work on the repository.

Local usage:
add your files:   $ git add xyz
commit them:      $ git commit
It helps if you have set your commit message template
to one of the provided templates.
After comitting, run this script:
$./parse_commit_msg.py
or
$python parse_commit_msg.py
If everything is OK you will get
"Solution commit message OK, continue checks..."
or
"Other commit message OK, continue checks...")
If not, you will see an error message pointing why the commit message is wrong.
In that case you can use
$git commit --amend
to fix the last commit message and re-run the parser.

It is easier to understand the grammar if read from top to bottom, starting
with the top-level parsers and working your way back to the atomic parsers.
"""

import sys
import subprocess
from typing import Any
from pyparsing import (Word, nums, LineStart, LineEnd, Combine,
                       CharsNotIn, srange, Suppress, ParseException, oneOf)

# Parse and fail actions for atomic (lowest-level) parsers


def warnings_sitename(
        bad_string: str,
        loc: str,
        _expr: str,
        _err: str) -> None:
    """ Warnings for sitename parsers """
    raise ParseException(bad_string, loc,
                         "Sitename must follow repository conventions:\n"
                         + "https://gitlab.com/fluidattacks/writeups"
                         + "/-/wikis/Structure\n")


def warnings_integer(bad_string: str, loc: str, _expr: str, _err: str) -> None:
    """ Warnings for integer parsers """
    raise ParseException(bad_string, loc,
                         "Expected a non-negative integer here. \
                         Only digits.\n")


def warnings_float(bad_string: str, loc: str, _expr: str, _err: str) -> None:
    """ Warnings for float parsers """
    raise ParseException(bad_string, loc,
                         "Expected an non-negative integer or float here.\n")


def warnings_any(bad_string: str, loc: str, _expr: str, _err: str) -> None:
    """ Warnings for anything-except-parens parsers """
    raise ParseException(bad_string, loc,
                         "Anything except parens works here.\n")
    """
    Future: add more specific warnings when faced with a cryptic message.
    sys.exit(1)
    """


def warnings_separator(
        bad_string: str,
        loc: str, _expr:
        str, _err: str) -> None:
    """ Missing blank line warning """
    raise ParseException(bad_string, loc, "Please leave a blank line "
                         + "to separate commit title from body.\n"
                         + "See git-commit(1).\n")


def warnings_issue(bad_string: str, loc: str, _expr: str, _err: str) -> None:
    """ Missing #0 issue """
    raise ParseException(bad_string, loc, "Please add a #0 "
                         + "as explained in"
                         + " https://docs.autonomicjump.com/templates/"
                         + "commit/hack-code\n")


# Atomic parsers


INTEGER = Word(nums).setName("entier").setFailAction(warnings_integer)
INTEGER.setParseAction(lambda toks: int(toks[0]))
FLOAT = Combine(Word(nums) + '.' + Word(nums))
FLOAT.setParseAction(lambda toks: float(toks[0]))
NUMBER = (FLOAT | INTEGER).setName("float").setFailAction(warnings_float)
SPACE = Word(' ', exact=1).setFailAction(warnings_sitename)
SOLUTIONS = oneOf(["sol(code):", "sol(hack):", "sol(vbd):"])
SITENAME = Word(srange("[a-z]") + nums + "-"
                ).setName("sitename").setFailAction(warnings_sitename)
ANY_CODE = CharsNotIn("()\n").setName("challcode").setFailAction(warnings_any)
COMPLEXITY = (NUMBER | ANY_CODE)
# warnings_sitename appears in separators to make message easier to understand
TITLE_CHALL = (LineStart() + Suppress(SOLUTIONS("scope"))
               + SPACE(" ")
               + Suppress("#0").setFailAction(warnings_issue)
               + SPACE(" ")
               + SITENAME("sitename")
               + Suppress(",").setFailAction(warnings_sitename)
               + SPACE(" ")
               + SITENAME("code")
               + SPACE(" ")
               + Suppress("(").setFailAction(warnings_sitename)
               + COMPLEXITY("bless") + Suppress(")")
               + Suppress(LineEnd())).leaveWhitespace()
TITLE_SYST = (LineStart() + Suppress("sol(vbd):")
              + SPACE(" ")
              + Suppress("#0").setFailAction(warnings_issue)
              + SPACE(" ")
              + SITENAME("sitename")
              + Suppress(",").setFailAction(warnings_sitename)
              + SPACE(" ")
              + SITENAME("code") + Suppress(LineEnd())).leaveWhitespace()


SOLUTION_COMMIT = (TITLE_CHALL("title")
                   ).setName("challsol")

SYSTEMS_COMMIT = (TITLE_SYST("title")
                  ).setName("syssol")

CLASSIFIER = SOLUTIONS.setResultsName("type")

# pp.Or not used because it generates ugly exceptions
# Instead we chose to use the CLASSIFIER parser above to
# determine which of the top-level parsers to use
# and generate the appropriate warnings.


def handle_except_exit(exception: Any, mess: str) -> None:
    """ Final error message. Prints line and a pointer to
    the guilty column. """
    print(exception.line)
    print(' ' * (exception.col - 1) + '^')
    print(exception)
    print(mess)
    sys.exit(1)


def check_body(message: str, body: str) -> None:
    try:
        commit = CLASSIFIER.parseString(message)
        if commit["type"] in {"sol(code):", "sol(hack):", "sol(vbd):"}:
            if body != "":
                print("[ERROR] Commit body must be empty")
                sys.exit(1)
            print("[INFO] Commit body OK, continue checks...")
    except ParseException:
        if body == "":
            print("[ERROR] Commit body can't be empty")
            sys.exit(1)
        print("Challenge commit body OK, continue checks...")


def parse_commit(message: str) -> None:
    """ Main function to parse a commit message """
    try:
        commit = CLASSIFIER.parseString(message)
        if commit["type"] == "sol(code):" or commit["type"] == "sol(hack):":
            try:
                SOLUTION_COMMIT.parseString(message)
                print("Challenge commit message OK, continue checks...")
                sys.exit(0)
            except ParseException as exc_bad_challenge:
                emsg = ("\nIf this is a commit for a CHALLENGE solution,"
                        + "\nfollow"
                        + " https://docs.autonomicjump.com/templates/"
                        + "commit/hack-code:")
                handle_except_exit(exc_bad_challenge, emsg)
        elif commit["type"] == "sol(vbd):":
            try:
                SYSTEMS_COMMIT.parseString(message)
                print("Systems commit message OK, continue checks...")
                sys.exit(0)
            except ParseException as exc_bad_challenge:
                emsg = ("\nIf this is a commit for a VBD solution,"
                        + "\nfollow"
                        + " https://docs.autonomicjump.com/templates/"
                        + "commit/hack-code :")
                handle_except_exit(exc_bad_challenge, emsg)
        else:
            print("Other commit message OK, continue checks...")
            sys.exit(0)
    except ParseException:
        print("Challenge commit message OK, continue checks...")
        sys.exit(0)


if len(sys.argv) > 1:  # for bats testing
    MESSAGE = sys.argv[1]
    parse_commit(MESSAGE)

else:
    try:
        CMD_OUT = subprocess.check_output('git log -1 --pretty=%B', shell=True)
        MESSAGE = CMD_OUT.decode('utf-8')
        COMMIT_BODY = subprocess.check_output(
            "git log -1 --pretty=format:'%b'", shell=True).decode('ascii')
        check_body(MESSAGE, COMMIT_BODY)
        parse_commit(MESSAGE)
    except subprocess.CalledProcessError as cmd_err:
        print("Something went wrong while retrieving the last commit:")
        print(cmd_err)
        sys.exit(1)
