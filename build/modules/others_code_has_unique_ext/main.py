#!/usr/bin/env python3

"""
This script checks unique extension urls in OTHERS.lst files
"""

import os
import sys
import re
from typing import Iterable


def ext(url: str) -> str:
    """Take the url an return the possible extension"""
    return url.split('.')[-1]


def has_extension(url: str) -> bool:
    """
    Take url eval if you have a recognizable extension with 2 criteria,
    the first if the extension is less than 7 characters long and
    the second if the extension does not have any special character
    """
    has_ext_len = len(ext(url)) <= 7
    especial_chars = bool(re.search("[^A-z0-9]", ext(url)))
    condition = has_ext_len and not especial_chars
    return condition


def has_unique_ext(file_path: str) -> bool:
    """
    Extract urls from a OTHERS.lst file, filter them, and
    check if has unique extensions.
    """
    with open(file_path) as file_handle:
        urls = file_handle.read().splitlines()
        extensions = [ext(url) for url in urls if has_extension(url)]
        extensions.sort()
        unique_extensions = sorted(set(extensions))
        return not unique_extensions == extensions


def yield_others(starting_folder: str) -> Iterable[str]:
    """Recursively yield paths to OTHERS.lst files starting from folder."""
    for dirpath, _, filenames in os.walk(starting_folder):
        for filename in filter('OTHERS.lst'.__eq__, filenames):
            file_path = os.path.join(dirpath, filename)
            yield file_path


def check_for_sorted_urls(starting_folder: str) -> bool:
    """Parse every OTHER.lst file and check if it has unique extension urls."""
    for others_file_path in yield_others(starting_folder):
        if has_unique_ext(others_file_path):
            print('[ERROR]', end=' ')
            print('You must not add external solutions for a', end=' ')
            print('language that already has an external solution.')
            print('  ', others_file_path)
            return True
    return False


def main():
    """Usual entrypoint."""
    folder = 'code'
    if check_for_sorted_urls(folder):
        sys.exit(1)
    sys.exit(0)


if __name__ == '__main__':
    main()
