#!/usr/bin/env python3

import os
import sys
import re
from typing import Any, List, Dict, Set
from datetime import datetime
from ruamel.yaml import YAML


def log_err(*args: Any, **kwargs: Any) -> None:
    """Logger for error category."""
    print('[ERROR]', *args, **kwargs)


def log_info(*args: Any, **kwargs: Any) -> None:
    """Logger for info category."""
    print('[INFO]', *args, **kwargs)


def is_positive(tokens: Set[Any]) -> None:
    eval_is_positive: Set[Any] = set(map(lambda values: values < 0, tokens))
    if True in eval_is_positive:
        log_err("Parameters must always be positive")
        sys.exit(1)


def generals_checks(tokens: Dict[str, Any], username: str) -> bool:
    sol_type = tokens['root-path'].split('/')[0]
    if tokens['user-name'] != username or sol_type != tokens['type']:
        log_err("Username and type must be equal in all the YAML file")
        return False
    return True


def is_stage_valid(tokens: str) -> bool:
    if tokens in {'immersion', 'training', 'challenges'}:
        return True
    log_err(f"'{tokens}' is not an allowed stage, the allowed stages are "
            + "immersion, training and challenges")
    return False


def is_type_valid(tokens: str) -> bool:
    if tokens in {'code', 'hack', 'vbd'}:
        return True
    log_err(f"'{tokens}' is not an allowed type, the allowed types are "
            + "hack, vbd and code")
    return False


def is_date_valid(tokens: str) -> bool:
    current_utc = datetime.utcnow()
    mr_date = datetime.strptime(tokens, '%Y-%m-%d %H:%M:%SZ')
    if current_utc < mr_date:
        log_err("Seems like the date in your YAML file is non compliant.")
        return False
    return True


def check_ranks(tokens: Dict[str, Any]) -> bool:
    ini = tokens['initial']
    fin = tokens['final']
    pro = tokens['progress']

    is_positive({ini, fin, pro})

    if fin > ini:
        log_err("Final rank must be less than or equal to initial rank.")
        return False

    if ini - fin != pro:
        log_err("Rank progress not correctly computed")
        return False
    return True


def check_scores(tokens: Dict[str, Any]) -> bool:
    ini = tokens['initial']
    fin = tokens['final']
    pro = tokens['progress']

    is_positive({ini, fin, pro})

    if ini > fin:
        log_err("Final score must be greater than or equal to initial score.")
        return False

    if abs(fin - ini - pro) > 0.1:
        log_err("Score progress not correctly computed")
        return False
    return True


def check_others(tokens: Any) -> bool:
    ini = tokens['in']
    fin = tokens['out']
    pro = tokens['totals']

    is_positive({ini, fin, pro})

    if ini + fin != pro:
        log_err("Total others not correctly computed")
        return False
    return True


def challenge_checks(tokens: Any) -> bool:
    sco_progress = tokens['score']['progress']
    sco_title = tokens['complexity']
    sco_site = tokens['root-path'].split('/')[1]
    if sco_progress != sco_title:
        if sco_site == 'codeabbey':
            is_positive({sco_progress, sco_title})

            if abs(sco_progress - sco_title) > 0.1:
                log_err(("The progress in score ({0}) must be "
                         + "the same as the blessing ({1})."
                         ).format(sco_progress, sco_title))
                return False
        else:
            log_info(("The progress in score ({0}) is usually "
                      + "the same as the challenge complexity ({1})."
                      ).format(sco_progress, sco_title))

    computed_productivity = float(tokens['productivity'])
    hours = tokens['effort']
    actual_productivity = float(sco_progress) / float(hours)
    is_positive({hours, actual_productivity, computed_productivity})
    if abs(actual_productivity - computed_productivity) > 0.1:
        log_err("Productivity incorrect")
        return False
    return True


def vbd_checks(tokens: Any) -> bool:
    computed_percentage = \
        float(tokens['discovery-percentage'].replace('%', ''))
    computed_total_discovered = \
        float(tokens['dicovered-vulnerabilities']['total'])
    estimated_total_vulns = float(tokens['estimated-vulnerabilities'])
    actual_percentage = computed_total_discovered * 100 / estimated_total_vulns
    is_positive({computed_percentage, computed_total_discovered,
                 estimated_total_vulns, actual_percentage})

    if abs(actual_percentage - computed_percentage) > 1:
        log_err("Discovery percentage not correctly computed")
        return False
    return True


def get_solution_yaml(files: List[str], username: str) -> Any:
    pattern: str = r'''^(code|hack|vbd)/[a-zA-Z0-9-]+/[a-zA-Z0-9-]+/
                    {}\.yml+$'''.format(username)
    patt: str = "".join(line.strip() for line in pattern.splitlines())
    try:
        solution: str = [x for x in files if re.match(patt, x)][0]
    except IndexError as error:
        submission_url: str = \
            'https://gitlab.com/autonomicjump/challenges/-/wikis/Submission'
        log_err('Valid YAML file not found. '
                f'Touched files in the last commit for the user {username} '
                f'are: {files}\n'
                'Seems like your YAML file is non compliant. '
                'Please read '
                f'{submission_url}')
        raise error
    return solution


def get_root_path(user_yaml_path: str, solution_yaml: Dict[str, Any]) -> None:
    root_solution_file: str = \
        f"{'/'.join(user_yaml_path.split('/')[:3])}/{solution_yaml['path']}"
    solution_yaml['root-path'] = root_solution_file


def main() -> None:
    branch_name: str = os.environ['CI_COMMIT_REF_NAME']
    touched_files: List[str] = \
        list(map(lambda path: path.rstrip(), sys.stdin.readlines()))
    user_yaml_path: str = get_solution_yaml(touched_files, branch_name)

    with open(user_yaml_path, 'r') as solution_path:
        yaml: Any = YAML(typ='safe')
        solution_yaml: Dict[str, Any] = yaml.load(solution_path)

    get_root_path(user_yaml_path, solution_yaml)

    if not generals_checks(solution_yaml, branch_name):
        sys.exit(1)
    if not is_stage_valid(solution_yaml['stage']):
        sys.exit(1)
    if not is_type_valid(solution_yaml['type']):
        sys.exit(1)
    if not is_date_valid(solution_yaml['date']):
        sys.exit(1)

    if solution_yaml['type'] != 'vbd':
        if not check_ranks(solution_yaml['national-rank']):
            sys.exit(1)
        if not check_ranks(solution_yaml['global-rank']):
            sys.exit(1)
        if not check_scores(solution_yaml['score']):
            sys.exit(1)
        if not check_others(solution_yaml['others']):
            sys.exit(1)
        if not challenge_checks(solution_yaml):
            sys.exit(1)
    else:
        if not vbd_checks(solution_yaml):
            sys.exit(1)


if __name__ == '__main__':
    main()
