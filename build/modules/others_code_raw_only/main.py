#!/usr/bin/env python3

"""
This script checks raw urls in OTHERS.lst files
"""

import os
import sys
from typing import List, Iterable


def delete_no_raw(urls) -> List[str]:
    """Take the URLs and delete the ones without raw"""
    raw_urls = [url for url in urls if 'raw' in url.lower()]
    return raw_urls


def normalize_others_file(file_path):
    """Extract urls from a OTHERS.lst file, filter them, and return them."""
    with open(file_path) as file_handle:
        urls = file_handle.read().splitlines()
        return tuple(delete_no_raw(urls)), tuple(urls)


def yield_others(starting_folder) -> Iterable[str]:
    """Recursively yield paths to OTHERS.lst files starting from folder."""
    for dirpath, _, filenames in os.walk(starting_folder):
        for filename in filter('OTHERS.lst'.__eq__, filenames):
            file_path = os.path.join(dirpath, filename)
            yield file_path


def check_for_raw_urls(starting_folder) -> bool:
    """Parse every OTHER.lst file and check if it has raw urls."""
    for others_file_path in yield_others(starting_folder):
        normalized_urls, entry_urls = normalize_others_file(others_file_path)
        if entry_urls != normalized_urls:
            print('[ERROR] URLs must be RAW version.')
            print('  ', others_file_path)
            return True
    return False


def main():
    """Usual entrypoint."""
    folder = 'code'
    if check_for_raw_urls(folder):
        sys.exit(1)
    sys.exit(0)


if __name__ == '__main__':
    main()
