---
id: languages
title: Languages
sidebar_label: Languages
slug: /languages
---

All languages packages
in our CI are supported by Nix,
Nix will allow you
to make an easy installation
of each language you going to use

You can find
the installation instruction
for Nix here:

- [Nix intallation](https://nixos.org/download.html)

Once you have installed Nix,
you can search
for the package you want to install on:

- [NixOS Search](https://search.nixos.org/packages)

You going to get
the following view on the package
that you select
then you have to use the command
that the page gives you
to execute the installation:

![image](/img/image.png)
