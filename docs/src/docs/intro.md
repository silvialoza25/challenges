---
id: home
title: Home
sidebar_label: Home
slug: /
---

Welcome to Challenges wiki,
here you can find
documentation on the repo structure,
local builds and others.

## Pages

Currently supported pages:

* [Challenges](challenges)
* [Submission](submission)
* [Immersion](immersion)
* [Templates](templates/commit/hack-code)
* [Builds](builds)
* [Languages](languages)
* [Help](help)
* [Policies](policy)

## Something missing?

If you can't find
documentation on a topic
you consider relevant,
make sure to open
an [issue](https://gitlab.com/autonomicjump/challenges/-/issues).
