---
id: builds
title: Builds
sidebar_label: Builds
slug: /builds
---

It is possible to run
local integrations
in order to identify
any errors before **pushing** or sending
a **merge request** to the repositories.
Currently we have two build systems
integrated on the repository
both of them based on Nix,
so the first steps will be
install NixOS in your system

## For GNU/Linux Operating Systems

1. Install Nix
  as explained here https://nixos.org/nix/download.html.

  On most systems,
  it is enough to do:

  ```md
  curl https://nixos.org/nix/install | sh
  ```

1. Install git, and bash,
  although on most systems
  this comes pre-installed.

Once NixOS intallation have finished
you need to install Makes framework

1. Install Makes

  ```md
  nix-env -if https://fluidattacks.com/makes/install/<version>
  ```

  You can check our current version
  [here](https://gitlab.com/autonomicjump/challenges/-/blob/master/.gitlab-ci.yml#L41)

  Check Makes [documentation](https://github.com/fluidattacks/makes/#getting-started)
  for more info.

Once NixOS and Makes are installed in our system,
we can proceed to make local builds

1. Meet the builder:

  In root folder of the challenges repo run

  ```md
  $ ./build.sh
  ```

  or

  ```md
  $ m .
  ```

  This will display
  a help message by default
  and the list of checks available.

  :::note
  Please make sure to commit your changes
  before running any local build,
  as staging changes will not be taken into account.
  :::

1. If the integration was successful,
  commit your changes
  and create a merge request.

## For Operating Systems different from Linux (Windows, Mac, etc)

In Operating Systems
different from Linux (Windows, Mac, etc):
Continuous integration will never be available
for OS different from Linux.
Additionally in your everyday life at Fluid Attacks
you will need to familiarize
with Linux and its weapons.
Hence we strongly recommend you
to install it on your computer,
or at least, to work with a Linux virtual machine.

We recommend installing
virtualization software (VMware, Virtualbox)
and creating a virtual machine
based on a Linux distribution
(e.g. Ubuntu, or another one of your liking).
Then, follow the same procedure
described above for Linux.
