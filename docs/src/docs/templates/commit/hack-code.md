---
id: hack-code
title: Hack and Code
sidebar_label: Hack and Code
slug: /templates/commit/hack-code
---

The following is the **commit template**
for challenges/code and challenges/hack

Fill the fields in angle brackets <\>
as indicated in these comments.
These comments will not appear
in the final commit message.

- **<scope\>** can either be code
  for programming or hack for ctf-hacking
- **<site-name\>** and **<chall-code\>**
  must conform to repository naming conventions
- **<complexity\>** is the blessing in codeabbey
 (can be a decimal), v.g. 9.33
  or something else (but no parens)
  for hacking sites, v.g. easy or 3 or 147

  Typically it should be equal to the progress
  in score as computed by wechall.
    - **programming example** `sol(code): #0 codeabbey, 12 (9.8)`
    - **hacking example**     `sol(hack): #0 2017game-picoctf, challenge-5 (20)`

Template:

```md
sol(<scope>): #0 <site-name>, <chall-code> (<complexity>)
```

Full hacking example:

```md
sol(hack): #0 world-of-wargame, & d1fficult-0ne (54)
```

Full programming example:

```md
sol(code): #0 codeabbey, 50 (5.22)
```

Remember to make ONE commit
for each challenge solution.
This commit must also include
the appropriate number of OTHERS solutions,
which is currently 10.
Then make ONE Merge Request
with that ONE commit for ONE challenge,
but ONLY after the pipeline has succeeded.
See https://docs.autonomicjump.com/submission.
