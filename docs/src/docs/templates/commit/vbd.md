---
id: vbd
title: VbD
sidebar_label: VbD
slug: /templates/commit/vbd
---

The following is the **commit template**
for VbD hacking solutions

Fill the fields in angle brackets <\>
as indicated in these comments.
These comments will not appear
in the final commit message.

- **<system-name\>** is the name of the system
- **<CWE\>** is the four digit CWE code of the vulnerability
- **<page-name\>** is the place where the vulnerability can be found,

  **example:** `sol(vbd): #0 juice-shop, 0640-backup-file`

Template:

```md
sol(vbd): #0 <system-name>, <CWE>-<page-name>
```

Full example:

```md
sol(vbd): #0 juice-shop, 0711-fdback-anthr-usrnm
```

Remember to make ONE commit
for each challenge solution.
This commit must also include
the appropriate number of OTHERS solutions,
which is currently 10.
Then make ONE Merge Request
with that ONE commit for ONE challenge,
but ONLY after the pipeline has succeeded.
See https://docs.autonomicjump.com/submission.
