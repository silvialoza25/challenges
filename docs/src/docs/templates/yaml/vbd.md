---
id: vbd
title: VbD
sidebar_label: VbD
slug: /templates/yaml/vbd
---

Template **{user-name}.yml** file
for ***challenge/vbd***
all comments must be deleted
in your **{user-name}.yml** file and
the name for this file will be **{user-name}.yml**

Fill the fields in angle brackets <\>
as indicated in the instruction after
the following box

```md
type: <scope>
user-name: <user-name>
stage: <phase>
dicovered-vulnerabilities:
  by-me: <X>
  in-repo: <Y>
  total: <X+Y>
estimated-vulnerabilities: <N>
discovery-percentage: <D%>
effort: <X>
date: <commit-date>
path: <solution-path>
```

- **<scope\>** can either
  be code for programming or hack for ctf-hacking.
- **<user-name\>** is your branch name
- **<phase\>** You are
  in one of the following phases,
  select one of those
  depending on your case :
  challenges \ immersion \ training
- **<solution-path\>** will be <solution-file\>**
  must conform
  to repository naming conventions.
- **<commit-date\>** is the date
  when the commit was created,
  you can get it using
  "git log" command,
  however you going to need
  add the UTC date
  (add 5 hours
  if you are in Bogota, Colombia)
  its format will be:

  'Year-Month-Day Hours:Min:SecondsZ'
  For example: '2021-04-16 09:41:52Z'

- X is the ACCUMULATED number
  of vulnerabilites in this system
  discovered by you

- Y is the ACCUMULATED number
  of vulnerabilites
  already reported in the repo,
  not including those previously reported by you.
  All of these can be zero
  if you find a vulnerability in a brand new ToE.

- N is the estimated number
  of vulnerabilities in the system.
  This number could be in the official documentation
  of the system or a site like VulnHub.

- D is (X+Y)*100/N,
  with at most two decimals.

- **Effort** is the number of hours
  dedicated to solving the challenge,
  and sending the MR to the challenges repository
  not necessarily in one sitting.

  For example,
  if you dedicated two hours today
  and later 20 minutes,
  and three more hours the day after,
  that's 5 hours and 20 minutes = 5.3 hours.
  Effort report
  must cover also the time required
  for searching existing solutions (OTHERS),
  as well as linting and compiling time
  before sending the MR.
  If the MR gets rejected,
  additional time used on
  fixing it has to be added
  Productivity is the progress
  in points divided by the effort: P=(Y-X)/H

Next time you find a vulnerability
in the same system,
if other people have reported 3 vulnerabilities
in that time,
you will have 16 by me,
5 already in repo, 21 total.

All of these values
must be filled in.
There cannot be any fields
that do not apply,
since all challenges
give some score and ranks.
See [Score](https://docs.autonomicjump.com/submission/score)

**Full example:**

```md
type: vbd
user-name: ludsrill
stage: immersion
dicovered-vulnerabilities:
  by-me: 1
  in-repo: 0
  total: 1
estimated-vulnerabilities: 1
discovery-percentage: 100%
effort: 3.3
date: '2021-04-08 14:57:03Z'
path: ludsrill.feature
```

The following are user.yml files
already merged in the repositorie

- [Example 1](https://gitlab.com/autonomicjump/challenges/-/blob/master/vbd/acuart/0020-impr-data-val-cart/rafa1894.yml)
- [Example 2](https://gitlab.com/autonomicjump/challenges/-/blob/master/vbd/acuart/0079-stored-xss/lekriss.yml)
