---
id: hack-code
title: Hack and code
sidebar_label: Hack and code
slug: /templates/yaml/hack-code
---

Template **{user-name}.yml** file
for ***challenge/code*** and ***challenges/hack***,
all comments must be deleted
in your **{user-name}.yml** file and
the name for this file will be **{user-name}.yml**

Fill the fields in angle brackets <\>
as indicated in the instruction after
the following box

```md
type: <scope>
user-name: <user-name>
stage: <phase>
complexity: <complexity>
others:
  in: <M>
  out: <N>
  totals: <M+N>
score:
  initial: <X>
  final: <Y>
  progress: <Y-X>
global-rank:
  initial: <M>
  final: <N>
  progress: <M-N>
national-rank:
  initial: <M>
  final: <N>
  progress: <M-N>
effort: <H>
productivity: <P>
date: <commit-date>
path: <solution-path>
```

- **<scope\>** can either
  be code for programming or hack for ctf-hacking.
- **<user-name\>** is your branch name
- **<phase\>** You are
  in one of the following phases,
  select one of those
  depending on your case :
  challenges \ immersion \ training
- **<complexity\>** is the blessing
  in codeabbey (can be a decimal),
  v.g. 9.33 or something else
  (but no parens) for hacking sites,
  v.g. easy or 3 or 147
  Typically it should be equal
  to the progress in score as computed by
  wechall.
- **<solution-path\>** will be <solution-file\>**
  must conform
  to repository naming conventions.
- **<commit-date\>** is the date
  when the commit was created,
  you can get it using
  "git log" command,
  however you going to need
  add the UTC date
  (add 5 hours
  if you are in Bogota, Colombia)
  its format will be:

  'Year-Month-Day Hours:Min:SecondsZ'
  For example: '2021-04-16 09:41:52Z'

- **M** and **N** stand for integers,
  **X, Y, H** and **P** can have decimals.

- **M** are the number
  of internal challenges solved
  by other talents
  for the specific challenge
  you're solving.

- **N** are the number
  of external challenges references
  in the OTHERS.lst file
  for the specific challenge
  you're solving

- **Effort** is the number of hours
  dedicated to solving the challenge,
  and sending the MR to the challenges repository
  not necessarily in one sitting.

  For example,
  if you dedicated two hours today
  and later 20 minutes,
  and three more hours the day after,
  that's 5 hours and 20 minutes = 5.3 hours.
  Effort report
  must cover also the time required
  for searching existing solutions (OTHERS),
  as well as linting and compiling time
  before sending the MR.
  If the MR gets rejected,
  additional time used on
  fixing it has to be added
  Productivity is the progress
  in points divided by the effort: P=(Y-X)/H

All of these values
must be filled in.
There cannot be any fields
that do not apply,
since all challenges
give some score and ranks.
See [Score](https://docs.autonomicjump.com/submission/score)

**Full example:**

```md
type: code
user-name: ludsrill
stage: immersion
complexity: 8.95
others:
  in: 4
  out: 14
  totals: 18
score:
  initial: 111.26
  final: 120.21
  progress: 8.95
global-rank:
  initial: 4285
  final: 3998
  progress: 287
national-rank:
  intial: 221
  final: 206
  progress: 15
effort: 11.0
productivity: 0.814
date: '2021-04-16 09:41:52Z'
path: ludsrill.elv
```

The following are user.yml files already merged in the repositorie

- [Example 1](https://gitlab.com/autonomicjump/challenges/-/blob/master/code/codeabbey/025/jmesa85.yml)
- [Example 2](https://gitlab.com/autonomicjump/challenges/-/blob/master/code/codeabbey/025/ludsrill.yml)
