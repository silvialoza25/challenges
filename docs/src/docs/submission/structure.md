---
id: structure
title: Structure
sidebar_label: Structure
slug: /submission/structure
---

In this page
you will go through the structure
and most important features
this repository.
It is ideal that you become familiar
with its structure
before you start trying
to post solutions,
as it is big and you might feel lost.

Solutions are stored
in the following folders
depending on the repository
you are currently in:

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs
  defaultValue="code"
  values={[
    {label: 'Code', value: 'code'},
    {label: 'Hack', value: 'hack'},
    {label: 'VbD', value: 'vbd'},
  ]}>
<TabItem value="code">

**Description**

Folder to store ***programming*** challenges.

**Estructure**

```md
<site> (directory)
└── <challenge-id> (directory)
    ├── <login-gitlab.ext> (solution file)
    └── <login-gitlab.yml> (YAML file)
```

**Example**

* [Codeabbey](https://gitlab.com/autonomicjump/challenges/tree/master/code/codeabbey/)
  * [135](https://gitlab.com/autonomicjump/challenges/tree/master/code/codeabbey/135/)
    * [ludsrill.exs](https://gitlab.com/autonomicjump/challenges/-/blob/master/code/codeabbey/135/ludsrill.exs)
    * [ludsrill.yml](https://gitlab.com/autonomicjump/challenges/-/blob/master/code/codeabbey/135/ludsrill.yml)

</TabItem>

<TabItem value="hack">

**Description**

Folder to store ***ctf-hacking*** challenges.

**Estructure**

```md
<site> (directory)
└── <challenge-id> (directory)
    ├── <login-gitlab.feature> (solution file)
    ├── <login-gitlab.yml> (YAML file)
    └── <login-gitlab> (evidence folder) (optionally)
        ├── <evidence-img.png> (evidence img in png) (optionally)
        └── <login-gitlab.ext> (script/exploit used) (optionally)
```

**Example**

* [247ctf](https://gitlab.com/autonomicjump/challenges/-/tree/master/hack/247ctf)
  * [hidden-painting](https://gitlab.com/autonomicjump/challenges/-/tree/master/hack/247ctf/hidden-painting)
    * [sullenestd1.feature](https://gitlab.com/autonomicjump/challenges/-/blob/master/hack/247ctf/hidden-painting/sullenestd1.feature)
    * [sullenestd1.yml](https://gitlab.com/autonomicjump/challenges/-/blob/master/hack/247ctf/hidden-painting/sullenestd1.yml)
    * [esullenestd1](https://gitlab.com/autonomicjump/challenges/-/blob/master/hack/247ctf/hidden-painting/sullenestd1)
      * [esullenestd1.py](https://gitlab.com/autonomicjump/challenges/-/blob/master/hack/247ctf/hidden-painting/sullenestd1.py)

</TabItem>

<TabItem value="vbd">

**Description**

Folder to store ***vulnerable vbd-hacking*** challenges.

**Estructure**

```md
<name-of-the-vulnerable-machine> (directory)
└── <cwe-code>-<exploit-name> (directory)
    ├── <login-gitlab.feature> (solution file)
    ├── <login-gitlab.yml> (YAML file)
    └── <login-gitlab> (evidence folder) (optionally)
        ├── <evidence-img.png> (evidence img in png) (optionally)
        └── <login-gitlab.ext> (script/exploit used) (optionally)
```

**Example**

* [acuart](https://gitlab.com/autonomicjump/challenges/tree/master/vbd/dvwa)
  * [0020-impr-data-val-cart](https://gitlab.com/autonomicjump/challenges/-/tree/master/vbd/acuart/0020-impr-data-val-cart)
    * [rafa1894.feature](https://gitlab.com/autonomicjump/challenges/-/blob/master/vbd/acuart/0020-impr-data-val-cart/rafa1894.feature)
    * [rafa1894.yml](https://gitlab.com/autonomicjump/challenges/-/blob/master/vbd/acuart/0020-impr-data-val-cart/rafa1894.yml)
    * [rafa1894](https://gitlab.com/autonomicjump/challenges/-/blob/master/vbd/acuart/0020-impr-data-val-cart/rafa1894)
      * [rafa1894.png](https://gitlab.com/autonomicjump/challenges/-/blob/master/vbd/acuart/0020-impr-data-val-cart/rafa1894/img1.png)

</TabItem>
</Tabs>

## 1.1. Rules

* The naming of all files and folders,
  must not exceed 35 characters,
  written in lowercase,
  without any special characters
  and in case a space is needed
  use a *-* (dash) to replace it.

* Vulnerability folders in vbd
  have the following naming structure:

  * `<cwe-code>-<exploit-name>` where:
  * `<cwe-code>` is the four digit code of the vulnerability
  * `<exploit-name>` is the name of the vulnerability
  * `Example` [0384-smgmt-cookies-httponly](https://gitlab.com/autonomicjump/challenges/tree/master/vbd/bwapp/0384-smgmt-cookies-httponly)

## 1.2. Files

Some of the folders
described in the structure
contain special files:

* **LINK.lst:** Contains the challenge URL.
  This file must only have one line
  with the challenge link and
  it must give a `HTTP 200` response
  when visiting it (No redirection).

* **DATA.lst:** Contains the test cases
  with which the challenge was validated.
  This file should only contain test cases
  that are immediately processable
  by any solution file.

* **OTHERS.lst:** It contains
  the links to the external solutions
  found on the Internet
  for said challenge
  which must not be read or used
  as a reference to solve the challenge.
  This file allows an automatic script
  to perform a similarity analysis
  with the challenges sent
  by the candidates.
