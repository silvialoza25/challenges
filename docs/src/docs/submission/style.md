---
id: style
title: Style
sidebar_label: Style
slug: /submission/style
---

Unless the language forces you
to do otherwise,
the source code must comply with the following:

1. Be in English (even the comments).

1. Indent using 2 spaces
  instead of tabs,
  unless the language requires otherwise.

1. Lines must not exceed
  80 characters in length.

1. Lines must not contain
  debug comments left behind.

1. Function definition
  must be separated by 1 empty line,
  unless the linter or the language requires otherwise.

1. Use one space around
  (on each side of)
  most binary and ternary operators,
  such as any of these:

  `=  +  -  <  >  *  /  %  |  &  ^  <=  >=  ==  !=  ?  :`
  unless the linter or the language requires otherwise.

1. Avoid using var names like x, y, z, i,
  instead of that use more useful names,
  i.e., if x is a constant
  you can use const_x or something that you prefer

1. Some languages such as haskell, ocaml and others
  have quite a different syntax
  than other more common languages such as python,
  so you should find and follow the style guides
  for these types of languages,
  however,
  if possible you must keep the rules set out above.

Embedded code snippets
must comply with the following:

1. Be enumerated.
  To do so add the parameter
  linenums to the source block.

1. Not have more than 8 lines.

1. No repeating a snippet
  that has already been used in the guide.

1. Add the lines of code
  to the post using a code block, don’t use images.
