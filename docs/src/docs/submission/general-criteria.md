---
id: general-criteria
title: General Criteria
sidebar_label: General Criteria
slug: /submission/
---

Solutions are sent
through a **Merge Request** (**MR**)
to the **master** branch
of the [challenges](https://gitlab.com/autonomicjump/challenges) repository.
Take a look at the
[Merge Requests](https://gitlab.com/autonomicjump/challenges/merge_requests?scope=all&utf8=%E2%9C%93&state=merged).

Before sending an **MR**
please verify that you meet
the following criteria:

1. You should only work on a branch
  whose name is exactly
  your username in **Gitlab**.

1. All files related to a challenge’s solution
  must respect the [Structure](/submission/structure)

1. If the solutions require additional files,
  they must be included
  in the corresponding challenge directory.

1. Each challenge solution must be submitted
  with **10** external solutions
  (**10** **URLs** in **OTHERS.lst** files).

1. Each challenge solution must be submitted
  with a **user-name.yml** file
    - [Programming and ctf-hacking challenges](https://docs.autonomicjump.com/templates/yaml/hack-code)
    - [vbd-hacking vulnerabilities](https://docs.autonomicjump.com/templates/yaml/vbd)

1. You are allowed to add
  new (just make sure to follow the [Structure](/submission/structure)):
    - Challenge sites or challenges
    for existing sites
    for **programming** and **ctf-hacking**
    - ToE's or new vulnerabilities
    to existing ToE's for **vbd-hacking**

1. If you add new sites/ToE's
  or challenges/vulnerabilities,
  you are allowed to add solutions
  to their OTHERS.lst files

1. The solution and all files
  associated with it
  must be all sent in **1** **commit**.

1. The **commit** for each solution
  must be sent in only **1** **MR**.

1. The **MR** must only be sent
  once your branch has successfully
  finished integrating (green).

1. If the **MR** is rejected
  it must not be reopened.
  The errors must be fixed
  and the solution sent in a new **MR**.

1. The **commit** message
  to send the solution
  must follow one of the templates
  according to the type of the solution:
    - [Programming and ctf-hacking challenges](https://docs.autonomicjump.com/templates/commit/hack-code)
    - [vbd-hacking vulnerabilities](https://docs.autonomicjump.com/templates/commit/vbd)

1. Challenge solutions,
  regardless if they are **programming**,
  **ctf-hacking** or **vbd-hacking**
  must always meet
  the following [style](/submission/style) guideline.
