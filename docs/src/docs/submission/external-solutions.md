---
id: external-solutions
title: External Solutions
sidebar_label: External Solutions
slug: /submission/external-solutions
---

## 1.1 OTHERS.lst links rules

In this section,
you will find all the rules
that apply to the 10 OTHERS.lst URL's you
must provide when uploading a MR:

1. They must be direct links (**HTTP 200**)
  without redirection (**HTTP 301/302**).
1. They don’t need to be solutions
  for the same challenge
  that you solved or the same website.
1. They must be **hacking** links
  if you solved a **hacking** challenge.
    - The links must be unique,
    in other words,
    two links can't be the same
    in an OTHERS.lst file.
    - If you send a **vbd-hacking** solution,
    the external solutions must be **vbd-hacking** solutions.
    - If you send a **ctf-hacking** solution,
    the external solutions
    can be **ctf-hacking** or **vbd-hacking** solutions.

1. They must be **programming** solutions
  if you solved a **programming** challenge.
    - You must not add external solutions
    for a language that already has an external solution.
    - Within the **OTHERS** of a **programming** solution
    the **URLs** must be ordered alphabetically by extension.
    - Programming OTHERS must **only include sources from repositories**;
    no websites, no video tutorials.
    The URL to the external solution
    must be provided on its
    **raw version:** [raw.githubusercontent.com](https://raw.githubusercontent.com/).
