---
id: specific-criteria
title: Specific Criteria
sidebar_label: Specific Criteria
slug: /submission/specific-criteria
---

## 1.1 **programming** solutions

1. A solution in the same language you chose
  (in the challenge folder)
  must not exist already.

1. They must not have an external
  indexed solution
  (links  **OTHERS.lst** of the challenge)
  for the language chosen by you.

1. Only one unique solution per user
  per challenge is counted.

1. The strictest **compilation**
  (optional for interpreted languages)
  and strictest **linting** (mandatory for all languages)
  commands used and their output
  must be included in the **prelude**
  at the beginning of the code.

  ```md
  /*
  $ cargo clippy #linting
  $ rustup run nightly #compilation
  Compiling milogin v0.1.0 (file:///../skhorn)
  Finished dev [unoptimized + debuginfo] target(s) in 0.22 secs
  $ rustc milogin.rs
  $
  */

  My solution's first line.
  ```md

1. The execution commands used and their output
  must be included in the *postlude*
  at the end of the code.

  ```md
  My solution's last line.

  /*
  $ cat DATA.lst | ./skhorn
  over obese obese normal obese obese obese obese ...
  */
  ```

1. If the solution takes a set of input data,
  such input must not be hardcoded
  into the solution.
  Meaning that the solution
  must read the **DATA.lst** file located
  in the challenge directory from **stdin**.
  If such file does not exist,
  you must include it in your commit

1. Most source codes
  go through an evaluation process
  in order to evaluate good coding habits.
  You can check what linters are currently
  integrated in our [CI](https://aws.amazon.com/devops/continuous-integration/)
  in this [link](https://gitlab.com/autonomicjump/challenges/-/tree/master/build/builders/solutions)

1. Source code must be created
  by hand (manually).
  Solutions built as a result
  of program transformation
  (compilation, transpilation, etc)
  or any other kind of automatic generation
  are not allowed.

1. When presenting several solutions
  to the same challenge,
  only one count as unique.

1. The solutions only allow libraries
  from the standard library,
  we only allow specific libs
  in some langs like rust,
  you can find out in the apart for that.

1. When you are submitting a solution
  make sure that you avoid using **GLOBAL_VARS**
  and state (classes. etc)
  and always **abstract using functional programming**
  in your solution.

1. All solutions must use
  an **indent of 2 spaces**
  unless the lang didn't allow that.

## 1.2 **ctf-hacking** and **vbd-hacking** solutions

1. They must not have a solution in **Gherkin** (**\*.feature**)
  in the repository (challenge folder).

1. They must not have
  an external indexed solution
  (links **OTHERS.lst** of the challenge).

1. They must be challenges that
  require a technical level
  (not mathematical nor riddle)
  from [WeChall](http://www.wechall.net/)
  or its related sites.

## 1.3 **ctf-hacking** solutions

1. They must follow the template
  [hacking-challenges.feature](https://docs.autonomicjump.com/templates/gherkin/hack)

1. The flag for ctf-hacking challenges
  must be censored in evidences
  and it must be implicit
  inside the .feature file

## 1.4 **vbd-hacking** solutions

1. They must follow the template
  [hacking-vbd.feature](https://docs.autonomicjump.com/templates/gherkin/vbd)

1. They must be challenges
  that Require exploiting **ToE** listed in:

1. [OWASP Off-Line apps](https://owasp.org/www-project-vulnerable-web-applications-directory/#div-offline)

1. [OWASP VMs / ISOs](https://owasp.org/www-project-vulnerable-web-applications-directory/#div-vm-iso)
    - [OWASP On-Line apps](https://owasp.org/www-project-vulnerable-web-applications-directory/#div-online)
