---
id: uniqueness
title: Uniqueness
sidebar_label: Uniqueness
slug: /submission/uniqueness
---

All solutions,
if compliant with all the previously given requirements,
will have a unique status.

For a **programming** solution
to lose its uniqueness:

- An external solution in the same language
  must be added to its OTHERS.lst file.

For **ctf-hacking** and **vbd-hacking** solutions
to lose their uniqueness:

- An external solution must be added
  to their OTHERS.lst file.
