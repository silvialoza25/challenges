---
id: score
title: Score
sidebar_label: Score
slug: /submission/score
---

As you go on solving challenges,
you must report your **total score**,
**ranking** and **score** obtained
for the specific challenged solved,
which will allow us
to follow your progress in this stage.
All this information
must be included in the **commit message**
following the format
described in the [General Criteria](/submission)

Here's how to get your scores and ranking
for each platform.

## 1. Programming Challenges

1. World Ranking

   1. In **codeabbey**,
   go to the “Ranking” tab:

   ![1](/img/1.png)

   1. Scroll to the bottom of the page and
   there you will find
   your position in the world ranking:

   ![2](/img/2.png)

1. Country Ranking

   1. While in the “Ranking” rab,
   select the country:

   ![3](/img/3.png)

   The page doesn’t directly show your position
   so you will have to manually count.
   To make this easier,
   you should take into account
   that each page shows **50** users.
   You must continue to the next page
   until you find your username
   on the ranking board

   ![4](/img/4.png)

## 2. **ctf-hacking** and **vbd-hacking**

![5](/img/5.png)

## 3. Autonomic Jump Rankings

This repository has a statistics site
that will allow you to know things
like how many unique solutions
you have uploaded,
your position in the overall hacker ranking,
most used programming languages,
among many others.
You can access the site
[here](https://app.autonomicjump.com/)
