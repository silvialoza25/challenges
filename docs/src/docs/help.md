---
id: help
title: Help
sidebar_label: Help
slug: /help
---

In our community,
we seek to answer ASAP your questions,
for this reason,
we have created a new format
to provide all the necessary information
to give us context about the issue
you are facing.
By sticking to this format
you'll get a response sooner,
if you don't
your question may remain unanswered
and the topic may be closed in the process.

You can see the [community](https://help.autonomicjump.com/),
search for any question that you have,
or open a new thread
if you didn't find your problem.

## 1. Template to post in the community for technical challenges/immersion

When you want to ask
for help in the community
it's better when you explain in detail
what happens because
that helps to understand your problem
and help to other users
with a similar problem
to understand what happens and find a solution too,
this improves the way how you can explain problems
and the way how you find solutions,
and show how you try
to fix problems and make research
about your problems.

For that reason we suggest
using the following template
and remember with more details and information you give,
 better solutions can be found for each case.

```md
### What happens

The description of what happens.

### What do you understand or find about that problem

Explain what do you think about what happens and/or what do you find in your research about what happens.

### You make any workaround? What did you do?

Explain your workaround if you made one or explain why you didn’t do one.

### (Optional) Why fails your workaround?

Explain why fails your workaround.

### Evidences

Add log, screenshots, URLs, or whatever that you consider that can help to solve the problem.

### I need help with

Explain what things you need help, this helps to find a fast answer to anyone that tries to help you.
```

---

### What happens

The description of what happens.

### What do you understand or find about that problem

Explain what do you think
about what happens
and/or what do you find
in your research about what happens.

### You make any workaround? What did you do?

Explain your workaround
if you made one
or explain why you didn’t do one.

### (Optional) Why fails your workaround?

Explain why fails your workaround.

### Evidences

Add log, screenshots,
 URLs, or whatever that you consider
 that can help to solve the problem.

### I need help with

Explain what things you need help,
this helps to find
a fast answer to anyone
that tries to help you.

---

The following link
shows a post
with a correct use of the template.

- https://help.autonomicjump.com/t/problem-in-test-user-yaml-test-policy-pipelines/1369
- https://help.autonomicjump.com/t/recomendation-about-sqli-learning-material/1299

This publication does not comply
with the characteristics stated above

- https://help.autonomicjump.com/t/mr-rejection-prelude-postlude/187

:::warning NOTE
If you found a solution in your own way
please post your solution too,
in case that you found the solution in the thread
please mark as solved with the solution.
:::
