---
id: libraries
title: Libraries
sidebar_label: Libraries
slug: /libraries
---

The libraries are packages
used to make some things more easily
on the language that you are working,
some languages have huge standard libraries,
and some language not,
we encourage the use of the standard libraries,
but we know that in some cases
is hard to make some challenges with only that,
for that reason we improve
a new way to use some specific libraries
that we added.

## Rust

We improve a package manager
to rust name cargo,
with that we can add libraries
from [**Crates.io**](https://crates.io/)
today we only support the library **nums**
that have a lot of numeric and mathematic uses,
like bigInt and others
to make easy the way
to solve some crypto challenges.

## Typescript / Javascript

We have npm package manager
and right now we didn't have any support
libraries in this langs,
now we are using npm to linter
and compile (in the case of typescript),
but with that,
we can add new libraries to work
if we found any good reason
to allow the use of any library in this langs
we can add it.

## PHP

We use composer how the PHP package manager,
but works in the same way as TS/JS,
if we found a good reason to add a new dependency
we can add it.
