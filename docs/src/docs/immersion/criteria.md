---
id: criteria
title: Criteria
sidebar_label: Criteria
slug: /immersion/criteria
---

1. For all challenges,
  use the same [criteria](/challenges/) listed
  in the challenge stage.
