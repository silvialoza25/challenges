---
id: cycles
title: Cycles
sidebar_label: Cycles
slug: /immersion/cycles
---

1. Training is done in weekly cycles.

1. Initially,
  you are offered *one cycle* of training.

1. At the end of each cycle,
  one of the following will happen:

    1. You do not receive any notification,
    which means the training cycle continues
    for *one more week*.

    1. You receive a notification
    that we do not want to continue
    with another cycle,
    which means we are terminating the training process.

    1. You receive a formal notification
    that we do not want to continue
    with another training
    cycle because we want you
    to advance to the next stage.

1. Usually *12 to 36 cycles* are required
  to finish this stage.

  :::note NOTE
  This is an estimate, it depends entirely on your productivity,
  perseverance, and performance.
  :::
