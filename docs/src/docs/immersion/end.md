---
id: end
title: End
sidebar_label: End
slug: /immersion/end
---

The immersion stage ends if:

1. You have successfully completed
  the link: [objectives](/immersion/objectives).

1. You are not meeting all [terms](/immersion/terms)
  and [criteria](/immersion/criteria) defined in this stage.

If you have any questions,
don't hesitate to contact us at careers@autonomicmind.com.
