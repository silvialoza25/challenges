---
id: start
title: Get Started
sidebar_label: Start
slug: /immersion/start
---

This stage begins when:

1. You finish the link:
  [challenges stage](/challenges/)

1. Send us an email (careers@autonomicmind.com)
  with the following:

:::note Quote
I have read and understood all the documentation and
terms of the immersion stage.
I accept the offer and confirm that I can start on AAAA/MM/DD.

Attached:

1. ProfessionalCard.pdf if it applies
  in your country and you are an undergraduate.

1. PaymentReceiptProfessionalCard.pdf if it applies in your country
  and you are an undergraduate,
  but don’t yet have it.

1. Neither, if you have not yet graduated
  from secondary school.
:::

---

:::caution IMPORTANT
When you have finished all the items mentioned in this section
and you begin the immersion stage, other procedural details such as,
how you do the everyday training, the programming languages used,
documentation format, and the required tools,
among others, will be disclosed.
:::
