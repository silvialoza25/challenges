---
id: introduction
title: Introduction
sidebar_label: Introduction
slug: /immersion/
---

Welcome to the *Immersion* stage.
*Immersion* is an intensive self-training
phase involving a variety of assignments
in `programming` and `hacking`.
