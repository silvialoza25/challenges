---
id: training
title: Training
sidebar_label: Training
slug: /immersion/training
---

The purpose is to solve within a short time
as many *unique challenges* as possible
always seeking a balance
between all categories (`code`, `hack`, `vbd`).
This means that every category
must have the same ammount
of unique solutions (standard deviation = 0).
Every `MR` must contribute
to the balance of this metric,
otherwise the `MR` will be closed.

You can keep tracking
of your metrics in the following page:

```md
https://app.autonomicjump.com/users/{$GITLAB_USERNAME}/
```

Just replace your `{$GITLAB_USERNAME}`.

1. Read carefully the programming challenge
  requirements below:

    1. Each programming challenge completed
    must be *one (1)* score point
    greater than the previous one.
    1. If no challenges with greater score exist,
    start again solving a challenge of lower score.
    1. If you reach a point
    where the need to obtain a higher score
    makes it hard for you to advance:

       1. Read about the topics you haven't mastered yet
        (graphs, trees, modular arithmetic, etc).
       1. Try different approaches,
        (Recursion, Heuristics, Dynamic
        Programming, Greedy, Divide and Conquer, etc).
       1. *Persevere!* We are interested in disciplined programmers.

    1. If you reach a point
    where you definitely cannot advance:

       1. Remember that in the challenge,
        the process is just as important as the goal.
       1. Pick a different challenge,
        the new difficulty is up to you,
        and try to solve it.
