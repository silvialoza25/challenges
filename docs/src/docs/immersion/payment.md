---
id: payment
title: Payment
sidebar_label: Payment
slug: /immersion/payment
---

1. The time you spend
  that is reported as training
  will be paid at *$9,468 COP* (Colombian pesos) an hour.

1. Based on the previously stated reference time of *48 hours/week*
  (see link: [Terms](/immersion/terms), sentence *8* above),
  we expect an *80%* minimum level of work time.

1. Regardless of expectation,
  you will be paid only
  for the actual time worked.

1. Your contract is
  for services with monthly payments.

1. During your training stage,
  you are considered an independent contractor.
  As such, you are responsible
  for paying any withholding
  and social security taxes.

1. On the *first business day*
  of the following month,
  we must have,
  in addition to your direct
  deposit account information:
    * An invoice for the hours
    you worked in the previous month.
    * Payment stub of your social security
    for the previous period.

If you live on the outskirts of Bogotá or Medellín,
you can begin the immersion process remotely;
no other terms can be changed.
If you choose to begin remotely,
you may do so *if and only if*:

1. When we ask you to come into
  the provided offices
  to continue the immersion process,
  you comply within *7 calendar days* of the request.

1. When you continue the immersion process in person,
  the hourly pay rate will be increased.
  It will be twice as much as previously indicated
  (see link: [Payment](/immersion/payment), number 1)
  in order to compensate you
  for any transportation and accommodation costs
  you may incur.

1. At this stage of the selection process
  we guarantee you a minimum of `2` immersion cycles.

1. To be clear;
  if you are hired,
  you are required to work in person
  at the provided facilities
  located in Medellín or Bogotá.
  Immersion is one stage
  of the selection process.
  In this stage,
  we improve your interaction with our team
  and expand your training into other topics.

If you are currently employed,
you can participate in the immersion process
under the following conditions:

1. Indicate how many hours per week
  you are willing to train.
  Your training minimum is `8` hours per week.

1. Work remotely without coming
  o our facilities.
