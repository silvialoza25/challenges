---
id: terms
title: Terms
sidebar_label: Terms
slug: /immersion/terms
---

1. All terms
  from the [challenges](/challenges/) stage apply.

1. Use the assigned tool in automatic mode
  to report all your work time.

1. Your training day begins at *8:00 AM*.
  `Autonomic Jump` will let you know the location
  of the offices you will be attending.

1. By *11:00 PM* of each training day,
  you must have sent
  at least one accepted-into-master `MR`,
  or a standard progress report
  if the `MR` has been rejected.
  You can fill your standard progress report
  through [This link](https://forms.autonomicjump.com/avance/).

1. You may then continue training
  and reporting your work time,
  done outside of the office,
  from a location of your choice,
  as long as you comply
  with the 2 previous rules.

1. If for some exceptional reason,
  you are not able to complete the training in person,
  a weekly training schedule to work remotely,
  will be established.

1. Training must always be done
  using your own personal laptop.

1. Your daily dedication to your work
  will be evaluated based on
  a reference time of *48 hours/week*.

1. Autonomic Jump's expectation is
  that you will be present in person,
  at the given offices, each business day.

1. If for personal or health reasons,
  you are unable to come to work in person,
  you will not be allowed
  to complete any challenges or report any training time
  for that day.

1. On the third business day of each week,
  you are required to work
  at your maximum level of productivity and commitment.
