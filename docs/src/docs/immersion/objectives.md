---
id: objectives
title: Objectives
sidebar_label: Objectives
slug: /immersion/objectives
---

1. Solve `30` unique `programming` challenges.
1. Solve `30` unique `ctf-hacking` challenges.
1. Solve `30` unique `vbd-hacking` challenges.
