---
id: philosophy
title: Philosophy
sidebar_label: Philosophy
slug: /immersion/philosophy
---

Your goal in this stage is to further
increase your technical proficiency
while substantiating your interpersonal
communication and teamwork skills.

Because this is a lengthy
and time-consuming process,
`Autonomic Jump`,
regardless of your progress
in points and ranking,
provides monetary compensation
for the time you invest
in completing the assignments.
