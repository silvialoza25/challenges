---
id: policy
title: Test Policy
sidebar_label: Policy
slug: /policy
---

In this place,
you can found all things related
to the Job `test_policy`.

This Job was made
to control automatically the way
how to the trainee work,
taking in count the deviation,
structure, complexity, and steps.

## Policy

The policy is a YAML file
that we use to adjust the parameters
to each trainee,
with that we can make different ways
to our trainees to advance in the process.

- [**Policies**](https://gitlab.com/autonomicjump/challenges/-/blob/master/policies/data.yaml)
- [**Policies Schema**](https://gitlab.com/autonomicjump/challenges/blob/51e1920a658d95954322e336a8a90dcbcd3c820b/policies/schema.yaml#L38)

## Deviation

Following our own rules
we see if your deviation always tends to 0,
for that you always should make
1 challenge per scope
before starting a new cycle,
your deviation is evaluated based on
the policy and which scopes are active.

## Sites

This field is composed
of 2 parameters, active and list.

- Active:
  This says if the policy limits
  the sites to the sites in the list.

- List:
  This is the list
  for the sites that the policy allows
  to make the challenges.

This field is present
in all scopes and we can limit
the sites where the trainees work,
if this is active our trainee
only can make challenges
in the sites listed in that field.

## Langs

This field is only
for Code challenges and has 2 parameters,
active and list.

- Active:
  This says if the policy limits
  the langs to the languages in the list.

- List:
  This is the list
  for the languages that the policy
  allows to make the challenges.

This helps to choose
the language that we want
our trainees to use.

## Complexity

This field is only
for Code challenges and has 5 parameters,
active, min, goal, min_step, and max_step.

- Active:
  This says if the policy evaluates
  the complexity.

- Min:
  Says the minimum complexity
  we can accept in the cycles.

- Goal:
  This says what is the expected complexity
  we want our trainee to reach with the cycles.

- Min_step:
  This is the minimum step
  that you must reach
  when uploading a new solution, for example:

  ```md
  min_step: 2

  your previous complexity: 3

  your minimum expected complexity: min_step + previous_complexity

  minimum_expected_complexity: 5
  ```

Knowing this our trainee
must surpass the minimum_expected_complexity.

- Max_step:
  This is the maximum step
  that you are allowed to reach
  when uploading a new solution,
  for example:

  ```md
  max_step: 3

  your previous complexity: 3

  your maximum expected complexity: max_step + previous_complexity

  maximum_expected_complexity: 6
  ```

Knowing this
our trainee shouldn’t surpass
the maximum_expected_complexity.
