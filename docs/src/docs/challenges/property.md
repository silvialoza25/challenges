---
id: property
title: Property
sidebar_label: Property
slug: /challenges/property
---

* The proprietary rights of all content
  in the Challenges repository
  is defined in the
  [COPYRIGHT](https://gitlab.com/autonomicjump/challenges/blob/master/COPYRIGHT.txt)
  file.

* The license and privileges
  that users have in the Challenges repository
  is defined in the
  [LICENSE](https://gitlab.com/autonomicjump/challenges/blob/master/LICENSE.txt)
  file.

* Carrying out a `merge request`
  implies the transfer of copyrights.
  Therefore, all information contained herein
  may be used by `Autonomic Jump`
  for any commercial purpose,
  always preserving
  the moral rights of their authors.
