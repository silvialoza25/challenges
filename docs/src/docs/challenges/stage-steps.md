---
id: stage-steps
title: Stage Steps
sidebar_label: Stage Steps
slug: /challenges/stage-steps
---

To successfully finish this stage, you must:

1. Register on `GitLab`
  using your personal email
  and the username of your choice.
  Your username must not exceed
  *12* characters in length
  and only contain lowercase letters and numbers.

1. Join our [forum](https://help.autonomicjump.com/),
  where you can interact
  with `Autonomic Jump` personnel
  and other candidates,
  who are working in the same stage that you are,
  to discuss questions or issues you may have.

1. Request permission
  to access the `Git Challenges` repository
  by filling out this
  [form](https://forms.autonomicjump.com/access/).

1. In the `Repo Access Message` field,
  paste the following message:

  :::note message
  I have read and understood
  all documentation pertaining
  to technical challenges,
  I agree to all of the terms
  and therefore request access
  to the `git` `Challenges` repository
  with my `GitLab` username.
  :::

1. Complete `three (3)` `programming` challenges.

1. Complete `three (3)` `ctf-hacking` challenges.

1. Complete `three (3)` `vbd-hacking` challenges.
