---
id: rules
title: Rules
sidebar_label: Rules
slug: /challenges/rules
---

* When solving `programming` challenges,
  we require you to use
  [Codeabbey](https://www.codeabbey.com/).
  Also, we suggest using a language
  that is not widely used.

    * Supported languages
    can be found [here](https://gitlab.com/autonomicjump/challenges/-/blob/master/code/lang-data-supported.yml).

    * Dropped (no longer supported) languages
    can be found [here](https://gitlab.com/autonomicjump/challenges/-/blob/master/code/lang-data-dropped.yml).

* When solving `ctf-hacking` challenges,
  we recommend [Root Me](https://www.root-me.org/?lang=en)

* After solving a challenge,
  immediately submit your answer.
  Do not save your answers to send later
  as a larger group submission.
  When you send them individually
  and immediately you will receive feedback
  that will tell you what,
  if anything, you did incorrectly
  and may help you with upcoming questions.
