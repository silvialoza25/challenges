---
id: plagiarism
title: Plagiarism
sidebar_label: Plagiarism
slug: /challenges/plagiarism
---

Making solutions
easily available to everyone
presents an opportunity for plagiarism.
How do we show the solution
and avoid plagiarism at the same time?
Plagiarism is not a technical problem.
Plagiarism is copying another's work
and then presenting it as your own.

To discourage plagiarism
we require that the author of each algorithm
is clearly stated in a centralized place.
This provides clear author attribution
and allows for public scrutiny
in case of plagiarism.

In other words,
the current model avoids plagiarism
through total transparency.
