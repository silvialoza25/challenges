---
id: submission
title: Submission
sidebar_label: Submission
slug: /challenges/submission
---

Now that you know all the rules
and have a general understanding
of why these challenges are important,
you can proceed to the [submission guide](/submission)
and start posting your solutions.
Good luck!
