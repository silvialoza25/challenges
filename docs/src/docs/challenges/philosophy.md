---
id: philosophy
title: Philosophy
sidebar_label: Philosophy
slug: /challenges/philosophy
---

In this stage,
we want you to demonstrate,
through practical exercises,
your technical skills
by solving problems
similar to the ones
you will face in the work
you're currently applying to.

The challenges
are divided into
`vbd-hacking`,
`ctf-hacking`,
and `programming`.
With the first,
you demonstrate your skill
to bypass security controls.
With the second,
you demonstrate your adaptation skill
when it comes to problem-solving
in unknown environments.
With the third,
you demonstrate your proficiency and speed
in understanding source code.

By solving challenges we:

* Promote the solution of unresolved challenges.

* Encourage self-directed
  and independent learning.

By uploading your solutions to `GitLab`,
you will be able to:

* Use the `GitLab` infrastructure
  to analyze your efficiency
  and work quality
  as you adapt to the process.

* Familiarize yourself with the tools
  (`Git`, `AsciiDoc`, `Python`, `Gherkin`, etc)
  and concepts (automation, unit tests,
  continuous integration, `linting`, etc.).

* Allow the community and the team to view your results.
