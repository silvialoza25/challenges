---
id: introduction
title: Introduction
sidebar_label: Introduction
slug: /challenges/
---

As this stage parallels
the other stages of the process,
you can work on this at the same time
you are completing previous stages.
The more progress you make on it now,
the greater the probability
that you may be evaluated
before other participants
who finish their work
in a linear fashion.

This stage can also function
as a kind of lifesaver for you.
If you have not yet graduated,
perhaps have no previous work experience,
or maybe got a low score
on the knowledge test,
but were able to successfully
finish this stage,
you have the most valuable competence of all:
The ability to learn new things on the fly
and apply them to solve real problems.
