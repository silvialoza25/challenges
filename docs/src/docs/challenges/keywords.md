---
id: keywords
title: Keywords
sidebar_label: Keywords
slug: /challenges/keywords
---

* `ToE` [Target of Evaluation](https://csrc.nist.gov/glossary/term/target_of_evaluation)
* `VBD` Vulnerable by design
* `CTF` [Capture The Flag](https://ctftime.org/ctf-wtf/)
