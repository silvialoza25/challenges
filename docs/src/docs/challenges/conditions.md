---
id: conditions
title: Conditions
sidebar_label: Conditions
slug: /challenges/conditions
---

The challenge stage ends
under any of the following conditions:

1. You have met all [objectives](/challenges/stage-steps)
  and sent an `email` with the links
  to your solutions in the `master` branch.

1. If there has been no activity
  (`push` to the `git` Challenges repository)
  within 14 calendar days.

1. If you clearly state via email,
  that you are no longer interested
  in continuing the process.

1. If you present someone else’s complete
  or partial solutions as your own (plagiarism).

1. If you solve a challenge
  with the help of others.

In all cases,
the email address for these steps is:
careers@autonomicmind.com

If you were removed
from the challenge due to circumstances 1, 2 or 3,
you may apply again, at any time,
and start the process over by clicking
[here](https://forms.autonomicjump.com/access/)
If your removal was a result
of circumstance 4 or 5
you are not eligible
for any future retry.
