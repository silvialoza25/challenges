module.exports = {

  Docs: [
    'home',
    {
      type: 'category',
      label: 'Challenges',
      items: [
        'challenges/introduction',
        'challenges/philosophy',
        'challenges/rules',
        'challenges/stage-steps',
        'challenges/conditions',
        'challenges/questions',
        'challenges/property',
        'challenges/plagiarism',
        'challenges/submission',
        'challenges/keywords',
      ],
    },
    {
      type: 'category',
      label: 'Submission',
      items: [
        'submission/general-criteria',
        'submission/specific-criteria',
        'submission/external-solutions',
        'submission/uniqueness',
        'submission/commit',
        'submission/merge-request',
        'submission/score',
        'submission/structure',
        'submission/style',
      ],
    },
    {
      type: 'category',
      label: 'Immersion',
      items: [
        'immersion/introduction',
        'immersion/philosophy',
        'immersion/objectives',
        'immersion/terms',
        'immersion/criteria',
        'immersion/cycles',
        'immersion/training',
        'immersion/payment',
        'immersion/start',
        'immersion/end',
      ],
    },
    {
      type: 'category',
      label: 'Templates',
      items: [
        {
          type: 'category',
          label: 'YAML',
          items: [
            'templates/yaml/hack-code',
            'templates/yaml/vbd',
          ],
        },
        {
          type: 'category',
          label: 'Gherkin',
          items: [
            'templates/gherkin/hack',
            'templates/gherkin/vbd',
          ],
        },
        {
          type: 'category',
          label: 'Commit',
          items: [
            'templates/commit/hack-code',
            'templates/commit/vbd',
          ],
        },
      ],
    },
    'policy',
    'builds',
    'languages',
    'help',
    'libraries'
  ]
};
