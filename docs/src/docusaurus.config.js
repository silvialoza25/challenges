module.exports = {
  title: 'Autonomic Jump',
  url: 'https://docs.autonomicjump.com/',
  baseUrl: process.env.env == 'prod' ? '/' : `/${process.env.CI_COMMIT_REF_NAME}/`,
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.png',
  organizationName: 'Autonomic Jump', // Usually your GitHub org/user name.
  projectName: 'Autonomic Jump', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Autonomic · Jump',
      logo: {
        alt: 'Autonomic Jump Logo',
        src: 'img/logo.png',
      },
      items: [
        {
          href: 'https://gitlab.com/autonomicjump/challenges',
          label: 'GitHub',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Community',
          items: [
            {
              label: 'Help Autonomic Jump',
              href: 'https://help.autonomicjump.com/',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/autonomicjump/challenges',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          routeBasePath: '/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  plugins: [
    require.resolve('docusaurus-lunr-search'),
  ],
};
