#|
$ sblint -v dantivar.lsp
[INFO] Lint file dantivar.lsp
$ clisp -c dantivar.lsp
Compiling file /home/../dantivar.lsp ...
Wrote file /home/../dantivar.fas
0 errors, 0 warnings
Bye.

|#
(defun check-num ( num )
  (setq temp num)
  (setq units 0)

  (loop while (>= temp 10) do
    (setq temp (floor temp 10))
    (setq units (+ units 1)))

  (cond ((= (mod temp 2) 0)
    (setq temp (+ temp 1))
    (setq temp (+ (* temp (expt 10 units)) 1))
    (return-from check-num temp)))
  (cond ((= (mod temp 5) 0)
    (setq temp (+ temp 2))
    (setq temp (+ (* temp (expt 10 units)) 1))
    (return-from check-num temp)))
  (return-from check-num num)
)

(defun mod-power (x y p)
  (setq res 1)
  (setq x (mod x p))

  (loop while (> y 0) do
    (if (/= (mod y 2) 0)
      (setq res (mod (* res x) p)))
    (setq y (floor y 2))
    (setq x (mod (* x x) p)))
  (return-from mod-power res)
)

(defun miller-test (di num)
  (setq a (+ 2 (random (- num 4))))
  (setq x (mod-power a di num))
  (if (or (= x 1) (= x (- num 1)))
    (return-from miller-test t))
  (loop while (/= di (- num 1)) do
    (setq x (mod (* x x) num))
    (setq di (* di 2))

    (if (= x 1)
      (return-from miller-test nil))
    (if (= x (- num 1))
      (return-from miller-test t)))
  (return-from miller-test nil)
)

(defun is-prime (num k)
  (if (or (<= num 1) (= num 4))
    (return-from is-prime nil))
  (if (<= num 3)
    (return-from is-prime t))
  (setq di (- num 1))
  (loop while (= (mod di 2) 0) do
    (setq di (/ di 2)))
  (loop for i from 1 to k do
    (if (equal (miller-test di num) nil)
      (return-from is-prime nil)))
  (return-from is-prime t)
)

(defun emirp-prime (num k)
  (setq i num)
  (if (= (mod i 2) 0)
    (setq i (+ i 1)))

  (loop
    (cond ((is-prime i k)
      (setq i (check-num i))
      (setq lst (map 'list #'digit-char-p (prin1-to-string i)))
      (setq lst (reverse lst))
      (setq my_num (read-from-string (format nil "~{~A~}" lst)))
      (if (is-prime my_num k)
        (return-from emirp-prime i))))
    (setq i (+ i 2)))
)


(defun read-data ()
  (setq test_cases (read))
  (loop for i from 1 to test_cases do
    (format t "~D " (emirp-prime (read) 4)))
)

(read-data)

#|
$ cat DATA.lst | clisp dantivar.lsp
30939269335276755753029 70542276912119908564307 30542990501070864739151
30180029436013545780049 90668339652399947893099 90377870022727936643971
10554112870693480545563 90723707961999868564111 70000000000000000000859
90000000000000000000001 30000000000000000000047 70000000000000000000859
70891076823639578114099 70344452305513385070083 10782355894393623321557
70000000000000000000859 10132869121360915557049 30718898702116166538083
|#
