# raku -c ludsrill.raku
# Syntax OK

sub main () {
  my ($a, $b) = $*IN.get.split(" ");
  say $a + $b;
}

main()

# cat DATA.lst | raku ludsrill.raku
# 29266
