# jq -n -f ludsrill.jq

def addpairs:
  if length < 2 then empty
  else (.[0] + .[1]), (.[2:] | addpairs)
  end;

addpairs

# cat DATA.lst | jq -s -f ludsrill.jq
# 29266
