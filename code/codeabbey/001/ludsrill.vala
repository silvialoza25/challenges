/* valac ludsrill.vala */

int addition (string data) {
  var split = data.split(" ");
  int a = int.parse(split[0]);
  int b = int.parse(split[0]);
  return a + b;
}

void main () {
  var data = stdin.read_line();
  int solution = addition(data);
  stdout.printf("%d", solution);
}

/*
  cat DATA.lst | ./ludsrill
  29266
 */
