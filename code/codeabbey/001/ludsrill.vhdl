-- ghdl -a ludsrill.vhdl
-- ghdl -e ludsrill

library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;

entity ludsrill is
end;

architecture beh of ludsrill is
begin
  process
    variable line_in, line_out : line;
    variable a,b : integer;
  begin
    readline(INPUT, line_in);
    read(line_in, a);
    read(line_in, b);

    write(line_out, a+b);
    writeline(OUTPUT, line_out);
    wait; -- needed to stop the execution
  end process;
end architecture beh;

-- cat DATA.lst | ghdl -r ludsrill
-- 29266
