;; $ clj-kondo --lint bridamo98.clj
;; linting took 24ms, errors: 0, warnings: 0

(ns bridamo98-001
  (:gen-class)
  (:require [clojure.core :as core]))

(defn sum-values [fst-number snd-number]
  (+ fst-number snd-number))

(defn main []
  (let [fst-number (core/read) snd-number (core/read)]
    (print (sum-values fst-number snd-number))))

(main)

;; $ cat DATA.lst | clj bridamo98.clj
;; 29266
