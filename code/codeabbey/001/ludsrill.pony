"""
$ ponyc ludsrill
  ludsrill
  Generating
  Reachability
  Selector painting
  Data prototypes
  Data types
  Function prototypes
  Functions
  Descriptors
  Optimising
  Writing ./ludsrill.o
  Linking ./ludsrill1
"""

actor Main
  let _env:Env
  new create(env:Env) =>
    _env=env
    env.input(
      object iso is InputNotify
      let _e:Main=this
      fun ref apply(data:Array[U8] iso) =>
        _e(consume data)
      fun ref dispose() =>
        None
      end, 512)

  fun get_int(head: (String val | None val)): I32 =>
    let head_string:String = head.string()
    var first:I32 = 0
    try
      first = match head_string.read_int[I32](0)?
        |(let x:I32,_) => x
      end
    end
    first

  fun addition(parts: Array[String]): I32 =>
    let head = try parts.apply(0)? end
    let last = try parts.apply(1)? end
    var first:I32 = 0
    var second:I32 = 0
    first = get_int(head)
    second = get_int(last)
    first + second

  be apply(s:Array[U8] iso)=>
    let c = String.from_iso_array(consume s)
    let parts:Array[String] = c.split(" ",0)
    _env.out.print(addition(parts).string())

"""
$ cat DATA.lst | ./ludsrill
  29266
"""
