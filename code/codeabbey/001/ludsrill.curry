-- $ curry-verify ludsrill.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `test'!

echo :: Prelude.IO [Prelude.Char]
echo = do
  line <- getLine
  return line

nth :: (Prelude.Eq a, Prelude.Num a) => a -> [[b]] -> [b]
nth _ [] = []
nth n (x:xs) = if n==1 then x else nth (n-1) xs

main :: IO ()
main = do
  input <- echo
  let
    split = words input
    first = read (nth 1 split)
    second = read (nth 2 split)
    solution = first + second
  putStrLn (show solution)

-- cat DATA.lst | pakcs :load ludsrill.curry :eval main :quit
-- 29266
