--[[
$ luacheck jmesa85.lua
Checking jmesa85.lua                              OK
Total: 0 warnings / 0 errors in 1 file
--]]

local function sum_a_b()
  -- Read stdin
  local a, b = io.read("*number", "*number")
  -- Write to stdin
  print(a + b)
end

sum_a_b()

--[[
$ cat DATA.lst | lua jmesa85.lua
18772
--]]
