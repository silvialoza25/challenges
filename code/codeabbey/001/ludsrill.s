;; nasm -f elf ludsrill.s
;; gcc -m32 -fno-pie ludsrill.o -o ludsrill

global main: function

extern __stack_chk_fail
extern printf
extern __isoc99_scanf

main:
  lea  ecx, [esp+4H]
  and  esp, 0FFFFFFF0H
  push dword [ecx-4H]
  push ebp
  mov  ebp, esp
  push ecx
  sub  esp, 20
  mov  eax, dword [gs:14H]
  mov  dword [ebp-0CH], eax
  xor  eax, eax
  sub  esp, 4
  lea  eax, [ebp-10H]
  push eax
  lea  eax, [ebp-14H]
  push eax
  push ?_002
  call __isoc99_scanf
  add  esp, 16
  mov  edx, dword [ebp-14H]
  mov  eax, dword [ebp-10H]
  add  eax, edx
  sub  esp, 8
  push eax
  push ?_003
  call printf
  add  esp, 16
  mov  eax, 0
  mov  ecx, dword [ebp-0CH]
  xor  ecx, dword [gs:14H]
  jz   ?_001
  call __stack_chk_fail

?_001:
  mov     ecx, dword [ebp-4H]
  leave
  lea     esp, [ecx-4H]
  ret

?_002:
  db 25H, 64H, 25H, 64H, 00H

?_003:
  db 25H, 64H, 0AH, 00H

;; cat DATA.lst | ./ludsrill
;; 29266
