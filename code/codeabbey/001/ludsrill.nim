#[
  $ nim check --checks:on ludsrill.nim
    Hint: 27749 LOC; 0.961 sec; 30.941MiB peakmem; Debug build;
    [SuccessX]

  $ nim c ludsrill.nim
]#

import strutils, sequtils

proc getnumber(): string =
  readLine(stdin)

proc main(): int =
  getnumber().split(' ').map(parseInt).foldl(a + b);

echo(main())

#[
  cat DATA.lst | ./ludsrill
]#
