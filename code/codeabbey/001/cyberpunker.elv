# elvish -compileonly cyberpunker.elv

#!/bin/elvish

use math
use str

fn number_data []{
  var input = []
  each [number]{
    set @input = (to-lines $input) (str:split ' ' $number)
  }
  put $input
}

fn add_total [data]{
  item_one = $data[0]
  item_two = $data[1]
  set result = (+ $item_one $item_two)
  put $result
}

echo (add_total (number_data))

# cat DATA.lst | elvish cyberpunker.elv
# 23510
