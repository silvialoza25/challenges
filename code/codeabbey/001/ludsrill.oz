%% ozc -c ludsrill.oz

functor
import
  Application System Open

define
  fun {Data}
    local
      class TextFile from Open.file Open.text end
    in
      {New TextFile init(name:stdin)}
    end
  end

  fun {Solve StdIn}
    local
      Input = {String.tokens {StdIn getS($)} & }
      First = {String.toInt {List.nth Input 1}}
      Secnd = {String.toInt {List.nth Input 2}}
      Solution = First + Secnd
    in
      Solution
    end
  end

  {System.show {Solve {Data}}}
  {Application.exit 0}
end

%% cat DATA.lst | ozengine ludsrill.ozf
%% 29266
