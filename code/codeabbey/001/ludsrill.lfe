;; lfec -L test.lfe

(defun split_data (str)
  (string:split str " "))

(defun addition (data)
  (let ((first (erlang:list_to_integer (lists:nth 1 data)))
        (second (erlang:list_to_integer (string:trim (lists:nth 2 data)))))
    (+ first second)))

(defun main ()
  (let* ((str (io:get_line ""))
         (splitting (split_data str))
         (solution (addition splitting)))
    (lfe_io:print solution)))

(main)

;; cat DATA.lst | lfe ludsrill.lfe
;; 29266
