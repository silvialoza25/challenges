#!/usr/bin/env python3
# $ mypy andrearey23.py
# Success: no issues found in 1 source file


def add() -> None:
    lines = input().split()
    print(int(lines[0]) + int(lines[1]))


add()

# $ cat DATA.lst | python andrearey23.py
# 18772
