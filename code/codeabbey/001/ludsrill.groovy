/*
  groovyc ludsrill.groovy
*/

def addition(String[] str) {
    int first = str.getAt(1).toInteger()
    int second = str.getAt(0).toInteger()
    int solution = first + second
}

def main = {
    def reader = new Scanner(System.in)
    String data = System.in.newReader().readLine();
    String[] str;
    str = data.split(" ");
    int solution = addition(str);
    print solution;
}

main()

/*
  cat DATA.lst | groovy ludsrill.groovy
  29266
*/
