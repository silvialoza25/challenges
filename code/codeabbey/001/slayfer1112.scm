#!
$ guile-tools compile -O3 \
      -W unsupported-warning \
      -W unused-variable \
      -W unused-toplevel \
      -W shadowed-toplevel \
      -W unbound-variable \
      -W macro-use-before-definition \
      -W arity-mismatch \
      -W duplicate-case-datum \
      -W bad-case-datum \
      -W format \
      -o slayfer1112.scm.go \
      slayfer1112.scm
wrote `slayfer1112.scm.go'
!#

(define (sum a b)
  (display (+ a b)))

(define (get-data)
  (let ((a (read))
        (b (read)))
    (vector a b)))

(define (main)
  (let ((data (get-data)))
    (let ((a (vector-ref data 0))
          (b (vector-ref data 1)))
    (sum a b))))

(main)

#!
$ cat DATA.lst | guile slayfer1112.scm
18772
!#
