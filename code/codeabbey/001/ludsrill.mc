% mmc -f ludsrill.mc
% mmc --make ludsrill

:- module ludsrill.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module list, string, int.

:- pred addition(int::in, int::in, int::out) is det.
:- pred processData(string::in, int::out, int::out) is det.

addition(First, Secnd, Solution) :-
  Solution = First + Secnd.

processData(String, First, Secnd) :-
  Words = string.words(String),
  First = string.det_to_int(list.det_index0(Words, 0)),
  Secnd = string.det_to_int(list.det_index0(Words, 1)).

main(!IO) :-
  io.read_line_as_string(Result, !IO),
  ( if
      Result = ok(String)
    then
      processData(String, First, Secnd),
      addition(First, Secnd, Solution),
      io.format("%i", [i(Solution)], !IO)
    else
      true).

% cat DATA.lst | ./ludsrill
% 29266
