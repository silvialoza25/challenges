;; nasm -felf64 ludsrill.asm
;; gcc -no-pie ludsrill.o -o ludsrill

default rel

global main: function

extern __stack_chk_fail
extern printf
extern __isoc99_scanf
extern _GLOBAL_OFFSET_TABLE_

main:
  push rbp
  mov  rbp, rsp
  sub  rsp, 16
  mov  rax, qword [fs:abs 28H]
  mov  qword [rbp-8H], rax
  xor  eax, eax
  lea  rdx, [rbp-0CH]
  lea  rax, [rbp-10H]
  mov  rsi, rax
  lea  rdi, [rel ?_002]
  mov  eax, 0
  call __isoc99_scanf
  mov  edx, dword [rbp-10H]
  mov  eax, dword [rbp-0CH]
  add  eax, edx
  mov  esi, eax
  lea  rdi, [rel ?_003]
  mov  eax, 0
  call printf
  mov  eax, 0
  mov  rcx, qword [rbp-8H]
  xor  rcx, qword [fs:abs 28H]
  jz   ?_001
  call __stack_chk_fail

?_001:
  leave
  ret

?_002:
  db 25H, 64H, 25H, 64H, 00H

?_003:
  db 25H, 64H, 0AH, 00H

;; cat DATA.lst | ./ludsrill
;; 29266
