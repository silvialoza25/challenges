/*
  $ ocamlc -o ludsrill -pp "refmt -p ml" -impl ludsrill.re
*/

let splitting = line => String.split_on_char(' ', line);

let maybe_read_line = () =>
  try (Some(read_line())) {
  | End_of_file => None
  };

let rec get_data = acc =>
  switch (maybe_read_line()) {
  | Some(line) => get_data(acc @ [splitting(line)])
  | None => Printf.printf("%i", int_of_string(List.nth(List.flatten(acc), 0))
      + int_of_string(List.nth(List.flatten(acc), 1)))
  };

let () = get_data([]);

/*
  cat DATA.lst | ./ludsrill.re
  29266
*/
