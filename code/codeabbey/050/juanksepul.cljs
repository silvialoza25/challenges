; clj-kondo --lint juanksepul.cljs
; linting took 766ms, errors: 0, warnings: 0

(ns codeabbey.juanksepul
  (:require [clojure.core :as core])
  (:require [clojure.string :as str]))

(defn filt-line [line]
  (str/lower-case (apply str (re-seq #"[a-zA-Z]" line))))

(defn read-data []
  (str/split-lines (core/slurp core/*in*)))

(defn is-palindrome [filtered-line]
  (if (= (seq filtered-line) (reverse filtered-line))
    (println "Y") (println "N")))

(defn !main []
  (doseq [line (rest (read-data))]
    (is-palindrome (filt-line line))))

(!main)

; cat DATA.lst | clj juanksepul.cljs
; Y
; N
; N
; Y
; N
; N
; Y
; Y
; Y
; N
; N
; Y
; Y
; Y
; N
; Y
; N
; N
; Y
