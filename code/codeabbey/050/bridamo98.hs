{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Data.Char
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let filteredData = map filterData inputData
  let solution = map evaluateSentence filteredData
  putStrLn (unwords solution)

filterData :: String -> String
filterData dat = result
  where
    aux = filter isLetter dat
    result = map toLower aux

evaluateSentence :: String -> String
evaluateSentence dat = result
  where
    aux = reverse dat
    result = if aux == dat
      then "Y"
      else "N"

{-
$ cat DATA.lst | ./bridamo98
  Y Y Y N N N N N Y Y Y N Y N Y
-}
