/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, bool, int, char.

:- pred str2list(string::in, list(string)::in, list(string)::out) is det.
str2list(String, PStrList, StrList) :-
(
  if length(String) = 0
  then StrList = PStrList
  else
    split(String, 1, Head, Tail),
    str2list(Tail, PStrList ++ [Head], StrList)
).

:- pred checkChars(list(string)::in, list(string)::in, bool::out) is det.
checkChars([], _, yes).
checkChars([Character | Tail], Letters, Contained) :-
(
  if
    member(Character, Letters),
    delete_first(Letters, Character, Remainder)
  then
    checkChars(Tail, Remainder, Contained)
  else Contained = no
).

:- pred checkList(list(string), list(string), int, int, int).
:- mode checkList(in, in, in, in, out) is det.
checkList([], _, _, FinalAcc, FinalAcc).
checkList([Word | Tail], Letters, Length, PartialAcc, FinalAcc) :-
(
  if
    length(Word) = Length
  then (
    if
      str2list(Word, [], StrList),
      checkChars(StrList, Letters, Contained),
      Contained = yes
    then checkList(Tail, Letters, Length, PartialAcc + 1, FinalAcc)
    else checkList(Tail, Letters, Length, PartialAcc, FinalAcc))
  else checkList(Tail, Letters, Length, PartialAcc, FinalAcc)
).

:- pred findWords(list(string), list(string), string, string).
:- mode findWords(in, in, in, out) is det.
findWords(_, [], FinalStr, strip(FinalStr)).
findWords(WordList, [Line | Tail], PartialStr, FinalStr) :-
(
  words_separator(char.is_whitespace, Line) = InputList,
  det_to_int(det_head(InputList)) = Length,
  det_tail(InputList) = Letters,
  checkList(WordList, Letters, Length, 0, Acc),
  PartialStr1 = PartialStr ++ " " ++ from_int(Acc),
  findWords(WordList, Tail, PartialStr1, FinalStr)
).

main(!IO) :-
  io.read_file_as_string(Result, !IO),
  (
    if Result = ok(FileContents)
    then
      split_at_string("zygotes", FileContents) = InputData,
      %words
      det_index1(InputData, 1, WordsStr),
      words(WordsStr) = WordList,
      %challenge data
      det_index1(InputData, 2, StrData),
      split_at_string("\n", StrData) = QueryList,
      det_drop(2,QueryList, PQueries),
      delete_all(PQueries, "", Queries),
      %solve challenge
      findWords(WordList ++ ["zygotes"], Queries, "", ResultStr),
      io.print_line(ResultStr, !IO)
    else if Result = error(_, IOError)
    then
      Msg = io.error_message(IOError),
      io.stderr_stream(Stderr, !IO),
      io.write_string(Stderr, Msg, !IO),
      io.set_exit_status(1, !IO)
    else
      true
  ).

/*
  $ cat words.lst DATA.lst | ./jpchavesm
  119 1 7 17 11 1 8 9 19 59 33 2 6 6 1
*/
