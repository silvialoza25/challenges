# elvish -compileonly cyberpunker.elv

#!/bin/elvish

use math
use str

fn number_data []{
  var input = []
  each [number]{
    set @input = (to-lines $input) (str:split ' ' $number)
  }
  put $input
}

fn add_total [data]{
  values = $data[0]
  if (== $values 44) {
    put (+ (to-lines $data) -$values)
  } elif (!= $values 44) {
    echo Error: invalid number of values!
  } else {
    echo Error!
  }
}

echo (add_total (number_data ))

# cat DATA.lst | elvish cyberpunker.elv
# 30649
