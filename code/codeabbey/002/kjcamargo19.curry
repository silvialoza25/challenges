-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import IO
import List
import Read

getData :: IO String
getData = do
  x <- getContents
  return x

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    useData = map readInt $ split (==' ') $ (tail (init dataLines)) !! 0
    finalResult = sum useData
  putStrLn (show finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 27601
