;; $ clj-kondo --lint bridamo98.clj
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-029
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn sum-in-loop[size-input content-input]
    (loop [i 0 result 0]
    (if (< i size-input)
      (recur (+ i 1)
        (+ result (nth content-input i)))
        result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))
  content-input (map #(Integer/parseInt %) (str/split (core/read-line) #" "))]
    (println (sum-in-loop size-input content-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.clj
;; 29357
