! $ fortran-linter hugobaymon.f90
! $ gfortran-9 hugobaymon.f90
! $
PROGRAM test

  implicit none

  integer, dimension(1:100) :: x
  integer :: n, i
  integer :: acum = 0

  read(*,*) n
  read(*,*) (x(i), i = 1, n)

  acum = sum(x(1: n))
  write(*, '(i0)') acum

END PROGRAM test

! cat DATA.lst | ./a.out
! 30857
