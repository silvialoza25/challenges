# $ lintr::lint('nickkar.r')

# formats input in a readable way
format_input <- function(data) {
  return(unlist(lapply((unlist(strsplit(data[2], " "))), as.integer)))
}

# sums the numbers in a vector
sum_in_loop <- function(nums) {
  return(sum(nums))
}

# driver code
main <- function() {
  data <- readLines("stdin")
  nums <- format_input(data)
  out <- sum_in_loop(nums)
  cat(out, "\n")
}

main()

# $ cat DATA.lst | Rscript nickkar.r
# 25645
