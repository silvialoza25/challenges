// ponyc -b asalgad2

use "collections"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail( array: Array[String]): Array[String] =>
          array.slice(1, array.size())

        fun sum( numbers: Array[String] ): I64 ? =>
          if numbers.size() > 0 then
            numbers(0)?.i64()? + sum( tail(numbers) )?
          else
            0
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")

          try
            var numbers: Array[String] = lines(1)?.split_by( " " )
            var result: I64 = sum( numbers )?
            env.out.print( result.string() )
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// 29980
