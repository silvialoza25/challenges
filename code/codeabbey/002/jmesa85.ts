/*
$ npx eslint jmesa85.ts
$ npx prettier --check jmesa85.ts
Checking formatting...
All matched files use Prettier code style!
*/

// CodeAbbey #002: Sum in Loop - Typescript

// Reducer
function getSum(acc: number, value: number): number {
  return acc + value;
}

function main(): number {
  // Read STDIN
  process.stdin.setEncoding('utf8');
  process.stdin.on('data', (rawData) => {
    // Get the test cases in the second line
    const [testCases] = rawData.toString().trim().split('\n').slice(1);
    // Process result
    const result = testCases.split(' ').map(Number).reduce(getSum);
    // Print result
    process.stdout.write(result.toString());
    return 0;
  });
  return 0;
}

main();

/*
$ tsc jmesa85.ts
$ cat DATA.lst | node jmesa85.js
30857
 */
