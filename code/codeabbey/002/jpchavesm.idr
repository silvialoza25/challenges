{-
  $ idris2 jpchavesm.idr -o jpchavesm
-}

module Main
import Data.Maybe
import Data.Strings
import Data.List

str2int : String -> Int
str2int elem = fromMaybe 0 $ (parseInteger {a=Int} elem)

main : IO ()
main = do
  size <- getLine
  strInput <- getLine
  let intArr = map str2int (words strInput)
  let sumArr = sum intArr
  putStrLn (cast sumArr)

{-
  $ cat DATA.lst | ./build/exec/jpchavesm
  30857
-}
