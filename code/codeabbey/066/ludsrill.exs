# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.08 seconds (0.01s to load, 0.07s running 55 checks on 1 file)
# 9 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule CaesarCipherCracker do

  def main do
    all_data = IO.read(:stdio, :all)
    prepositions = ['A', 'AS', 'AT', 'BY', 'IN', 'OF', 'ON', 'TO', 'UP', 'IT']
    try do
      get_data(all_data)
        |> Enum.map(&String.split(&1, " "))
        |> Enum.map(&to_char/1)
        |> Enum.map(&caesar_cipher_cracker(&1, prepositions, 1))
        |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def to_char(list) do
    Enum.map(list, &Kernel.to_charlist/1)
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
      |> Enum.reject(fn x -> x == "" end)
      |> tl()
  end

  def caesar_cipher_cracker(list, prepositions, count) do
    new_list = Enum.map(list, &make_shift(&1, count))
    aux = compare_lists(new_list, prepositions)

    if aux == "Retry" do
      caesar_cipher_cracker(list, prepositions, count + 1)
    else
      solution = new_list
                  |> Enum.slice(0..2)
                  |> Enum.map(&Kernel.to_charlist/1)
                  |> Enum.join(" ")
      solution <> " #{Integer.to_string(count)}"
    end
  end

  def make_shift(value, count) do
    Enum.map(value, fn x -> if x - count < 65 do x - count + 26 else x -
      count end end)
  end

  def compare_lists([], _), do: "Retry"
  def compare_lists([head | tail], prepositions) do
    if Enum.any?(prepositions, fn x -> head == x end) do
      "Cracked"
    else
      compare_lists(tail, prepositions)
    end
  end
end

CaesarCipherCracker.main()

# cat DATA.lst | elixir ludsrill.exs
# SOME OF HIM 11 CALLED IT THE 2 GREENFIELDS ARE GONE 9 NOR IRON BARS 24 IN
# ANCIENT PERSIA 21
