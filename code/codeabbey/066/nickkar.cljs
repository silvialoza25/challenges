;; $ clj-kondo --lint nickkar.cljs
;; linting took 93ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]
            [clojure.string :as str]
            [clojure.edn :as edn]))

;; relative frequency of english letters in a text
(def modl [8.1 1.5 2.8 4.3 13.0 2.2 2.0 6.1 7.0 0.15 0.77 7.0 2.4
           6.8 7.5 1.9 0.095 6.0 6.3 9.1 2.8 0.98 2.4 0.15 2.0 0.074])

;; counts the times a letter appears in a string
(defn count-letter [s c]
  (let [ch (re-pattern (str c))]
    (count (re-seq ch s))))

;; calculates the average of the elements of a numeric vector
(defn average [vec]
  (/ (apply + vec) (count vec)))

;; finds the relative frequency of every letter in the english alphabet [A-Z]
;; given a string
(defn find-freq
  ([s] (find-freq s [] 65))
  ([s freq i]
   (cond
     (<= i 90) (find-freq s (conj freq (count-letter s (char i))) (+ i 1))
     :else (map #(* % 100) (map #(/ % (apply + freq)) freq)))))

;; takes a string and shifts it using caesar cipher method
(defn caesar-shift [s shift]
  (str/replace s #"[ABCDEFGHIJKLMNOPQRSTUVWXYZ]"
               #(str (cond
                       (> (+ shift (int (.charAt %1 0))) 90)
                       (char (+ 64 (mod (+ shift (int (.charAt %1 0))) 90)))
                       :else
                       (char (+ shift (int (.charAt %1 0))))))))

;; finds most likely key of an caesar ciphered encrypted message
(defn findkey
  ([s] (findkey s 100 (find-freq s) 0 0))
  ([s difference freq key i]
   (let [newdif (average (map #(Math/pow % 2) (map - freq modl)))]
     (cond
       (> i 25) (+ 1 (- 25 key))
       (< newdif difference)
       (findkey s newdif (find-freq (caesar-shift s (+ i 1)) [] 65) i (+ i 1))
       :else
       (findkey s difference
                (find-freq (caesar-shift s (+ i 1)) [] 65)
                key (+ i 1))))))

(defn itera [input-size]
  (loop [x 1]
    (when (< x (+ input-size 1))
      (let [line (core/read-line)
            key (findkey line)
            out (take 3 (str/split (caesar-shift line (- 26 key)) #" "))]
        (print (str (nth out 0) " " (nth out 1) " " (nth out 2) " " key " ")))
      (recur (+ x 1)))))

(defn main []
  (let [input-size (edn/read-string (core/read-line))]
    (itera input-size)
    (println "")))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; ARE WONDERS MANY 2 WE CALL HER 15 SOME OF HIM 11 CARTHAGE MUST BE 20
;; IT CAN BE 19
