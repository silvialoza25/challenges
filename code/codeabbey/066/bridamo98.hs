{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.Map (fromListWith, toList)
import Data.List
import Data.Char
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  dat <- replicateM intSizeInput getLine
  let datWithoutSpaces = map (filter (/=' ')) dat
  let sizes = map length datWithoutSpaces
  let alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  let extendDat = map (++ alpha) dat
  let divDat = map words dat
  let frequencies = map frequency extendDat
  let percentFreq = calcPercentFreq frequencies sizes
  let idealFreq = [8.1, 1.5, 2.8, 4.3, 13.0, 2.2, 2.0, 6.1, 7.0, 0.15, 0.77,
                  7.0, 2.4, 6.8, 7.5, 1.9, 0.095, 6.0, 6.3, 9.1, 2.8, 0.98,
                  2.4, 0.15, 2.0, 0.074]
  let decodeKeys = findDecodeKeys idealFreq percentFreq
  let solution = buildSolution divDat decodeKeys
  putStrLn (unwords solution)

buildSolution :: [[String]] -> [Int] -> [String]
buildSolution [] [] = []
buildSolution dat keys = res
  where
    iDat = head dat
    iKey = head keys
    msg1 = decodeMsg (head iDat) iKey
    msg2 = decodeMsg (iDat !! 1) iKey
    msg3 = decodeMsg (iDat !! 2) iKey
    iRes = [msg1, msg2, msg3, show (26 - iKey)]
    res = iRes ++ buildSolution (drop 1 dat) (drop 1 keys)

decodeMsg :: String -> Int -> String
decodeMsg msg code = res
  where
    ascciNum = map fromEnum msg
    orgIndex = map (+(-65)) ascciNum
    indexPlusCode = map (+code) orgIndex
    newIndexes = map (`mod` 26) indexPlusCode
    newAssciNum = map (+65) newIndexes
    res = getDecodedMsg newAssciNum

getDecodedMsg :: [Int] -> String
getDecodedMsg [] = []
getDecodedMsg indexes = res
  where
    res = (toEnum (head indexes) :: Char) : getDecodedMsg (drop 1 indexes)

findDecodeKeys :: [Float] -> [[Float]] ->  [Int]
findDecodeKeys idealFreq [] = []
findDecodeKeys idealFreq percentFreq = res
  where
    ipercentFreq = head percentFreq
    iDecodeKey = getCaesarKey ipercentFreq idealFreq 999999999.0 1 0
    res = iDecodeKey : findDecodeKeys idealFreq (drop 1 percentFreq)

getCaesarKey :: [Float] -> [Float] -> Float -> Int -> Int -> Int
getCaesarKey real ideal minDiff curCaesarKey 26 = curCaesarKey
getCaesarKey real ideal minDiff curCaesarKey cont= res
  where
    iDiff = getDifference real ideal
    nextDiff = min iDiff minDiff
    nextKey = if iDiff < minDiff
                then cont
                else curCaesarKey
    nextReal = last real : take (length real - 1) real
    res = getCaesarKey nextReal ideal nextDiff nextKey (cont + 1)

frequency :: (Ord a) => [a] -> [(a, Float)]
frequency xs = drop 1 (toList (fromListWith (+) [(x, 1.0) | x <- xs]))

calcPercentFreq :: [[(a, Float)]] -> [Int] -> [[Float]]
calcPercentFreq [] [] = []
calcPercentFreq frequencies sizes = res
  where
    iFreq = head frequencies
    iSize = head sizes
    iAmountFreq = map snd iFreq
    iRealAmountFreq = map (+(-1)) iAmountFreq
    iFreq1 = map (*100.0) iRealAmountFreq
    iPercFreq = map (/(fromIntegral iSize :: Float)) iFreq1
    res = iPercFreq : calcPercentFreq (drop 1 frequencies) (drop 1 sizes)

getDifference :: [Float] -> [Float] -> Float
getDifference real ideal = res
  where
    diffOfValues = squerDiff real ideal
    res = sum diffOfValues / fromIntegral (length diffOfValues) :: Float

squerDiff :: [Float] -> [Float] -> [Float]
squerDiff = zipWith (\ p q -> (p - q) ** 2)

{-
$ cat DATA.lst | ./bridamo98
  TO US IN 18 THAT ALL MEN 23 WE CALL HER 9 WHEN MY GUITAR 11 IF BLOOD BE 1
-}
