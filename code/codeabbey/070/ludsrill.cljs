;; clj-kondo --lint ludsrill.cljs
;; linting took 28ms, errors: 0, warnings: 0

(ns ludsrill.070
    (:gen-class)
    (:require [clojure.core :as core])
    (:require [clojure.edn :as edn]))

(defn linear-congruential-generator [Xcur]
  (let [constant-A 445N
        constant-C 700001N
        module 2097152N]
    (mod (+ (* constant-A Xcur) constant-C) module)))

(defn random-generator [seed]
  (loop [counter 0
         word ""
         random seed]
    (let [consonant ["b" "c" "d" "f" "g" "h" "j" "k" "l" "m" "n" "p" "r" "s"
            "t" "v" "w" "x" "z"]
          vowel ["a" "e" "i" "o" "u"]]
      (if (= counter 6)
        [word random]
        (let [new-random (linear-congruential-generator random)]
          (if (even? counter)
            (recur (inc counter) (str word (nth consonant (mod new-random 19)))
              new-random)
            (recur (inc counter) (str word (nth vowel (mod new-random 5)))
              new-random)))))))

(defn create-word-vec [seed]
  (loop [counter 900000
         word-vec []
         random seed]
    (if (zero? counter)
      word-vec
      (let [[word new-random] (random-generator random)]
        (recur (dec counter) (conj word-vec word) new-random)))))

(defn -main []
  (let [data (edn/read-string (core/read-line))
        words (frequencies (create-word-vec data))]
        (print (first (first (sort-by second > (seq words)))))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; zewira
