{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Data.Map (fromListWith, toList)
import Data.Ord
import Control.Monad

main = do
  dat <- getLine
  let x0 = read dat :: Int
  let n = 900000
  let intDat2 = replicate n 6
  let indexesAux = getRandNumbers 445 700001 2097152 (sum intDat2) x0
  let indexes = getIndexes indexesAux intDat2
  let vowels = "aeiou"
  let consonants = "bcdfghjklmnprstvwxz"
  let solution = getWords indexes consonants vowels
  let repetitions = countRepetitions solution
  let max = [fst (maximumBy (comparing snd) repetitions)]
  putStrLn (unwords max)

getWords :: [[Int]] -> String -> String -> [String]
getWords [] consonants vowels = []
getWords indexes consonants vowels = res
  where
    indexI = head indexes
    resi = buildWord indexI consonants vowels 1
    res = resi : getWords (drop 1 indexes) consonants vowels

buildWord :: [Int] -> String -> String -> Int -> String
buildWord [] consonants vowels n = []
buildWord indexI consonants vowels n = res
  where
    index = head indexI
    letter = if n `mod` 2 == 0
               then vowels !! index
               else consonants !! index
    res = letter : buildWord (drop 1 indexI) consonants vowels (n + 1)

getIndexes :: [Int] -> [Int] -> [[Int]]
getIndexes randNumbers [] = []
getIndexes randNumbers lengths = res
  where
    ilength = head lengths
    resi = randToIndex (take ilength randNumbers) 19
    res = resi : getIndexes (drop ilength randNumbers) (drop 1 lengths)

randToIndex :: [Int] -> Int -> [Int]
randToIndex [] modOp = []
randToIndex rand modOp = res
  where
    resi = head rand `mod` modOp
    nextModOp = if modOp == 19
                then 5
                else 19
    res = resi : randToIndex (drop 1 rand) nextModOp

getRandNumbers :: Int -> Int -> Int -> Int -> Int -> [Int]
getRandNumbers a c m 0 xCur = []
getRandNumbers a c m n xCur = res
  where
    xNext = (a * xCur + c) `mod` m
    res = xNext : getRandNumbers a c m (n - 1) xNext

countRepetitions :: (Ord a) => [a] -> [(a, Int)]
countRepetitions repe = toList (fromListWith (+) [(item, 1) | item <- repe])

{-
$ cat DATA.lst | ./bridamo98
  ciwelo
-}
