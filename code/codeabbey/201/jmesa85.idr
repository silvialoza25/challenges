{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #201: Point in Polygon - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Nat
import Data.Strings

%default partial

stoi : String -> Int
stoi str = fromMaybe 0 $ parseInteger {a=Int} str

elemAt : Int -> List a -> a
elemAt 0 (x::xs) = x
elemAt index (_::xs) = elemAt (index - 1) xs

getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

-- For a given polygon, transforms a list of points into a list of edges
getPolyEdges : List(List Int) -> List(List(List(Int)))
getPolyEdges (point1::point2::ps) =
  [point1,point2] :: getPolyEdges (point2::ps)
getPolyEdges _ = []

-- Given three colinear points p, q, r,
-- checks if point 'q' lies on line segment 'pr'
isOnSegment : List Int -> List Int -> List Int -> Bool
isOnSegment [pxpos, pypos] [qxpos, qypos] [rxpos, rypos] =
  qxpos <= max pxpos rxpos &&
  qxpos >= min pxpos rxpos &&
  qypos <= max pypos rypos &&
  qypos >= min pypos rypos

-- Finds the orientation of ordered triplet (p, q, r)
-- The function returns following values
-- 0 --> p, q and r are colinear
-- 1 --> Clockwise
-- 2 --> Counterclockwise
getOrientation : List Int -> List Int -> List Int -> Int
getOrientation [pxpos, pypos] [qxpos, qypos] [rxpos, rypos] = let
  orient =
    (qypos - pypos) * (rxpos - qxpos) - (qxpos - pxpos) * (rypos - qypos) in
  if orient == 0 then 0 else if orient > 0 then 1 else 2

-- The function that returns true if line segment 'p1q1' and 'p2q2' intersect
doIntersect : List Int -> List Int -> List Int -> List Int -> Bool
doIntersect po_p1 po_q1 po_p2 po_q2 = let
  orient1 = getOrientation po_p1 po_q1 po_p2
  orient2 = getOrientation po_p1 po_q1 po_q2
  orient3 = getOrientation po_p2 po_q2 po_p1
  orient4 = getOrientation po_p2 po_q2 po_q1
  in (orient1 /= orient2 && orient3 /= orient4)
    || (orient1 == 0 && isOnSegment po_p1 po_p2 po_q1)
    || (orient2 == 0 && isOnSegment po_p1 po_q2 po_q1)
    || (orient3 == 0 && isOnSegment po_p2 po_p1 po_q2)
    || (orient4 == 0 && isOnSegment po_p2 po_q1 po_q2)

-- True if the proyection of this point crosses the given edge segment
crossPointEdge : List Int -> List(List Int) -> Bool
crossPointEdge [posX, posY] [edge_p1, edge_p2] =
  let MAX_WIDTH = 10000 in -- Given by the problem context
  doIntersect [posX, posY] [MAX_WIDTH, posY] edge_p1 edge_p2

-- Returns how many edges the proyection of the point crosses
countCrossings : List Int -> List(List(List Int)) -> Nat
countCrossings point edges =
  length $ filter (==True) (map (\edge => crossPointEdge point edge) edges)

-- Ray crossing method
-- Point : List Int = [x, y]
-- Edge : List(List Int) = [[x1, y1], [x2, y2]]
isPointInside : List Int -> List(List(List Int)) -> Bool
isPointInside point edges = modNat (countCrossings point edges) 2 /= 0

main : IO()
main = do
  -- Read Stdin
  inputData <- getLines
  let
    -- Parse all lines from strings to Int
    inputLines = map (map stoi) $ map words inputData
    -- Filter the lines with the number of points
    counts = filter (\x => (length x) == 1) inputLines
    -- Get the number of points that compose the polygon
    nPolyPoints = elemAt 0 $ elemAt 0 counts
    -- Get the polygon points
    polyInputData = take (integerToNat (cast nPolyPoints)) $ drop 1 inputLines
    -- Get the test cases (points)
    testPointsData = drop ((integerToNat (cast nPolyPoints)) + 2) inputLines
    -- Get the polygon edges
    edges = getPolyEdges polyInputData
    -- Calculate ray crossings for all test points
    crossings = map (\point => isPointInside point edges) testPointsData
    -- Format the results filtering the indexes
    indexed = zip crossings [1..(length crossings)]
    results =
      map (\(_,index) => index) $ filter (\(cond,_) => cond == True) indexed
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  5 9 20 21 22 34 41 45 49 56 58 60 64 77 82
-}
