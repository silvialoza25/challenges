{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

elemAt : Nat -> List a -> a
elemAt 0 (x::xs) = x
elemAt index (_::xs) = elemAt (minus index 1) xs

rmdups : Eq a => List a -> List a
rmdups [] = []
rmdups (x::xs) =
  let
    res =
      if x `elem` xs
        then rmdups xs
        else x :: rmdups xs
  in res

sortComp : (Char, Nat) -> (Char, Nat) -> Ordering
sortComp fstItem sndItem =
  let
    (fstChar,fstCount) = fstItem
    (sndChar,sndCount) = sndItem
    res =
      if fstCount < sndCount
        then GT
        else if fstCount > sndCount
          then LT
          else if fstChar > sndChar
            then GT
            else LT
  in res

insertLastPart : List (List Char, Nat) -> (List Char, Nat)
  -> Nat -> Nat -> List (List Char, Nat)
insertLastPart fstPart lastPart count 0 = [lastPart] ++ fstPart
insertLastPart fstPart lastPart count temIndex =
  let
    index = minus temIndex 1
    (_,curCount) = elemAt index fstPart
    res =
      if curCount >= count
        then (take (index + 1) fstPart) ++ [lastPart]
          ++ (drop (index + 1) fstPart)
        else insertLastPart fstPart lastPart count (minus temIndex 1)
  in res

initHuffmanCoding : (Char, Nat) -> (List Char, Nat)
initHuffmanCoding repetition =
  let
    (char, count) = repetition
  in ([char], count)

joinStates : (List Char, Nat) -> (List Char, Nat) -> (List Char, Nat)
joinStates penUltimate last =
  let
    (fstChars, fstCount) = penUltimate
    (sndChars, sndCount) = last
  in (fstChars ++ sndChars, fstCount + sndCount)

joinHuffman : List (List Char, Nat) -> List Char -> List Char
joinHuffman initHuffStates res =
  let
    size = length initHuffStates
    res =
      if size == 2
        then let
          (chars, count) = joinStates (elemAt 0 initHuffStates)
            (elemAt 1 initHuffStates)
        in res ++ chars
        else let
          fstPart = take (minus size 2) initHuffStates
          penUltimate = elemAt (minus size 2) initHuffStates
          last = elemAt (minus size 1) initHuffStates
          (chars, count) = joinStates penUltimate last
          nxtStates =  insertLastPart fstPart (chars, count)
            count (minus size 2)
        in joinHuffman nxtStates (res ++ chars)
  in res

findCompressionRatio : List (Char, Nat) -> List (Char, Nat)
  -> Nat -> Double -> Double
findCompressionRatio [] [] curRes size =
  (size * 8.0) / (cast {to=Double} curRes)
findCompressionRatio counters lengthCodes curRes size =
  let
    (_,counter) = fromMaybe (' ',0) (head' counters)
    (_,lengthCode) = fromMaybe (' ',0) (head' lengthCodes)
  in findCompressionRatio (drop 1 counters) (drop 1 lengthCodes)
    (curRes + (counter * lengthCode)) size

main : IO ()
main = do
  inputData <- getLine
  let
    formatMsj = unpack inputData
    uniqueMsj = rmdups formatMsj
    tempRepe = map (\x => (x,(length (filter (==x) formatMsj))))
      uniqueMsj
    repetitions = sortBy (sortComp) tempRepe
    initHuffStates = map initHuffmanCoding repetitions
    tempCodes = joinHuffman initHuffStates []
    lengthCodes = sort
      (map (\x => (x,(length (filter (==x) tempCodes)))) uniqueMsj)
    solution = findCompressionRatio (sort tempRepe)
      lengthCodes 0 (cast {to=Double} (length formatMsj))
  printLn solution

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  1.8537336412625096
-}
