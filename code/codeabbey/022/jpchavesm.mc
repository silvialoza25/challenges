/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, float, char.

:- pred float2int(string::in, float::out) is det.
float2int(X, string.det_to_float(X)).

:- func calcMin(float, float, float) = int.
calcMin(PrinterX, PrinterY, TotalPages) = MinTime :-
(
  MinTime = min(
    ceiling_to_int(max(PrinterX,PrinterY) * TotalPages / (PrinterX + PrinterY))
      * round_to_int(min(PrinterX, PrinterY)),
    ceiling_to_int(min(PrinterX,PrinterY) * TotalPages / (PrinterX + PrinterY))
      * round_to_int(max(PrinterX, PrinterY))
  )
).

:- pred findMins(list(string), string, string).
:- mode findMins(in, in, out) is det.
findMins([], FinalStr, strip(FinalStr)).
findMins([Line | Tail], PartialStr, FinalStr) :-
(
  words_separator(char.is_whitespace, Line) = InputListStr,
  map(float2int, InputListStr, InputList),
  det_index0(InputList, 0, PrinterX),
  det_index0(InputList, 1, PrinterY),
  det_index0(InputList, 2, TotalPages),
  calcMin(PrinterX, PrinterY, TotalPages) = MinTime,
  PartialStr1 = PartialStr ++ " " ++ from_int(MinTime),
  findMins(Tail, PartialStr1, FinalStr)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1, FileContents, InputList),
    findMins(InputList, "", Mins),
    io.print_line(Mins, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  19371330 130063734 235367520 168347904 71055962 129278957
  339813300 226320532 343414323 136872554 407003691 191135798
  422279840 96854667 216196224 94798524 101240082 290952864
  312747560 189324891
*/
