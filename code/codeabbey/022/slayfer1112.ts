/*
$ eslint slayfer1112.ts
$ tsc slayfer1112.ts
*/

function printers(printerA: number, printerB: number, pages: number): number {
  const printsA: number =
    Math.ceil((printerA / (printerA + printerB)) * pages) * printerB;
  const printsB: number =
    Math.ceil((printerB / (printerA + printerB)) * pages) * printerA;
  if (printsA < printsB) {
    return printsA;
  }
  return printsB;
}

function solution(entry: string): number {
  const dat: number[] = entry.split(' ').map((value) => Number(value));
  const [printerA, printerB, pages]: number[] = dat;
  process.stdout.write(`${printers(printerA, printerB, pages)} `);
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(1, entryArray.length);
    dat.map((value) => solution(value));
    process.stdout.write('\n');
    return 0;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
263758430 237548096 379877502 197944 211802745 193536936
349935036 252645018 290500263 39628224 355061139 275607571
356903610 25905942 248863520 173586819 140773700 335585812
236054268 377929433
*/
