#!/usr/bin/php
<?php
/*
$ ./vendor/bin/phplint edwinhaq.php --no-configuration --no-cache
phplint 2.0.2 by overtrue and contributors.

No config file loaded.

.

Time: < 1 sec   Memory: 2.0 MiB Cache: No

OK! (Files: 1, Success: 1)
*/

function reduce_print_time(
  $target_time,
  $fast_printer,
  $slow_printer,
  $total_pages,
  $step
) {
  $time_fast_printer = $fast_printer * ($total_pages - $step);
  $time_slow_printer = $step * $slow_printer;
  $max_time = max($time_fast_printer, $time_slow_printer);
  if ($max_time <= $target_time) {
    return reduce_print_time(
      $max_time,
      $fast_printer,
      $slow_printer,
      $total_pages,
      $step + 1,
    );
  } else {
    return $target_time;
  }
}

function two_printers($printer_x, $printer_y, $total_pages) {
  $slow_printer = max($printer_x, $printer_y);
  $fast_printer = min($printer_x, $printer_y);
  if ($slow_printer % $fast_printer == 0) {
    $proportion = floor($slow_printer / $fast_printer);
    return ceil(
      ($total_pages * $fast_printer * $proportion) / (1 + $proportion),
    );
  }
  $time_fast_printer_only = $fast_printer * $total_pages;
  if ($time_fast_printer_only < $slow_printer) {
    return $time_fast_printer_only;
  }
  $first_step = floor($time_fast_printer_only / ($printer_x + $printer_y));
  return reduce_print_time(
    $time_fast_printer_only,
    $fast_printer,
    $slow_printer,
    $total_pages,
    $first_step,
  );
}

foreach (range(1, trim(fgets(STDIN))) as $i) {
  [$x, $y, $n] = explode(' ', trim(fgets(STDIN)));
  print two_printers($x, $y, $n) . ' ';
}

/*
$ cat DATA.lst | ./edwinhaq.php
370738206 83912328 388981932 267566478 55680042 153034020 3407454 285256374
99058230 319148905 327668880 70680924 56497596 246442077 447160920 49332528
125006184
*/


?>
