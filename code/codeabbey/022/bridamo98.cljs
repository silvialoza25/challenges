;; $ clj-kondo --lint bridamo98.cljs
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-018
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn find-min-time [x y n]
  (let [min-val (core/min x y) max-val (core/max x y)]
    (loop [min-res (* n min-val) max-res 0]
      (if (< min-res max-res)
        (+ min-res min-val)
        (recur (- min-res min-val) (+ max-res max-val))))))

(defn solve-all[size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (let [ args (str/split (core/read-line) #" ")]
          (recur (+ i 1)
          (str result (find-min-time (edn/read-string (args 0))
          (edn/read-string (args 1)) (edn/read-string (args 2))) " ")))
          result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 370738206 83912328 388981932 267566478 55680042 153034020 3407454 285256374
;; 99058230 319148905 327668880 70680924 56497596 246442077 447160920 49332528
;; 125006184
