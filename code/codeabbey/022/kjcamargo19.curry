-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import Float (truncate)
import IO (getContents)
import List (split, init)

getData :: IO String
getData = do
  content <- getContents
  return content

strToF :: String -> Float
strToF text = read text :: Float

ceiling :: Float -> Float
ceiling number = intPart
  where
    sep = split (=='.') (show number)
    intPart = 1 + strToF (sep !! 0)

minimumTime :: Float -> Float -> Float -> Float
minimumTime printerA printerB pages = result
  where
    timeA =  printerB * ceiling (pages * printerA / (printerA + printerB))
    timeB =  printerA * ceiling (pages * printerB / (printerA + printerB))
    result = min timeA timeB

getSolution :: String -> Int
getSolution dataIn = result
  where
    finalData = map strToF (words dataIn)
    printerA = finalData !! 0
    printerB = finalData !! 1
    npages = finalData !! 2
    result = truncate $ minimumTime printerA printerB npages

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> show (getSolution x)) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 340794288 299607282 243826992 119636082 76060959 313658508 20027907
-- 273250960 320090870 415758649 439063864 44471840 303827916 460244889
-- 333600828 57428712 430523140 297156086 266199332
