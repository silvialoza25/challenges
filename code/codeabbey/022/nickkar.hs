-- $ ghc -o nickkar nickkar.hs
-- [1 of 1] Compiling Main ( nickkar.hs, nickkar.o )
-- Linking code ...
-- $ hlint nickkar.hs
-- No hints

import Control.Monad ( replicateM, forM )
import Text.Printf

convert::String -> [Int]
convert = map read . words

checksteps:: Int -> Int -> Int -> Int -> Int -> Int -> Int
checksteps step time x y px py
    | max (x*(px+step)) (y*(py-step)) < time
    = checksteps
    newstep
    (max (x*(px+step)) (y*(py-step)))
    x y
    (px+step) (py-step)

    | max (x*(px-step)) (y*(py+step)) < time
    = checksteps newstep
    (max (x*(px-step)) (y*(py+step)))
    x y
    (px-step) (py+step)

    | step > 1 = checksteps newstep (max (x*px) (y*py)) x y px py

    | otherwise = time

    where
        newstep = floor ((fromIntegral step::Double)/2)

main::IO()
main = do
    n <- readLn :: IO Int
    inputs <- replicateM n getLine
    ca5es <- forM inputs (return . convert)
    sol <- forM ca5es (\a ->
        return(checksteps ((a!!2)-1) 2147483647 (head a) (a!!1) 0 (a!!2))
        )
    mapM_ (printf "%d ") sol
    putStr "\n"

-- $ cat DATA.lst | ./nickkar
-- 263758430 237548096 379877502 197946 211802745 193536936 349935036
-- 252645018 290500263 39628224 355061139 275607571 356903610 25905942
-- 248863520 173586826 140773700 335585812 236054268 377929433
