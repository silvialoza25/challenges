{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #022: Two Printers - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stod : String -> Double
stod str = fromMaybe 0 $ parseDouble str

-- Gets lines in the Stdin
getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

processTestCase : List Double -> Int
processTestCase [timeX, timeY, pages] =
  let
    pagesX = timeY * ceiling (pages * timeX / (timeX + timeY))
    pagesY = timeX * ceiling (pages * timeY / (timeX + timeY))
  in
    cast $ min pagesX pagesY

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse test cases
    testCases = map (map stod) $ map words inputData
    -- Process data
    results = map processTestCase testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  19371330 130063734 235367520 168347904 71055962 129278957 339813300
  226320532 343414323 136872554 407003691 191135798 422279840 96854667
  216196224 94798524 101240082 290952864 312747560 189324891
-}
