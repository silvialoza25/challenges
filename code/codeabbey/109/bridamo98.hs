{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}
import Data.Map (fromListWith, toList)
import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let intInputData = map convert inputData
  let solution = map findSolution intInputData
  putStrLn (unwords solution)

findSolution :: [Int] -> String
findSolution dat = res
  where
    combinations = makeCombinations dat
    repetitions = getRepetitions combinations
    countToSquare = map (^2) repetitions
    resInt = if head dat `mod` 2 /= 0 && head dat /= 1
               then sum countToSquare * (dat !! 1)
               else sum countToSquare
    res = show resInt

getRepetitions :: [[Int]] -> [Int]
getRepetitions dat = res
  where
    sums = map sum dat
    repetitions = countRepetitions sums
    res = map snd repetitions

makeCombinations :: [Int] -> [[Int]]
makeCombinations dat = res
  where
    n = head dat
    b = dat !! 1
    sizeCombination = if n == 1
                        then n
                        else n `div` 2
    res = replicateM sizeCombination [0..b-1]

countRepetitions :: (Ord a) => [a] -> [(a, Int)]
countRepetitions repe = toList (fromListWith (+) [(item, 1) | item <- repe])

convert :: String -> [Int]
convert dat = res
  where
    splitedData = words dat
    res = map read splitedData :: [Int]

{-
$ cat DATA.lst | ./bridamo98
  2228154512 169 869042856 2062595840 369512 6 9 36 3
  29632604816 2320 709120972 8 30162301 35751527189
-}
