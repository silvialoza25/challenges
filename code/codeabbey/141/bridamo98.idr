{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

fromIntegerNat : Int -> Nat
fromIntegerNat 0 = Z
fromIntegerNat number =
  if (number > 0) then
    S (fromIntegerNat (assert_smaller number (number - 1)))
  else
    Z

toIntegerNat : Nat -> Int
toIntegerNat Z = 0
toIntegerNat (S k) = 1 + toIntegerNat k

elemAt : Int -> List a -> a
elemAt 0 (x::xs) = x
elemAt index (_::xs) = elemAt (index - 1) xs

injectValue : a -> IO a
injectValue dat = pure dat

getInputData : String -> IO(String)
getInputData lastLine = do
  line <- getLine
  if line == "" && lastLine == ""
    then injectValue ""
    else do
      xs <- getInputData line
      injectValue (line ++ " ~" ++ xs)

helper : Int -> String
helper 0 = ""
helper number =
  let
    res =
      if number `mod` 2 == 1
        then "1" ++ helper (number `div` 2)
        else "0" ++ helper (number `div` 2)
  in res

decToBin : Int -> String
decToBin 0 = "0"
decToBin number = reverse (helper number)

binToDec : Int -> Int
binToDec 0 = 0
binToDec binNum = 2 * binToDec (div binNum 10) + (mod binNum 10)

getHexLet : Int -> String
getHexLet dec =
  if dec < 10
    then show dec
    else case dec of
      10 => "A"
      11 => "B"
      12 => "C"
      13 => "D"
      14 => "E"
      15 => "F"

getHex : List Char -> String
getHex bin =
  let
    dec = binToDec (cast {to=Int} (pack bin))
    res = getHexLet dec
  in res

formatSol : Int -> Int -> String
formatSol offset size =
  let
    binOff = decToBin offset
    binSize = (length binOff)
    fullBin = (replicate (minus 12 binSize) '0') ++ (unpack binOff)
    fstPart = getHex (take 4 fullBin)
    sndPart = getHex (take 4 (drop 4 fullBin))
    thrPart = getHex (drop 8 fullBin)
    res = (getHexLet size) ++ fstPart ++ sndPart ++ thrPart
  in res

getIndex : List Char -> List Char -> Nat -> Nat -> Nat
getIndex text fragment size 0 = 0
getIndex text fragment size curIndex =
  let
    actIndex = (minus curIndex 1)
    txtFrag = drop actIndex (take (actIndex + size) text)
    res =
      if txtFrag == fragment
        then curIndex
        else getIndex text fragment size (minus curIndex 1)
  in res

findOffAndSize : List Char -> Nat -> List Char -> (Nat, Nat)
findOffAndSize text txtSize [] = (0, 0)
findOffAndSize text txtSize fragment =
  let
    fragSize = length fragment
    posIndex = getIndex text fragment fragSize (minus (txtSize + 1) fragSize)
    res =
      if posIndex == 0
        then findOffAndSize text txtSize (take (minus fragSize 1) fragment)
        else ((minus txtSize posIndex), fragSize)
  in res

solveAll : List Int -> List Char -> List String
solveAll [] divText = []
solveAll indexes divText =
  let
    index = fromIntegerNat (fromMaybe 0 (head' indexes))
    frag = (take 15 (drop index divText))
    searchSpace = (take 4096 (drop (minus index 4096) divText))
    (offset, size) = findOffAndSize searchSpace (length searchSpace) frag
    res = formatSol (toIntegerNat offset) (toIntegerNat size)
  in res :: solveAll (drop 1 indexes) divText

main : IO ()
main = do
  sizeInput <- getLine
  indexes <- getLine
  text <- getInputData " "
  let
    intIndexes = map (cast {to=Int}) (words indexes)
    divText = (unpack text)
    solution = solveAll intIndexes divText
  putStrLn (unwords solution)

{-
  $ cat DATA.lst doyle.txt | ./build/exec/bridamo98
  540D 201D 23C9 1007 2779 5669 2940 46FA 75E7 43BD 49BA 40C9 3270 878A
  100D C01C 1088 472D 1007 7194 20E5 301E 31F0 2018 4C51 4B24 51B1 20A2
  41EE 7366 3E0A 5014 22F1 77B1 346F 316E 23A5 4C8A 403D 4D10 D2EE 45B4
-}
