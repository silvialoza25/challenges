/**
  $ haxelib run checkstyle \
    --exitcode \
    --show-parser-errors \
    --nothreads \
    --default-config \
    -s jmesa85.hx
**/

// CodeAbbey #141: Sliding Window Search - Haxe

using StringTools;
using Lambda;

class Main {
  static public function main():Void {
    // Read Stdin, DATA.lst and doyle.txt concatenated
    var data = Sys.stdin().readAll().toString();
    // Get test cases
    var testCases = data.split("\n")[1].split(" ").map(Std.parseInt);
    // Get text
    var doyleTxt = data.substring(data.indexOf('T'));
    // Process test cases
    var results = testCases.map(
      function(item) return processTestCase(item, doyleTxt));
    // Print results
    trace(results.join(" "));
  }

  static private function processTestCase(index:Int, text:String):String {
    // Prepare comparison window
    var window = text.substring(index, index + 15);
    // Prepare 4096 bytes block
    var preceedingBlock = text.substring(index - 4096, index);
    // Calculate sliding window
    var output:Array<Int> = sliding(window, preceedingBlock);
    // Format output
    var hexResult = StringTools.hex(output[0] * 4096 + output[1], 4);
    return hexResult;
  }

  static private function sliding(window:String, prec:String):Array<Int> {
    // Base case
    if (window.length == 0) {
      // NOT found!
      return [0, 0];
    }
    // Base case
    var auxIndex = prec.lastIndexOf(window);
    if (auxIndex != -1) {
      // Found!
      return [window.length, 4095 - auxIndex];
    }
    // Recursive case
    return sliding(window.substring(0, window.length - 1), prec);
  }
}

/**
  $ cp jmesa85.hx Main.hx && cat DATA.lst doyle.txt | haxe -main Main --interp
  2049 3037 6075 1018 35FC 3114 3CB7 F171 6B1C 65AA 54A0 2164 7507 25E5 304A
  36F0 4C95 30C8 6074 390B A07D 304A 692C 20F9 3616 2F7F 301F 36C4 4AEC 5727
  304C 78BD 7E9B 3FFF 1015 35A5 3018 4286 3206 50BF 3984 8157 2041 7547 73A5
  2B9B 40AA 5659 739D 300B
**/
