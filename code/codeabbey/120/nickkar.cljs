;; $ clj-kondo --lint nickkar.cljs
;; linting took 43ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(defn read-vector ([input-size] (read-vector 0 input-size []))
  ([i input-size vec]
   (cond
     (>= i input-size) vec
     :else (read-vector (inc i) input-size (conj vec (core/read))))))

(defn index-bubblesort
  ([v] (index-bubblesort v []))
  ([v out]
   (let [argmax (first (apply max-key second (map-indexed vector v)))
         maxval (nth v argmax)
         lastarg (dec (count v))
         lastval (nth v lastarg)]
     (cond
       (= (count v) 1) out
       :else
       (let [swapped (assoc (assoc v argmax lastval) lastarg maxval)
             removed (remove #(= maxval %) swapped)]
         (index-bubblesort (vec removed) (conj out argmax)))
       )
     )))

(defn main []
  (let [input-size (core/read)
        values (read-vector input-size)]
    (apply println (index-bubblesort values))))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 126 107 114 117 17 91 60 30 50 42 97 41 80 4 1 90 11 86 107 65 48 31 42 64
;; 16 2 103 33 85 47 73 77 40 34 81 23 3 43 55 78 64 90 23 60 77 72 22 15 37
;; 29 46 4 31 66 8 2 4 31 24 13 64 41 11 10 65 21 48 53 2 36 17 0 51 15 56 21
;; 55 34 30 6 11 11 34 32 11 37 6 9 10 20 23 32 31 15 22 5 30 10 20 17 15 22
;; 13 10 8 16 19 22 5 0 18 16 2 2 2 9 14 3 6 3 7 0 7 2 3 4 5 3 2 1 0
