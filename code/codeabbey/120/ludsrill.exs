# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.08 seconds (0.02s to load, 0.06s running 55 checks on 1 file)
# 8 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule SelectionSort do

  def main do
    data = to_int(String.split(hd(Enum.slice(String.split(
            IO.read(:stdio, :all), "\n"), 1..1)), " "))
    sort_list(data, length(data))
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def get_subsequence(data, counter) do
    Enum.slice(data, 0..counter - 1)
  end

  def maximum(subsequence) do
    Enum.max(subsequence)
  end

  def max_position(data, maximum) do
    Enum.find_index(data, fn x -> x == maximum end)
  end

  def modify_list(data, subsequence) do
    IO.write("#{max_position(data, maximum(subsequence))} ")
    List.replace_at(data, max_position(data, maximum(subsequence)),
      List.last(subsequence))
      |> List.replace_at(Enum.count(subsequence) - 1, maximum(subsequence))
  end

  def sort_list(data, counter) do
    if counter != 1 do
      sort_list(modify_list(data, get_subsequence(data, counter)), counter - 1)
    end
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e SelectionSort.main
# 12 2 78 99 24 62 77 23 48 46 95 41 102 80 79 64 26 61 73 44 92 82 13 76 46 32
# 12 93 3 39 58 48 23 48 76 81 44 39 34 38 0 27 77 28 70 53 26 58 42 25 25 39
# 22 59 8 22 42 47 11 20 8 50 20 17 5 46 12 34 16 4 48 13 6 27 37 4 24 15 14 18
# 2 6 2 37 27 34 32 19 22 9 11 16 14 8 26 4 19 3 0 3 17 2 5 9 8 1 12 13 6 7 9 7
# 2 7 1 4 4 0 1 1
