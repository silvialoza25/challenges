{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

fromIntegerNat : Int -> Nat
fromIntegerNat 0 = Z
fromIntegerNat number =
  if (number > 0) then
    S (fromIntegerNat (assert_smaller number (number - 1)))
  else
    Z

getChar : Int -> String
getChar code =
  if code == 1
    then "+"
    else "-"

calcBfDat : Double -> String
calcBfDat number =
  let
    leftSide = concat (map getChar (replicate ((fromIntegerNat
      (cast {to=Int} (number / 10.0))) + 1) 1))
    rightSide = concat (map getChar (replicate (fromIntegerNat
      (10 - (mod (cast {to=Int} number) 10))) 0))
    staticCodes = "[>++++++++++<-]>"
    ending = ":"
  in leftSide ++ staticCodes ++ rightSide ++ ending

main : IO ()
main = do
  inputData <- getLine
  let number = cast {to=Double} inputData
  let solution = [calcBfDat number]
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  ++++++[>++++++++++<-]>--------:
-}
