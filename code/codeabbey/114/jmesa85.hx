/**
  $ haxelib run checkstyle \
    --exitcode \
    --show-parser-errors \
    --nothreads \
    --default-config \
    -s jmesa85.hx
**/

// CodeAbbey #114: Tricky Printing - Haxe
// Note: This code generates dynamically the BrainFuck for the problem
// The output string is the solution for CodeAbbey

using StringTools;

class Main {
  static public function main():Void {
    var data = Std.parseInt(Sys.stdin().readAll().toString().trim());
    trace(getBrainFuckCode(data));
  }

  static private function getMinusChain(chain:String, counter:Int) {
    if (counter == 0)
      return chain;
    return getMinusChain(chain + '-', counter - 1);
  }

  static private function getPlusChain(chain:String, counter:Int) {
    if (counter == 0)
      return chain;
    return getPlusChain(chain + '+', counter - 1);
  }

  static private function getBrainFuckCode(value:Int):String {
    var tens:Int = Std.int(value / 10) + 1;
    var units:Int = 10 - (value % 10);
    var tensStr = getPlusChain('', tens);
    var unitsStr = getMinusChain('', units);
    return tensStr + '[>++++++++++<-]>' + unitsStr + ':';
  }
}

/**
  $ cp jmesa85.hx Main.hx && cat DATA.lst | haxe -main Main --interp
  +++++++++[>++++++++++<-]>----:
**/
