#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun factorial (number)
  (if (= number 1)
    1
    (* number (factorial (- number 1)))))

(defun calc-catalan-number(pairs-number)
  (floor (factorial (* 2 pairs-number))
    (* (expt (factorial pairs-number) 2) (+ pairs-number 1))))

(defun main()
  (let ((pairs-number (read)))
    (format t "~a~%" (calc-catalan-number pairs-number))))

(main)

#|
  cat DATA.lst | clisp bridamo98.lsp
  44803055521788070241614228618239281078485436639636641483691
  10646782012035727771549117085363233955617956677998857957259
  31752019190209472924174023323727648201259251349784015182722
  854558000
|#
