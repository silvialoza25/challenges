// dartanalyzer ludsrill.dart
// Analyzing ludsrill.dart...
// No issues found!

import 'dart:core';
import 'dart:io';

void main() {
  int r = int.parse(stdin.readLineSync());
  int n = r * 2;
  BigInt combinations =
      factorial(n) ~/ (factorial(r) * factorial(r) * BigInt.from(r + 1));
  print(combinations);
}

BigInt factorial(int value) {
  if(value == 0 || value == 1) {
    return BigInt.from(1);
  } else{
    return factorial(value - 1) * BigInt.from(value);
  }
}

// cat DATA.lst | dart ludsrill.dart
// 1177550288986571666095246266241237334514945364691269432506318857782339325442
// 36388383163526404966100061326115048166465508703008638514112580311200
