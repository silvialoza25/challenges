###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #140: Proper Bracket Sequences - Coffeescript
# Based on "Catalan number" in combinatorics

factorial = (x) ->
  if x is BigInt(0) then BigInt(1)
  else BigInt(factorial(x - BigInt(1)) * x)

getCatalanNumber = (n) ->
  factorial(BigInt(2) * n) / (factorial(n + BigInt(1)) * factorial(n))

main = ->
  # Read STDIN
  data = require('fs').readFileSync(0).toString().trim()
  # Format input number
  inputNumber = BigInt(data)
  # Process each test case
  result = getCatalanNumber(inputNumber)
  # Print result
  console.log(result.toString())

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
117755028898657166609524626624123733451494536469126943250631885778
233932544236388383163526404966100061326115048166465508703008638514112580311200
###
