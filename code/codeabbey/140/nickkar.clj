;; $clj-kondo --lint nickkar.clj
;; linting took 60ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class))

(defn fact [x]
  (cond (<= x 1) 1 :else (* x (fact (dec x)))))

(defn catnum [n]
  (/ (fact (* n 2)) (* (fact n) (fact (inc n)))))

(defn main []
  (let [n (bigint (read))]
    (println (str (catnum n)))))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; 1859543927052383573141662118617979937876646717504578983436437473210524268177
;; 8002764367301487194210975205837015052120754833175521497365733211143730672154
;; 6118551526190812044118251373049520351408628467972416390480783244077644949134
;; 4312400
