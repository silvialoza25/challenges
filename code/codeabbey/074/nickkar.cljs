;; $ clj-kondo --lint nickkar.cljs
;; linting took 46ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]
            [clojure.string :as str]))

(def hourrate 30)
(def minrate 6)
(def hl 6)
(def ml 9)

(defn format-input [s]
  (map #(str/split % #":") (str/split s #" "))
  )

(defn cartesian-format ([size input] (cartesian-format size 0 input []))
  ([size i input out]
   (cond
     (>= i size) out
     :else
     (let [ca5e (nth input i)
           hour (mod (core/bigint (nth ca5e 0)) 12)
           min (core/bigint (nth ca5e 1))
           hangle (/ (* Math/PI (* (+ hour (/ min 60)) hourrate)) 180)
           mangle (/ (* Math/PI (* min minrate)) 180)
           hx (+ (* hl (Math/sin hangle)) 10)
           hy (+ (* hl (Math/cos hangle)) 10)
           mx (+ (* ml (Math/sin mangle)) 10)
           my (+ (* ml (Math/cos mangle)) 10)]
       (cartesian-format size (inc i) input (conj out hx hy mx my)))
     )
   )
  )

(defn main []
  (let [input-size (core/read-string (core/read-line))
        input (format-input (core/read-line))]
    (apply println (cartesian-format input-size input))
    ))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 14.944757131732095 13.398437421548998 2.718847050625472 15.290067270632257
;; 12.296100594190538 15.54327719506772 1.0 9.999999999999998 15.8577760427196
;; 11.298637683628618 5.499999999999999 2.205771365940053 14.91491226573395
;; 13.441458618106278 2.205771365940053 14.5 8.958110933998418 4.091153481926752
;; 17.794228634059948 5.500000000000002 7.655613229064352 15.52302912071464
;; 18.95069705831446 10.940756169408882 4.091153481926752 8.958110933998418
;; 2.2057713659400546 5.4999999999999964
