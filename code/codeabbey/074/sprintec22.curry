-- $ curry-verify sprintec22.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `sprintec22'!

import Char
import Float
import IO
import Integer
import List
import Prelude
import Read

strToI :: String -> Int
strToI string = read string :: Int

getData :: IO String
getData = do
  line <- getContents
  return line

findHandPos :: Int -> Int -> Float -> Float -> [Float]
findHandPos handDirection handLength piNumber min = result
  where
    fixedHand =
      if handLength == 6
        then  handDirection `mod` 12
        else  handDirection
    handDegree =
      if handLength == 6
        then (piNumber /. 6.0) *. ((i2f fixedHand) +. (min /. 60.0))
        else (piNumber /. 30.0) *. (i2f fixedHand)
    fixedHandDegree = (piNumber /. 2.0) - handDegree
    x = (i2f handLength) *. cos fixedHandDegree +. 10.0
    y = (i2f handLength) *. sin fixedHandDegree +. 10.0
    result = [x, y]

findSolution :: [Int] -> String
findSolution time = result
  where
    minHandLength = 9
    hrsHandLength = 6
    piNumber = 3.14159265359
    hours = time !! 0
    minutes = time !! 1
    minHandPos = findHandPos minutes minHandLength piNumber 0.0
    hrsHandPos = findHandPos hours hrsHandLength piNumber (i2f minutes)
    x_Hour = hrsHandPos !! 0
    y_Hour = hrsHandPos !! 1
    x_Min = minHandPos !! 0
    y_Min = minHandPos !! 1
    result = show x_Hour ++ " " ++ show y_Hour ++ " " ++ show x_Min ++ " "
      ++ show y_Min

convert :: String -> [Int]
convert dat = result
  where
    hours = strToI (take 2 dat)
    minutes = strToI (drop 3 dat)
    result = [hours, minutes]

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    data_1 = words (usefulData !! 0)
    formatedInputData = map convert data_1
    finalResult = map findSolution formatedInputData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs -q :load sprintec22.curry :eval main :quit
-- 10.052359212989627 15.999771538385033 10.940756169408017 18.950697058314553
-- 8.346175865097305 4.232429824370287 8.128794782639137 1.196671593395969
-- 8.958110934000182 15.90884651807356 2.2057713659392766 5.500000000001344
-- 14.97422543533012 13.355157420824675 3.311696570704987 16.022175457231427
-- 7.559580141546743 15.481272745856293 18.559508646656326 12.781152949374706
-- 11.24747014490603 15.868885604402944 15.290067270631809 2.718847050625147
-- 10.679219282606867 15.961431134059591 18.803328406604223 11.871205217359956
