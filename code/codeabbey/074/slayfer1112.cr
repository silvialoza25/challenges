#! /usr/bin/crystal

# $ ameba --all --fail-level Convention slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 8.92 milliseconds
# $ crystal build --error-trace --error-on-warnings \
# --threads 1 --no-codegen slayfer1112.cr

_ = gets # cases
args = gets

# Dictionary with the angles in rad for every min
def dicc
  pi = 3.141_592_653_589_793_238_46
  counter = 180
  dicc_hour = Hash(Int32, Float64).new
  dicc_min = Hash(Int32, Float64).new
  0.0.step(to: 359.5, by: 0.5) do |degrees|
    dicc_hour[counter] = degrees*pi/180
    if counter <= 1
      counter = 721
    end
    counter -= 1
  end
  counter = 15
  0.0.step(to: 354.0, by: 6.0) do |degrees|
    dicc_min[counter] = degrees*pi/180
    if counter <= 0
      counter = 60
    end
    counter -= 1
  end
  return dicc_hour, dicc_min
end

def solution(array)
  hours = [] of Int32
  mins = [] of Int32
  d_hour, d_min = dicc()
  array.each do |x|
    x = x.split(":")
    y = (x[0].to_i * 60) + x[1].to_i
    if y > 720
      y -= 720
    end
    hours << y
    mins << x[1].to_i
  end
  min_hand = 9.0
  hour_hand = 6.0
  0.step(to: array.size - 1) do |x|
    posx_hour = (10.0 + Math.cos(d_hour[hours[x]]) * hour_hand).round(8)
    posy_hour = (10.0 + Math.sin(d_hour[hours[x]]) * hour_hand).round(8)

    posx_min = (10.0 + Math.cos(d_min[mins[x]]) * min_hand).round(8)
    posy_min = (10.0 + Math.sin(d_min[mins[x]]) * min_hand).round(8)

    print "#{posx_hour} #{posy_hour} #{posx_min} #{posy_min} "
  end
end

if args
  args = args.split
  solution(args)
end

# $ cat DATA.lst | crystal slayfer1112.cr
# 4.58257569 2.0 1.73205081 4.3588989400000004 7.0 1.0 3.0 5.0 8.18535277
# 3.60555128 4.3588989400000004 7.0 1.0 1.0 1.0 4.3588989400000004 7.0
# 4.58257569 6.0 1.73205081 2.64575131
