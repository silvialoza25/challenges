{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- getLine
  let formatedInputData = map convert (words inputData)
  let minHandLength = 9
  let hrsHandLength = 6
  let piNumber = 3.14159265359
  let solution = findSolution formatedInputData minHandLength
                 hrsHandLength piNumber
  putStrLn (unwords solution)

findSolution :: [[Int]] -> Int -> Int -> Double -> [String]
findSolution [] minHandLength hrsHandLength piNumber = []
findSolution formatedInputData minHandLength hrsHandLength piNumber = res
  where
    iInputData = head formatedInputData
    hours = head iInputData
    minutes = iInputData !! 1
    minHandPos = findHandPos minutes minHandLength piNumber 0.0
    hrsHandPos = findHandPos hours hrsHandLength piNumber
                 (fromIntegral minutes :: Double)
    xHrs = head hrsHandPos
    yHrs = hrsHandPos !! 1
    xMin = head minHandPos
    yMin = minHandPos !! 1
    iRes = show xHrs ++ " " ++ show yHrs ++ " "
           ++ show xMin ++ " " ++ show yMin
    res = iRes : findSolution (drop 1 formatedInputData)
          minHandLength hrsHandLength piNumber

findHandPos :: Int -> Int -> Double -> Double -> [Double]
findHandPos handDirection handLength piNumber min = res
  where
    fixedHand = if handLength == 6
                  then  handDirection `mod` 12
                  else  handDirection
    handDegree = if handLength == 6
                   then (piNumber / 6.0) * ((fromIntegral fixedHand :: Double)
                        + (min / 60.0))
                   else (piNumber / 30.0) * (fromIntegral fixedHand :: Double)
    fixedHandDegree = (piNumber / 2.0) - handDegree
    x = (fromIntegral handLength :: Double) * cos fixedHandDegree + 10.0
    y = (fromIntegral handLength :: Double) * sin fixedHandDegree + 10.0
    res = [x, y]

convert :: String -> [Int]
convert dat = res
  where
    hours = read (take 2 dat) :: Int
    minutes = read (drop 3 dat) :: Int
    res = [hours, minutes]

{-
$ cat DATA.lst | ./bridamo98
  10.052359212989627 15.999771538385033 10.940756169408017 18.950697058314553
  8.346175865097305 4.232429824370287 8.128794782639137 1.196671593395969
  8.958110934000182 15.90884651807356 2.2057713659392766 5.500000000001344
  14.97422543533012 13.355157420824675 3.311696570704987 16.022175457231427
  7.559580141546743 15.481272745856293 18.559508646656326 12.781152949374706
  11.24747014490603 15.868885604402944 15.290067270631809 2.718847050625147
  10.679219282606867 15.961431134059591 18.803328406604223 11.871205217359956
-}
