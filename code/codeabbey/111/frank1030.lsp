#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

$ clisp -c frank1030.lsp
Compiling file /Users/Frank/Desktop/frank/111/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/111/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-dat()
  (let ((dat))
    (setq dat(read))
  )
)

(defun fkm (a)
  (let ((count1 1))
    (loop for i from 2 to  a
      do (if (= (gcd a i) 1)
        (setq count1 (+ count1 1))
      )
    )
    (return-from fkm count1)
  )
)

(defun necklace-count (index)
  (let ((ncolors 0) (nbeads 0) (total 0))
    (loop for i from 1 to index
      do (setq ncolors (get-dat))
      do (setq nbeads (get-dat))
      do (loop for i from 1 to nbeads
        do (if (= (mod nbeads i) 0)
          (setq total (+ (* (fkm (/ nbeads i)) (expt ncolors i))  total))
        )
      )
      do (print (/ total nbeads))
      do (setq total 0)
    )
  )
)

(defun main ()
  (let ((index 0))
    (setq index(get-dat))
    (necklace-count index)
  )
)

(main)

#|
$ cat DATA.lst | clisp frank1030.lsp

834  956635  8230  45  2195  1119796  11
|#
