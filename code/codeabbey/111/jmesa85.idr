{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #111: Necklace index - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Nat
import Data.Strings

%default partial

gcdiv : (a: Nat) -> (b: Nat) -> Nat
gcdiv a Z = a
gcdiv Z b = b
gcdiv a (S b) = gcdiv (S b) (modNatNZ a (S b) SIsNotZ)

stoI : String -> Integer
stoI s = fromMaybe 0 $ parseInteger {a=Integer} s

getLines : IO (List String)
getLines = do
  x <- getLine
  if x == "" then pure [] else do
    xs <- getLines
    pure (x :: xs)

getNecklacesCount : Integer -> Integer -> Integer -> Nat -> Nat
getNecklacesCount colors beads index count =
  if index >= beads then divNat count (integerToNat beads)
  else let
    gcdCB = gcdiv (integerToNat index) (integerToNat beads)
    newCount = count + (power (integerToNat colors) gcdCB)
  in
  getNecklacesCount colors beads (index + 1) newCount

processTestCase : List Integer -> Nat
processTestCase [colors, beads] = getNecklacesCount colors beads 0 0

main : IO ()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse input array
    testCases = map (map stoI) $ map words inputData
    -- Process data
    results = map processTestCase testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  249 39996 66 498004 3696 122643 20008
-}
