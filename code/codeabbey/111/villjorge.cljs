;; $ clj-kondo --lint villjorge.cljs
;; linting took 70ms, errors: 0, warnings: 0

(ns villjorge-111
  (:gen-class)
  (:require [clojure.core :as core]))

(defn flt [pos bead]
  (if (= bead 0)
    (+ pos 0)
    (flt bead (mod pos bead))))

(defn ** [x n] (reduce * (repeat n x)))

(defn combinations [c b]
  (let [color c
        bead b
        ans_comb (atom 0)]
    (loop [x 1]
      (when (<= x bead)
        (swap! ans_comb + (** color (flt x bead)))
        (recur (+ x 1))))
    (print (/ @ans_comb bead) "")))

(defn get-data [size-input]
  (loop [x 1]
    (when (< x (+ size-input 1))
      (combinations (core/read) (core/read))
      (recur (+ x 1)))))

(defn main []
  (let [size-input (core/read)]
    (get-data size-input))
  (println))

(main)

;; $ cat DATA.lst | clj villjorge.cljs
;; 834 956635 8230 45 2195 1119796 11
