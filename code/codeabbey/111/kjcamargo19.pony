// ponyc -b kjcamargo19

use "collections"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun iterator(data: Array[String]): Array[String] =>
          data.slice(1, data.size())

        fun gcd(const_i: U16, beads: U16): U16 =>
          if const_i == 0 then
            return beads
          else
            gcd(beads % const_i,const_i)
          end

        fun cal_total(const_i: U16, beads: U16, color: U16, sum':F32): F32 =>
          var sum: F32 = sum'
          if const_i == beads then
            return sum
          else
            var const_k: U16 = gcd(const_i, beads)
            sum = sum + color.f32().pow(const_k.f32())
            cal_total(const_i+1,beads,color,sum)
          end

        fun obtain_data(data: Array[String])? =>
          if data.size() > 0 then
            var line: Array[String] = data(0)?.split_by(" ")
            var colors: U16 = line(0)?.u16()?
            var beads: U16 = line(1)?.u16()?

            var count: F32 = cal_total(0,beads, colors,0)
            var number_necklaces: String = (count/beads.f32()).string()
            env.out.write(number_necklaces + " ")
            obtain_data( iterator (data))?
          end

        fun ref apply(data: Array[U8] iso) =>
          var input_data: String = String.from_iso_array(consume data)
          var line_sep: Array[String] = input_data.split_by("\n")
          var real_data: Array[String] = line_sep.slice(1, line_sep.size())

          try
            obtain_data(real_data)?
          end

        fun ref dispose() =>
          env.out.write("")
      end,
      512
    )
// cat DATA.lst | ./kjcamargo19
// 66 119 5934 451 616 9 48915 2635
