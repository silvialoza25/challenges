/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, bool, char.

:- func isPrimeAux(int, int) = bool.
isPrimeAux(Num, AuxCheck) =
(
  if pow(AuxCheck,2) =< Num
  then (
    if Num mod AuxCheck = 0; Num mod (AuxCheck + 2) = 0
    then no
    else isPrimeAux(Num,AuxCheck + 6))
  else yes
).

:- pred isPrime(int::in,bool::out) is det.
isPrime(Num, IsPrime) :-
(
  if Num = 2; Num = 3
  then IsPrime = yes
  else (
    if Num mod 2 = 0; Num mod 3 = 0
    then IsPrime = no
    else IsPrime = isPrimeAux(Num, 5)) %starting check
).

:- func writeAsPrime(int, int) = int.
writeAsPrime(Num, Prime) =
(
  if Num = 0 then Prime
  else writeAsPrime(Prime mod Num, Num)
).

:- func calcCountAux(int, int, int) = int.
calcCountAux(Colors, Beads, Step) =
(
  if Step > Beads then 0
  else pow(Colors, writeAsPrime(Beads, Step))
    + calcCountAux(Colors, Beads, Step + 1)
).

:- pred calcCount(int::in, int::in, int::out) is det.
calcCount(Beads, Colors, Count) :-
(
  if
    Beads = 1 then Count = Colors
  else if
    isPrime(Beads, IsPrime),
    IsPrime = yes
  then Count = Colors + (pow(Colors,Beads) - Colors) / Beads
  else
    calcCountAux(Colors, Beads, 1) = Perm,
    Count = Perm / Beads
).

:- pred countNecklaces(list(string), string, string).
:- mode countNecklaces(in, in, out) is det.
countNecklaces([], FinalFreq, strip(FinalFreq)).
countNecklaces([Line | Tail], PartialFreq, FinalFreq) :-
(
  words_separator(char.is_whitespace, Line) = InputList,
  det_index0(InputList, 0, Colors),
  det_index0(InputList, 1, Beads),
  calcCount(det_to_int(Beads), det_to_int(Colors), Count),
  PartialFreqs1 = PartialFreq ++ " " ++ from_int(Count),
  countNecklaces(Tail, PartialFreqs1, FinalFreq)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1, FileContents, NeckList),
    countNecklaces(NeckList, "", Counts),
    io.print_line(Counts, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  249 39996 66 498004 3696 122643 20008
*/
