{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let intInputData = map convert inputData
  let solution = map solve intInputData
  putStrLn (unwords solution)

solve :: [Int] -> String
solve dat = res
  where
    numOfColors = head dat
    sizeOfNecklace = dat !! 1
    res = show (sum (findNumOfNecklaces numOfColors sizeOfNecklace
          [1..sizeOfNecklace]) `div` sizeOfNecklace)

findNumOfNecklaces :: Int -> Int -> [Int] -> [Int]
findNumOfNecklaces numOfColors sizeOfNecklace [] = []
findNumOfNecklaces numOfColors sizeOfNecklace counter = res
  where
    iCounter = head counter
    numOfSections = findExponent iCounter sizeOfNecklace
    iRes = numOfColors ^ numOfSections
    res = iRes : findNumOfNecklaces
          numOfColors sizeOfNecklace (drop 1 counter)

findExponent :: Int -> Int -> Int
findExponent partition index = res
  where
    res = if index == 0
            then partition
            else findExponent index (partition `mod` index)

convert :: String -> [Int]
convert dat = result
  where
    aux = words dat
    result = map read aux :: [Int]

{-
$ cat DATA.lst | ./bridamo98
  49776 12 10 208 88725 5 45 498004
-}
