;; $clj-kondo --lint nickkar.clj
;; linting took 40ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class))

;; Reads the input and puts the values in a readable vector
(defn read-input ([num-cases] (read-input num-cases 0 []))
  ([num-cases index out]
    (cond
      (= index num-cases) out
      :else (read-input num-cases (inc index) (conj out [(read) (read)])))))

;; find the greatest common divisor of two numbers "a" and "b"
(defn gcd [num-a num-b]
  (cond
    (= num-b 0) num-a
    :else (gcd num-b (mod num-a num-b))))

;; Euler's totient function (counts how many coprimes a number "n" has)
(defn totient ([num-n] (totient num-n 1 0))
  ([num-n index count]
   (cond
     (> index num-n) count
     (= (gcd index num-n) 1) (totient num-n (inc index) (inc count))
     :else (totient num-n (inc index) count))))

;; finds the numbers under "n" which the division equals a whole number
(defn find-divisors ([num-n] (find-divisors num-n 1 []))
  ([num-n index out]
   (cond
     (> index num-n) out
     (= (mod num-n index) 0) (find-divisors num-n (inc index) (conj out index))
     :else (find-divisors num-n (inc index) out))))

;; solve a single case using sums and totient functions to count how many
;; necklaces can be crafted using certain number of beads and colors. "num-d"
;; represents the current divisor, since the function has to iterate over all
;; whole divisors of the number of beads
(defn solve-case
  ([beads colors]
   (solve-case beads colors (find-divisors beads) 0 0))
  ([beads colors divisors index actual]
   (cond
     (= index (count divisors)) (Math/round (/ actual beads))
     :else
     (let [num-d (nth divisors index)
           newactual
           (+ actual (* (totient num-d) (Math/pow colors (/ beads num-d))))]
       (solve-case beads colors divisors (inc index) newactual)))))

;; iterates over every test-case
(defn solve-cases ([num-cases cases] (solve-cases num-cases cases 0 []))
  ([num-cases cases index out]
    (cond
      (>= index num-cases) out
      :else
      (let [[colors beads] (nth cases index)
            necklaces (solve-case beads colors)]
        (solve-cases num-cases cases (inc index) (conj out necklaces))))))

;; driver code
(defn main []
  (let [num-cases (read)
        cases (read-input num-cases)]
    (apply println (solve-cases num-cases cases))))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; 834 956635 8230 45 2195 1119796 11
