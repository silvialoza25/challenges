-- $ ghc -o smendoz3 smendoz3.hs
--   [1 of 1] Compiling Main ( smendoz3.hs, smendoz3.o )
--   Linking code ...
-- $ hlint smendoz3.hs
--   No hints

import Control.Monad
import Data.Maybe

type Stack = String --String as a Stack structure

main = do
  input <- getLine
  inputs <- replicateM (read input) getLine
  putStr (unwords (map charToString (checkAll inputs)) ++ "\n")

charToString :: Char -> String
charToString c = [c]

checkAll :: [String] -> String
checkAll = map checkString

isOpen :: Char -> Bool
isOpen c = c == '(' || c == '[' || c == '{' || c == '<'

expect c = fromJust $ lookup c $ zip "[{(<" "]})>"

checkString :: String -> Char
checkString s = checkStringHelper (filterNonParen s) []

checkStringHelper :: String -> Stack -> Char
checkStringHelper (s:xs) stack
  | isOpen s = checkStringHelper xs (expect s:stack)
  | stack /= [] =
    if s == head stack then checkStringHelper xs (tail stack) else '0'
  | otherwise = '0'

checkStringHelper [] [] = '1'
checkStringHelper [] stack = '0'


filterNonParen :: String -> String
filterNonParen = filter (`elem` paren)
  where paren =  ['(', ')', '[', ']', '{', '}', '<','>']

-- $ cat DATA.lst | ./smendoz3
--   1 1 0 1 0 0 0 0 1 1 0 0 1 1 0 1 1 0
