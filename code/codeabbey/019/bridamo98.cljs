;; $ clj-kondo --lint bridamo98.clj
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-019
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn delete-pairs [only-brackets num-of-pairs]
  (loop [i 0 cur-sentence only-brackets]
    (if (< i num-of-pairs)
      (recur (+ i 1)
        (str/replace cur-sentence #"(\[\])|(\{\})|(\<\>)|(\(\))" ""))
      (if (= (count cur-sentence) 0) 1 0))))

(defn check-correctness [line]
  (let [only-brackets (apply str (re-seq #"[\[\]\<\>\{\}\(\)]" line))
        num-of-brackets (count only-brackets)]
    (if (= (mod num-of-brackets 2) 0)
      (delete-pairs only-brackets (quot num-of-brackets 2)) 0)))

(defn solve-all[size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (let [line (core/read-line)]
        (recur (+ i 1)
          (str result (check-correctness line) " ")))
      result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj -M bridamo98.cljs
;; 1 0 0 0 1 0 1 0 1 1 1 1 0 0 1
