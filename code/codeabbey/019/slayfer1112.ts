/*
$ eslint slayfer1112.ts
$ tsc slayfer1112.ts
*/

function matchingBrackets(brackets: string, steps: number, i: number): number {
  if (i < steps) {
    const regex = /(\[\])|(\{\})|(<>)|(\(\))/g;
    const newBrackets: string = brackets.replace(regex, '');
    const len: number = newBrackets.length;
    if (len === 0) {
      return 1;
    }
    return matchingBrackets(newBrackets, steps, i + 1);
  }
  return 0;
}

function solution(entry: string): number {
  const brackets: string = entry.replace(/[^([{<)\]}>]/g, '');
  const steps: number = brackets.length;
  const check: number = matchingBrackets(brackets, steps, 0);
  process.stdout.write(`${check} `);
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(1, entryArray.length);
    dat.map((brackets) => solution(brackets));
    process.stdout.write('\n');
    return 0;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
0 0 1 1 0 1 1 1 1 1 1 0 0 0 0 1 1 1 1 0
*/
