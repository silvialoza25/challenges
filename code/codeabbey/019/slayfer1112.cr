#! /usr/bin/crystal

# $ ameba \
#   --all \
#   --fail-level Convention \
#   slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 3.54 milliseconds
# $ crystal build slayfer1112.cr

def solution(array)
  brackets = array.gsub(/[^([{<)\]}>]/,"")
  match = 0
  1.step(to: brackets.size/2, by: 1) do |_|
    brackets = brackets.gsub(/(\[\])|({})|(<>)|(\(\))/,"")
    if brackets.size == 0
      match = 1
    end
  end
  print "#{match} "
end

cases = gets

if cases
  cases = cases.is_a?(String) ? cases.try &.to_i : cases
  0.step(to: cases,by:1) do |_|
    args = gets
    if args
      solution(args)
    end
  end
end

puts

# $ cat DATA.lst | crystal slayfer1112.cr
# 0 0 1 1 0 1 1 1 1 1 1 0 0 0 0 1 1 1 1 0
