# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.04s to load, 0.05s running 55 checks on 1 file)
# 7 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule MatchingBrackets do
  def main do
    all_data = IO.read(:stdio, :all)
    brackets = %{"(" => ")", "[" => "]", "{" => "}", "<" => ">"}
    try do
      get_data(all_data)
        |> Enum.map(&matching_brackets(&1, [], brackets))
        |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_data(all_data) do
    split_data = String.split(all_data, "\n")
    List.delete_at(split_data, 1 +
      String.to_integer(Enum.at(split_data, 0)))
      |> Enum.map(&String.split(&1, ""))
      |> Enum.map(&Enum.reject(&1, fn x -> x == "" or String.match?(x,
          ~r/^[[:alnum:], \+|\-|\*|\/|\^|\%]+$/) end))
      |> tl()
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def matching_brackets([], [], _), do: 1
  def matching_brackets([], _, _), do: 0
  def matching_brackets([head | tail], acc, brackets) do
    cond do
      String.match?(head, ~r/^[\(|\{|\<|\[]+$/) ->
        matching_brackets(tail, acc ++ [head], brackets)
      Map.get(brackets, List.last(acc)) == head ->
        matching_brackets(tail, Enum.reverse(
          Enum.reverse(acc) -- [List.last(acc)]), brackets)
      true -> 0
    end
  end
end

MatchingBrackets.main()

# cat DATA.lst | elixir ludsrill.exs
# 1 0 1 0 0 0 1 0 1 1 0 1 0 0 1 0
