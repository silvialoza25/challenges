# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.02s to load, 0.1s running 55 checks on 1 file)
# 6 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule CombinationsWithRepetitions do
  def main do
    [head | tail] = get_data()
    solution = combinations(tail, head)
    str_solution = Enum.map(Enum.uniq(Enum.map(solution,
                                       &Enum.take(&1, head))), &to_str/1)
    Enum.map(str_solution, &IO.write("#{&1} "))
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def to_str(list) do
    Enum.join(Enum.map(list, &Integer.to_string/1), "")
  end

  def get_data do
    all_data = List.delete(String.split(IO.read(:stdio, :all), "\n"), "")
    to_int(List.delete(List.first(Enum.map(all_data, &String.split(&1, " "))),
     "of"))
  end

  def combinations(s, n) do
    cond do
      n == 0 -> [nil]
      Enum.empty?(s) -> []
      true ->
        Enum.map(combinations(tl(s), n - 1), fn(x) -> [hd(s)] ++ x end) ++
        combinations(tl(s), n)
    end
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e CombinationsWithRepetitions.main
# 00123 00124 00125 00126 00127 00128 00133 00134 00135 00136 00137 00138 00145
# 00146 00147 00148 00156 00157 00158 00167 00168 00178 00233 00234 00235 00236
# 00237 00238 00245 00246 00247 00248 00256 00257 00258 00267 00268 00278 00334
# 00335 00336 00337 00338 00345 00346 00347 00348 00356 00357 00358 00367 00368
# 00378 00456 00457 00458 00467 00468 00478 00567 00568 00578 00678 01233 01234
# 01235 01236 01237 01238 01245 01246 01247 01248 01256 01257 01258 01267 01268
# 01278 01334 01335 01336 01337 01338 01345 01346 01347 01348 01356 01357 01358
# 01367 01368 01378 01456 01457 01458 01467 01468 01478 01567 01568 01578 01678
# 02334 02335 02336 02337 02338 02345 02346 02347 02348 02356 02357 02358 02367
# 02368 02378 02456 02457 02458 02467 02468 02478 02567 02568 02578 02678 03345
# 03346 03347 03348 03356 03357 03358 03367 03368 03378 03456 03457 03458 03467
# 03468 03478 03567 03568 03578 03678 04567 04568 04578 04678 05678 12334 12335
# 12336 12337 12338 12345 12346 12347 12348 12356 12357 12358 12367 12368 12378
# 12456 12457 12458 12467 12468 12478 12567 12568 12578 12678 13345 13346 13347
# 13348 13356 13357 13358 13367 13368 13378 13456 13457 13458 13467 13468 13478
# 13567 13568 13578 13678 14567 14568 14578 14678 15678 23345 23346 23347 23348
# 23356 23357 23358 23367 23368 23378 23456 23457 23458 23467 23468 23478 23567
# 23568 23578 23678 24567 24568 24578 24678 25678 33456 33457 33458 33467 33468
# 33478 33567 33568 33578 33678 34567 34568 34578 34678 35678 45678
