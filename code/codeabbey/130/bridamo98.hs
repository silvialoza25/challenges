{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Data.Char
import Control.Monad
import Numeric

main = do
  dat <- getLine
  let divDat = words dat
  let fixDivDat = take 1 divDat ++ drop 2 divDat
  let intDat = map read fixDivDat :: [Int]
  let r = head (take 1 intDat)
  let n = drop 1 intDat
  let indexes = [0..(length n - 1)]
  let indexCombinations = combinations r indexes
  let dupliCombi = buildCombinations n indexCombinations
  let solution = removeDuplicates dupliCombi
  putStrLn (unwords solution)

combinations :: Int -> [Int] -> [[Int]]
combinations 0 _ = [[]]
combinations _ [] = []
combinations n (x : xs) = map (x :)
                          (combinations (n - 1) xs) ++ combinations n xs

buildCombinations :: [Int] -> [[Int]] -> [String]
buildCombinations index [] = []
buildCombinations index indexCombinations = res
  where
    iCom = head indexCombinations
    iRes = map (head . show) (getCombination index iCom)
    res = iRes : buildCombinations index (drop 1 indexCombinations)

getCombination :: [Int] -> [Int] -> [Int]
getCombination index [] = []
getCombination index combination = res
  where
    iCom = head combination
    iRes = index !! iCom
    res = iRes : getCombination index (drop 1 combination)

removeDuplicates :: Eq a => [a] -> [a]
removeDuplicates [] = []
removeDuplicates (x:xs)   | x `elem` xs   = removeDuplicates xs
                          | otherwise     = x : removeDuplicates xs

{-
$ cat DATA.lst | ./bridamo98
  00123 00124 00125 00126 00127 00128 00133 00134 00135 00136 00137 00138
  00145 00146 00147 00148 00156 00157 00158 00167 00168 00178 00233 00234
  00235 00236 00237 00238 00245 00246 00247 00248 00256 00257 00258 00267
  00268 00278 00334 00335 00336 00337 00338 00345 00346 00347 00348 00356
  00357 00358 00367 00368 00378 00456 00457 00458 00467 00468 00478 00567
  00568 00578 00678 01233 01234 01235 01236 01237 01238 01245 01246 01247
  01248 01256 01257 01258 01267 01268 01278 01334 01335 01336 01337 01338
  01345 01346 01347 01348 01356 01357 01358 01367 01368 01378 01456 01457
  01458 01467 01468 01478 01567 01568 01578 01678 02334 02335 02336 02337
  02338 02345 02346 02347 02348 02356 02357 02358 02367 02368 02378 02456
  02457 02458 02467 02468 02478 02567 02568 02578 02678 03345 03346 03347
  03348 03356 03357 03358 03367 03368 03378 03456 03457 03458 03467 03468
  03478 03567 03568 03578 03678 04567 04568 04578 04678 05678 12334 12335
  12336 12337 12338 12345 12346 12347 12348 12356 12357 12358 12367 12368
  12378 12456 12457 12458 12467 12468 12478 12567 12568 12578 12678 13345
  13346 13347 13348 13356 13357 13358 13367 13368 13378 13456 13457 13458
  13467 13468 13478 13567 13568 13578 13678 14567 14568 14578 14678 15678
  23345 23346 23347 23348 23356 23357 23358 23367 23368 23378 23456 23457
  23458 23467 23468 23478 23567 23568 23578 23678 24567 24568 24578 24678
  25678 33456 33457 33458 33467 33468 33478 33567 33568 33578 33678 34567
  34568 34578 34678 35678 45678
-}
