{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Data.Char
import Control.Monad

main = do
  dat <- getLine
  let intDat =  convert dat
  let n = head intDat
  let x0 = intDat !! 1
  dat2 <- getLine
  let intDat2 = convert dat2
  let indexesAux = getRandNumbers 445 700001 2097152 (sum intDat2) x0
  let indexes = getIndexes indexesAux intDat2
  let vowels = "aeiou"
  let consonants = "bcdfghjklmnprstvwxz"
  let solution = getWords indexes consonants vowels
  putStrLn (unwords solution)

getWords :: [[Int]] -> String -> String -> [String]
getWords [] consonants vowels = []
getWords indexes consonants vowels = res
  where
    indexI = head indexes
    resi = buildWord indexI consonants vowels 1
    res = resi : getWords (drop 1 indexes) consonants vowels

buildWord :: [Int] -> String -> String -> Int -> String
buildWord [] consonants vowels n = []
buildWord indexI consonants vowels n = res
  where
    index = head indexI
    letter = if n `mod` 2 == 0
               then vowels !! index
               else consonants !! index
    res = letter : buildWord (drop 1 indexI) consonants vowels (n + 1)

getIndexes :: [Int] -> [Int] -> [[Int]]
getIndexes randNumbers [] = []
getIndexes randNumbers lengths = res
  where
    ilength = head lengths
    resi = randToIndex (take ilength randNumbers) 19
    res = resi : getIndexes (drop ilength randNumbers) (drop 1 lengths)

randToIndex :: [Int] -> Int -> [Int]
randToIndex [] modOp = []
randToIndex rand modOp = res
  where
    resi = head rand `mod` modOp
    nextModOp = if modOp == 19
                then 5
                else 19
    res = resi : randToIndex (drop 1 rand) nextModOp

convert :: String -> [Int]
convert dat = result
  where
    aux = words dat
    result = map read aux :: [Int]

getRandNumbers :: Int -> Int -> Int -> Int -> Int -> [Int]
getRandNumbers a c m 0 xCur = []
getRandNumbers a c m n xCur = res
  where
    xNext = (a * xCur + c) `mod` m
    res = xNext : getRandNumbers a c m (n - 1) xNext

{-
$ cat DATA.lst | ./bridamo98
  lum bukuvut cibuxum pofumiv xowej dejemepo punovevu xed sojar
  gepi bupobo kesebi jocev magapepo vivip nejuxah nalob
-}
