###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #072: Funny Words Generator - Coffeescript

# xNext = (a * x0 + c) % m, for n number of times
# For this challenge: a, c, m are predefined
getLcg = (x0, n) ->
  a = 445
  c = 700001
  m = 2097152
  # Base case
  if n == 0
    return x0
  # Recursive case
  xNext = (a * x0 + c) % m
  getLcg(xNext, n - 1)

getVowelAtIndex = (index) ->
  "aeiou"[index]

getConsonantAtIndex = (index) ->
  "bcdfghjklmnprstvwxz"[index]

generateFunnyWord = (wordLength, currIndex, prevRandomValue) ->
  # Base case
  if currIndex == wordLength
    return ""
  # Recursive case
  # Generate new letter
  randomValue = getLcg(prevRandomValue, 1)
  if currIndex % 2 == 0
    # Even position
    return (getConsonantAtIndex(randomValue % 19) +
      generateFunnyWord(wordLength, currIndex + 1, randomValue))
  else
    # Odd position
    return (getVowelAtIndex(randomValue % 5) +
      generateFunnyWord(wordLength, currIndex + 1, randomValue))

# Standard addition
getSum = (acc, value) ->
  acc + value

processTestCase = (testCase, wordIndex, array, initialSeed) ->
  # The wordIndex and the other test cases will be evaluated
  # in order to get the correct seed for each new word
  if wordIndex == 0
    # This is the first word
    return generateFunnyWord(testCase, 0, initialSeed)
  else
    prevLetterCount = array.slice(0, wordIndex).reduce(getSum)
    newWordSeed = getLcg(initialSeed, prevLetterCount)
    return generateFunnyWord(testCase, 0, newWordSeed)

main = ->
  # Read data from STDIN
  data = require('fs').readFileSync(0).toString().trim().split('\n')
  # Get initial seed
  initialSeed = parseInt(data[0].split(' ')[1])
  # Get the test cases
  testCases = data[1].split(' ').map(Number)
  # Process each test case
  results = testCases.map((value, index, array) ->
    processTestCase(value, index, array, initialSeed))
  # Print results
  console.log(results.join(' '))

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
fajoli jasumid husiwu welu rocir gekis roxokota xif vev lit semahi borajosa
 mat lipo binavab waveha niboro miregu hewekij koke mof wulemami
###
