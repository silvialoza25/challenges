#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun join-string-list (string-list)
  (format nil "~{~A~^~} " string-list))

(defun linear-congruential-generator (n xcur)
  (let ((a 445)
        (c 700001)
        (m 2097152)
        (word ())
        (consonants "bcdfghjklmnprstvwxz")
        (vowels "aeiou")
        (xnext) (letter))

    (loop for i from 0 to (- n 1)
      do(setq xnext (mod (+ (* a xcur) c) m))
      (setq xcur xnext)
      (when (evenp i)
        (setq letter (mod xnext 19))
        (setq word (append word (list (char consonants letter)))))
      (when (oddp i)
        (setq letter (mod xnext 5))
        (setq word (append word (list (char vowels letter))))))
    (format t (join-string-list word))
    (return-from linear-congruential-generator xcur)))

(defun funny-words-generator ()
  (let ((data)
        (seed)
        (word-length))
  (setq data (read-data))
  (setq seed (second (car data)))
  (setq word-length (first (cdr data)))
  (dolist (item word-length)
    (setq seed (linear-congruential-generator item seed)))))

(funny-words-generator)

#|
cat DATA.lst | clisp ludsrill.lsp
nazoxo dihil nav mocipedu wuxal jicosohi rera neja vim devudomi
lowahizi buse cojefo bap gusa vuj nidiko fimoso
|#
