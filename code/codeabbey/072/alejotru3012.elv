# elvish -compileonly alejotru3012.elv

use str

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })]
}

fn generate_rnds [cur_x index]{
  if (> $index 0) {
    var_a = 445
    var_c = 700001
    var_m = 2097152
    new_rnd = (% (+ (* $cur_x $var_a) $var_c) $var_m)
    put $new_rnd
    generate_rnds $new_rnd (- $index 1)
  }
}

fn generate_words [index rnds consonant vowels]{
  if (< $index (count $rnds)) {
    if (== (% $index 2) 0) {
      echo $consonant[(% $rnds[$index] 19)]
    } else {
      echo $vowels[(% $rnds[$index] 5)]
    }
    generate_words (+ $index 1) $rnds $consonant $vowels
  }
}

fn calculate [data]{
  consonants = 'bcdfghjklmnprstvwxz'
  vowels = 'aeiou'
  var seed = $data[0][1]
  each [value]{
    @rnd_numbers = (generate_rnds $seed $value)
    set seed = $rnd_numbers[(- (count $rnd_numbers) 1)]
    echo (str:join '' [(generate_words 0 $rnd_numbers $consonants $vowels)])
  } $data[1]
}

echo (str:join ' ' [(calculate (input_data))])

# cat DATA.lst | elvish alejotru3012.elv
# hawoj vuxivaj sano dobejod niwatase xiru lubup gahuporo vedo
# kogibe kegum vobinaf giwaritu jifuho cumer secamedi buvofav
# hegaxiju seradiro gesi marohedi hesiv simetil
