# elvish -compileonly cyberpunker.elv

#!/bin/elvish

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })]
}

fn mortageCalculator [data]{
  each [values]{
    var loan = $values[0]
    var interestRate = (/ (/ $values[1] 100) 12)
    var months = $values[2]
    var divide = (- 1 (/ 1 (math:pow (+ 1 $interestRate) $months)))
    result = (math:ceil (/ (* $loan $interestRate) $divide))
    put $result
  } $data
}

echo (mortageCalculator (input_data))

# cat DATA.lst | elvish cyberpunker.elv
#28059
