{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #037: Mortgage Calculator - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi s = fromMaybe 0 $ parseInteger {a=Int} s

roundUp : Double -> Int
roundUp amount = cast (ceiling amount)

processTestCase : Int -> Int -> Int -> Double
processTestCase loan irate months =
  let
    -- Monthly rate
    frate = (cast irate) / 100 / 12
    fmon = (cast months)
    floan = (cast loan)
    in
  floan * (frate * ((1 + frate) `pow` fmon)) / (((1 + frate) `pow` fmon) - 1)

main : IO ()
main = do
  inputData <- getLine
  let
    -- Parse input array
    [loan, irate, months] = map stoi $ words inputData
    -- Process data
    result = roundUp $ processTestCase loan irate months
  -- Print results
  putStrLn $ show result

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  12405
-}
