;; $ clj-kondo --lint bridamo98.cljs
;; linting took 65ms, errors: 0, warnings: 0

(ns bridamo98-037
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn ** [x n] (reduce * (repeat n x)))

(defn find-monthly-payment[p r l]
  (let [r-frac (/ (/ r 100) 12) exp (** (+ r-frac 1) l)
  result (* p (/ (* r-frac exp) (- exp 1)))]
    (int (Math/ceil result))))

(defn main []
  (let [args (str/split (core/read-line) #" ")]
    (println (find-monthly-payment (edn/read-string (args 0))
    (edn/read-string (args 1)) (edn/read-string (args 2))))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 59591
