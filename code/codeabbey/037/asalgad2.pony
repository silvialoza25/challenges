// ponyc -b asalgad2

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify
        fun exp( const_x: F64, const_n: I64 ): F64 =>
          if const_n < 2 then
            const_x
          else
            const_x * exp( const_x, const_n-1 )
          end

        fun mortage(const_p: F64, const_r: F64, const_n: I64): F64 =>
          var const_b: F64 = 1 + ( const_r / 1200 )
          var const_bn: F64 = exp( const_b, const_n )
          ( const_p * const_bn * (const_b - 1) ) / (const_bn - 1)

        fun ref apply(data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume data)
          try
            var lines: Array[String] = input.split_by("\n")
            var args: Array[String] = lines(0)?.split_by(" ")
            var const_p: F64 = args(0)?.f64()?
            var const_r: F64 = args(1)?.f64()?
            var const_n: I64 = args(2)?.i64()?
            var result: F64 = mortage(const_p, const_r, const_n )
            env.out.print(result.ceil().string())
          end

        fun ref dispose() =>
          env.out.write("")
      end
    )

// cat DATA.lst | ./asalgad2
// 48092
