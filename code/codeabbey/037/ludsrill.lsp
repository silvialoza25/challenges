#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun mortgage ()
  (let ((data)
        (loan)
        (rate)
        (months)
        (monthly-payment))
    (setq data (first (read-data)))
    (setq loan (first data))
    (setq rate (/ (/ (second data) 100) 12))
    (setq months (third data))
    (setq monthly-payment (ceiling (* loan rate)
      (- 1 (expt (+ 1 rate) (- months)))))
    (print monthly-payment)))

(mortgage)

#|
cat DATA.lst | clisp ludsrill.lsp
9957
|#
