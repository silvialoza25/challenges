/**
linting
$ haxelib run checkstyle -s Miyyer1946.hx
Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
No issues found.
**/

class Main {
  static function main() {
    var data = Sys.stdin().readLine();
    var numbers:Array<String> = data.split(" ");
    var loan:Int = Std.parseInt(numbers[0]);
    var interest:Float = Std.parseFloat(numbers[1]) / 100 / 12;
    var time:Float = Std.parseFloat(numbers[2]);
    var divisor:Float = (loan * Math.pow((1 + interest),time) * interest);
    var divident:Float = (Math.pow((1 + interest),time) - 1);
    var result:Float = Math.round(divisor / divident);
    trace(result);
  }
}

/**
$ cp miyyer1946.hx Main.hx && cat DATA.lst | haxe -main Main --interp
Miyyer1946.hx:18: 13437
**/
