# elvish -compileonly alejotru3012.elv

use math
use str

fn input_data []{
  var input = []
  each [x]{
    set @input = (to-lines $input) (str:split ' ' $x)
  }
  put $input
}

fn reverse_polish_notation [data]{
  var list = []
  each [val]{
    if (eq $val add) {
      res = (+ (take 2 $list))
      set @list = $res (drop 2 $list)
    } elif (eq $val sub) {
      res = (- $list[1] $list[0])
      set @list = $res (drop 2 $list)
    } elif (eq $val mul) {
      res = (* (take 2 $list))
      set @list = $res (drop 2 $list)
    } elif (eq $val div) {
      res = (/ $list[1] $list[0])
      set @list = $res (drop 2 $list)
    } elif (eq $val mod) {
      res = (% $list[1] $list[0])
      set @list = $res (drop 2 $list)
    } elif (eq $val sqrt) {
      res = (math:sqrt $list[0])
      set @list = $res (drop 1 $list)
    } else {
      set list = [$val (to-lines $list)]
    }
  } $data
  put $list
}

echo (reverse_polish_notation (input_data))[0]

# cat DATA.lst | elvish alejotru3012.elv
# 858
