-- hlint ludsrill.hs
-- No hints

import Control.Monad
import Data.List
import Prelude

main = do
  array <- getLine
  let xs = words array
  print $ reversePolishNotation xs []

selectOperation :: [Int] -> String -> [Int]
selectOperation stack operation
  | operation == "add" = take (length stack - 2)
    stack ++ [stack !! (length stack - 2) + last stack]
  | operation == "sub" = take (length stack - 2)
    stack ++ [stack !! (length stack - 2) - last stack]
  | operation == "mul" = take (length stack - 2)
    stack ++ [stack !! (length stack - 2) * last stack]
  | operation == "div" = take (length stack - 2)
    stack ++ [stack !! (length stack - 2) `div` last stack]
  | operation == "mod" = take (length stack - 2)
    stack ++ [stack !! (length stack - 2) `mod` last stack]
  | operation == "sqrt" = take (length stack - 1)
    stack ++ [round (sqrt (fromIntegral (last stack)))]

reversePolishNotation :: [String] -> [Int] -> Int
reversePolishNotation [] stack = head stack
reversePolishNotation array stack
  | head array == "add" = reversePolishNotation (tail array)
    (selectOperation stack "add")
  | head array == "sub" = reversePolishNotation (tail array)
    (selectOperation stack "sub")
  | head array == "mul" = reversePolishNotation (tail array)
    (selectOperation stack "mul")
  | head array == "div" = reversePolishNotation (tail array)
    (selectOperation stack "div")
  | head array == "mod" = reversePolishNotation (tail array)
    (selectOperation stack "mod")
  | head array == "sqrt" = reversePolishNotation (tail array)
    (selectOperation stack "sqrt")
  | otherwise = reversePolishNotation (tail array)
    (stack ++ [read (head array) :: Int])

-- cat DATA.lst | runhaskell ludsrill.hs
-- 939
