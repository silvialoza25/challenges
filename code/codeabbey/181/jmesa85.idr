{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #181: Reverse Polish Notation - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi s = fromMaybe 0 $ parseInteger {a=Int} s

intAt : Int -> List Int -> Int
intAt 0 [] = 0
intAt _ [] = 0
intAt 0 (x::xs) = x
intAt k (_::xs) = intAt (k - 1) xs

solveRPN: List String -> List Int -> Int
solveRPN [] stack = intAt 0 stack
solveRPN ("add"::xs) (pos0::pos1::ys) = solveRPN xs ((pos1 + pos0)::ys)
solveRPN ("sub"::xs) (pos0::pos1::ys) = solveRPN xs ((pos1 - pos0)::ys)
solveRPN ("mul"::xs) (pos0::pos1::ys) = solveRPN xs ((pos1 * pos0)::ys)
solveRPN ("div"::xs) (pos0::pos1::ys) = solveRPN xs ((pos1 `div` pos0)::ys)
solveRPN ("mod"::xs) (pos0::pos1::ys) = solveRPN xs ((pos1 `mod` pos0)::ys)
solveRPN ("sqrt"::xs) (pos0::ys) = solveRPN xs ((cast aux)::ys)
  where
    aux : Double
    aux = sqrt (cast pos0)
solveRPN (num::xs) stack = solveRPN xs ((stoi num)::stack)

main : IO ()
main = do
  inputData <- getLine
  let
    -- Parse input array
    testCase = words inputData
    -- Process data
    result = solveRPN testCase []
  -- Print results
  printLn result

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  678
-}
