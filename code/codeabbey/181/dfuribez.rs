/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};

fn add(a: f64, b: f64) -> f64 {
  a + b
}

fn sub(a: f64, b: f64) -> f64 {
  a - b
}

fn mul(a: f64, b: f64) -> f64 {
  a * b
}

fn div(a: f64, b: f64) -> f64 {
  a / b
}

fn md(a: f64, b: f64) -> f64 {
  a % b
}

fn sqrt(a: f64, _b: f64) -> f64 {
  a.sqrt()
}

fn calc(elements: Vec<&str>) {
  let mut stack = Vec::new();
  let functions: Vec<&dyn Fn(f64, f64) -> f64> =
    vec![&add, &sub, &mul, &div, &md, &sqrt];
  let names: Vec<&str> = vec!["add", "sub", "mul", "div", "mod", "sqrt"];
  for element in elements.iter() {
    match element.parse::<f64>() {
      Ok(ok) => stack.push(ok),
      Err(_e) => {
        let index = names.iter().position(|x| x == element).unwrap_or(0);
        let a: f64;
        let b: f64;
        if element == &"sqrt" {
          b = 0.0;
          a = stack.pop().unwrap_or(0.0);
        } else {
          b = stack.pop().unwrap_or(0.0);
          a = stack.pop().unwrap_or(0.0);
        }
        let result: f64 = (functions[index])(a, b);
        stack.push(result);
      }
    }
  }
  println!("{:#?}", stack[0]);
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();

  for line in lines {
    let l = line.unwrap();
    let params: Vec<&str> = l.split_whitespace().collect();
    calc(params);
  }
}

/*
$ cat DATA.lst | ./dfuribez
800.0
*/
