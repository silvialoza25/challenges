{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.Map (fromListWith, toList)
import Data.List
import Control.Monad

main = do
  let sizeInput = 3
  inputData <- replicateM sizeInput getLine
  let divInputData = map convert inputData
  let repInputData = map countRepetitions divInputData
  let sides = [2, 4, 6, 8, 10, 12]
  let numOfDices = [1..5]
  let highCost = 9999.0
  let sideDiceComb = [ (x,y) | x<-numOfDices, y<-sides ]
  let combinations = map makeCombinations sideDiceComb
  let repetitions = map getRepetitions combinations
  let bestDiceSide = findSolutions repInputData repetitions highCost
  let solution = buildSolution bestDiceSide sideDiceComb
  putStrLn (unwords solution)

buildSolution :: [Int] -> [(Int, Int)] -> [String]
buildSolution [] sideDiceComb = []
buildSolution bestDiceSide sideDiceComb = res
  where
    iBest = head bestDiceSide
    iSolution = sideDiceComb !! iBest
    iRes = show (fst iSolution) ++ "d" ++ show (snd iSolution)
    res = iRes : buildSolution (drop 1 bestDiceSide) sideDiceComb

findSolutions :: [[(Int, Int)]] -> [[(Int, Int)]] -> Double -> [Int]
findSolutions [] repetitions highCost = []
findSolutions inputData repetitions highCost = res
  where
    iCase = head inputData
    deviation = calcTotalDeviation iCase repetitions 0 highCost
    minDeviation = fst (head (minimumsSnd deviation))
    res = minDeviation : findSolutions (drop 1 inputData)
          repetitions highCost

minimumsSnd :: Ord b => [(a, b)] -> [(a, b)]
minimumsSnd [] = []
minimumsSnd xs = filter ((==) minsnd . snd) xs
  where minsnd = minimum (map snd xs)

calcTotalDeviation :: [(Int, Int)] -> [[(Int, Int)]] ->
                      Int -> Double -> [(Int, Double)]
calcTotalDeviation iCase [] count highCost = []
calcTotalDeviation iCase repetitions count highCost = res
  where
    iRepetition = head repetitions
    iDeviation = calcSingleDeviation iCase iRepetition 0.0 highCost
    iTotalDeviation = (count, iDeviation)
    res = iTotalDeviation : calcTotalDeviation iCase
          (drop 1 repetitions) (count + 1) highCost


calcSingleDeviation :: [(Int, Int)] -> [(Int, Int)] ->
                       Double -> Double -> Double
calcSingleDeviation [] iRepetition devi highCost = devi
calcSingleDeviation iCase iRepetition devi highCost = res
  where
    iValue = head iCase
    realFreq = (fromIntegral (snd iValue) :: Double) /
               (fromIntegral (sum (map snd iCase)) :: Double)
    teoricalData = filter (filterCombFunc (fst iValue)) iRepetition
    teoricalFreq = if not (null teoricalData)
                 then (fromIntegral (snd (head teoricalData)) :: Double)
                      / (fromIntegral (sum (map snd iRepetition)) :: Double)
                 else highCost
    deviation = abs (teoricalFreq - realFreq) ** 2
    res = calcSingleDeviation (drop 1 iCase) iRepetition
          (devi + deviation) highCost

filterCombFunc :: Int -> (Int, Int) -> Bool
filterCombFunc target tuple = res
  where
    res = fst tuple == target

makeCombinations :: (Int, Int) -> [[Int]]
makeCombinations sideDice = res
  where
    res = replicateM (fst sideDice) [1..(snd sideDice)]


convert :: String -> [Int]
convert dat = result
  where
    aux = words dat
    filteredData = take (length aux - 1) aux
    result = map read filteredData :: [Int]

getRepetitions :: [[Int]] -> [(Int, Int)]
getRepetitions dat = res
  where
    sums = map sum dat
    repetitions = countRepetitions sums
    res = repetitions

countRepetitions :: (Ord a) => [a] -> [(a, Int)]
countRepetitions repe = toList (fromListWith (+) [(item, 1) | item <- repe])

{-
$ cat DATA.lst | ./bridamo98
  1d4 1d12 1d4
-}
