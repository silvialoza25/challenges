# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.04s to load, 0.08s running 55 checks on 1 file)
# 15 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule DungeonsandDragonsDice do
  def main do
    data = get_data()
    dice = [1, 2, 3, 4, 5]
    sides = [2, 4, 6, 8, 10, 12]
    solution(data, dice, sides)
  end

  def solution(data, dice, sides) do
    Enum.map(data, &get_dices(&1, get_combinations(dice, sides)))
      |> Enum.zip(Enum.map(data, &get_mode/1))
      |> Enum.map(&dragon_dice/1)
      |> Enum.map(&IO.write("#{hd(&1)}d#{List.last(&1)} "))
  end

  def dragon_dice({good_dice, mode}) do
    Enum.at(good_dice, Enum.find_index(solver(good_dice, mode),
      fn x -> Enum.max(solver(good_dice, mode)) == x end))
  end

  def solver(good_dice, mode) do
    Enum.map(higher_probability(good_dice),
      fn [a, b] -> Enum.max([Map.get(mode, a), Map.get(mode, b)]) end)
  end

  def higher_probability(good_dice) do
    Enum.map(good_dice, fn [a, b] ->
      [Kernel.floor((a + b * a) / 2), Kernel.ceil((a + b * a) / 2)] end)
  end

  def get_combinations([], _), do: []
  def get_combinations([_ | xs], []), do:
    get_combinations(xs, [2, 4, 6, 8, 10, 12])
  def get_combinations([x | xs], [y | ys]) do
    if Enum.count([y | ys]) > 0 do
      [[x, y]] ++ get_combinations([x | xs], ys)
    end
  end

  def get_dices(list, combinations) do
    Enum.map(combinations, fn [a, b] -> if a <= Enum.min(list) and
                              a * b >= Enum.max(list) do [a, b] end end)
      |> Enum.reject(fn x -> x == nil end)
      |> filter_dice(0)
  end

  def filter_dice([], _), do: []
  def filter_dice([head | tail], counter) do
    cond do
      hd(head) == counter -> [head] ++ filter_dice(tail, counter + 1)
      hd(head) == counter - 1 -> filter_dice(tail, counter)
      true -> filter_dice([head | tail], counter + 1)
    end
  end

  def get_mode(list) do
    Enum.reduce(list, %{}, fn x, acc -> Map.update(acc, x, 1, &(&1 + 1)) end)
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def get_data do
    String.split(IO.read(:stdio, :all), "\n")
      |> List.delete_at(3)
      |> Enum.map(&String.split(&1, " "))
      |> Enum.map(&to_int/1)
      |> Enum.map(&Enum.slice(&1, 0..length(&1) - 2))
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e DungeonsandDragonsDice.main
# 1d4 2d10 1d2
