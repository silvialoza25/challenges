;; $clj-kondo --lint nickkar.clj
;; linting took 33ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class))

(defn read-input ([n] (read-input n 0 []))
  ([n i out]
    (cond
      (>= i n) out
      :else (read-input n (inc i) (conj out [(read) (read) (read)])))))

(defn xtnd-GCD [a b]
  (cond
    (= b 0) [a 1 0]
    :else
    (let [[g x y] (xtnd-GCD b (mod a b))]
      [g y (- x (* y (quot a b)))])))

(defn solve-cases ([n numbers] (solve-cases n numbers 0 []))
  ([n numbers i out]
    (cond
      (>= i n) out
      :else
      (let [[M A B] (nth numbers i)
            [r a] (xtnd-GCD A M)
            x (* a (- 0 B))]
        (cond
          (= r 1)
          (solve-cases n numbers (inc i) (conj out (mod x M)))
          :else
          (solve-cases n numbers (inc i) (conj out -1)))))))

(defn main []
  (let [n (read)
        num (read-input n)]
    (apply println (flatten (solve-cases n num)))))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; 1658128 39031599 18275 -1 33179 30326514 -1 -1 536575 24744627 -1 -1 -1 -1 -1
;; 2109734 758150978 16436 -1 14366837 38742 336430 315862220 -1 6237210
;; 214655567 -1 -1
