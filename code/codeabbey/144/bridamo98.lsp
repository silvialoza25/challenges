#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun custom-gcd(x y)
  (let ((s-prev 1)(s-cur 0)(t-prev 0)(t-cur 1)(x-initial x)
  (y-initial y)(x-aux x)(y-aux y)(q NIL)(r NIL)(aux NIL))
    (loop
    (if (> x-aux y-aux)
      (progn
        (setq q (floor x-aux y-aux))
        (setq r (mod x-aux y-aux))
        (setq x-aux y-aux)
        (setf y-aux r))
      (progn
        (setq q (floor y-aux x-aux))
        (setq r (mod y-aux x-aux))
        (setf y-aux x-aux)
        (setf x-aux r)))
    (if (not(= r 0))
      (progn
        (setq aux s-cur)
        (setq s-cur (- s-prev (* q s-cur)))
        (setq s-prev aux)
        (setq aux t-cur)
        (setq t-cur (- t-prev (* q t-cur)))
        (setq t-prev aux))
      (progn
        (if (> x-initial y-initial)
          (progn
            (setq aux (+ (* s-cur x-initial)(* t-cur y-initial)))
            (return-from custom-gcd (list aux s-cur t-cur)))
          (progn
            (setq aux (+ (* t-cur x-initial)(* s-cur y-initial)))
            (return-from custom-gcd (list aux t-cur s-cur))))))
    (when (= r 0) (return r)))))

(defun solve-single-problem (M A B)
  (let ((gcd-params (custom-gcd A M))
  (r NIL) (a NIL) (x-aux NIL))
    (setq r (nth 0 gcd-params))
    (setq a (nth 1 gcd-params))
    (if (= r 1) (progn
        (if (< a 0)
          (setq a (+ a (* (ceiling (* a (- 0 1)) M) M))))
        (setq x-aux (* a B (- 0 1)))
        (return-from solve-single-problem
        (+ x-aux (* (ceiling (* x-aux (- 0 1)) M) M))))
      (return-from solve-single-problem (- 0 1)))))

(defun main()
  (let ((size-input (read)))
    (loop for i from 0 to (- size-input 1)
      do(format t "~a " (solve-single-problem
        (read) (read) (read))))))

(main)

#|
  cat DATA.lst | clisp bridamo98.lsp
  1658128 39031599 18275 -1 33179 30326514 -1 -1 536575
  24744627 -1 -1 -1 -1 -1 2109734 758150978 16436 -1
  14366837 38742 336430 315862220 -1 6237210
  214655567 -1 -1
|#
