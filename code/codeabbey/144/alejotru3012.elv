# elvish -compileonly alejotru3012.elv

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn gcd_const [s_prev s_cur t_prev t_cur var_x var_y]{
  var_r = (% $var_x $var_y)
  if (== $var_r 0) {
    put [$var_y $s_cur $t_cur]
  } else {
    var_q = (math:trunc (/ $var_x $var_y))
    s_next = (- $s_prev (* $var_q $s_cur))
    t_next = (- $t_prev (* $var_q $t_cur))
    gcd_const $s_cur $s_next $t_cur $t_next $var_y $var_r
  }
}

fn gcd [var_x var_y]{
  const = (gcd_const 1 0 0 1 $var_x $var_y)
  put [$@const]
}

fn to_range [var_m num]{
  if (>= $num 0) {
    put $num
  } else {
    to_range $var_m (+ $var_m $num)
  }
}

fn mod [num_1 num_2]{
  aux = (% $num_1 $num_2)
  if (< $aux 0) {
    put (+ $aux $num_2)
  } else {
    put $aux
  }
}

fn modular_inverse [var_m var_a var_b]{
  gcd_vals = (gcd $var_a $var_m)
  if (== $gcd_vals[0] 1) {
    gcd_a = (to_range $var_m $gcd_vals[1])
    echo (mod (* -$var_b $gcd_a) $var_m)
  } else {
    echo -1
  }
}

fn calculate [data]{
  each [values]{
    modular_inverse $values[0] $values[1] $values[2]
  } $data
}

echo (str:join ' ' [(calculate (input_data))])

# cat DATA.lst | elvish alejotru3012.elv
# 20151823 -1 -1 72955 3900902 130146 297625 -1 -1 -1
# 139590782 -1 438445 -1 -1 -1 681878 -1 73702 1447373
# -1 4326 47811 916858 -1 -1 44130380 297808640 -1 60018098
