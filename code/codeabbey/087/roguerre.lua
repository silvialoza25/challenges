--[[
$ luacheck roguerre.lua
Checking roguerre.lua                             OK
Total: 0 warnings / 0 errors in 1 file
--]]
-- imports to ease in array pretty printing
local ArrPrinter = require 'pl.pretty'

-- reads lines from stdin
local readLines = function ()
  local data = io.read("*a")
  local lines = {}
  for line in data:gmatch("[^\r\n]+") do
    table.insert(lines, line)
  end
  return lines
end

-- parses a line of values into an array of numbers
local readNumbers = function (line)
  local nums = {}
  for strNum in line:gmatch("%w+") do
    table.insert(nums, tonumber(strNum))
  end
  return nums
end

-- gets subarray from array
local getSubArray = function(array, idx1, idx2)
  local subArray = {}
  for count = idx1, idx2 do
    table.insert(subArray, array[count])
  end
  return subArray
end

-- inserts new leaf whose value is number
local insertLeaf = function (rootNode, number)
  local currentNode = rootNode
  local currentValue = rootNode[2]
  while true do
    local previousNode = currentNode
    local insertAt
    if number < currentValue then
      -- go to the left branch
      currentNode = currentNode[1]
      insertAt = 1
    else
      -- go to the right branch
      currentNode = currentNode[3]
      insertAt = 3
    end
    -- break condition
    if currentNode == "-" then
      previousNode[insertAt] = {"-", number, "-"}
      break
    else
      currentValue = currentNode[2]
    end
  end
end

-- builds the tree as an array of nodes
local buildTree = function (rootNode, otherNumbers)
  -- find current number position in the tree
  for index = 1, #otherNumbers do
    insertLeaf(rootNode, otherNumbers[index])
  end
end

-- builds a formatted string from a table
local buildStrRepr = function (arr)
  local asString = ArrPrinter.write(arr, "")
  -- turning { or } into ( or )
  asString = string.gsub(asString, "{", "(")
  asString = string.gsub(asString, "}", ")")
  -- deleting "
  asString = string.gsub(asString, '"', "")
  return asString
end

local lines = readLines()
local numbers = readNumbers(lines[2])

local rootNode = {"-", numbers[1], "-"}
local otherNumbers = getSubArray(numbers, 2, lines[1])

-- building the tree, no return value, modifying in place
buildTree(rootNode, otherNumbers)

-- printing the solution
print(buildStrRepr(rootNode))

--[[
$ cat DATA.lst | lua roguerre.lua
(((-,1,((-,2,((-,3,(-,4,-)),5,-)),6,(((-,7,(-,8,-))...
--]]
