{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

replaceElement : Nat -> a -> List a -> List a
replaceElement index x xs =
  let
    left = take index xs
    right = drop (index + 1) xs
  in left ++ [x] ++ right

elemAt : Nat -> List a -> a
elemAt 0 (x::xs) = x
elemAt index (_::xs) = elemAt (minus index 1) xs

injectValue : a -> IO a
injectValue dat = pure dat

getInputData : IO(List String)
getInputData = do
  line <- getLine
  if line == ""
    then injectValue []
    else do
      xs <- getInputData
      injectValue (line :: xs)

createTreeArr : Nat -> List (String)
createTreeArr size = replicate size "-"

insertNode : List String -> String -> Nat -> List String
insertNode tree node index =
  let
    modTree =
      if index < (length tree)
        then tree
        else tree ++ (replicate ((minus index (length tree)) + 1) "-")
    res =
      if (elemAt index modTree) == "-"
        then replaceElement index node modTree
        else if (cast {to=Int} (elemAt index modTree)) < (cast {to=Int} node)
          then insertNode modTree node (index * 2 + 2)
          else insertNode modTree node (index * 2 + 1)
  in res

fillEmptyArr : List String -> List String -> List String
fillEmptyArr tree [] = tree
fillEmptyArr tree nodes =
  let
    node = fromMaybe "" (head' nodes)
    nxtTree = insertNode tree node 0
  in fillEmptyArr nxtTree (drop 1 nodes)

buildTreeDescription : List String -> Nat -> String
buildTreeDescription tree index =
  let
    curNode =
      if index < (length tree)
        then elemAt index tree
        else "-"
    res =
      if curNode == "-"
        then curNode
        else "(" ++ (buildTreeDescription tree (index * 2 + 1)) ++ "," ++
          curNode ++ "," ++ (buildTreeDescription tree (index * 2 + 2)) ++ ")"
  in res

main : IO ()
main = do
  inputData <- getInputData
  let
    nodesNum = cast {to=Int} (fromMaybe "" (head' inputData))
    nodes = words (fromMaybe "" (last' inputData))
    tree = fillEmptyArr [] nodes
    solution = [buildTreeDescription tree 0]
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  (((((((-,1,-),2,((-,3,(-,4,-)),5,-)),6,-),7,((-,8,(-,9,-))
  ,10,(-,11,(((-,12,-),13,-),14,(-,15,-))))),16,((-,17,(-,18,-))
  ,19,(-,20,-))),21,((-,22,-),23,(-,24,-))),25,(-,26,((-,27,-),28,-)))
-}
