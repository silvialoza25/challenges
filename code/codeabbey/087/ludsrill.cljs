;; clj-kondo --lint ludsrill.cljs
;; linting took 54ms, errors: 0, warnings: 0

(ns ludsrill.087
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn build-tree [tree next-value]
  (let [root (nth tree 1)
        left (nth tree 0)
        right (nth tree 2)]
    (if (= root "-")
      (assoc tree 1 next-value)
      (if (> root next-value)
        (if (= left "-")
          (assoc tree 0 ["-" next-value "-"])
          (assoc tree 0 (build-tree left next-value)))
        (if (= right "-")
          (assoc tree 2 ["-" next-value "-"])
          (assoc tree 2 (build-tree right next-value)))))))

(defn print-tree [tree]
  (when (seq tree)
    (if (vector? (first tree))
      (cons (list (symbol (str/join "," (print-tree (first tree)))))
        (print-tree (rest tree)))
      (cons (first tree) (print-tree (rest tree))))))

(defn -main []
  (let [data (first (rest (get-data)))
        solution (reduce (fn [acc, x] (build-tree acc (Integer. x)))
          ["-" "-" "-"] data)]
    (print (list (str/join "," (print-tree solution))))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; (((((-,1,-),2,(-,3,((-,4,-),5,-))),6,((-,7,-),8,((-,9,-),10,((-,11,-),12,
;; (-,13,-))))),14,(((-,15,(-,16,-)),17,(-,18,(((-,19,-),20,-),21,(-,22,-))))
;; ,23,(-,24,-))),25,(((-,26,(((-,27,-),28,-),29,-)),30,(-,31,-)),32,((-,33,-)
;; ,34,(-,35,(-,36,(-,37,-))))))
