;; $clj-kondo --lint nickkar.clj
;; linting took 33ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class))

(defn gems ([ca5es] (gems ca5es 0 []))
  ([ca5es i out]
   (cond
     (>= i ca5es) out
     :else
     (let [N (read)
           T (read)]
       (gems ca5es (inc i) (conj out (* N (dec T))))
       )
     )
   )
  )

(defn main []
  (let [ca5es (read)]
    (apply println (gems ca5es))))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; 27 68 44 85 28 14 57
