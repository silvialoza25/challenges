-- $ curry-verify juandiegoe.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `juandiegoe'!

import IO
import List

parseInt :: String -> Int
parseInt str = read str :: Int

getLines :: IO String
getLines = do
  dataLine <- getContents
  return dataLine

getValues :: [[Prelude.Char]] -> Prelude.Int
getValues arr = result
  where
    numA = parseInt (arr !! 0)
    numB = parseInt (arr !! 1)
    result = (numA * (numB - 1))

main :: Prelude.IO ()
main = do
  input <- getLines
  let
    values = (split (== '\n') input)
    dropData = drop 1 (init values)
    cleanData = map words dropData
    result = map (\x -> show (getValues x)) cleanData
  putStrLn (unwords result)

-- cat DATA.lst | pakcs :load juandiegoe.curry :eval main :quit
-- 57 20 19 32 45 64 34
