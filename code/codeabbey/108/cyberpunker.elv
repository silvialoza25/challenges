# elvish -compileonly cyberpunker.elv

#!/bin/elvish

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn starMedals [data]{
  each [values]{
    var rays = $values[0]
    var step = $values[1]
    var draw = (* $rays (- $step 1))
    put $draw
  } $data
}

echo (starMedals (input_data))

# cat DATA.lst | elvish cyberpunker.elv
# 57 20 19 32 45 64 34
