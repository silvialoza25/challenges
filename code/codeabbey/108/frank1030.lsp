#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

$ clisp -c frank1030.lsp
Compiling file /Users/Frank/Desktop/frank/108/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/108/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-dat()
  (let ((dat))
    (setq dat(read))
  )
)

(defun solution()
  (let ((index 0) (n 0) (tt 0) (r 0))
    (setq index(get-dat))
    (loop for i from 0 to (1- index)
      do (setq n(get-dat))
      do (setq tt(get-dat))
      do (setq r(* n (- tt 1)))
      do (print r)
    )
  )
)

(solution)

#|
$ cat DATA.lst | clisp frank1030.lsp

20 51 27 102 32 57 15 11 17
|#
