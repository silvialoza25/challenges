{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

injectValue : a -> IO a
injectValue dat = pure dat

getInputData : IO(List String)
getInputData = do
  line <- getLine
  if line == ""
    then injectValue []
    else do
      xs <- getInputData
      injectValue (line :: xs)

findNumOfGems : List Int -> String
findNumOfGems [rays, steps] = show (rays * (steps - 1))

main : IO ()
main = do
  inputData <- getInputData
  let
    formatInputData = map (\x => map (cast {to=Int}) x)
      (map words (drop 1 inputData))
    solution = map findNumOfGems formatInputData
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  26 15 34 48 22 13
-}
