;$ clj-kondo --lint frank1030.cljs
;linting took 467ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core]))

(defn star-medals [index]
  (let [x (atom index) n (core/read) tt (core/read) res (* n (- tt 1))]
    (if (> @x 0)
      (do (swap! x dec)
        (print res "")
        (if (not= @x 0)
          (star-medals @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (core/read)]
    (star-medals index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;20 51 27 102 32 57 15 11 17
