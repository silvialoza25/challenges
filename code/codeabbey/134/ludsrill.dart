// dartanalyzer ludsrill.dart
// Analyzing ludsrill.dart...
// No issues found!

import 'dart:core';
import 'dart:io';

void main() {
  List<int> input =
      stdin.readLineSync().split(" ").map((item) => int.parse(item)).toList();
  List<int> solution = FlyingTextScreensaver(
      input.elementAt(0), input.elementAt(1), input.elementAt(2), [0, 0], 0);
  print(solution.join(" "));
}

List<int> FlyingTextScreensaver(
    int width, int height, int length, List<int> accumulator, int counter) {
  return FlyingDownRight(width, height, length - 1, accumulator, counter);
}

List<int> FlyingDownLeft(
    int width, int height, int length, List<int> accumulator, int counter) {
  if (counter != 100) {
    int xPosition = accumulator[accumulator.length - 2];
    int yPosition = accumulator[accumulator.length - 1];
    if (xPosition > 0 && yPosition < height - 1) {
      return FlyingDownLeft(width, height, length,
          accumulator + [xPosition - 1, yPosition + 1], counter + 1);
    } else if (xPosition == 0) {
      return FlyingDownRight(width, height, length, accumulator, counter);
    } else {
      return FlyingUpLeft(width, height, length, accumulator, counter);
    }
  } else {
    return accumulator;
  }
}

List<int> FlyingDownRight(
      int width, int height, int length, List<int> accumulator, int counter) {
  if (counter != 100) {
    int xPosition = accumulator[accumulator.length - 2];
    int yPosition = accumulator[accumulator.length - 1];
    if (xPosition + length < width - 1 && yPosition < height - 1) {
      return FlyingDownRight(width, height, length,
          accumulator + [xPosition + 1, yPosition + 1], counter + 1);
    } else if (xPosition + length == width - 1) {
      return FlyingDownLeft(width, height, length, accumulator, counter);
    } else {
      return FlyingUpRight(width, height, length, accumulator, counter);
    }
  } else {
    return accumulator;
  }
}

List<int> FlyingUpLeft(
      int width, int height, int length, List<int> accumulator, int counter) {
  if (counter != 100) {
    int xPosition = accumulator[accumulator.length - 2];
    int yPosition = accumulator[accumulator.length - 1];
    if (xPosition > 0 && yPosition > 0) {
      return FlyingUpLeft(width, height, length,
          accumulator + [xPosition - 1, yPosition - 1], counter + 1);
    } else if (xPosition == 0) {
      return FlyingUpRight(width, height, length, accumulator, counter);
    } else {
      return FlyingDownLeft(width, height, length, accumulator, counter);
    }
  } else {
    return accumulator;
  }
}

List<int> FlyingUpRight(
      int width, int height, int length, List<int> accumulator, int counter) {
  if (counter != 100) {
    int xPosition = accumulator[accumulator.length - 2];
    int yPosition = accumulator[accumulator.length - 1];
    if (xPosition + length < width - 1 && yPosition > 0) {
      return FlyingUpRight( width, height, length,
          accumulator + [xPosition + 1, yPosition - 1], counter + 1);
    } else if (xPosition + length == width - 1) {
      return FlyingUpLeft(width, height, length, accumulator, counter);
    } else {
      return FlyingDownRight(width, height, length, accumulator, counter);
    }
  } else {
    return accumulator;
  }
}

// cat DATA.lst | dart ludsrill.dart
// 0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13 14 14 15 15
// 16 16 17 17 18 16 19 15 20 14 21 13 22 12 23 11 24 10 25 9 26 8 27 7 28 6 29
// 5 30 4 31 3 32 2 33 1 34 0 35 1 36 2 37 3 36 4 35 5 34 6 33 7 32 8 31 9 30
// 10 29 11 28 12 27 13 26 14 25 15 24 16 23 17 22 16 21 15 20 14 19 13 18 12
// 17 11 16 10 15 9 14 8 13 7 12 6 11 5 10 4 9 3 8 2 7 1 6 0 5 1 4 2 3 3 2 4
// 1 5 0 6 1 7 2 8 3 9 4 10 5 11 6 12 7 13 8 14 9 15 10 16 11 17 12 16 13 15
// 14 14 15 13 16 12 17 11 18 10 19 9 20 8 21 7 22 6 23 5 24 4 25 3 26 2
