;; $ clj-kondo --lint bridamo98.cljs
;; linting took 29ms, errors: 0, warnings: 0

(ns bridamo98-134
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn abs [number] (max number (- number)))

(defn find-new-delta [coordinate delta up-limit]
  (let [new-delta (if (= coordinate 0)
  (abs delta) (if (= coordinate up-limit)
  (* (abs delta) (- 0 1)) delta))]
    new-delta))

(defn simulate-flying-text [content-input]
  (let [width (edn/read-string (content-input 0))
  height (edn/read-string (content-input 1))
  length (edn/read-string (content-input 2))]
    (loop [step 0 x 0 y 0 dx 1 dy 1 result ""]
      (if (> step 100)
        result
        (let [new-dx (find-new-delta x dx (- width length))
        new-dy (find-new-delta y dy (- height 1))
        new-result (str result x " " y " ")]
          (recur (+ step 1) (+ x new-dx) (+ y new-dy)
          new-dx new-dy new-result))))))

(defn main []
  (let [content-input (str/split (core/read-line) #" ")]
    (println (simulate-flying-text content-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.clj
;; 18 102
