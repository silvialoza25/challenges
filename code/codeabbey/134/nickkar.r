# $ lintr::lint('nickkar.r')

# checks if the text reached the borders, moves the text and calls again
# simulate to store the new values
move <- function(height, width, len, steps, step, x, y, stepx, stepy, index,
out) {
  rigth <- x + len + stepx
  left <- x + stepx
  vert <- y + stepy
  if ((rigth > width || left < 0) && (vert >= height || vert < 0)) {
    simulate(height, width, len, steps, step, x - stepx, y - stepy, -stepx,
    -stepy, index + 1, out)
  }
  else if (rigth > width || left < 0) {
    simulate(height, width, len, steps, step, x - stepx, y + stepy, -stepx,
    stepy, index + 1, out)
  }
  else if (vert >= height || vert < 0) {
    simulate(height, width, len, steps, step, x + stepx, y - stepy, stepx,
    -stepy, index + 1, out)
  }
  else {
    simulate(height, width, len, steps, step, x + stepx, y + stepy, stepx,
    stepy, index + 1, out)
  }
}

# stores the current coordinates of the text and calls the move function
simulate <- function(height, width, len, steps, step, x=0, y=0,
stepx=step, stepy=step, index=1, out=c()) {
  if (index > steps) {
    return(c(out, x, y))
  }
  else {
    move(height, width, len, steps, step, x, y, stepx, stepy, index,
    c(out, x, y))
  }
}

# Driver code
main <- function() {
  data <- readLines("stdin")
  width <- as.integer(unlist(strsplit(data, " "))[1])
  height <- as.integer(unlist(strsplit(data, " "))[2])
  len <- as.integer(unlist(strsplit(data, " "))[3])
  steps <- 100
  step <- 1
  cat(simulate(height, width, len, steps, step), "\n")
}

main()

# $ cat DATA.lst | Rscript nickkar.r
# 0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13 14 14 15 15
# 16 16 17 17 18 16 19 15 20 14 21 13 22 12 23 11 24 10 25 9 26 8 27 7 28 6 29
# 5 30 4 31 3 32 2 33 1 34 0 35 1 36 2 37 3 36 4 35 5 34 6 33 7 32 8 31 9 30 10
# 29 11 28 12 27 13 26 14 25 15 24 16 23 17 22 16 21 15 20 14 19 13 18 12 17 11
# 16 10 15 9 14 8 13 7 12 6 11 5 10 4 9 3 8 2 7 1 6 0 5 1 4 2 3 3 2 4 1 5 0 6 1
# 7 2 8 3 9 4 10 5 11 6 12 7 13 8 14 9 15 10 16 11 17 12 16 13 15 14 14 15 13 16
# 12 17 11 18 10 19 9 20 8 21 7 22 6 23 5 24 4 25 3 26 2
