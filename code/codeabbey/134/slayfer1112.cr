#! /usr/bin/crystal

# $ ameba --all --fail-level Convention slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 4.19 milliseconds
# $ crystal build slayfer1112.cr

def data_entry
  values = [] of Int32
  args = gets
  if args
    dat = args.split
    val1 = dat[0].to_i
    val2 = dat[1].to_i
    val3 = dat[2].to_i
    values << val1
    values << val2
    values << val3
    values
  end
end

def solution(array)
  w = array[0]
  h = array[1]
  l = array[2]
  x = 1
  y = 1
  pos = [0, 0]
  print "#{pos.join(" ")} "
  1.step(to: 100, by: 1) do |_|
    pos[0] += x
    pos[1] += y
    if (pos[0] + l) > w || pos[0] < 0
      x = -1*x
      pos[0] += x + x
    end
    if pos[1] >= h || pos[1] < 0
      y = -1*y
      pos[1] += y + y
    end
    print "#{pos.join(" ")} "
  end
end

data = data_entry()
data ? solution(data) : print "Invalid data "
puts

# $ cat DATA.lst | ./slayfer1112
# 0 0 1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13
# 14 14 15 15 16 14 17 13 18 12 19 11 20 10 21 9 22 8 23 7 24 6 25
# 5 26 4 27 3 28 2 29 1 30 0 31 1 30 2 29 3 28 4 27 5 26 6 25 7 24
# 8 23 9 22 10 21 11 20 12 19 13 18 14 17 15 16 14 15 13 14 12 13
# 11 12 10 11 9 10 8 9 7 8 6 7 5 6 4 5 3 4 2 3 1 2 0 1 1 0 2 1 3
# 2 4 3 5 4 6 5 7 6 8 7 9 8 10 9 11 10 12 11 13 12 14 13 15 14 14
# 15 13 16 12 17 11 18 10 19 9 20 8 21 7 22 6 23 5 24 4 25 3 26 2
# 27 1 28 0 29 1 30 2 31 3 30 4 29 5 28 6 27 7 26 8 25 9 24 10
