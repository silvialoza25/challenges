;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))

(defn get-vals [line]
  (let [values (str/split line #" ")
        val1 (read-string (values 0))
        val2 (read-string (values 1))]
    [val1 val2]))

(defn solution [_ body]
  (doseq [x body]
    (let [[val1 val2] (get-vals x)
          val3 (Math/round (double (/ val1 val2)))]
      (print (str val3 " ")))))

(defn main []
  (let [[head body] (get-data)]
    (solution head body)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 2 5 4 91528 8 -4 18117 30841 -1 -19 6 -90 10 10 13 4 7
