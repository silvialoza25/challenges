// ponyc -b asalgad2

use "collections"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail( array: Array[String]): Array[String] =>
          array.slice(1, array.size())

        fun execute( values: Array[String] ) ?=>
          if values.size() > 0 then
            var params: Array[String] = values(0)?.split( " " )
            var const_a: F64 = params(0)?.f64()?
            var const_b: F64 = params(1)?.f64()?

            var result: F64 = ( const_a / const_b).round()
            env.out.write( result.i64().string() )
            env.out.write( " " )

            execute( tail(values) )?
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")
          var useful_lines: Array[String] = lines.slice(1, lines.size()-1)

          try
            execute( useful_lines )?
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// 8 11 -1 13 9 -2 33185 10 11 17 2543 -83 10227 19219 3440 7 12 14 10 3166 4295
// 30188 1 19
