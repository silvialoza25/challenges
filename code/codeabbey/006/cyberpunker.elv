# elvish -compileonly cyberpunker.elv

#!/bin/elvish

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn roundValue [valueA valueB]{
  var div = (/ $valueA $valueB)
  var result = (math:floor (+ $div 0.5))
  put $result
}

fn main [data]{
  each [values]{
    var testA = $values[0]
    var testB = $values[1]
    roundValue $testA $testB
  } $data
}

echo (main (input_data))

# cat DATA.lst | elvish cyberpunker.elv

# 8 -10 13 7 7 11 4810 4 -2 5 9 8 13501 12 30 13 1
# 3747 31832 16 -11 7713 7961 19 1330 20 7 2
