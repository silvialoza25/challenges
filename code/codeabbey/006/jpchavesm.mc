/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, float, char.

:- pred div(float::in, float::in, float::out) is det.
div(X, Y, X / Y).

:- pred str2float(string::in, float::out) is det.
str2float(X, string.det_to_float(X)).

:- pred roundNums(list(string), string, string).
:- mode roundNums(in, in, out) is det.
roundNums([], FinalRound, strip(FinalRound)).
roundNums([Line | Tail], PartialRound, FinalRound) :-
(
  words_separator(char.is_whitespace, Line) = StrList,
  map(str2float, StrList, FloatList),
  foldr(div, FloatList, 1.0, Quot),
  PartialRound1 = PartialRound ++ " " ++ from_int(round_to_int(Quot)),
  roundNums(Tail, PartialRound1, FinalRound)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1,FileContents,RoundList),
    roundNums(RoundList, "", ResultStr),
    io.print_line(ResultStr, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  5 10 16 23 2 -2 5 1 7782 20676 6 3 12 14 16
  20 21 2 8 3994 -4 5899 -5 14353 0 132381
*/
