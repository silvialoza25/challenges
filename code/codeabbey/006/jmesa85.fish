# $ fish --no-execute jmesa85.fish
# $

# CodeAbbey #006: Rounding - Fish Shell

function main
  echo $argv | awk '
    NR > 1 {
    division = $1 / $2;
    rounded = (division > 0)? division + 0.5 : division - 0.5;
    printf("%d\n", rounded);
    }' | awk '{printf $0" "}'
end

main $argv

# $ fish jmesa85.fish "$(cat DATA.lst)"
# 5 10 16 23 2 -2 5 1 7782 20676 6 3 12 14 16
# 20 21 2 8 3994 -4 5899 -5 14353 0 132381
