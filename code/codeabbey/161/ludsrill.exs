# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.06 seconds (0.01s to load, 0.05s running 55 checks on 1 file)
# 12 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule TopologicalSorting do
  def main do
    data = get_data()
    entries = Enum.uniq(get_initials(data))
    Enum.uniq(topological_sorting(entries, data, [], data))
     |> Enum.map(&IO.write("#{&1} "))
  end

  def get_data do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    [_ | tail] = List.delete_at(all_data, 1 +
                  String.to_integer(Enum.at(all_data, 0)))
    Enum.map(tail, &String.split(&1, " "))
  end

  def topological_sorting([], _data, acc, _new_data), do: acc
  def topological_sorting([head | tail], data, acc, new_data) do
    s = [head | tail]
    new = changing(data, s, new_data)
    topological_sorting(hd(new), List.last(new), acc ++ [head], List.last(new))
  end

  def changing([], s, org_data), do: [tl(s), org_data]
  def changing(data, s, org_data) do
    [head | tail] = data
    [x | _] = s
    if x == hd(head) do
      new = review(List.delete(org_data, [x, List.last(head)]), s, x,
              List.last(head), org_data)
      changing(tail, hd(new), List.last(new))
    else
      changing(tail, s, org_data)
    end
  end

  def review([], s, a, value, data), do: [s ++ [value], List.delete(data,
                                          [a, value])]
  def review([head | tail], s, a, value, data) do
    if value != List.last(head) do
      review(tail, s, a, value, data)
    else
      [s, List.delete(data, [a, value])]
    end
  end

  def get_initials(data) do
    Enum.reduce(data, [], fn [a, _], acc -> acc ++ [a] end)
      |> Enum.map(&compare(&1, data))
      |> Enum.reject(fn x -> x == nil end)
  end

  def compare(value, []), do: value
  def compare(value, [x | xs]) do
    if value == List.last(x) do
      nil
    else
      compare(value, xs)
    end
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e TopologicalSorting.main
# Vd Fh Ox Ci Bz Na Id Hi Fu Np Ti Kl Eo Hg Yu Bc Wk Vs Nl Ay Ej Lq Km Tv Bi Kn
# Kv Pp Cg Sl Sn Xg Lt Aj Ri Le Bj Hq Gu Tr Vf Jc En Nk Pw Ty Vr Aa Ec Ea Jx Et
# Gd Jm Dl Dk Kj Sh Ku Ws Nv Xv Tm Mh Oh Sa Fi Kt Sd Pk Fm Mb Hl Yi Oz Tl Ee In
# Xs Ww Qf Vg Fx Vv Jg Pe Qw Bm Ou Gp Od Jo Ai Ij Vi Aq Of Gf Bs Kp Zv Zk Dx Al
# Oc Vm Ex Lg Di Xx Yx Yz Oj Ho Uq Xl Ic Cd Es So Yk Jj Br Xn Yg Bq Xk Dh Oo Js
# Ng Hc Lp Gj Wu Rk Iy Ys Dj Fq Gm Uj Nz Te Um Py Tt Ny Fl Zw Qy Fe Nx Zt Ir
