#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defvar *data*)
(defvar *first-value*)
(defvar *step*)
(defvar *number-iterations*)
(defparameter arithmetic-progression 0)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(setq *data* (cdr (read-data)))

(dolist (item *data*)
  (setq *first-value* (first item))
  (setq *step* (second item))
  (setq *number-iterations* (third item))

  (loop for i from 0 to (- *number-iterations* 1)
    do(setq arithmetic-progression (+ arithmetic-progression
        (+ *first-value*  (* i *step*)))))
  (format t "~s " arithmetic-progression)
  (setq arithmetic-progression 0))

#|
cat DATA.lst | clisp ludsrill.lsp
23310 13593 48954 1358 76285 20254 2954 495 6237 41495 3162
|#
