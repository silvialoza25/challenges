# elvish -compileonly alejotru3012.elv

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn sum_of_members [init_val step index sum]{
  if (== $index 1) {
    echo $sum
  } else {
    next_val = (+ $init_val $step)
    sum_of_members $next_val $step (- $index 1) (+ $sum $next_val)
  }
}

fn calculate [data]{
  each [values]{
    sum_of_members $values[0] $values[1] $values[2] $values[0]
  } $data
}

echo (str:join ' ' [(calculate (input_data))])

# cat DATA.lst | elvish alejotru3012.elv
# 24153 29870 13804 1809 2170 29535 1898 53250 42133 6424 2987
