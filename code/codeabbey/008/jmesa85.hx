/**
  $ haxelib run checkstyle \
    --exitcode \
    --show-parser-errors \
    --nothreads \
    --default-config \
    -s jmesa85.hx
**/

// CodeAbbey #008: Arithmetic Progression - Haxe

using StringTools;

class Main {
  static public function main():Void {
    var data = Sys.stdin().readAll().toString().trim().split("\n");
    var testCasesStr:Array<String> = data.slice(1);
    var results = testCasesStr.map(processTestCase);
    trace(results.join(" "));
  }

  static function processTestCase(testCaseStr:String):Int {
    var testCase = testCaseStr.split(" ");
    var first = Std.parseInt(testCase[0]);
    var stepSize = Std.parseInt(testCase[1]);
    var numberValues = Std.parseInt(testCase[2]);
    var aSubN = first + (numberValues - 1) * stepSize;
    return Std.int(numberValues * (first + aSubN) / 2);
  }
}

/**
  $ cp jmesa85.hx Main.hx && cat DATA.lst | haxe -main Main --interp
  7238 1881 31730 2120 3024 6321 44714 17820 56160
**/
