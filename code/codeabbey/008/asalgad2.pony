// ponyc -b asalgad2

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail( array: Array[String] ): Array[String] =>
          array.slice(1, array.size())

        fun progression( const_a: I64, const_b: I64, const_n: I64 ): I64 =>
          if const_n > 0 then
            const_a + progression(const_a+const_b, const_b, const_n-1)
          else
            0
          end

        fun iterate( lines: Array[String] ) ? =>
          if lines.size() > 0 then
            var values: Array[String] = lines(0)?.split(" ")
            var const_a: I64 = values(0)?.i64()?
            var const_b: I64 = values(1)?.i64()?
            var const_n: I64 = values(2)?.i64()?

            var result: I64 = progression( const_a, const_b, const_n )
            env.out.write( result.string() )
            env.out.write( " " )

            iterate( tail(lines) )?
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")
          var useful_lines: Array[String] = lines.slice(1, lines.size()-1)

          try
            iterate( useful_lines )?
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// 1425 60294 3220 12282 4185 1340 20619 13860
