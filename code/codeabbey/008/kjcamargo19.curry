-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import IO (getContents)
import List (split, init)
import Read (readInt)

getData :: IO String
getData = do
  content <- getContents
  return content

getSumMembers :: Int -> Int -> Int -> Int -> Int
getSumMembers constA constB constN sum
  | constN == 1 = sum
  | otherwise =
    let
      newVal = constA + constB
      accum = sum + newVal
    in
      getSumMembers newVal constB (constN -1) accum

getSolution :: String -> Int
getSolution dataIn = result
  where
    fData = map readInt (words dataIn)
    constA = fData !! 0
    constB = fData !! 1
    constN = fData !! 2
    result = getSumMembers constA constB constN constA

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> show (getSolution x)) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 31725 10700 36750 528 16172 19557 369 1666 7602 1316 2934
