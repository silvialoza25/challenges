# $ lintr::lint('nickkar.r')

series <- function(a, b, n, count=a, i=1) {
    if (i >= n) {
        return(count)
    }
    else {
        newc <- count + (a + (i * b))
        series(a, b, n, newc, (i + 1))
    }
}

solveca5es <- function(cases, out = c(), i=1) {
    if (i > length(cases)) {
        return(out)
    }
    else {
        tmp <- unlist(strsplit(cases[i], " "))
        num1 <- as.double(tmp[1])
        num2 <- as.double(tmp[2])
        num3 <- as.double(tmp[3])
        r <- series(num1, num2, num3)
        solveca5es(cases, (append(out, r)), i + 1)
    }
}

main <- function() {
    data <- readLines("stdin")
    out <- solveca5es(data[2:length(data)])
    cat(out)
    cat("\n")
}

main()

# $ cat DATA.lst | rscript nickkar.r
# 650 960 936 36134 17125 4984 4745 2466
