;; $ clj-kondo --lint bridamo98.clj
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-008
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn find-sum-arith-prog[a b n]
  (loop [i 0 res 0]
    (if (< i n)
      (recur (+ i 1) (+ res (+ a (* i b))))
      res)))

(defn solve-all[size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (let [args (map #(Integer/parseInt %)
      (str/split (core/read-line) #" "))]
        (recur (+ i 1)
        (str result (find-sum-arith-prog (nth args 0)
        (nth args 1) (nth args 2)) " ")))
      result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.clj
;; 7238 1881 31730 2120 3024 6321 44714 17820 56160
