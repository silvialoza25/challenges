;; $ clj-kondo --lint miyyer1946.clj
;; linting took 24ms, errors: 0, warnings: 0

(ns miyyer1946-008
  (:gen-class)
  (:require [clojure.core :as core]))

(defn sum-values [a b c]
(loop [sum 0 cnt 0]
    (if (>= cnt c)
        (print (str (+ sum) " "))
    (recur (+ a (* b cnt) sum) (inc cnt))))
)

(defn sums-in-loop [size-input]
  (loop [x 1]
    (when (< x (+ size-input 1))
      (sum-values (core/read) (core/read) (core/read))
      (recur (+ x 1))
    )
  )
)

(defn main []
  (let [size-input (core/read)]
    (sums-in-loop size-input)
  )
)

(main)

;$ cat DATA.lst | clj miyyer1946.cljs
;44028 14265 14469 2717 21482 12818 44354 4495 13104 12000
