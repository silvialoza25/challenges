# elvish -compileonly alejotru3012.elv

use str

fn input_data []{
  put [(each [line]{
    put (str:split ' ' $line)
  })]
}

fn get_character [code]{
  num_bin = (echo (base 2 $code))
  if (== (% (str:count $num_bin 1) 2) 0) {
    if (== (count $num_bin) 8) {
      num_bin = $num_bin[1..]
    }
    new_code = (base 10 "0b"$num_bin)
    echo (str:from-codepoints $new_code)
  }
}

fn get_word [data]{
  each [value]{
    get_character $value
  } $data
}

echo (str:join '' [(get_word (input_data))])

# cat DATA.lst | elvish alejotru3012.elv
# BVONdNdfBKdjJ uMDqR1D t MR2m FcvGQn 4WJFj fkLMhyWOg4O1nlb  GPy1nmtxAlk .
