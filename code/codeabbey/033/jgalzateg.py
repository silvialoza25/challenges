#!/usr/bin/env python3
# $ mypy jgalzateg.py
# Success: no issues found in 1 source file

from typing import List


def supr_corrup(num: int) -> bool:
    return bool(str(bin(num)).count('1') % 2 == 0)


def supr_frst_char(num: int) -> int:
    if num > 127:
        num = num - 128
    return num


def to_str(vec: List[str], i: int) -> str:
    accum = ""
    if i > 0:
        accum = accum + to_str(vec, i - 1) + str(vec[i])
    else:
        accum = accum + str(vec[i])
    return accum


if __name__ == "__main__":
    MSG_INPUT = input("")
    MSG_INT = list(map(int, MSG_INPUT.split(" ")))
    MSG_CLEAN = list(filter(supr_corrup, MSG_INT))
    MSG_READY = list(map(supr_frst_char, MSG_CLEAN))
    MSG_TRANSLATE = list(map(chr, MSG_READY))
    print(to_str(MSG_TRANSLATE, len(MSG_TRANSLATE) - 1))

# cat DATA.lst | python jgalzateg.py
# q7 0QSIPZbqXh91KrQmg0H aCBv 8ANxzX 26f R7EOU99SVlPZcq bjzWw 2.
