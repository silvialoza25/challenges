/*
$ rustfmt rechavar.rs
$ rustc rechavar.rs
*/
use std::io::{self, Read};
use std::str;

fn get_binary(x: &str) -> String {
  let my_int: i64 = x.parse().unwrap();
  let binary = format!("{:b}", my_int);
  if binary.len() < 8 {
    let mut fixed_binary: String = "0".to_owned();
    fixed_binary.push_str(&binary);
    fixed_binary
  } else {
    binary
  }
}

fn count_num_of_bits(s: &String) -> i64 {
  let mut num_of_bits: i64 = 0;
  for i in s.chars() {
    if i == '1' {
      num_of_bits += 1;
    } else {
      continue;
    }
  }
  num_of_bits
}

fn validate_binary(s: &str) -> bool {
  let binary = get_binary(s);
  let num_of_bits: i64 = count_num_of_bits(&binary);
  if num_of_bits % 2 == 0 {
    true
  } else {
    false
  }
}

fn check_msg(array: Vec<&str>) -> Vec<&str> {
  let mut validated_vector: Vec<&str> = Vec::new();
  for i in 0..array.len() {
    if validate_binary(array[i]) {
      validated_vector.push(array[i]);
    }
  }
  validated_vector
}

fn clear_highest_bit(binary: String) -> String {
  if binary.chars().nth(0).unwrap() == '1' && binary.len() == 8 {
    let mut clear_binary: String = "0".to_owned();

    clear_binary.push_str(&binary[1..]);
    clear_binary
  } else {
    binary
  }
}

fn decode_msg(array: Vec<&str>) -> Vec<String> {
  let mut decoded_vector: Vec<String> = Vec::new();
  for code in array {
    let clear_binary: String = clear_highest_bit(get_binary(code));
    decoded_vector.push(clear_binary);
  }
  return decoded_vector;
}

fn get_msg(array: Vec<String>) -> String {
  let mut msg = String::from("");
  for binary in array {
    let intval = u8::from_str_radix(&binary, 2).unwrap();
    msg.push(intval as char);
  }
  msg
}

fn main() -> io::Result<()> {
  let mut buffer = String::new();
  io::stdin().read_to_string(&mut buffer)?;
  let encoded_msg: Vec<&str> = buffer.split(" ").collect();
  let clean_msg = check_msg(encoded_msg);
  let decoded_msg = decode_msg(clean_msg);
  let msg = get_msg(decoded_msg);
  println!("{}", msg);
  Ok(())
}

/*
cat DATA.lst | ./rechavar
Kfn7Iq 39Z wDb JwTbvXz qbS 2 GfDWxFr OFPOI svGoMA8r cunwDBW.
*/
