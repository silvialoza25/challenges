;; clj-kondo --lint ludsrill.cljs
;; linting took 33ms, errors: 0, warnings: 0

(ns ludsrill.033
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn completing-binary [binary]
  (if (< (count binary) 8)
    (completing-binary (str "0" binary))
    [binary]))

(defn convert-to-list [string]
  (remove nil? (map (fn [^Character c] (if (or (= c \1) (= c \0)) c nil))
    (str string))))

(defn decode [binary]
  (cons '0 (rest binary)))

(defn encode [item]
  (let [binary-raw (convert-to-list (completing-binary
                                      (. Integer toString (Integer. item) 2)))]
    [binary-raw]))

(defn solution [item]
  (let [frequencie (frequencies (first item))]
    (if (odd? (Integer. (get frequencie \1)))
      nil
      (print (char (. Integer parseInt
                    (str/join "" (decode (first item))) 2))))))

(defn -main []
  (let [data (first (get-data))]
  (doseq [item data]
    (solution (encode item)))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; QBt3u W5qa7I YPpKigaj Y Gr YmQ TmKRNjQSA8  PO sWkcI CXId G Yg3Ppn .
