# mypy --strict edissons.py
# Success: no issues found in 1 source file

from functools import reduce
from typing import List, Tuple


def get_next(equals: List[bool]) -> str:
    localcounter = 0
    numbers = []
    for equal in equals:
        if equal:
            localcounter += 1
        else:
            numbers.append(bin(localcounter + 1)[2:])
            localcounter = 0

    return reduce(lambda a, b: a + b, numbers)


def count_steps(data: str, steps: int) -> Tuple[str, int]:
    steps += 1
    compare = data[1:] + "x"
    equals = map(lambda x, y: x == y, data, compare)
    data = get_next(list(equals))
    if data != "10":
        return count_steps(data, steps)

    return data, steps


def main() -> None:
    data = input()
    counter = filter(lambda x: x == "1", data)
    ones = len(list(counter))
    _, steps = count_steps(data, 0)
    print(steps, 2**(ones - 1))


if __name__ == "__main__":
    main()


# $ cat data.lst | python3 edissons.py
# 26 2305843009213693952
