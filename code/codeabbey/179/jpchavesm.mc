/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, integer, char.

:- pred getOnes(char::in, int::in, int::out) is det.
getOnes(Char, Acc0, Acc1) :-
(
  if Char = '1'
  then Acc1 = Acc0 + 1
  else Acc1 = Acc0
).

:- pred condenseList(string::in, string::in, string::out) is det.
condenseList(X, Y, X ++ Y).

:- pred computeStep(list(char), list(char), list(string), string).
:- mode computeStep(in, in, in, out) is det.
computeStep([], SubList, NewList, NewSequence) :-
(
  length(SubList) = Length,
  int_to_base_string(Length, 2, BinLength),
  append(NewList, [BinLength], NewList1),
  foldr(condenseList, NewList1, "", NewSequence)
).
computeStep([Char | Tail], SubList, NewList, NewSequence) :-
(
  if is_empty(SubList)
  then computeStep(Tail, [Char], NewList, NewSequence)
  else if det_last(SubList, Char)
  then computeStep(Tail, SubList ++ [Char], NewList, NewSequence)
  else
    list.length(SubList) = Length,
    int_to_base_string(Length, 2, BinLength),
    append(NewList, [BinLength], NewList1),
    computeStep(Tail, [Char], NewList1, NewSequence)
).

:- pred computeSteps(string::in, int::in, int::out) is det.
computeSteps(Sequence, Steps, Result) :-
(
  if Sequence = "10"
  then Result = Steps
  else
    to_char_list(Sequence) = SeqList,
    computeStep(SeqList, [], [], NewSequence),
    computeSteps(NewSequence, Steps + 1, Result)
).

:- pred lookNSay(string::in, string::out) is det.
lookNSay(BinSequence, Result) :-
(
  computeSteps(BinSequence, 0, Steps),
  foldl(getOnes, BinSequence, 0, Ones),
  Parents = pow(integer(2), integer(Ones - 1)),
  Result = from_int(Steps) ++ " " ++ to_string(Parents)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, "", !IO).

:- pred read_lines(io.text_input_stream::in,
  string::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ Line,
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    lookNSay(strip(FileContents), ResultStr),
    io.print_line(ResultStr, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  20 2305843009213693952
*/
