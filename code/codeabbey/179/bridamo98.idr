{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

helper : Int -> String
helper 0 = ""
helper number =
  let
    res =
      if number `mod` 2 == 1
        then "1" ++ helper (number `div` 2)
        else "0" ++ helper (number `div` 2)
  in res

toBin : Int -> String
toBin 0 = "0"
toBin number = reverse (helper number)

divide : List Char -> Char -> Int -> List Int
divide [] lst curCount = [curCount]
divide binNum lst curCount =
  let
    curChar = fromMaybe ' ' (head' binNum)
    res =
      if curChar == lst
        then divide (drop 1 binNum) lst (curCount + 1)
        else [curCount] ++ (divide (drop 1 binNum) curChar 1)
  in res

findStepsToCycle : String -> Int -> Int
findStepsToCycle inputData steps =
  let
    res =
      if inputData == "10"
        then steps
        else findStepsToCycle
          (concat (map toBin (divide (unpack inputData) '1' 0))) (steps + 1)
  in res

main : IO ()
main = do
  inputData <- getLine
  let steps = findStepsToCycle inputData 0
  let numberOfOnes = length (filter (=='1') (unpack inputData))
  let parents = div (product (replicate numberOfOnes 2)) 2
  let solution = [(show steps), (show parents)]
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  20 2305843009213693952
-}
