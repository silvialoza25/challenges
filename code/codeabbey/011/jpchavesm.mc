/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, char.

:- func sumDigits(int) = int.
sumDigits(Number) =
(
  if Number = 0
  then 0
  else Number rem 10 + sumDigits(Number div 10)
).

:- pred sumOfDigits(list(string), string, string).
:- mode sumOfDigits(in, in, out) is det.
sumOfDigits([], Solution, Solution).
sumOfDigits([Line | Tail], PartialSolution, Solution) :-
(
  string.words_separator(char.is_whitespace, Line) = NumsStr,
  map(det_to_int,NumsStr) = Nums,
  det_index0(Nums,0) = FirstNum,
  det_index0(Nums,1) = SecondNum,
  det_index0(Nums,2) = ThirdNum,
  sumDigits(FirstNum * SecondNum + ThirdNum) = Answer,

  PartialSolution1 = PartialSolution ++ " " ++ from_int(Answer),
  sumOfDigits(Tail, PartialSolution1, Solution)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1, FileContents, ComputeList),
    sumOfDigits(ComputeList, "", Solution),
    io.print_line(Solution,!IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  21 22 19 31 25 18 12 38 18 18 15
*/
