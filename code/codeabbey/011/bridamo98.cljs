;; $ clj-kondo --lint bridamo98.cljs
;; linting took 28ms, errors: 0, warnings: 0

(ns bridamo98-011
  (:gen-class)
  (:require [clojure.core :as core]))

(defn sum-digits [a b c]
    (loop [num (+ c (* a b)) result 0]
      (if (< num 1)
        (print (str (int result) " "))
        (recur (Math/floor (/ num 10)) (+ result (mod num 10)))
      )
    )
)

(defn solve-all [size-input]
  (loop [x 0]
    (when (< x size-input)
      (sum-digits (core/read) (core/read) (core/read))
      (recur (+ x 1))
    )
  )
  (println)
)

(defn main []
  (let [size-input (core/read)]
    (solve-all size-input)
  )
)

(main)

;; $ cat DATA.lst | clj bridamo98.clj
;; 21 18 26 28 22 22 11 22 17 19 27 28 21 22
