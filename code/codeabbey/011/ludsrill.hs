-- hlint ludsrill.hs
-- No hints

import Control.Monad
import Data.List

main = do
  input <- getLine
  inputs <- replicateM (read input) getLine
  let splitting = map (toInt . splitString) inputs
  let initialValue = map firstOperation splitting
  let organizedData = map organizeData initialValue
  let solution = map sumOfDigits organizedData
  putStrLn (unwords solution)

splitString :: String -> [String]
splitString = splitWith (== ' ')

splitWith :: (a -> Bool) -> [a] -> [[a]]
splitWith cond [] = []
splitWith cond xs = first : splitWith cond (safeTail rest)
  where
    (first, rest) = break cond xs
    safeTail [] = []
    safeTail (_:ys) = ys

toInt :: [String] -> [Int]
toInt = map read

digs :: Integral x => x -> [x]
digs 0 = []
digs x = digs (x `div` 10) ++ [x `mod` 10]

firstOperation :: [Int] -> Int
firstOperation [a, b, c] = d
  where
    d = (a * b) + c

organizeData :: Int -> [Int]
organizeData c = new
  where
    new = digs c

sumOfDigits :: [Int] -> String
sumOfDigits c = show (sum c)

-- cat DATA.lst | runhaskell ludsrill.hs
-- 25 15 16 26 17 12 29 18 16 14 28 14 17 27 33
