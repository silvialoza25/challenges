# $ lintr::lint('nickkar.r')

findigitsum <- function(num, out=0) {
    if (num == 0) {
        return(out)
    }
    else {
        digits <- floor(num / 10)
        remainder <- num %% 10
        findigitsum(digits, out + remainder)
    }
}

solveca5es <- function(cases, out = c(), i=1) {
    if (i > length(cases)) {
        return(out)
    }
    else {
        tmp <- unlist(strsplit(cases[i], " "))
        num1 <- as.double(tmp[1])
        num2 <- as.double(tmp[2])
        num3 <- as.double(tmp[3])
        r <- findigitsum((num1 * num2) + num3)
        solveca5es(cases, (append(out, r)), i + 1)
    }
}

main <- function() {
    data <- readLines("stdin")
    out <- solveca5es(data[2:length(data)])
    cat(out)
    cat("\n")
}

main()

# $ cat DATA.lst | rscript nickkar.r
# 25 15 16 26 17 12 29 18 16 14 28 14 17 27 33
