-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import IO
import List
import Read

getData :: IO String
getData = do
  x <- getContents
  return x

getSum :: Int -> Int -> Int
getSum number suma
  | number == 0 = newSum
  | otherwise = getSum newNumber newSum
  where
    newNumber = div number 10
    modulo = mod number 10
    newSum = suma + modulo

solve :: String -> String
solve dataIn = (show result)
  where
    datos = words dataIn
    constA = readInt (datos !! 0)
    constB = readInt (datos !! 1)
    constC = readInt (datos !! 2)
    result = getSum ((constA * constB) + constC) 0

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    finalResult = map (\x -> solve x) (tail (init dataLines))
  putStrLn (show (unwords finalResult))

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 22 20 25 10 10 34 25 30 32 16 12
