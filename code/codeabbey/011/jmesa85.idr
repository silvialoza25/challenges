{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #011: Sum of digits - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi s = fromMaybe 0 $ parseInteger {a=Int} s

getLines : IO (List String)
getLines = do
  x <- getLine
  if x == "" then pure []
    else do
      xs <- getLines
      pure (x :: xs)

sumDigits : Int -> Int
sumDigits 0 = 0
sumDigits x = (x `mod` 10) + sumDigits (x `div` 10)

processTestCase : List Int -> Int
processTestCase [a, b, c] = sumDigits(a * b + c)

main : IO ()
main = do
  inputData <- getLines
  let
    testCases = map (map stoi) $ map words $ drop 1 inputData
    results = map processTestCase testCases
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  21 22 19 31 25 18 12 38 18 18 15
-}
