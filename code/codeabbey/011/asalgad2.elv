# elvish -compileonly asalgad2.elv

use str

fn filter_first [first @rest]{ put $rest }
fn get_tail [list]{ put $list[1..] }

fn operate [const_a const_b const_c]{
  put (+ (* $const_a $const_b) $const_c)
}

fn sum_digits [args]{
  if (eq (count $args) 0) {
    put ""
  } else {
    input = $args[0]
    total = (operate ( str:split ' ' $input ))
    put (+ ( str:split '' (to-string $total) ))
    sum_digits (get_tail $args)
  }
}

print (sum_digits (filter_first (all))) " "
# cat DATA.lst | elvish asalgad2.elv
# 18  23  25  20  30  11  21  17  11  17  19  18  21  23
