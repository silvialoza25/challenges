# elvish -compileonly alejotru3012.elv

use str
use math

fn count_members [gang qty_p qty_g index]{
  if (> (count $gang) 0) {
    member_i = 1
    each [values]{
      new_qty_p = (- $qty_p $values[0])
      new_qty_g = (- $qty_g $values[1])
      if (or (< $new_qty_p 0) (< $new_qty_g 0)) {
        continue
      }
      if (and (== $new_qty_p 0) (== $new_qty_g 0)) {
        put $index
        return
      }
      res = (count_members $gang[$member_i..] $new_qty_p ^
        $new_qty_g (+ $index 1))
      if (!= $res -1) {
        put $res
        return
      }
      member_i = (+ $member_i 1)
    } $gang
  }
  put -1
}

fn read_gangs [cur_gang gang_size]{
  line = (read-line);
  if (not-eq $line '') {
    if (and (== (count $cur_gang) $gang_size)) {
      if (!= $gang_size 0) {
        @ordered = (order &less-than=[i_a i_b]{ < $i_a[2] $i_b[2]} $cur_gang)
        put $ordered
      }
      read_gangs [] $line
    } else {
      ar_line = [(str:split ' ' $line)]
      cur_gang = [$@cur_gang [$ar_line[0] $ar_line[1] ^
        (+ $ar_line[0] $ar_line[1])]]
      read_gangs $cur_gang $gang_size
    }
  } else {
    @ordered = (order $cur_gang)
    put $ordered
  }
}

fn main []{
  params = [(str:split ' ' (read-line))]
  qty_p qty_g = $params[1] $params[2]
  @gangs = (read_gangs [] 0)
  @res = (each [gang]{
    echo (count_members $gang $qty_p $qty_g 1)
  } $gangs)
  echo (str:join ' ' $res)
}

main

# cat DATA.lst | elvish alejotru3012.elv
# 6 6 6 6 7 8 12 8 10 11 10 9
