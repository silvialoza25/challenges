# $ lintr::lint('nickkar.r')

countvowels <- function(cases) {
  a <- lengths(regmatches(cases, gregexpr("a", cases)))
  o <- lengths(regmatches(cases, gregexpr("o", cases)))
  u <- lengths(regmatches(cases, gregexpr("u", cases)))
  i <- lengths(regmatches(cases, gregexpr("i", cases)))
  e <- lengths(regmatches(cases, gregexpr("e", cases)))
  y <- lengths(regmatches(cases, gregexpr("y", cases)))
  return(a + o + u + i + e + y)
}

main <- function() {
    data <- readLines("stdin")
    out <- countvowels(data[2:length(data)])
    cat(out)
    cat("\n")
}

main()

# $ cat DATA.lst | rscript nickkar.r
# 11 14 13 19 5 14 15 9 8 8 6 8 8 7 9
