#!/usr/bin/env bash
# $ shellcheck --external-sources --exclude=SC1090,SC2154, jmesa85.sh
# $

# CodeAbbey #020: Vowel Count - Bash

main () {
  test_cases=$(cat)
  echo "$test_cases" | awk 'NR>1 {print gsub(/[aeiouy]/,"")}' \
    | awk '{printf $0" "}'
}

main

# $ cat DATA.lst | ./jmesa85.sh
# 15 10 12 11 14 7 7 9 4 12 9 10 14 11 10
