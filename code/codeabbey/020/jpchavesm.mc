/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, char, int.

:- pred checkVowels(list(char)::in,int::in,string::out) is det.
checkVowels([],PartialSum,from_int(PartialSum)).
checkVowels([Char | Tail],PartialSum,Sum) :-
  (
    if member(Char,['a','e','i','o','u','y'])
    then checkVowels(Tail,PartialSum+1,Sum)
    else checkVowels(Tail,PartialSum,Sum)
  ).

:- pred countVowels(list(string)::in,string::in,string::out) is det.
countVowels([],PartialSum,PartialSum).
countVowels([Words | Tail],PartialSum,Sums) :-
  (
    to_char_list(Words) = Chars,
    checkVowels(Chars,0,VowelSum),
    PartialSum1 = PartialSum ++ " " ++ VowelSum,
    countVowels(Tail,PartialSum1,Sums)
  ).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
                   list(string)::in,io::di,io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    list.det_drop(1,FileContents,ListStr),
    countVowels(ListStr,"",VowelSumStr),
    io.print_line(VowelSumStr,!IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  15 10 12 11 14 7 7 9 4 12 9 10 14 11 10
*/
