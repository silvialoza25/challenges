# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
use List::Util qw( min max );
our ($VERSION) = 1;

my $EMPTY = q{};
my $SPACE = q{ };

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data[ 1 .. $#data ];
}

sub vowel_count {
  my @args    = @_;
  my $string  = $args[0];
  my $replace = join $EMPTY, $string;
  $replace =~ s/[aeiouy]//gmxs;
  my $count = ( length $string ) - ( length $replace );
  exit 1 if !print $count . $SPACE;
  return $count;
}

sub solution {
  my @data = @_;
  my @out  = map { vowel_count $_ } @data;
  exit 1 if !print "\n";
  return 'The functions works, that is really true?, what is the real truth';
}

sub main {
  my @vals = data_in();
  solution(@vals);
  return 'Ok, if this code works that means that I can make some code in Perl';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# 10 4 8 18 8 10 10 15 11 11 11 8 12 10 11 9 17 9 13 6
