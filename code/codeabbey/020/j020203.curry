-- $ curry-verify j020203.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 3.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `j020203'!

getLines :: Int -> IO [String]
getLines t = forM [1..t] $ \_ -> getLine

vowels :: String -> String
vowels n
  | n == "" = ""
  | vow == "a" ||
    vow == "e" ||
    vow == "i" ||
    vow == "o" ||
    vow == "u" ||
    vow == "y" = vow ++ (vowels (tail n))
  | otherwise = "" ++ (vowels (tail n))
  where
    vow = take 1 n

vowelsList :: [String] -> [String]
vowelsList [] = []
vowelsList (x:xs) = (vowels x) : (vowelsList xs)

wordLengths :: [String] -> [Int]
wordLengths = map length

main :: IO ()
main = do
  numStr <- getLine
  let
    num = read (numStr) + 0
  sentences <- getLines num
  let
    vows = vowelsList sentences
    cantVowels = unwords (map show( wordLengths vows))
  putStrLn (id cantVowels)

-- cat DATA.lst | pakcs -q :load j020203.curry :eval main :quit
-- 15 10 12 11 14 7 7 9 4 12 9 10 14 11 10
