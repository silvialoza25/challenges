#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun euclidian-distance (item)
  (let ((x1 (first item))
        (y1 (second item))
        (x2 (third item))
        (y2 (fourth item))
        (distance))
    (setq distance (sqrt (coerce (+ (expt (- x2 x1) 2)
      (expt (- y2 y1) 2)) 'double-float)))))

(defun point-to-segment-distance ()
  (let ((c)
        (a) (b)
        (data)
        (alpha)
        (beta)
        (theta)
        (distance))
    (setq *read-default-float-format* 'double-float)
    (setq data (cdr (read-data)))
    (dolist (item data)
      (setq a (euclidian-distance (subseq item 2)))
      (setq b (euclidian-distance (subseq item 0 4)))
      (setq c (euclidian-distance (append (subseq item 4) (subseq item 0 2))))

      (setq alpha (acos (/ (+ (expt a 2) (expt b 2) (- (expt c 2)))
                    (* 2 a b))))
      (setq beta (acos (/ (+ (expt a 2) (- (expt b 2)) (expt c 2)) (* 2 a c))))
      (setq theta (acos (/ (+ (-(expt a 2)) (expt b 2) (expt c 2)) (* 2 b c))))

      (setq distance (* (sin alpha) a))
      (cond ((and (< theta (/ (* 90 pi) 180)) (< alpha (/ (* 90 pi) 180)))
            (format t "~,7F " distance))
            ((> theta alpha) (format t "~,7F " c))
            ((< theta alpha) (format t "~,7F " a))))))

(point-to-segment-distance)

#|
cat DATA.lst | clisp ludsrill.lsp
5.0000000 6.4031242 18.9736660 6.3245553 7.2801099 13.8924440 8.3562902
7.2111026 12.6491106 4.3453614 0.3429972 12.1655251 3.1574089 2.7955373
2.6872067 8.0622577 11.1803399 11.71216872067 8.0622577 11.1803399
11.7121649 3.1622777 8.9442719 7.1896218
|#
