{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let formatedInputData = map convert inputData
  let solution = map solveAll formatedInputData
  putStrLn (unwords solution)

solveAll :: [Double] -> String
solveAll singleProblem = res
  where
    x1 = head singleProblem
    y1 = singleProblem !! 1
    x2 = singleProblem !! 2
    y2 = singleProblem !! 3
    xp = singleProblem !! 4
    yp = singleProblem !! 5
    iRes = calcMinDistance x1 y1 x2 y2 xp yp
    res = show iRes


calcMinDistance :: Double -> Double -> Double -> Double
                   -> Double -> Double -> Double
calcMinDistance x1 y1 x2 y2 xp yp = distance
  where
    seglineDistance = squaredDistance x1 y1 x2 y2
    t = if seglineDistance == 0.0
          then 0.0
          else max 0.0 (min 1.0
               (((xp - x1) * (x2 - x1) + (yp - y1) * (y2 - y1))
               / seglineDistance))
    projectionPx = x1 + t * (x2 - x1)
    projectionPy = y1 + t * (y2 - y1)
    distance = sqrt (squaredDistance xp yp projectionPx projectionPy)

squaredDistance :: Double -> Double -> Double -> Double -> Double
squaredDistance x1 y1 x2 y2 = res
  where
    res = ((x1 - x2) ** 2) + (y1 - y2) ** 2

convert :: String -> [Double]
convert dat = res
  where
    divDat = words dat
    res = map read divDat :: [Double]

{-
$ cat DATA.lst | ./bridamo98
  9.27881082225677 5.0 7.211102550927978 7.9230769230769225 17.679832312220476
  5.830951894845301 10.06553865886723 0.5144957554275262 3.605551275463989
  12.41867527967479 10.04987562112089 7.810249675906654 2.6736956911078433
  12.649110640673518 12.649110640673518 7.0710678118654755 10.816653826391969
  6.324555320336759 2.0
-}
