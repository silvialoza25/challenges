/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include <stdio.h>
#define MAX_SIZE 1024

/*struct of Points*/
typedef struct Point {
  double x;
  double y;
} Point;

/*struct of Rect*/
typedef struct Rect {
  double m;
  double b;
} Rect;

/*Function that return the distance of the nearest point of the segment*/
static double min_dist_to_point (/*@in@*/ Point p, /*@in@*/ Point p1,
  /*@in@*/ Point p2)
  /*@modifies nothing@*/ {
  double d1, d2;
  double x1, x2, y1, y2;
  x1 = p1.x - p.x, y1 = p1.y - p.y;
  x2 = p2.x - p.x, y2 = p2.y - p.y;
  d1 = sqrt((x1 * x1) + (y1 * y1));
  d2 = sqrt((x2 * x2) + (y2 * y2));
  if ((d1 - d2) > DBL_EPSILON) { return d2; }
  else { return d1; }
}

/*Function that return the distance of the nearest point of the segment*/
static double distance_project (/*@in@*/ Point p, /*@in@*/ Rect s12)
  /*@modifies nothing@*/ {
  double d;
  d = fabs((s12.m * p.x) - p.y + s12.b) / sqrt((s12.m * s12.m) + 1);
  return d;
}

/*Function that say if a point has orthogonal projection to a segment*/
static bool has_ort_project (/*@in@*/ Point p, /*@in@*/ Rect r1,
  /*@in@*/ Rect r2)
  /*@modifies nothing@*/ {
  double comparator;
  comparator = p.y - (r1.m * p.x);
  if ((r1.b - r2.b) > DBL_EPSILON) {
    if (comparator <= r1.b && comparator >= r2.b) { return true; }
    else { return false; }
  }
  else {
    if (comparator <= r2.b && comparator >= r1.b) { return true; }
    else { return false; }
  }
}

/*Function that create a rect with 2 points*/
static Rect create_rect (/*@in@*/ Point p1, /*@in@*/ Point p2)
  /*@modifies nothing@*/ {
  Rect s12;
  s12.m = (p2.y - p1.y) / (p2.x - p1.x);
  s12.b = p1.y - (s12.m * p1.x);
  return s12;
}

/*Function that create a rect with a point and his orthogonal m*/
static Rect create_rect_ort (/*@in@*/ Point p, /*@reldef@*/ double m)
  /*@modifies nothing@*/ {
  Rect rect;
  rect.m = -1 / m;
  rect.b = p.y - (rect.m * p.x);
  return rect;
}

/*Function that print the distance with format*/
static int print_dist (/*@reldef@*/double d)
  /*@modifies nothing@*/ {
  if (fabs(d - (int) d) < DBL_EPSILON) {
    printf("%.0f ", d);
    return 1;
  }
  else {
    printf("%.10f ", d);
    return 1;
  }
}

int main(void) {
  char temp[MAX_SIZE];
  int cases, i = 0;
  double d;
  Point p1, p2, p3;
  Rect r1, r2, s12;
  if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
  cases = atoi(temp);
  for (i = 0; i < cases; i++) {
    if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
    d = (double) sscanf(temp,
      "%le %le %le %le %le %le",
      &p1.x, &p1.y, &p2.x, &p2.y, &p3.x, &p3.y);
    s12 = create_rect(p1,p2);
    r1 = create_rect_ort(p1, s12.m);
    r2 = create_rect_ort(p2, s12.m);
    if (has_ort_project(p3,r1,r2)) { d = distance_project(p3, s12); }
    else { d = min_dist_to_point(p3, p1, p2); }
    if (print_dist(d) == 0) { return 0; }
  }
  printf("\n");
  return 1;
}

/*
$ cat DATA.lst | ./slayfer1112
10.58701590 6.40312424 5.83095189 4.71495167 4.43760157 11.66190379 0.89442719
6.32455532 1 14.83762691 4.12310563 10.64689794 7.69468660 0.97014250
3.36242210 8.77895573 4.47213595 12.55143265 9.83991671 5.83095189
*/
