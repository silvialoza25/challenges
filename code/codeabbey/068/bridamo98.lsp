#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun calc-rendezvous-distance (s a b)
  (let ((t-of-rendezvous (/ s (+ a b))))
    (format t "~a " (float (* t-of-rendezvous a)))))

(defun solve-all (size-input)
  (loop for i from 0 to (- size-input 1)
    do(calc-rendezvous-distance (read) (read) (read))))

(defun main ()
  (let ((size-input (read)))
    (solve-all size-input)))

(main)

#|
  cat DATA.lst | clisp bridamo98.lsp
  7.3170733 180.63158 6.5102043 4.3421054 49.037735 47.357143
  3.8823528 17.32353 124.90196 19.555555 6.4166665 7.6363635
  123.061226 13.5 16.64865 54.727272 32.5 36.416668 4.4210525
  26.232557 8.3404255 144.375 3.4375 285.16666 73.2 6.0444446
  9.24 52.085106
|#
