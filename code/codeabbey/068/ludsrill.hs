-- hlint ludsrill.hs
-- No hints

main = do
  n <- readLn :: IO Int
  bicycleRace n

bicycleRace :: Int -> IO ()
bicycleRace 0 = return ()
bicycleRace n = do
  array <- getLine
  let xs = map read $ words array :: [Float]
  let a = head xs
  let b = xs !! 1
  let c = xs !! 2
  putStr $ solution a b c
  putStr " "
  bicycleRace (n-1)

solution :: Float -> Float -> Float -> String
solution a b c = d where
  d = show ((a / (b + c)) * b)

-- cat DATA.lst | runhaskell ludsrill.hs
-- 13.533334 139.54839 4.736842 20.222221 22.08 29.387756 95.789474 6.9 11.04
-- 7.2000003 128.08511 3.5714288 81.200005 15.166667 4.1379313 9.868421
-- 11.400001 58.047615 14.0 67.87805 54.239998 57.999996 211.45161 39.818184
-- 136.66667 10.903226
