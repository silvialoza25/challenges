;; $clj-kondo --lint nickkar.cljs
;; linting took 68ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

;; reads the input and puts the values in a readable vector
(defn read-input ([num-cases] (read-input num-cases 0 []))
  ([num-cases index out]
    (cond
      (= index num-cases) out
      :else
      (read-input num-cases (inc index)
                  (conj out [(core/read) (core/read) (core/read)])))))

;; finds the distance from the first city to the meeting point
(defn find-distance [dist vel_a vel_b]
  (double (* vel_a (/ dist (+ vel_a vel_b)))))

;; iterates over every test-case
(defn solve-cases ([num-cases cases] (solve-cases num-cases cases 0 []))
  ([num-cases cases index out]
    (cond
      (>= index num-cases) out
      :else
      (let [[dist vel_a vel_b] (nth cases index)
            meetdist (find-distance dist vel_a vel_b)]
        (solve-cases num-cases cases (inc index) (conj out meetdist))))))

;; driver code
(defn main []
  (let [num-cases (core/read)
        cases (read-input num-cases)]
    (apply println (solve-cases num-cases cases))))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 13.53333333333333 139.5483870967742 4.736842105263158 20.22222222222222
;; 22.08 29.38775510204082 95.78947368421053 6.9 11.04 7.2 128.0851063829787
;; 3.571428571428571 81.2 15.16666666666667 4.137931034482759 9.868421052631579
;; 11.4 58.04761904761905 14.0 67.8780487804878 54.24 58.0 211.4516129032258
;; 39.81818181818182 136.6666666666667 10.90322580645161
