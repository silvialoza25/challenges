// ponyc -b kjcamargo19

use "format"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun iterator(data: Array[String]): Array[String] =>
          data.slice(1, data.size())

        fun format_string(value: F64): String =>
          Format.float[F64](value where fmt=FormatFixLarge, prec= 9)

        fun get_distance(speed_a:  F64, speed_b: F64, distance: F64): F64 =>
          speed_a * (distance / (speed_a + speed_b))

        fun obtain_data(data: Array[String])? =>
          if data.size() > 0 then
            var line: Array[String] = data(0)?.split_by(" ")
            var distance: F64 = line(0)?.f64()?
            var speed_a: F64 = line(1)?.f64()?
            var speed_b: F64 = line(2)?.f64()?

            var result: F64 = get_distance(speed_a, speed_b, distance)
            env.out.write(format_string(result))
            env.out.write(" ")
            obtain_data( iterator (data))?
          end

        fun ref apply(data: Array[U8] iso) =>
          var input_data: String = String.from_iso_array(consume data)
          var line_sep: Array[String] = input_data.split_by("\n")
          var real_data: Array[String] = line_sep.slice(1, line_sep.size()-1)

          try
            obtain_data(real_data)?
          end

        fun ref dispose() =>
          None

      end,
      512
    )

// cat DATA.lst | ./kjcamargo19
// 3.978723404 11.481481481 37.588235294 45.043478261 160.615384615
// 8.615384615 42.697674419 25.909090909 124.000000000 6.487804878
// 30.500000000 13.920000000 7.179487179 17.111111111 27.500000000
// 8.636363636 242.526315789 16.916666667 104.545454545 43.333333333
// 228.608695652 140.264705882 64.272727273 7.767857143 10.365853659
// 24.830188679 68.000000000
