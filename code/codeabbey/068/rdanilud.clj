;;clj-kondo --lint rdanilud.clj
;;linting took 18ms, errors: 0, warnings: 0

(ns rdanilud
(:gen-class)
(:require [clojure.string :as str]))

(defn captu []
  (let [data (str/split-lines (slurp *in*))
      header (str/split (data 0) #" ")
      body (subvec data 1 (alength (to-array-2d data)))]
    [header body]))

(defn resul [info]
  (let [distance (read-string (info 0))
  rider1 (read-string(info 1))
  rider2 (read-string(info 2))
  result (*(/ distance (+ rider1 rider2))rider1)]
    (print (str (double result) " "))))

(defn main []
  (let [[_ body] (captu)]
    (doseq [x body]
      (let [vec (str/split x #" ")]
        (resul vec)))
          (println)))

(main)

;;cat DATA.lst | clj rdanilud.clj
;;13.53333333333333 139.5483870967742 4.736842105263158 20.22222222222222
;;22.08 29.38775510204082 95.78947368421053 6.9 11.04 7.2 128.0851063829787
;;3.571428571428571 81.2 15.16666666666667 4.137931034482759 9.868421052631579
;;11.4 58.04761904761905 14.0 67.8780487804878 54.24 58.0 211.4516129032258
;;39.81818181818182 136.6666666666667 10.90322580645161
