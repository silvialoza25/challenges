# elvish -compileonly cyberpunker.elv

#!/bin/elvish

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn bicycleRace [data]{
  each [values]{
    var bicyclistOne = (put $values[1])
    var bicyclistTwo = (put $values[2])
    var distance = (put $values[0])
    var adding = (+ $bicyclistOne $bicyclistTwo)
    divide = (/ $distance $adding)
    meetingPoint = (* $bicyclistOne $divide)
    put $meetingPoint
  } $data
}

echo (bicycleRace (input_data))

# cat DATA.lst | elvish cyberpunker.elv
#27.209302325581397 6.476190476190476 5.217391304347826 7.859649122807017
#33.14285714285714 20.142857142857142 5.684210526315789 6 5.133333333333333
#145.6 156.92307692307693 7.451612903225807 6.722222222222223 5.4035087719298245
#8.571428571428571 9.868421052631579 4.439024390243903 10.31578947368421
#52.199999999999996 27.659574468085104
