# $flake8
# $ mypy ludsrill.py
# Success: no issues found in 1 source file

from typing import Tuple, List, Dict
import random
import math
import sys


def getdata() -> Tuple[List[List[float]], List[float]]:

    dataraw = sys.stdin.readlines()
    data = []
    for i in dataraw:
        temp = i.split(',')
        temp = temp[0].split()
        data.append(temp)
    data.pop(0)
    y_inputs = [(float(i.pop(4))) for i in data]
    x_inputs = [[float(data[i][j]) for j in range(0, len(data[0]))]
                for i in range(0, len(data))]
    return x_inputs, y_inputs


def initialize_parameters(x_inputs: List[List[float]]
                          ) -> Tuple[List[List[float]], List[List[float]]]:

    n_inputs = len(x_inputs[0])
    n_hidden = 5
    n_outputs = 1
    weight_1 = [[random.uniform(-1, 1) * 0.01 for j in range(0, n_inputs)]
                for i in range(0, n_hidden)]
    weight_2 = [[random.uniform(-1, 1) * 0.01 for j in range(0, n_hidden)]
                for i in range(0, n_outputs)]
    return weight_1, weight_2


def dot(weight_1: List[List[float]], x_inputs: List[List[float]]
        ) -> List[List[float]]:

    sum_product_wx: List[List[float]] = []
    length = int(len(weight_1))
    for i in range(0, length):
        sum_product_wx.append([])
        for k in range(0, x_inputs[0].__len__()):
            suma: float = 0
            for j in range(0, weight_1[0].__len__()):
                suma = suma + weight_1[i][j] * x_inputs[j][k]
            sum_product_wx[i].append(suma)

    return sum_product_wx


def tanh(sum_product_wx: List[List[float]]) -> List[List[float]]:
    k: List[List[float]] = []
    for i in range(0, sum_product_wx.__len__()):
        k.append([])
        for j in range(0, sum_product_wx[0].__len__()):
            k[i].append(2 / (1 + math.exp(-sum_product_wx[i][j] * 2)) - 1)
    return k


def forward_propagation(x_inputs: List[List[float]],
                        weight_1: List[List[float]],
                        weight_2: List[List[float]]
                        ) -> Tuple[List[List[float]], List[List[float]]]:
    sum_product1 = dot(weight_1, transpose(x_inputs))
    activation_1 = tanh(sum_product1)
    sum_product2 = dot(weight_2, activation_1)
    activation_2 = tanh(sum_product2)
    return activation_1, activation_2


def compute_cost(activation_2: List[List[float]],
                 y_inputs: List[float]) -> float:
    shape = len(y_inputs)
    error = [(activation_2[0][i] - y_inputs[i])**2
             for i in range(0, len(y_inputs))]
    accum: float = 0
    for i in error:
        accum += i
    cost = math.sqrt(accum / shape)
    return cost


def transpose(x_inputs: List[List[float]]) -> List[List[float]]:
    x_inputs = [[x_inputs[j][i] for j in range(len(x_inputs))]
                for i in range(len(x_inputs[0]))]
    return x_inputs


def backward_propagation(activation_1: List[List[float]],
                         activation_2: List[List[float]],
                         x_input: List[List[float]],
                         y_input: List[float],
                         dictionary: Dict[str, List[List[float]]]
                         ) -> Tuple[List[List[float]], List[List[float]]]:

    weight_1 = dictionary["weight_1"]
    weight_2 = dictionary["weight_2"]

    shape = len(x_input[1])
    learning_rate = 0.8
    d_z2 = [[(activation_2[0][i] - y_input[i]) for i in range(0,
                                                              len(y_input))]]
    aux_dw2 = dot(d_z2, transpose(activation_1))
    d_w2 = [[(1 / shape) * aux_dw2[i][j] for j in range(0, len(aux_dw2[0]))]
            for i in range(0, len(aux_dw2))]
    aux1_dz1 = [[activation_1[i][j]**2 for j in range(0, len(activation_1[0]))]
                for i in range(0, len(activation_1))]
    aux2_dz1 = [[1 - aux1_dz1[i][j] for j in range(0, len(aux1_dz1[0]))]
                for i in range(0, len(aux1_dz1))]
    aux3_dz1 = dot(transpose(weight_2), d_z2)
    d_z1 = [[aux3_dz1[i][j] * aux2_dz1[i][j]
             for j in range(0, len(aux3_dz1[0]))]
            for i in range(0, len(aux3_dz1))]
    aux_dw1 = dot(d_z1, x_input)
    d_w1 = [[(1 / shape) * aux_dw1[i][j] for j in range(0, len(aux_dw1[0]))]
            for i in range(0, len(aux_dw1))]
    aux_weight1 = [[learning_rate * d_w1[i][j] for j in range(0, len(d_w1[0]))]
                   for i in range(0, len(d_w1))]
    aux_weight2 = [[learning_rate * d_w2[i][j] for j in range(0, len(d_w2[0]))]
                   for i in range(0, len(d_w2))]
    weight_1 = [[weight_1[i][j] - aux_weight1[i][j]
                 for j in range(0, len(weight_1[0]))]
                for i in range(0, len(weight_1))]
    weight_2 = [[weight_2[i][j] - aux_weight2[i][j]
                 for j in range(0, len(weight_2[0]))]
                for i in range(0, len(weight_2))]
    return weight_1, weight_2


def nn_model() -> None:
    x_inputs, y_inputs = getdata()
    y_inputs = [y_inputs[i] * 0.1 for i in range(0, len(y_inputs))]
    weight_1, weight_2 = initialize_parameters(x_inputs)
    for i in range(0, 50000):
        dictionary: Dict[str, List[List[float]]] = {
            "weight_1": weight_1,
            "weight_2": weight_2
        }
        activation_1, activation_2 = forward_propagation(x_inputs,
                                                         weight_1, weight_2)
        cost = compute_cost(activation_2, y_inputs)
        weight_1, weight_2 = backward_propagation(activation_1,
                                                  activation_2, x_inputs,
                                                  y_inputs, dictionary)
    print("Error Final", cost)
    predict(weight_1, weight_2)
    print("5")
    print("0.1")
    for i in range(0, weight_1.__len__()):
        for j in range(0, weight_1[0].__len__()):
            print(weight_1[i][j])

    for i in range(0, weight_2.__len__()):
        for j in range(0, weight_2[0].__len__()):
            print(weight_2[i][j])


def predict(weight_1: List[List[float]], weight_2: List[List[float]]) -> None:
    x_input = [[-0.351469194358, -0.87169002657,
                -0.152818594457, -0.0126210957013]]

    activation_1, activation_2 = forward_propagation(x_input,
                                                     weight_1, weight_2)
    activation_1 = transpose(activation_1)
    print("activation", activation_2)


if __name__ == "__main__":
    nn_model()

# $ cat DATA.lst | python ./ludsrill.py
# 5 0.1 -0.0019649570236834005 0.010501367274236086
# -0.011778960879736478 0.024995920165252823 -0.32733561535443256
# 0.10883278086362096 -0.2760978430037215 0.419909226668633
# 0.06636016929833806 -0.008460010277109856 0.06225377883320628
# -0.0880773390999962 0.2701591892079545 -0.07935617489391365
# 0.22748740690834462 -0.32476290206097497 0.03826127503009562
# -0.014192867036553454 0.016338364163903683 -0.049000986340177084
# -0.02556983253465229 -0.612145349525376 0.12653114813903266
# 0.48736177304896594 0.06333914358611513
