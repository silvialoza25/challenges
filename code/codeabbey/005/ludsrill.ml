(*
$ ocp-lint --severity-limit 3
  Scanning files in project "."...
  Found '3' file(s)
  Running analyses... 3 / 3
  Merging database...
  == New Warnings ==
    * 0 file(s) were linted
    * 0 warning(s) were emitted:
    * 0 file(s) couldn't be linted
*)

let get_max subList =
  Printf.printf "%i " (List.hd subList)

let splitting line =
  String.split_on_char ' ' line

let maybe_read_line () =
  try Some (read_line ()) with
  | End_of_file -> None

let rec get_data acc =
  match maybe_read_line () with
  | Some (line) -> get_data (acc @ [(List.sort compare
                                (List.map int_of_string (splitting line)))])
  | None -> List.iter get_max (List.tl acc)

let () = get_data []

(*
$ cat DATA.lst | ocaml ludsrill.ml
  -7893983 -4645516 -990916 -6900446 -6818914 -2692360 -7097204 -6312538
  -3820340 -4434631 -3860718 -4653126 -6995039 -3895486 -1098338 -5022142
  -7056364 -7066115 -702733 -5979590 -3977566 -8723384 -3991324 -9521217
  -8873724 -3875308 -7377105
*)
