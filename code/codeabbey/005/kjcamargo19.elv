# elvish -compileonly kjcamargo.elv

use str
use math

#This function read all values from DATA.lst file
fn read_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

#This function evaluate the minimun number of three
fn minimun [data]{
  each [val]{
    data_line = $val
    echo (math:min $@data_line)
  } $data
}

#Output Value
echo (str:join ' ' [(minimun (read_data))])

# cat DATA.lst | elvish kjcamargo19.elv
# 4969126 -476506 -9432416 -6393758 -7121078 -8520897 -4645602 -8121660
# -9616197 -8541816 -366189 -3906699 5184132 1486287 -4199933 -6203480
# 2549213 -1446637 2075143 -5849319 -2728582 -8754450 -8770327 -4623460
# -9543851 -7526039 -4773910
