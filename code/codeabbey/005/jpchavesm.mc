/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, char.

:- pred findMin(int::in, int::in, int::in, int::out) is det.
findMin(Num1, Num2, Num3, Min) :-
(
  if Num1 =< Num2, Num1 =< Num3 then Min = Num1
  else if Num2 =< Num1, Num2 =< Num3 then Min = Num2
  else Min = Num3
).

:- pred findMins(list(string), string, string).
:- mode findMins(in, in, out) is det.
findMins([], FinalMin, strip(FinalMin)).
findMins([Line | Tail], PartialMin, FinalMin) :-
(
  words_separator(char.is_whitespace, Line) = InputList,
  map(det_to_int, InputList) = NumList,
  det_index0(NumList, 0, Num1),
  det_index0(NumList, 1, Num2),
  det_index0(NumList, 2, Num3),
  findMin(Num1, Num2, Num3, Min),
  PartialMin1 = PartialMin ++ " " ++ from_int(Min),
  findMins(Tail, PartialMin1, FinalMin)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1, FileContents, MinList),
    findMins(MinList, "", Mins),
    io.print_line(Mins, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  -3554128 1132726 -7003236 -5025336 4799918 -4032555 -7024328 -4445836
  -9053969 -5477778 -6236715 4498245 -3312830 -7132561 -9372653 -1556232
  -7732649 -7653517 -6707486 -8839822 -3595149 -8805411 -2118241 2267521
  -7377188 -9893265 3926854
*/
