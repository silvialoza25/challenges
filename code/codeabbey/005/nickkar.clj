;; $clj-kondo --lint nickkar.clj
;; linting took 38ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class))

(defn read-input ([cases] (read-input cases 0 []))
  ([cases i out]
    (cond
      (>= i cases) out
      :else (read-input cases (inc i) (conj out [(read) (read) (read)])))))

(defn solve-cases ([cases nums] (solve-cases cases nums 0 []))
  ([cases nums i out]
    (cond
      (>= i cases) out
      :else
      (solve-cases cases nums (inc i) (conj out (apply min (nth nums i)))))))

(defn main []
  (let [cases (read)
        nums (read-input cases)]
    (apply println (solve-cases cases nums))))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; -7893983 -4645516 -990916 -6900446 -6818914 -2692360 -7097204 -6312538
;; -3820340 -4434631 -3860718 -4653126 -6995039 -3895486 -1098338 -5022142
;; -7056364 -7066115 -702733 -5979590 -3977566 -8723384 -3991324 -9521217
;; -8873724 -3875308 -7377105
