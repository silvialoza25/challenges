;;clj-kondo --lint rdanilud.cljs
;;linting took 41ms, errors: 0, warnings: 0
(ns rdanilud
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str]))

(defn item-rm [item vec-old]
  (let [vec-modify (atom [])]
    (doseq [itemc vec-old]
      (when (not= itemc item)
        (swap! vec-modify #(conj % itemc))))@vec-modify))

(defn update-vecsuccess [data-fail data-success]
  (let [update-vec (atom [])]
    (doseq [[idx item] (map-indexed vector data-success )]
      (let [update-subv (atom [])]
        (doseq [subitem item]
          (when (not (.contains (data-fail idx) subitem))
            (swap! update-subv #(conj % subitem))))
        (swap! update-vec #(conj % @update-subv))))@update-vec))

(defn update-vecfail [data-correct data-fail]
  (let [update-vec (atom []) compare-vec [\0 \1 \2 \3 \4 \5 \6 \7 \8 \9]]
    (doseq [[idx item] (map-indexed vector data-correct)]
      (if (.contains compare-vec item)
        (swap! update-vec #(conj % (item-rm item compare-vec)))
        (swap! update-vec #(conj % (data-fail idx)))))@update-vec))

(defn vec-success [data-correct num-guess vec-idx]
  (let [vec-result (atom [])]
    (doseq [item [0 1 2 3]]
      (if (.contains vec-idx item)
        (swap! vec-result #(conj % (data-correct item)))
        (swap! vec-result #(conj %
         (.charAt(subs (num-guess 0) item (+ item 1)) 0)))))@vec-result))

(defn check-success [data-correct data-fail num-guess]
  (let [vec-result (atom []) vec-idx (atom []) num-success (num-guess 1)]
    (doseq [[idx item] (map-indexed vector (num-guess 0))]
      (when  (.contains (data-fail idx) item) (swap! vec-idx #(conj % idx))))
    (if  (= num-success  (str (- 4 (count @vec-idx))))
      (compare-and-set! vec-result @vec-result
        (vec-success data-correct num-guess @vec-idx))
      (compare-and-set! vec-result @vec-result data-correct))@vec-result))

(defn update-correct [data-correct data-fail]
  (let [update-vec (atom [])]
    (doseq [item data-fail]
      (if (= (count item) 9)
        (doseq [itemc [48 49 50 51 52 53 54 55 56 57]]
          (when (not (.contains item (char itemc)))
            (swap! update-vec #(conj % (char itemc)))))
        (swap! update-vec #(conj % (data-correct (count @update-vec))))))
    @update-vec))

(defn last-compare [data-correct data-success]
  (let [vec-result (atom []) compare-vec [\0 \1 \2 \3 \4 \5 \6 \7 \8 \9]]
    (doseq [[idx item] (map-indexed vector data-success)]
      (if (and (= 1 (count item))
        (not (.contains compare-vec (data-correct idx))))
        (swap! vec-result #(conj % (item 0)))
        (swap! vec-result #(conj % (data-correct idx)))))@vec-result))

(defn vec-add [item vec-old]
  (let [vec-modify (atom [])]
    (doseq [itemc vec-old] (swap! vec-modify #(conj % itemc)))
    (swap! vec-modify #(conj % item))@vec-modify))

(defn compara-success [num-guess data-fail data-success]
  (let [vec-updatef (atom []) vec-updates (atom [])]
    (doseq [[idx item] (map-indexed vector (num-guess 0))]
      (when (= "0" (num-guess 1))
        (if (.contains (data-fail idx) item)
          (swap! vec-updatef #(conj % (data-fail idx)))
          (swap! vec-updatef #(conj % (vec-add item (data-fail idx)))))
        (compare-and-set! vec-updates @vec-updates data-success))
      (when (not= "0" (num-guess 1))
        (if (not (.contains (data-fail idx) item))
          (if (.contains (data-success idx) item)
            (swap! vec-updates #(conj % (data-success idx)))
            (swap! vec-updates #(conj % (vec-add item (data-success idx)))))
          (swap! vec-updates #(conj % (data-success idx))))
        (compare-and-set! vec-updatef @vec-updatef data-fail)))
    [@vec-updatef @vec-updates]))

(defn reint [data-read]
  (let [vec-update (atom [])]
    (compare-and-set! vec-update @vec-update data-read)
    (doseq [item data-read]
      (swap! vec-update #(conj % item)))@vec-update))

(defn read-data [num-rows]
  (let [data-read (atom [])]
    (loop [x -1]
      (when (< x num-rows)
        (swap! data-read #(conj % (str/split (core/read-line) #" ")))
        (recur (inc x)))) (reint (rest @data-read))))

(defn guess [num-rows]
  (let [data-ini (read-data num-rows) data-fail (atom [[][][][]])
    data-success (atom [[][][][]]) data-correct (atom [])]
    (compare-and-set! data-correct [] ["" "" "" ""])
      (doseq [item data-ini]
        (compare-and-set! data-correct @data-correct
          (check-success @data-correct @data-fail item))
        (compare-and-set! data-fail @data-fail
          (update-vecfail @data-correct @data-fail))
        (compare-and-set! data-correct @data-correct
          (update-correct @data-correct @data-fail))
        (compare-and-set! data-fail @data-fail(
          (compara-success item @data-fail @data-success) 0))
        (compare-and-set! data-success @data-success
          ((compara-success item @data-fail @data-success) 1))
        (compare-and-set! data-success @data-success
          (update-vecsuccess @data-fail @data-success)))
    (compare-and-set! data-correct @data-correct
      (last-compare @data-correct @data-success))
    (str/join @data-correct)))

(defn main []
  (let [num-rows (core/read)]
    (println (guess num-rows))))
(main)
;;cat DATA.lst | clj rdanilud.cljs
;;6776
