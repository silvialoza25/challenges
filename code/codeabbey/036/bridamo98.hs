{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let divData = map words inputData
  let completeSet = map putPadding [0..9999]
  let solution = findSecretNumber completeSet divData
  putStrLn (unwords solution)

findSecretNumber :: [String] -> [[String]] -> [String]
findSecretNumber set [] = set
findSecretNumber set inputData = res
  where
    singleHint = head inputData
    nxtSet
      | singleHint !! 1 == "0" =
        filter (filterConditionForHint0 (head singleHint)) set
      | singleHint !! 1 == "1" =
        filter (filterConditionForHint1 (head singleHint)) set
      | singleHint !! 1 == "2" =
        filter (filterConditionForHint2 (head singleHint)) set
      | otherwise =
        filter (filterConditionForHint3 (head singleHint)) set
    res = findSecretNumber nxtSet (drop 1 inputData)

putPadding :: Int -> String
putPadding number = res
  where
    padding = if number < 1000
                then if number < 100
                       then if number < 10
                              then "000"
                              else "00"
                       else "0"
                else ""
    res = padding ++ show number

filterConditionForHint0 :: String -> String -> Bool
filterConditionForHint0 guess word = res
  where
    res = not (head guess == head word || guess !! 1 == word !! 1
          || guess !! 2 == word !! 2 || guess !! 3 == word !! 3)

filterConditionForHint1 :: String -> String -> Bool
filterConditionForHint1 guess word = res
  where
    res
      | guess !! 2 == word !! 2 =
        head guess /= head word &&
          guess !! 1 /= word !! 1 && guess !! 3 /= word !! 3
      | guess !! 1 == word !! 1 =
        head guess /= head word &&
          guess !! 2 /= word !! 2 && guess !! 3 /= word !! 3
      | head guess == head word =
        guess !! 1 /= word !! 1 &&
          guess !! 2 /= word !! 2 && guess !! 3 /= word !! 3
      | guess !! 3 == word !! 3 =
        guess !! 1 /= word !! 1 &&
          guess !! 2 /= word !! 2 && head guess /= head word
      | otherwise = False :: Bool

filterConditionForHint2 :: String -> String -> Bool
filterConditionForHint2 guess word = res
  where
    res
      | head guess == head word && guess !! 1 == word !! 1 =
        guess !! 2 /= word !! 2 && guess !! 3 /= word !! 3
      | head guess == head word && guess !! 2 == word !! 2 =
        guess !! 1 /= word !! 1 && guess !! 3 /= word !! 3
      | guess !! 1 == word !! 1 && guess !! 2 == word !! 2 =
        head guess /= head word && guess !! 3 /= word !! 3
      | head guess == head word && guess !! 3 == word !! 3 =
        guess !! 1 /= word !! 1 && guess !! 2 /= word !! 2
      | guess !! 1 == word !! 1 && guess !! 3 == word !! 3 =
        head guess /= head word && guess !! 2 /= word !! 2
      | guess !! 2 == word !! 2 && guess !! 3 == word !! 3 =
        head guess /= head word && guess !! 1 /= word !! 1
      | otherwise = False :: Bool

filterConditionForHint3 :: String -> String -> Bool
filterConditionForHint3 guess word = res
  where
    res
      | head guess == head word &&
          guess !! 1 == word !! 1 && guess !! 2 == word !! 2
        = guess !! 3 /= word !! 3
      | head guess == head word &&
          guess !! 2 == word !! 2 && guess !! 3 == word !! 3
        = guess !! 1 /= word !! 1
      | guess !! 1 == word !! 1 &&
          guess !! 2 == word !! 2 && guess !! 3 == word !! 3
        = head guess /= head word
      | head guess == head word &&
          guess !! 1 == word !! 1 && guess !! 3 == word !! 3
        = guess !! 2 /= word !! 2
      | otherwise = False :: Bool

{-
$ cat DATA.lst | ./bridamo98
 8749
-}
