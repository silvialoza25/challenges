# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.08 seconds (0.02s to load, 0.06s running 55 checks on 1 file)
# 6 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule Anagrams do
  def main do
    all_data = IO.read(:stdio, :all)
    try do
      [data, bd] = get_data(all_data)
      bd = Enum.map(List.flatten(bd), &sort_dictionary/1)
      List.flatten(data)
        |> Enum.map(&anagram(bd, sort_dictionary(&1), 0))
        |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_data(all_data) do
    data = String.split(all_data, "\n")
            |> Enum.map(&String.split(&1, " "))
            |> Enum.map(&Enum.reject(&1, fn x -> x == "" end))
            |> Enum.reject(&Enum.empty?/1)
    [Enum.slice(data, 1..String.to_integer(hd(hd(data)))),
      Enum.slice(data, String.to_integer(hd(hd(data))) + 1..length(data))]
  end

  def sort_dictionary(word) do
    String.split(word, "")
      |> Enum.reject(fn x -> x == "" end)
      |> Enum.sort()
  end

  def anagram([], _, count), do: count - 1
  def anagram([head | tail], value, count) do
    if value == head do
      anagram(tail, value, count + 1)
    else
      anagram(tail, value, count)
    end
  end
end

Anagrams.main()

# cat DATA.lst words.lst | elixir ludsrill.exs
# 3 4 3 3 4 4 4
