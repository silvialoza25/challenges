-- $ ghc -o smendoz3 smendoz3.hs
--   [1 of 1] Compiling Main ( smendoz3.hs, smendoz3.o )
--   Linking code ...
-- $ hlint smendoz3.hs
--   No hints

import Data.Map
import Control.Monad
import Data.Maybe

main = do
  input <- getLine
  inputs <- replicateM (read input) getLine
  print (unwords (checkAll inputs))

checkAll :: [String] -> [String]
checkAll = Prelude.map function

function :: String -> String
function x = show (strEnt x x 0)

strEnt :: String -> String -> Double -> Double
strEnt [] s result = result
strEnt [x] s result = res
  where res = result + entropy x s
strEnt (x:xs) s result = strEnt (Prelude.filter (/=x) xs) s res
  where res = result + entropy x s

entropy :: Char -> String -> Double
entropy c s = abs (logBase 2 (frec (aux c s) s)) * frec (aux c s) s

frec :: Integer -> String -> Double
frec num str = fromIntegral num / fromIntegral (length str)

aux :: Char -> String -> Integer
aux x str = getVal x (mapString str)

mapString :: String -> Map Char Integer
mapString x = fromListWith (+) [(c, 1) | c <- x]

getVal :: Char -> Map Char Integer -> Integer
getVal letter hash = fromMaybe 0 (Data.Map.lookup letter hash)

-- $  cat DATA.lst | ./smendoz3
--   3.536886723742169 3.5227119556807582 3.4472624994412104 3.195295934496218
--   3.7362434129830606 3.309685696912064 3.2089873002588005 3.6163485660751657
--   3.6234651896016463 3.4771752334356236 3.447401504705916 3.5849625007211565
--   3.3082708345352603 3.4414460711655215 3.6732696895151076 3.534219021180341
--   3.2998963911678905 3.351953074246375 3.351953074246375 3.41379956460568
--   3.477175233435624 3.463280517810811 3.3426831892554922
