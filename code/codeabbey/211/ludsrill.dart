// dartanalyzer ludsrill.dart
// Analyzing ludsrill.dart...
// No issues found!

import 'dart:core';
import 'dart:math';
import 'dart:io';

void main() {
  List<List<String>> input = getData([]);
  List<Map<String, int>> hashMaps =
      input.map((item) => makeHashMap(item, Map())).toList();
  List<double> solution = hashMaps.map((element) => entropy(element)).toList();
  print(solution.join(" "));
}

List<List<String>> getData(List<List<String>>input) {
  String getLine = stdin.readLineSync();
  if (getLine == null) {
    return input.sublist(1);
  } else {
    List<String> newLine = getLine.split("");
    return getData(input + [newLine]);
  }
}

Map<String, int> makeHashMap(List<String> file, Map<String, int> map) {
  file.forEach((letter) =>
      map[letter] = !map.containsKey(letter) ? (1) : (map[letter] + 1));
  return map;
}

double entropy(Map<String, int> file) {
  var values = file.values;
  int total = values.reduce((sum, element) => sum + element);
  double solution = values.fold(0.0,
      (prev, element) =>
          prev + ((element / total) * - (log((element / total)) / log(2))));
  return solution;
}

// cat DATA.lst | dart ludsrill.dart
// 3.286895754518763 3.3862661425265324 3.6167292966721747 3.3082708345352603
// 3.423533209849189 3.976385649991104 3.446439344671015 3.286629486786197
// 2.9655839357277824 3.5932696895151075 3.0441104177484006 3.4713544870139303
// 3.6897037321995474 3.266332639970623 3.03856233607563 3.5599000019230833
// 3.5220552088742005 3.355388542207534 3.4472624994412104
