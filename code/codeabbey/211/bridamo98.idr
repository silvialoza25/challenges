{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

toIntegerNat : Nat -> Int
toIntegerNat Z = 0
toIntegerNat (S natNumber) = 1 + toIntegerNat natNumber

injectValue : a -> IO a
injectValue dat = pure dat

getInputData : IO(List String)
getInputData = do
  line <- getLine
  if line == ""
    then injectValue []
    else do
      xs <- getInputData
      injectValue (line :: xs)

countListComp : Eq a => List a -> a -> Int
countListComp [] find = 0
countListComp ys find =
  let
    xs = [xs | xs <- ys, xs == find]
  in toIntegerNat (length xs)

rmdups : Eq a => List a -> List a
rmdups [] = []
rmdups (x::xs) =
  let
    res =
      if x `elem` xs
        then rmdups xs
        else x :: rmdups xs
  in res

calcFreq : List Char -> Char -> Double
calcFreq sentence char =
  let
    numOccu = cast {to=Double} (countListComp sentence char)
    size = cast {to=Double} (toIntegerNat (length sentence))
    freq = numOccu / size
  in freq

calcEntropies : List Char -> List Char -> List Double
calcEntropies sentence [] = []
calcEntropies sentence noDupSentence =
  let
    char = fromMaybe ' ' (head' noDupSentence)
    iFreq = calcFreq sentence char
    charEntropy = iFreq * ((log iFreq / log 2) * -1.0)
  in charEntropy :: calcEntropies sentence (drop 1 noDupSentence)

solveSingleProblem : String -> String
solveSingleProblem sentence =
  let
    divSentence = unpack sentence
    entropy = sum (calcEntropies divSentence (rmdups divSentence))
    res = show entropy
  in res

main : IO ()
main = do
  inputSize <- getLine
  inputData <- getInputData
  let solution = map solveSingleProblem inputData
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  3.5424812503605785 3.446439344671016 3.3005590923909547 2.910992253015046
  3.6818808028034042 3.5032583347756456 3.202819531114783 2.974937501201927
  3.6385304691242912 3.594905461227894 3.7080481500712317 3.3190795706241745
  3.6978458230844122 3.4992275417233567 3.319773835029478 3.0306390622295662
  3.4992275417233567 3.932999306637289 3.494680368408909 3.6234651896016468
-}
