{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

fromIntegerNat : Int -> Nat
fromIntegerNat 0 = Z
fromIntegerNat number =
  if (number > 0) then
    S (fromIntegerNat (assert_smaller number (number - 1)))
  else
    Z

abs : Double -> Double
abs n =
  if n >= 0
    then n
    else -n

replaceElement : Nat -> a -> List a -> List a
replaceElement index x xs =
  let
    left = take index xs
    right = drop (index + 1) xs
  in left ++ [x] ++ right

elemAt : Nat -> List a -> a
elemAt 0 (x::xs) = x
elemAt index (_::xs) = elemAt (minus index 1) xs

injectValue : a -> IO a
injectValue dat = pure dat

getInputData : IO(List String)
getInputData = do
  line <- getLine
  if line == ""
    then injectValue []
    else do
      xs <- getInputData
      injectValue (line :: xs)

getDriverVelocity : Double -> List Double -> Double
getDriverVelocity driverPos driverVels =
  if driverPos < 20.0
    then elemAt 0 driverVels
    else if driverPos < 40.0
      then elemAt 1 driverVels
      else if driverPos < 60.0
        then elemAt 2 driverVels
        else if driverPos < 80.0
          then elemAt 3 driverVels
          else if driverPos <= 100.0
            then elemAt 4 driverVels
            else 0.0

simulateMovement : Double -> Double -> Double -> Double -> Double
  -> Double -> Double -> Double -> List Double -> Double
simulateMovement boxPos boxVel driverPos time deltaTime
  boxMass coeElast coeFric driverVels =
  if ((driverPos >= 100.0) && ((abs (boxPos - driverPos)) <= 0.1)
    && ((abs boxVel) <= 0.1))
    then time
    else let
      springForce = coeElast * (driverPos - boxPos)
      frictionForce = coeFric * boxVel
      totalForce = springForce + frictionForce
      acceleration = totalForce / boxMass
      nxtBoxPos = boxPos + boxVel * deltaTime
      nxtBoxVel = boxVel + acceleration * deltaTime
      nxtTime = time + deltaTime
      nxtDriverPos = driverPos + (getDriverVelocity driverPos driverVels)
        * deltaTime
    in simulateMovement nxtBoxPos nxtBoxVel nxtDriverPos nxtTime
      deltaTime boxMass coeElast coeFric driverVels

checkIfValid : List Double -> Nat -> Bool
checkIfValid velocities pos =
  let
    lastElem = elemAt 4 velocities
    fixVelocities = [0.0] ++ velocities ++ [lastElem]
    curElem = elemAt pos velocities
    res = ((abs ((elemAt (pos + 2) fixVelocities) - curElem)) <= 3.0) &&
      ((abs ((elemAt pos fixVelocities) - curElem)) <= 3.0) && (curElem > 0.0)
  in res

tryOptimizationAttempt : List Double -> List Double -> (Bool, List Double)
tryOptimizationAttempt curVelocities optAttempt =
  let
    [pos, variation] = optAttempt
    forPos = fromIntegerNat (cast {to=Int} pos)
    posRes = replaceElement forPos
      ((elemAt forPos curVelocities) + variation) curVelocities
    res =
      if checkIfValid posRes forPos
        then (True, posRes)
        else (False, curVelocities)
  in res

findBestTime : List (List Double) -> List Double -> Double
  -> Double -> Double -> Double -> Double -> Double -> Double
    -> Double -> Double -> (List Double, Double)
findBestTime [] curVelocities boxPos boxVel driverPos time deltaTime boxMass
  coeElast coeFric bestTime = (curVelocities, bestTime)
findBestTime attempts curVelocities boxPos boxVel driverPos time deltaTime
  boxMass coeElast coeFric bestTime =
  let
    iAtt = fromMaybe [] (head' attempts)
    (flag, posNxtVelocities) = tryOptimizationAttempt curVelocities iAtt
    posBestTime =
      if flag
        then simulateMovement boxPos boxVel driverPos time deltaTime
          boxMass coeElast coeFric posNxtVelocities
        else bestTime
    (nxtVelocities, nxtBestTime) =
      if posBestTime < bestTime
        then (posNxtVelocities, posBestTime)
        else (curVelocities, bestTime)
  in findBestTime (drop 1 attempts) nxtVelocities boxPos boxVel driverPos
    time deltaTime boxMass coeElast coeFric nxtBestTime

main : IO ()
main = do
  inputData <- getInputData
  let
    initialStep = map (cast {to=Double}) (words (elemAt 1 inputData))
    modifications = map (\x => map (cast {to=Double}) x)
      (map words (drop 2 inputData))
    boxPos = 0.0
    boxVel = 0.0
    driverPos = 0.0
    time = 0.0
    deltaTime = 0.2
    boxMass = 5.0
    coeElast = 0.5
    coeFric = -0.5
    bestTime = 9999999.0
    (resVelocities, bestTime) = findBestTime modifications initialStep boxPos
      boxVel driverPos time deltaTime boxMass coeElast coeFric bestTime
    solution = (map show resVelocities) ++ [(show bestTime)]
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  2 3 2.0 3.4 1.7000000000000002 49.80000000000017
-}
