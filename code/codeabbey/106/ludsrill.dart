// dartanalyzer --fatal-infos --fatal-warnings --no-declaration-casts
// --no-implicit-casts --no-implicit-dynamic --lints ludsrill.darts

import 'dart:core';
import 'dart:io';
import 'dart:math';

void main() {
  List<List<String>> input = getData([]);
  List<double> speedList = input.first.map((item) =>
      double.parse(item)).toList();
  List<double> getSolution = solution(speedList, input.sublist(1),
      transportationProcess(0, 0, 0, 0, speedList, 20));
  print(getSolution.join(" "));
}

List<double> solution(List<double> start, List<List<String>> list,
    double betterTime) {
  if (list.isEmpty) {
    return [start, [betterTime]].expand((element) => element).toList();

  } else {
    List<double> getNewSpeed = getNewList(start, list.first);
    double followingTime = transportationProcess(0, 0, 0, 0, getNewSpeed, 20);
    if (followingTime < betterTime) {
      return solution(getNewSpeed, list.sublist(1), followingTime);
    } else {
      return solution(start, list.sublist(1), betterTime);
    }
  }
}

List<List<String>> getData(List<List<String>>input) {
  String getLine = stdin.readLineSync();
  if (getLine == null) {
    return input.sublist(1);
  } else {
    List<String> newLine = getLine.split(" ");
    return getData(input + [newLine]);
  }
}

bool verifyNeighborhood(int position, List<double> speedList) {
  if (position == 0) {
    if ((speedList[position] - speedList[position + 1]).abs() <= 3) {
      return true;
    } else {
      return false;
    }
  } else if (position == 4) {
    if ((speedList[position] - speedList[position - 1]).abs() <= 3) {
      return true;
    } else {
      return false;
    }
  } else {
    if ((speedList[position] - speedList[position - 1]).abs() <= 3 &&
        (speedList[position] - speedList[position + 1]).abs() <= 3) {
      return true;
    } else {
      return false;
    }
  }
}

List<double> getNewList(List<double> speedList, List<String> list) {
  String operation = list[1][0];
  double change = double.parse(list[1].substring(1));
  int position = int.parse(list[0]);

  if (operation == "+") {
    List<double> auxSpeed = modifyList(position, speedList, "+", change);
    if (auxSpeed[position] > 0 && auxSpeed[0] <= 3 &&
        verifyNeighborhood(position, auxSpeed)) {
      return auxSpeed;
    } else {
      return speedList;
    }

  } else {
    List<double> auxSpeed = modifyList(position, speedList, "-", change);
    if (auxSpeed[position] > 0 && auxSpeed[0] <= 3 &&
        verifyNeighborhood(position, auxSpeed)) {
      return auxSpeed;
    } else {
      return speedList;
    }
  }
}

List<double> modifyList(int position, List<double> speedList, String condition,
    double change) {
  List<double> auxSpeed = new List.from(speedList);
  if(condition == "+") {
    auxSpeed[position] = dp(auxSpeed[position] + change, 1);
    return auxSpeed;
  } else {
    auxSpeed[position] = dp(auxSpeed[position] - change, 1);
    return auxSpeed;
  }
}

double transportationProcess(double x, double v, double xd, double t,
    List<double> listSpeed, int checker) {
  double dt = 0.2;
  double m = 5;
  double k = 0.5;
  double b = -0.5;

  if (xd < 100) {
    double force_spring = k * (xd - x);
    double force_friction = b * v;
    double force_total = force_spring + force_friction;
    double acceleration = force_total / m;
    double auxX = x + v * dt;
    double auxV = v + acceleration * dt;
    double auxT = t + dt;
    double auxXD = xd + listSpeed.first * dt;
    if (auxXD > checker) {
      return transportationProcess(auxX, auxV, auxXD, auxT,
          listSpeed.sublist(1), checker + 20);
    } else {
      return transportationProcess(auxX, auxV, auxXD, auxT, listSpeed, checker);
    }

  } else if ((xd - x).abs() <= 0.1 && v.abs() <= 0.1) {
    return t;

  } else {
    double force_spring = k * (xd - x);
    double force_friction = b * v;
    double force_total = force_spring + force_friction;
    double acceleration = force_total / m;
    double auxX = x + v * dt;
    double auxV = v + acceleration * dt;
    double auxT = t + dt;
    double auxXD = xd;
    return transportationProcess(auxX, auxV, auxXD, auxT, [0], checker);
  }
}

double dp(double val, int places){
  double mod = pow(10.0, places).toDouble();
  return ((val * mod).round().toDouble() / mod);
}

// cat DATA.lst | dart ludsrill.dart
// 3.0 1.6 3.3 1.2 1.3 68.40000000000043
