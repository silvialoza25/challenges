{-
 $ ghc  miyyer1946.hs
   [1 of 1] Compiling Main ( miyyer1946.hs )
   Linking code ...
 $ hlint miyyer1946.hs
   No hints
-}

main = do
  n <- readLn :: IO Int
  readPairs n

readPairs :: Int -> IO ()
readPairs n = do
  array <- getLine
  let xs = map read $ words array :: [Float]
  let num_test_case = head xs / 4
  putStr $ show (validation (head xs) (xs !! 1) 1 1 - 1)
  putStr " "
  readPairs (n-1)

validation :: Float -> Float -> Float -> Int -> Int
validation a b i count = do
  let human_legs = a - i * 4
  let girls = human_legs / 2
  let pig_breasts = b - girls * 2
  let b_per_pig = pig_breasts / i;
  if i >= a / 4 - 1 then
    count
  else
    validation a b (i + 1) (count + result b_per_pig pig_breasts i)

result :: Float -> Float -> Float -> Int
result b_per_pig pig_breasts i
  | floor b_per_pig `mod` 2 == 0 && floor pig_breasts `mod` floor i == 0 = 1
  | otherwise = 0

{-
 $ cat DATA.lst | ./miyyer1946
 3 5 5 9 17 5 9 5 3 13 4 6 5 11 4 7 7
-}
