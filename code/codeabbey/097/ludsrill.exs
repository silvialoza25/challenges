# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.03s to load, 0.08s running 55 checks on 1 file)
# 5 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule GirlsAndPigs do
  def main do
    data = get_data()
    solution = Enum.map(data, &pig_and_girls(&1, 1, 0))
    Enum.map(solution, &IO.write("#{&1} "))
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def get_data do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    [_ | tail] = List.delete_at(all_data, 1 +
                  String.to_integer(Enum.at(all_data, 0)))
    Enum.map(Enum.map(tail, &String.split(&1, " ")), &to_int/1)
  end

  def pig_and_girls([a, b], add, count) do
    pigs = add
    girls = (a - 4 * pigs) / 2
    x = (b - girls * 2) / pigs
    if rem(trunc(x), 2) == 0 and x - Float.floor(x) == 0 and girls > 0 do
      pig_and_girls([a, b], add + 1, count + 1)
    else
      if x - Float.floor(x) != 0 or rem(trunc(x), 2) != 0 do
        pig_and_girls([a, b], add + 1, count)
      else
        count
      end
    end
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e GirlsAndPigs.main
# 33 3 3 3 21 3 3 7 13 13 3 30 7
