{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #097: Girls and Pigs - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi str = fromMaybe 0 $ parseInteger {a=Int} str

getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

-- Using List comprehensions
girlsAndPigs : List Int -> List (Int, Int, Int)
girlsAndPigs [legs, breasts] =
  [(girls, pigs, bpp) | -- "bpp" var is "breasts per pig"
  bpp <- [2,4..breasts],
  pigs <- [1..(div legs 4)],
  girls <- [1..(div legs 2)],
  (girls * 2) + (pigs * 4) == legs,
  (girls * 2) + (pigs * bpp) == breasts]

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse input array
    testCases = map (map stoi) $ map words inputData
    -- Process the range needed
    results = map length $ map girlsAndPigs testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  12 4 2 7 36 12 21 21 10 2 3 13 4 19 12 6 3
-}
