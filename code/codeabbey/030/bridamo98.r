# $ lintr::lint('bridamo98.r')

reverse_string <- function(input) {
  result <- intToUtf8(rev(utf8ToInt(input)))
  return(result)
}

main <- function() {
  input <- readLines("stdin")
  result <- reverse_string(input)
  cat(result, "\n")
}

main()

# $ cat DATA.lst | Rscript bridamo98.r
# rorepme nwolc elpmis elggurts ekam kcip llit blub
