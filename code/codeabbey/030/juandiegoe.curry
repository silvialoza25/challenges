-- $ curry-verify juandiegoe.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `juandiegoe'!

main :: Prelude.IO ()
main = do
  line1 <- getLine
  let cad = reverse line1
  let res = filter (/='"') cad
  putStr res

-- cat DATA.lst | pakcs :load juandiegoe.curry :eval main :quit
-- peek tnecsednacni sutcac yats nrut kcip eraf dna no tuoba
