# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
use POSIX;
our ($VERSION) = 1;

my $EMPTY = q{};
my $SPACE = q{ };

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data[ 0 .. $#data ];
}

sub solution {
  my @data = @_;
  my $vals = $data[0];
  my @dat  = split $EMPTY, $vals;
  my $len  = $#dat - 1;
  while ( $len >= 0 ) {
    exit 1 if !print $dat[$len];
    $len--;
  }
  return 'This is time of probability!';
}

sub main {
  my @vals = data_in();
  for my $test (@vals) {
    solution($test);
  }
  exit 1 if !print "\n";
  return 'You make it in the first attemp?';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# aococ erehw ffo yhw flehs nwolc eraf
