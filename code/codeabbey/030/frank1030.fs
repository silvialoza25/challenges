//#!/usr/bin/env fsharpi --exec
(*
Building frank1030 (Debug|anycpu)
Build started 11/8/2020 2:35:01 a. m..
Build succeeded.
    0 Warning(s)
    0 Error(s)
Time Elapsed 00:00:06.82
Build: 1 succeeded, 0 failed, 0 up-to-date, 0 skipped
Build successful.
*)

module main

    let main =
        let mutable word = " "
        word <- stdin.ReadLine()
        let mutable word_array = word.ToCharArray() |> Array.rev
        let reverse_word = word_array |> Array.ofSeq |> System.String
        printf "%A" reverse_word

//$ cat DATA.lst|./frank1030.fs
//sutcac tuoba ydrapoej flehs ffo reppus eraf no
