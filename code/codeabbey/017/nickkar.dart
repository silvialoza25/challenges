/*
$ dartanalyzer nickkar.dart
Analyzing nickkar.dart...
No issues found!
*/

import 'dart:io';

// reads the number of testcases from input
int getNumCases() {
  String testcases = stdin.readLineSync();
  return int.parse(testcases);
}

// reads testcase input from stdin
List<int> getTestCases() {
  List<String> str_in = stdin.readLineSync().split(" ");
  List<int> int_in = str_in.map((val) => int.parse(val)).toList();
  return(int_in);
}

// iterates over every testcase and calculates the checksum on every case
int solveTestCases(int numcases, List<int> testcases, {int index = 0,
int result = 0, int mul = 113, int modu = 10000007}){
  if (index == numcases) {
    return(result);
  }
  else {
    int ca5e = testcases[index];
    int newresult = ((result + ca5e) * mul) % modu;
    return(
      solveTestCases(numcases, testcases,
      index:index + 1, result: newresult));
  }
}

// Driver code
void main() {
  int numcases = getNumCases();
  List<int> cases = getTestCases();
  int out = solveTestCases(numcases, cases);
  print(out);
}

/*
$ cat DATA.lst | dart nickkar.dart
1496389
*/
