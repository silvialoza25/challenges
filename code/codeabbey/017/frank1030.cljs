;$ clj-kondo --lint frank1030.cljs
;linting took 485ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn checksum [index x numbers a]
  (let [res (* (+ a (nth numbers @x)) 113)]
    (if (= @x (- index 1))
      (if (>= res 10000007)
          (print (mod res 10000007))
          (print res))
      (print ""))
    (if (< @x (- index 1))
      (do (swap! x inc)
        (if (>= res 10000007)
          (checksum index x numbers (mod res 10000007))
          (checksum index x numbers res)))
      (print ""))
    (print "")))

(defn main []
  (let [index (edn/read-string (core/read-line)) list (core/read-line)
    numbers (map edn/read-string (str/split list #" ")) x (atom 0)]
    (checksum index x numbers 0)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;6389830
