(*
$ ocp-lint --severity-limit 3
  Scanning files in project "."...
  Found '3' file(s)
  Running analyses... 3 / 3
  Merging database...
  == New Warnings ==
    * 0 file(s) were linted
    * 0 warning(s) were emitted:
    * 0 file(s) couldn't be linted
*)

let modulo x y =
  let result = x mod y in
  if result >= 0 then result
  else result + y

let splitting line =
  String.split_on_char ' ' line

let maybe_read_line () =
  try Some (read_line ())
  with End_of_file -> None

let rec get_data acc =
  match maybe_read_line () with
  | Some (line) -> get_data (acc @ List.map int_of_string (splitting line))
  | None -> (List.tl acc)

let rec array_checksum data checksum =
  match data with
  | x::xs -> array_checksum xs (modulo ((checksum + x) * 113) 10000007)
  | [] -> print_int checksum

let main () =
  let data = get_data [] in
  (array_checksum data 0)

let () = main ()

(*
$ cat DATA.lst | ocaml ludsrill.ml
  1496389
*)
