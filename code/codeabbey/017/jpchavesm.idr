{-
  $ idris2 jpchavesm.idr -o jpchavesm
-}

module Main
import Data.Maybe
import Data.Strings

%default partial

seed : Int
seed = 113

limit : Int
limit = 10000007

str2int : String -> Int
str2int elem = fromMaybe 0 $ (parseInteger {a=Int} elem)

calcCheck : Int -> Int -> Int
calcCheck first prevChecksum = mod ((prevChecksum + first) * seed) limit;

arrCheck : List Int -> Int -> Int -> Int
arrCheck [] 0 result = result
arrCheck (x::xs) times result = arrCheck xs (times-1) (calcCheck x result)

main : IO ()
main = do
  size <- getLine
  strInput <- getLine
  let intArr = map str2int (words strInput)
  let checksum = arrCheck intArr (cast size) 0
  putStrLn (cast checksum)

{-
  $ cat DATA.lst | ./build/exec/jpchavesm
  1496389
-}
