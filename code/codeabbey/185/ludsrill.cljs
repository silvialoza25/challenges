;; clj-kondo --lint ludsrill.cljs
;; linting took 47ms, errors: 0, warnings: 0

(ns ludsrill.185
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
          (cons (str/split input-string #" ") (get-data))
          nil))))

(defn select-quadrant [alpha xp yp]
  (cond (and (< xp 0) (> yp 0)) (- (Math/PI) alpha)
        (and (< xp 0) (< yp 0)) (+  (Math/PI) alpha)
        (and (> xp 0) (< yp 0)) (- (* 2 (Math/PI)) alpha)
        (and (> xp 0) (> yp 0)) (+ (* 2 (Math/PI)) alpha)))

(defn simple-3d-scene [item tx ty theta]
  (let [x (edn/read-string (first item))
        y (edn/read-string (second item))
        alpha-max (+ (Math/atan (/ 0.2 0.5)) (* 2 (Math/PI)))
        alpha-min (- (* 2 (Math/PI)) (Math/atan (/ 0.2 0.5)))
        xp (+ (* (Math/cos theta) (- x tx)) (* (Math/sin theta) (- y ty)))
        yp (+ (* (- (Math/sin theta)) (- x tx)) (* (Math/cos theta) (- y ty)))
        hypo (Math/sqrt (+ (* xp xp) (* yp yp)))
        alpha (Math/abs (Math/atan (/ yp xp)))]

    (when (and (< hypo 60) (> (select-quadrant alpha xp yp) alpha-min)
               (< (select-quadrant alpha xp yp) alpha-max))
      (print (core/format "%s " xp))
      (print (core/format "%s " yp)))))

(defn -main []
  (let [data (rest (get-data))
        tx (edn/read-string (first (first data)))
        ty (edn/read-string  (second (first data)))
        theta (edn/read-string  (nth (first data) 2))]

    (doseq [item (rest data)]
      (simple-3d-scene item tx ty theta))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 54.46484496731092 9.92877951647567 2.140350087356797 0.647226006547752
;; 32.21544601948165 -1.4714067302631983 33.061344093742946 3.4565194509981483
;; 56.94355428437223 12.547175995527958 18.3877602590886 -5.1855831546974525
;; 34.21610894484747 4.272925072398159 47.61695811695216 -12.232142072769749
;; 59.05441191886774 7.111710969709836 41.005010889483664 14.268464596911826
;; 6.251870647218133 0.9560927833909869 29.14849560159515 10.215928942773056
;; 40.9460259837611 2.1030825321800792 51.92715074452703 -4.85499902730837
;; 57.840662162040644 -5.870076716421922 43.08637607111789 2.7503085387278317
