#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defvar *data*)
(defvar *aux-first-chunk*)
(defvar *aux-second-chunk*)
(defvar *transformation*)
(defparameter *aux* ())
(defparameter *first-chunk* ())
(defparameter *second-chunk* ())

(setq *read-default-float-format* 'single-float)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun euclidian-distance (item)
  (let ((x1 (first (first item)))
        (y1 (second (first item)))
        (x2 (first (second item)))
        (y2 (second (second item)))
        (distance))
    (setq distance (sqrt (+ (expt (- x2 x1) 2) (expt (- y2 y1) 2))))))

(defun make-cluster (data)
  (let ((group-stars ())
        (aux-stars ()))
    (dolist (item data)
      (loop for i from 0 to (- (length data) 1)
        do(when (and (< (abs (- (first item) (first (elt data i)))) 5)
                 (< (abs (- (second item) (second (elt data i)))) 5))
            (setq aux-stars (append aux-stars (list (elt data i))))))
      (when (> (length aux-stars) 1)
        (setq group-stars (append group-stars (list aux-stars))))
      (setq aux-stars ()))
    (return-from make-cluster group-stars)))

(defun minimum (list)
  (let ((aux-x ())
        (aux-y ()))
    (dolist (item list)
      (setq aux-x (append aux-x (list (first item))))
      (setq aux-y (append aux-y (list (second item)))))
    (list (reduce #'min aux-x) (reduce #'min aux-y))))

(defun maximum (list)
  (let ((aux-x ())
        (aux-y ()))
    (dolist (item list)
      (setq aux-x (append aux-x (list (first item))))
      (setq aux-y (append aux-y (list (second item)))))
    (list (reduce #'max aux-x) (reduce #'max aux-y))))

(defun make-cluster-matching (data)
  (let ((filtered-data data)
        (aux-list ())
        (aux-list-chunks ())
        (aux-cluster-chunk2 ())
        (euclidian2())
        (aux-cluster-chunk1 ())
        (euclidian1 ())
        (avg-coordinate-chunk1)
        (avg-coordinate-chunk2)
        (cluster-chunk1)
        (cluster-chunk2))

    (dolist (item filtered-data)
      (if (= (length item) 2)
        (setq aux-list (append aux-list (list item)))
        (setq aux-list-chunks (append aux-list-chunks item))))

    (loop for i from 0 to (- (first aux-list-chunks) 1)
      do(setq *first-chunk* (append *first-chunk* (list (elt aux-list i))))
      (setq *aux-first-chunk* (append *first-chunk* (list (elt aux-list i)))))

    (loop for i from 0 to (- (second aux-list-chunks) 1)
      do(setq *second-chunk* (append *second-chunk*
        (list (elt aux-list (+ i (first aux-list-chunks))))))
      (setq *aux-second-chunk* (append *second-chunk*
      (list (elt aux-list (+ i (first aux-list-chunks)))))))

    (sort *first-chunk*  #'< :key #'car)
    (sort *second-chunk*  #'< :key #'car)

    (setq cluster-chunk1 (make-cluster *first-chunk*))
    (setq cluster-chunk2 (make-cluster *second-chunk*))

    (dolist (item cluster-chunk1)
      (when (= (length item) 2)
        (setq aux-cluster-chunk1 (append aux-cluster-chunk1 (list item)))
        (setq euclidian1 (append euclidian1
                          (list (euclidian-distance item))))))

    (dolist (item cluster-chunk2)
      (when (= (length item) 2)
        (setq aux-cluster-chunk2 (append aux-cluster-chunk2 (list item)))
        (setq euclidian2(append euclidian2 (list (euclidian-distance item))))))

    (loop for i from 0 to (- (length aux-cluster-chunk1) 1)
      do(setq avg-coordinate-chunk1
        (list (/ (+ (first (first (elt aux-cluster-chunk1 i)))
              (first (second (elt aux-cluster-chunk1 i)))) 2)
              (/ (+ (second (first (elt aux-cluster-chunk1 i)))
              (second (second (elt aux-cluster-chunk1 i)))) 2)))

      (loop for j from 0 to (- (length aux-cluster-chunk2) 1)
        do(setq avg-coordinate-chunk2
          (list (/ (+ (first (first (elt aux-cluster-chunk2 j)))
                (first (second (elt aux-cluster-chunk2 j)))) 2)
                (/ (+ (second (first (elt aux-cluster-chunk2 j)))
                (second (second (elt aux-cluster-chunk2 j)))) 2)))

        (when (and (< (abs (- (elt euclidian1 i) (elt euclidian2 j))) 0.1)
              (< (abs (- (first avg-coordinate-chunk1)
              (first avg-coordinate-chunk2))) 5)
              (< (abs (- (second avg-coordinate-chunk1)
              (second avg-coordinate-chunk2))) 5))

          (setq *aux* (append *aux* (list (list (elt aux-cluster-chunk1 i)
                      (elt aux-cluster-chunk2 j))))))))))

(defun transformation-of-coordinates ()
  (let* ((theta) (tx) (ty) (xtrans) (ytrans)
         (cluster1 (first (first *aux*)))
         (cluster2 (second (first *aux*)))
         (x1 (first (first cluster1)))
         (y1 (second (first cluster1)))
         (x2 (first (second cluster1)))
         (y2 (second (second cluster1)))
         (x1p (first (first cluster2)))
         (y1p (second (first cluster2)))
         (x2p (first (second cluster2)))
         (y2p (second (second cluster2))))

    (setq theta (- (atan (/ (- y2p y1p) (- x2p x1p)))
                (atan (/ (- y2 y1) (- x2 x1)))))

    (setq tx (- (+ x1p (* y1 (sin theta))) (* x1 (cos theta))))
    (setq ty (- (- y1p (* x1 (sin theta))) (* y1 (cos theta))))

    (setq *transformation* ())
    (loop for i from 0 to (- (length *first-chunk*) 1)
      do(setq xtrans (+ (- (* (first (elt *first-chunk* i)) (cos theta))
                  (* (second (elt *first-chunk* i)) (sin theta))) tx))
      (setq ytrans (+ (+ (* (first (elt *first-chunk* i)) (sin theta))
                  (* (second (elt *first-chunk* i)) (cos theta))) ty))
      (setq *transformation* (append *transformation*
                              (list (list xtrans ytrans)))))))

(defun find-wandering-star()
  (let ((final ())
        (copy-item)
        (transformation-in-chunk2 ())
        (diff-btw-chunks ())
        (filtered-diff ())
        (cluster-for-diff ())
        (wandering-star ())
        (aux-position)
        (aux-position-2)
        (inf-limit-xy)
        (sup-limit-xy))

    (dolist (item *transformation*)
      (dolist (item2 *second-chunk*)
        (when (and (< (abs (- (first item) (first item2))) 0.6)
              (< (abs (- (second item) (second item2))) 0.6))
          (setq transformation-in-chunk2
          (append transformation-in-chunk2 (list item))))))

    (dolist (item *transformation*)
        (when (equal (find item transformation-in-chunk2 :test #'equal) NIL)
          (setq diff-btw-chunks (append diff-btw-chunks (list item)))))

    (setq inf-limit-xy (minimum *second-chunk*))
    (setq sup-limit-xy (maximum *second-chunk*))

    (dolist (item diff-btw-chunks)
      (when (and (< (first item) (first sup-limit-xy))
            (< (second item) (second sup-limit-xy))
            (> (first item) (first inf-limit-xy))
            (> (second item) (second inf-limit-xy)))
        (setq filtered-diff (append filtered-diff (list item)))))

    (dolist (item filtered-diff)
      (loop for i from 0 to (- (length *second-chunk*) 1)
        do(when (and (< (abs (- (first item)
                      (first (elt *second-chunk* i)))) 5)
                (< (abs (- (second item) (second (elt *second-chunk* i)))) 5))
          (setq cluster-for-diff (append cluster-for-diff
                                  (list (elt *second-chunk* i))))))
      (setq final (append final (list (append cluster-for-diff (list item)))))
      (setq cluster-for-diff ()))

    (dolist (item final)
    (setq copy-item item)
      (dolist (item2 item)
        (loop for i in transformation-in-chunk2
          do(when (and (< (abs (- (first item2) (first i))) 0.8)
                    (< (abs (- (second item2) (second i))) 0.8))
            (setq copy-item (remove item2 copy-item :test #'equal)))))
      (when (= (length copy-item) 2)
        (setq wandering-star (append wandering-star (list copy-item)))))

    (dolist (item wandering-star)
      (when (and (< (abs (- (first (first item)) (first (second item)))) 0.8)
                (< (abs (- (second (first item)) (second (second item)))) 0.8))
        (setq wandering-star (remove item wandering-star :test #'equal))))

    (setq wandering-star (first wandering-star))
    (setq aux-position (second wandering-star))
    (setq aux-position-2 (position aux-position *transformation*
                          :test #'equal))
    (format t "~s " (position (elt *first-chunk* aux-position-2)
                    *aux-first-chunk* :test #'equal))
    (format t "~s " (position (first wandering-star)
                    *aux-second-chunk* :test #'equal))))

(setq *data* (read-data))
(setq *parameters* (car *data*))
(make-cluster-matching *data*)
(transformation-of-coordinates)
(find-wandering-star)

#|
cat DATA.lst | clisp ludsrill.lsp
13 22
|#
