;; clj-kondo --lint ludsrill.cljs
;; linting took 28ms, errors: 0, warnings: 0

(ns ludsrill.034
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn solution [item x x-mayor x-menor]
  (let [A (edn/read-string (first item))
        B (edn/read-string (second item))
        C (edn/read-string (nth item 2))
        D (edn/read-string (nth item 3))]

    (if (> (- (- (+ (* A x) (* B (Math/sqrt (* x x x))))
                (* C (Math/exp (/ (- x) 50)))) D) 0.0000001)
        (recur item (/ x-mayor 2) x x-menor)
        (if (< (- (- (+ (* A x) (* B (Math/sqrt (* x x x))))
                    (* C (Math/exp (/ (- x) 50)))) D) 0.00000001)
            (recur item (/ (+ x-mayor x-menor) 2) x-mayor x)
            (print (core/format "%s " (double x)))))))

(defn -main []
  (let [data (rest (get-data))]
    (doseq [item data]
      (solution item 100 100 0))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 93.1178532585869 87.76579049215295 80.55702906836746 65.3463028376552
;; 16.73298186111577 68.4765892876869 80.66302867316449
