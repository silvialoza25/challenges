-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import Float
import IO
import List
import Read

getData :: IO String
getData = do
  x <- getContents
  return x

strToF :: String -> Float
strToF s = read s :: Float

findX :: Float -> Float -> Float -> Float -> Float -> Float -> Float
findX constA constB constC constD initP finalP =
  let
    newX = (initP + finalP) / 2
    precision = 0.000000000001
    termOne = constA * newX
    termTwo = constB * sqrt (newX ^. 3)
    termThree = constC * (exp (-newX / 50))
    result = termOne + termTwo - termThree - constD
  in
    if (abs result) < precision then newX
      else if result > 0 then findX constA constB constC constD initP newX
        else findX constA constB constC constD newX finalP

getValues :: String -> Float
getValues dataIn = result
  where
    array = words dataIn
    constA = strToF (array !! 0)
    constB = strToF (array !! 1)
    constC = strToF (array !! 2)
    constD = strToF (array !! 3)
    result = findX constA constB constC constD 0 100

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> show (getValues x)) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 38.596698774473026 90.07990762685317 55.53411867403746 90.72439450253523
-- 80.52462413979643 33.08137744647625 60.516067618780234
