/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, float, char, math.

:- func checkSolution(float, float, float, float, float, float, float, float)
  = float.
checkSolution(ConstA, ConstB, ConstC, ConstD, MinRange, MaxRange, CurrentX,
  Result) =
(
  if abs(Result) < 0.00000001
  then CurrentX
  else if Result > 0.0
  then calcBinSearch(ConstA, ConstB, ConstC, ConstD, MinRange, CurrentX)
  else calcBinSearch(ConstA, ConstB, ConstC, ConstD, CurrentX, MaxRange)
).

:- func calcBinSearch(float, float, float, float, float, float) = float.
calcBinSearch(ConstA, ConstB, ConstC, ConstD, MinRange, MaxRange) = Answer :-
(
  CurrentX = MinRange + (MaxRange - MinRange) / 2.0,
  Result = ConstA * CurrentX + ConstB * sqrt(pow(CurrentX, 3.0))
    - ConstC * exp(-CurrentX / 50.0) - ConstD,
  Answer = checkSolution(ConstA, ConstB, ConstC, ConstD, MinRange, MaxRange,
    CurrentX, Result)
).

:- pred binarySearch(list(string)::in, string::in, string::out) is det.
binarySearch([], FinalResult, FinalResult).
binarySearch([Line | Tail], PartialResult, FinalResult) :-
(
  string.words_separator(char.is_whitespace, Line) = Input,
  det_to_float(det_index0(Input, 0)) = ConstA,
  det_to_float(det_index0(Input, 1)) = ConstB,
  det_to_float(det_index0(Input, 2)) = ConstC,
  det_to_float(det_index0(Input, 3)) = ConstD,
  calcBinSearch(ConstA, ConstB, ConstC, ConstD, 0.0, 100.0) = Answer,
  PartialResult1 = PartialResult ++ " " ++ float_to_string(Answer),
  binarySearch(Tail, PartialResult1, FinalResult)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
                   list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    list.det_drop(1, FileContents, ComputeList),
    binarySearch(ComputeList, "", FinalResult),
    io.print_line(FinalResult, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  53.90027868124889 68.7333886133274 66.41882108233403 94.67274136768538
  39.888521731700166 35.807384254803765 30.238088115584105
*/
