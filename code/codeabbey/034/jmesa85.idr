{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #034: Binary Search - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stod : String -> Double
stod str = fromMaybe 0 $ parseDouble str

-- Gets lines in the Stdin
getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

binarySearch : List Double -> Double -> Double -> Double
binarySearch constants low high =
  let
    PRECISION = 0.00000001
    [conA, conB, conC, conD] = constants
    value = low + (high - low) / 2
    result =
      conA * value + conB * sqrt(pow value 3) - conC * exp (-value / 50) - conD
  in
    if (abs result) < PRECISION then value
      else if result > 0 then binarySearch constants low value
        else  binarySearch constants value high

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse test cases
    testCases = map (map stod) $ map words inputData
    -- Process data
    results = map (\elem => binarySearch elem 0 100.0) testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  53.90027868124889 68.7333886133274 66.41882108233403
  94.67274136768538 39.888521731700166 35.807384254803765
  30.238088115584105
-}
