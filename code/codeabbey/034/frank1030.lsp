#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

$ clisp -c frank1030.lsp
Compiling file /Users/Frank/Desktop/frank/034/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/034/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-dat()
  (let ((dat))
    (setq dat(read))
  )
)

(defun solution()
  (let ((index 0) (a 0) (b 0) (c 0) (d 0) (res 0) (mini 0) (maxi 100)
    (mid 0.0) (f 0) (s 0))
    (setq index(get-dat))
    (loop for i from 0 to (1- index)
      do (setq mini 0)
      do (setq maxi 100)
      do (setq res 0)
      do (setq f 0)
      do (setq s 0)
      do (setq a(get-dat))
      do (setq b(get-dat))
      do (setq c(get-dat))
      do (setq d(get-dat))
      do (loop while (= s 0)
        do (setq mid (/(+ maxi mini) 2))
        do (setq f (+ (* a mid) (* b (sqrt(expt mid 3)))))
        do (setq f (- f (* c (exp (/ (* mid -1) 50)))))
        do (setq f (- f d))
        do (if (< f 0)
          (setq mini mid)
          (setq maxi mid)
        )
        do (if (and (< f 0.0000001) (> f -0.0000001))
          (progn
            (setq res mid)
            (setq s 1)
          )
        )
      )
      do (format t "~,7f " res)
    )
  )
)

(solution)

#|
$ cat DATA.lst | clisp frank1030.lsp
58.7017300 44.2712330 65.4955500 55.5414600 34.6687200 89.7230500
|#
