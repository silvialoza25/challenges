# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.01s to load, 0.08s running 55 checks on 1 file)
# 6 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule QuadraticEquation do
  def main do
    all_data = IO.read(:stdio, :all)
    try do
      get_data(all_data)
        |> Enum.map(&quadratic_equation/1)
        |> Enum.map(&IO.write("#{hd(&1)} #{List.last(&1)}; "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
    |> Enum.reject(fn item -> item == "" end)
    |> Enum.map(&String.split(&1, " "))
    |> Enum.map(&to_int/1)
    |> tl()
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def quadratic_equation([item_A, item_B, item_C]) do
    aux = :math.pow(item_B, 2) - 4 * item_A * item_C
    if aux < 0 do
      first_real_part = div(- item_B, 2 * item_A)
      second_real_part = div(- item_B, 2 * item_A)
      first_root = Integer.to_string(first_real_part) <> "+" <>
        Integer.to_string(div(Kernel.round(:math.sqrt(- aux)), 2 * item_A)) <>
        "i"
      second_root = Integer.to_string(second_real_part) <> "-" <>
        Integer.to_string(div(Kernel.round(:math.sqrt(- aux)), 2 * item_A)) <>
        "i"
      [first_root, second_root]
    else
      first_root = Kernel.round((- item_B + :math.sqrt(aux)) / (2 * item_A))
      second_root = Kernel.round((- item_B - :math.sqrt(aux)) / (2 * item_A))
      Enum.sort([first_root, second_root], :desc)
    end
  end
end

QuadraticEquation.main()

# cat DATA.lst | elixir ludsrill.exs
# -3 -5; 10 1; 3 -10; 9 7; -10+9i -10-9i; 2 -8; 5 -1; 5 -7; 10 -1; 10 -1;
# 2+8i 2-8i; 4+10i 4-10i; 7 -7; -5+6i -5-6i; -10+1i -10-1i
