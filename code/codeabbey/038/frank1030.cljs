;$ clj-kondo --lint frank1030.cljs
;linting took 1521ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.pprint :refer [cl-format]]))

(defn validator [index]
  (let [x (atom index) a (core/read) b (Math/pow (core/read) 1)
    c (core/read) res (- (Math/pow b 2) (* 4 a c))
    n3 (Math/round (/ (* b -1) (* 2 a)))
    n4 (Math/round (/ (Math/sqrt (* res -1)) (* 2 a)))]
    (if (> @x 0)
      (do (swap! x dec)
        (if (>= res 0)
         (print (cl-format nil "~d ~d; "
         (/ (+ (* b -1) (Math/sqrt res)) (* 2 a))
         (/ (- (* b -1) (Math/sqrt res)) (* 2 a))))
         (print (cl-format nil "~d+~di ~d-~di; " n3 n4 n3 n4)))
        (if (not= @x 0)
          (validator @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (core/read)]
    (validator index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;-5+5i -5-5i; 6+6i 6-6i; 6+2i 6-2i; 2 0; 7 2; 2+7i 2-7i; -1 -6; 5+10i 5-10i;
;-3+5i -3-5i; 10+8i 10-8i; 9 -10; 0 -9; 9+1i 9-1i
