#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun find-roots (a b c)
  (let ((inside-sqrt (- (expt b 2) (* 4 a c)))
  (first-term (/ (* b (- 0 1)) (* 2 a)))
  (second-term NIL)(result NIL))
    (if (< inside-sqrt 0)
      (progn
        (setq second-term
          (/ (sqrt (* inside-sqrt (- 0 1))) (* 2 a)))
        (setq result (concatenate 'string
        (write-to-string first-term) "+"
        (write-to-string second-term) "i" " "
        (write-to-string first-term) "-"
        (write-to-string second-term) "i")))
      (progn
        (setq second-term (/ (sqrt inside-sqrt) (* 2 a)))
        (setq result (concatenate 'string
        (write-to-string (+ first-term second-term)) " "
        (write-to-string (- first-term second-term))))))
    (return-from find-roots result)))

(defun main ()
  (let ((size-input (read)))
    (loop for i from 0 to (- size-input 1)
      do(format t "~a; " (find-roots (read) (read) (read))))))

(main)

#|
  cat DATA.lst | clisp bridamo98.lsp
  0+6i 0-6i; 1 -8; -2 -8; -10+5i -10-5i; 10 -1; 9+5i 9-5i; 2+8i 2-8i; 5 -4;
  7+2i 7-2i; 5 -1; 7 7; -4 -6; 3+2i 3-2i; 5 2; 0+8i 0-8i; -8+6i -8-6i; 5 0
|#
