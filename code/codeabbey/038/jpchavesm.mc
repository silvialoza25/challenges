/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, float, math, char.

:- pred str2float(string::in, float::out) is det.
str2float(X, string.det_to_float(X)).

:- pred getRealRoots(float, float, float, string, string).
:- mode getRealRoots(in, in, in, out, out) is det.
getRealRoots(ConstA, ConstB, ConstC, Root1, Root2) :-
(
  Discr = ConstB * ConstB - 4.0 * ConstA * ConstC,
  IRoot1 = round_to_int((-ConstB + sqrt(Discr)) / (2.0 * ConstA)),
  IRoot2 = round_to_int((-ConstB - sqrt(Discr)) / (2.0 * ConstA)),
  Root1 = from_int(IRoot1),
  Root2 = from_int(IRoot2)
).

:- pred getComplexRoots(float, float, float, string, string).
:- mode getComplexRoots(in, in, in, out, out) is det.
getComplexRoots(ConstA, ConstB, ConstC, Root1, Root2) :-
(
  Discr = ConstB * ConstB - 4.0 * ConstA * ConstC,
  RealRoot = round_to_int(-ConstB / (2.0 * ConstA)),
  CompRoot = round_to_int(sqrt(-Discr) / (2.0 * ConstA)),
  Root1 = from_int(RealRoot) ++ "+" ++ from_int(CompRoot) ++ "i",
  Root2 = from_int(RealRoot) ++ "-" ++ from_int(CompRoot) ++ "i"
).

:- pred solveEquation(float, float, float, string, string).
:- mode solveEquation(in, in, in, out, out) is det.
solveEquation(ConstA, ConstB, ConstC, Root1, Root2) :-
(
  if ConstB * ConstB - 4.0 * ConstA * ConstC >= 0.0
  then getRealRoots(ConstA, ConstB, ConstC, Root1, Root2)
  else getComplexRoots(ConstA, ConstB, ConstC, Root1, Root2)
).

:- pred getRoots(list(string), string, string).
:- mode getRoots(in, in, out) is det.
getRoots([], FinalStr, strip(FinalStr)).
getRoots([Line | Tail], PartialStr, FinalStr) :-
(
  words_separator(char.is_whitespace, Line) = StrList,
  map(str2float, StrList, FloatList),
  det_index1(FloatList, 1) = ConstA,
  det_index1(FloatList, 2) = ConstB,
  det_index1(FloatList, 3) = ConstC,
  solveEquation(ConstA, ConstB, ConstC, Root1, Root2),
  PartialStr1 = PartialStr ++ " " ++ Root1 ++ " " ++ Root2 ++ ";",
  getRoots(Tail, PartialStr1, FinalStr)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1,FileContents,RootList),
    getRoots(RootList, "", ResultStr),
    io.print_line(det_remove_suffix(ResultStr, ";"), !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  -3 -5; 10 1; 3 -10; 9 7; -10+9i -10-9i; 2 -8; 5 -1; 5 -7; 10 -1;
  10 -1; 2+8i 2-8i; 4+10i 4-10i; 7 -7; -5+6i -5-6i; -10+1i -10-1i
*/
