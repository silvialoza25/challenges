# $ lintr::lint('dantivar.r')

input <- readLines("stdin")
i <- 1
result <- c()

objects <- as.integer(input[1])

while (i <= objects) {
    values <- as.integer(unlist(strsplit(input[i + 1], " ")))
    a <- values[1]
    b <- values[2]
    c <- values[3]

    sqrt <- b ** 2 - 4 * a * c
    if (sqrt < 0) {
        x1 <- (-b + sqrt(as.complex(sqrt))) / (2 * a)
        x2 <- (-b - sqrt(as.complex(sqrt))) / (2 * a)
    } else {
        x1 <- (-b + sqrt(sqrt)) / (2 * a)
        x2 <- (-b - sqrt(sqrt)) / (2 * a)
    }

    x1 <- toString(round(x1))
    x2 <- toString(round(x2))

    value <- paste(x1, x2, sep = " ")
    result <- append(result, value)
    i <- i + 1
}

cat(paste(result, collapse = "; "))
# $ cat DATA.lst | Rscript dantivar.r
# -5+5i -5-5i; 6+6i 6-6i; 6+2i 6-2i; 2 0; 7 2; 2+7i 2-7i; -1 -6; 5+10i 5-10i;
# -3+5i -3-5i; 10+8i 10-8i; 9 -10; 0 -9; 9+1i 9-1i
