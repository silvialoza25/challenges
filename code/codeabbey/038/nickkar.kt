/*
# Lint
$ ktlint --experimental --verbose --disabled_rules=experimental:indent,indent \
> nickkar.ky
# Compile
$ kotlinc -Werror nickkar.kt -include-runtime -d nickkar.jar
*/

// reads the input and puts it in a readable array
fun read_input(
    num_test: Int,
    index: Int = 0,
    test_cases: Array<IntArray> = emptyArray()
): Array<IntArray> {
  if (index == num_test) {
    return (test_cases)
  } else {
    val (a, b, c) = readLine()!!.split(' ').map(String::toInt)
    var values: IntArray = intArrayOf(a, b, c)
    var tmp = test_cases.plus(values)
    return (read_input(num_test, index + 1, tmp))
  }
}

// solves the quadratic function for real and imaginary numbers
fun solve_quadratic(num_a: Int, num_b: Int, num_c: Int): String {
  if (((num_b * num_b) - (4 * num_a * num_c)) < 0) {
    var complex =
    Math.sqrt((-((num_b * num_b) - (4 * num_a * num_c))).toDouble())
    var first = Math.round((-num_b / (2 * num_a)).toDouble())
    var second = Math.round(complex / (2 * num_a))
    return(first.toString() + "+" + second.toString() + "i" + " " +
    first.toString() + "-" + second.toString() + "i")
  } else {
    var root = Math.sqrt(((num_b * num_b) - 4 * num_a * num_c).toDouble())
    var x1 = Math.round((-num_b + root) / (2 * num_a))
    var x2 = Math.round((-num_b - root) / (2 * num_a))
    return(Math.max(x1, x2).toString() + " " + Math.min(x1, x2).toString())
  }
}

// iterates over every test-case
fun solve_cases(
    num_test: Int,
    test_cases: Array<IntArray>,
    index: Int = 0,
    outt: Array<String> = emptyArray()
): Array<String> {
  if (index == num_test) {
    return outt
  } else {
    val (num_a, num_b, num_c) = test_cases[index]
    var roots = solve_quadratic(num_a, num_b, num_c)
    var tmp = outt.plus(roots)
    return(solve_cases(num_test, test_cases, index + 1, tmp))
  }
}

// driver code
fun main() {
  var num_test = readLine()!!.toInt()
  var test_cases = read_input(num_test)
  var outt = solve_cases(num_test, test_cases)
  println(outt.joinToString("; "))
}

/*
$ cat DATA.lst | java -jar nickkar.jar
0+6i 0-6i; 1 -8; -2 -8; -10+5i -10-5i; 10 -1; 9+5i 9-5i; 2+8i 2-8i; 5 -4;
7+2i 7-2i; 5 -1; 7 7; -4 -6; 3+2i 3-2i; 5 2; 0+8i 0-8i; -8+6i -8-6i; 5 0
*/
