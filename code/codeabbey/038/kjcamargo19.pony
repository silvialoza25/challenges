// ponyc -b kjcamargo19

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun iterator(data: Array[String]): Array[String] =>
          data.slice(1, data.size())

        fun get_roots(const_a: F64, const_b: F64, const_c: F64) =>
          var b_pow2: F64 = const_b.pow(2)
          var const_4ac: F64 = 4 * const_a * const_c
          var const_2a: F64 = 2 * const_a
          if (b_pow2 - const_4ac) >= 0 then
            var res1: F64 = (-const_b + (b_pow2 - const_4ac).sqrt()) / const_2a
            var res2: F64 = (-const_b - (b_pow2 - const_4ac).sqrt()) / const_2a
            env.out.write(res1.string() + " " + res2.string())
          else
            var res1: F64 = -const_b / const_2a
            var res2: F64 = (const_4ac - b_pow2).sqrt() / const_2a
            env.out.write(res1.string() + "+" + res2.string() + "i ")
            env.out.write(res1.string() + "-" + res2.string() + "i")
          end

        fun obtain_data(data: Array[String])? =>
          if data.size() > 0 then
            var line: Array[String] = data(0)?.split_by(" ")
            var const_a: F64 = line(0)?.f64()?
            var const_b: F64 = line(1)?.f64()?
            var const_c: F64 = line(2)?.f64()?

            get_roots(const_a, const_b, const_c)
            env.out.write("; ")
            obtain_data( iterator (data))?
          end

        fun ref apply(data: Array[U8] iso) =>
          var input_data: String = String.from_iso_array(consume data)
          var line_sep: Array[String] = input_data.split_by("\n")
          var real_data: Array[String] = line_sep.slice(1, line_sep.size()-1)

          try
            obtain_data(real_data)?
          end

        fun ref dispose() =>
          None

      end,
      512
    )

// cat DATA.lst | ./kjcamargo19
// 8 -7; 10+6i 10-6i; 2 1; -2+3i -2-3i; 10+6i 10-6i; -5 -10; 4 -10;
// 9+8i 9-8i; 2+2i 2-2i; 10 4; -9+1i -9-1i; -10+8i -10-8i; -8+4i -8-4i; 9 -1;
