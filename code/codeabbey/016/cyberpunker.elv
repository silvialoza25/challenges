# elvish -compileonly cyberpunker.elv

#!/bin/elvish

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn averageArray [data]{
  each [row]{
    var long = (+ (to-lines $row[0..]))
    var size = (count $row[1..])
    var result = (/ $long $size)
    put (math:round $result)
  } $data
}

echo (averageArray (input_data))

# cat DATA.lst | elvish cyberpunker.elv
# 2455 81 1058 647 223 450 1175 7449 6749 233
