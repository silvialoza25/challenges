#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun delimiterp (c)
  (char= c #\Space))

(defun split (string &key (delimiterp #'delimiterp))
  (loop
    :for beg = (position-if-not delimiterp string)
    :then (position-if-not delimiterp string :start (1+ end))
    :for end = (and beg (position-if delimiterp string :start beg))
    :when beg :collect (subseq string beg end)
    :while end))

(defun calc-avg (line)
  (let ((number-list (map 'list #'parse-integer (split line))))
    (format t "~a " (round (/ (reduce '+ number-list)
    (- (length number-list) 1))))))

(defun solve-all (size-input)
  (loop for i from 0 to (- size-input 1)
    do(calc-avg (read-line))))

(defun main ()
  (let ((size-input (read)))
    (solve-all size-input)))

(main)

#|
  cat DATA.lst | clisp bridamo98.lsp
  305 5515 1128 1037 243 245 1163 6236 529 456
|#
