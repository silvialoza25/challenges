###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #16: Average of an array - Coffeescript

# Returns the formatted result
processTestCase = (testCaseStr) ->
  numbersArr = testCaseStr.split(' ').map(Number)
  numbersSum = numbersArr.reduce((a, b) -> a + b)
  # Return average, last 0 is not counted
  Math.round(numbersSum / (numbersArr.length - 1))

main = ->
  # Read STDIN
  data = require('fs').readFileSync(0).toString().trim().split('\n')
  # Discard number of test cases at index 0
  testCasesAsStrings = data.slice(1)
  # Process each test case
  results = testCasesAsStrings.map(processTestCase)
  # Print results
  console.log(results.join(' '))
  return

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
3341 4008 81 2541 650 10968 1304 6709 1167 128 151 569 1461
###
