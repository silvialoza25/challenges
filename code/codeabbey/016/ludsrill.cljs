;; clj-kondo --lint ludsrill.cljs
;; linting took 30ms, errors: 0, warnings: 0

(ns ludsrill.016
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
          (cons (str/split input-string #" ") (get-data))
          nil))))

(defn round [value]
  (if (>= (Math/abs (- (Math/floor value) value)) 0.5)
    (+ (Math/floor value) 1)
    (Math/floor value)))

(defn -main []
  (let [data (rest (get-data))]
    (doseq [item data]
      (print (core/format "%.0f " (round (/ (reduce + (map #(Integer. %) item))
                                          (- (count item) 1))))))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 1975 4624 8438 4078 8231 113 163 949 1066
