# $ lintr::lint('nickkar.r')

format <- function(vec, index=1, out=c()) {
  if (vec[index] == 0) {
      return(out)
  } else {
      format(vec, index + 1, c(out, as.double(vec[index])))
  }
}

array_avrg <- function(cases, out = c(), index=1) {
  if (index > length(cases)) {
    return(out)
  }
  else {
      tmp <- format(unlist(strsplit(cases[index], " ")))
      array_avrg(cases, (append(out, round(mean(tmp)))), index + 1)
  }
}

main <- function() {
  data <- readLines("stdin")
  out <- array_avrg(data[2:length(data)])
  cat(out, "\n")
}

main()

# $ cat DATA.lst | Rscript nickkar.r
# 305 5515 1128 1037 243 245 1163 6236 529 456
