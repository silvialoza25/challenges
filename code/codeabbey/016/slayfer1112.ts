/*
$ eslint slayfer1112.ts
$ tsc slayfer1112.ts
*/
function solution(entry: string[]): number {
  const entryF: number[] = entry.map(parseFloat);
  const sum: number = entryF.reduce((acc, val) => acc + val, 0);
  const avg: number = Math.round(sum / (entryF.length - 1));
  process.stdout.write(`${avg} `);
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat = entryArray.slice(1, entryArray.length);
    dat.map((val) => solution(val.split(' ')));
    process.stdout.write('\n');
    return 0;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
277 611 10977 517 1098 5051 135 138 947 4499 7933 4607 471
*/
