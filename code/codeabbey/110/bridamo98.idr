{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

factorial : Int -> Double
factorial 0 = 1.0
factorial number = (cast {to=Double} number) * factorial (number - 1)

findPosTickets : Int -> Int -> Int -> Int -> Int
findPosTickets iLimit limit base maxLimit =
  foldl (calcRepOfTiecket) 0 [0..(maxLimit)]
  where
    calcRepOfTiecket : Int -> Int -> Int
    calcRepOfTiecket curRes iMaxLimit =
      let
        fstCounter =
          if (iMaxLimit `mod` 2) == 0
            then 1
            else -1
        sndCounter = factorial (iLimit - (iMaxLimit * base) + limit - 1)
        thrdCounter = factorial (iLimit - (iMaxLimit * base))
        fthCounter = (factorial (limit - iMaxLimit)) * (factorial iMaxLimit)
        iRes =
          if (thrdCounter * fthCounter) == 0.0
            then 0
            else cast {to=Int} ((fstCounter * sndCounter *
              (cast {to=Double} limit)) / (thrdCounter * fthCounter))
        res = curRes + iRes
      in res

findLuckyTicketsNum : Int -> Int -> Int -> Int
findLuckyTicketsNum ticketLength base limit =
  foldl (findAmountPerLimit) 2 [1..((limit * (base - 1)) - 1)]
  where
    findAmountPerLimit : Int -> Int -> Int
    findAmountPerLimit curRes iLimit =
      let
        posTickets = findPosTickets iLimit limit base (div iLimit base)
        res = curRes + (posTickets * posTickets)
      in res

main : IO ()
main = do
  inputData <- getLine
  let
    divInputData = words inputData
    ticketLength = cast {to=Int} (fromMaybe "" (head' divInputData))
    base = cast {to=Int} (fromMaybe "" (last' divInputData))
    solution = findLuckyTicketsNum ticketLength base (div ticketLength 2)
  printLn solution

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  7405708168968812193
-}
