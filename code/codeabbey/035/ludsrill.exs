# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.07 seconds (0.02s to load, 0.05s running 55 checks on 1 file)
# 5 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule SavingsCalculator do

  def main do
    data = get_data()
    Enum.map(Enum.map(data, &savings_calculator(&1, 0)), &IO.write("#{&1} "))
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def get_data do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    [_ | tail] = List.delete_at(all_data, 1 +
                  String.to_integer(Enum.at(all_data, 0)))
    Enum.map(Enum.map(tail, &String.split(&1, " ")), &to_int/1)
  end

  def savings_calculator([a, b, c], count) do
    if a < b do
      savings_calculator([Float.floor(a * (1 + c / 100), 2), b, c], count + 1)
    else
      count
    end
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e SavingsCalculator.main
# 12 13 61 273 9 15 268 324 302 92 10 26 137 33 163 134 152 324 110
