;; $ clj-kondo --lint bridamo98.cljs
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-035
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn get-years [s r p]
  (loop [years 0 current s interest (/ p 100)]
    (if (>= current r) years
      (recur (+ years 1) (+ current (* current interest)) interest))))

(defn solve-all[size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (let [ args (str/split (core/read-line) #" ")]
          (recur (+ i 1)
          (str result (get-years (edn/read-string (args 0))
          (edn/read-string (args 1)) (edn/read-string (args 2))) " ")))
          result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 10 152 68 55 21 5 24 92 12 302 163 10 324 92 33 6 152 149 102
