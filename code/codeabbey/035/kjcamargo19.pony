// ponyc -b kjcamargo19

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun iterator(data: Array[String]): Array[String] =>
          data.slice(1, data.size())

        fun get_percentage(bank_rate: F32): F32 =>
          bank_rate/100

        fun get_years(money':F32, goal:F32, years':U16, percentage:F32): U16 =>
          var money: F32 = money'
          var years: U16 = years'
          if money>goal then
            return years
          else
            money = (((money + (money * percentage))*100).floor())/100
            get_years(money,goal,years+1,percentage)
          end

        fun obtain_data(data: Array[String])? =>
          if data.size() > 0 then
            var line: Array[String] = data(0)?.split_by(" ")
            var money: F32 = line(0)?.f32()?
            var goal: F32 = line(1)?.f32()?
            var bank_rate: F32 = line(2)?.f32()?

            var percentage: F32 = get_percentage(bank_rate)
            var number_years: U16 = get_years(money,goal,0,percentage)
            env.out.write(number_years.string() + " ")
            obtain_data( iterator (data))?
          end

        fun ref apply(data: Array[U8] iso) =>
          var input_data: String = String.from_iso_array(consume data)
          var line_sep: Array[String] = input_data.split_by("\n")
          var real_data: Array[String] = line_sep.slice(1, line_sep.size())

          try
            obtain_data(real_data)?
          end

        fun ref dispose() =>
          env.out.write("")
      end,
      512
    )
// cat DATA.lst | ./kjcamargo19
// 102 19 303 297 92 303 40 102 42 10 99 302 163 137 273 109 279 92 6 102
