;; clj-kondo --lint ludsrill.cljs
;; linting took 37ms, errors: 0, warnings: 0

(ns ludsrill.064
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn create-hashmap [board]
  (map (fn [x y] (when (= x "1") (hash-map y [0 -1]))) board (range)))

(defn wave-propagation [hashmap vertex width]
  (if (seq vertex)
    (do
      (when (and (= (first vertex) 0) (= (first (get hashmap 0)) 0))
          (wave-propagation (assoc hashmap 0 [1,-1]) vertex width))

      (cond (and (= (first (get hashmap (+ (first vertex) 1))) 0)
              (not= (mod (first vertex) width) (- width 1)))
            (wave-propagation (assoc hashmap (+ (first vertex) 1)
              [1, (first vertex)]) (conj vertex (+ (first vertex) 1)) width)

            (= (first (get hashmap (+ (first vertex) width))) 0)
            (wave-propagation (assoc hashmap (+ (first vertex) width)
              [1, (first vertex)]) (conj vertex (+ (first vertex) width))
              width)

            (and (= (first (get hashmap (- (first vertex) 1))) 0)
              (not= (mod (first vertex) width) 0))
            (wave-propagation (assoc hashmap (- (first vertex) 1)
              [1, (first vertex)]) (conj vertex (- (first vertex) 1)) width)

            (= (first (get hashmap (- (first vertex) width))) 0)
            (wave-propagation (assoc hashmap (- (first vertex) width)
              [1, (first vertex)]) (conj vertex (- (first vertex) width))
              width)

            :else (wave-propagation hashmap (into [] (rest vertex)) width)))

    hashmap))

(defn get-path [hashmap A]
  (when (not= A (- 1))
    (if (not= A 0)
        (cons A (get-path hashmap (second (get hashmap A))))
        (cons A (get-path hashmap (- 1))))))

(defn convert-output [path width]
  (when (> (count path) 1)
    (cond (= (- (first path) (second path)) (- width))
            (cons "D" (convert-output (rest path) width))
          (= (- (first path) (second path)) (- 1))
            (cons "R" (convert-output (rest path) width))
          (= (- (first path) (second path)) width)
            (cons "U" (convert-output (rest path) width))
          (= (- (first path) (second path)) 1)
            (cons "L" (convert-output (rest path) width)))))

(defn short-form [path]
  (str/join #"" (map (fn [x] (core/format "%s%s" (count x) (first x)))
    (partition-by identity path))))

(defn -main []
  (let [data (get-data)
        measure (first data)
        board-measure (* (Integer. (first measure))
          (Integer. (second measure)))
        board (flatten (map #(str/split (first %) #"") (rest data)))
        hashmap (into (sorted-map) (apply merge (create-hashmap board)))
        flow (wave-propagation hashmap [0] (Integer. (first measure)))
        path-A (convert-output (get-path flow (- (Integer. (first measure)) 1))
                 (Integer. (first measure)))
        path-B (convert-output (get-path flow (- board-measure
                (Integer. (first measure)))) (Integer. (first measure)))
        path-C (convert-output (get-path flow (- board-measure 1))
                (Integer. (first measure)))]

    (print (core/format "%s " (short-form path-A)))
    (print (core/format "%s " (short-form path-B)))
    (print (core/format "%s " (short-form path-C)))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 2D8L2U2L6D6L2D2R2D2L2D6R2U4R4D6R2D8L2D2L2D4L2U4L14U2L4U16L
;; 2U2R10U2R2U6R6D2R8U2R4U2R2U16L 6U8L2D2L2D4L2U4L14U2L4U16L
