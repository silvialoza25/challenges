{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #062: Prime Ranges - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi str = fromMaybe 0 $ parseInteger {a=Int} str

getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

-- Variant "Guarded Filters"
-- https://wiki.haskell.org/Prime_numbers
primesToGT : Int -> List Int
primesToGT upperLimit = sieve [2..upperLimit]
  where
  sieve : List Int -> List Int
  -- "prime" variable is a candidate
  sieve (prime::xs) = if (prime * prime > upperLimit) then prime :: xs
    else prime :: sieve [num | num <- xs, mod num prime > 0]

-- Returns the number of primes in the range [low, high]
processTestCase : Int -> Int -> List Int -> Nat
processTestCase low high primes =
  length $ filter (>= low) $ filter (<= high) primes

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse input array
    testCases = map (map stoi) $ map words inputData
    -- Generate prime numbers list
    primes = primesToGT 3000000 -- Upper range
    -- Process the range needed
    results = map (\[low, high] => processTestCase low high primes) testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  34619 29318 12138 4123 74181 1240 14264 6540 4661 4918 1240 3239
  5105 42254 55995 10685 16672 21487 34077 17227 21560 11379 70058
  63540 69426 19552 8285 38513 79168
-}
