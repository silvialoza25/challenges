;; $ clj-kondo --lint nickkar.clj
;; linting took 68ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(defn read-input ([n] (read-input n 0 []))
  ([n i out]
   (cond
     (>= i n) out
     :else (read-input n (inc i) (conj out [(core/read) (core/read)]))
     ))
  )

(defn primelst ([numberlst](primelst 0 numberlst))
  ([i lst]
   (cond
     (>= (nth lst i) 1733) (vec lst)
     :else
     (let [actual (nth lst i)
           newlist (filterv #(or (not= (mod % actual) 0) (= % actual)) lst)]
       (primelst (inc i) newlist)
       )
     ))
  )

(defn filter-lst [primelst min max]
  (filter #(and (<= % max) (>= % min)) primelst)
  )

(defn number-of-primes ([n input primes] (number-of-primes n input primes 0 []))
  ([n input primes i out]
   (cond
     (>= i n) out
     :else
     (let [min (nth (nth input i) 0)
           max (nth (nth input i) 1)]
       (number-of-primes
        n input primes (inc i) (conj out (count (filter-lst primes min max)))))
     )
   ))

(defn main []
  (let [n (core/read)
        input (read-input n)
        numberlst (range 2 3000000)
        primenumlst (primelst numberlst)]
    (apply println (number-of-primes n input primenumlst))
    ))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; 7883 38942 25486 2422 16123 1507 25876 3779 76947 53668 43983 46127 19169
;; 27433 14427 1423 11513 20889 32276 1233 396
