# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.03s to load, 0.1s running 55 checks on 1 file)
# 8 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule PrimeRanges do

  def main do
    data = get_data()
    get_primes = generate_primes(2, [2])
    solution = (Enum.map(data, &get_range(get_primes, &1)))
    Enum.map(solution, &IO.write("#{length(&1)} "))
  end

  def get_data do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    [_ | tail] = List.delete_at(all_data, 1 +
                  String.to_integer(Enum.at(all_data, 0)))

    Enum.map(tail, &String.split(&1, " "))
      |> Enum.map(&to_int/1)
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def get_range(primes, [a, b]) do
    Enum.slice(primes, Enum.find_index(primes, fn x -> x == a end)..
      Enum.find_index(primes, fn x -> x == b end))
  end

  def generate_primes(number, acc) do
    if number <= 3_000_000 do
      if check_prime(number, acc) do
        generate_primes(number + 1, acc)
      else
        generate_primes(number + 1, acc ++ [number])
      end
    else
      acc
    end
  end

  def check_prime(number, [head | tail]) do
    if rem(number, head) == 0 do
      true
    else
      check_prime(number, tail)
    end
  end

  def check_prime(_, []) do
    false
  end

end

# cat DATA.lst | elixir -r ludsrill.exs -e PrimeRanges.main
# 7883 38942 25486 2422 16123 1507 25876 3779 76947 53668 43983 46127 19169
# 27433 14427 1423 11513 20889 32276 1233 396
