#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.79 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

class Primes
  S  = {1, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 49, 53, 59}
  S1 = {1, 13, 17, 29, 37, 41, 49, 53}
  S2 = {7, 19, 31, 43}
  S3 = {11, 23, 47, 59}

  @@primes = {2_u64 => true, 3_u64 => true, 5_u64 => true}
  @@prime_list : Array(UInt64)
  @@last : UInt64

  @@prime_list = @@primes.keys.sort
  @@last = @@prime_list[-1]
  @@limit = 5_u64

  def self.loop_s1()
    (1..@@limit).each do |x|
      xx = 4 * x ** 2
      break if xx > @@limit

      (1..@@limit).step(2) do |y|
        n = (xx + y ** 2).to_u64
        next if n <= @@last
        break if n > @@limit

        if S1.includes?(n % 60) && @@primes.has_key?(n)
          @@primes[n] = !@@primes[n]
        end
      end
    end
  end

  def self.loop_s2()
    (1..@@limit).step(2).each do |x|
      xx = 3 * x ** 2
      break if xx > @@limit

      (2..@@limit).step(2) do |y|
        n = (xx + y ** 2).to_u64
        next if n <= @@last
        break if n > @@limit

        if S2.includes?(n % 60) && @@primes.has_key?(n)
          @@primes[n] = !@@primes[n]
        end
      end
    end
  end

  def self.loop_s3()
    (2..@@limit).each do |x|
      xx = 3 * x ** 2
      break if xx - (x - 1) ** 2 > @@limit
      (1..x - 1).step(2) do |y|
        n = (xx - (x - y) ** 2).to_u64
        next if n <= @@last

        if S3.includes?(n % 60) && @@primes.has_key?(n)
          @@primes[n] = !@@primes[n]
        end
      end
    end
  end

  def self.growup

    ((@@last // 60)..(@@limit // 60)).each do |n|
      S.each do |x|
        num = (60 * n + x).to_u64
        next if num <= @@last
        break if num > @@limit

        @@primes[num] = false
      end
    end

    self.loop_s1
    self.loop_s2
    self.loop_s3

    (0..(@@limit // 60)).each do |n|
      S.each do |x|
        num = (60 * n + x).to_u64
        next if num < 7

        num_2 = num * num
        break if num_2 > @@limit

        if @@primes.has_key?(num)
          if @@primes[num]
            (0..(@@limit // 60)).each do |nn|
              S.each do |xx|
              c = (num_2 * (60 * nn + xx)).to_u64
              break if c > @@limit

              @@primes[c] = false if @@primes.has_key?(c)
              end
            end
          end
        end
      end
    end

    @@primes.delete_if { |_key, value| !value }
    @@prime_list = @@primes.keys.sort
    @@last = @@prime_list[-1]
  end

  def self.index(prime)
    if prime > @@limit
      @@limit = prime.to_u64
      self.growup
    end

    @@prime_list.index(prime.to_u64)
  end

  def self.[](index : Int)
    @@prime_list[index]
  end

  def self.[](range : Range)
    @@prime_list[range]
  end

  def self.to_s
    @@prime_list
  end
end

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map { |x| x.to_u64 }

(0...args.size).step(2) do |x|
  a = Primes.index(args[x]) || 0
  b = Primes.index(args[x + 1]) || 0

  print "#{(a - b).abs + 1} "
end

puts

# $ ./richardalmanza.cr
# 83240 8887 14177 7452 51121 4484 11804 31344 2119 60923 17235 27382 11202
# 38837 28516 43885 25869 5028 49326 9581 32738 3878 16131 535 54619 3369 5562
