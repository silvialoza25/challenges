;; $ clj-kondo --lint bridamo98.cljs
;; linting took 73ms, errors: 0, warnings: 0

(ns bridamo98-062
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn is-prime [n]
  (if (or (< n 1) (= n 1)) 0
    (if (or (< n 3) (= n 3)) 1
      (if (or (= (mod n 2) 0) (= (mod n 3) 0)) 0
        (loop [i 5]
          (if (or (< (* i i) n) (= (* i i) n))
            (if (or (= (mod n i) 0) (= (mod n (+ i 2)) 0)) 0
              (recur (+ i 6))) 1 ))))))

(defn get-primes-amount[d-limit u-limit]
  (loop [current d-limit result 0]
    (if (> current u-limit) result
      (recur (+ current 1) (+ result (is-prime current))))))

(defn solve-all [size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (let [prime-range (str/split (core/read-line) #" ")]
        (recur (+ i 1)
        (str result (get-primes-amount (edn/read-string (get prime-range 0))
        (edn/read-string (get prime-range 1))) " ")))
      result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 14666 35668 12210 8331 4987 35092 10371 46198 28460
;; 6092 19100 31738 6359 13173 9406 82052 13504 11894
;; 2885 12246 14584 4702 21450 1635 62783 29676 49501
