// ponyc -b kjcamargo19

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun iterator(data: Array[String]): Array[String] =>
          data.slice(1, data.size())

        fun prime(number: U32, const_i:U32): U8 =>
          var limit: U32 = number.f32().sqrt().u32()
          if const_i <= limit then
            if (number % const_i) == 0 then
              return 0
            end
            prime(number, const_i+1)
          else
            return 1
          end

        fun ranges(from: U32, to: U32, const_i:U32, numberPrimes':U32): U32 =>
          var numberPrimes: U32 = numberPrimes'
          if from > to then
            return numberPrimes
          else
            if (prime(from, const_i)) == 1 then
              numberPrimes = numberPrimes + 1
            end
            ranges(from+1, to, const_i, numberPrimes)
          end

        fun obtain_data(data: Array[String])? =>
          if data.size() > 0 then
            var line: Array[String] = data(0)?.split_by(" ")
            var from: U32 = line(0)?.u32()?
            var to: U32 = line(1)?.u32()?

            var result:U32 = ranges(from, to, 2, 0)
            env.out.write(result.string() + " ")
            obtain_data( iterator (data))?
          end

        fun ref apply(data: Array[U8] iso) =>
          var input_data: String = String.from_iso_array(consume data)
          var line_sep: Array[String] = input_data.split_by("\n")
          var real_data: Array[String] = line_sep.slice(1, line_sep.size())

          try
            obtain_data(real_data)?
          end

        fun ref dispose() =>
          None

      end,
      512
    )
// cat DATA.lst | ./kjcamargo19
// 13405 15522 16227 6284 43680 12172 15456 16227 18995 61585 78098
// 31118 33541 22353 15316 3474 30097 51193 6093 23016 15523
