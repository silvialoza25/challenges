/*
  $ rustfmt vmelendez.rs
  $ rustc vmelendez.rs
  $
*/

fn is_prime(n: i32) -> bool {
  if n <= 1 {
    return false;
  }

  if n <= 3 {
    return true;
  }

  if n % 2 == 0 || n % 3 == 0 {
    return false;
  }

  let mut i = 5;
  while i * i <= n {
    if n % i == 0 || n % (i + 2) == 0 {
      return false;
    }
    i = i + 6;
  }

  return true;
}

fn check() -> i32 {
  let input: Vec<i32> = {
    let mut a = String::new();
    std::io::stdin().read_line(&mut a).unwrap();
    a.split_whitespace()
      .map(|x| x.trim().parse::<i32>().unwrap())
      .collect()
  };

  let mut prims = 0;
  for i in input[0]..input[1] + 1 {
    if is_prime(i) == true {
      prims += 1;
    }
  }

  return prims;
}

fn main() -> std::io::Result<()> {
  let n: Vec<i32> = {
    let mut a = String::new();
    std::io::stdin().read_line(&mut a).unwrap();
    a.split_whitespace()
      .map(|x| x.trim().parse::<i32>().unwrap())
      .collect()
  };

  let mut vec = Vec::new();
  for _i in 0..n[0] {
    vec.push(check());
  }

  let mut out_string = String::new();
  let vec_iter = vec.iter();

  for i in vec_iter {
    out_string += &i.to_string();
    out_string += &" ".to_owned();
  }

  println!("{}", out_string);
  Ok(())
}

/*
  $ cat .\DATA.lst | .\vmelendez.exe
  14666 35668 12210 8331 4987 35092 10371 46198 28460 6092 19100 31738 6359
  13173 9406 82052 13504 11894 2885 12246 14584 4702 21450 1635 62783 29676
  49501
*/
