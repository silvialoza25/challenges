{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #152: RSA Cryptography - Idris2
-- Based on "https://www.cryptool.org/en/cto/highlights/rsa-step-by-step"

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoI : String -> Integer
stoI str = fromMaybe 0 $ parseInteger {a=Integer} str

integerAt : Integer -> List Integer -> Integer
integerAt 0 [] = 0
integerAt _ [] = 0
integerAt 0 (x::xs) = x
integerAt index (_::xs) = integerAt (index - 1) xs

-- Extended Euclidean Algorithm
-- Results in tuples (r, a, b) where r is the GCD
euclidean :
  Integer -> Integer -> Integer -> Integer -> Integer -> Integer
  -> (Integer, Integer, Integer)
euclidean xCur yCur sPrev sCur tPrev tCur =
  if (mod xCur yCur) == 0 then (yCur, sCur, tCur)
  else let
    quotient = div xCur yCur
    sNext = sPrev - quotient * sCur
    tNext = tPrev - quotient * tCur
  in
    euclidean yCur (mod xCur yCur) sCur sNext tCur tNext

-- Modular Inverse
modInv : Integer -> Integer -> Integer
modInv numA modulus =
  let
    mAbs = if modulus < 0 then modulus * -1 else modulus
    aAbs = if numA < 0 then mAbs - (mod (numA * -1) mAbs) else numA
    (greatestCD, aux, _) = euclidean aAbs mAbs 1 0 0 1
  in
    if greatestCD == 1 then mod aux mAbs else greatestCD

-- Check Least Significant Bit
checkLsb : Integer -> Integer -> Integer -> Integer -> Integer
checkLsb base exp modulus result =
  if mod exp 2 == 1
  -- Use modular multiplication properties for this power of 2
  then mod (result * base) modulus
  else result

-- Modular Exponentiation
modExp : Integer -> Integer -> Integer -> Integer -> Integer
modExp base exp modulus result =
  let
    -- Calculate mod for the next power of 2
    aux = checkLsb base exp modulus result
  in if exp <= 0 then result
    else modExp (mod (base * base) modulus) (div exp 2) modulus aux

rsaDecrypt : Integer -> Integer -> Integer -> Integer -> Integer
rsaDecrypt primeP primeQ exponent cipher =
  let
    modulusN = primeP * primeQ
    phi_n = (primeP - 1) * (primeQ - 1)
    auxD = modInv exponent phi_n -- Part of the secrete key
    numD = if auxD < 0 then auxD + phi_n else auxD
    baseExpOne = mod cipher modulusN -- This is exp 1
  in
    modExp baseExpOne numD modulusN 1 --Decrypt

-- Integer to ASCII representation
formatDecoded : Integer -> String
formatDecoded 0 = ""
formatDecoded decoded = let charCode = cast (mod decoded 100) in
  formatDecoded (div decoded 100) ++ strCons (chr charCode) ""

main : IO()
main = do
  -- Read Stdin
  primePStr <- getLine
  primeQStr <- getLine
  cipherStr <- getLine
  let
    -- Parse data
    primeP = stoI primePStr -- p
    primeQ = stoI primeQStr -- q
    cipher = stoI cipherStr -- encoded
    -- Process numeric data
    decoded = rsaDecrypt primeP primeQ 65537 cipher
    -- Remove end
    decodedStr = reverse $ snd $ break (== '0') $ reverse $ show decoded
    -- Convert to ASCII
    message = formatDecoded $ stoI decodedStr
  -- Print result
  putStrLn message

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  STOCK FRANC PUPPY BLOOD ARRAY TOWEL MONEY
-}
