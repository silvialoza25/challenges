-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import IO (getContents)
import Integer (even)
import List (split)
import Read (readInt)

getData :: IO String
getData = do
  content <- getContents
  return content

euclidean :: Int -> Int -> Int -> Int -> Int -> Int -> (Int, Int)
euclidean constX constY Sprev Scur Tprev Tcur
  | constY == 0 = (constX, Sprev)
  | otherwise = euclidean constY constR nSprev nScur nTprev nTcur
  where
    constR = mod constX constY
    constQ = div constX constY
    Snext = Sprev - (constQ * Scur)
    Tnext = Tprev - (constQ * Tcur)
    nSprev = Scur
    nScur = Snext
    nTprev = Tcur
    nTcur = Tnext

modularInv :: Int -> Int -> Int
modularInv numberA modulo =
  let
    moduloAbs = abs modulo
    (greatest, constA) = euclidean numberA moduloAbs 1 0 0 1
  in
    if greatest == 1 then (mod constA moduloAbs)
    else greatest

getModExpo :: Int -> Int -> Int -> Int
getModExpo base expo nMod
  | expo == 0 = 1
  | even expo =
    let
      termA = getModExpo base (div expo 2) nMod
      prevAns = rem (termA * termA) nMod
    in
      rem (prevAns + nMod) nMod
  | otherwise =
    let
      termA = rem base nMod
      prevAns = termA * (getModExpo base (expo - 1) nMod)
    in
      rem (prevAns + nMod) nMod

decrypt :: Int -> Int -> Int -> Int -> Int
decrypt constP constQ constE cipher = result
  where
    constN = constP * constQ
    phiN = (constP - 1) * (constQ - 1)
    constD = modularInv constE phiN
    base = mod cipher constN
    result = getModExpo base constD constN

toAscii :: Int -> String -> String
toAscii number accum
  | number == 0 = reverse accum
  | otherwise = toAscii newNumber letters
  where
    (newNumber, character) = divMod number 100
    letters = if character == 0 then "" else accum ++ [chr character]

getOriginalText :: [Int] -> String
getOriginalText dataIn = decodedText
  where
    constP = dataIn !! 0
    constQ = dataIn !! 1
    constE = 65537
    cipher = dataIn !! 2
    numEnconded = reverse (show (decrypt constP constQ constE cipher))
    withoutRandom = readInt (reverse $ snd (break (== '0') numEnconded))
    decodedText = toAscii withoutRandom ""

main :: IO ()
main = do
  input <- getData
  let
    useData = take 3 (split (=='\n') input)
    usefulData = map (\x -> readInt x) useData
    decryptedText = getOriginalText usefulData
  putStrLn decryptedText

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- MONEY STORM STOCK PUPPY GLOVE TOWEL FRANC
