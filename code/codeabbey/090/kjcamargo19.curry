-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import Char
import Float
import Integer
import IO
import List

getData :: IO String
getData = do
  x <- getContents
  return x

strToInt :: String -> Int
strToInt s = read s :: Int

strToF :: String -> Float
strToF s = read s :: Float

floor :: Float -> Int
floor number = intPart
  where
    sep = split (=='.') (show number)
    intPart = strToInt (sep !! 0)

getLetter :: Int -> String -> Char
getLetter number letters = newLetter
  where
    value = i2f number
    size = length letters
    indexF = value /. (i2f (factorial (size-1)))
    indexI = floor indexF
    newLetter = letters !! indexI

getNumber :: Int -> Int -> Int
getNumber number size = result
  where
    value = i2f number
    indexF = value /. (i2f (factorial (size-1)))
    indexI = floor indexF
    newNumber = value - ((i2f indexI) *. (i2f (factorial (size-1))))
    result = truncate newNumber

lexPermutation :: Int -> String -> String -> String
lexPermutation number lettersIn lettersOut
  | length lettersIn == 1 = newString
  | otherwise = lexPermutation newNumber validate newString
  where
    dataIn = number
    letras = lettersIn
    size = length lettersIn
    newNumber = getNumber dataIn size
    newLetter = getLetter dataIn letras
    newString = lettersOut ++ [newLetter]
    validate = letras \\ newString

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = map strToInt $ tail (init dataLines)
    dataR = map (\x -> lexPermutation x "ABCDEFGHIJKL" "") usefulData
    finalResult = unwords dataR
  putStrLn (show finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- FKIDCLGJAHEB LGDIFKEBJCHA DCJLBFEHIKGA LGCHJFDIAEKB JAEKBDHICFGL
-- LBCHFADJGIEK AEFIDJKBGCHL KIDBHJELGFCA CHGDKEAFJILB JGKHILCDABEF
-- BLJEAHKGFCDI CFBEDLHAIJGK FIHJDBKLACEG KJFCAIEGDLBH DFJLEHICKBAG
-- EFKLAGJICBHD FJEIKCHBDAGL
