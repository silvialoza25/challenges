# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.01s to load, 0.08s running 55 checks on 1 file)
# 7 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule LexicographicPermutations do
  def main do
    all_data = IO.read(:stdio, :all)
    constant = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"]
    try do
      get_data(all_data)
      |> Enum.map(&found(constant, hd(&1), []))
      |> Enum.map(&Enum.join/1)
      |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
    |> Enum.reject(fn item -> item == "" end)
    |> Enum.map(&String.split(&1, " "))
    |> Enum.map(&to_int/1)
    |> tl()
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def factorial(number, accumulator) do
    if number == 0 do
      accumulator
    else
      factorial((number - 1), (number * accumulator))
    end
  end

  def get_remainder(remainder, position, fact_measure) do
    if position * fact_measure <= remainder do
      get_remainder(remainder, position + 1, fact_measure)
    else
      position - 1
    end
  end

  def found(list, remainder, acc) do
    fact_measure = factorial(length(list) - 1, 1)
    position = get_remainder(remainder, 1, fact_measure)
    if length(list) > 1 do
      aux_list = Enum.reject(list, fn item ->
        item == Enum.at(list, position) end)
      found(aux_list, remainder - position * fact_measure,
        acc ++ [Enum.at(list, position)])
    else
      acc ++ list
    end
  end
end

LexicographicPermutations.main()

# cat DATA.lst | elixir ludsrill.exs
# KLFHCBDIEAJG FIHJDBKLACEG KLFCEHBIJGAD EBDLKJCAIHGF AJIFDHCBGELK AIGJKHFLCDBE
# JIFDHLCAKBGE LGCHJFDIAEKB CAHIKBJDGELF AHEDLIKJFBGC BLJEAHKGFCDI LBCHFADJGIEK
# CFDALGJKHEBI DFHALJGCBIEK EHAKFBDCIJGL FLGBEJAIDCHK GKAIBLHJCEFD FJEKCHIADLGB
# KGLHDIBECJFA AHCKLGDFJBEI KIDBHJELGFCA
