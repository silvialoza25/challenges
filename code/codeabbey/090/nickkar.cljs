;; $ clj-kondo --lint nickkar.cljs
;; linting took 65ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

;; read input and returns a vector with the information
(defn read-cases ([ncases] (read-cases ncases 0 []))
  ([ncases i out]
    (cond
      (>= i ncases) out
      :else
      (read-cases ncases (inc i) (conj out (core/read))))))

;; factorial function
(defn fact [num]
  (if (<= num 1N) 1N (* num  (fact (- num 1N)))))

;; function finds the nth permutation order given an ordered set of elements
(defn ith-perm ([alph ith] (ith-perm alph ith (dec (count alph)) ""))
  ([alph ith perm out]
    (cond
      (= (count alph) 1) (apply str out alph)
      :else
      (let [index (mod (quot ith (fact perm)) (inc perm))
           letter (nth alph index)]
        (ith-perm (remove #(= letter %) alph) ith (dec perm)
                 (str out letter))))))

;; iterates over all test-cases
(defn solve-cases ([ncases cases alph] (solve-cases ncases cases alph 0 []))
  ([ncases cases alph index out]
    (cond
     (>= index ncases) out
     :else
     (solve-cases ncases cases alph (inc index)
                  (conj out (ith-perm alph (nth cases index)))))))

;; Driver code
(defn main []
  (let [ncases (core/read)
        cases (read-cases ncases)
        alph "ABCDEFGHIJKL"]
    (apply println (solve-cases ncases cases alph))))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; KLFCEHBIJGAD LGDIFKEBJCHA JIFDHLCAKBGE DFABGHLJKIEC AFDBGJELKHCI EHAKFBDCIJGL
;; FALIEJDBCGHK KCEGJLHDAIBF FKIDCLGJAHEB EKBCLIJAHDFG JGKHILCDABEF ICKJGBHDEALF
;; GJCKABEHDFIL JAEKBDHICFGL FJCLGKIAEBDH GFBDCAKLEIHJ GAECHDKBFLIJ CAHIKBJDGELF
;; AEFIDJKBGCHL CDHEBLIKAJFG LDJEKHAFGBIC DFJLEHICKBAG AJIFDHCBGELK
