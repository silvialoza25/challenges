;$ clj-kondo --lint frank1030.cljs
;linting took 441ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core]))

(defn abs [n]
  (max n (- n)))

(defn exp [x n]
     (if (zero? n) 1
         (* x (exp x (dec n)))))

(defn extended-gcd [a b]
  (cond (zero? a) [(abs b) 0 1]
    (zero? b) [(abs a) 1 0]
    :else (loop [s 0
      s0 1
      t 1
      t0 0
      r (abs b)
      r0 (abs a)]
      (if (zero? r)
        [r0 s0 t0]
        (let [q (quot r0 r)]
          (recur (- s0 (* q s)) s
            (- t0 (* q t)) t
            (- r0 (* q r)) r))))))

(defn mul_inv [a b]
  (let [b (if (neg? b) (- b) b)
    a (if (neg? a) (- b (mod (- a) b)) a)
    egcd (extended-gcd a b)]
    (if (= (first egcd) 1)
      (mod (second egcd) b)
      (str (first egcd)))))

(defn find-multiplier [s0 s1 s2 m]
  (let [a (mod (* (- s2 s1) (mul_inv (- s1 s0) m)) m)]
    a))

(defn find-increment [s0 s1 m a]
  (let [c (mod (- s1 (* s0 a)) m)]
    c))

(defn next-s [s2 a c m]
  (let [s31 (* s2 a) s32 (+ s31 c) s3 (mod s32 m)]
    s3))

(defn get-dat [index]
  (let [x (atom index) s0 (core/read) s1 (core/read) s2 (core/read)
    m 18446744073709551616
    a (find-multiplier s0 s1 s2 m) c (find-increment s0 s1 m a)
    s3 (next-s s2 a c m)]
    (if (> @x 0)
      (do (swap! x dec)
        (print s3 "")
        (if (not= @x 0)
          (get-dat @x)
          (print "")))
      (println ""))))

(defn main []
  (let [index (core/read)]
    (get-dat index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;1381617998375784141 12034695945651883927
;2725646950818235668 7622574854638424919
;7471133727510392510 12221227998837807212
;17386306587179252830 2307395407076907544
;15039809821803963138 11723706414870843799
;6882529733364128408 1657281374532672443
;8418627365732254018 15211837669688974521
;16318644023773046573 18080940097570119063
;10667075159310577755 7739119240870607265
;13082316693797825692 2981734670287605631
;7626555103848654878 11188959359255478562
;14826756929931762325 12869861468759800800
