#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

$ clisp -c frank1030.lsp
Compiling file /Users/Frank/Desktop/frank/216/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/216/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-dat()
  (let ((dat))
    (setq dat(read))
  )
)

(defun egcd (a b)
  (do ((r (cons b a) (cons (- (cdr r) (* (car r) q)) (car r)))
    (s (cons 0 1) (cons (- (cdr s) (* (car s) q)) (car s)))
    (u (cons 1 0) (cons (- (cdr u) (* (car u) q)) (car u)))
    (q nil))
    ((zerop (car r)) (values (cdr r) (cdr s) (cdr u)))
    (setq q (floor (/ (cdr r) (car r))))
  )
)

(defun invmod (a m)
  (multiple-value-bind (r s) (egcd a m)
  (unless (= 1 r) (error a m))s)
)

(defun find-multiplier (s0 s1 s2 m)
  (let ((a 0))
    (setq a (mod (* (- s2 s1) (invmod (- s1 s0) m)) m))
    (return-from find-multiplier a)
  )
)

(defun find-increment (s0 s1 m a)
  (let ((c 0))
    (setq c (mod (- s1 (* s0 a)) m))
    (return-from find-increment c)
  )
)

(defun next-s (s2 a c m)
  (let ((s3 0))
    (setq s3 (* s2 a))
    (setq s3 (+ s3 c))
    (setq s3 (mod s3 m))
    (return-from next-s s3)
  )
)

(defun main ()
  (let ((s0 0) (s1 0) (s2 0) (index 0) (m 0) (a 0) (c 0) (s3 0))
    (setq index (get-dat))
    (setq m (expt 2 64))
    (loop for i from 1 to index
      do (setq s0 (get-dat))
      do (setq s1 (get-dat))
      do (setq s2 (get-dat))
      do (setq a (find-multiplier s0 s1 s2 m))
      do (setq c (find-increment s0 s1 m a))
      do (setq s3 (next-s s2 a c m))
      do (print s3)
    )
  )
)

(main)

#|
$ cat DATA.lst | clisp frank1030.lsp
17168768104129676748
12983789159380637769
7989318077291446203
13195145911395951001
2553393721488606865
13958165887772911120
16458256830172631543
1446743428283962637
14001069139930511375
14275449315735402846
3790233238010333260
17669067267533365044
17529357248758541549
3817327874329108358
7447005828708649079
17848711159704464844
7828534594901702269
3995457604873658977
7093937703941312317
6725587951482251967
1278048703995008675
4943466300170580738
999334567006702906
14783028198123730772
918355663406936262
9870633452175593388
5582148297617089600
8443533942284946455
12745539990463334053
11654804222213958204
10781185282394367064
|#
