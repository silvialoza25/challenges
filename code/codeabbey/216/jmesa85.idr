{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #216: Cracking Linear Congruential Generator - Idris2
-- Based on "https://tailcall.net/blog/cracking-randomness-lcgs/"

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoI : String -> Integer
stoI str = fromMaybe 0 $ parseInteger {a=Integer} str

integerAt : Integer -> List Integer -> Integer
integerAt 0 [] = 0
integerAt _ [] = 0
integerAt 0 (x::xs) = x
integerAt index (_::xs) = integerAt (index - 1) xs

-- Gets lines in the Stdin
getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

-- Extended Euclidean Algorithm
-- Results in tuples (r, a, b) where r is the GCD
euclidean :
  Integer -> Integer -> Integer -> Integer -> Integer -> Integer
  -> (Integer, Integer, Integer)
euclidean xCur yCur sPrev sCur tPrev tCur =
  if (mod xCur yCur) == 0 then (yCur, sCur, tCur)
  else let
    quotient = div xCur yCur
    sNext = sPrev - quotient * sCur
    tNext = tPrev - quotient * tCur
  in
  euclidean yCur (mod xCur yCur) sCur sNext tCur tNext

modInv : Integer -> Integer -> Integer
modInv numA modulus =
  let
    mAbs = if modulus < 0 then modulus * -1 else modulus
    aAbs = if numA < 0 then mAbs - (mod (numA * -1) mAbs) else numA
    (greatestCD, aux, _) = euclidean aAbs mAbs 1 0 0 1
  in
  if greatestCD == 1 then mod aux mAbs else greatestCD

-- stateN = (a * s0 + c) % modulus, for n number of times
getLcg : Integer -> Integer -> Integer -> Integer -> Integer -> Integer
getLcg _ _ _ state0 0 = state0
getLcg multiplier incr modulus state0 numN =
  let
    stateN = mod (multiplier * state0 + incr) modulus
    result = getLcg multiplier incr modulus stateN (numN - 1)
  in
  if result < 0 then result + modulus else result

-- increment  = (s1 - s0 * multiplier) % modulus
getLcgIncrement : Integer -> Integer -> Integer -> Integer -> Integer
getLcgIncrement state0 state1 modulus multiplier =
  let incr = mod (state1 - state0 * multiplier) modulus in
  if incr < 0 then incr + modulus else incr

-- multiplier = ((s2 - s1) / (s1 - s0)(modInv m)) (mod m)
getLcgMultiplier : Integer -> Integer -> Integer -> Integer -> Integer
getLcgMultiplier state0 state1 state2 modulus =
  mod ((state2 - state1) * (modInv (state1 - state0) modulus)) modulus

-- Returns next value in the sequence
processTestCase : List Integer -> Integer
processTestCase params =
  let
    MODULUS = 18446744073709551616 -- Constant 2**64
    state0 = integerAt 0 params
    state1 = integerAt 1 params
    state2 = integerAt 2 params
    multiplier = getLcgMultiplier state0 state1 state2 MODULUS
    incr = getLcgIncrement state0 state1 MODULUS multiplier
  in
  getLcg multiplier incr MODULUS state2 1

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse test cases
    testCases = map (map stoI) $ map words inputData
    -- Process data
    results = map processTestCase testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  95644856469764438 8431281435852247129 17290655064951850340
  8805154374933764017 14034991246557301424 9597438208484731439
  14621108127443882704 14234481385000543710 12646909935395658291
  3330620776641177760 4426950463317427492 3210014052412140689
  1436144732780770784 10901582519329028166 3114423591349005000
  17811481015920882557 10469481510017804256 9789848710586730479
  17685564424862047999 7480363013604022740 16488388587329072702
  10389624000002796276 9345290600062839890 10208269593114944452
  9795431909531599595
-}
