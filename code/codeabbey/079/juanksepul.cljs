; clj-kondo --lint juanksepul.cljs
; linting took 672ms, errors: 0, warnings: 0

(ns codeabbey.juanksepul
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.set :as set]))

(defn read-data []
  (str/split-lines (core/slurp core/*in*)))

(defn nodes [city-map]
  (re-seq #"[A-Z]" (str (seq city-map))))

(defn leaf-nodes [city-map]
  (filter #(= (get (core/frequencies (nodes city-map)) %) 1)
          (set (nodes city-map))))

(defn filt-leaves [city-map]
  (filter #(= (set/intersection
               (set (leaf-nodes city-map))
               (set (str/split % #"-"))) #{}) city-map))

(defn is-cyclic [city-map]
  (loop [new-city-map (filt-leaves city-map)]
    (cond
      (= (count new-city-map) 0) (print "0 ")
      (= (count (leaf-nodes new-city-map)) 0) (print "1 ")
      :else (recur (filt-leaves new-city-map)))))

(defn !main []
  (doseq [raw-city-map (rest (read-data))]
    (is-cyclic (rest (str/split raw-city-map #" ")))))

(!main)

; cat DATA.lst | clj juanksepul.cljs
; 0 0 0 0 0 0 0 0 1 0 1 1 1 0 0 1 1 0 0 0 0 0 1 1 0 0 1
