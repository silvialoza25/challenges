#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defparameter *nested-data* ())
(defvar *data*)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect line)))

(defun organize-list(data)
  (loop for i in data
    collect (list i)))

(defun split-by-one-space (item)
  (loop for i = 0 then (1+ j)
    as j = (position #\Space item :start i)
    do(when (= i 0)
      (return-from split-by-one-space (subseq item i j)))
    while j))

(defun detect-cycles ()
  (let ((all-node)
        (cities)
        (cities-purgue)
        (count-roads))
    (dolist (rows-nested-data *nested-data*)
      (setq all-node ())
      (setq cities ())
      (setq cities-purgue ())
      (dolist (item rows-nested-data)
        (setq count-roads (parse-integer (split-by-one-space item)))
        (loop for i across item
          do(cond ((and (string/= (string i) " ") (string/= (string i) "-"))
                   (setq all-node (append all-node (list i))))))

        (loop for i in all-node
          do(cond ((equal (numberp (digit-char-p i)) NIL)
                   (setq cities (append cities (list i))))))

        (dolist (item cities)
          (cond ((string/= item (find item cities-purgue))
                 (setq cities-purgue (append cities-purgue (list item))))))

        (cond ((= (length cities-purgue) (+ count-roads 1))
               (format t "~s " '0))
        (T (format t "~s " '1)))))))

(setq *data* (cdr (read-data)))
(setq *nested-data* (organize-list *data*))
(detect-cycles)

#|
cat DATA.lst | clisp ludsrill.lsp
1 1 1 0 0 1 1 0 0 1 1 0 0 1 0 1 1 1 1 1 1 1
|#
