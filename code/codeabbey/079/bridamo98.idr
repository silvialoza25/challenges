{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

fromIntegerNat : Int -> Nat
fromIntegerNat 0 = Z
fromIntegerNat number =
  if (number > 0) then
    S (fromIntegerNat (assert_smaller number (number - 1)))
  else
    Z

toIntegerNat : Nat -> Int
toIntegerNat Z = 0
toIntegerNat (S k) = 1 + toIntegerNat k

elemAt : Int -> List a -> a
elemAt 0 (x::xs) = x
elemAt index (_::xs) = elemAt (index - 1) xs

replaceElement : Nat -> a -> List a -> List a
replaceElement index x xs =
  let
    left = take index xs
    right = drop (index + 1) xs
  in left ++ [x] ++ right

changeElem : Nat -> Nat -> a -> List (List a) -> List (List a)
changeElem row col x xs =
  let
    rowToReplaceIn = elemAt (toIntegerNat row) xs
    modifiedRow = replaceElement col x rowToReplaceIn
  in replaceElement row modifiedRow xs

injectValue : a -> IO a
injectValue dat = pure dat

getInputData : IO(List String)
getInputData = do
  line <- getLine
  if line == ""
    then injectValue []
    else do
      xs <- getInputData
      injectValue (line :: xs)

rmdups : Eq a => List a -> List a
rmdups [] = []
rmdups (x::xs) =
  let
    res =
      if x `elem` xs
        then rmdups xs
        else x :: rmdups xs
  in res

findIndex : List Char -> Char -> Nat -> Nat
findIndex [] item curIndex = curIndex
findIndex list item curIndex =
  let
    curItem = fromMaybe ' ' (head' list)
  in
  if curItem == item
    then curIndex
    else findIndex (drop 1 list) item (curIndex + 1)

initMatrix : Nat -> List (List Int)
initMatrix size = replicate size (replicate size 0)

formatAdjList : String -> List Char
formatAdjList dat =
  let
    divDat = unpack dat
    res = [fromMaybe ' ' (head' divDat)] ++ [fromMaybe ' ' (last' divDat)]
  in res

fullMatrix : List (List Char) -> List (List Int)
  -> List Char -> List (List Int)
fullMatrix [] matrix states = matrix
fullMatrix roads matrix states =
  let
    road = fromMaybe [] (head' roads)
    fstState = fromMaybe ' ' (head' road)
    sndState = fromMaybe ' ' (last' road)
    fstIndex = findIndex states fstState 0
    sndIndex = findIndex states sndState 0
    nxtMatrix = changeElem sndIndex fstIndex ((toIntegerNat fstIndex) + 1)
      (changeElem fstIndex sndIndex ((toIntegerNat sndIndex) + 1) matrix)
  in fullMatrix (drop 1 roads) nxtMatrix states

checkIfCycle : List (List Int) -> List Int -> Int -> Int -> (Int, List Int)
checkIfCycle adjacentMat visited parent curNode = recurForNei adjacentMat
  (replaceElement (fromIntegerNat curNode) 1 visited) parent curNode
    (elemAt curNode adjacentMat)
  where
    recurForNei : List (List Int) -> List Int -> Int -> Int
      -> List Int -> (Int, List Int)
    recurForNei adjacent visitedArr par node [] = (0, visitedArr)
    recurForNei adjacent visitedArr par node neighbors =
      let
        curNei = (fromMaybe 0 (head' neighbors)) - 1
        (res, nxtVisited) =
          if (elemAt curNei visitedArr) == 0
            then checkIfCycle adjacent visitedArr node curNei
            else if curNei /= par
              then (1, visitedArr)
              else (0, visitedArr)
      in if res == 0
        then recurForNei adjacent nxtVisited par node (drop 1 neighbors)
        else (1, nxtVisited)

checkAllIndexes : List (List Int) -> List Int -> Nat -> String
checkAllIndexes adjacentMat [] size = "0"
checkAllIndexes adjacentMat indexes size =
  let
    index = fromMaybe 0 (head' indexes)
    (posRes,_) = checkIfCycle adjacentMat (replicate size 0) (-1) index
    res =
      if posRes == 0
        then checkAllIndexes adjacentMat (drop 1 indexes) size
        else show posRes
  in res

solveSingleProblem : String -> String
solveSingleProblem problem =
  let
    divDat = words problem
    adjacentList = map formatAdjList (drop 1 divDat)
    charDat = drop 1 (unpack problem)
    uniqueList = sort (rmdups (filter (/= '-') (filter (/= ' ') charDat)))
    sizeGraph = length uniqueList
    adjacentMat = map (filter (/=0))
      (fullMatrix adjacentList (initMatrix sizeGraph) uniqueList)
    res = checkAllIndexes adjacentMat [0..((toIntegerNat sizeGraph) - 1)]
      sizeGraph
  in res

main : IO ()
main = do
  inputData <- getInputData
  let
    intSizeInput = cast {to=Int} (elemAt 0 inputData)
    problems = drop 1 inputData
    solution = map solveSingleProblem problems
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  1 0 1 1 0 1 1 0 1 0 0 0 1 1 0 1 1 0 1
-}
