/*
$ eslint slayfer1112.ts
*/
function smoothNum([valA, valB, valC]: number[]): number {
  const smooth: number = (valA + valB + valC) / (2 + 1);
  return smooth;
}

function smoothArr(
  arrS: number[],
  [start, end]: number[],
  res: number[]
): number[] {
  if (start < end) {
    const arr: number[] = [arrS[start], arrS[start + 1], arrS[start + 2]];
    const smooth: number = smoothNum(arr);
    return smoothArr(arrS, [start + 1, end], res.concat(smooth));
  }
  return res;
}

function solution(entry: string[]): number {
  const entryF: number[] = entry.map(parseFloat);
  const size: number = entryF.length;
  const first: number[] = [entryF[0 + 0]];
  const body: number[] = smoothArr(entryF, [1, size - 2], []);
  const last: number[] = [entryF[size - 1]];
  const result: number[] = first.concat(body, last);
  process.stdout.write(result.join(' '));
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const arr = entryArray[1].split(' ');
    solution(arr);
    return 0;
  });
  return 0;
}

main();
/*
$ tsc slayfer1112.ts
$ cat DATA.lst | node slayfer1112.js
27.8 31.2 34.93 41.57 44.73 48.47 48.9 47.13 44.7 43.73 37.03
33.2 27.8 25.93 18.9 17.53 13.93 16.2 14.37 17.57 15.3 14.13
15.53 23.47 31.47 36.73 40.07 43.93 43 44.97 46.43 46.5 44 43.07
41.43 33.73 25.7 21.33 20.2 17.47 11.47 9.8 7.83 11.57 13.17
16.53 20.47 20.77 25.57 30.57 42.67 47.13 50.3 48.93 49.17 46.47
48.4 49.6 47.77 44.23 34.87 29.6 16.67 12.93 9.7 10.93 10.63
13.73 16.23 18.57 20.7 25.9 27.77 31.83 35.8 43.13 50.27 55.8
56.47 52.4 46.67 43.57 38.77 35.27 30.8 27.33 24.73 20.33 17.03
12.03 11.33 10.03 7.23 10.63 18.2 27.5 27.63 30.6 34.73 43.53
51.53 57.83 58.17 49.2 42.47 41.23 40.5 34.9 29.1 21.47 18.93
11.57 11.03 11.2 15.6 15.9 15.73 17.47 26.43 30.97 34.5 35.13
39.9 43.97 46.87 48.8 50.4 49.77 45.27 36.93 30.47 25.63 29.17
26.57 22.93 13.1 9.97 10.13 10.63 10.43 13.43 20.63 25.4 28.47
31.27 35.9 40.97 43.5 44.33 46.2 49.43 51.63 49.23 43.57 42.83
38.7 37.4 29.47 21.97 10.5 7.53 7.07 8.27 8.77 12.53 17.17 23.1 31.7
*/
