-- $ curry-verify mankar91.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `mankar91'!

import List
import Maybe
import Read

orderParents :: [(Prelude.Int, a)] -> [(Prelude.Int, a)] ->
  Prelude.Int -> [a]
orderParents parents answer index
  | index == length parents = map snd answer
  | otherwise = orderParents parents (answer ++ [parent]) (index + 1)
  where
    parent = parents !!
      fromJust (findIndex (\item -> fst item == index) parents)

addParent :: (a, b) -> c -> (c, a)
addParent current item = (item, fst current)

visitNode :: [(Prelude.Int, Prelude.Int)] -> [Prelude.Int] ->
  [(Prelude.Int, Prelude.Int)] -> [[Prelude.Int]] ->
  ([(Prelude.Int, Prelude.Int)], [Prelude.Int], [(Prelude.Int, Prelude.Int)])
visitNode [] seen parents _ = ([], seen, parents)
visitNode (current:stack) seen parents graph
  | elem (fst current) seen = (stack, seen, parents)
  | otherwise
  = (neighbors ++ stack, seen ++ [fst current], parents ++ [current])
  where
    neighbors = map (addParent current) (graph !! (fst current) \\ seen )

depthFirstSearch :: ([(Prelude.Int, Prelude.Int)], [Prelude.Int],
  [(Prelude.Int, Prelude.Int)]) -> [[Prelude.Int]] -> [Prelude.Int]
depthFirstSearch (stack, seen, parents) graph
  | stack == [] = orderParents parents [] 0
  | otherwise = depthFirstSearch result graph
  where
    result = visitNode stack seen parents graph

-- Empty adjacency list
initGraph :: (Prelude.Eq a, Prelude.Num a) => a -> [[a]] -> [[a]]
initGraph nodes graph
  | nodes == 0 = graph
  | otherwise = initGraph (nodes - 1) ([]:graph)

-- Fills adjacency list as edges are read
insert :: [Prelude.Int] -> [[Prelude.Int]] -> [[Prelude.Int]]
insert edge graph
  = replace node_a (edge !! 0) (replace node_b (edge !! 1) graph)
  where
    node_a = sortBy (<=) ( graph !! (edge !! 0) ++ [edge !! 1] )
    node_b = sortBy (<=) ( graph !! (edge !! 1) ++ [edge !! 0] )

readGraph :: (Prelude.Eq b, Prelude.Num b) =>
  [[Prelude.Int]] -> a -> b -> Prelude.IO ()
readGraph prevGraph nodes edges = do
  edgeNodes <- getLine
  let
    edge = map readInt (words edgeNodes)
    graph = insert edge prevGraph
  if edges == 1
    then putStrLn (unwords (map show
      (depthFirstSearch ([(0, -1)], [], [])  graph)))
    else readGraph graph nodes (edges - 1)

main :: Prelude.IO ()
main = do
  size <- getLine
  let
    nodes = readInt (words size !! 0)
    edges = readInt (words size !! 1)
    emptyGraph = initGraph nodes []
  readGraph emptyGraph nodes edges

-- $ cat DATA.lst | pakcs -q :load mankar91.curry :eval main :quit
-- -1 0 1 12 23 2 3 14 9 15 33 4 24 29 36 18 7 11 16 17 21 30 6 5 13 32 9 41
-- 37 19 28 20 8 35 20 45 39 25 30 22 42 38 10 21 31 20 25
