-- hlint ludsrill.hs
-- No hints

import Control.Monad
import Data.List
import Data.Map (Map)
import qualified Data.Map as Map

main = do
  input <- getLine
  inputs <- replicateM (read $ head (tail (words input)) :: Int) getLine
  let tree = map (toInt . words) inputs
  let hashmap = myMap (read $ head (words input) :: Int)
  let result = Map.toList (depthFirstSearch tree [0] [] hashmap)
  mapM_ printing result

printing :: (Int, [Int]) -> IO ()
printing (a, [b, c]) = do
  putStr (show c)
  putStr " "

myMap :: Int -> Map Int [Int]
myMap n = Map.fromList (map makePair [0..(n-1)])
  where makePair x = (x, [0, -1])

toInt :: [String] -> [Int]
toInt = map read

depthFirstSearch :: [[Int]] -> [Int] -> [Int] -> Map Int [Int]
                      -> Map Int [Int]
depthFirstSearch (x:xs) (vertex:ys) = getQueue (x:xs) (vertex:ys)
  where
    getQueue (x:xs) []          path hashmap = hashmap
    getQueue []     []          path hashmap = hashmap
    getQueue []     (vertex:ys) path hashmap = getQueue (x:xs)
                                                (sort path ++ ys) [] hashmap

    getQueue (x : xs) (vertex : ys) path hashmap

      | head (hashmap Map.! 0) == 0 && vertex == 0 =
        getQueue (x : xs) (vertex : ys) path
          (Map.insert 0 [1, -1] hashmap)
      | head (hashmap Map.! vertex) == 0 =
        getQueue (x : xs) (vertex:ys) path
          (Map.insert vertex [1, last (hashmap Map.! vertex)] hashmap)
      | head x == vertex && head (hashmap Map.! head (tail x)) == 0 =
        getQueue xs (vertex : ys) (path ++ tail x)
          (Map.insert (head (tail x)) [0, vertex] hashmap)
      | head (tail x) == vertex && head (hashmap Map.! head x) == 0 =
        getQueue xs (vertex : ys) (path ++ [head x])
          (Map.insert (head x) [0, vertex] hashmap)
      | otherwise = getQueue xs (vertex : ys) path hashmap

-- cat DATA.lst | runhaskell ludsrill.hs
-- -1 8 12 15 24 1 21 29 4 23 6 2 27 17 5 14 11 43 30 48 3 20 26 31 7 10 13 25
-- 37 0 11 32 18 40 26 36 9 17 37 26 37 48 35 35 20 19 28 33 33
