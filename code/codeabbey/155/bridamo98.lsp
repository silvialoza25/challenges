#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defstruct
  (node (:conc-name dr-))
  value
  seen
  neighbors
  whoseen
  last-seen
)

(defun add-last(value)
  (setq stack (append stack value))
)

(defun get-last()
  (setq value (first (last stack)))
  (setq stack (subseq stack 0 (- (length stack) 1)))
  (return-from get-last value)
)

(defun initialize-structures (amount-nodes nodes)
  (loop for i from 0 to (- amount-nodes 1)
    do(setf (aref nodes i)
      (make-node
        :value i
        :seen (- 0 1)
        :neighbors (make-array amount-nodes :initial-element 0)
        :whoseen (- 0 1)
        :last-seen (- 0 1)
      )
    )
  )
)

(defun load-edge(nodes)
  (setq fth (read))
  (setq sth (read))
  (setf (aref (dr-neighbors (aref nodes fth)) sth) 1)
  (setf (aref (dr-neighbors (aref nodes sth)) fth) 1)
)

(defun load-input-data (amount-edges nodes)
  (loop for i from 0 to (- amount-edges 1)
    do(load-edge nodes)
  )
)

(defun update-neighbor (neighbor i current)
  (if (= neighbor 1)
    (progn
      (setf (dr-last-seen (aref nodes i)) current)
      (add-last (list (aref nodes i)))
    )
  )
)

(defun find-neighbors(current)
  (setq cont (- (length nodes) 1))
  (loop
    (update-neighbor (aref (dr-neighbors
      (aref nodes current) ) cont ) cont current)
    (setq cont (- cont 1))
    (when (< cont 0) (return NIL))
  )
)

(defun depth-first-search()
  (loop
    (setq current (dr-value (get-last)))
    (if (= (dr-seen (aref nodes current)) (- 0 1))
        (progn
          (setf (dr-whoseen (aref nodes current))
            (dr-last-seen (aref nodes current)))
          (setf (dr-seen (aref nodes current)) 1)
          (find-neighbors current)
        )
    )
    (when (not stack) (return NIL))
  )
)

(defun print-solution(nodes)
  (loop for i from 0 to (- amount-nodes 1)
    do(format t "~a " (dr-whoseen (aref nodes i)))
  )
)

(defvar stack '())
(defvar amount-nodes (read))
(defvar amount-edges (read))
(defvar nodes (make-array amount-nodes))

(initialize-structures amount-nodes nodes)
(load-input-data amount-edges nodes)
(defvar current (- 0 1))
(add-last (list (aref nodes 0)))
(depth-first-search)
(print-solution nodes)

#|
  cat DATA.lst | clisp bridamo98.lsp
  -1 5 1 9 11 16 34 8 17 26 15 3 14 24 2 0 25 28 12 30 19
  24 21 35 36 4 10 23 18 23 7 35 36 44 40 22 45 20 46 29
  37 39 29 32 6 6 41
|#
