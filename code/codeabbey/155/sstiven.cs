/*
$mcs -out:sstiven.exe sstiven.cs #compilation

$gendarme sstiven.exe #linting
Gendarme v4.2.0.0
Copyright (C) 2005-2011 Novell, Inc. and contributors

Initialization: 2.3 seconds
sstiven.exe: 0.1 seconds
TearDown: <0.1 seconds
One assembly processed in 2.4 seconds.
No defect found.
Produced on 6/29/2020 1:34:50 AM for:
   sstiven

Processed 235 rules and found no defects.
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]

namespace FluidAttacks{
  public sealed class Node{
    public int Number{ get; }
    public bool IsSeen{ get; set; }
    private readonly List<Node> _neighbours;

    public IEnumerable<Node> Neighbours{
      get{ return _neighbours.AsEnumerable<Node>(); }
    }

    public Node(int number){
      Number = number;
      IsSeen = false;
      _neighbours = new List<Node>();
    }

    public void AddNeighbour(Node node){
      _neighbours.Add(node);
    }
  }

  internal static class Program{
    public static void Main(){
      var initialParams = Console.ReadLine()?.Split(' ');
      if (initialParams == null) return;
      if (!int.TryParse(initialParams[0], out var nodeAmount))
        return;
      if (!int.TryParse(initialParams[1], out var edgeAmount))
        return;

      var edges = new int[edgeAmount, 2];

      for (var i = 0; i < edgeAmount; i += 1){
        var values = Console.ReadLine()?.Split(' ');
        if (values == null) return;
        edges[i, 0] = Convert.ToInt32(
          values[0],
          CultureInfo.CurrentCulture
        );
        edges[i, 1] = Convert.ToInt32(
          values[1],
          CultureInfo.CurrentCulture
        );
      }

      var nodes = GenerateGraph(edges, nodeAmount);
      var watchedFrom = new Dictionary<int, int>{{0, -1}};
      DeepFirstSearch(nodes[0], watchedFrom);
      PrintSeenFrom(watchedFrom);
    }

    private static Node[] GenerateGraph(int[,] edges, int nodeAmount){
      var nodes = new Node[nodeAmount];

      for (var i = 0; i < nodeAmount; i++){
        nodes[i] = new Node(i);
      }

      for (var i = 0; i < edges.GetLength(0); i += 1){
        nodes[edges[i, 0]].AddNeighbour(nodes[edges[i, 1]]);
        nodes[edges[i, 1]].AddNeighbour(nodes[edges[i, 0]]);
      }

      return nodes;
    }

    private static void DeepFirstSearch(
      Node destination,
      Dictionary<int, int> watchedFrom
    ){
      destination.IsSeen = true;
      IEnumerable<Node> neighbours
        = destination.Neighbours.OrderBy(node => node.Number);
      foreach (var neigh in neighbours){
        if (neigh.IsSeen) continue;
        watchedFrom.Add(neigh.Number, destination.Number);
        DeepFirstSearch(neigh, watchedFrom);
      }
    }

    private static void PrintSeenFrom(Dictionary<int, int> seenFrom){
      if (seenFrom == null) return;

      for (var i = 0; i < seenFrom.Count; i++){
        Console.WriteLine(seenFrom[i]);
      }
    }
  }
}

/*
$cat DATA.lst | mono sstiven.exe
-1
5
1
9
11
16
34
8
17
26
15
3
14
24
2
0
25
28
12
30
19
24
21
35
36
4
10
23
18
23
7
35
36
44
40
22
45
20
46
29
37
39
29
32
6
6
41
*/
