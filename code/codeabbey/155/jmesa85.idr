{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #155: Deep First Search - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi str = fromMaybe 0 $ parseInteger {a=Int} str

intAt : Int -> List Int -> Int
intAt 0 [] = 0
intAt _ [] = 0
intAt 0 (x::xs) = x
intAt index (_::xs) = intAt (index - 1) xs

-- Gets lines in the Stdin
getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

-- To be used by Data.List.sortBy
sortSeenNodes : List Int -> List Int -> Ordering
sortSeenNodes [node1, _] [node2, _] = if node1 > node2 then GT else LT

-- Is node in "seen" list
isNodeSeen : Int -> List(List Int) -> Bool
isNodeSeen _ [] = False
isNodeSeen node seen =
  let reduced = map (\[item, _] => item) seen in elem node reduced

-- Searchs among the edges for the given node
getNeighbors : Int -> List(List Int) -> List Int
getNeighbors node edges =
  let
    edgesByNode =
      filter (\[node1, node2] => node1 == node || node2 == node) edges
    neighbors =
      map (\[node1, node2] => if node1 == node
      then node2
      else node1) edgesByNode
  in
  sort neighbors

searchGraph :
  List(List Int) -> List Int -> List(List Int) -> List Int -> List(List Int)
searchGraph _ [] seen _ = seen
searchGraph edges stack seen prevNode =
  let
    currNode = intAt 0 stack -- LIFO
    newSeen = seen ++ [[currNode, (intAt 0 prevNode)]]
    neighbors = getNeighbors currNode edges
    newPrevNode = map (\item => currNode) neighbors
    newStack = neighbors ++ (drop 1 stack)
  in
  if isNodeSeen currNode seen
  then searchGraph edges (drop 1 stack) seen (drop 1 prevNode)
  else searchGraph edges newStack newSeen ((newPrevNode) ++ (drop 1 prevNode))

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse input array
    edges = map (map stoi) $ map words inputData
    -- Process data
    results = searchGraph edges [0] [] [(-1)]
    -- Sort "seen nodes"
    sorted = sortBy (sortSeenNodes) results
    -- Get "seen by node" list
    seenBy = map (\[_, node] => node) sorted
  -- Print results
  putStrLn $ unwords $ map show seenBy

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  -1 3 23 16 2 14 5 9 0 44 20 33 29 6 21 18 8 46 11 1 25 10 18 19 31
  7 27 31 43 22 17 43 30 41 13 24 7 42 15 45 24 34 32 42 4 43 12 27
-}
