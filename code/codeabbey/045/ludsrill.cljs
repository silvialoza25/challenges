;; clj-kondo --lint ludsrill.cljs
;; linting took 50ms, errors: 0, warnings: 0

(ns ludsrill.045
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn get-mod [data]
  (if (seq data)
    (cons (mod (Integer. (first data)) 52) (get-mod (rest data)))
    nil))

(defn cards [suits ranks remm]
  (if (seq suits)
      (if (seq remm)
        (cons (str (first suits) (first remm)) (cards suits ranks (rest remm)))
        (cards (rest suits) ranks ranks))
      nil))

(defn create-couple [maze data]
  (into (sorted-map) (map (fn [x y] (hash-map x y)) data maze)))

(defn cards-shuffling [original module]
  (loop [original original module module counter 0]
    (let [aux (get original counter)
          aux-2 (get original (first module))
          new (assoc (assoc original (first module) aux) counter aux-2)]
      (if (< counter 51)
          (recur new (rest module) (inc counter))
          [new]))))

(defn -main []
  (let [data (get-mod (first (get-data)))
        ranks (list 'A '2 '3 '4 '5 '6 '7 '8 '9 'T 'J 'Q 'K)
        suits (list 'C 'D 'H 'S)
        maze (cards suits ranks ranks)
        original-maze (create-couple maze (take 53 (range)))
        solution (first (cards-shuffling original-maze data))]

  (doseq [item solution]
    (print (core/format "%s " (second item))))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; DT D6 HK H4 H2 HA S8 DJ SK C4 CK C9 HJ S5 SA HT DK DA C6 H6 CJ SQ H8 S4 C8
;; HQ DQ CA S3 H9 S6 C7 ST H3 C3 SJ S9 D2 CQ D7 S7 H7 C5 D3 S2 D8 D9 D5 D4 H5
;; CT C2
