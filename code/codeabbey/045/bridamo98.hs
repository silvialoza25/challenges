{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Data.Char
import Control.Monad

main = do
  dat <- getLine
  let dividedDat = words dat
  let intDividedDat = map read dividedDat :: [Int]
  let indexes = map calcModulus intDividedDat
  let suit = ["C", "D", "H", "S"]
  let rank = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"]
  let initialCards = [x ++ y | x<- suit, y<- rank]
  putStrLn (unwords initialCards)

swapElementsAt :: Int -> Int -> [a] -> [a]
swapElementsAt i j xs = do
  let elemI = xs !! i
  let elemJ = xs !! j
  let left = take i xs
  let middle = take (j - i - 1) (drop (i + 1) xs)
  let right = drop (j + 1) xs
  left ++ [elemJ] ++ middle ++ [elemI] ++ right

calcModulus :: Int -> Int
calcModulus num = res
  where
    res = num  `mod` 52

{-
$ cat DATA.lst | ./bridamo98
  H7 HT D6 DT SA C4 C7 C2 CJ C3 DQ S9 D3 H5 CT H4 H9 D4 H6
  S4 D2 DK H8 CK SQ S8 H3 C5 C8 D8 HK C9 DJ DA S5 CA ST S6
  D7 SK D5 HQ H2 D9 S2 C6 S7 CQ SJ HA HJ S3
-}
