;; $clj-kondo --lint nickkar.clj
;; linting took 61ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class))

(def initial-order
  ["CA" "C2" "C3" "C4" "C5" "C6" "C7" "C8" "C9" "CT" "CJ" "CQ" "CK"
   "DA" "D2" "D3" "D4" "D5" "D6" "D7" "D8" "D9" "DT" "DJ" "DQ" "DK"
   "HA" "H2" "H3" "H4" "H5" "H6" "H7" "H8" "H9" "HT" "HJ" "HQ" "HK"
   "SA" "S2" "S3" "S4" "S5" "S6" "S7" "S8" "S9" "ST" "SJ" "SQ" "SK"])

(defn swap [coll i j]
  (assoc coll j (coll i) i (coll j)))

(defn suffle ([coll] (suffle coll 0))
  ([coll i]
   (cond
     (> i 51) coll
     :else (suffle (swap coll i (mod (read) 52)) (inc i))
     )
   ))

(defn main []
  (apply println (suffle initial-order)))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; DT D6 HK H4 H2 HA S8 DJ SK C4 CK C9 HJ S5 SA HT DK DA C6 H6 CJ SQ H8 S4
;; C8 HQ DQ CA S3 H9 S6 C7 ST H3 C3 SJ S9 D2 CQ D7 S7 H7 C5 D3 S2 D8 D9 D5
;; D4 H5 CT C2
