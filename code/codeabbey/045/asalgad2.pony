// ponyc -b asalgad2

use "collections"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail( array: Array[String] ): Array[String] =>
          array.slice(1, array.size())

        fun print_array( array: Array[String] ) ? =>
          if array.size() >= 1 then
            env.out.write( array(0)?.string() )
            env.out.write( " " )
            print_array( tail( array ) )?
          end

        fun get_rank( ): Array[String] =>
          ["A"; "2"; "3"; "4"; "5"; "6"; "7"; "8"; "9"; "T"; "J"; "Q"; "K"]

        fun combine( suite: String, rank: Array[String],
                     const_i: USize ): Array[String] ? =>
          if (const_i >= 0) and (const_i < 13) then
            rank.update( const_i, suite + rank(const_i)? )?
            combine( suite, rank, const_i + 1 )?
          else
            rank
          end

        fun make_swaps( args: Array[String], deck: Array[String],
                        const_i: USize )  ? =>
          if (const_i >= 0) and (const_i < 52) then
            var position: USize = args(const_i)?.usize()? % 52
            var first_card: String = deck(const_i)?
            var second_card: String = deck(position)?
            deck.update( const_i, second_card )?
            deck.update( position, first_card )?
            make_swaps( args, deck, const_i + 1 )?
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")

          try
            var args: Array[String] = lines(0)?.split(" ")
            var deck: Array[String] = Array[String].init("", 52)

            combine( "C", get_rank(), 0 )?.copy_to( deck, 0, 0, 13 )
            combine( "D", get_rank(), 0 )?.copy_to( deck, 0, 13, 13 )
            combine( "H", get_rank(), 0 )?.copy_to( deck, 0, 26, 13 )
            combine( "S", get_rank(), 0 )?.copy_to( deck, 0, 39, 13 )

            make_swaps( args, deck, 0 )?
            print_array( deck )?
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// CQ HK H6 CJ SA D9 S9 SK CA D5 H4 DT S6 H2 DK CT S2 C3 C6 S4 C7 H9 C8 D4 C2 S5
// CK SQ HT DA HA S8 D7 C4 HJ C9 ST DJ D8 C5 H5 S7 D6 H8 SJ H3 DQ S3 H7 D2 D3 HQ
