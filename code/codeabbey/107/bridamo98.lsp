#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defstruct
  (movement (:conc-name dr-))
  degree
  move
)

(defun order-condition(a b)
  (< (dr-degree a) (dr-degree b))
)

(defun order-moves(movements)
  (sort movements 'order-condition)
)

(defun get-degree(board i j)
  (let ((cont 0))
    (loop for ii from 0 to 7
      do(if (string= (aref board (+ i (aref movements ii 0))
      (+ j (aref movements ii 1))) "-")
        (setq cont (+ cont 1))
      )
    )
    (return-from get-degree cont)
  )
)

(defun get-best-movement(board i j)
  (let ((moves '()) (deg NIL)(move NIL)(aux NIL))
    (setq aux (aref board i j ))
    (setf (aref board i j ) "x")
    (loop for ii from 0 to 7
      do(if (string= (aref board (+ i (aref movements ii 0))
      (+ j (aref movements ii 1))) "-")
        (progn
          (setq deg (get-degree board (+ i (aref movements ii 0))
          (+ j (aref movements ii 1))))
          (setq move (make-movement
            :degree deg
            :move ii
          ))
          (setq moves (append moves (list move)))
        )
      )
    )
    (setf (aref board i j ) aux)
    (return-from get-best-movement (order-moves moves))
  )
)

(defun initialize-board(board m n en em)
  (loop for i from 2 to (+ m 1)
    do(loop for j from 2 to (+ n 1)
      do(setf (aref board i j) "-")
    )
  )
  (setf (aref board (+ (- m en) 1) (+ em 2)) "x")
)

(defun print-solution (ri rj solution)
  (format t "~a ~a " rj ri)
  (loop for i from 0 to (- (length solution) 1)
    do(format t "~a " (aref solution i))
  )
)

(defun find-solution-in-depth(level i j board solution)
  (let ((best-moves NIL)(ite 0)(ri 0)(rj 0) (res 0))
    (if (= level (- (* M N) 1))
      (return-from find-solution-in-depth 1)
    )
    (setq best-moves (get-best-movement board i j))
    (if (= (length best-moves) 0)
      (progn
        (return-from find-solution-in-depth 0)
      )
    )
    (setq ri (+ i (aref movements (dr-move (nth ite best-moves)) 0)))
    (setq rj (+ j (aref movements (dr-move (nth ite best-moves)) 1)))
    (setf (aref board ri rj) (write-to-string level))
    (setf (aref solution (- level 1)) (dr-move (nth ite best-moves)))
    (setq res (find-solution-in-depth (+ level 1) ri rj board solution))
    (return-from find-solution-in-depth res)
  )
)

(defun find-solution(M N board)
  (let ((i (+ M 1))(j 2)(res 0)(ri 0) (rj 0))
    (loop
      (loop
        (if (string= (aref board i j) "-")
          (progn
            (setq board (make-array (list (+ M 4) (+ N 4))
            :initial-element "x"))
            (initialize-board board M N emp-sqr-n emp-sqr-m)
            (setq solution (make-array (- (* M N) 2)))
            (setf (aref board i j) "0")
            (setq res (find-solution-in-depth 1 i j board solution))
            (if (= res 1)
              (progn
                (setq ri (+ (- M i) 1))
                (setq rj (- j 2))
              )
            )
          )
        )
        (setq j (+ j 1))
        (when (or (> j (+ N 1)) (= res 1)) (return NIL))
      )
      (setq j 2)
      (setq i (- i 1))
      (when (or (< i 2) (= res 1))(return NIL))
    )
    (print-solution ri rj solution)
  )
)

(defvar M (read))
(defvar N (read))
(defvar emp-sqr-m (read))
(defvar emp-sqr-n (read))

(defparameter board (make-array (list (+ M 4) (+ N 4)) :initial-element "x"))
(defparameter solution (make-array (- (* M N) 2)))

(defparameter movements (make-array '(8 2)
  :initial-contents '((-1 2) (-2 1) (-2 -1) (-1 -2)
  (1 -2) (2 -1) (2 1) (1 2)))
)

(initialize-board board M N emp-sqr-n emp-sqr-m)
(find-solution M N board)

#|
  cat DATA.lst | clisp bridamo98.lsp
  39 2 5 0 7 0 7 0 7 0 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 4
  3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 5 0 3 6 1 4 6 5 6 5 6 5
  6 5 6 5 6 5 6 5 6 5 6 5 6 5 6 5 6 5 6 0 7 0 7 0 7 0 7 0 7 0 7 0 7 0 7 0 7
  0 7 0 7 0 7 0 7 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 3 4 3 4
  3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 3 4 4 1 4 5 6 5 6 5 6 5 6 5 6 5 6 5 6
  5 6 5 0 1 2 5 2 1 2 0 3 0 3 6 6 6 1 2 2 2 2 0 3 0 3 6 6 6 6 3 3 0 0 2 2 2
  2 0 3 0 3 1 0 0 2 5 0 0 5 4 4 5 2 0 6 0 2 0 0 6 4 5 2 4 5 5 6 1 1 3 0 0 0
  0 0 6 4 4 1 1 4 4 6 0 0 0 0 6 4 4 1 1 4 4 4 4 4 4 5 0 0 3 5 0 1 0 5 5 4 5
  0 0 1 0 0 0 0 0 0 6 4 4 1 1 4 4 4 4 4 4 4 5 0 0 0 3 0 0 5 4 4 4 4 4 5 0 0
  5 0 1 0 0 0 0 0 0 0 0 0 6 4 4 1 1 4 4 4 4 4 4 4 5 0 0 0 3 0 0 5 4 4 4 4 4
  4 5 0 0 0 0 0 0 0 0 0 0 0 0 0 6 4 4 1 1 4 4 4 4 4 4 4 4 4 4 7 0 0 0 0 2 0
  0 5 4 4 4 4 4 4 4 1 4 4 5 0 6 1 0 5 3 5 0 1 0 0 0 0 0 0 0 0 0 5 4 4 4 4 4
  4 4 4 5 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6 4 4 1 1 4 4 4 4 4 4 5 0 0 0 0 0 5
  4 4 4 4 4 4 1 4 4 4 4 5 4 4 4 1 1 6 5 0 5 4 5 0 0 3 1 1 0 0 0 0 0 0 0 0 0
  0 0 0 0 0 0 5 4 4 4 4 4 4 4 4 4 4 4 4 4 4 5 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0
  0 0 6 4 4 4 4 4 4 4 4 4 4 4 3 4 4 7 0 5 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 5 4
  4 4 4 4 4 4 4 4 4 4 4 4 4 4 1 4 5 7 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 4
  4 1 4 5 4 1 4 5 4 1 4 5 4 1 4 5 4 1 4 5 4 1 4 5 4 1 4 5 4 1 4 5 4 1 4 5 5
  0 5 4 7 1 1 1 0 5 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6 4 3 4 4 4 4 4 4 4
  4 4 4 4 4 4 4 4 4 4 6 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 5 4 4 4 4 4
  4 4 4 4 4 4 4 4 4 4 5 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6 4 4 4 3 4 4 4
  4 4 4 4 4 4 4 4 4 4 4 4 4 4 2 4 5 0 5 4 5 0 5 4 7 1 1 1 1 0 0 0 6 4 4 4 5
  3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6 4 4 1 1 7 5 6 3 3 0 3
  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 5 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
  0 0 0 0 0 0 7 4 7 6 7 2 2 2 7 6 4 2 3 4 4 4 4 1 4 4 4 4 4 4 4 4 4 4 4 4 5
  0 0 0 0 0 0 0 0 0 0 0 5 3 0 0 0 0 0 0 5 4 4 4 4 4 4 4 1 4 4 4 4 4 4 4 4 4
  4 4 4 4 4 4 5 0 0 0 0 0 0 0 0 0 0 0 0 0 0 5 3 0 0 0 0 0 0 0 0 0 0 7 4 4 4
  4 4 4 4 4 4 5 2 0 0 0 5 0 0 1 0 0 0 0 0 7 4 4 1 4 4 4 4 4 4 4 4 4 4 4 4 1
  4 4 4 4 4 4 4 4 4 4 5 0 0 0 3 0 0 0 5 0 0 0 0 0 0 0 6 4 1 4 4 4 4 4 4 4 4
  4 4 4 4 4 5 0 0 0 2 0 0 0 5 0 0 0 0 0 0 0 0 0 0 0 5 0 0 0 0 1 0 0 0 0 7 4
  4 4 1 4 4 4 4 4 4 4 4 4 4 4 4 1 4 4 4 4 4 5 0 0 0 0 5 0 0 0 0 0 0 0 0 0 0
  0 0 5 2 0 0 0 0 0 7 4 6 0 3 3 4 7 4 3 4 4 7 0 5 2 0 0 0 7 4 7 2 7 6 5 6 3
  2 2 2 4 4 4 4 3 4 4 4 5 0 0 0 0 0 6 4 3 4 4 4 4 4 1 4 4 4 4 4 4 4 4 4 4 4
  1 4 4 5 0 5 2 5 5 5 6 5 6 5 6 5 6 7 2 4 7 0 7 0 7 0 7 0 7 0 7 0 7 0 7 0 7
  0 0 6 0 7 2 3 3 5 5 0 7 1 3 0 5 7 7 1 3 0 5 3 1 6 7 6 3 3 2 3 6 0 2 7 6 6
  7 0 7 1 3 4 3 0 5 3 3 0 3 4 4 1 0 7 6 6 1 7 5 7 0 3 1 1 3 5 5 0 5 0 1 2 5
  3 4 6 0 1 1 1 3 5 5 5 0 1 0 2 1 4 5 5 0 5 4 5 0 3 4 1 0 1 6 1 1 1 3 5 5 5
  5 3 6 0 2 1 0 1 1 1 3 5 5 0 0 5 4 4 5 0 0 5 5 4 1 4 5 4 1 1 6 4 1 0 1 1 1
  1 1 1 3 5 5 0 0 5 4 4 5 0 0 5 5 2 1 1 1 1 1 4 5 5 5 5 6 4 5 0 5 4 4 1 1 7
  1 3 6 1 1 1 1 1 1 1 3 5 5 5 5 5 5 5 0 5 4 5 0 5 4 4 7 1 1 1 1 1 1 1 1 1 1
  1 1 4 5 5 5 5 5 5 5 5 5 5 5 5 4 1 1 1 1 1 1 1 7 1 1 1 1 1 1 1 2 5 5 5 5 5
  5 5 5 5 5 5 5 2 7 1 1 1 1 1 1 1 1 1 1 1 1 3 5 5 0 2 2 0 0 2 3 6 5 7 2 2 3
  6 3 4 4 4 4 4 4 4 4 3 4 4 4 1 4 4 4 4 4 4 4 4 5 4 6 1 0 1 0 0 0 5 0 0 0 0
  0 0 0 0 0 0 0 0 5 4 4 4 4 4 3 4 4 4 4 4 4 4 4 2 5 4 1 4 5 5 0 5 3 4 7 5 5
  7 1 2 1 0 1 0 5 0 0 3 0 0 0 0 0 0 5 0 0 0 0 0 0 0 0 0 0 0 0 0 7 4 4 4 4 4
  4 4 4 4 4 1 4 4 4 4 4 4 4 4 5 2 0 5 0 0 0 0 0 0 0 5 0 0 0 0 0 0 0 0 0 0 1
  0 7 4 4 1 7 4 3 4 4 4 4 4 4 4 4 4 4 4 4 4 4 1 4 4 4 4 4 4 5 0 0 0 0 0 5 0
  0 0 0 0 0 0 0 0 0 0 0 0 0 0 5 0 0 0 3 6 7 6 5 5 5 5 5 5 5 5 5 5 5 5 4 1 1
  1 1 1 1 1 6 1 1 1 1 1 2 2 3 4 1 4 4 4 4 4 4 4 4 4 4 1 4 4 4 4 4 4 5 0 0 0
  0 0 5 2 0 6 0 0 0 0 0 0 0 0 0 7 0 1 1 6 7 2 5 6 3 3 0 3 4 4 4 4 4 4 4 4 4
  4 4 4 4 4 1 4 4 4 4 4 1 4 5 4 4 5 2 0 5 4 6 5 0 1 1 0 3 0 0 0 5 0 5 3 0 0
  5 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 5 0 0 5 3 0 0 7 4 7 6 5 5 5 5 5 5 5 5 2 7
  1 1 1 1 1 1 1 1 2 2 7 5 3 6 5 2 7 2 2 7 6 4 2 3 4 4 4 4 4 4 4 4 4 4 4 4 1
  4 4 4 4 7 0 1 0 0 0 0 0 0 0 0 0 0 0 7 4 4 1 4 4 4 4 4 4 4 4 4 4 3 5 4 1 4
  4 4 7 0 0 0 5 0 0 0 0 0 0 0 0 0 0 5 3 0 0 0 0 0 7 4 4 4 4 5 2 0 0 0 0 0 7
  4 6 0 3 3 4 7 4 3 4 4 4 4 4 1 4 4 4 4 4 4 7 0 0 0 0 0 0 5 0 2 4 4 4 4 4 4
  4 4 1 4 4 4 3 4 4 1 4 6 3 0 0 6 4 4 5 2 0 0 0 5 0 5 2 0 0 7 4 4 4 4 5 2 0
  5 0 5 2 0 0 0 0 5 0 0 0 0 0 0 0 0 7 4 4 4 4 4 4 1 4 4 4 4 4 4 5 3 3 5 0 5
  0 5 4 6 0 1 3 0 1 0 0 5 4 5 0 0 1 0 3 0 0 6 4 4 4 4 5 0 2 0 0 0 0 0 0 0 0
  0 0 0 0 0 0 0 0 0 0 7 6 5 2 0 3 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 3
  6 7 0 0 1 0 0 0 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 7 6 3 6 1 3 5 6 1 3 6 3 3 4
  7 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 3 4 5 0 6 4 5 0 7 1 2 4 1 6 3 6 7 7 2
  2 5 0 1 1 4 4 7 0 5 2 5 6 7 1 2 5 7 2 7 6 0 7 2 3 3 2 1 1 0 0 0 0 0 0 0 0
  0 0 0 0 0 0 0 7 4 6 0 3 3 4 7 4 3 4 4 4 4 4 4 4 4 4 5 0 0 0 0 0 0 0 0 0 0
  0 6 1 7 5 2 2 7 6 4 2 7 5 5 2 7 5 3 0 0 2 3 6 3 3 0 6 4 4 1 4 4 4 4 4 4 4
  4 4 4 4 4 4 4 5 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 7 4 4 4 4 4 4 4 4 4 4 4 1 4
  4 5 0 5 4 5 0 7 4 2 0 0 6 3 5 6 1 3 2 1 6 6 6 7 1 3 3 2 1 0 5 0 1 0 0 0 0
  0 0 0 0 0 0 7 4 4 4 4 4 4 4 4 4 4 4 4 4 6 1 6 3 0 2 5 6 6 7 7 1 3 3 3 0 1
  4 1 0 0 0 0 0 0 0 0 5 0 0 0 6 1 3 5 5 7 6 1 1 3 4 2 7 6 5 2 3 5 7 6 3 0 1
  4 3 0 3 4 4 4 4 4 4 4 4 5 5 0 1 0 0 0 0 0 0 0 7 4 4 1 4 4 4 4 4 4 4 5 5 7
  6 3 3 0 1 1 1 0 0 0 0 0 7 4 4 4 4 4 4 5 5 7 2 1 1 0 0 0 0 0 0 0 5 4 4 4 4
  4 4 5 3 5 6 1 1 0 0 0 0 0 0 0 0 6 7 2 4 6 3 3 4 4 4 1 4 4 4 4 5 0 5 6 3 6
  7 0 7 2 3 3 1 0 0 0 0 0 5 0 7 2 4 4 4 4 4 1 4 5 5 0 3 5 7 6 3 1 7 1 3 0 0
  0 5 2 0 0 0 0 7 4 4 4 4 4 3 4 5 0 0 0 0 0 0 7 4 4 4 4 4 4 1 6 3 5 7 7 1 2
  4 1 6 0 3 0 0 0 6 1 1 6 7 2 5 6 3 5 7 2 2 2 7 6 4 2 3 4 5 0 0 7 4 6 0 3 3
  4 4 5 0 0 6 3 0 1 4 7 7 6 3 3 0 5 6 1 4 4 3 0 0 2 4 4 4 7 0 7 2 4 6
|#
