(*
$ ocp-lint --severity-limit 3
  Scanning files in project "."...
  Found '3' file(s)
  Running analyses... 3 / 3
  Merging database...
  == New Warnings ==
    * 0 file(s) were linted
    * 0 warning(s) were emitted:
    * 0 file(s) couldn't be linted
*)

let explode s =
  let rec exp i l =
    if i < 0 then l
    else if s.[i] = ' ' then exp (i - 1) l
    else exp (i - 1) (s.[i] :: l) in
  exp (String.length s - 1) []

let maybe_read_line () =
  try Some (read_line ())
  with End_of_file -> None

let rec get_data acc =
  match maybe_read_line () with
  | Some (line) -> get_data (acc @ [(explode line)])
  | None -> (List.tl acc)

let calculate_distance item =
  let rec get_hick item hicks =
    match item with
    | [] -> Printf.printf "%.8f "
            (sqrt ((List.nth hicks 0) ** 2.0 +. (List.nth hicks 1) ** 2.0))
    | x::xs -> get_hick xs
              [(List.nth hicks 0 +. sin (x *. (2.0 *. asin 1.0) /. 180.0));
               (List.nth hicks 1 +. cos (x *. (2.0 *. asin 1.0) /. 180.0))]
  in get_hick item [0.0; 0.0]

let convert_to_degrees movements =
  let rec convertion movement result =
    match movement with
    | x::xs when x == 'A' -> convertion xs (result @ [0.0])
    | x::xs when x == 'B' -> convertion xs (result @ [60.0])
    | x::xs when x == 'C' -> convertion xs (result @ [120.0])
    | x::xs when x == 'D' -> convertion xs (result @ [180.0])
    | x::xs when x == 'E' -> convertion xs (result @ [240.0])
    | x::xs when x == 'F' -> convertion xs (result @ [300.0])
    | _ -> calculate_distance result
  in convertion movements []

let main () =
  let data = get_data [] in
    List.iter convert_to_degrees data

let () = main ()

(*
$ cat DATA.lst | ocaml ludsrill.ml
  1.73205081 3.00000000 2.00000000 2.00000000 4.35889894 3.46410162 5.56776436
  4.58257569 1.73205081 1.00000000 0.00000000 1.73205081 4.35889894 1.00000000
  7.93725393 2.64575131 9.53939201 3.46410162
*)
