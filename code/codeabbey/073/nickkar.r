# $ lintr::lint('nickkar.r')

options(digits = 8)

step <- function(position, word, i) {
    if (i <= nchar(word)) {
        letter <- substring(word, i, i)
        switch(letter,
        "A" = {
            step(position + c(1, 0), word, i + 1)
        },
        "B" = {
            step(position + c(1 / 2, sqrt(3) / 2), word, i + 1)
        },
        "C" = {
            step(position + c(-1 / 2, sqrt(3) / 2), word, i + 1)
        },
        "D" = {
            step(position + c(-1, 0), word, i + 1)
        },
        "E" = {
            step(position + c(-1 / 2, -sqrt(3) / 2), word, i + 1)
        },
        "F" = {
            step(position + c(1 / 2, -sqrt(3) / 2), word, i + 1)
        }, {
            print("Wrong Input")
        }
        )
    }
    else {
        return(position)
    }
}

query <- function(word) {
    position <- step(c(0, 0), word, 1)
    distance <- sqrt(((position[1]) ^ 2) + ((position[2]) ^ 2))
    return(distance)
}

execute <- function(words, solutions, j) {
    if (j <= length(words)) {
        solutions <- c(solutions, query(words[j]))
        execute(words, solutions, j + 1)
    }
    else {
        return(solutions)
    }
}

data <- readLines("stdin")
words <- c(data[2:length(data)])
solutions <- vector()

print(execute(words, solutions, 1))

# $ cat DATA.lst | rscript nickkar.r
# [1] 0.0000000 4.5825757 3.6055513 4.0000000 3.4641016 6.2449980 2.6457513
# [8] 1.7320508 1.7320508 1.0000000 5.2915026 1.0000000 5.2915026 1.0000000
# [15] 3.0000000 1.0000000 2.0000000 1.7320508 2.6457513 6.5574385 3.6055513
# [22] 3.4641016 0.0000000 1.7320508
