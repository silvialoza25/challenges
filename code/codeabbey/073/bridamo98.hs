{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let diagDistance = 0.86602540378
  let rectDistance = 1.0
  let nonDistance = 0.0
  let solution = map show (solve inputData diagDistance
                 rectDistance nonDistance)
  putStrLn (unwords solution)

solve :: [String] -> Double -> Double -> Double -> [Double]
solve [] diagDistance rectDistance nonDistance = []
solve dat diagDistance rectDistance nonDistance = result
  where
    moves = head dat
    deltas = calcDeltas moves diagDistance rectDistance nonDistance
    xDistance = sum (map fst deltas)
    yDistance = sum (map snd deltas)
    iResult = sqrt((xDistance ** 2) + (yDistance ** 2))
    result = iResult : solve (drop 1 dat) diagDistance
             rectDistance nonDistance

calcDeltas :: String -> Double -> Double -> Double -> [(Double, Double)]
calcDeltas [] diagDistance rectDistance nonDistance = []
calcDeltas moves diagDistance rectDistance nonDistance = deltas
  where
    move = head moves
    iDeltas = findSingleDeltas move diagDistance rectDistance nonDistance
    deltas = iDeltas : calcDeltas (drop 1 moves) diagDistance
             rectDistance nonDistance

findSingleDeltas :: Char -> Double -> Double -> Double -> (Double, Double)
findSingleDeltas move diagDistance rectDistance nonDistance = deltas
  where
    deltas
      | move == 'A' = (rectDistance, nonDistance)
      | move == 'B' = (rectDistance / 2.0, diagDistance)
      | move == 'C' = (-(rectDistance / 2.0), diagDistance)
      | move == 'D' = (-rectDistance, nonDistance)
      | move == 'E' = (-(rectDistance / 2.0), -diagDistance)
      | otherwise = (rectDistance / 2.0, -diagDistance)

{-
$ cat DATA.lst | ./bridamo98
  2.6457513110631377 4.582575694934869 2.220446049250313e-16
  3.4641016151333157 0.9999999999961561 2.645751311051514
  1.7320508075666579 2.645751311051514 5.5677643628127615 2.6457513110515145
  1.999999999992312 6.557438524301414 1.7320508075666579 4.358898943537146
  8.544003745281088 8.717797887065473 3.9999999999846243 3.6055512754629233
  2.6457513110515145 0.9999999999961561 4.0
-}
