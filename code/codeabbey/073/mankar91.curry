-- $ curry-verify mankar91.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `mankar91'!

import Float

findDistance :: [Prelude.Float] -> Prelude.Float
findDistance [] = 0
findDistance [_] = 0
findDistance (_ : _ : _ : _) = 0
findDistance [horizontal, vertical]
  = sqrt (horizontal * horizontal + vertical * vertical * (0.75))

getMovement :: Prelude.Fractional a => Prelude.Char -> [a]
getMovement step
  | step == 'A' = [1, 0]
  | step == 'B' = [0.5, 1]
  | step == 'C' = [-0.5, 1]
  | step == 'D' = [-1, 0]
  | step == 'E' = [-0.5, -1]
  | step == 'F' = [0.5, -1]

processStep :: [Prelude.Float] -> Prelude.IO ()
processStep [] = return()
processStep [_] = return()
processStep (_ : _ : _ : _) = return()
processStep [horizontal, vertical] = do
  step <- getChar
  if step /= '\n' then
    processStep (zipWith (+) [horizontal, vertical] (getMovement step))
    else
      putStr (show (findDistance [horizontal, vertical]) ++ " ")

processTestCases :: (Prelude.Eq a, Prelude.Num a) =>
  a -> Prelude.IO ()
processTestCases tests = do
  processStep [0,0]
  if tests == 1
    then putChar '\n'
    else processTestCases (tests - 1)

main :: Prelude.IO ()
main = do
  tests <- getLine
  processTestCases (read tests)

-- $ cat DATA.lst | pakcs -q :load mankar91.curry :eval main :quit
-- 3.0 9.16515138991168 0.0 2.6457513110645907 2.6457513110645907 2.0 1.0
-- 8.18535277187245 4.358898943540674 4.358898943540674 7.211102550927978
-- 7.211102550927978 2.0 2.6457513110645907 4.58257569495584 2.6457513110645907
-- 7.937253933193772 3.605551275463989 3.605551275463989 10.816653826391969
-- 3.605551275463989 1.0
