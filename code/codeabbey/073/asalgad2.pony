// ponyc -b asalgad2

use "format"
use "collections"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail( array: Array[String] ): Array[String] =>
          array.slice(1, array.size())

        fun tail_chars( array: Array[U8] ): Array[U8] =>
          array.slice(1, array.size())

        fun print_array( array: Array[String] ) ? =>
          if array.size() >= 1 then
            env.out.write( array(0)?.string() )
            env.out.write( " " )
            print_array( tail( array ) )?
          end

        fun get_vector( direction: U8 ): Array[F64]  =>
          if direction == 66 then
            [ 0.5; 0.8660254037844386 ]
          elseif direction == 67 then
            [ -0.5; 0.8660254037844386 ]
          elseif direction == 68 then
            [ -1; 0 ]
          elseif direction == 69 then
            [ -0.5; -0.8660254037844386 ]
          elseif direction == 70 then
            [ 0.5; -0.8660254037844386 ]
          else
            [ 1; 0 ]
          end

        fun sum_vector(const_a: Array[F64], const_b: Array[F64]): Array[F64]? =>
          [ const_a(0)? + const_b(0)?; const_a(1)? + const_b(1)? ]

        fun move( sequence: Array[U8], start: Array[F64] ): Array[F64] ? =>
          if sequence.size() > 0 then
            var direction: Array[F64] = get_vector( sequence(0)? )
            move( tail_chars(sequence), sum_vector(direction, start)? )?
          else
            start
          end

        fun calc_distance( position: Array[F64] ): F64 ? =>
          var const_a: F64 = position(0)?.pow(2)
          var const_b: F64 = position(1)?.pow(2)
          (const_a + const_b).sqrt()

        fun format_float( number: F64, precision: USize ): String =>
          Format.float[F64](number where fmt=FormatFixLarge, prec=precision)

        fun iterate( lines: Array[String] ) ? =>
          if lines.size() > 0 then
            var sequence: String = lines(0)?

            var end_pos: Array[F64] = move(sequence.array().clone(), [0; 0])?
            var distance: F64 = calc_distance( end_pos )?
            env.out.write( format_float( distance, 8 ) )
            env.out.write( " " )

            iterate( tail(lines) )?
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")
          var useful_lines: Array[String] = lines.slice(1, lines.size()-1)

          try
            iterate( useful_lines )?
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// 1.73205081 2.64575131 1.73205081 6.24499800 3.46410162 3.00000000 3.60555128
// 1.00000000 4.00000000 1.73205081 2.00000000 1.73205081 1.00000000 3.60555128
// 4.58257569 1.00000000 1.73205081 2.64575131
