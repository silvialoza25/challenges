// dartanalyzer ludsrill.dart
// Analyzing ludsrill.dart...
// No issues found!

import 'dart:core';
import 'dart:io';
import 'dart:math';

void main() {
  List<int> input = getData([]);
  List<int> solution = input.map((item) => beamBalanceAndMasses(item)).toList();
  print(solution.join(" "));
}

List<int> getData(List<List<int>>input) {
  String getLine = stdin.readLineSync();
  if (getLine == null) {
    return input.sublist(1).first;
  } else {
    List<String> newLine = getLine.split(" ");
    return getData(input + [newLine.map((item) => int.parse(item)).toList()]);
  }
}

int beamBalanceAndMasses(int input) {
  List<int> weightList = maxWeight(input, 0, []);
  return balance(input, weightList, weightList.length - 2);
}

List<int> maxWeight(int input, int threePow, List<int> weightList) {
  if (input > calculateMaxWeight(threePow, 0, 0)) {
    return maxWeight(input, threePow + 1, weightList + [-1]);
  }
  else {
    return (weightList + [1]);
  }
}

int balance(int input, List<int> weightList, int threePow) {
  if (threePow >= 0) {
    if (currentMass(threePow, weightList, 0, 0) + pow(3, threePow) <= input) {
      return balance(input, modifyList(threePow, weightList), threePow);
    } else {
      return balance(input, weightList, threePow - 1);
    }
  } else {
    return weightList.fold(
        0, (acc, element) => (element.abs() == 1) ? acc + 1 : acc);
  }
}

List<int> modifyList(int position, List<int> list) {
  list.replaceRange(position, position + 1, [list[position] + 1]);
  return list;
}

int currentMass(int threePow, List<int> weightList, int count, int acc) {
  if (count < weightList.length) {
    return currentMass(threePow, weightList, count + 1,
        (pow(3, count).toInt() * weightList[count]) + acc);
  } else {
    return acc;
  }
}

int calculateMaxWeight(int threePow, int count, int power) {
  if (count < threePow) {
    return calculateMaxWeight(
        threePow, count + 1, pow(3, count).toInt() + power);
  } else if (count == threePow) {
    return calculateMaxWeight(
        threePow, count + 1, pow(3, count).toInt() + power);
  } else {
    return power;
  }
}

// cat DATA.lst | dart ludsrill.dart
// 6 6 11 4 3 4 7 7 6 9 4 6 9 9 6 6 12 9 8 4 7 11 7
