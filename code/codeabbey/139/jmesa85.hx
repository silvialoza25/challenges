/**
  $ haxelib run checkstyle \
    --exitcode \
    --show-parser-errors \
    --nothreads \
    --default-config \
    -s jmesa85.hx
**/

// CodeAbbey #139: Beam Balance and Masses - Haxe

using StringTools;
using Math;

class Main {
  static public function main():Void {
    // Read Stdin
    var data = Sys.stdin().readAll().toString().trim().split("\n");
    // Get the test cases
    var testCases:Array<Int> = data[1].split(" ").map(Std.parseInt);
    // Calculate
    var result = testCases.map(balanceMass);
    // Print results
    trace(result.join(" "));
  }

  // Returns number of weights needed to balance
  static function balanceMass(mass) {
    var weight = Math.pow(3, getGreaterPowerOf3(mass, 0));
    // The next weight is fine for the mass
    if (weight == mass)
      return 1;
    // Check if we are over
    if ((weight / 2) > mass)
      return 1 + balanceMass(mass - weight / 3);
    // Check if we are below
    if ((weight / 2) < mass)
      return 1 + balanceMass(weight - mass);
    return 1;
  }

  // Iterates to find a greater power of three for the given value
  static function getGreaterPowerOf3(value, exp) {
    var powerOf3 = Math.pow(3, exp);
    // Base case
    if (powerOf3 >= value)
      return exp;
    // Recursive case
    return getGreaterPowerOf3(value, exp + 1);
  }
}

/**
  $ cp jmesa85.hx Main.hx && cat DATA.lst | haxe -main Main --interp
  15 4 5 9 6 3 7 12 14 12 4 5 7 9 7 9 8 7 10 6 4 4
**/
