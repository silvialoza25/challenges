/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, char.

:- func char2int(char) = int.
char2int(Char) = Int :-
(
  if Char = '0' then Int = 0
  else if Char = '1' then Int = 1
  else Int = 2
).

:- func int2char(int) = char.
int2char(Int) = Char :-
(
  if Int = 0 then Char = '0'
  else Char = '1'
).

:- pred filterExp(char::in) is semidet.
filterExp(Char) :- Char \= '0'.

:- pred performOperation(char, int, char, int).
:- mode performOperation(in, in, out, out) is det.
performOperation(Char, Carry, NewChar, NewCarry) :-
(
  if (char2int(Char) + Carry) = 2
  then
    NewChar = 'n', %mercury doesnt like '-' and '-1' wouldnt fit
    NewCarry = 1
  else if (char2int(Char) + Carry) = 3
  then
    NewChar = '0',
    NewCarry = 1
  else
    NewChar = int2char((char2int(Char) + Carry)),
    NewCarry = 0
).

:- pred balanceTernary(list(char), int, list(char), list(char)).
:- mode balanceTernary(in, in, in, out) is det.
balanceTernary([], Carry, FinalList, FinalList ++ [int2char(Carry)]).
%if carry is 0 then it will get filtered out and wont affect the result
%this is done to maintain the deterministic declaration
balanceTernary([Char | Tail], Carry, PartialList, FinalList) :-
(
  performOperation(Char, Carry, NewChar, NewCarry),
  PartialList1 = PartialList ++ [NewChar],
  balanceTernary(Tail, NewCarry, PartialList1, FinalList)
).

:- pred findBalance(list(string), string, string).
:- mode findBalance(in, in, out) is det.
findBalance([], FinalStr, strip(FinalStr)).
findBalance([Line | Tail], PartialStr, FinalStr) :-
(
  det_to_int(Line) = Num,
  int_to_base_string(Num, 3) = UnbTernaryStr, %unbalanced ternary
  to_char_list(UnbTernaryStr, UnbTernaryList),
  reverse(UnbTernaryList, RevUnbTernaryList), %needed for balancing

  balanceTernary(RevUnbTernaryList, 0, [], RevBalTernaryList),
  filter(filterExp, RevBalTernaryList, ExpList), %filter out zeroes
  PartialStr1 = PartialStr ++ " " ++ from_int(length(ExpList)),
  findBalance(Tail, PartialStr1, FinalStr)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_index0(FileContents, 1, InputStr),
    words_separator(char.is_whitespace, InputStr) = InputList,
    findBalance(InputList, "", ResultStr),
    io.print_line(ResultStr, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  15 4 5 9 6 3 7 12 14 12 4 5 7 9 7 9 8 7 10 6 4 4
*/
