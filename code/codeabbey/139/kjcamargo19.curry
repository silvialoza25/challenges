-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import IO
import List
import Read

getData :: IO String
getData = do
  x <- getContents
  return x

getWeight :: Int -> Int -> Int
getWeight remI quotI
  | remI == 0 = quotI - 1
  | otherwise = quotI

exponents :: Int -> Int -> Int
exponents expo remI
  | remI /= 2 = expo + 1
  | otherwise = expo

amountMasses :: Int -> Int -> Int
amountMasses expo weight
  | weight == 0 = masses
  | otherwise = amountMasses newExp newWeight
  where
    masses = expo + 1
    remain = rem weight 3
    quoti = quot weight 3
    newExp = exponents expo remain
    newWeight = getWeight remain quoti

printRes :: Int -> String
printRes weight = show (amountMasses 0 (weight-1))

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    useData = map readInt $ split (==' ') $ (tail (init dataLines)) !! 0
    values = map (\x -> printRes x) useData
    finalResult = unwords values
  putStrLn (show finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 12 14 5 10 10 13 10 6 6 4 14 8 11 5 14 13 9
