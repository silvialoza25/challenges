{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  inputData <- getLine
  let intInputData = convert inputData
  let solution = map findSolution intInputData
  putStrLn (unwords solution)

findSolution :: Int -> String
findSolution dat = res
  where
    res = show (getExponents 0 (dat - 1))

getExponents :: Int -> Int -> Int
getExponents exp 0 = exp + 1
getExponents exp target = res
  where
    remain = target `rem` 3
    quotient = target `quot` 3
    nxtExp = if remain /= 2
               then exp + 1
               else exp
    nxtTarget = if remain == 0
                  then quotient - 1
                  else quotient
    res = getExponents nxtExp nxtTarget

convert :: String -> [Int]
convert dat = res
  where
    splitedData = words dat
    res = map read splitedData :: [Int]

{-
$ cat DATA.lst | ./bridamo98
  8 11 9 4 7 5 6 12 5 8 6 7 8 6 13 3 8 9 8 12 13
-}
