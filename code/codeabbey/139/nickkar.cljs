;; $clj-kondo --lint nickkar.cljs
;; linting took 84ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]
            [clojure.string :as str]))

(defn read-input []
  (into [] (map core/read-string (str/split (core/read-line) #" "))))

(defn to-b3 ([num] (to-b3 num ""))
  ([num out]
    (cond
      (<= num 0) out
      :else (to-b3 (quot num 3) (str (int (mod num 3)) out)))))

(defn find-weights [num]
  (cond
    (= (count (re-seq #"2" (to-b3 num))) 0) (count (re-seq #"1" (to-b3 num)))
    :else
    (let [b3rep (to-b3 num)
          i (.indexOf (apply str (reverse b3rep)) "2")
          newnum (+ num (- (Math/pow 3 (inc i)) (Math/pow 3 i)))]
      (find-weights newnum))))

(defn solve-cases ([cases nums] (solve-cases cases nums 0 []))
  ([cases nums i out]
    (cond
      (>= i cases) out
      :else
      (solve-cases cases nums (inc i) (conj out (find-weights (nth nums i)))))))

(defn main []
  (let [cases (core/read-string (core/read-line))
        nums (read-input)]
    (apply println (solve-cases cases nums))))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 8 11 13 7 5 8 10 11 14 13 8 2 5 10 10 12 13 14 14 5 13
