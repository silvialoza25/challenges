-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import IO (getContents)
import List (split, init)
import Read (readInt)

getData :: IO String
getData = do
  content <- getContents
  return content

strToF :: String -> Float
strToF text = read text :: Float

calculateRoot :: Float -> Float -> Float -> Float
calculateRoot constX nSteps root
  | nSteps == 0 = root
  | otherwise = calculateRoot constX (nSteps - 1) squareRoot
  where
    squareRoot = (root + (constX / root)) / 2

getSolution :: String -> Float
getSolution dataIn = result
  where
    finalData = map strToF (words dataIn)
    constX = finalData !! 0
    numberSteps = finalData !! 1
    result = calculateRoot constX numberSteps 1

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> show (getSolution x)) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 120.48893070809362 22.068076490713914 7.681145747868609 55.036351623268054
-- 94.84779798899743 48.16612521823069 80.72174428244226 116.51180738921052
-- 104.03432316661413 287.6716878665678 28.982753492378876 8.06225774829855
-- 8.062259639529445
