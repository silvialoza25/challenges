# elvish -compileonly asalgad2.elv

use str

fn filter_first [first @rest]{ put $rest }
fn get_tail [list]{ put $list[1..] }

fn aprox_sqrt [ const_x const_r ]{
  put ( / (+ $const_r (/ $const_x $const_r) ) 2 )
}

fn operate [const_x const_n const_r]{
  if (< $const_n 1) {
    put $const_r
  } else {
    put (operate $const_x (- $const_n 1) (aprox_sqrt $const_x $const_r))
  }
}

fn calculate [args]{
  if (eq (count $args) 0) {
    put ""
  } else {
    set @input = ( str:split ' ' $args[0] )
    operate $input[0] $input[1] 1
    calculate (get_tail $args)
  }
}

print (calculate (filter_first (all))) " "

# cat DATA.lst | elvish asalgad2.elv
# 9.219560764417094 6.164414002968977 102.6119657498932 63.28133150672288
# 51.22499389946279 56.71860364994895 7056.37481394713 69.78658198759415
# 6.7833397581298644 7.913071078802565 3960.5 231.11484426889004
