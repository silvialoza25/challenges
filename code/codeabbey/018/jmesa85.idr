{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #018: Square Root - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stod : String -> Double
stod s = fromMaybe 0 $ parseDouble s

elemAt : Int -> List a -> a
elemAt 0 (x::xs) = x
elemAt k (_::xs) = elemAt (k - 1) xs

getLines : IO(List String)
getLines = do
  x <- getLine
  if x == "" then pure []
    else do
      xs <- getLines
      pure (x :: xs)

heronsMethod : Double -> Double -> Double -> Double
heronsMethod valueX 0 root = root
heronsMethod valueX iterN root =
heronsMethod valueX (iterN - 1) ((root + valueX / root) / 2)

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse input array
    testCases = map (map stod) $ map words inputData
    -- Process data
    results = map (\[x, y] => heronsMethod x y 1) testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  89.4371287553441 3.872983346207417 10367.249975883275 57.42616678915661
  60.31430207036753 279.7463136487413 16.462077633155314 9.16515138991168
  32.43131362221127 4.358898943540673 259.25142513761125 289.89209521687604
  183.7486320109439
-}
