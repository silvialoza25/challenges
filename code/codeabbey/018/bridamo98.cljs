;; $ clj-kondo --lint bridamo98.cljs
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-018
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn calc-sqrt[x n]
  (loop [i n r 1]
    (if (> i 0)
      (let [ d (float (/ x r)) new-r (float (/ (+ r d) 2))]
          (recur (- i 1) new-r)) r)))

(defn solve-all[size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (let [ args (str/split (core/read-line) #" ")]
          (recur (+ i 1)
          (str result (calc-sqrt (edn/read-string (args 0))
          (edn/read-string (args 1))) " "))) result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;;69.91248 19.416561 339.60837 28.856234 273.569 105.80113 9.949875
;;159.90622 96.38983 81.81076 15.139088 268.25934 26.305893
