# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.05s to load, 0.04s running 55 checks on 1 file)
# 6 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule SquareRoot do
  def main do
    all_data = IO.read(:stdio, :all)
    try do
      get_data(all_data)
        |> Enum.map(&herons_method(&1, 1))
        |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_data(all_data) do
    split_data = String.split(all_data, "\n")
    List.delete_at(split_data, 1 +
      String.to_integer(Enum.at(split_data, 0)))
      |> Enum.map(&String.split(&1, " "))
      |> Enum.map(&to_int/1)
      |> tl()
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def herons_method([_, 0], r), do: r
  def herons_method([value, iteration], r) do
    herons_method([value, iteration - 1], 0.5 * (r + value / r))
  end
end

SquareRoot.main()

# cat DATA.lst | elixir ludsrill.exs
# 21.968304239299798 227.09524110368295 98.93937557953195 6.855654600401045 8.0
# 1011.2497525365009 96.14112653638028 109.43034314119645 223.82583019125414
# 73.87827826905551 5454.248976571302
