# $ lintr::lint('nickkar.r')

options(digits = 15)

squareroot <- function(x, n, r=1, i=1) {
    if (i > n) {
        return(r)
    }
    else {
        newr <- (r + (x / r)) / 2
        squareroot(x, n, newr, (i + 1))
    }
}

solveca5es <- function(cases, out = c(), i=1) {
    if (i > length(cases)) {
        return(out)
    }
    else {
        tmp <- unlist(strsplit(cases[i], " "))
        num1 <- as.double(tmp[1])
        num2 <- as.double(tmp[2])
        r <- squareroot(num1, num2)
        solveca5es(cases, (append(out, r)), i + 1)
    }
}

main <- function() {
    data <- readLines("stdin")
    out <- solveca5es(data[2:length(data)])
    cat(out)
    cat("\n")
}

main()

# $ cat DATA.lst | rscript nickkar.r
# 69.9124842494887 19.41655996644 339.608388710881 28.8562324753559
# 273.569004092258 105.801126249682 9.9498743710662 159.906222518075
# 96.3898336028984 81.8107572388864 15.1390876723858 268.259328438139
# 26.3058928759318
