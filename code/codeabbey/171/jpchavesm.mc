/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, float, math, char.

:- pred str2float(string::in, float::out) is det.
str2float(X, string.det_to_float(X)).

:- func deg2rad(float) = float.
deg2rad(Deg) = Deg * pi / 180.0.

:- pred findHeight(float, float, int).
:- mode findHeight(in, in, out) is det.
findHeight(Distance, AngleB,
  round_to_int(Distance * tan(deg2rad(AngleB - 90.0)))).

:- pred measureTrees(list(string), string, string).
:- mode measureTrees(in, in, out) is det.
measureTrees([], FinalStr, strip(FinalStr)).
measureTrees([Line | Tail], PartialStr, FinalStr) :-
(
  words_separator(char.is_whitespace, Line) = StrList,
  map(str2float, StrList, FloatList),
  det_index1(FloatList, 1) = Distance,
  det_index1(FloatList, 2) = AngleB,
  findHeight(Distance, AngleB, Height),
  PartialStr1 = PartialStr ++ " " ++ from_int(Height),
  measureTrees(Tail, PartialStr1, FinalStr)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1,FileContents, MeasureList),
    measureTrees(MeasureList, "", ResultStr),
    io.print_line(ResultStr, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  70 59 18 15 43 48 59 15 59 70 62 25 48
*/
