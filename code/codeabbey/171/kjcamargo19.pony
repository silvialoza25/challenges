// ponyc -b kjcamargo19

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun iterator(data: Array[String]): Array[String] =>
          data.slice(1, data.size())

        fun get_height(distance: F64, angle_b: F64): F64 =>
          var const_pi:F64 = 1
          var result: F64 = (angle_b - 90) * (const_pi.pi() / 180)
          (distance * result.tan()).round()

        fun obtain_data(data: Array[String])? =>
          if data.size() > 0 then
            var line: Array[String] = data(0)?.split_by(" ")
            var distance: F64 = line(0)?.f64()?
            var angle_b: F64 = line(1)?.f64()?

            var result: F64 = get_height(distance,angle_b)
            env.out.write(result.string() + " ")
            obtain_data( iterator (data))?
          end

        fun ref apply(data: Array[U8] iso) =>
          var input_data: String = String.from_iso_array(consume data)
          var line_sep: Array[String] = input_data.split_by("\n")
          var real_data: Array[String] = line_sep.slice(1, line_sep.size()-1)

          try
            obtain_data(real_data)?
          end

        fun ref dispose() =>
          None

      end,
      512
    )

// cat DATA.lst | ./kjcamargo19
// 33 65 30 54 59 46 20 38 32 37 55 35 41 22
