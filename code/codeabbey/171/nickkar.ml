(*
$ ocamlc -w @0..1000 nickkar.ml
*)

open Printf
open List

(* divides the input lines to readeble separated parts *)
let splitting line =
  map float_of_string (String.split_on_char ' ' line)

(* reads the input and returns them in a vector*)
let rec read_num num_cases index out =
  if index >= num_cases-1 then [out]
  else out :: read_num num_cases (index+1) (splitting (read_line()))

(* calculates the height of the trees and prints them *)
let prnt case =
  printf "%d "
  (int_of_float (Float.round (Float.mul (Float.tan (Float.div (Float.mul
  (Float.sub (nth case 1) 90.0) Float.pi) 180.0)) (nth case 0))))

(* driver code *)
let num_cases = read_int()
let() =
  iter prnt (read_num num_cases 0 (splitting (read_line())));
  print_string "\n"

(*
$ cat DATA.lst | ocaml nickkar.ml
70 59 18 15 43 48 59 15 59 70 62 25 48
*)
