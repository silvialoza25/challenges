;$ clj-kondo --lint frank1030.cljs
;linting took 584ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core]))

(defn tree-height [index]
  (let [x (atom index) d (core/read) b (- (core/read) 90)
    height (* (Math/tan (* b (/ 3.14 180))) d) res (Math/round height)]
    (if (> @x 0)
      (do (swap! x dec)
        (print res "")
        (if (not= @x 0)
          (tree-height @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (core/read)]
    (tree-height index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;29 47 64 29 29 58 54 27 48 25 15 51 47
