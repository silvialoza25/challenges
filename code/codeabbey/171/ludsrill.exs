# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.06 seconds (0.01s to load, 0.05s running 55 checks on 1 file)
# 5 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule TreeHeightMeasurement do
  def main do
    all_data = IO.read(:stdio, :all)
    try do
      get_data(all_data)
        |> Enum.map(&measurement/1)
        |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_data(all_data) do
    split_data = String.split(all_data, "\n")
    List.delete_at(split_data, 1 +
      String.to_integer(Enum.at(split_data, 0)))
      |> Enum.map(&String.split(&1, " "))
      |> Enum.map(&to_float/1)
      |> tl()
  end

  def to_float(list) do
    Enum.map(Enum.map(Enum.map(list, &Float.parse/1), &Tuple.to_list/1),
      &List.first/1)
  end

  def measurement([d, b]) do
    Kernel.round(d / :math.tan((180 - b) * :math.pi() / 180))
  end
end

TreeHeightMeasurement.main()

# cat DATA.lst | elixir ludsrill.exs
# 70 59 18 15 43 48 59 15 59 70 62 25 48
