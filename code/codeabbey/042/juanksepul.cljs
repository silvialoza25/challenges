; clj-kondo --lint juanksepul.cljs
; linting took 222ms, errors: 0, warnings: 0

(ns codeabbey.juanksepul
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn read-data []
  (str/split-lines (core/slurp core/*in*)))

(defn filt-line [line]
  [(re-seq #"[B-Z]" line)
   (re-seq #"[0-9]" line)
   (re-seq #"[A]" line)])

(defn sum-l [letters]
  (if-not (= letters nil) (* 10 (count letters)) 0))

(defn cast-n [nums]
  (map edn/read-string nums))

(defn sum-n [nums]
  (if-not (= nums nil) (reduce + (cast-n nums)) 0))

(defn sum-a [pre-cnt aces]
  (+ pre-cnt (* 11 (count aces))))

(defn sub-a [cum-cnt]
  (if (> cum-cnt 21) (- cum-cnt 10) cum-cnt))

(defn print-res [fin-cnt]
  (if (> fin-cnt 21)
    (print "Bust ")
    (print (str fin-cnt " "))))

(defn cnt-pts [lets nums aces]
  (if (= aces nil)
    (+ (sum-l lets) (sum-n nums))
    (loop
     [cum (sum-a (+ (sum-l lets) (sum-n nums)) aces)
      a-num (count aces)]
      (if (= a-num 0) cum
          (recur (sub-a cum) (dec a-num))))))

(defn !main []
  (doseq [line (rest (read-data))]
    (let [[lets nums aces] (filt-line line)]
      (print-res (cnt-pts lets nums aces)))))

(!main)

; cat DATA.lst | clj juanksepul.cljs
; 16 17 18 20 21 18 Bust Bust 21 19 18 21 Bust 17 16 16
; 18 Bust 17 16 21 Bust 16 21 21 19 21 17 17
