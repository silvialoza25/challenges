#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defvar *data*)
(defvar *item*)
(defvar *accumulator*)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect line)))

(defun split-by-one-space (item)
  (loop for i = 0 then (1+ j)
    as j = (position #\Space item :start i)
    collect (subseq item i j)
    while j))

(setq *accumulator* 0)
(setq *data* (cdr (read-data)))

(dolist (*item* *data*)
  (setq aux-item-split (split-by-one-space *item*))
  (loop for i in aux-item-split
    do(cond ((string= i '"A") (cond ((> *accumulator* 10) (setq a 1))
            ((<= *accumulator* 10) (setq a 11))))
            ((string= i '"J") (setq a 10))
            ((string= i '"Q") (setq a 10))
            ((string= i '"K") (setq a 10))
            ((string= i '"T") (setq a 10))
            ((string= i '"2") (setq a 2))
            ((string= i '"3") (setq a 3))
            ((string= i '"4") (setq a 4))
            ((string= i '"5") (setq a 5))
            ((string= i '"6") (setq a 6))
            ((string= i '"7") (setq a 7))
            ((string= i '"8") (setq a 8))
            ((string= i '"9") (setq a 9)))

    (setq *accumulator* (+ *accumulator* a)))

  (cond ((> *accumulator* 21) (print 'Bust))
        ((<= *accumulator* 21) (format t "~s " *accumulator*)))
  (setq *accumulator* 0))

#|
cat DATA.lst | clisp ludsrill.lsp
16 21 18 21 21 19 21 18 17 20 17 17
BUST 16 21 17 16 16 16 19 BUST 20 16
|#
