{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

toIntegerNat : Nat -> Int
toIntegerNat Z = 0
toIntegerNat (S k) = 1 + toIntegerNat k

injectValue : a -> IO a
injectValue dat = pure dat

getInputData : IO(List String)
getInputData = do
  line <- getLine
  if line == ""
    then injectValue []
    else do
      xs <- getInputData
      injectValue (line :: xs)

getValue : String -> Int
getValue card =
  case card of
    "A" => 11
    "2" => 2
    "3" => 3
    "4" => 4
    "5" => 5
    "6" => 6
    "7" => 7
    "8" => 8
    "9" => 9
    "T" => 10
    "J" => 10
    "Q" => 10
    "K" => 10

count : List String -> Int
count [] = 0
count cards =
  let
    iCard = fromMaybe "" (head' cards)
    iValue = getValue iCard
  in iValue + (count (drop 1 cards))

fixAces : Int -> Int -> String
fixAces value 0 =
  if value > 21
    then "Bust"
    else show value
fixAces value aces =
  let
    res =
      if value > 21
        then fixAces (value - 10) (aces - 1)
        else show value
  in res

blackjackCounting : List String -> String
blackjackCounting cards =
  let
    fstValue = count cards
    res = fixAces fstValue
      (toIntegerNat (length (filter (=="A") cards)))
  in res

main : IO ()
main = do
  inputData <- getInputData
  let
    hands = map words (drop 1 inputData)
    solution = map blackjackCounting hands
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  16 20 21 18 17 20 21 17 18 21 17 20 19 Bust 21
  21 17 17 21 16 Bust 21 21 21 16 21 17 17 18 Bust
-}
