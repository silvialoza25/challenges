/**
  $ haxelib run checkstyle \
    --exitcode \
    --show-parser-errors \
    --nothreads \
    --default-config \
    -s jmesa85.hx
**/

// CodeAbbey #042: Blackjack Counting - Haxe

using StringTools;
using Lambda;

class Main {
  static public function main():Void {
    // Read Stdin
    var data = Sys.stdin().readAll().toString().trim();
    // Get test cases
    var testCases = data.split("\n").slice(1);
    // Process test cases
    var results = testCases.map(processTestCase);
    // Print results
    trace(results.join(" "));
  }

  static private function processTestCase(testCase:String):String {
    // In order to ease counting, take first ace to the end
    var sorted = sortFirstAce(testCase.split(" "));
    // Count cards
    var count:Int = sorted.fold(countCards, 0);
    // Return count
    if (count <= 21) return Std.string(count);
    else return "Bust";
  }

  // Reducer for counting
  static private function countCards(card:String, acc:Int) {
    switch (card) {
      case "T", "J", "Q", "K":
        return acc + 10;
      case "A":
        if (acc <= 10) return acc + 11;
        return acc + 1;
      default:
        return acc + Std.parseInt(card);
    }
  }

  static private function sortFirstAce(cards:Array<String>):Array<String> {
    var aceIndex = cards.indexOf("A");
    if (aceIndex != -1) {
      var leftArr = cards.slice(0, aceIndex);
      var rightArr = cards.slice(aceIndex + 1);
      return leftArr.concat(rightArr).concat(["A"]);
    }
    return cards;
  }
}

/**
  $ cp jmesa85.hx Main.hx && cat DATA.lst | haxe -main Main --interp
  20 Bust 20 19 19 20 16 16 18 Bust 21 19 18 19 16 Bust 16 20 21 17
  20 20 16 16 19 16 17 19 19
**/
