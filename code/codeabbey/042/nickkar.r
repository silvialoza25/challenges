# $ lintr::lint('nickkar.r')

# changes to the numeric representation of the cards
num_repr <- function(str) {
  switch(str, "A" = 11, "T" = 10, "J" = 10, "Q" = 10, "K" = 10, as.integer(str))
}

# calculates if the sum of the car5ds could be 21 or less if the aces values
# change from 11 to 1
solve_aces <- function(cards, sum, index=1) {
  if (index > length(cards[cards == "A"])) {
    return(sum)
  }
  else {
    if (sum <= 21) {
      return(sum)
    }
    else {
      solve_aces(cards, sum - 10, index + 1)
    }
  }
}

# checks if the cards sum is less or equal than 21, if not call the respective
# functions if the condition becomes true when changing the aces values from
# 11 to 1, returns Bust if the condition never becomes true
solve_case <- function(cards) {
  num_rep <- sapply(cards, num_repr)
  if (sum(num_rep) <= 21) {
    return(sum(num_rep))
  }
  else {
    tmp <- solve_aces(cards, sum(num_rep))
    if (tmp <= 21) {
      return(tmp)
    }
    else {
      return("Bust")
    }
  }
}

# iterates over every case
solve_cases <- function(cases, out = c(), index=1) {
  if (index > length(cases)) {
    return(out)
  }
  else {
      tmp <- unlist(strsplit(cases[index], " "))
      solve_cases(cases, c(out, solve_case(tmp)), index + 1)
  }
}

# Driver code
main <- function() {
  data <- readLines("stdin")
  out <- solve_cases(data[2:length(data)])
  cat(out, "\n")
}

main()

# $ cat DATA.lst | Rscript nickkar.r
# 16 17 18 20 21 18 Bust Bust 21 19 18 21 Bust 17 16 16 18 Bust 17 16 21 Bust
# 16 21 21 19 21 17 17
