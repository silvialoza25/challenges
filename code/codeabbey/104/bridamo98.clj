;; $ clj-kondo --lint bridamo98.clj
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-104
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn ** [x n] (reduce *
(repeat (core/bigint n) (core/bigint  x))))

(defn distance[X1 Y1 X2 Y2]
  (let [fst (** (- X2 X1) 2) snd (** (- Y2 Y1) 2)]
    (Math/sqrt (+ fst snd))))

(defn find-area[X1 Y1 X2 Y2 X3 Y3]
  (let [a (distance X1 Y1 X2 Y2) b (distance X2 Y2 X3 Y3)
  c (distance X3 Y3 X1 Y1) s (/ ( + a b c) 2)]
    (Math/sqrt (* s (-  s a) (-  s b) (-  s c)))))

(defn solve-all[size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (let [ args (str/split (core/read-line) #" ")]
          (recur (+ i 1)
          (str result (find-area (edn/read-string (args 0))
          (edn/read-string (args 1)) (edn/read-string (args 2))
          (edn/read-string (args 3)) (edn/read-string (args 4))
          (edn/read-string (args 5))) " ")))
          result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 2058824.999999994 408448.50000003 1962019.9999999907 8166047.999999994
;; 4313036.000000035 1.1631530500000006E7 1.107204900000001E7
;; 4688396.000000001 3598616.5000000023 1.1097630000000004E7 4977228.499999996
;; 2206271.5000000037 7792157.499999996 9652407.0
