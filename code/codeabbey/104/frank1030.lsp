#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

$ clisp -c frank1030.lsp
Compiling file /Users/Frank/Desktop/frank/104/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/104/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-index()
  (let ((dat))
  (setq dat(read))
  )
)

(defun triangle-area()
  (let ((area 0) (x1 0) (x2 0) (x3 0) (y1 0) (y2 0) (y3 0) (index 0)
  (a 0) (b 0) (c 0) (s 0))
    (setq index(get-index))
    (loop for i from 0 to (- index 1)
      do (setq x1(read))
      do (setq y1(read))
      do (setq x2(read))
      do (setq y2(read))
      do (setq x3(read))
      do (setq y3(read))
      do (setq a(sqrt (+ (* (- x2 x1) (- x2 x1)) (* (- y2 y1) (- y2 y1)))))
      do (setq b(sqrt (+ (* (- x2 x3) (- x2 x3)) (* (- y2 y3) (- y2 y3)))))
      do (setq c(sqrt (+ (* (- x1 x3) (- x1 x3)) (* (- y1 y3) (- y1 y3)))))
      do (setq s(/ (+ a b c) 2))
      do (setq area(sqrt(* s (- s a) (- s b) (- s c))))
      do (format t "~,1f " area)
    )
  )
)

(triangle-area)

#|
$ cat DATA.lst | clip frank1030.lsp
2058821
408456
1961997.5
8166049.5
4313035
11631535
11072046
4688396.5
3598619
11097632
4977227
2206272
7792161
9652407
|#
