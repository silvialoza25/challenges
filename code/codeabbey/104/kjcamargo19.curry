-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import Float
import IO
import List

getData :: IO String
getData = do
  content <- getContents
  return content

strToF :: String -> Float
strToF s = read s :: Float

getDistance :: Float -> Float -> Float -> Float -> Float
getDistance constX1 constX2 constY1 constY2 = result
  where
    termOne = (constX2 - constX1) ^. 2
    termTwo = (constY2 - constY1) ^. 2
    result = sqrt (termOne + termTwo)

calArea :: Float -> Float -> Float -> Float -> Float
calArea legA legB legC semiPerimeter = area
  where
    semiPerA = semiPerimeter - legA
    semiPerB = semiPerimeter - legB
    semiPerC = semiPerimeter - legC
    area = sqrt (semiPerimeter * semiPerA * semiPerB * semiPerC)

getArea :: Float -> Float -> Float -> Float -> Float -> Float -> Float
getArea constX1 constY1 constX2 constY2 constX3 constY3 = area
  where
    legA = getDistance constX1 constX2 constY1 constY2
    legB = getDistance constX2 constX3 constY2 constY3
    legC = getDistance constX3 constX1 constY3 constY1
    semiPerimeter = (legA + legB + legC) / 2
    area = calArea legA legB legC semiPerimeter

getSolution :: String -> Float
getSolution dataIn  = result
  where
    fData = map strToF (words dataIn)
    constX1 = fData !! 0
    constY1 = fData !! 1
    constX2 = fData !! 2
    constY2 = fData !! 3
    constX3 = fData !! 4
    constY3 = fData !! 5
    result = getArea constX1 constY1 constX2 constY2 constX3 constY3

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> show (getSolution x)) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 2541642.999999993 23366.00000003891 2209384.000000021 10430853.499999996
-- 6771645.999999988 12544807.999999994 571699.4999999992 7674792.999999996
-- 12333969.000000013 27632824.499999996 20906584.999999985 9112799.999999996
-- 21798541.500000007 22813112.00000001 2026933.999999998 5048773.499999998
