/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, float, char.

:- func str2Vertex(string) = list(float).
str2Vertex(InputStr) = Coords :-
(
  string.words_separator(char.is_whitespace, InputStr) = StrCoords,
  det_to_float(det_index0(StrCoords, 0)) = PosX1,
  det_to_float(det_index0(StrCoords, 1)) = PosY1,
  det_to_float(det_index0(StrCoords, 2)) = PosX2,
  det_to_float(det_index0(StrCoords, 3)) = PosY2,
  det_to_float(det_index0(StrCoords, 4)) = PosX3,
  det_to_float(det_index0(StrCoords, 5)) = PosY3,
  Coords = [PosX1, PosY1, PosX2, PosY2, PosX3, PosY3]
).

:- func triangleArea(list(float)) = float.
triangleArea(Coords) = Area :-
(
  det_index0(Coords, 0, PosX1),
  det_index0(Coords, 1, PosY1),
  det_index0(Coords, 2, PosX2),
  det_index0(Coords, 3, PosY2),
  det_index0(Coords, 4, PosX3),
  det_index0(Coords, 5, PosY3),
  Area = abs(PosX1*PosY2 + PosX2*PosY3 + PosX3*PosY1 -
    PosX1*PosY3 - PosX2*PosY1 - PosX3*PosY2) / 2.0
).

:- pred findArea(list(string), string, string).
:- mode findArea(in, in, out) is det.
findArea([], FinalArea, FinalArea).
findArea([Line | Tail], PartialArea, FinalArea) :-
(
  str2Vertex(Line) = Coords,
  triangleArea(Coords) = TriangleArea,
  PartialArea1 = PartialArea ++ " " ++ from_float(TriangleArea),
  findArea(Tail, PartialArea1, FinalArea)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
                   list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1, FileContents, ComputeList),
    findArea(ComputeList, "", FinalArea),
    io.print_line(FinalArea, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  6645012.0 7106200.5 5389294.0 857773.5 3207274.0 2013786.0 4701292.0
  7469806.0 8668547.5 1022973.0 6396648.0 10912317.0 4919773.0 1773106.5
  9890899.5
*/
