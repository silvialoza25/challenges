/**
  $ haxelib run checkstyle \
    --exitcode \
    --show-parser-errors \
    --nothreads \
    --default-config \
    -s jmesa85.hx
**/

// CodeAbbey #104: Triangle Area - Haxe

using StringTools;

class Main {
  static public function main():Void {
    // Read Stdin
    var data = Sys.stdin().readAll().toString().trim().split("\n");
    // Get the test cases
    var testCasesStr:Array<String> = data.slice(1);
    // Process each test case
    var results:Array<String> = testCasesStr.map(processTestCase);
    trace(results.join(" "));
  }

  static private function processTestCase(coordinatesStr:String):String {
    var coordinates = coordinatesStr.split(' ').map(Std.parseInt);
    // Destructuring assignment not available
    var x1 = coordinates[0];
    var y1 = coordinates[1];
    var x2 = coordinates[2];
    var y2 = coordinates[3];
    var x3 = coordinates[4];
    var y3 = coordinates[5];
    // Apply formula for Area of a Convex Polygon using the determinant
    var area = Math.abs(
      ((x1 * y2 + x2 * y3 + x3 * y1) - (x2 * y1 + x3 * y2 + x1 * y3)) / 2);
    return Std.string(area);
  }
}

/**
  $ cp jmesa85.hx Main.hx && cat DATA.lst | haxe -main Main --interp
  2058825 408448.5 1962020 8166048 4313036 11631530.5 11072049 4688396
  3598616.5 11097630 4977228.5 2206271.5 7792157.5 9652407
**/
