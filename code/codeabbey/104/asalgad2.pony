// ponyc -b asalgad2

use "format"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail( array: Array[String] ): Array[String] =>
          array.slice(1, array.size())

        fun distance( const_x1: F64, const_y1: F64,
                      const_x2: F64, const_y2: F64 ): F64 =>
          ((const_x1 - const_x2).pow(2) + (const_y1 - const_y2).pow(2)).sqrt()

        fun format_float( number: F64, precision: USize ): String =>
          Format.float[F64](number where fmt=FormatFixLarge, prec=precision)

        fun iterate( lines: Array[String] ) ? =>
          if lines.size() > 0 then
            var values: Array[String] = lines(0)?.split(" ")
            var const_x1: F64 = values(0)?.f64()?
            var const_y1: F64 = values(1)?.f64()?
            var const_x2: F64 = values(2)?.f64()?
            var const_y2: F64 = values(3)?.f64()?
            var const_x3: F64 = values(4)?.f64()?
            var const_y3: F64 = values(5)?.f64()?

            var side1: F64 = distance( const_x1, const_y1, const_x2, const_y2 )
            var side2: F64 = distance( const_x2, const_y2, const_x3, const_y3 )
            var side3: F64 = distance( const_x3, const_y3, const_x1, const_y1 )

            var perim: F64 = (side1 + side2 + side3) / 2

            var sub_perim1: F64 = perim - side1
            var sub_perim2: F64 = perim - side2
            var sub_perim3: F64 = perim - side3
            var area:F64=(perim*((sub_perim1)*(sub_perim2)*(sub_perim3))).sqrt()

            env.out.write( format_float(area, 1) )
            env.out.write( " " )

            iterate( tail(lines) )?
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")
          var useful_lines: Array[String] = lines.slice(1, lines.size()-1)

          try
            iterate( useful_lines )?
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// 5628970.0 959954.0 11509770.0 3438899.5 1598130.5 633678.0 3331557.5
// 4277532.5 3914078.0 1617781.5 9794253.0 707293.5 2914272.5 1456895.0
