/**
  $ haxelib run checkstyle \
    --exitcode \
    --show-parser-errors \
    --nothreads \
    --default-config \
    -s jmesa85.hx
**/

/*
  CodeAbbey #150: Introducing Regexps - Haxe
  Note: CodeAbbey expects a "especially formatted" regex as code solution.
  The Haxe code applies these regex and generates the expected answer:
  ^[01]+[Bb]$|^0[bB][01]+$ bin
  ^0[Xx][\dA-Fa-f]+$|^[\d]+[\dA-Fa-f]*[hH]$ hex
  ^0[0-7]+$ oct
  ^\d+$ dec
*/

using StringTools;

class Main {
  static public function main():Void {
    // Read Stdin
    var data = Sys.stdin().readAll().toString().trim().split("\n");
    // Calculate output data
    var results = data.map(processTestCase);
    // Print results
    trace(results.join(" "));
  }

  // Test for regex matching
  static function processTestCase(strToMatch:String):String {
    if (~/^[01]+[Bb]$|^0[bB][01]+$/.match(strToMatch))
      return "bin"; // 0b0101 or 0101b
    if (~/^0[Xx][\dA-Fa-f]+$|^[\d]+[\dA-Fa-f]*[hH]$/.match(strToMatch))
      return "hex"; // 0xFFFF or FFFFX
    if (~/^0[0-7]+$/.match(strToMatch))
      return "oct"; // 0777, leading zero
    if (~/^\d+$/.match(strToMatch))
      return "dec"; // Decimal
    else
      return strToMatch;
  }
}

/**
  $ cp jmesa85.hx Main.hx && cat DATA.lst | haxe -main Main --interp
  bin bin dec hex hex hex oct bin bin hex hex hex oct bin f0bh hex hex
  bin hex hex oct 2022000b 2B18 hex c8 2211121B bin bin dec
**/
