;; $ clj-kondo --lint nickkar.cljs
;; linting took 51ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(defn binary-rep [bin]
  (let [rep (hash-map :11 " " :101 "e"
                         :011 "a" :0011 "h"
                         :0101 "s" :1001 "t"
                         :10001 "o" :10000 "n"
                         :01001 "i" :01000 "r"
                         :00011 "u" :00101 "d"
                         :001001 "l" :001000 "!"
                         :000101 "c" :000100 "f"
                         :000011 "m" :0000101 "p"
                         :0000100 "g" :0000011 "w"
                         :0000010 "b" :0000001 "y"
                         :00000001 "v" :000000001 "j"
                         :0000000001 "k" :00000000001 "x"
                         :000000000001 "q" :000000000000 "z")]
    (get rep (keyword bin))))

(defn dec-32 [enc32]
  (let [rep (hash-map :0 "00000" :1 "00001"
                      :2 "00010" :3 "00011"
                      :4 "00100" :5 "00101"
                      :6 "00110" :7 "00111"
                      :8 "01000" :9 "01001"
                      :A "01010" :B "01011"
                      :C "01100" :D "01101"
                      :E "01110" :F "01111"
                      :G "10000" :H "10001"
                      :I "10010" :J "10011"
                      :K "10100" :L "10101"
                      :M "10110" :N "10111"
                      :O "11000" :P "11001"
                      :Q "11010" :R "11011"
                      :S "11100" :T "11101"
                      :U "11110" :V "11111")]
    (get rep (keyword (str enc32)))))

(defn merge-bits ([input] (merge-bits input 0 ""))
  ([input i out]
   (cond
     (>= i (count input)) out
     :else (merge-bits input (inc i) (str out (nth input i))))))

(defn unpack ([bits] (unpack bits 1 ""))
  ([bits i out]
   (cond
     (> i (count bits)) out
     :else (let [testbits (merge-bits (take i bits))
                 letter (binary-rep testbits)]
             (cond
               (= letter nil) (unpack bits (inc i) out)
               :else (let [newbits (merge-bits (drop i bits))]
                       (unpack newbits 0 (str out letter))
                       )
               )
             )
     )
   ))

(defn main []
  (let [input (core/read-line)
        bin-inp (map dec-32 input)
        merged (merge-bits bin-inp)
        unpacked (unpack merged)]
    (println unpacked)
    ))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; world !he has refused his !assent to !laws the most wholesome and necessary
;; for waging !war against us !he has plundered our seas ravaged our coasts
;; burnt as to them shall seem most likely to effect their !safety and
;; !happiness
