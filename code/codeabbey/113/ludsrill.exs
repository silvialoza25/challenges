# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.08 seconds (0.02s to load, 0.06s running 55 checks on 1 file)
# 9 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule GroundZero do

  def main do
    all_data = IO.read(:stdio, :all)
    try do
      get_data(all_data)
        |> Enum.map(&found_ground_zero(&1, 0, 0))
        |> Enum.map(&Enum.map(&1, fn item -> IO.write("#{item} ") end))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
      |> tl()
      |> Enum.map(&tl(String.split(&1, "; ")))
      |> Enum.map(&Enum.map(&1, fn item ->
          to_float(String.split(item, " ")) end))
      |> Enum.reject(fn item -> item == [] end)
  end

  def to_float(list) do
    Enum.map(Enum.map(Enum.map(list, &Float.parse/1), &Tuple.to_list/1),
      &List.first/1)
  end

  def found_ground_zero(data, x_position, y_position) do
    max_yposition = 10_000
    max_xposition = 10_000
    distance = Enum.map(data, fn sensor -> euclidian_distance(x_position,
      Enum.at(sensor, 0), y_position, Enum.at(sensor, 1)) end)

    time = calculate_time(Enum.at(Enum.at(data, 0), 2),
      Enum.at(Enum.at(data, 1), 2), Enum.at(distance, 0), Enum.at(distance, 1))

    speed = calculate_speed(distance, data, time, [])

    if (verify_speed(speed) and time > 0.0 and
      time < Enum.at(Enum.at(data, 0), 2)) do
      [x_position, y_position, Float.round(time, 1)]
    else
      if y_position < max_yposition do
        found_ground_zero(data, x_position, y_position + 1)
      else
        if x_position < max_xposition do
          found_ground_zero(data, x_position + 1, 0)
        else
          [0.0, 0.0, 0.0]
        end
      end
    end
  end

  def calculate_time(timeSensor1, timeSensor2, distanceSensor1,
      distanceSensor2) do
      rescue_time = 10_000.0
    if distanceSensor1 != distanceSensor2 do
      (timeSensor2 * distanceSensor1 - timeSensor1 * distanceSensor2) /
        (distanceSensor1 - distanceSensor2)
    else
      rescue_time
    end
  end

  def calculate_speed(distance, data, time, acc) do
    rescue_speed = 10_000.0
    if Enum.empty?(distance) do
      acc
    else
      try do
        speed = Enum.at(distance, 0) / (Enum.at(Enum.at(data, 0), 2) - time)
        calculate_speed(tl(distance), tl(data), time, acc ++ [speed])
      rescue
        _ -> calculate_speed(tl(distance), tl(data), time,
          acc ++ [rescue_speed])
      end
    end
  end

  def euclidian_distance(new_x, sensor_x, new_y, sensor_y) do
    :math.sqrt(:math.pow(new_x - sensor_x, 2) + :math.pow(new_y - sensor_y, 2))
  end

  def verify_speed(speed) do
    max_err_speed = 0.01
    if length(speed) == 1 do
      true
    else
      if abs(Enum.at(speed, 0) - Enum.at(speed, 1)) < max_err_speed do
        verify_speed(tl(speed))
      else
        false
      end
    end
  end
end

GroundZero.main()

# cat DATA.lst | elixir ludsrill.exs
# 6213 9074 94.3 5861 9222 90.5 969 5071 51.5 2303 3072 26.9 56 3915 99.2
# 9078 3928 42.9 3324 8723 36.2 4476 1228 15.6
