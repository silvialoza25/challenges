-- $ ghc -o nickkar nickkar.hs
-- [1 of 1] Compiling Main ( nickkar.hs, nickkar.o )
-- Linking nickkar ...
-- $ hlint nickkar.hs
-- No hints

import Control.Monad ( replicateM, forM )
import Data.List
import Data.Maybe
import Text.Printf

convert::String -> [Int]
convert = map read . words

ins :: Int -> [Int] -> [Int]
ins x l = let (lower, greater) = span (< x) l in lower ++ x : greater

insertionsort:: [Int] -> [Int] -> [Int] -> Int -> [Int]
insertionsort list slist shifts i
    | slist == sort list = shifts
    | otherwise = insertionsort
    list
    nslist
    (shifts ++
    [(length shifts - fromMaybe (-1) (elemIndex (list!!i) nslist)) + 1]
    )
    (i+1)
    where
        nslist = ins (list!!i) slist


main::IO()
main = do
    n <- readLn :: IO Int
    input <- fmap (map read.words) getLine :: IO [Int]
    let out = insertionsort input [head input] [] 1
    mapM_ (printf "%d ") out
    putStr "\n"

-- $ cat DATA.lst | ./nickkar
-- 1 2 3 3 4 5 4 3 2 4 9 8 1 10 12 14 1 17 14 19 19 16 10 0 7 11 11 9 14 15 8
-- 6 3 14 24 16 19 34 4 0 24 5 7 36 45 13 34 35 9 33 30 37 37 43 3 0 57 32 18
-- 25 26 35 39 17 61 52 8 57 41 0 30 59 70 34 64 48 68 11 37 19 55 49 11 35 54
-- 75 22 77 46 89 42 39 44 66 10 71 69 12 29 6 47 47 94 94 48 75 59 38 53 30
-- 109 35 3 69 49 11 49 26 27 109 67 97
