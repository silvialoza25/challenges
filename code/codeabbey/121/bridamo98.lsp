#|
  $ sblint -v bridamo98.lsp
  $ [INFO] Lint file bridamo98.lsp
|#

(defun solve(input limit maxi cont aux iterator flag i)
  (loop
    (if (> (aref input i) maxi)
      (progn
        (setq maxi (aref input i))
        (format t "~a " cont)
      )
      (progn
        (setq aux (aref input i))
        (setq iterator i)
        (setq flag 1)
        (loop
          (if (= iterator 0)
            (progn
              (setf (aref input iterator) aux)
              (setq flag 0)
            )
            (progn
              (if (< (aref input (- iterator 1)) aux)
                (progn
                  (setf (aref input iterator) aux)
                  (setq flag 0)
                )
                (progn
                  (setf (aref input iterator) (aref input (- iterator 1)))
                  (setq iterator (- iterator 1))
                  (setq cont (+ cont 1))
                )
              )
            )
          )
          (when (= flag 0) (return flag))
        )
        (format t "~a " cont)
        (setq cont 0)
      )
    )
    (setq i (+ i 1))
  (when (> i limit) (return i))
  )
)

(defun get_data(x number input)
  (setq number (read))
    (setf (aref input x) number)
)

(defvar size_input (read))
(defparameter input (make-array size_input))
(defvar number 0)
(defvar limit (- size_input 1))
(defvar cont 0)
(defvar aux 0)
(defvar iterator 0)
(defvar flag 0)
(defvar i 1)

(loop for x from 0 to (- size_input 1)
  do(get_data x number input)
)

(defvar maxi (aref input 0))

(solve input limit maxi cont aux iterator flag i)

#|
  cat DATA.lst | clisp bridamo98.lsp
  1 2 3 3 4 5 4 3 2 4 9 8 1 10 12 14 1 17 14 19 19 16 10 0 7 11 11 9 14 15 8
  6 3 14 24 16 19 34 4 0 24 5 7 36 45 13 34 35 9 33 30 37 37 43 3 0 57 32 18 25
  26 35 39 17 61 52 8 57 41 0 30 59 70 34 64 48 68 11 37 19 55 49 11 35 54 75
  22 77 46 89 42 39 44 66 10 71 69 12 29 6 47 47 94 94 48 75 59 38 53 30 109 35
  3 69 49 11 49 26 27 109 67 97
|#
