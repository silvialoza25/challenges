# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.02s to load, 0.07s running 55 checks on 1 file)
# 7 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule InsertionSort do

  def main do
    data = to_int(String.split(List.first(Enum.slice(String.split(
            IO.read(:stdio, :all), "\n"), 1..1)), " "))
    Enum.map(insertion_sort(data, [], []), &IO.write("#{&1} "))
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def insert(value, [], counter), do: [[value], counter]
  def insert(value, [head | tail], counter) do
    acc = [head | tail]
    if head > value do
      [[value | acc], length(acc)]
    else
      [aux, counter] = insert(value, tail, counter)
      [[head | aux], counter]
    end
  end

  def insertion_sort([head | tail], acc, acum_count) do
    [aux, counter] = insert(head, acc, 0)
    insertion_sort(tail, aux, acum_count ++ [counter])
  end
  def insertion_sort([], _acc, counter), do: tl(counter)
end

# cat DATA.lst | elixir -r ludsrill.exs -e InsertionSort.main
# 1 1 0 3 4 5 7 2 4 0 10 1 2 6 8 4 11 11 5 6 0 16 20 19 20 8 19 15 29 24 28 20
# 27 30 29 28 23 34 36 40 38 5 24 9 42 10 38 27 25 15 47 2 50 24 21 43 7 10 59
# 5 40 22 27 57 42 11 67 2 35 36 36 58 37 71 73 36 5 13 61 47 12 76 83 79 11 31
# 43 82 17 50 38 51 42 12 62 54 51 48 49 56 71 97 61 18 35 35 99 27 103 81 53
# 75 96 31 81 95 0 73 44 39 85 3 74 23 17 46 96 7 55 124 43 15 67 66
