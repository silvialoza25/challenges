{-
  $ idris2 jmesa85.idr -o jmesa85
  $
-}

-- CodeAbbey #121: Insertion Sort - Idris2

module Main
import Data.Strings
import Data.List

strToInt : String -> Int
strToInt s =
  case parseInteger {a=Int} s of
    Nothing => 0
    Just j  => j

elemAt : (n : Nat) -> (xs : List Int) -> Int
elemAt 0 [] = 0
elemAt (S _) [] = 0
elemAt Z (x :: xs) = x
elemAt (S k) (_ :: xs) = elemAt k xs

getPos : List Int -> Int -> Nat -> Nat
getPos nums item aux =
  if (aux == length nums) || item < (elemAt aux nums) then aux
  else getPos nums item (aux + 1)

insertionSort : List Int -> Nat -> String
insertionSort nums auxIndex =
  if auxIndex == length nums then ""
  else
    let
      item : Int
      item = elemAt auxIndex nums
      headArr : List Int
      headArr = take auxIndex nums
      tailArr : List Int
      tailArr = drop (auxIndex + 1) nums
      position : Nat
      position = getPos headArr item 0
      leftArr : List Int
      leftArr = take position headArr
      rightArr : List Int
      rightArr = drop position headArr
      sortedArr : List Int
      sortedArr = leftArr ++ ([item]) ++ rightArr ++ tailArr
      shiftedCounter : Int
      shiftedCounter = (cast (length headArr)) - (cast position) in
    show (shiftedCounter) ++ " " ++ (insertionSort sortedArr (auxIndex + 1))

main : IO ()
main = do
  n <- getLine
  input <- getLine
  let
    inputArr = words input
    nums = map (\item => strToInt item) inputArr
    out = insertionSort nums 1
  putStrLn out

{-
  $ idris2 jmesa85.idr -o jmesa85 && cat DATA.lst | ./build/exec/jmesa85
  1 0 1 1 3 0 2 0 3 3 4 6 1 4 4 10 13 8 17 18 17 9 19 23 13 21 7 28 6 28 23 23
  16 27 17 8 14 29 24 15 0 30 40 25 34 6 23 18 34 20 0 38 53 49 13 40 22 42 34
  19 6 13 25 14 21 26 18 28 60 27 14 19 27 66 24 48 45 53 9 36 7 40 21 40 33
  68 60 71 29 80 33 21 44 62 66 25 48 70 78 12 29 68 30 20 5 78 107 0 16 3 64
  12 54 45 54 15 16 45 39 6 43 73
-}
