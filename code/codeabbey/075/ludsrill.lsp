#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defvar *data*)
(defvar *item*)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(setq *data* (cdr (read-data)))

(dolist (*item* *data*)
  (setq count-1 (count 1 *item*))
  (setq count-2 (count 2 *item*))
  (setq count-3 (count 3 *item*))
  (setq count-4 (count 4 *item*))
  (setq count-5 (count 5 *item*))
  (setq count-6 (count 6 *item*))
  (setq counter (list count-1 count-2 count-3 count-4 count-5 count-6))
  (setq dice-1 (count 1 counter))
  (setq dice-2 (count 2 counter))
  (setq dice-3 (count 3 counter))
  (setq dice-4 (count 4 counter))
  (setq dice-5 (count 5 counter))
  (cond ((= dice-2 1) (format t "pair "))
        ((= dice-3 1) (format t "three "))
        ((= dice-4 1) (format t "four "))
        ((= dice-2 2) (format t "two-pairs "))
        ((= dice-5 1) (format t "yacht "))
        ((and (= dice-1 5) (equal (sort *item* #'<) '(2 3 4 5 6)))
         (format t "big-straight "))
        ((and (= dice-1 5) (equal (sort *item* #'<) '(1 2 3 4 5)))
         (format t "small-straight "))
        ((and (= dice-3 1) (= dice-2 1)) (format t "full-house "))
        (T (format t "none "))))

#|
cat DATA.lst | clisp ludsrill.lsp
yacht pair three small-straight two-pairs small-straight pair yacht
big-straight pair pair big-straight big-straight two-pairs two-pairs
pair two-pairs yacht small-straight small-straight small-straight
small-straight three yacht big-straight three pair small-straight
|#
