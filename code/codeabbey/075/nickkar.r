# $ lintr::lint('nickkar.r')

formatinput <- function(ca5es, out=c(), i=1) {
    if (i > length(ca5es)) {
        return(out)
    }
    else {
        tmp <- strsplit(ca5es[i], " ")
        formatinput(ca5es, c(out, tmp),  i + 1)
    }
}

findcomb <- function(ca5es, out=c(), i=1) {
    if (i > length(ca5es)) {
        return(out)
    }
    ca5e <- ca5es[[i]]
    if (length(ca5e) == length(unique(ca5es[[i]]))) {
        if (all(ca5e[order(ca5e)] == c(1, 2, 3, 4, 5))) {
            findcomb(ca5es, c(out, "small-straight"), i + 1)
        }
        else if (all(ca5e[order(ca5e)] == c(2, 3, 4, 5, 6))) {
            findcomb(ca5es, c(out, "big-straight"), i + 1)
        }
        else {
            findcomb(ca5es, c(out, "none"), i + 1)
        }
    }
    else {
        tmp <- switch(length(unique(ca5e)),
        "yacht",
        "four/full-house",
        "three/two-pairs",
        "pair",
        "error"
        )
        if (tmp == "three/two-pairs") {
            if (length(unique(ca5e[duplicated(ca5e)])) == 1) {
                findcomb(ca5es, c(out, "three"), i + 1)
            }
            else {
                findcomb(ca5es, c(out, "two-pairs"), i + 1)
            }
        }
        else if (tmp == "four/full-house") {
            if (length(unique(ca5e[duplicated(ca5e)])) == 1) {
                findcomb(ca5es, c(out, "four"), i + 1)
            }
            else {
                findcomb(ca5es, c(out, "full-house"), i + 1)
            }
        }
        else {
            findcomb(ca5es, c(out, tmp), i + 1)
        }
    }
}

main <- function() {
    file <- readLines("stdin")
    data <- formatinput(file[2:length(file)])
    cat(findcomb(data), "\n")
}

main()

# $ cat DATA.lst | Rscript nickkar.r
# two-pairs pair small-straight pair pair pair small-straight two-pairs three
# yacht small-straight none yacht two-pairs pair pair two-pairs big-straight
# two-pairs full-house small-straight pair two-pairs two-pairs pair
# small-straight yacht pair none none small-straight
