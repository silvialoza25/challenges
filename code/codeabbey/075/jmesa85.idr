{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #075: Yacht or Dice Poker - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi str = fromMaybe 0 $ parseInteger {a=Int} str

-- Gets lines in the Stdin
getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

processTestCase : List String -> String
processTestCase unsortedDice =
  let
    dice = sort unsortedDice
    uniq = length $ nub dice
    auxFour = length $ nub $ drop 1 $ take 4 dice
    auxThree =
      (length $ nub $ take 3 dice) == 1 ||
      (length $ nub $ drop 1 $ take 3 dice) == 1 ||
      (length $ nub $ drop 2 dice) == 1
  in
    if dice == ["2", "3", "4", "5", "6"] then "big-straight"
    else if dice == ["1", "2", "3", "4", "5"] then "small-straight"
    else if uniq == 1 then "yacht"
    else if uniq == 4 then "pair"
    else if uniq == 2 && auxFour == 2 then "full-house"
    else if uniq == 2 && auxFour == 1 then "four"
    else if uniq == 3 && auxThree then "three"
    else if uniq == 3 && not auxThree then "two-pairs"
    else "none"

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse test cases
    testCases = map words inputData
    -- Process data
    results = map processTestCase testCases
  -- Print results
  putStrLn $ unwords results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  yacht pair pair two-pairs big-straight full-house two-pairs big-straight
  pair two-pairs none big-straight pair full-house big-straight big-straight
  small-straight yacht small-straight small-straight big-straight yacht pair
  big-straight three big-straight three yacht pair
-}
