/*
$ cargo clippy
Checking code_75 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.09s
$ cargo build
Compiling code_75 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.30s
$ rustrc sullenestd1.rs
*/

use std::io::{self, BufRead};

fn all_same<T: Eq>(vec: &[T]) -> bool {
  vec
    .get(0)
    .map(|first| vec.iter().all(|x| x == first))
    .unwrap_or(true)
}

fn sequences(vec: &[i8]) -> String {
  if vec.iter().eq([1, 2, 3, 4, 5].iter()) {
    "small-straight".to_string()
  } else if vec.iter().eq([2, 3, 4, 5, 6].iter()) {
    "big-straight".to_string()
  } else {
    " ".to_string()
  }
}

fn combination_name(pair: i8, three: i8, four: i8) -> String {
  if four > 0 {
    "four".to_string()
  } else if three == 1 && pair == 1 {
    "full-house".to_string()
  } else if three == 1 {
    "three".to_string()
  } else if pair == 2 {
    "two-pairs".to_string()
  } else if pair == 1 {
    "pair".to_string()
  } else {
    "none".to_string()
  }
}

fn count(
  numbers: Vec<i8>,
  pair: &mut i8,
  three: &mut i8,
  four: &mut i8,
) -> String {
  for x in 1..7 {
    let mut cont = 0;
    for y in &numbers {
      if &x == y {
        cont += 1;
      }
    }
    if cont == 2 {
      *pair += 1;
    } else if cont == 3 {
      *three += 1;
    } else if cont == 4 {
      *four += 1;
      break;
    }
  }
  combination_name(*pair, *three, *four)
}

fn yatch(mut numbers: Vec<i8>) -> String {
  if all_same(&numbers) {
    "yatch".to_string()
  } else {
    numbers.sort();
    let mut aux = sequences(&numbers);
    if aux != " " {
      aux
    } else {
      let mut pair: i8 = 0;
      let mut three: i8 = 0;
      let mut four: i8 = 0;
      aux = count(numbers, &mut pair, &mut three, &mut four);
      aux
    }
  }
}

fn main() {
  let input = io::stdin();
  let lines = input.lock().lines();
  let mut n = 1;
  for x in lines {
    if n == 1 {
      n = 2
    } else {
      let line = x.unwrap();
      let numbers: Vec<i8> = line
        .trim()
        .split(' ')
        .map(|x| x.parse().expect("Not an integer!"))
        .collect();
      println!("{}", yatch(numbers));
    }
  }
}

/*
$ cat DATA.lst | ./code_75
pair
big-straight
three
big-straight
two-pairs
two-pairs
two-pairs
big-straight
yatch
pair
two-pairs
pair
three
full-house
two-pairs
yatch
full-house
small-straight
big-straight
yatch
big-straight
small-straight
three
big-straight
none
pair
*/
