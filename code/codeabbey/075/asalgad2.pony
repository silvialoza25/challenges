// ponyc -b asalgad2

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail( array: Array[String] ): Array[String] =>
          array.slice(1, array.size())

        fun swap( values: Array[String], const_i: USize, const_j: USize ) ? =>
          var temp: String = values(const_i)?
          values.update( const_i, values(const_j)? )?
          values.update( const_j, temp )?

        fun bubble( values: Array[String], const_n: USize,
                    const_i: USize, const_j: USize ) ? =>
          if const_i < const_n then
            if const_j < (const_n - const_i-1) then
              if values(const_j)? > values(const_j+1)? then
                swap( values, const_j, const_j+1 )?
              end
              bubble( values, const_n, const_i, const_j+1 )?
            else
              bubble( values, const_n, const_i+1, 0 )?
            end
          end

        fun compare( array1: Array[String], array2: Array[String] ): USize ? =>
          if array1.size() > 0 then
            if array1(0)? != array2(0)? then
              0
            else
              compare( tail(array1), tail(array2) )?
            end
          else
            1
          end

        fun count( values: Array[String], value: String ): USize ? =>
          if values.size() > 0 then
            if values(0)? == value then
              1 + count( tail(values), value )?
            else
              count( tail(values), value )?
            end
          else
            0
          end

        fun get_count( values: Array[String] ): Array[String]? =>
          var one: String = count( values, "1" )?.string()
          var two: String = count( values, "2" )?.string()
          var three: String = count( values, "3" )?.string()
          var four: String = count( values, "4" )?.string()
          var five: String = count( values, "5" )?.string()
          var six: String = count( values, "6" )?.string()
          [one; two; three; four; five; six]

        fun identify( values: Array[String] ) ? =>
          var counts: Array[String] = get_count( values )?
          bubble( counts, counts.size(), 0, 0 )?
          bubble( values, values.size(), 0, 0 )?

          if counts(5)? == "5" then
            env.out.write( "yacht " )
          elseif counts(5)? == "4" then
            env.out.write( "four " )
          elseif counts(5)? == "3" then
            if counts(4)? == "2" then
              env.out.write( "full-house " )
            else
              env.out.write( "three " )
            end
          elseif counts(5)? == "2" then
            if counts(4)? == "2" then
              env.out.write( "two-pairs " )
            else
              env.out.write( "pair " )
            end
          elseif compare( values, ["2"; "3"; "4"; "5"; "6"] )? == 1 then
            env.out.write( "big-straight " )
          elseif compare( values, ["1"; "2"; "3"; "4"; "5"] )? == 1 then
            env.out.write( "small-straight " )
          else
            env.out.write( "none " )
          end

        fun iterate( lines: Array[String] ) ? =>
          if lines.size() > 0 then
            identify( lines(0)?.split(" ") )?
            iterate( tail(lines) )?
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")
          var useful_lines: Array[String] = lines.slice(1, lines.size()-1)

          try
            iterate( useful_lines )?
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// small-straight three three two-pairs big-straight pair small-straight
// small-straight big-straight pair three big-straight big-straight pair
// two-pairs three pair three pair small-straight two-pairs big-straight
// big-straight pair pair small-straight pair small-straight
