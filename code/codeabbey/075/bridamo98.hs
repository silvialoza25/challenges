{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Data.Char
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  dat <- replicateM intSizeInput getLine
  let datWithoutSpaces = map deleteSpacesAndSort dat
  let solution = map solve datWithoutSpaces
  putStrLn (unwords solution)

deleteSpacesAndSort :: String -> String
deleteSpacesAndSort dat = sort (filter (/= ' ') dat)

solve :: String -> String
solve dat = res
  where
    p0 = head dat
    p1 = dat !! 1
    p2 = dat !! 2
    p3 = dat !! 3
    p4 = dat !! 4
    resAux
      | dat == "23456" = "big-straight"
      | dat == "12345" = "small-straight"
      | ((p0 == p1 && p2 == p4) || (p0 == p2 && p3 == p4)) && p0 /= p4 =
        "full-house"
      | p0 == p4 = "yacht"
      | p0 == p3 || p1 == p4 = "four"
      | p0 == p2 || p1 == p3 || p2 == p4 = "three"
      | p0 == p1 = [p0]
      | p1 == p2 = [p1]
      | p2 == p3 = [p2]
      | p3 == p4 = [p3]
      | otherwise = "none"
    res
      | length resAux /= 1 = resAux
      | (p0 == p1 && head resAux /= p0) ||
          (p1 == p2 && head resAux /= p1) ||
            (p2 == p3 && head resAux /= p2) ||
              (p3 == p4 && head resAux /= p3)
        = "two-pairs"
      | otherwise = "pair"

{-
$ cat DATA.lst | ./bridamo98
  two-pairs pair small-straight pair pair pair small-straight two-pairs three
  yacht small-straight none yacht two-pairs pair pair two-pairs big-straight
  two-pairs full-house small-straight pair two-pairs two-pairs pair
  small-straight yacht pair none none small-straight
-}
