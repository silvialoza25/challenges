; clj-kondo --lint juanksepul.cljs
; linting took 1014ms, errors: 0, warnings: 0

(ns codeabbey.juanksepul
   (:require [clojure.core :as core])
   (:require [clojure.string :as str]))

(defn read-data []
  (str/split-lines (core/slurp core/*in*)))

(defn get-nums [line]
  (re-seq #"[1-6]" line))

(defn is-spec [nums]
  (cond
    (= '("1" "2" "3" "4" "5") (sort nums)) "small-straight"
    (= '("2" "3" "4" "5" "6") (sort nums)) "big-straight"
    :else nil))

(defn cnt-nums [nums]
  (vals (core/frequencies nums)))

(defn classify [nums]
  (let [f-vals (vec (cnt-nums nums))]
    (cond
      (some #(= 5 %) f-vals) "yacht"
      (some #(= 4 %) f-vals) "four"
      (some #(= 3 %) f-vals) (if (some #(= 2 %) f-vals)
                               "full-house" "three")
      (= (count (filter #(= 2 %) f-vals)) 2) "two-pairs"
      :else "pair")))

(defn !main []
  (doseq [line (rest (read-data))]
    (let [nums (get-nums line)]
      (if-not (is-spec nums)
        (print (str (classify nums) " "))
        (print (str (is-spec nums) " "))))))

(!main)

; cat DATA.lst | clj juanksepul.cljs
; yacht three pair three small-straight two-pairs big-straight
; big-straight pair big-straight small-straight big-straight
; pair small-straight two-pairs big-straight big-straight
; small-straight three pair pair two-pairs pair small-straight
; two-pairs big-straight small-straight two-pairs big-straight
; two-pairs two-pairs
