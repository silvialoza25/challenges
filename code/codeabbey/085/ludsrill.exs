# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.02s to load, 0.09s running 55 checks on 1 file)
# 4 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule Rotationin2DSpace do
  def main do
    all_data = IO.read(:stdio, :all)

    if all_data != "" do
      split_data = String.split(all_data, "\n")
      List.delete_at(split_data, 1 +
        String.to_integer(hd(String.split(Enum.at(split_data, 0), " "))))
        |> Enum.map(&String.split(&1, " "))
        |> solution()
    else
      IO.puts("Data is empty")
    end
  end

  def solution([head | tail]) do
    Enum.map(tail, fn [name, x, y] -> apply_rotation(name,
      String.to_integer(x), String.to_integer(y),
      String.to_integer(List.last(head)) * :math.pi() / 180) end)
      |> Enum.sort(&(List.last(&1) <= List.last(&2)))
      |> Enum.map(&IO.write("#{List.first(&1)} "))
  end

  def apply_rotation(name, x, y, degree) do
    x_prima = x * :math.cos(degree) - y * :math.sin(degree)
    y_prima = y * :math.cos(degree) + x * :math.sin(degree)
    [name, Kernel.round(x_prima), Kernel.round(y_prima)]
  end
end

Rotationin2DSpace.main()

# cat DATA.lst | elixir ludsrill.exs
# Diadem Thabit Mizar Unuk Alcyone Kochab Castor Algol Nembus Lesath Aldebaran
# Rigel Yildun Electra Media Heka Mira Gemma Kastra Zosma Bellatrix Albireo
# Pherkad Vega Alcor Polaris Sirius Jabbah Capella Altair Deneb Fomalhaut
# Procyon Betelgeuse
