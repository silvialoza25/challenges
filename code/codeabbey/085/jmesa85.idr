{-
  $ idris2 jmesa85.idr -o jmesa85
  $
-}

-- CodeAbbey #085: Rotation in 2D Space - Idris2

module Main
import System
import Data.List
import Data.Strings

-- Not available in included libraries
round : Double -> Int
round x = if x > 0
  then if x - floor x < 0.5 then cast $ floor x else cast $ ceiling x
  else if ceiling x - x < 0.5 then cast $ ceiling x else cast $ floor x

-- Helps to get rid of the Maybe
strToInt : String -> Int
strToInt s =
  case parseInteger {a=Int} s of
    Nothing => 0
    Just j  => j

-- Gets the star tuple and return its name
getStarName : (String, Int, Int) -> String
getStarName (name,_) = name

-- Sort function by y position and x position
-- To be used by Data.List.sortBy
sortStars : (String, Int, Int) -> (String, Int, Int) -> Ordering
sortStars (name1, xPos1, yPos1) (name2, xPos2, yPos2) =
  if yPos1 > yPos2 then GT
  else if yPos2 > yPos1 then LT
    else compare xPos1 xPos2

-- Performs a 2D rotation for the given star (name, x, y)
-- Angle in radians (Double)
rotateStar : (String, Int, Int) -> Double -> (String, Int, Int)
rotateStar (name, xPos, yPos) angleRad = (name, xRotated, yRotated)
  where
    xRotated : Int
    yRotated : Int
    xAux : Double
    yAux : Double
    -- Aux casting Int to Double
    xAux = the Double (cast xPos)
    yAux = the Double (cast yPos)
    -- Calculate new rotated coordinates
    xRotated = round ((xAux * (cos angleRad)) - (yAux * (sin angleRad)))
    yRotated = round ((xAux * (sin angleRad)) + (yAux * (cos angleRad)))

-- Creates a tuple (name, x, y) with the star info
partial makeStar : String-> (String, Int, Int)
makeStar info =
  let
    [nameStr, xPosStr, yPosStr] = words info
    xPos = strToInt xPosStr
    yPos = strToInt yPosStr
    in
  (nameStr, xPos, yPos)

-- Gets lines in the Stdin
getLines : IO (List String)
getLines = do
  x <- getLine
  if x == "" then pure []
    else do
      xs <- getLines
      pure (x :: xs)

-- Point of entry
partial main : IO ()
main = do
  -- Read STDIN
  inputData <- getLines
  let
    -- Parse angle in first line
    [header] = (take 1 inputData)
    [_, angle] = map strToInt (words header)
    -- From degrees to radians
    angleRad = (cast angle) / 180.0 * pi
    -- Parse stars info to "List (name, x, y)"
    heaven = map makeStar (drop 1 inputData)
    -- Perform rotation
    rotated = map (\star => rotateStar star angleRad) heaven
    -- Sort rotated stars
    sorted = sortBy (sortStars) rotated
    -- Retrieve only the names
    names = map getStarName sorted
  -- Join the results in a single string
  putStrLn (unwords names)

{-
  $ idris2 jmesa85.idr -o jmesa85 && cat DATA.lst | ./build/exec/jmesa85
  Capella Procyon Electra Vega Altair Algol Fomalhaut Nembus Yildun Media
  Polaris Betelgeuse Kastra Albireo Pherkad Rigel Diadem Mizar Gemma Unuk
  Mira Deneb Kochab Thabit Castor Lesath Sirius Alcor Zosma Alcyone Heka
  Aldebaran Bellatrix
-}
