;; $clj-kondo --lint nickkar.clj
;; linting took 34ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.string :as str]))

(defn read-input ([n] (read-input n 0 []))
  ([n i out]
    (cond
      (>= i n) out
      :else
      (let [[name strx stry] (str/split (read-line) #" ")
           x (read-string strx)
           y (read-string stry)]
        (read-input n (inc i) (conj out [name x y]))))))

(defn to-rads [deg]
  (* deg (/ Math/PI 180)))

(defn movestars ([stars angle] (movestars stars angle 0 []))
  ([stars angle i out]
    (cond
      (>= i (count stars)) out
      :else
      (let [[name x y] (nth stars i)
           rad-angle (to-rads angle)
           tmpx (- (* x (Math/cos rad-angle)) (* y (Math/sin rad-angle)))
           tmpy (+ (* x (Math/sin rad-angle)) (* y (Math/cos rad-angle)))
           newx (Math/round tmpx)
           newy (Math/round tmpy)]
        (movestars stars angle (inc i) (conj out [name newx newy]))))))

(defn main []
  (let [firstln (str/split (read-line) #" ")
        n (read-string (nth firstln 0))
        angle (read-string (nth firstln 1))
        stars (read-input n)
        movedstars (movestars stars angle)
        sorted (sort-by last movedstars)]
    (apply println (map first sorted))))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; Diadem Thabit Mizar Unuk Alcyone Kochab Castor Algol Nembus Lesath Aldebaran
;; Rigel Yildun Electra Media Heka Mira Gemma Kastra Zosma Bellatrix Albireo
;; Pherkad Vega Alcor Polaris Sirius Jabbah Capella Altair Deneb Fomalhaut
;; Procyon Betelgeuse
