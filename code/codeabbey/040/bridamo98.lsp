#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun delimiterp (c)
  (char= c #\Space))

(defun split (string &key (delimiterp #'delimiterp))
  (loop
    :for beg = (position-if-not delimiterp string)
    :then (position-if-not delimiterp string :start (1+ end))
    :for end = (and beg (position-if delimiterp string :start beg))
    :when beg :collect (subseq string beg end)
    :while end))

(defun init-board (board rows)
  (if (> rows 0)
    (init-board (append board (list (split (read-line)))) (- rows 1))
    board))

(defun init-fst-column (board dp-table cur-row rows)
  (when (< cur-row rows)
    (progn
      (when (string= (nth 0 (nth cur-row board)) "+")
        (setf (aref dp-table cur-row 0) (aref dp-table (- cur-row 1) 0)))
      (init-fst-column board dp-table (+ 1 cur-row) rows))))

(defun init-fst-row (board dp-table cur-col cols)
  (when (< cur-col cols)
    (progn
      (when (string= (nth cur-col (nth 0 board)) "+")
        (setf (aref dp-table 0 cur-col) (aref dp-table 0 (- cur-col 1))))
      (init-fst-row board dp-table (+ 1 cur-col) cols))))

(defun iterate-over-cols(board dp-table cur-row cur-col cols)
  (when (< cur-col cols)
    (let ((cur-position (nth cur-col (nth cur-row board))))
      (when (or (string= cur-position "+") (string= cur-position "$"))
        (setf (aref dp-table cur-row cur-col)
              (+ (aref dp-table (- cur-row 1) cur-col)
                 (aref dp-table cur-row (- cur-col 1)))))
      (iterate-over-cols board dp-table cur-row (+ cur-col 1) cols))))

(defun iterate-over-rows(board dp-table cur-row rows cols)
  (if (< cur-row rows)
    (progn
      (iterate-over-cols board dp-table cur-row 1 cols)
      (iterate-over-rows board dp-table (+ cur-row 1) rows cols))
    (aref dp-table (- rows 1) (- cols 1))))

(defun main(rows cols)
  (let ((board (init-board '() rows))
        (dp-table (make-array (list rows cols) :initial-element 0)))
    (setf (aref dp-table 0 0) 1)
    (init-fst-column board dp-table 1 rows)
    (init-fst-row board dp-table 1 cols)
    (write (iterate-over-rows board dp-table 1 rows cols))))

(main (read) (read))

#|
  cat DATA.lst | clisp bridamo98.lsp
  5653762
|#
