// dartanalyzer ludsrill.dart
// Analyzing ludsrill.dart...
// No issues found!

import 'dart:core';
import 'dart:io';

void main() {
  List<String> input = getData([]);
  int rows = int.parse(input[0]);
  int columns = int.parse(input[1]);
  List<List<String>> board = toMatrix(rows, columns, input.sublist(2));
  List<List<int>> path = editMatrix(0, 0, makeMatrix(rows, columns), 1);
  print(pathInTheGrid(path, board).last.last);

}

List<List<int>> pathInTheGrid(List<List<int>> path, List<List<String>> board) {
  List<List<int>> initializedPath =
      initializeRow(1, board, initializeColumn(1, board, path));
  return nestedFor(1, 1, initializedPath, board);
}

List<List<int>> initializeColumn(
    int count, List<List<String>> board, List<List<int>> path) {
  if (count < board.length) {
    if (board[count][0] == "+" || board[count][0] == "\$") {
      return initializeColumn(
          count + 1, board, editMatrix(count, 0, path, path[count - 1][0]));
    } else {
      return initializeColumn(count + 1, board, path);
    }
  } else {
    return path;
  }
}

List<List<int>> initializeRow(
    int count, List<List<String>> board, List<List<int>> path) {
  if (count < board[0].length) {
    if (board[0][count] == "+" || board[0][count] == "\$") {
      return initializeRow(
          count + 1, board, editMatrix(0, count, path, path[0][count - 1]));
    } else {
      return initializeRow(count + 1, board, path);
    }
  } else {
    return path;
  }
}

List<List<int>> nestedFor(int countRow, int countColumn, List<List<int>> path,
    List<List<String>> board) {
  if (countColumn < board[0].length && countRow < board.length) {
    if (board[countRow][countColumn] == "+" ||
        board[countRow][countColumn] == "\$") {
      int aux =
          path[countRow - 1][countColumn] + path[countRow][countColumn - 1];
      return nestedFor(countRow, countColumn + 1,
          editMatrix(countRow, countColumn, path, aux), board);
    } else {
      return nestedFor(countRow, countColumn + 1, path, board);
    }
  } else if (countRow < board.length){
      return nestedFor(countRow + 1, 1, path, board);
  } else{
    return path;
  }
}

List<List<int>> editMatrix(
    int row, int col, List<List<int>> matrix, int newValue) {
  List<List<int>> newArray = new List.from(matrix);
  newArray[row][col] = newValue;
  return newArray;
}

List<String> getData(List<String>input) {
  String getLine = stdin.readLineSync();
  if (getLine == null) {
    return input.sublist(0);
  } else {
    List<String> newList = getLine.split(" ").toList();
    return getData(input + newList);
  }
}

List<List<int>> makeMatrix(int rows, int cols) {
  return new Iterable<List<int>>.generate(
      rows, (row) => new Iterable<int>.generate(cols, (col) => 0).toList())
      .toList();
}

List<List<String>> toMatrix(int rows, int cols, List<String> matrix) {
  return new Iterable<List<String>>.generate(
      rows, (row) => new Iterable<String>
      .generate(cols, (col) => matrix[col + (row * cols)]).toList())
      .toList();
}

// cat DATA.lst | dart ludsrill.dart
// 4435538
