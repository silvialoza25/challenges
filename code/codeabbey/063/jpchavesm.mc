/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, bool.

:- func isPrimeAux(int, int) = bool.
isPrimeAux(Num, AuxCheck) =
(
  if pow(AuxCheck,2) =< Num
  then (
    if Num mod AuxCheck = 0; Num mod (AuxCheck + 2) = 0
    then no
    else isPrimeAux(Num,AuxCheck + 6))
  else yes
).

:- pred isPrime(int::in,bool::out) is det.
isPrime(Num, IsPrime) :-
(
  if Num = 2; Num = 3
  then IsPrime = yes
  else (
    if Num mod 2 = 0; Num mod 3 = 0
    then IsPrime = no
    else IsPrime = isPrimeAux(Num, 5)) %starting check
).

:- func findFactors(int,int,list(int)) = list(int).
findFactors(Num, Factor, FactorList) =
(
  if
    Num > 1
  then
    (if Num mod Factor = 0, isPrime(Factor,IsPrime), IsPrime = yes
    then findFactors(Num / Factor, Factor, FactorList ++ [Factor])
    else findFactors(Num, Factor + 1, FactorList))
  else FactorList
).

:- pred list2factors(list(int), string, string).
:- mode list2factors(in, in, out) is det.
list2factors([], FinalString, remove_prefix_if_present("*", FinalString)).
list2factors([Factor | FactorTail], PartialString, FinalString) :-
(
  from_int(Factor) = FactorStr,
  PartialString1 = PartialString ++ "*" ++ FactorStr,
  list2factors(FactorTail, PartialString1, FinalString)
).

:- pred integerFact(list(string), string, string).
:- mode integerFact(in, in, out) is det.
integerFact([], FinalFactor, strip(FinalFactor)).
integerFact([Line | Tail], PartialFactor, FinalFactor) :-
(
  det_to_int(chomp(Line)) = Num,
  findFactors(Num, 2, []) = FactorList,
  list2factors(FactorList, "", FactorStr),
  PartialFactor1 = PartialFactor ++ " " ++ FactorStr,
  integerFact(Tail, PartialFactor1, FinalFactor)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1, FileContents, ComputeList),
    integerFact(ComputeList, "", Factorizations),
    io.print_line(Factorizations, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  149*293*313*313*409 53*139*151*233*467 83*271*349*397*419 113*223*257*331*503
  193*227*233*373*503 397*467*499*523 97*167*311*317*521 53*107*139*173*491
  181*233*313*383*503 79*223*229*409*541 137*167*193*233*457 79*131*139*353*577
  59*179*269*367*569 73*107*191*239*353 197*239*331*401*499 331*467*587*593
  97*113*193*313*449 113*257*257*373*479 137*233*353*421*433 317*433*569*569
  113*173*191*337*421 223*433*463*479
*/
