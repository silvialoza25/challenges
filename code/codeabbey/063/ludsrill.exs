# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.07 seconds (0.02s to load, 0.05s running 55 checks on 1 file)
# 5 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule IntegerFactorization do
  def main do
    data = get_data()
    solution = Enum.map(data, &integer_factorization(&1, 2, ""))
    Enum.map(solution, &IO.write("#{&1} "))
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def get_data do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    [_ | tail] = List.delete_at(all_data, 1 +
                  String.to_integer(Enum.at(all_data, 0)))
    List.flatten(Enum.map(Enum.map(tail, &String.split(&1, " ")), &to_int/1))
  end

  def integer_factorization(item, prime, accum) do
    cond do
      item == 1 -> String.slice(accum, 1..(String.length(accum)))
      rem(item, prime) == 0 and item != 0 ->
        integer_factorization(div(item, prime), prime,
          accum <> "*" <> Integer.to_string(prime))
      rem(item, prime) != 0 and item != 0 ->
        integer_factorization(item, prime + 1, accum)
    end
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e IntegerFactorization.main
# 149*293*313*313*409 53*139*151*233*467 83*271*349*397*419 113*223*257*331*503
# 193*227*233*373*503 397*467*499*523 97*167*311*317*521 53*107*139*173*491
# 181*233*313*383*503 79*223*229*409*541 137*167*193*233*457 79*131*139*353*577
# 59*179*269*367*569 73*107*191*239*353 197*239*331*401*499 331*467*587*593
# 97*113*193*313*449 113*257*257*373*479 137*233*353*421*433 317*433*569*569
# 113*173*191*337*421 223*433*463*479
