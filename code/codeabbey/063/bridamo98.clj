;; $ clj-kondo --lint bridamo98.clj
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-063
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn prime-fact[number]
  (loop [n number p 2 res ""]
    (if (or (> n (* p p)) (= n (* p p)))
      (let [modu (mod n p) nxt-res (if (= modu 0)
      (str res p "*") res) nxt-n (if (= modu 0)
      (quot n p) n) nxt-p (if (= modu 0) p (+ p 1))]
        (recur nxt-n nxt-p nxt-res))(str res n))))

(defn solve-all[size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (recur (+ i 1)
        (str result (prime-fact (edn/read-string
        (core/read-line))) " "))
        result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.clj
;; 149*293*313*313*409 53*139*151*233*467 83*271*349*397*419
;; 113*223*257*331*503 193*227*233*373*503 397*467*499*523
;; 97*167*311*317*521 53*107*139*173*491 181*233*313*383*503
;; 79*223*229*409*541 137*167*193*233*457 79*131*139*353*577
;; 59*179*269*367*569 73*107*191*239*353 197*239*331*401*499
;; 331*467*587*593 97*113*193*313*449 113*257*257*373*479
;; 137*233*353*421*433 317*433*569*569 113*173*191*337*421
;; 223*433*463*479
