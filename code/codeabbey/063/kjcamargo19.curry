-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import IO
import List
import Read

getData :: IO String
getData = do
  content <- getContents
  return content

factorization :: Int -> Int -> String -> String
factorization number prime accum
  | number == 1 = accum
  | (rem number prime == 0) && (number /= 0)
  = factorization (div number prime) prime (accum ++ "*" ++ (show prime))
  | otherwise = factorization number (prime + 1) accum

getSolution :: Int -> String
getSolution number  = result
  where
    result = drop 1 (factorization number 2 "")

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = map (\x -> readInt x) (drop 1 (init dataLines))
    finalResult = map (\x -> getSolution x) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 67*97*293*509*509 193*229*311*367*479 59*163*271*311*593 79*103*167*233*349
-- 227*233*283*359*487 97*113*223*317*577 59*83*101*197*599 83*293*373*463*557
-- 83*173*197*271*547 89*167*281*373*433 293*353*383*587 101*157*347*383*397
-- 97*101*317*379*503 269*401*443*521 71*109*139*251*331 211*227*367*467*569
-- 107*263*293*317*401 97*97*347*479*569 197*223*227*233*521 79*283*373*479*587
-- 367*421*467*599 79*233*313*409*461 151*193*311*349*397 173*223*359*367*461
