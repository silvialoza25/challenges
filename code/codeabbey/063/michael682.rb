# frozen_string_literal: true

# $ rubocop michael682.rb
# Inspecting 1 file
# 1 file inspected, no offenses detected

def condition1(num, index, flag, txt)
  # factorize2
  if (num % index).zero?
    num /= index
    txt += index.to_s + '*'
    index = 2
    flag = false
  else
    index += 1
  end
  [num, flag, txt, index]
end

def condition(txt, num)
  # factorize
  flag = true
  if num != 1 || num != 2 || num != 3
    nroot = Math.sqrt(num)
    flag, txt = factor(nroot, num, flag, txt)
  else
    txt += num.to_s
  end
  txt += num.to_s if flag
  txt
end

def factor(nroot, num, flag, txt)
  # factorize
  i = 2
  while i < (nroot + 1) && num != 1
    num, flag, txt, i = condition1(num, i, flag, txt)
  end
  [flag, txt]
end

def prime(cont)
  txt = ''
  inp = cont.split("\n").map { |i| Integer(i) }
  inp = inp[1..-1]
  inp.each do |n|
    txt = condition(txt, n)
    txt = txt[0...txt.size - 1] + ' '
  end
  txt
end

file = File.open('DATA.LST', 'rb')
cont = file.read
txt = prime(cont)
print txt

# $ ruby michael682.rb
# 73*109*271*521*587 83*223*311*487*577 103*317*401*521*541
# 229*349*509*557 173*227*379*463*479 61*97*179*337*491
# 71*107*173*433*523 139*241*331*331*421 53*73*139*293*421
# 59*137*251*479*541 199*337*389*421*523 79*157*317*367*563
# 359*401*541*563 53*263*367*389*487 101*157*241*349*587
# 101*109*173*379*503 277*313*419*439*491 97*199*229*263*269
# 181*281*367*373*383 61*101*151*199*211 389*397*439*521
# 127*127*131*197*211 151*229*347*383*439 173*293*337*443*467
# 239*307*373*449*449 59*211*383*431*563
