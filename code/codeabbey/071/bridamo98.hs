{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- getLine
  let intInputData = convert inputData
  let mainCounter = 1
  let fstCounter = 0
  let sndCounter = 1
  let solution = map show (solveAll intInputData
                 mainCounter fstCounter sndCounter)
  putStrLn (unwords solution)

solveAll :: [Int] -> Int -> Int -> Int -> [Int]
solveAll [] mainCounter fstCounter sndCounter = []
solveAll numbers mainCounter fstCounter sndCounter = res
  where
    iNum = head numbers
    modFib = fibDivAdv iNum mainCounter
             fstCounter sndCounter
    res = modFib : solveAll (drop 1 numbers)
          mainCounter fstCounter sndCounter

fibDivAdv :: Int -> Int -> Int -> Int -> Int
fibDivAdv number mainCounter fstCounter sndCounter = res
  where
    modF = (fstCounter + sndCounter) `mod` number
    res = if modF == 0
            then mainCounter + 1
            else fibDivAdv number (mainCounter + 1)
                 sndCounter modF

convert :: String -> [Int]
convert dat = result
  where
    aux = words dat
    result = map read aux :: [Int]

{-
$ cat DATA.lst | ./bridamo98
  334698 29400 112470 2700 118608 172533 73308 71100 80460
  7080 421540 320630 69750 132282 47340 43370 1007598 607968
  209048 19320
-}
