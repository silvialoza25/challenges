;; clj-kondo --lint ludsrill.cljs
;; linting took 28ms, errors: 0, warnings: 0

(ns ludsrill.071
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn fibonacci-divisibility-advanced [item counter a b]
  (let [modular-fibo (mod (+ a b) item)]
    (if (= modular-fibo 0)
      (print (core/format "%s " (inc counter)))
      (recur item (inc counter) b modular-fibo))))

(defn -main []
  (let [data (first (rest (get-data)))]
    (doseq [item data]
     (fibonacci-divisibility-advanced (Integer. item) 1 0 1))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 233172 1620 20874 29400 138793 334698 416370 30520 1024332 101610 73308
;; 147480 43370 18810 320630 17094 270 22704
