{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #071: Fibonacci Divisibility Advanced - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi str = fromMaybe 0 $ parseInteger {a=Int} str

findFibo : Int -> Int -> Int -> Int -> Int
findFibo _ index _ 0 = index - 1
findFibo divisor index prevFibo nextFibo =
  let modFibo = mod (prevFibo + nextFibo) divisor in
  findFibo divisor (index + 1) nextFibo modFibo

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLine
  let
    -- Parse test cases
    testCases = map stoi $ words inputData
    -- Process data
    results = map (\elem => findFibo elem 2 0 1) testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  1007598 102012 132804 112410 195769 66300 172533 147480 386976
  383084 29400 10500 287329 416370 231228 857744 18810 29160
  1015890 43370 421540 19320
-}
