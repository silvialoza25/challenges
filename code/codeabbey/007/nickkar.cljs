;; $ clj-kondo --lint nickkar.cljs
;; linting took 76ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(defn F-to-C [a]
  (Math/round (double (* (- a 32) (/ 5 9))))
  )

(defn itera [input-size]
  (loop [x 1]
    (when (< x (+ input-size 1))
      (print (F-to-C (int (core/read))))
      (print " ")
      (recur (+ x 1))))
  )

(defn main []
  (let [input-size (core/read)]
    (itera input-size)
    (println "")))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 196 27 185 148 132 177 77 202 22 140 148 223 4 156 42 307 243 133 213
;; 57 14 244 157 166 99 230 54 266 184 28 158 64 56 27 212 188 204
