--[[
$ luacheck jmesa85.lua
Checking jmesa85.lua                              OK
Total: 0 warnings / 0 errors in 1 file
--]]

local function round(not_rounded)
  return math.floor(not_rounded + 0.5)
end

local function fahrenheit_to_celsius(celsius)
  return (celsius - 32) * 5 / 9
end

local function process_data(str_data)
  local value = io.read("*n")

  if value == nil then
    -- Base case
    return str_data
  else
    -- Recursive Case
    return process_data(str_data .. " "
      .. tostring(round(fahrenheit_to_celsius(value))))
  end
end

local function main()
  -- Obtain amount of values to sum, but discard it
  local _ = io.read('*n')
  -- Proccess values
  print(process_data(""))
end

main()

--[[
$ cat DATA.lst | lua jmesa85.lua
 196 27 185 148 132 177 77 202 22 140 148 223 4 156 42 307 243 133 213
57 14 244 157 166 99 230 54 266 184 28 158 64 56 27 212 188 204
--]]
