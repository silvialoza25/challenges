;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))

(defn solution [arr arr0]
  (doseq [x arr]
    (if (= x arr0)
      ()
      (let [val (read-string x)
            res (Math/round (double (* (- val 32) (double (/ 5 9)))))]
        (print (str res " "))))))

(defn main []
  (let [[head _] (get-data)]
    (solution head (head 0))
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 177 236 32 238 162 51 96 97 49 24 21 27 167 292 160 202 162
;; 138 221 289 182 206 178 277 204 31 302 313 223 81 99 84 2
