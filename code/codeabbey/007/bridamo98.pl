#!/usr/bin/perl
# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 bridamo98.pl
# bridamo98.pl source OK
#
# Compile:
# $ perl -c bridamo98.pl
# bridamo98.pl syntax OK

package bridamo98;

use warnings FATAL => 'all';
use strict;
use Readonly;
Readonly my $FST_CONV_LIT => 32.0;
Readonly my $SND_CONV_LIT => 0.555555555556;

our ($VERSION) = 1;

my $SPACE = q{ };

sub far_to_cel {
  my @data   = @_;
  my $number = $data[0];
  return ( $number - $FST_CONV_LIT ) * $SND_CONV_LIT;
}

sub solve_all {
  my @data       = @_;
  my @input      = @{ $data[0] };
  my @fixedInput = @input[ 1 .. $#input ];
  foreach my $number (@fixedInput) {
    exit 1 if !printf '%.0f ', far_to_cel($number);
  }
  return;
}

sub main {
  my @input = split $SPACE, <>;
  solve_all( \@input );
  exit 1 if !print "\n";
  return;
}

main();

# $ cat DATA.lst | perl bridamo98.pl
# 65 243 176 7 293 180 203 22 310 38 258 299 9 13 39 314 2 282
# 41 67 217 137 316 229 117 297 154 284 116 199 110 182 126 287
