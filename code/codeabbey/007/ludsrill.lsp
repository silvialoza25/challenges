#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun fahrenheit-to-celsius ()
  (let ((data)
        (celsius-value))
    (setq *read-default-float-format* 'double-float)
    (setq data (cdr (first (read-data))))

    (dolist (fahrenheit-value data)
      (setq celsius-value (* (- fahrenheit-value 32) (/ 5 9)))

      (if (> (+ celsius-value 0.5) (ceiling celsius-value))
        (setq celsius-value (ceiling celsius-value))
        (setq celsius-value (floor celsius-value)))
      (format t "~d " celsius-value))))

(fahrenheit-to-celsius)

#|
cat DATA.lst | clisp ludsrill.lsp
196 27 185 148 132 177 77 202 22 140 148 223 4 156 42 307 243 133 213 57 14
244 157 166 99 230 54 266 184 28 158 64 56 27 212 188 204
|#
