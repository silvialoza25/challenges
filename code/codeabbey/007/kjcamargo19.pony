// ponyc -b kjcamargo19

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun iterator( data: Array[String]): Array[String] =>
          data.slice(1, data.size())

        fun convert_celsius(value: F32): I32 =>
          (((value - 32) * 5) / 9).round().i32()

        fun get_answer( values: Array[String] )? =>
          if values.size() > 0 then
            var answer: I32 = convert_celsius(values(0)?.f32()?)
            env.out.write(answer.string() + " ")
            get_answer( iterator(values) )?
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input_data: String = String.from_iso_array(consume raw_data)
          var line_sep: Array[String] = input_data.split_by("\n")

          try
            var values: Array[String] = line_sep(0)?.split_by(" ")
            var useful_data: Array[String] = values.slice(1, values.size())
            get_answer( useful_data )?
          end

        fun ref dispose() =>
          None
      end,
      512
    )

// cat DATA.lst | ./kjcamargo19
// 64 289 157 162 243 246 52 3 91 278 94 281 256 190 171 236 18 29 155
// 280 38 109 121 123 153 261 7 213 252 92 225 147 177 293 136 312 257
