# elvish -compileonly alejotru3012.elv

use str
use math

fn input_data []{
  each [line]{
    put [(str:split ' ' $line)]
  }
}

fn fahrenheit_to_celsius [degrees]{
  echo (math:round (* (/ 5 9) (- $degrees 32)))
}

fn calculate [data]{
  each [value]{
    fahrenheit_to_celsius $value
  } $data[1..]
}

echo (str:join ' ' [(calculate (input_data))])

# cat DATA.lst | elvish alejotru3012.elv
# 75 32 94 75 233 138 49 11 77 257 281 188 249 314 149 131 128 170 216
# 84 88 175 227 41 254 111 202 268 49 168 92 297 136 112 12 217 161 104 18
