{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

fromIntegerNat : Int -> Nat
fromIntegerNat 0 = Z
fromIntegerNat number =
  if (number > 0) then
    S (fromIntegerNat (assert_smaller number (number - 1)))
  else
    Z

toIntegerNat : Nat -> Int
toIntegerNat Z = 0
toIntegerNat (S k) = 1 + toIntegerNat k

elemAt : Int -> List a -> a
elemAt 0 (x::xs) = x
elemAt index (_::xs) = elemAt (index - 1) xs

matrixAt : Int -> Int -> List (List a) -> a
matrixAt posX posY matrix = elemAt posY (elemAt posX matrix)

replaceElement : Nat -> a -> List a -> List a
replaceElement index x xs =
  let
    left = take index xs
    right = drop (index + 1) xs
  in left ++ [x] ++ right

changeElem : Int -> Int -> a -> List (List a) -> List (List a)
changeElem row col x xs =
  let
    rowToReplaceIn = elemAt row xs
    modifiedRow = replaceElement (fromIntegerNat col) x rowToReplaceIn
  in replaceElement (fromIntegerNat row) modifiedRow xs

injectValue : a -> IO a
injectValue dat = pure dat

getInputData : IO(List String)
getInputData = do
  line <- getLine
  if line == ""
    then injectValue []
    else do
      xs <- getInputData
      injectValue (line :: xs)

fotmatMovements : String -> Int
fotmatMovements mov =
  let
    divMov = unpack mov
    lstChar = fromMaybe ' ' (last' divMov)
    strRes =
      if lstChar == ','
        then pack (take (minus (length divMov) 1) divMov)
        else pack divMov
  in cast {to=Int} strRes

findArea : List (List Char) -> Char -> Int -> Int -> (Int, List (List Char))
findArea board selChar posX posY =
  let
    upLimit = toIntegerNat (minus (length board) 1)
    (occurs, finalBoard) =
      if posX < 0 || posY < 0 || posX > upLimit ||
        posY > upLimit || (matrixAt posX posY board) /= selChar
        then (0, board)
        else let
          nxtBoard = changeElem posX posY '-' board
          (upOccurs, upBoard) = findArea nxtBoard selChar (posX - 1) posY
          (rigthOccurs, rigthBoard) = findArea upBoard selChar posX (posY + 1)
          (downOccurs, downBoard) = findArea rigthBoard
            selChar (posX + 1) posY
          (leftOccurs, leftBoard) = findArea downBoard selChar posX (posY - 1)
          totalOccurs = upOccurs + rigthOccurs + downOccurs + leftOccurs + 1
        in (totalOccurs, leftBoard)
  in (occurs, finalBoard)

fallColumn : List Char -> Int -> Int -> Bool -> List Char
fallColumn column index insertPos mustInsert =
  let
    val = elemAt index column
    (nxtCol, nxtInsPos, nxtMustInsert) =
      if mustInsert && val /= '-'
        then (replaceElement (fromIntegerNat index) '-'
          (replaceElement (fromIntegerNat insertPos) val column),
            insertPos - 1, mustInsert)
        else if val /= '-'
          then (column, insertPos - 1, mustInsert)
          else (column, insertPos, True)
  in if index == 0
    then nxtCol
    else fallColumn nxtCol (index - 1) nxtInsPos nxtMustInsert

fallAllColumns : List Char -> List Char
fallAllColumns column =
  let
    size = toIntegerNat (minus (length column) 1)
    res = fallColumn column size size False
  in res

fallMatrix : List (List Char) -> Int -> Int -> Bool -> List (List Char)
fallMatrix matrix index insertPos mustInsert =
  let
    actVal = elemAt index matrix
    val = fromMaybe ' ' (last' actVal)
    empVal = replicate (length matrix) '-'
    (nxtMat, nxtInsPos, nxtMustInsert) =
      if mustInsert && val /= '-'
        then (replaceElement (fromIntegerNat index) empVal
          (replaceElement (fromIntegerNat insertPos) actVal matrix),
            insertPos + 1, mustInsert)
        else if val /= '-'
          then (matrix, insertPos + 1, mustInsert)
          else (matrix, insertPos, True)
  in if index == toIntegerNat (minus (length matrix) 1)
    then nxtMat
    else fallMatrix nxtMat (index + 1) nxtInsPos nxtMustInsert

calcScore : List (List Char) -> List Int -> Int
calcScore board [] = 0
calcScore board movements =
  let
    iYMov = fromMaybe 0 (head' movements)
    iXMov = (toIntegerNat (length board)) - 1 - (elemAt 1 movements)
    selChar = matrixAt iXMov iYMov board
    (iScore, nxtBoard) =
      if selChar == '-'
        then (0, board)
        else let
          (count, auxBoard) = findArea board selChar iXMov iYMov
          horFixedBoard = map fallAllColumns (transpose auxBoard)
          vertFixedBoard = fallMatrix horFixedBoard 0 0 False
          nxtBoard = transpose vertFixedBoard
          iScore = div (count * (count + 1)) 2
        in (iScore, nxtBoard)
  in iScore + (calcScore nxtBoard (drop 2 movements))

main : IO ()
main = do
  inputData <- getInputData
  let
    intSizeInput = cast {to=Int} (elemAt 0 inputData)
    board = map unpack (take (fromIntegerNat intSizeInput) (drop 1 inputData))
    fotmatMov = map fotmatMovements
      (words (elemAt (intSizeInput + 2) inputData))
    solution = [show (calcScore board fotmatMov)]
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  1080
-}
