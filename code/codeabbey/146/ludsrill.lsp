#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect line)))

(defun split-by-comma (item)
  (loop for i = 0 then (1+ j)
    as j = (position #\, item :start i)
    collect (subseq item i j)
    while j))

(defun split-by-one-space (item)
  (loop for i = 0 then (1+ j)
    as j = (position #\Space item :start i)
    collect (subseq item i j)
    while j))

(defun join-string-list (string-list)
  (format nil "~{~A~^~} " string-list))

(defun organize-data ()
  (let ((aux)
        (aux-colors)
        (collect)
        (color-cubes)
        (colors)
        (coordinates)
        (data)
        (movements))
    (setq data (read-data))
    (setq color-cubes ())
    (loop for i from 1 to (- (length data) 1) do
      (when (<= i (parse-integer (first data)))
        (setq color-cubes (append color-cubes (list (elt data i)))))
      (when (> i (+ (parse-integer (first data)) 1))
        (setq movements (first (last data)))))

    (setq collect ())
    (setq movements (split-by-one-space (join-string-list
      (split-by-comma movements))))

    (loop for i from 0 to (- (length movements) 1) do
      (if (equal (elt movements i) '"")
        NIL
        (setq collect (append collect
          (list (parse-integer (elt movements i)))))))

    (setq coordinates ())
    (loop for i from 0 to (- (length collect) 2) by 2 do
      (setq aux (subseq collect i (+ i 2)))
      (setq coordinates (append coordinates (list (list
        (- (- (length (first color-cubes)) 1) (second aux)) (first aux))))))

    (setq colors ())
    (dolist (item color-cubes)
      (setq aux-colors ())
      (loop for i across item do
        (setq aux-colors (append aux-colors (list (digit-char-p i)))))
      (setq colors (append colors (list aux-colors))))

    (return-from organize-data (list colors coordinates))))

(defun gravity (color-cubes)
  (let ((pos))
    (loop for i from 0 to (- (length color-cubes) 1) do
      (loop for j from 0 to (- (length (first color-cubes)) 1) do
        (when (= (elt (elt color-cubes i) j) 0)
          (loop for k from i :downto 1 do
            (setf (elt (elt color-cubes k) j)
              (elt (elt color-cubes (- k 1)) j))
            (setf (elt (elt color-cubes (- k 1)) j) 0)))))

    (loop for i from 0 to (- (length (first (last color-cubes))) 1) do
      (when (= (elt (first (last color-cubes)) i) 0)
        (setq pos (position '0 (first (last color-cubes))))
          (loop for i from 0 to (- (length color-cubes) 1)do
            (loop for j from pos to (- (length (first color-cubes)) 2) do
              (setf (elt (elt color-cubes i) j)
                (elt (elt color-cubes i) (+ j 1)))
              (setf (elt (elt color-cubes i) (+ j 1)) 0)))))))

(defun remove-item-and-find-adjacent (item color-cubes)
  (let ((aux-coordinates)
        (color)
        (color-down)
        (color-up)
        (color-right)
        (color-left))

    (setq aux-coordinates ())
    (setq color (elt (elt color-cubes (first item)) (second item)))

    (when (>= (- (second item) 1) 0)
      (setq color-left
        (elt (elt color-cubes (first item)) (- (second item) 1)))

      (cond ((and (/= color 0) (= color color-left))
              (setq aux-coordinates (append aux-coordinates
                (list (list (first item) (- (second item) 1))))))))

    (when (<= (+ (second item) 1) (- (length (first color-cubes)) 1))
      (setq color-right
        (elt (elt color-cubes (first item)) (+ (second item) 1)))

      (cond ((and (/= color 0) (= color color-right))
              (setq aux-coordinates (append aux-coordinates
                (list (list (first item) (+ (second item) 1))))))))

    (when (>= (- (first item) 1) 0)
      (setq color-up (elt (elt color-cubes (- (first item) 1)) (second item)))
      (cond ((and (/= color 0) (= color color-up))
              (setq aux-coordinates (append aux-coordinates
                (list (list (- (first item) 1) (second item))))))))

    (when (<= (+ (first item) 1) (- (length color-cubes) 1))
      (setq color-down
        (elt (elt color-cubes (+ (first item) 1)) (second item)))

      (cond ((and (/= color 0) (= color color-down))
              (setq aux-coordinates (append aux-coordinates
                (list (list (+ (first item) 1) (second item))))))))

    (when (/= (elt (elt color-cubes (first item)) (second item)) 0)
      (setf (elt (elt color-cubes (first item)) (second item)) 0))

    (return-from remove-item-and-find-adjacent aux-coordinates)))

(defun get-coordinates (coordinates color-cubes counter)
  (let ((aux-coordinates)
        (aux-color-cubes)
        (group))
    (loop while (> (length coordinates) 0) do
      (dolist (item coordinates)
        (when (/= (elt (elt color-cubes (first item)) (second item)) 0)
          (setq counter (+ counter 1)))
        (setq aux-color-cubes color-cubes)
        (setq aux-coordinates (remove-item-and-find-adjacent item color-cubes))
        (when (> (length aux-coordinates) 0)
          (setq group (get-coordinates aux-coordinates color-cubes counter))
          (setq aux-coordinates (first group))
          (setq counter (second group))))

      (setq coordinates aux-coordinates))
    (return-from get-coordinates (list aux-coordinates counter))))

(defun color-cube ()
  (let ((aux-coordinates)
        (color-cubes)
        (coordinates)
        (counter 0)
        (organized-data)
        (score 0)
        (group))
    (setq organized-data (organize-data))
    (setq color-cubes (first organized-data))
    (setq coordinates (second organized-data))
    (setq aux-coordinates ())
    (dolist (item coordinates)
      (setq group (get-coordinates (list item) color-cubes counter))
      (setq counter (second group))
      (gravity color-cubes)
      (setq score (+ score (* counter (/ (+ counter 1) 2))))
      (setq counter 0))
    (print score)))

(color-cube)

#|
cat DATA.lst | clisp ludsrill.lsp
1262
|#
