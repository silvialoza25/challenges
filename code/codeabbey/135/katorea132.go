/*
$ golint -min_confidence 0.5 -set_exit_status katorea132.go

$ go build katorea132.go

*/
package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strconv"
)

func mapInitializer() map[string]string {
	bitsTable := map[string]string{
		" ": "11", "e": "101", "t": "1001", "o": "10001",
		"n": "10000", "a": "011", "s": "0101", "i": "01001",
		"r": "01000", "h": "0011", "d": "00101", "l": "001001",
		"!": "001000", "u": "00011", "c": "000101", "f": "000100",
		"m": "000011", "p": "0000101", "g": "0000100", "w": "0000011",
		"b": "0000010", "y": "0000001", "v": "00000001", "j": "000000001",
		"k": "0000000001", "x": "00000000001", "q": "000000000001",
		"z": "000000000000"}
	return bitsTable
}

func binToHex(bitString string) string {
	transform, _ := strconv.ParseUint(bitString, 2, 8)
	return fmt.Sprintf("%02X", transform)
}

func stringIterator(runes []rune, idx int, buff *bytes.Buffer, length int) {
	bitsTable := mapInitializer()
	if idx < length {
		buff.WriteString(bitsTable[fmt.Sprintf("%c", runes[idx])])
		stringIterator(runes, idx+1, buff, length)
	}
}

func bitOctetFixer(extraZeroes int, counter int, buff *bytes.Buffer) {
	if counter < extraZeroes {
		buff.WriteString("0")
		bitOctetFixer(extraZeroes, counter+1, buff)
	}
}

func stringToHex(low int, high int, length int, bitString string) {
	if high <= length {
		fmt.Printf("%v ", binToHex(bitString[low:high]))
		stringToHex(low+8, high+8, length, bitString)
	}
}

func main() {
	var buffer bytes.Buffer

	info := bufio.NewReader(os.Stdin)
	phrase, _ := info.ReadString('\n')
	runes := []rune(phrase)
	fmt.Println(len(runes))
	stringIterator(runes, 0, &buffer, len(runes))
	trailingZeros := len(buffer.String()) % 8
	if trailingZeros != 0 {
		bitOctetFixer(8-trailingZeros, 0, &buffer)
	}
	bitString := buffer.String()
	stringToHex(0, 8, len(bitString), buffer.String())

}

/*
$ cat DATA.lst | ./katorea132
60 D8 1E 23 46 40 A6 D1 35 0B B0 51 25 2A 69 80 4E 23 46 1C 56 70 16 48 D8 22
6E 41 2C 1A EE 05 D9 33 50 98 04 C4 1C 0A C3 B0 96 49 20 72 00 23 28 4A B9 68
5B 02 D8 4F 18 66 95 C8 06 92 49 D9 31 85 C4 8A 39 3B CD 80 D1 78 89 C9 D4 A3
88 82 24 5A BB 81 79 3B 89 14 72 77 45 16 53 23 2D E2 27 11 A3 4C 26 C2 53 18
2E 58 E9 87 27 72 20 C3 BB 81 70 40 72 18 E4 E2 84 C8 1E 22 72 74
*/
