/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, char.

:- pred char2code(char::in,string::out) is det.
char2code(Char, Code) :-
(
  if Char = ' ' then Code = "11"
  else if Char = 'e' then Code = "101"
  else if Char = 't' then Code = "1001"
  else if Char = 'o' then Code = "10001"
  else if Char = 'n' then Code = "10000"
  else if Char = 'a' then Code = "011"
  else if Char = 's' then Code = "0101"
  else if Char = 'i' then Code = "01001"
  else if Char = 'r' then Code = "01000"
  else if Char = 'h' then Code = "0011"
  else if Char = 'd' then Code = "00101"
  else if Char = 'l' then Code = "001001"
  else if Char = 'u' then Code = "00011"
  else if Char = 'c' then Code = "000101"
  else if Char = 'f' then Code = "000100"
  else if Char = 'm' then Code = "000011"
  else if Char = 'p' then Code = "0000101"
  else if Char = 'g' then Code = "0000100"
  else if Char = 'w' then Code = "0000011"
  else if Char = 'b' then Code = "0000010"
  else if Char = 'y' then Code = "0000001"
  else if Char = 'v' then Code = "00000001"
  else if Char = 'j' then Code = "000000001"
  else if Char = 'k' then Code = "0000000001"
  else if Char = 'x' then Code = "00000000001"
  else if Char = 'q' then Code = "000000000001"
  else if Char = 'z' then Code = "000000000000"
  else Code = "001000" %to translate '!' to code
).

:- pred unwords(string::in, string::in, string::out) is det.
unwords(X, Y, X ++ " " ++ Y).

:- pred condense(string::in, string::in, string::out) is det.
condense(X, Y, X ++ Y).

:- pred bin2hex(string::in, string::out) is det.
bin2hex(X, int_to_base_string(det_base_string_to_int(2, X), 16)).

:- func padBits(string) = string.
padBits(String) =
(
  if length(String) mod 8 = 0
  then String
  else padBits(String ++ "0")
).

:- pred padHex(string::in, string::out) is det.
padHex(InputStr, PaddedStr) :- %add a "0" prefix to Hex strings < A
(
  if length(InputStr) = 1
  then PaddedStr = "0" ++ InputStr
  else PaddedStr = InputStr
).

:- pred str2list(string::in, list(string)::in, list(string)::out) is det.
str2list(String, PStrList, StrList) :-
(
  if length(String) = 0
  then StrList = PStrList
  else
    split_by_codepoint(String, 8, Head, Tail),
    PStrList1 = PStrList ++ [Head],
    str2list(Tail, PStrList1, StrList)
).

:- pred encodeMsg(string, string).
:- mode encodeMsg(in, out) is det.
encodeMsg(InputMsg, HexStr) :-
(
  to_char_list(InputMsg, CharList),
  map(char2code, CharList, EncodedList),
  foldr(condense, EncodedList, "", BinEncodedStr),
  padBits(BinEncodedStr) = PaddedBinStr,
  str2list(PaddedBinStr, [], BinEncodedList),
  map(bin2hex, BinEncodedList, HexEncodedList),
  map(padHex, HexEncodedList, PaddedHexEncodedList),
  foldr(unwords, PaddedHexEncodedList, "", HexStr)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, "", !IO).

:- pred read_lines(io.text_input_stream::in,
  string::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ Line,
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    remove_suffix_if_present("\n", FileContents) = RawStr,
    encodeMsg(RawStr, HexStr),
    io.print_line(HexStr, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  4C 16 50 30 EC 27 12 28 D3 09 44 4A 31 53 00 9C 9D D5 87 76 09 62
  48 E6 E8 19 37 4C 26 3C 9D 5B AC B9 AB 90 24 51 CB 00 53 00 9A 61
  C9 DC 3A E1 CA 43 BD 00 21 62 B4 BC C7 64 93 C9 DC AE 01 2A 17 88
  9A 60 02 D5 31 86 22 22 1E 0D 32 71 1C EE 05 DA 2E 43 04 B1 24 06
  97 11 11 0F 64 93 90 C9 26 84 4B 80 B7 98 F2 77 20 12 13 29 53 C8
  15 11 07 0D C0 BC 9B 9D 92 4E 16 24 A6 52 2B 24
*/
