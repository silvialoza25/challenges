# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.03s to load, 0.1s running 55 checks on 1 file)
# 8 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule VariableLengthCode do

  def main do
    data = String.graphemes(String.replace(IO.read(:stdio, :all),
            ~r"[\n]", ""))
    encode = table()
    binary_data = encoder(data, encode, "")
    Enum.map(to_hex(binary_data, []), fn(x) -> if String.length(x) == 2 do
              IO.write("#{x} ") else IO.write("0#{x} ") end end)
  end

  def encoder([head | tail], encode, accumulator) do
    encoder(tail, encode, accumulator <> Map.get(encode, head))
  end

  def encoder([], _ , accumulator) do
    Regex.scan(~r/.{1,8}/, accumulator)
  end

  def to_hex([head | tail], accumulator) do
      aux = Integer.to_string(List.first(Tuple.to_list(Integer.parse(
              to_complete(List.first(head)), 2))), 16)
      to_hex(tail, accumulator ++ [aux])
  end

  def to_hex([], accumulator) do
      accumulator
  end

  def to_complete(binary) do
    if String.length(binary) < 8 do
      to_complete(binary <> "0")
    else
      binary
    end
  end

  def table do
    %{" " => "11",         "e" =>  "101",        "t" => "1001",
      "o" => "10001",      "n" => "10000",       "a" => "011",
      "s" => "0101",       "i" => "01001",       "r" => "01000",
      "h" => "0011",       "d" => "00101",       "l" => "001001",
      "!" => "001000",     "u" => "00011",       "c" => "000101",
      "f" => "000100",     "m" => "000011",      "p" => "0000101",
      "g" => "0000100",    "w" => "0000011",     "b" => "0000010",
      "y" => "0000001",    "v" => "00000001",    "j" => "000000001",
      "k" => "0000000001", "x" => "00000000001", "q" => "000000000001",
      "z" => "000000000000"}
  end
end
# cat DATA.lst | elixir -r ludsrill.exs -e VariableLengthCode.main
# 05 6C 39 3B 85 72 9B 09 D4 62 09 50 E0 2D E2 27 27 56 E4 0B 12 63 04 D5 DC 0B
# A8 C5 3D 2B C2 20 F9 3B C2 8B 55 53 20 79 8E 4D 01 03 90 0D A3 16 30 14 91 96
# E4 05 AC 5B 8B 18 4A 18 B3 90 C9 25 2E 02 D5 EA CB 04 4A 54 F2 05 88 61 D4 0B
# 77 02 F3 1C B1 D9 24 F2 77 34 84 3C 85 AD 79 8E 0A B4 64 34 06 BB 08 D3 05 9E
# 4E A5 19 02 C4 70 94 01 E6 38 2A 2C 43 BC 9D E8 01 A2 8E 53 18 54 28
