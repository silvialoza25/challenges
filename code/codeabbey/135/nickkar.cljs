;; $ clj-kondo --lint nickkar.cljs
;; linting took 56ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(defn binary-rep [ch]
  (let [bin (hash-map (keyword " ") "11" :e "101"
                      :t "1001" :o "10001"
                      :n "10000" :a "011"
                      :s "0101" :i "01001"
                      :r "01000" :h "0011"
                      :d "00101" :l "001001"
                      :! "001000" :u "00011"
                      :c "000101" :f "000100"
                      :m "000011" :p "0000101"
                      :g "0000100" :w "0000011"
                      :b "0000010" :y "0000001"
                      :v "00000001" :j "000000001"
                      :k "0000000001" :x "00000000001"
                      :q "000000000001" :z "000000000000")]
    (get bin (keyword (str ch)))))

(defn hex-rep [bin]
  (let [hexrep (hash-map :0000 "0" :0001 "1"
                         :0010 "2" :0011 "3"
                         :0100 "4" :0101 "5"
                         :0110 "6" :0111 "7"
                         :1000 "8" :1001 "9"
                         :1010 "A" :1011 "B"
                         :1100 "C" :1101 "D"
                         :1110 "E" :1111 "F")]
    (get hexrep (keyword bin))))

(defn complete-tail ([s](complete-tail s (- 8 (mod (count s) 8))))
  ([s i]
   (cond
     (= i 0) s
     :else (complete-tail (str s "0") (dec i))
     )
   ))

(defn merge-bits ([input] (merge-bits input 0 ""))
  ([input i out]
   (cond
     (>= i (count input)) out
     :else (merge-bits input (inc i) (str out (nth input i))))))

(defn to-hex ([input] (to-hex input 0 []))
  ([input i out]
   (cond
     (>= i (count input)) out
     :else
     (let [byte (map (partial apply str) (partition-all 4 (nth input i)))
           f (nth byte 0)
           s (nth byte 1)]
       (to-hex input (inc i) (conj out (str (hex-rep f) (hex-rep s)))))
     )
   ))

(defn main []
  (let [input (core/read-line)
        bin-inp (map binary-rep input)
        merged (complete-tail (merge-bits bin-inp))
        byte-rep (map (partial apply str) (partition-all 8 merged))]
    (apply println (to-hex byte-rep))
    ))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 60 D8 1E 23 46 40 A6 D1 35 0B B0 51 25 2A 69 80 4E 23 46 1C 56 70 16 48 D8 22
;; 6E 41 2C 1A EE 05 D9 33 50 98 04 C4 1C 0A C3 B0 96 49 20 72 00 23 28 4A B9 68
;; 5B 02 D8 4F 18 66 95 C8 06 92 49 D9 31 85 C4 8A 39 3B CD 80 D1 78 89 C9 D4 A3
;; 88 82 24 5A BB 81 79 3B 89 14 72 77 45 16 53 23 2D E2 27 11 A3 4C 26 C2 53 18
;; 2E 58 E9 87 27 72 20 C3 BB 81 70 40 72 18 E4 E2 84 C8 1E 22 72 74
