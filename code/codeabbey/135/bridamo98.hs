{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Numeric
import Data.Char
import Control.Monad

main = do
  inputData <- getLine
  let encodedSolution = encodeMessage inputData
  let joinedSolution = intercalate "" encodedSolution
  let paddSolution = joinedSolution ++ replicate
                     (8 - length joinedSolution `mod` 8) '0'
  let divSolution = divInOctets paddSolution
  let solution = map (decToHex . toDec) divSolution
  putStrLn (unwords solution)

decToHex :: Int -> String
decToHex dec = fixedHex
  where
    hex = map toUpper (showHex dec "")
    fixedHex = if length hex == 2
                 then hex
                 else "0" ++ hex

encodeMessage :: String -> [String]
encodeMessage [] = []
encodeMessage dat = res
  where
    iCode = getCode (head dat)
    res = iCode : encodeMessage (drop 1 dat)

getCode :: Char -> String
getCode char = res
  where
    res
      | char == ' ' = "11"
      | char == 'e' = "101"
      | char == 't' = "1001"
      | char == 'o' = "10001"
      | char == 'n' = "10000"
      | char == 'a' = "011"
      | char == 's' = "0101"
      | char == 'i' = "01001"
      | char == 'r' = "01000"
      | char == 'h' = "0011"
      | char == 'd' = "00101"
      | char == 'l' = "001001"
      | char == '!' = "001000"
      | char == 'u' = "00011"
      | char == 'c' = "000101"
      | char == 'f' = "000100"
      | char == 'm' = "000011"
      | char == 'p' = "0000101"
      | char == 'g' = "0000100"
      | char == 'w' = "0000011"
      | char == 'b' = "0000010"
      | char == 'y' = "0000001"
      | char == 'v' = "00000001"
      | char == 'j' = "000000001"
      | char == 'k' = "0000000001"
      | char == 'x' = "00000000001"
      | char == 'q' = "000000000001"
      | char == 'z' = "000000000000"

divInOctets::[a] -> [[a]]
divInOctets [] = []
divInOctets l = take 8 l : divInOctets (drop 8 l)

toDec :: String -> Int
toDec = foldl' (\acc x -> acc * 2 + digitToInt x) 0

{-
$ cat DATA.lst | ./bridamo98
  4C AE 24 47 02 B9 4C 61 C6 1A 8C 53 C2 A1 30 15 21 49 AB B8
  17 8A 02 38 24 00 13 00 9A 65 70 B1 07 50 BA 61 A8 C5 3C 48
  A0 3E 63 83 34 8A 7C 9D 03 B4 5D 8A 28 D6 62 1D 2E 40 21 CE
  0C EC 37 93 18 04 E5 0D 30 E2 26 C1 0D 6A EE 05 C6 A3 40 57
  29 8C 17 06 99 3E 32 75 0B CC 75 18 20 0D 16 71 AF 31 DE 01
  1A 12 A5 48 B2 98 C3 12 28 A9 09 0E 63 C4 68 C5 8C 16 53 23
  94 C6 1B 81 40
-}
