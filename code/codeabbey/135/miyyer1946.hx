/**
linting
$ cp miyyer1946.hx Main.hx
$ haxelib run checkstyle -s Miyyer1946.hx
Running Checkstyle v2.6.1 using 79/79 checks on 1 source file...
No issues found.
**/

class Main {
  static public function main():Void {
    var data = Sys.stdin().readLine();
    var value:String = concatenation(data,0,"");
    var format_binary = format_binary(value);
    var position = (format_binary.length / 8);
    var array = div_in_array(format_binary,format_binary.length,"",0,[]);
    var result = convert_to_hex(array, 1, Std.int(position),"");
    trace(result);
  }

  static function convert_to_hex(data:Array <String>, cont:Int, position:Int,
      result:String):String {
    result += Std.string(StringTools.hex(Std.int(binaryToDec(data[cont],0,0,
      data[cont].length)))) + " ";
    if (cont >= position) {
      return(result);
    }
    return(convert_to_hex(data,cont + 1, position, result));
  }

  static function div_in_array(input:String, pos:Int, conversion:String,
      cont:Int, result:Array <String>):Array <String> {
    result.push(conversion);
    if (cont >= pos) {
      return(result);
    }
    return(div_in_array(input,pos,conversion=input.substr(cont,8),
      cont + 8,result));
  }

  static function binaryToDec(s:String, value:Float, power:Float,
      cont:Int):Float {
    if(s.charAt(cont-1) == '1'){
      value += power_calculation(power);
    }
    if (cont < 0) {
      return(value);
    }
    return binaryToDec(s,value,power + 1, cont - 1);
  }

  static function concatenation(data:String, cont:Int, result:String):String {
    var map2:Map<String,String> = [
    ' ' => '11',
    'e' => '101',
    't' => '1001',
    'o' => '10001',
    'n' => '10000',
    'a' => '011',
    's' => '0101',
    'i' => '01001',
    'r' => '01000',
    'h' => '0011',
    'd' => '00101',
    'l' => '001001',
    '!' => '001000',
    'u' => '00011',
    'c' => '000101',
    'f' => '000100',
    'm' => '000011',
    'p' => '0000101',
    'g' => '0000100',
    'w' => '0000011',
    'b' => '0000010',
    'y' => '0000001',
    'v' => '00000001',
    'j' => '000000001',
    'k' => '0000000001',
    'x' => '00000000001',
    'q' => '000000000001',
    'z' => '000000000000'
    ];
    if (cont >= data.length){
      return(result);
    }
    return (concatenation(data,cont + 1,result+=map2[data.charAt(cont)]));
  }

  static function format_binary(input:String):String {
    if ((input.length % 8) == 0) {
      return(input);
    }
    return (format_binary(input += "0"));
  }

  static function power_calculation(power:Float):Float {
    return (Math.pow(2,power));
  }
}

/**
$ cat DATA.lst | haxe -main Main --interp
Miyyer1946.hx:55: 4C 16 50 30 EC 27 12 28 D3 09 44 4A 31 53 00 9C 9D D5 87
76 09 62 48 E6 E8 19 37 4C 26 3C 9D 5B AC B9 AB 90 24 51 CB 00 53 00 9A 61
C9 DC 3A E1 CA 43 BD 00 21 62 B4 BC C7 64 93 C9 DC AE 01 2A 17 88 9A 60 02
D5 31 86 22 22 1E 0D 32 71 1C EE 05 DA 2E 43 04 B1 24 06 97 11 11 0F 64 93
90 C9 26 84 4B 80 B7 98 F2 77 20 12 13 29 53 C8 15 11 07 0D C0 BC 9B 9D 92
4E 16 24 A6 52 2B 24
**/
