{-
 $ ghc  miyyer1946.hs
   [1 of 1] Compiling Main ( miyyer1946.hs )
   Linking code ...
 $ hlint miyyer1946.hs
   No hints
-}

main = do
  n <- readLn :: IO Int
  readPairs n

readPairs :: Int -> IO ()
readPairs n = do
  array <- getLine
  let notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
  let xs = map read $ words array :: [Float]
  let value = (logBase 2 ((head xs * 1024) / 440) + 1 / 24) * 12 + 9
  let index = floor value `mod` 12
  let number = floor (value / 12 - 6)
  putStr (notes !! index)
  putStr $ show number
  putStr " "
  readPairs (n - 1)

{-
 $ cat DATA.lst | ./miyyer1946
 G#3 A1 C3 C5 F1 A#5 C4 G#1 E4 A4 D4 B3 E2 D2 B1 C#2 F5 F#2 E5 G4 F#4
-}
