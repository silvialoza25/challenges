-- $ curry-verify mankar91.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `mankar91'!

import Float

noteList :: [[Prelude.Char]]
noteList = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]

noteName :: Prelude.Int -> [Prelude.Char]
noteName steps = noteList !! ((9 + steps) `mod` 12)

octave :: Prelude.Integral a => a -> a
octave steps = (12 * 4 + 9 + steps) `div` 12

getNote :: Prelude.Float -> [Prelude.Char]
getNote frequency = (noteName steps) ++ show (octave steps)
  where steps = getSteps frequency

getSteps :: Prelude.Float -> Prelude.Int
getSteps frequency = round (logBase 2 ((frequency / 440) ^. 12))

main :: Prelude.IO ()
main = do
  getLine
  input <- getLine
  let
    frequencies = map read (words input)
    result = map getNote frequencies
  putStrLn (unwords result)

-- $ cat DATA.lst | pakcs -q :load mankar91.curry :eval main :quit
-- G#5 E5 F5 B4 G1 G3 D#3 G4 D2 F3 C1 A3 D#2 D3 C3 B3 B1 G5 F#1 G#2 A1
