# $ lintr::lint('nickkar.r')

options(digits = 12)

findsinglenote <- function(hz, bases, scale=0, i=1, argmin=1, minscale=0) {
    afreq <- (2^scale) * bases[i]
    ifreq <- (2^minscale) * bases[argmin]
    if (i > length(bases)) {
        return(c(argmin, minscale))
    }
    else if (scale > 12) {
        findsinglenote(hz, bases, 0, i + 1, argmin, minscale)
    }
    else if ((abs(hz - afreq)) < (abs(hz - ifreq))) {
        findsinglenote(hz, bases, scale + 1, i, i, scale)
    }
    else {
        findsinglenote(hz, bases, scale + 1, i, argmin, minscale)
    }
}

findnotes <- function(hz, bases, out=c(), i=1) {
    if (i > length(hz)) {
        return(out)
    }
    else {
        temp <- findsinglenote(hz[i], bases)
        note <- switch(temp[1], "C", "C#", "D",
        "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B")
        str <- paste(note, temp[2], sep = "")
        findnotes(hz, bases, c(out, str), i + 1)
    }
}

findscale <- function(hz, out=c(), i=0) {
    if (i > length(hz)) {
        return(out)
    }
    tmp <- floor(log(hz / 16.35160) / log(2))
    return(c(out, tmp))
}

formatinput <- function(cases, out = c(), i=1) {
    if (i > length(cases)) {
        return(out)
    }
    else {
        tmp <- as.double(cases[i])
        formatinput(cases, (append(out, tmp)), i + 1)
    }
}

main <- function() {
    # frequency of notes in 0 scale
    bases <- c(16.35160, 17.32391, 18.35405, 19.44544, 20.60172, 21.82676,
            23.12465, 24.49971, 25.95654, 27.50000, 29.13524, 30.86771)
    data <- readLines("stdin")
    freq <- formatinput(unlist(strsplit(data[2], " ")))
    out <- findnotes(freq, bases)
    print(paste(out, collapse = " "))
}

main()

# $ cat DATA.lst | rscript nickkar.r
# [1] "D3 A3 B4 D#2 C1 C4 F#4 A4 D4 G#4 D#1 A1 A#2 G3 F4 D1 C#5 E4 G#3 E1"
