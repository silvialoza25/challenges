{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #089: Instrument Tuner - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stod : String -> Double
stod str = fromMaybe 0 $ parseDouble str

strAt : Int -> List String -> String
strAt 0 (x::xs) = x
strAt index (_::xs) = strAt (index - 1) xs

getNote : Int -> String
getNote halfSteps = let
  notation = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
  -- Get the note index in the notation array
  index = if halfSteps < 0 then 11 - (mod ((halfSteps - 2) * - 1) 12)
    else mod (halfSteps + 9) 12
  -- Get the octave
  octave = if halfSteps < 0 then 4 - div ((halfSteps - 2) * - 1) 12
    else 4 + div (halfSteps + 9) 12
  in
  strAt index notation ++ show octave

processTestCase : Double -> String
processTestCase freq = let
  -- Calculate musical half steps between the given
  -- frequency and the reference 440 Hz (A4)
  baseExp = pow 2.0 (1 / 12)
  halfSteps = floor ((log (freq / 440)) / (log baseExp) + 0.5)
  in
  getNote (cast halfSteps)

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLine
  let
    -- Parse input array
    testCases = map stod $ words inputData
    -- Process the range needed
    results = map processTestCase testCases
  -- Print results
  putStrLn $ unwords results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  B2 D1 C#4 A#4 F2 F5 F3 F#3 G1 B1 D5 D3 C3 B3 E4 B5 C#3 C#5 D#3
-}
