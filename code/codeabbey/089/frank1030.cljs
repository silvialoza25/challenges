;$ clj-kondo --lint frank1030.cljs
;linting took 542ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.pprint :refer [cl-format]]))

(defn get-data [i]
  (let [x (atom i) freq (core/read)
    notes ["C" "C#" "D" "D#" "E" "F" "F#" "G" "G#" "A" "A#" "B"]
    value (+ (* (+ (/ (Math/log (/ (* freq 1024) 440)) (Math/log 2))
      (/ 1 24)) 12) 9)
    note (nth notes (Math/floor (mod value 12)))
    octave (Math/floor (- (/ value 12) 6))]
    (if (>= @x 0)
      (do (swap! x dec)
        (print (cl-format nil "~d~d " note octave))
        (if (not= @x 0)
          (get-data @x)
          (print "")))
      (print ""))))

(defn main []
  (let [i (core/read)]
    (get-data i)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;B2 A#5 G#5 G#3 C4 A4 D5 C3 E2 F#2 A3 C#4 G#4 F#1 F2 A#3 G3 G#1
