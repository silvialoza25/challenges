# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.05s to load, 0.06s running 55 checks on 1 file)
# 6 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule InstrumentTuner do

  def main do
    data = to_float(String.split(hd(Enum.slice(String.split(
            IO.read(:stdio, :all), "\n"), 1..1)), " "))
    solution = Enum.map(data, &find_note(&1, 0))
    Enum.map(solution, &IO.write("#{&1} "))
  end

  def find_note(frequencie, counter) do
    cons = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"]
    cons_2 = ["A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A"]
    if frequencie < 440 do
      if frequencie >= get_limits(current_frequencie(counter), counter) do
          Enum.at(Enum.reverse(cons_2), rem(abs(counter), 12)) <>
            Integer.to_string(Kernel.ceil((46 - abs(counter)) / 12))
      else
        find_note(frequencie, counter - 1)
      end
    else
      if frequencie <= get_limits(current_frequencie(counter), counter) do
        Enum.at(cons, rem(counter, 12)) <> Integer.to_string(
                                            Kernel.ceil((46 + counter) / 12))
      else
        find_note(frequencie, counter + 1)
      end
    end
  end

  def current_frequencie(counter) do
    440 * :math.pow(1.059463, counter)
  end

  def get_limits(current_frequencie, counter) do
    if current_frequencie >= 440 do
      current_frequencie + (440 * :math.pow(1.059463, counter + 1) -
        current_frequencie) / 2
    else
      current_frequencie - (current_frequencie - 440 *
        :math.pow(1.059463, counter - 1)) / 2
    end
  end

  def to_float(list) do
    Enum.map(Enum.map(Enum.map(list, &Float.parse/1), &Tuple.to_list/1),
      &List.first/1)
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e InstrumentTuner.main
# E2 D2 D#5 C#1 G1 A2 C3 F#3 G#2 F4 C#3 B1 A#5 G5 C#5 C5 D3 E3 A#1 D5
