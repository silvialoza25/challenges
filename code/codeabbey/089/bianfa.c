/*
linter:
$ cppcheck --error-exitcode=1 bianfa.c && \
splint -strict -internalglobs -modfilesys -boundsread bianfa.c

Checking bianfa.c ...
Splint 3.1.2 --- 20 Feb 2018

Finished checking --- no warnings

compilation:
$ gcc bianfa.c -o bianfa -lm
*/

#include <stdio.h>
#include <math.h>

static int getOctave(double frequency) /*@*/ {
 double octave, base = log(2.0);

 if((int) frequency < 440) {
  octave = 4 - (log(440.0/frequency)/base);
 } else {
  octave = 4 + (log(frequency/440.0)/base);
 }
 octave = (octave + 0.8);

 return (int) octave;
}

static int getNote(double frequency, double currentFrequency,
int octave, int index) /*@*/ {
 double step, op, upperLimit = frequency + pow(2.0, (double) octave)/2.0,
 lowerLimit = frequency - pow(2.0, (double) octave)/2.0;
 step = fabs(9.0-index);
 op = pow(2.0, step/12);

 if ( (index < 9 && (currentFrequency / op) <= lowerLimit) ||
  (index >= 9 && (currentFrequency * op) <= lowerLimit) ) {
  return getNote(frequency, currentFrequency, octave, index + 1);
 } else if ( (index < 9 && (currentFrequency / op) >= upperLimit) ||
  (index >= 9 && (currentFrequency * op) >= upperLimit) ) {
  return getNote(frequency, currentFrequency, octave, index - 1);
 } else {
  return index;
 }
}

static int showNotes(int i, int size) /*@*/ {
 /*@observer@*/const char notes[][3] = { "C@", "C#", "D@", "D#", "E@", "F@",
 "F#", "G@", "G#", "A@", "A#", "B@" };
 double frequency = 66.1, currentFrequency;
 int scan, octave, noteIndex;

 scan = scanf("%lf", &frequency);

 octave = getOctave(frequency);

 if(octave <= 4) {
  currentFrequency = 440.0/(pow(2.0, fabs(4.0 - octave)));
 }else {
  currentFrequency = 440.0*(pow(2.0, fabs(4.0 - octave)));
 }
 noteIndex = getNote(frequency, currentFrequency, octave, 9);
 if ((int) notes[noteIndex][1] == 64) {
  printf("%c%d ", notes[noteIndex][0], octave);
 } else {
  printf("%s%d ", notes[noteIndex], octave);
 }

 if (i+1 < size) {
  return showNotes(i+1, size);
 } else {
  printf("\n");
  return scan;
 }
}

int main(void) {
 int scan, size;

 scan = scanf("%d", &size);

 return scan * showNotes(0, size);
}

/*
$ cat DATA.lst | ./bianfa
D3 A3 B4 D#2 C1 C4 F#4 A4 D4 G#4 D#1 A1 A#2 G3 F4 D1 C#5 E4 G#3 E1
*/
