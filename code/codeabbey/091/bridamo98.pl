#!/usr/bin/perl
# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 bridamo98.pl
# bridamo98.pl source OK
#
# Compile:
# $ perl -c bridamo98.pl
# bridamo98.pl syntax OK

package bridamo98;

use List::Util qw( max );
use English '-no_match_vars';
use warnings FATAL => 'all';
use POSIX qw(log2);
use strict;

our ($VERSION) = 1;

my $SPACE = q{ };

local $OUTPUT_FIELD_SEPARATOR = $SPACE;

my @board = ();
my @moves = ();

sub vertical_combination {
  my @data            = @_;
  my $j_i             = $data[0];
  my $step            = $data[1];
  my $i               = 0;
  my $j               = $j_i;
  my $last_tile       = 0;
  my $index_last_tile = 0;
  for ( 0 .. ( 2 + 1 ) ) {
    $i = $_;
    while ( $j <= ( 2 + 1 ) and $j >= 0 ) {
      if ( $board[$j]->[$i] == $last_tile and $last_tile != 0 ) {
        $board[$index_last_tile]->[$i] = $board[$j]->[$i] * 2;
        $last_tile                     = 0;
        $board[$j]->[$i]               = $last_tile;
      }
      elsif ( $board[$j]->[$i] != 0 ) {
        $last_tile       = $board[$j]->[$i];
        $index_last_tile = $j;
      }
      $j = $j + $step;
    }
    $j         = $j_i;
    $last_tile = 0;
  }
  return;
}

sub horizontal_combination {
  my @data            = @_;
  my $j_i             = $data[0];
  my $step            = $data[1];
  my $i               = 0;
  my $j               = $j_i;
  my $last_tile       = 0;
  my $index_last_tile = 0;
  for ( 0 .. ( 2 + 1 ) ) {
    $i = $_;
    while ( $j <= ( 2 + 1 ) and $j >= 0 ) {
      if ( $board[$i]->[$j] == $last_tile and $last_tile != 0 ) {
        $board[$i]->[$index_last_tile] = $board[$i]->[$j] * 2;
        $last_tile                     = 0;
        $board[$i]->[$j]               = $last_tile;
      }
      elsif ( $board[$i]->[$j] != 0 ) {
        $last_tile       = $board[$i]->[$j];
        $index_last_tile = $j;
      }
      $j = $j + $step;
    }
    $j         = $j_i;
    $last_tile = 0;
  }
  return;
}

sub vertical_move {
  my @data                   = @_;
  my $j_i                    = $data[0];
  my $step                   = $data[1];
  my $i                      = 0;
  my $j                      = $j_i;
  my $index_last_empty_field = 0 - 1;
  for ( 0 .. ( 2 + 1 ) ) {
    $i = $_;
    while ( $j <= ( 2 + 1 ) and $j >= 0 ) {
      if ( $board[$j]->[$i] != 0 and $index_last_empty_field != 0 - 1 ) {
        $board[$index_last_empty_field]->[$i] = $board[$j]->[$i];
        $board[$j]->[$i]                      = 0;
        $index_last_empty_field               = $index_last_empty_field + $step;
      }
      elsif ( $board[$j]->[$i] == 0 and $index_last_empty_field == 0 - 1 ) {
        $index_last_empty_field = $j;
      }
      $j = $j + $step;
    }
    $j                      = $j_i;
    $index_last_empty_field = 0 - 1;
  }
  return;
}

sub horizontal_move {
  my @data                   = @_;
  my $j_i                    = $data[0];
  my $step                   = $data[1];
  my $i                      = 0;
  my $j                      = $j_i;
  my $index_last_empty_field = 0 - 1;
  for ( 0 .. ( 2 + 1 ) ) {
    $i = $_;
    while ( $j <= ( 2 + 1 ) and $j >= 0 ) {
      if ( $board[$i]->[$j] != 0 and $index_last_empty_field != 0 - 1 ) {
        $board[$i]->[$index_last_empty_field] = $board[$i]->[$j];
        $board[$i]->[$j]                      = 0;
        $index_last_empty_field               = $index_last_empty_field + $step;
      }
      elsif ( $board[$i]->[$j] == 0 and $index_last_empty_field == 0 - 1 ) {
        $index_last_empty_field = $j;
      }
      $j = $j + $step;
    }
    $j                      = $j_i;
    $index_last_empty_field = 0 - 1;
  }
  return;
}

sub count {
  my @max = @{
    [
      max @{ $board[0] },
      max @{ $board[1] },
      max @{ $board[2] },
      max @{ $board[ 2 + 1 ] }
    ]
  };

  my @result = (0) x log2(@max);
  my $i      = 0;
  my $j      = 0;
  while ( $i <= ( 2 + 1 ) ) {
    while ( $j <= ( 2 + 1 ) ) {
      if ( $board[$i][$j] != 0 ) {
        $result[ log2( $board[$i][$j] ) - 1 ]++;
      }
      $j++;
    }
    $j = 0;
    $i++;
  }
  exit 1 if !print @result;
  return;

}

sub solve {
  for ( 0 .. $#moves ) {
    if ( $moves[$_] eq 'U' ) {
      vertical_combination( 0, 1 );
      vertical_move( 0, 1 );
    }
    if ( $moves[$_] eq 'D' ) {
      vertical_combination( 2 + 1, 0 - 1 );
      vertical_move( 2 + 1, 0 - 1 );
    }
    if ( $moves[$_] eq 'L' ) {
      horizontal_combination( 0, 1 );
      horizontal_move( 0, 1 );
    }
    if ( $moves[$_] eq 'R' ) {
      horizontal_combination( 2 + 1, 0 - 1 );
      horizontal_move( 2 + 1, 0 - 1 );
    }
  }
  return;
}

sub get_data {
  for ( 0 .. ( 2 + 1 ) ) {
    my @line = split $SPACE, <>;
    $board[$_]->[0]       = $line[0];
    $board[$_]->[1]       = $line[1];
    $board[$_]->[2]       = $line[2];
    $board[$_]->[ 2 + 1 ] = $line[ 2 + 1 ];
  }
  @moves = split $SPACE, <>;
  return;
}

sub main {
  get_data();
  solve();
  count();
  return;
}

main();
exit 1 if !print "\n";

# $ cat DATA.lst | perl bridamo98.pl
# 2 4 1 1
