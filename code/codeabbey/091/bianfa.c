/*
linter:
$ cppcheck --error-exitcode=1 bianfa.c && \
splint -strict -internalglobs -modfilesys -boundsread bianfa.c

Checking bianfa.c ...
Splint 3.1.2 --- 20 Feb 2018

Finished checking --- no warnings

compilation:
$ gcc bianfa.c -o bianfa
*/

#include <stdio.h>

static void clearLineL(int gameTable[][4], int i, int j)
/*@modifies gameTable[][4]@*/ /*@requires i <= 3@*/ {
 int iCurrent;

 iCurrent = i;
 for (i=i; i<4; i++) {
  if(gameTable[j][i] != -1) {
   gameTable[j][iCurrent] = gameTable[j][i];
   gameTable[j][i] = -1;
   iCurrent += 1;
  }
 }
}

static void playL(int gameTable[][4]) /*@modifies gameTable[][4]@*/ {
 int i, j;

 for (i=0; i<3; i++) {
  for (j=0; j<4; j++) {

   if(gameTable[j][i] == -1) {
    clearLineL(gameTable, i, j);
   }

   if(gameTable[j][i] == -1 || gameTable[j][i] != gameTable[j][i+1]) {
    /*@innercontinue@*/ continue;
   }

   gameTable[j][i] += gameTable[j][i+1];
   gameTable[j][i+1] = -1;
  }
 }
}

static void clearLineR(int gameTable[][4], int i, int j)
/*@modifies gameTable[][4]@*/ /*@requires i <= 3@*/ {
 int iCurrent;

 iCurrent = i;
 for (i=i; i>=0; i--) {
  if(gameTable[j][i] != -1) {
   gameTable[j][iCurrent] = gameTable[j][i];
   gameTable[j][i] = -1;
   iCurrent -= 1;
  }
 }
}

static void playR(int gameTable[][4]) /*@modifies gameTable[][4]@*/ {
 int i, j;

 for (i=0; i<3; i++) {
  for (j=0; j<4; j++) {

   if(gameTable[j][3-i] == -1) {
    clearLineR(gameTable, 3-i, j);
   }

   if(i == 0 || gameTable[j][3-i] == -1
   || gameTable[j][3-i] != gameTable[j][3-i+1]) {
    /*@innercontinue@*/ continue;
   }

   gameTable[j][i] += gameTable[j][3-i+1];
   gameTable[j][3-i+1] = -1;
  }
 }
}

static void clearLineU(int gameTable[][4], int i, int j)
/*@modifies gameTable[][4]@*/ /*@requires j <= 3@*/ {
 int iCurrent;

 iCurrent = i;
 for (i=i; i<4; i++) {
  if(gameTable[i][j] != -1) {
   gameTable[iCurrent][j] = gameTable[i][j];
   gameTable[i][j] = -1;
   iCurrent += 1;
  }
 }
}

static void playU(int gameTable[][4]) /*@modifies gameTable[][4]@*/ {
 int i, j;

 for (i=0; i<3; i++) {
  for (j=0; j<4; j++) {

   if(gameTable[i][j] == -1) {
    clearLineU(gameTable, i, j);
   }

   if(gameTable[i][j] == -1 || gameTable[i][j] != gameTable[i+1][j]) {
    /*@innercontinue@*/ continue;
   }

   gameTable[i][j] += gameTable[i+1][j];
   gameTable[i+1][j] = -1;
  }
 }
}

static void clearLineD(int gameTable[][4], int i, int j)
/*@modifies gameTable[][4]@*/ /*@requires j <= 3@*/ {
 int iCurrent;

 iCurrent = i;
 for (i=i; i>=0; i--) {
  if(gameTable[i][j] != -1) {
   gameTable[iCurrent][j] = gameTable[i][j];
   gameTable[i][j] = -1;
   iCurrent -= 1;
  }
 }
}

static void playD(int gameTable[][4]) /*@modifies gameTable[][4]*/ {
 int i, j;

 for (i=3; i>=0; i--) {
  for (j=0; j<4; j++) {

   if(gameTable[i][j] == -1) {
    clearLineD(gameTable, i, j);
   }

   if(gameTable[i][j] == -1 || gameTable[i][j] != gameTable[i-1][j]) {
    /*@innercontinue@*/ continue;
   }

   gameTable[i][j] += gameTable[i-1][j];
   gameTable[i-1][j] = -1;
  }
 }
}

static void runGame(int gameTable[][4]) /*@modifies gameTable[][4]*/ {
 int scan = 0;
 char piece;

 while(scan != -1) {
  scan = scanf("%c ", &piece);
  if (scan != -1) {
   switch(piece) {
    case 'U':
     playU(gameTable);
     /*@switchbreak@*/ break;

    case 'R':
     playR(gameTable);
     /*@switchbreak@*/ break;

    case 'D':
     playD(gameTable);
     /*@switchbreak@*/ break;

    case 'L':
     playL(gameTable);
     /*@switchbreak@*/ break;
   }
  }
 }
}

static int getHigher(int gameTable[][4]) /*@*/ {
 int higher = 0, i, j;

 for(i=0; i<4; i++) {
  for(j=0; j<4; j++) {
   if (gameTable[i][j] > higher) {
    higher = gameTable[i][j];
   }
  }
 }

 return higher;

}

static int showScore(int gameTable[][4]) /*@*/ {
 int flag, score, number, limit, i, j;
 flag = 0;
 number = 0;
 limit = getHigher(gameTable);

 while(flag == 0) {
  score = 0;
  number += 2;
  for (i=0; i<4; i++) {
   for (j=0; j<4; j++) {
    if(gameTable[i][j] == number) {
     score += 1;
    }
   }
  }

  if (number <= 4 || score > 0) {
   printf("%d ", score);
  }

  if(number > limit && score == 0) {
   flag = 1;
  }
 }

 printf("\n");
 return 0;
}

int main(void) {
 int i, scan, gameTable[4][4];

 for (i=0; i<4; i++) {
  scan = scanf("%d %d %d %d", &gameTable[i][0], &gameTable[i][1],
  &gameTable[i][2], &gameTable[i][3]);
 }

 runGame(gameTable);

 return showScore(gameTable) * scan;
}

/*
$ cat DATA.lst | ./bianfa
2 2 2 1
*/
