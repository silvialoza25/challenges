#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

$ clisp -c frank1030.lsp
Compiling file /Users/Frank/Desktop/frank/048/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/048/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-dat()
  (let ((dat))
    (setq dat(read))
  )
)

(defun sequence-collatz(x)
  (let ((c 0))
    (loop while (/= x 1)
      do (if (= (mod x 2) 0)
        (progn
          (setq x (/ x 2))
          (setq c (+ c 1))
        )
        (progn
          (setq x (+ (* x 3) 1))
          (setq c (+ c 1))
        )
      )
    )
    (return-from sequence-collatz c)
  )
)

(defun main()
  (let ((index 0) (x 0) (r 0))
    (setq index(get-dat))
    (loop for i from 1 to index
      do (setq x (get-dat))
      do (setq r (sequence-collatz x))
      do (print r)
    )
  )
)

(main)

#|
$ cat DATA.lst | clisp frank1030.lsp
34 9 1 18 37 1 33 30 55 6 45 128 11 15 37 16 18 102 109 18 29 9 19 106 14
|#
