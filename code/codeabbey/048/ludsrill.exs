# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.08 seconds (0.02s to load, 0.06s running 55 checks on 1 file)
# 5 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule CollatzSequence do

  def main do
  data = get_data()
  Enum.map(data, &collatz_sequence(&1, 0))
    |> Enum.map(&IO.write("#{&1} "))
  end

  def get_data do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    [_ | tail] = List.delete_at(all_data, 2)
    Enum.at(Enum.map(Enum.map(tail, &String.split(&1, " ")), &to_int/1), 0)
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def collatz_sequence(value, counter) do
    cond do
      rem(value, 2) == 0 -> collatz_sequence(div(value, 2), counter + 1)
      value != 1 -> collatz_sequence(3 * value + 1, counter + 1)
      true ->  counter
    end
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e CollatzSequence.main
# 48 44 14 114 24 6 12 89 95 34 115 17 79 23 27 123 21 26 149
