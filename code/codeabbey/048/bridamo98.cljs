;; $ clj-kondo --lint bridamo98.cljs
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-048
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn collatz-sequence[number]
  (loop [cont 0 i-number number]
    (if (= i-number 1) cont
      (let [next-i-number (if (= (mod i-number 2) 0)
        (quot i-number 2) (+ (* 3 i-number) 1))]
        (recur (+ cont 1) next-i-number)))))

(defn solve-all[size-input content-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (recur (+ i 1)
        (str result (collatz-sequence
        (edn/read-string (content-input i))) " ")) result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))
  content-input (str/split (core/read-line) #" ")]
    (println (solve-all size-input content-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 34 91 16 18 37 18 33 30 55 6 45 128 110 15 37 16 18 102 109 18 29 9 19 106 14
