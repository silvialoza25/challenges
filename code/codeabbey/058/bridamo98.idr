{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

elemAt : Int -> List a -> a
elemAt 0 (x::xs) = x
elemAt index (_::xs) = elemAt (index - 1) xs

getCardName : Double -> List String -> List String -> String
getCardName number suits ranks =
  let
    suitIndex = cast {to=Int} (floor (number / 13.0))
    rankIndex = mod (cast {to=Int} number) 13
    suit = elemAt suitIndex suits
    rank = elemAt rankIndex ranks
  in rank ++ "-of-" ++ suit

findNames : List Double -> List String -> List String -> List String
findNames [] suits ranks = []
findNames numbers suits ranks =
  let
    number = fromMaybe 0.0 (head' numbers)
    iRes = getCardName number suits ranks
  in iRes :: findNames (drop 1 numbers) suits ranks

main : IO ()
main = do
  inputSize <- getLine
  inputData <- getLine
  let numbers = map (cast {to=Double}) (words inputData)
  let suits = ["Clubs", "Spades", "Diamonds", "Hearts"]
  let ranks = ["2", "3", "4", "5", "6", "7", "8", "9",
    "10", "Jack", "Queen", "King", "Ace"]
  let solution = findNames numbers suits ranks
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  7-of-Hearts 3-of-Clubs Queen-of-Hearts 6-of-Hearts Ace-of-Spades
  Jack-of-Clubs 7-of-Clubs King-of-Hearts 10-of-Clubs Queen-of-Diamonds
  8-of-Hearts 9-of-Clubs Jack-of-Spades 6-of-Spades 5-of-Diamonds
  4-of-Hearts 7-of-Spades Jack-of-Diamonds 8-of-Clubs 4-of-Diamonds
-}
