"""pylint Your code has been rated at 10.00/10
Process finished with exit code 0"""
from typing import Tuple, List


def get_data() -> Tuple[int, List[int]]:
    """
    read the input and return the data
    """
    count: int = int(input())
    data: List[int] = [int(x) for x in input().split()]
    return count, data


def solve_name(cards_number: List[int], count: int,
               k: int = 0) -> None:
    """
    the function take the input data and calculate the card's name
    """
    suits: List[str] = ['Clubs', 'Spades', 'Diamonds', 'Hearts']
    ranks: List[str] = ['2', '3', '4', '5', '6', '7', '8', '9',
                        '10', 'Jack', 'Queen', 'King', 'Ace']
    if k != count:
        suit_value: float = cards_number[k] / 13
        suit: str = suits[int(suit_value)]

        rank_value: int = cards_number[k] % 13
        rank: str = ranks[rank_value]

        print('{0}-of-{1}'.format(rank, suit))
        solve_name(cards_number, count, k + 1)
    else:
        return


def main() -> None:
    """
    solve the challenge
    """
    count: int
    cards: List[int]
    count, cards = get_data()
    solve_name(cards, count)


main()

# $ cat DATA.lst | python3 juan6630.py
# 6-of-Diamonds 2-of-Diamonds 8-of-Hearts 3-of-Clubs 10-of-Diamonds
# 10-of-Hearts 8-of-Clubs King-of-Clubs 9-of-Spades 2-of-Hearts 5-of-Spades
# 3-of-Spades Queen-of-Diamonds Queen-of-Hearts Ace-of-Spades Ace-of-Hearts
# Ace-of-Diamonds 4-of-Spades 9-of-Diamonds Ace-of-Clubs 3-of-Diamonds
