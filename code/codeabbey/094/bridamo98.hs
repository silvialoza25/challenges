{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let intInputData = map convert inputData
  let solution = map solve intInputData
  putStrLn (unwords solution)

solve :: [Int] -> String
solve dat = res
  where
    toSquare = map (^2) dat
    res = show (sum toSquare)

convert :: String -> [Int]
convert dat = result
  where
    aux = words dat
    result = map read aux :: [Int]

{-
$ cat DATA.lst | ./bridamo98
  1718 1354 279 730 667 101 59 533 494 126 1470 146 1784
-}
