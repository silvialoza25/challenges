;; clj-kondo --lint ludsrill.cljs
;; linting took 54ms, errors: 0, warnings: 0

(ns ludsrill.084
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn divide-list [list count acc-vertex acc-weight]
  (if (seq list)
    (if (= (mod count 2) 0)
      (recur (rest list) (inc count) (concat acc-vertex [(first list)])
        acc-weight)
      (recur (rest list) (inc count) acc-vertex
        (concat acc-weight [(first list)])))
    [acc-vertex acc-weight]))

(defn random-generator [random acc vertex]
  (let [constant-A 445
        constant-C 700001
        module 2097152
        aux (mod (+ (* constant-A random) constant-C) module)]
    (if (<= (count acc) (- (* 4  vertex) 1))
      (recur aux (concat acc (list aux)) vertex)
      (map #(+ (mod % vertex) 1) acc))))

(defn modify-map [map weight vertex startVertex]
  (if (or (some #{startVertex} (first (get map vertex))) (= vertex startVertex))
    map
    (let [auxmap (update map vertex #(vector (conj (first %) startVertex)
           (conj (second %) weight)))
          finalmap (update auxmap startVertex #(vector (conj (first %) vertex)
           (conj (second %) weight)))]
    finalmap)))

(defn graph-generator [map weight vertex startVertex count]
  (if (empty? weight)
    map
    (if (< count 2)
      (let [auxMap (modify-map map (first weight) (first vertex) startVertex)]
      (recur auxMap (rest weight) (rest vertex) startVertex (inc count)))
      (recur map weight vertex (inc startVertex) 0))))

(defn make-hashmap [vertex map counter]
  (if (<= counter vertex)
    (recur vertex (assoc map counter [[][]]) (inc counter))
    map))

(defn make-hashmap-distance [vertex map counter start]
  (if (<= counter vertex)
    (if (= counter start)
      (recur vertex (assoc map counter 0) (inc counter) start)
      (recur vertex (assoc map counter 99999999) (inc counter) start))
    map))

(defn modify-distance-map [vertex weight distance-map start]
  (if (seq vertex)
    (if (< (+ (get distance-map start) (first weight))
           (get distance-map (first vertex)))
      (let [aux (assoc distance-map (first vertex) (+ (get distance-map start)
             (first weight)))]
      (recur (rest vertex) (rest weight) aux start))
      (recur (rest vertex) (rest weight) distance-map start))
    distance-map))

(defn get-following [distance-map new-visited]
  (let [sorted-map (sort-by second distance-map)]
    (if (some #{(first (first sorted-map))} new-visited)
      (recur (rest sorted-map) new-visited)
      (first (first sorted-map)))))

(defn dijkstra [map visited distance-map start count nodes]
  (let [neighbor (get map start)
        new-distance-map (modify-distance-map (first neighbor) (second neighbor)
          distance-map start)
        new-visited (conj visited start)
        following (get-following (into [] new-distance-map) new-visited)]
    (if (< count nodes)
        (recur map new-visited new-distance-map following (inc count) nodes)
        distance-map)))

(defn -main []
  (let [data (first (get-data))
        nodes (Integer. (first data))
        seed (Integer. (second data))
        starting-node (Integer. (nth data 2))
        randoms-values (random-generator seed [] nodes)
        vertex-weight (divide-list randoms-values 0 [] [])
        final-hashmap (graph-generator (make-hashmap nodes {} 1)
          (second vertex-weight) (first vertex-weight) 1 0)
        make-distance-map (make-hashmap-distance nodes {} 1 starting-node)
        solution (into (sorted-map) (dijkstra final-hashmap [] make-distance-map
          starting-node 1 nodes))]
    (print (str/join " " (map #(second %)solution)))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
; 2613 2553 2429 2918 3138 2548 2437 2806 3092 2596 2693 2492 2376 1916 2397
; 2994 1822 2703 2572 2643 2456 1189 2964 2344 1414 1801 1856 2435 1923 2413
; 2503 2932 2148 2121 1600 2017 2819 1832 2291 2910 2447 1736 1949 2225 2116
; 2946 2550 2542 2859 3192 2409 2576 1395 2350 2676 2156 2640 2318 3039 2407
; 2554 1759 2243 1829 2244 2727 2632 3010 2116 3135 2551 2449 2670 2714 2371
; 1931 2410 2678 2079 1691 2382 1977 2831 2738 2135 2972 1981 2445 2035 2168
; 2104 1430 1923 2182 2138 2073 2573 2679 2346 3164 2512 1963 2474 2011 3078
; 2227 2227 2658 2229 2629 2192 2150 2527 2073 1626 2031 2675 1275 2336 2877
; 1245 2283 1538 3110 2678 2489 1837 2639 2438 2754 2899 2955 1562 2660 2493
; 3344 1834 2360 2454 1541 2708 2418 2995 2577 2781 2603 2325 1953 2254 1566
; 1847 2422 1190 2826 2223 3003 3138 2371 2710 2190 2873 2394 2629 2591 2619
; 2054 2647 2527 3059 1858 1685 2153 2257 2198 2216 2072 2429 1559 1911 2867
; 3127 2210 2749 2292 2876 1138 2532 1721 2154 1874 2696 2439 2905 2347 2273
; 2594 2486 2035 2399 2474 2548 2546 3315 2429 2117 2707 1955 2009 2016 2507
; 2217 1596 2955 2094 1948 2812 2370 2453 2158 3183 2217 2918 2838 3217 2570
; 2955 1999 2064 2496 3217 2603 3125 2047 2201 2702 2984 1953 2119 2622 3184
; 3261 0 2855 2033 2264 2477 1947 2507 2962 2759 2636 2966 2140 3014 2034 2169
; 2960 2120 3276 2795 2210 2153 2159 2470 2849 3072 2609 2957 2375 3014 2424
; 1707 2938 1674 2775 1921 2041 3232 1813 2113 2234 2332 2094 2487 2694 2410
; 2091 2735 2670 2863 2586 2497 2811 1849 1828 2008 2429 2844 2439 2861 2870
; 2460 2520 2279 2239 2073 2265 1944 2891 1828 2548 2093 1052 2671 2876 2294
; 1378 1808 2784 2528 2638 2386 3260 1602 3060 2246 2689 1747 2398 3193 1871
; 2651 2542 2335 2591 2641 2254 2386 3045 2437 3075 2949 2662 2468 2372 2231
; 1913 2888 2361 2478 1878 1857 2911 2341 1470 2113 2355 1755 3163 2585 2278
; 2673 2605 1949 2665 2953 2029 2976 2547 2806 1903 1989 2073 2841 887 2520
; 2288 878 2753 2395 2281 1664 2140 2097 2337 2671 3031 1923 2296 2924 2402
; 2597 1730 2110 2962 1136 2588 1448 2690 2872 2677 1784 2646 2855 3282 2055
; 2737 2097 2725 2399 1999 2202 2688 2528 1126 2352 2205 1986 2363 2774 3010
; 2148 2991 2594 3494 2737 2679 2568 2793 2178 1781 2169 2943 2432 2476 2740
; 2305 2478 2193 2638 1265 2439 2446 1519 2804 1936 1731 2832 2105 1831 2314
; 2882 2830 2498 2621 2720 3073 2327 3019 2703 2407 1949 1977 2442 1995 2498
; 2029 2111 2503 2523 2860 1272 2266 3006 2619 2602 2084 2377 2207 2795 1406
; 2833 2694 1274 2600 2797 1773 1811 2628 2069 3398 2258 2919 2495 3026 2838
; 2812 2252 2193 2604 3048 2457 1409 2207 2322 1826 2012 1967 2446 3131 3284
; 3077 2195 2600 2500 3359 2882 2417 1612 2075 2079 2226 2700 1964 2782 2549
; 2291 1748 1748 3039 2346 3158 3277 2428 2478 2500 2496 2516 1961 2314 2732
; 2204 2825 2560 2098 2070 1507 2901 2494 2351 2251 2353 2102 2149 2533 2253
; 2206 2775 2832 2122 2838 2786 2753 3407 2365 2168 1613 2057 3329 2823 2136
; 2999 1825 2559 1385 2634 2770 2008 2396 2888 1061 2563 1677 2464 2146 2254
; 2901 2531 2126 1688 2593 2783 2218 2102 2037 1955 2729 2478 1819 3028 1193
; 2598 2539 2096 2079 1645 2399 2579 2278 1958 2340 2764 2146 2611 2972 1526
; 1927 1931 2985 2732 2503 2646 1206 2296 2284 2492 2781 2778 2548 2209 2135
; 2078 2876 1995 2441 2009 1280 2197 2204 1654 2240 2486 1975 2370 3229 2424
; 2564 3086 2046 2382 2806 2173 2283 1233 2543 1621 2478 2866 2460 1694 2645
; 2959 1756 1796 2648 2524 2027 2327 2015 2280 2138 2012 1861 2882 2580 3030
; 2521 2546 3084 1558 1917 2791 2740 2549 2484 1816 2568 2605 2429 2845 3032
; 2616 2906 3119 2741 2001 2106 2257 2715 2829 2876 2074 2893 2450 2616 3090
; 2707 2150 1791 1909 2801 3024 2329 2299 2409 2231 2570 2199 2692 2210 2612
; 2693 1923 2153 2782 2505 2510 2503 1911 2059 2428 2928 2388 2645 1784 2348
; 2905 2129 2349 2474 2412 2759 2256 2164 1946 2537 2140 2418 2457 2273 3211
; 2138 2538 915 2844 2048 2665 2155 2159 2349 2633 2184 2138 2525 2456 3417
; 2142 2289 1559 1050 2426 2550 1872 2517 2309 2414 2852 2521 1496 2751 2617
; 2335 3089 2526 1648 2732 2865 2956 2134 1988 2290 2353 1517 2156 1208 1531
; 2480 2276 2544 2169 1897 2131 2152 2835 1967 3180 2764 2593 2523 2476 2201
; 2397 2609 2019 2704 2377 2579 2233 2145 2948 2157 2499 1299 1888 3334 3338
; 3059 2465 2188 2833 2619 1911 3108 1356 2836 2337 2627 2053 2026 3095 2059
; 2402 2130 2968 1918 1577 2661 3581 1811 2167 2302 2760 1615 2206 2925 2428
; 2220 2694 1656 3082 2409 2084 2504 2663 2469 2579 2726 1137 2986 2798 2783
; 2458 2309 2680 2606 2203 2435 2273 2581 2618 2665 2568 1228 2684 1961 1862
; 2549 2830 1823 2408 2542 2530 2291 2993 2677 2525 1845 2426 2722 1850 2620
; 2592 2512 2514 2251 2572 2002 2373 1806 1687 2302 1172 2351 2483 2568 1701
; 2056 2861 2045 2559 2419 2952 2741 715 2709 2233 2695 2987 2933 2503 2386
; 2903 2716 2399 3692 2484 2605 2967 885 2092 3220 2209 2271 3059 1777 2793
; 1859 2611 2409 2421 1868 2388 2822 1440 1696 2445 1726 2456 2807 2221 2520
; 3068 2495 2203 1988 2397 1576 2611 2155 2059 2124 2599 1979 1945 2758 2625
; 2942 2200 2778 2131 1463 2954 1966 2059 2762 2659 2577 2353 2314 2339 2936
; 1539 2001 2462 2366 2427 2466
