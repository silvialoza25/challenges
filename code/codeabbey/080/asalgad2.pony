// ponyc -b asalgad2

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify
        fun tail_strings( array: Array[String]): Array[String] =>
          array.slice(1, array.size())

        fun tail_numbers( array: Array[F64]): Array[F64] =>
          array.slice(1, array.size())

        fun print_array( array: Array[F64] ): String ? =>
          if array.size() >= 1 then
            env.out.write( array(0)?.string() )
            env.out.write( " " )
            print_array( tail_numbers( array ) )?
          end
          "ok"

        fun calc_prob( const_pa: F64, const_pb: F64 ): F64 =>
          const_pa / ( (const_pa +  const_pb) - (const_pa * const_pb) )

        fun iterate(lines: Array[String], results: Array[F64]): Array[F64] ? =>
          if lines.size() < 1 then
            results
          else
            var args: Array[String] = lines(0)?.split_by(" ")
            var const_pa: F64 = args(0)?.f64()?
            var const_pb: F64 = args(1)?.f64()?
            var prob: F64 = calc_prob( const_pa / 100, const_pb / 100 ) * 100
            results.push( prob.round() )
            iterate( tail_strings(lines), results )?
          end

        fun ref apply(data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume data)
          var lines: Array[String] = input.split_by("\n")
          var useful_lines: Array[String] = lines.slice(1, lines.size()-1)

          try
            var results: Array[F64] = Array[F64]( useful_lines.size() )
            iterate( useful_lines, results )?
            print_array( results )?
          end

        fun ref dispose() =>
          env.out.write("")
      end,
      1024
    )

// cat DATA.lst | ./asalgad2
// 67 39 88 67 48 65 83 43 44 36 72 63 34 86 72 78 25 73 95 29 65
