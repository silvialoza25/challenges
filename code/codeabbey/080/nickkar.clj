;; $clj-kondo --lint nickkar.clj
;; linting took 61ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class))

(def error 0.000000000001)

(defn inf-sum ([prob1 prob2] (inf-sum 0 0 (/ prob1 100) (/ prob2 100)))
  ([i current prob1 prob2]
   (cond
     (>= i 100) (* (+ current error) 100)
     :else
     (let [aprob1 (- 1 prob1)
           aprob2 (- 1 prob2)
           newcurr (+ current (* prob1 (Math/pow (* aprob1 aprob2) i)))]
       (inf-sum (inc i) newcurr prob1 prob2))
     )
   ))

(defn find-prob ([ca5es] (find-prob ca5es 0 []))
  ([ca5es i out]
   (cond
     (>= i ca5es) out
     :else
     (let [prob1 (read)
           prob2 (read)]
       (find-prob ca5es (inc i) (conj out (Math/round (inf-sum prob1 prob2))))
       )
     )
   )
  )

(defn main []
  (let [ca5es (read)]
    (apply println (find-prob ca5es))))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; 82 52 55 52 86 53 89 89 92 13 99 75 62 71 58 98 61 45 93 60 16 54 74 80 68 29
