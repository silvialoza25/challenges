# elvish -compileonly cyberpunker.elv

#!/bin/elvish

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn duelChances [data]{
  each [values]{
    var playerA = (/ $values[0] 100)
    var playerB = (/ $values[1] 100)
    var add = (+ $playerA $playerB)
    var mainOperation = (* (/ $playerA (- $add (* $playerA $playerB))) 100)
    var rounded = (math:round $mainOperation)
    put $rounded
  } $data
}

echo (duelChances (input_data))

# cat DATA.lst | elvish cyberpunker.elv
#55 29 23 78 56 71 91 82 58 73 66 90 21 87
#55 24 94 74 63 66 77 46 24 70 60 87 46
