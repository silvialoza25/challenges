{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #080: Duel Chances - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi s = fromMaybe 0 $ parseInteger {a=Int} s

getLines : IO (List String)
getLines = do
  x <- getLine
  if x == "" then pure []
    else do
      xs <- getLines
      pure (x :: xs)

getDuelChance : Int -> Int -> Double
getDuelChance iPA iPB =
  let
    probA = (cast iPA) / 100
    probB = (cast iPB) / 100
    duel = (probA / (probA + probB - (probA * probB))) * 100
    in
  -- Round
  floor $ duel + 0.5

processTestCase : List Int -> Int
processTestCase [probA, probB] = cast $ getDuelChance probA probB

main : IO ()
main = do
  -- Read Stdin
  inputData <- getLines
  let
    -- Parse input array
    testCases = map (map stoi) $ map words $ drop 1 inputData
    -- Process data
    results = map processTestCase testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  48 90 42 91 23 26 74 50 91 52 42 70 69 39 86
  36 90 97 26 54 77 15 78 64 76 64 82 95 70 92
-}
