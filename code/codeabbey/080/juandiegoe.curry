-- $ curry-verify juandiegoe.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `juandiegoe'!

import IO
import List
import Read
import Float

getLines :: IO String
getLines = do
  dataLine <- getContents
  return dataLine

parseFloat :: String -> Float
parseFloat str = read str :: Float

getProb :: [[Prelude.Char]] -> Prelude.Int
getProb arr = result
  where
    numA = (parseFloat (arr !! 0)) / 100
    numB = (parseFloat (arr !! 1))/100
    result = round ((numA / (numA + numB - (numA * numB))) * 100)

main :: Prelude.IO ()
main = do
  input <- getLines
  let
    values = (split (== '\n') input)
    dropData = drop 1 (init values)
    cleanData = map words dropData
    result = map (\x -> show (getProb x)) cleanData
  putStrLn (unwords result)

-- cat DATA.lst | pakcs :load juandiegoe.curry :eval main :quit
-- 67 39 88 67 48 65 83 43 44 36 72 63 34 86 72 78 25 73 95 29 65
