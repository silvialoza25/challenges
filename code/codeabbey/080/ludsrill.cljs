;; clj-kondo --lint ludsrill.cljs
;; linting took 28ms, errors: 0, warnings: 0

(ns ludsrill.080
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn dual-chances [item]
  (let [a (/ (edn/read-string (first item)) 100)
        b (/ (edn/read-string (second item)) 100)]
    (print (core/format "%s "
            (Math/round (* 100 (double (/ a (- (+ a b) (* a b))))))))))

(defn -main []
  (let [data (rest (get-data))]
    (doseq [item data]
      (dual-chances item))))

(-main)

;; cat DATA.lst | ludsrill.cljs
;; 82 52 55 52 86 53 89 89 92 13 99 75 62 71 58 98 61 45 93 60 16 54 74 80 68
;; 29
