#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun calc-a-prob(a-prob b-prob)
  (let ((total 100) (total-a 0 )
  (total-b 0) (i 1) (ai 0) (bi 0))
    (loop
      (setq ai (* a-prob total))
      (setq total-a (+ total-a ai))
      (setq total (- total ai))
      (setq bi (* b-prob total))
      (setq total-b (+ total-b bi))
      (setq total (- total bi))
      (setq i (+ i 1))
      (when (> i 1000)(return total-a)))))

(defun main()
  (let ((size-input (read)))
    (loop for i from 0 to (- size-input 1)
      do(format t "~a " (round (calc-a-prob
      (/ (read) 100) (/ (read) 100)))))))

(main)

#|
  cat DATA.lst | clisp bridamo98.lsp
  89 93 69 40 74 53 48 86 27 66 69 73 41 55 28
  95 70 70 46 69 91 66 73 29 37 60 64 92 52
|#
