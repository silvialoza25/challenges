{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let intInputData = map convert inputData
  let solution = map solve intInputData
  putStrLn (unwords solution)

convert :: String -> [Int]
convert dat = result
  where
    aux = words dat
    result = map read aux :: [Int]

solve :: [Int] -> String
solve dat = result
  where
    sortedDat = sort dat
    a = head sortedDat
    b = sortedDat !! 1
    c = sortedDat !! 2
    result = if a + b > c
      then show 1
      else show 0
{-
$ cat DATA.lst | ./bridamo98
  0 1 0 1 1 1 0 0 0 1 0 1 0 0 0 1 1 0 0 0 0 0 1 1 0 1
-}
