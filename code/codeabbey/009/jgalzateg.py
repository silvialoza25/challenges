#!/usr/bin/env python3
# $ mypy jgalzateg.py
# Success: no issues found in 1 source file

from typing import List


def is_triangle(lista: List[int]) -> int:
    var_res = 0
    if valid_t(lista[0], lista[1], lista[2]):
        if valid_t(lista[1], lista[2], lista[0]):
            if valid_t(lista[2], lista[0], lista[1]):
                var_res = 1
    return var_res


def valid_t(sidea: int, sideb: int, sidec: int) -> bool:
    return bool(sidea + sideb > sidec)


def read_data(i: int) -> str:
    inp_char = input("")
    lst_char = inp_char.split(" ")
    lst_int = list(map(int, lst_char))
    acum = ""
    if i > 1:
        acum = acum + str(is_triangle(lst_int)) + " " + read_data(i - 1)
    else:
        acum = acum + str(is_triangle(lst_int))
    return acum


if __name__ == "__main__":
    NUM = input("")
    RES = read_data(int(NUM))
    print(RES)

# cat DATA.lst | python jgalzateg.py
# 1 0 0 0 0 1 1 0 0 0 0 1 1 0 1 0 1 1 0 0 0 0 1
