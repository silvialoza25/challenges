;; clj-kondo --lint ludsrill.cljs
;; linting took 29ms, errors: 0, warnings: 0

(ns ludsrill.009
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn -main []
  (let [data (rest (get-data))]
    (doseq [item data]
      (if (and (> (+ (Integer. (nth item 0)) (Integer. (nth item 1)))
                (Integer. (nth item 2)))
               (> (+ (Integer. (nth item 0)) (Integer. (nth item 2)))
                (Integer. (nth item 1)))
               (> (+ (Integer. (nth item 1)) (Integer. (nth item 2)))
                (Integer. (nth item 0))))

        (print "1 ")
        (print "0 ")))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 0 1 0 1 1 1 0 0 0 1 0 1 0 0 0 1 1 0 0 0 0 0 1 1 0 1
