-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import IO
import List
import Read

getData :: IO String
getData = do
  content <- getContents
  return content

sort :: [Int] -> [Int]
sort [] = []
sort (x:xs) = sort (filter (<= x) xs) ++ [x] ++ sort (filter (> x) xs)

buildTriangle :: Int -> Int -> Int -> Int
buildTriangle constA constB constC
  | (constA + constB) > constC = 1
  | otherwise = 0

getSolution :: String -> Int
getSolution dataIn  = result
  where
    fData = sort (map readInt (words dataIn))
    constA = fData !! 0
    constB = fData !! 1
    constC = fData !! 2
    result = buildTriangle constA constB constC

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> show (getSolution x)) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 1 1 0 1 1 1 0 0 0 0 0 0 1 0 1 1 1 0 0 0 1 0 0 0 0 1 0 0 1 0
