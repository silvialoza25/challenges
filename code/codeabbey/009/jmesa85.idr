{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #009: Triangles - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi str = fromMaybe 0 $ parseInteger {a=Int} str

intAt : Int -> List Int -> Int
intAt 0 [] = 0
intAt _ [] = 0
intAt 0 (x::xs) = x
intAt index (_::xs) = intAt (index - 1) xs

-- Gets lines in the Stdin
getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

processTestCase : List Int -> Int
processTestCase triplet = let sNum = sort triplet in
  if (intAt 0 sNum) + (intAt 1 sNum) > (intAt 2 sNum) then 1 else 0

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse test cases
    testCases = map (map stoi) $ map words inputData
    -- Process data
    results = map processTestCase testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  0 1 1 0 1 1 0 0 1 1 1 0 1 1 1 1 1 0 1 0 1 1 0 1 1 0 1
-}
