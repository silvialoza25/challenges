# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.02s to load, 0.07s running 55 checks on 1 file)
# 7 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule BinarySearchInArray do
  def main do
    all_data = IO.read(:stdio, :all)

    try do
      [data, bd] = get_data(all_data)
      bd = calculate_range(bd)
      Enum.map(data, &changing_base(hd(&1)))
        |> Enum.map(&binary_search(bd, &1))
        |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_data(all_data) do
    data = String.split(all_data, "\n")
            |> Enum.map(&String.split(&1, " "))
            |> Enum.map(&Enum.reject(&1, fn x -> x == "" end))
            |> Enum.reject(&Enum.empty?/1)
    [Enum.slice(data, 1..String.to_integer(hd(hd(data)))),
      Enum.slice(data, String.to_integer(hd(hd(data))) + 1..length(data))]
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def calculate_range(list) do
    list
      |> Enum.map(fn [start, offset, country] ->
                  [changing_base(start), changing_base(offset), country] end)
      |> Enum.map(fn [start, offset, country] ->
                  [start, start + offset, country] end)
  end

  def changing_base(value) do
    Integer.parse(value, 36)
      |> Tuple.to_list()
      |> hd()
  end

  def binary_search(list, value) do
    aux = Kernel.floor(length(list) / 2)
    if aux != 0 do
      if Enum.at(Enum.at(list, aux - 1), 1) >= value do
        binary_search(Enum.slice(list, 0..aux - 1), value)
      else
        binary_search(Enum.slice(list, aux..length(list)), value)
      end
    else
      List.last(hd(list))
    end
  end
end

BinarySearchInArray.main()

# cat DATA.lst db-ip.lst | elixir ludsrill.exs
# CA BR IL US US GB IS GE US NL US RU FR CN US GR GB LB US FR MX ES DE US UA HK
# RU US IT JP AU US CH HK PL GB ES US CA AU NZ CA DE CA US AU AU FR RU US NZ CO
# NL FR US CA CZ ES GB JP AU MU NO TW AU HU CA HR EE US BA CA SG ES MA FO GR US
# GB AU PK CZ LK FR NL DE RU DE ZW AU NZ GB GB US RU KZ IE SE RO TH US CA FR US
# ... AU US PL AU HK FR TW FI DE AU GB US US DE PL BY AR NL
