# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.04s to load, 0.1s running 55 checks on 1 file)
# 5 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule LinearCongruentialGenerator do

  def main do
    data = get_data()
    Enum.map(Enum.map(data, &generator/1), &IO.write("#{&1} "))
  end

  def get_data do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    [_ | tail] = List.delete_at(all_data, 1 +
                  String.to_integer(Enum.at(all_data, 0)))
    Enum.map(Enum.map(tail, &String.split(&1, " ")), &to_int/1)
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def generator([a, c, m, x, n]) do
    if n != 0 do
      generator([a, c, m, rem(a * x + c, m), n - 1])
    else
      x
    end
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e LinearCongruentialGenerator.main
# 10465 22 105 3 714 572 157 22322 76 94281 0 248910 580 7 265 1 34965
