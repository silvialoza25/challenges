/*
$ dartanalyzer --fatal-infos --fatal-warnings --no-declaration-casts \
 --no-implicit-casts --no-implicit-dynamic --lints samjoo.dart
Analyzing samjoo.dart...
No issues found!
*/

import 'dart:io';

/// Reads and parse [linesNum] lines from the STDIN.
void readData(int linesNum) {
  if(linesNum == 0) return;
  List<int> testCase = stdin.readLineSync().split(' ').map((elem) {
    return int.parse(elem);
  }).toList();
  linearCongruentialGenerator(testCase);
  readData(linesNum-1);
}

/// Finds the next value between [X0] and [N].
int getNthValue(int A, int C, int M, int X0, int N, int Xcur) {
  if(N <= 0) return Xcur;
  Xcur = (A * Xcur + C) % M;
  return getNthValue(A, C, M, X0, N-1, Xcur);
}

void linearCongruentialGenerator(List<int> constants) {
  int res = getNthValue(constants[0], constants[1], constants[2], constants[3],
                        constants[4], constants[3]);
  stdout.write("${res} ");
}

void main(List<String> args) {
  readData(int.parse(stdin.readLineSync()));
}

/*
$ cat DATA.lst |dart samjoo.dart
32204 7981 24 629 65 8506 2 3966 3334 14926 847165 23032 74 11
*/
