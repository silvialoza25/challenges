{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #009: Triangles - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi str = fromMaybe 0 $ parseInteger {a=Int} str

intAt : Int -> List Int -> Int
intAt 0 [] = 0
intAt _ [] = 0
intAt 0 (x::xs) = x
intAt index (_::xs) = intAt (index - 1) xs

-- Gets lines in the Stdin
getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

-- xNext = (a * x0 + c) % m, for n number of times
getLcg : Int -> Int -> Int -> Int -> Int -> Int
getLcg _ _ _ xZero 0 = xZero
getLcg numA numC numM xZero numN =
  let xNext = mod (numA * xZero + numC) numM in
  getLcg numA numC numM xNext (numN - 1)

processTestCase : List Int -> Int
processTestCase params =
  let
    numA = intAt 0 params
    numC = intAt 1 params
    numM = intAt 2 params
    xZero = intAt 3 params
    numN = intAt 4 params
  in
  getLcg numA numC numM xZero numN

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse test cases
    testCases = map (map stoi) $ map words inputData
    -- Process data
    results = map processTestCase testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  2 10 1469 6 675931 16485 1 0 1113 45 128039 2 1375 7474
-}
