#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun calc-rand-number(A C M X0 N)
  (let ((x-cur X0) (x-next NIL)(i 1))
    (loop
      (setq x-next (mod (+ (* A x-cur) C) M))
      (setq x-cur x-next)
      (setq i (+ i 1))
      (when (> i N)(return x-cur))
    )
  )
)

(defun main (size-input)
  (loop for i from 0 to (- size-input 1)
      do(format t "~a " (calc-rand-number
      (read) (read) (read) (read) (read))))
)

(defvar size-input (read))
(main size-input)

#|
  cat DATA.lst | clisp bridamo98.lsp
  336161 0 6219 183 9367 30 392 23452 3750 54882 771115 274 2349 147053 2 1575
|#
