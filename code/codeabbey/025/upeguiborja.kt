/*
# Lint
$ ktlint --experimental --verbose --disabled_rules=experimental:indent,indent \
> upeguiborja.ky
# Compile
$ kotlinc -Werror upeguiborja.kt -include-runtime -d upeguiborja.jar
*/

fun main(args: Array<String>) {
  val n_tests: Int = readLine()!!.toInt()

  var problem_list: MutableList<List<Int>> = mutableListOf<List<Int>>()

  for (i in 0 until n_tests) {
    val problem: List<Int> = readLine()!!.split(" ").map { it.toInt() }
    problem_list.add(problem)
  }

  for (problem in problem_list) {
    val (multiplier, increment, modulus, x, n_times) = problem
    val result: Int = lcn(multiplier,
                          increment,
                          modulus,
                          x,
                          n_times)
    print("$result ")
  }

  println()
}

fun lcn(mult: Int, incr: Int, mod: Int, x: Int, n_times: Int): Int {
  val xi: Int = (mult * x + incr) % mod
  if (n_times > 1) {
    return lcn(mult, incr, mod, xi, n_times = n_times - 1)
  }
  return xi
}

/*
$ cat DATA.lst | java -jar upeguiborja.jar
286 0 2 401907 35 30 149439 10 57 647 38566 2129 83549 13339 6
*/
