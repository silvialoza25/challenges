;; $ clj-kondo --lint miyyer1946.clj
;; linting took 24ms, errors: 0, warnings: 0

(ns miyyer1946-008
  (:gen-class)
  (:require [clojure.core :as core]))

(defn result [a b c d e]
(loop [sum d cnt 0]
    (if (>= cnt e)
        (print sum "")
    (recur (mod (+ (* a sum) b) c) (inc cnt))))
)

(defn get-data [size-input]
  (loop [x 1]
    (when (< x (+ size-input 1))
      (result (core/read) (core/read) (core/read) (core/read) (core/read))
      (recur (+ x 1))
    )
  )
)

(defn main []
  (let [size-input (core/read)]
    (get-data size-input)
  )
)

(main)

;$ cat DATA.lst | clj miyyer1946.cljs
;336161 0 6219 183 9367 30 392 23452 3750 54882 771115 274 2349 147053 2 1575
