;; $clj-kondo --lint nickkar.cljs
;; linting took 37ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]
            [clojure.string :as str]))

;; reads the input and puts the values in a readable vector
(defn read-input []
  (into [] (map core/read-string (str/split (core/read-line) #" "))))

;; converts degrees angles into radians
(defn to-rads [angle]
  (* angle (/ Math/PI 180)))

;; simulates the shot
(defn simulate ([terrain Vini Aini] (simulate terrain Vini (to-rads Aini) 1))
  ([terrain Vini Aini time]
   (let [x (* Vini time (Math/cos Aini))
         y (- (* Vini time (Math/sin Aini)) (* 0.5 9.81 (Math/pow time 2)))
         index (quot x 4)
         terr (* 4 (nth terrain index))]
     (cond
       (<= y terr) (int (Math/floor x))
       :else
       (simulate terrain Vini Aini (+ time 0.01))))))

;; iterates over every test-case
(defn solve-cases ([cases] (solve-cases cases 0 []))
  ([cases index out]
   (cond
     (= index cases) out
     :else
     (let [terrain (read-input)
           [Vini1 Aini1] (read-input)
           shot1 (simulate terrain Vini1 Aini1)
           [Vini2 Aini2] (read-input)
           shot2 (simulate terrain Vini2 Aini2)
           [Vini3 Aini3] (read-input)
           shot3 (simulate terrain Vini3 Aini3)]
       (solve-cases cases (inc index) (conj out shot1 shot2 shot3))))))

;; driver code
(defn main []
  (let [cases 3]
    (apply println (solve-cases cases))))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 84 84 52 60 90 110 80 60 36
