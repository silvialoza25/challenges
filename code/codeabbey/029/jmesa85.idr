{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #029: Sort with Indexes - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi str = fromMaybe 0 $ parseInteger {a=Int} str

-- Sorting function used by "sortBy"
sortArray : (Int, Int) -> (Int, Int) -> Ordering
sortArray (elem1, _) (elem2, _) =
  if elem1 > elem2 then GT
  else if elem2 > elem1 then LT
    else compare elem1 elem2

sortWithIndexes : List Int -> List Int
sortWithIndexes array =
  let
    -- Add original index in a tuple with each element
    zipped = zip array [1..(cast $ length array)]
    -- Sort array
    sorted = sortBy sortArray zipped
  in
    -- Filter original indexes
    map (\(_, index) => index) sorted

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLine
  let
    -- Parse test cases
    testCases = map stoi $ words inputData
    -- Process data
    results = sortWithIndexes testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  5 20 2 19 12 14 1 17 9 16 11 15 8 6 7 4 10 18 13 3
-}
