;; $ clj-kondo --lint bridamo98.clj
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-029
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn sort-with-indexes[size-input content-input]
  (let [indexes (range 1 (+ size-input 1)) numb-and-index
  (sort-by second (map list indexes content-input))]
    (loop [i 0 result ""]
    (if (< i size-input)
      (recur (+ i 1)
        (str result (first (nth numb-and-index i)) " "))
        result))))

(defn main []
  (let [size-input (edn/read-string (core/read-line))
  content-input (map #(Integer/parseInt %) (str/split (core/read-line) #" "))]
    (println (sort-with-indexes size-input content-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.clj
;; 20 17 5 18 16 19 10 11 1 14 3 2 15 8 7 6 4 13 12 21 9
