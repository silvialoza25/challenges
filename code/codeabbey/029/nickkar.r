# $ lintr::lint('nickkar.r')

formatinput <- function(vec, out=c(), i=1) {
    if (length(out) == length(vec)) {
        return(out)
    }
    else {
        formatinput(vec, c(out, as.double(vec[i])), i + 1)
    }
}

indexsort <- function(vec, out=c(), i=1) {
    if (length(vec) == length(out)) {
        return(out)
    }
    else {
        newout <- c(out, which.min(vec))
        newvec <- vec
        newvec[which.min(vec)] <- Inf
        indexsort(newvec, newout, i + 1)
    }
}

main <- function() {
    data <- readLines("stdin")
    vec <- formatinput(unlist(strsplit(data[2], " ")))
    out <- indexsort(vec)

    cat(out)
    cat("\n")
}

main()

# $ cat DATA.lst | rscript nickkar.r
# 14 5 18 20 9 15 19 16 8 2 3 12 10 11 21 17 6 1 13 4 7
