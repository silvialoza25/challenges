# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
our ($VERSION) = 1;

sub index_of {
  my @args    = @_;
  my @array   = @{ $args[0] };
  my $element = $args[1];
  my ($index) = grep { $array[$_] eq "$element" } ( 0 .. @array - 1 );
  $index = defined($index) ? $index : '-1';
  return $index;
}

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data;
}

sub solution {
  my @unsort = @_;
  my $SPACE  = q{ };
  my @sort   = sort { $a <=> $b } @unsort;
  foreach (@sort) {
    my $index = int( index_of( \@unsort, "$_" ) ) + 1;
    exit 1 if !print $index. $SPACE;
  }
  exit 1 if !print "\n";
  return 'Brutal mode is harder than broke with your wife';
}

sub main {
  my @vals  = data_in();
  my $SPACE = q{ };
  my @body  = split $SPACE, $vals[1];
  solution(@body);
  return 'This is the first pearl code that I made';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# 20 10 2 16 13 19 9 1 3 15 5 8 11 21 14 6 4 18 12 23 22 7 17
