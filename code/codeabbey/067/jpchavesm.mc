/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, integer, int, char.

:- pred str2integer(string::in, integer::out) is det.
str2integer(X, det_from_string(X)).

:- pred unwords(string::in, string::in, string::out) is det.
unwords(X, Y, X ++ " " ++ Y).

:- pred genFibanacci(int, list(integer), list(integer)).
:- mode genFibanacci(in, in, out) is det.
genFibanacci(Limit, PartialList, FullList) :-
(
  if length(PartialList) = Limit
  then FullList = PartialList
  else
    det_index1(PartialList, length(PartialList)) = N_1,
    det_index1(PartialList, length(PartialList) - 1) = N_2,
    genFibanacci(Limit, PartialList ++ [N_1 + N_2], FullList)
).

:- pred getIndex(list(integer), list(integer), list(int), list(int)).
:- mode getIndex(in, in, in, out) is det.
getIndex(_, [], ResultList, ResultList).
getIndex(FibList, [Num | Tail], IndexList, ResultList) :-
(
  det_index0_of_first_occurrence(FibList, Num) = Index,
  getIndex(FibList, Tail, IndexList ++ [Index], ResultList)
).

:- pred searchIndexes(list(string), string).
:- mode searchIndexes(in, out) is det.
searchIndexes(InputList, FinalStr) :-
(
  map(str2integer, InputList, IntegerList),
  genFibanacci(1000, [integer(0), integer(1)], FibList),
  getIndex(FibList, IntegerList, [], IndexList),
  map(int_to_string, IndexList, StrIndexList),
  foldr(unwords, StrIndexList, "", FinalStr)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, "", !IO).

:- pred read_lines(io.text_input_stream::in,
  string::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ Line,
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    words_separator(char.is_whitespace, FileContents) = StrList,
    det_drop(1,StrList, InputList),
    searchIndexes(InputList, ResultStr),
    io.print_line(strip(ResultStr), !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  973 772 436 669 59 433 381 465 383 338 730 103 617 252 562 758
*/
