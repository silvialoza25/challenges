# elvish -compileonly cyberpunker.elv

#!/bin/elvish

use str
use math

fn inputData []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn fibonacciSequence [number mult plus]{
  var fibonacci = (+ (* $mult (math:log $number)) $plus)
  echo (math:round $fibonacci)
}

fn fibonacciNumbers [data]{
  each [values]{
    var valueF = $values[0]
    var multiply = 2.078087
    var add = 1.672276
    fibonacciSequence $valueF $multiply $add
  } $data
}

echo (fibonacciNumbers (inputData))

# cat DATA.lst | elvish cyberpunker.elv

# 274 404 349 480 786 639 173 730 54 952 580 908 416 938 515
# 793 161 689 843 507
