;; clj-kondo --lint ludsrill.cljs
;; linting took 28ms, errors: 0, warnings: 0

(ns ludsrill.067
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn -main []
  (let [data (rest (get-data))]
    (doseq [item data]
      (if (= (edn/read-string (first item)) 0)
          (print "0 ")
          (print (core/format "%s " (Math/round
                  (+ (* 2.078087 (Math/log (edn/read-string (first item))))
                   1.672276))))))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 973 772 436 669 59 433 381 465 383 338 730 103 617 252 562 758
