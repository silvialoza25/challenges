#!/usr/bin/perl
# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 bridamo98.pl
# bridamo98.pl source OK
#
# Compile:
# $ perl -c bridamo98.pl
# bridamo98.pl syntax OK

package bridamo98;

use warnings FATAL => 'all';
use strict;
use Math::BigInt;
our ($VERSION) = 1;

my $SPACE = q{ };

sub fibonacci {
  my @data   = @_;
  my $number = $data[0];
  my $first  = 0;
  my $sec    = 1;
  my $result = 2;
  my $current;
  while ( 1 == 1 ) {
    $current = Math::BigInt->new( $first + $sec );
    if ( $current == $number ) {
      last;
    }
    else {
      $first  = $sec;
      $sec    = $current;
      $result = $result + 1;
    }
  }
  return $result;
}

sub solve_all {
  my @data  = @_;
  my @input = @{ $data[0] };
  my $result;
  foreach my $number (@input) {
    $result = fibonacci($number);
    exit 1 if !print "$result ";
  }
  return;
}

sub get_data {
  my @data      = @_;
  my $sizeInput = $data[0];
  my @input     = ();
  for ( 0 .. $sizeInput - 1 ) {
    push @input, <>;
  }
  return @input;
}

sub main {
  my $sizeInput = int <>;
  my @input     = get_data($sizeInput);
  solve_all( \@input );
  return;
}
main();
exit 1 if print "\n";

# $ cat DATA.lst | perl bridamo98.pl
# 973 772 436 669 59 433 381 465 383 338 730 103 617 252 562 758
