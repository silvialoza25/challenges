# elvish -compileonly alejotru3012.elv

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn gcd [nums]{
  if (== $nums[0] $nums[1]) {
    put $nums[0]
  } else {
    min_n = (math:min $@nums)
    max_n = (math:max $@nums)
    gcd [$min_n (- $max_n $min_n)]
  }
}

fn calculate [data]{
  each [values]{
    n_gcd = (gcd $values)
    n_lcm = (/ (* $values[0] $values[1]) $n_gcd)
    echo '('$n_gcd $n_lcm')'
  } $data
}

echo (str:join ' ' [(calculate (input_data))])

# cat DATA.lst | elvish alejotru3012.elv
# (3 144) (8 16) (1 2400) (47 134232) (1 51591) (1 190) (4 88)
# (1 55464409) (53 488395) (1 1647000) (1 719206) (1 12646)
# (1 49203) (89 33820) (34 7956) (1 30) (1 947063) (255 84150)
# (2 30972) (3360 6720) (1 8207) (1 14)
