###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# Get Greatest Common Divisor (GCD) for the given two numbers
getGcd = (number1, number2) ->
  # Base case
  if number1 == number2
    return number1
  # Recursive case
  if number1 > number2
    getGcd(number1 - number2, number2)
  else
    getGcd(number1, number2 - number1)

# Get Least Common Multiple (LCM) for the given two numbers
getLcm = (number1, number2) ->
  number1 * number2 / getGcd(number1, number2)

# Returns the formatted result
processTestCase = (testCaseStr) ->
  numbersArr = testCaseStr.split(' ')
  value1 = parseInt(numbersArr[0])
  value2 = parseInt(numbersArr[1])
  '(' + getGcd(value1, value2) + ' ' + getLcm(value1, value2) + ')'

main = ->
  # Read STDIN
  data = require('fs').readFileSync(0).toString().trim().split('\n')
  # Discard number of test cases at index 0
  testCasesAsStrings = data.slice(1)
  # Process each test case
  results = testCasesAsStrings.map(processTestCase)
  # Print results
  console.log(results.join(' '))
  return

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
(174 36540) (37 104044) (1 60818) (363 726) (1 1614) (70 36540) (345 15180)
 (1 653) (33 165) (1 66738) (3 684) (1 92898) (152 238336) (74 498316)
 (85 276675) (64 72192) (56 49392)
###
