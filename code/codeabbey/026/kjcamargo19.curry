-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import Float
import IO
import List
import Read

getData :: IO String
getData = do
  content <- getContents
  return content

greatestCD :: Int -> Int -> Int
greatestCD constA constB
  | constA == constB = constA
  | otherwise = greatestCD minNumber (maxNumber - minNumber)
  where
    minNumber = min constA constB
    maxNumber = max constA constB

leastCM :: Int -> Int -> Int
leastCM constA constB = result
  where
    result = truncate (i2f (constA * constB) / i2f (greatestCD constA constB))

getSolution :: String -> String
getSolution dataIn  = result
  where
    fData = map readInt (words dataIn)
    constA = fData !! 0
    constB = fData !! 1
    greatest = show (greatestCD constA constB)
    least = show (leastCM constA constB)
    result = "(" ++ greatest ++ " " ++ least ++ ")"

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> getSolution x) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- (300 2700) (68 284852) (66 90288) (76 526680) (291 20370) (472 31152)
-- (2 1288) (4 9559200) (9 549) (1 137746) (1 169904) (74 173160) (54 132246)
-- (140 98000) (5 4150) (25 45325) (14 8820) (76 75924) (56 321552)
