// ponyc -b asalgad2

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail( array: Array[String] ): Array[String] =>
          array.slice(1, array.size())

        fun max( const_a: I64, const_b: I64 ): I64 =>
          if const_a > const_b then
            const_a
          else
            const_b
          end

        fun min( const_a: I64, const_b: I64 ): I64 =>
          if const_a < const_b then
            const_a
          else
            const_b
          end

        fun gcd( const_a: I64, const_b: I64 ): I64 =>
          if const_a != const_b then
            var new_a: I64 = max(const_a, const_b)
            var new_b: I64 = min(const_a, const_b)
            gcd( max(new_a-new_b, new_b), min(new_a-new_b, new_b) )
          else
            const_a
          end

        fun lcm( const_a: I64, const_b: I64, great_div: I64 ): I64 =>
          ( const_a * const_b ) / great_div

        fun iterate( lines: Array[String] ) ? =>
          if lines.size() > 0 then
            var values: Array[String] = lines(0)?.split(" ")
            var const_a: I64 = values(0)?.i64()?
            var const_b: I64 = values(1)?.i64()?

            var great_div: I64 = gcd( const_a, const_b )
            var least_div: I64 = lcm( const_a, const_b, great_div )

            env.out.write( "(" )
            env.out.write( great_div.string() )
            env.out.write( " " )
            env.out.write( least_div.string() )
            env.out.write( ") " )

            iterate( tail(lines) )?
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")
          var useful_lines: Array[String] = lines.slice(1, lines.size()-1)

          try
            iterate( useful_lines )?
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// (31 10912) (552 60720) (40 26800) (60 171600) (2 466872) (11 29172)
// (96 99840) (112 37296) (88 238392) (1 35) (2 1164658) (1 546385)
// (234 97344) (41 49569) (41 64206) (92 24840) (148 34188) (2 260) (49 54145)
// (3 1530) (1 6522945) (450 17550) (75 146025)
