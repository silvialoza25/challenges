/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, char.

:- func findGCD(int, int) = int.
findGCD(NumA,NumB) =
(
  if NumA = NumB
  then NumA
  else if NumA > NumB
  then findGCD(NumA - NumB,NumB)
  else findGCD(NumA,NumB - NumA)
).

:- pred findGCDnLCM(list(string), string, string).
:- mode findGCDnLCM(in, in, out) is det.
findGCDnLCM([], Solution, Solution).
findGCDnLCM([Line | Tail], PartialSolution, Solution) :-
(
  string.words_separator(char.is_whitespace, Line) = NumsStr,
  map(det_to_int,NumsStr) = Nums,
  det_index0(Nums,0) = FirstNum,
  det_index0(Nums,1) = SecondNum,

  findGCD(FirstNum, SecondNum) = GCD,
  FirstNum * SecondNum / GCD = LCM,
  PartialSolution1 = PartialSolution ++ " (" ++ from_int(GCD) ++ " " ++
    from_int(LCM) ++ ")",

  findGCDnLCM(Tail, PartialSolution1, Solution)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1, FileContents, ComputeList),
    findGCDnLCM(ComputeList, "", Solution),
    io.print_line(Solution,!IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  (174 36540) (37 104044) (1 60818) (363 726) (1 1614) (70 36540) (345 15180)
  (1 653) (33 165) (1 66738) (3 684) (1 92898) (152 238336) (74 498316)
  (85 276675) (64 72192) (56 49392)
*/
