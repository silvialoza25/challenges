# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.03s to load, 0.1s running 55 checks on 1 file)
# 6 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule GreatestCommonDivisor do

  def main do
    data = get_data()
    gcm = Enum.map(data, &greatest_common_divisor/1)
    lcm  = least_common_multiple(gcm, data)
    Enum.each(Enum.map(Enum.zip(gcm, lcm), &Tuple.to_list/1),
      fn([a, b]) -> IO.write("(#{a} #{b}) ") end)
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def get_data do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    [_ | tail] = List.delete_at(all_data, 1 +
                  String.to_integer(Enum.at(all_data, 0)))
    Enum.map(Enum.map(tail, &String.split(&1, " ")), &to_int/1)
  end

  def least_common_multiple(greatest_common_divisor, data) do
    Enum.map(Enum.map(Enum.zip(greatest_common_divisor, data),
      &Tuple.to_list/1), fn([c, [a, b]]) -> div(a * b, c) end)
  end

  def greatest_common_divisor([a, b]) do
    if b == 0 do
      a
    else
      greatest_common_divisor([b, rem(a, b)])
    end
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e GreatestCommonDivisor.main
# (32 36800) (2 120) (1 2596347) (1 29233) (19 38874) (1 56084) (19 30590)
# (150 25650) (1 25140) (272 156400) (1 84) (2 20784) (37 358900) (3 2490)
# (1 3660) (1 37940) (1 8535) (34 233002) (1 427) (1 1041) (360 39600)
# (21 17808)
