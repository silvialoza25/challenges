;$ clj-kondo --lint frank1030.cljs
;linting took 343ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.pprint :refer [cl-format]]))

(defn gcd [a b]
  (if (zero? b)
    a
    (recur b (mod a b))))

(defn lcm [a b]
  (/ (* a b) (gcd a b)))

(defn validator [index]
  (let [x (atom index) a (core/read) b (core/read) rgcd (gcd a b)
    rlcm (lcm a b)]
    (if (> @x 0)
      (do (swap! x dec)
        (print (cl-format nil "(~d ~d) " rgcd rlcm))
        (if (not= @x 0)
          (validator @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (core/read)]
    (validator index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;(1 23751) (2 47286) (1860 37200) (52 102492) (11 13728)
;(71 191913) (2 1786) (78 323544) (28 100492) (108 19656)
;(52 146224) (275 22000) (2 1012) (5 190) (1 602)
;(8 824) (62 335916) (44 11704) (4 12) (1 2577120)
