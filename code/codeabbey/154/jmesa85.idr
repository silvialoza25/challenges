{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #154: Breadth First Search - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi s = fromMaybe 0 $ parseInteger {a=Int} s

intAt : Int -> List Int -> Int
intAt 0 [] = 0
intAt _ [] = 0
intAt 0 (x::xs) = x
intAt k (_::xs) = intAt (k - 1) xs

-- Gets lines in the Stdin
getLines : IO(List String)
getLines = do
  x <- getLine
  if x == "" then pure []
    else do
      xs <- getLines
      pure (x :: xs)

-- To be used by Data.List.sortBy
sortSeenNodes : List Int -> List Int -> Ordering
sortSeenNodes [x, _] [y, _] = if x > y then GT else LT

-- Is node in "seen" list
isNodeSeen : Int -> List(List Int) -> Bool
isNodeSeen node seen =
  let reduced = map (\[x, _] => x) seen in elem node reduced

getNeighborsNotSeen : Int -> List(List Int) -> List(List Int) -> List Int
getNeighborsNotSeen node edges seen =
  let
    edgesByNode = filter (\[x, y] => x == node || y == node) edges
    neighbors = map (\[x, y] => if x == node then y else x) edgesByNode
    in
  sort $ filter (\x => not (isNodeSeen x seen)) neighbors

searchGraph : List(List Int) -> List Int -> List(List Int) -> List(List Int)
searchGraph _ [] seen = seen
searchGraph edges queue seen =
  let
    nextNode = intAt 0 queue
    neighbors = getNeighborsNotSeen nextNode edges seen
    newSeen = seen ++ (map (\x => [x, nextNode]) neighbors)
    newQueue = (drop 1 queue) ++ neighbors
  in
  searchGraph edges newQueue  newSeen

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse input array
    edges = map (map stoi) $ map words inputData
    -- Process data
    results = searchGraph edges [0] [[0, -1]]
    -- Sort "seen nodes"
    sorted = sortBy (sortSeenNodes) results
    -- Get "seen by node" list
    seenBy = map (\[_, y] => y) sorted
  -- Print results
  putStrLn $ unwords $ map show seenBy

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  -1 44 0 13 33 38 16 25 44 15 11 13 2 0 12 33 44 13 38 38 2 23 12 0 17
  0 33 13 36 25 29 25 7 2 43 20 44 45 0 43 25 20 43 2 0 0
-}
