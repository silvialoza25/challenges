-- hlint ludsrill.hs
-- No hints

import Control.Monad
import Data.List
import Data.Map (Map)
import qualified Data.Map as Map

main = do
  input <- getLine
  inputs <- replicateM (read $ head (tail (words input)) :: Int) getLine
  let tree = map (toInt . words) inputs
  let hashmap = myMap (read $ head (words input) :: Int)
  let result = Map.toList (breadthFirstSearch tree [0] [] hashmap)
  mapM_ printing result

printing :: (Int, [Int]) -> IO ()
printing (a, [b, c]) = do
  putStr (show c)
  putStr " "

myMap :: Int -> Map Int [Int]
myMap n = Map.fromList (map makePair [0..(n-1)])
   where makePair x = (x, [0, -1])

toInt :: [String] -> [Int]
toInt = map read

breadthFirstSearch :: [[Int]] -> [Int] -> [Int] -> Map Int [Int]
                      -> Map Int [Int]
breadthFirstSearch (x:xs) (vertex:ys) = getQueue (x:xs) (vertex:ys)
  where
    getQueue (x:xs) []          path hashmap = hashmap
    getQueue []     []          path hashmap = hashmap
    getQueue []     (vertex:ys) path hashmap = getQueue (x:xs)
                                                (ys ++ sort path) [] hashmap
    getQueue (x : xs) (vertex : ys) path hashmap
      | head (hashmap Map.! 0) == 0 && vertex == 0 =
        getQueue (x : xs) (vertex : ys) (path ++ [-1])
          (Map.insert 0 [1, -1] hashmap)
      | head x == vertex && head (hashmap Map.! head (tail x)) == 0 =
        getQueue xs (vertex : ys) (path ++ tail x)
          (Map.insert (head (tail x)) [1, vertex] hashmap)
      | head (tail x) == vertex && head (hashmap Map.! head x) == 0 =
        getQueue xs (vertex : ys) (path ++ [head x])
          (Map.insert (head x) [1, vertex] hashmap)
      | otherwise = getQueue xs (vertex : ys) path hashmap

-- cat DATA.lst | runhaskell ludsrill.hs
-- -1 26 23 20 23 31 31 31 14 18 19 0 34 20 31 13 20 22 20 23 0 29 20 0 4 3 11
-- 11 36 27 2 0 4 7 0 31 3 19 26 29
