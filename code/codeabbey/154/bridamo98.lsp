#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defstruct
  (node (:conc-name dr-))
  value
  seen
  neighbors
)

(defun add-last(value)
  (setq queue (append queue value))
)

(defun get-first()
  (setq value (car queue))
  (setq queue (cdr queue))
  (return-from get-first value)
)

(defun initialize-structures (amount-nodes nodes)
  (loop for i from 0 to (- amount-nodes 1)
    do(setf (aref nodes i)
      (make-node
        :value i
        :seen (- 0 1)
        :neighbors (make-array amount-nodes :initial-element 0)
      )
    )
  )
)

(defun load-edge(nodes)
  (setq fth (read))
  (setq sth (read))
  (setf (aref (dr-neighbors (aref nodes fth)) sth) 1)
  (setf (aref (dr-neighbors (aref nodes sth)) fth) 1)
)

(defun load-input-data (amount-edges nodes)
  (loop for i from 0 to (- amount-edges 1)
    do(load-edge nodes)
  )
)

(defun update-neighbor (neighbor i)
  (if (= neighbor 1)
    (progn
      (if (= (dr-seen (aref nodes i)) (- 0 1))
        (progn
          (setf (dr-seen (aref nodes i)) (dr-value current))
          (add-last (list (aref nodes i)))
        )
      )
    )
  )
)

(defun find-neighbors(current)
  (loop for i from 0 to (- amount-nodes 1)
    do(update-neighbor (aref (dr-neighbors current) i ) i)
  )
)

(defun breadth-first-search()
  (loop
    (setq current (get-first))
    (find-neighbors current)
    (when (not queue) (return NIL))
  )
)

(defun print-solution(nodes)
  (format t "-1 ")
  (loop for i from 1 to (- amount-nodes 1)
    do(format t "~a " (dr-seen (aref nodes i)))
  )
)

(defvar queue '())
(defvar amount-nodes (read))
(defvar amount-edges (read))
(defvar nodes (make-array amount-nodes))

(initialize-structures amount-nodes nodes)
(load-input-data amount-edges nodes)
(defvar current NIL)
(setf (dr-seen (aref nodes 0)) 1)
(add-last (list (aref nodes 0)))
(breadth-first-search)
(print-solution nodes)

#|
  cat DATA.lst | clisp bridamo98.lsp
  -1 11 29 11 33 25 7 10 33 25 0 0 28 11 22 41 29 32 28 25 29 20
  24 11 0 10 28 1 10 0 25 28 11 10 2 32 29 33 1 16 1 10 25 29
|#
