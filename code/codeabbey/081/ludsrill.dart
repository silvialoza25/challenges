// dartanalyzer ludsrill.dart
// Analyzing ludsrill.dart...
// No issues found!

import 'dart:core';
import 'dart:io';

void main() {
  List<int> input = getData([]);
  List<List<String>> solution = input.map((e) => convertToBinary(e)).toList();
  List<int> counting = solution.map((e) => countBits(e)).toList();
  print(counting.join(" "));
}

List<int> getData(List<List<int>>input) {
  String getLine = stdin.readLineSync();
  if (getLine == null) {
      return input.sublist(1).first;
  } else {
      List<int> newLine = getLine.split(" ").map((e) => int.parse(e)).toList();
      return getData(input + [newLine]);
  }
}

List<String> convertToBinary(int value) {
  if (value > 0) {
    return value.toRadixString(2).split("");
  } else if (value < 0) {
    return complementofBinary((value * -1).toRadixString(2).split(""));
  } else {
    return ["0"];
  }
}

List<String> complementofBinary(List<String> value) {
  List<String> aux = completeBits(value).map((e) => (e == "1") ? "0" : "1")
    .toList();
  return twoComplement(aux.reversed.toList()).reversed.toList();
}

List<String> twoComplement(List<String> value) {
  if (value.first == "0") {
    return ["1"] + value.sublist(1);
  } else {
    return ["0"] + twoComplement(value.sublist(1));
  }
}

List<String> completeBits(List<String> value) {
  if (value.length < 32) {
    return completeBits(["0"] + value);
  } else {
    return value;
  }
}

int countBits(List<String> value) {
  return value.fold(0, (t, e) => (e == "1") ? 1 + t : t);
}

// cat DATA.lst | dart ludsrill.dart
// 17 13 2 21 20 1 24 21 15 0 8 28 29 4 25 30 24 13 2 5 4 9 31 11 9 5 24 27 11
// 15 10 14 14 13 20 19 13 32 24 14 26 30 15 12 9 26 5 16 5 17 17 30
