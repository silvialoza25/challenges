{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #081: Bit Count - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi s = fromMaybe 0 $ parseInteger {a=Int} s

bitCount : Int -> Int -> Int
bitCount 0 acc = acc
bitCount num acc = bitCount (num `div` 2) (acc + (num `mod` 2))

processTestCase : Int -> Int
processTestCase num =
  -- Two's Complement 32 bits
  -- It is needed to add 1 to 2**32, due to an error in the Double type
  let twosCompl = if num >= 0 then num else (cast ((2 `pow` 32) + 1)) + num in
  bitCount twosCompl 0

main : IO ()
main = do
  _ <- getLine -- Discarded line
  inputData <- getLine
  let
    -- Parse input array
    nums = map stoi $ words inputData
    -- Process data
    results = map processTestCase nums
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  19 6 27 29 2 11 2 15 8 14 14 27 12 29 19 24 17 13 5 27 15 27 16 2 21 20 3 18
  9 20 27 14 20 12 14 16 7 9 3 7 11 18 15 22 15 7 4 29 16 27 26 28 16 15 10 3
  30 3 13 12
-}
