;; $ clj-kondo --lint miyyer1946.clj
;; linting took 24ms, errors: 0, warnings: 0

(ns miyyer1946-081
(:gen-class)
(:require [clojure.string :as str])
(:require [clojure.core :as core]))

(defn main []
   (let [size-input (core/read-line) data (str/split (core/read-line) #" ")]
    (loop [x 1]
      (when (<= x (Integer. size-input))
        (let [result (. Integer toBinaryString (Integer. (nth data (- x 1))))]
        (print (count (re-seq #"1" result))"")
        (recur (+ x 1)))))))

(main)

;$ cat DATA.lst | clj miyyer1946.cljs
;8 7 9 28 27 4 14 22 22 4 4 13 26 15 18 7 2 22 7 7 28 3
;27 30 28 14 10 18 9 31 12 29 3 26 18 12 5 5 28 26 19
