/* $ npx eslint paolagiraldo.ts */

process.stdin.setEncoding('utf8');
process.stdin.on('readable', () => {
  const inputData = process.stdin.read();
  if (inputData !== null) {
    const diceSides = 6;
    const rowsData = inputData.split('\n').slice(1);
    rowsData.forEach((row: string) => {
      const nums = row.split(' ');
      const diceOne = (Number(nums[0]) % diceSides) + 1;
      const diceTwo = (Number(nums[1]) % diceSides) + 1;
      process.stdout.write(`${diceOne + diceTwo} `);
      return 'Output';
    });
  }
  return 'Data loaded';
});

/* $ tsc paolagiraldo.ts
$ cat DATA.lst | node paolagiraldo.js
10 9 7 9 5 4 6 6 6 6 3 7 6 10 5 10 10 12 5 8 8 7 5 11 5
 */
