# $ lintr::lint('nickkar.r')

operate <- function(cases, out=c(), index=1) {
    if (index > length(cases)) {
        return(out)
    }
    tmp <- unlist(strsplit(cases[index], " "))
    num1 <- as.double(tmp[1])
    num2 <- as.double(tmp[2])
    eq <- (num1 %% 6) + (num2 %% 6) + 2
    return(operate(cases, c(out, eq), index + 1))
}

main <- function() {
    data <- readLines("stdin")
    cases <- data[2:length(data)]
    out <- operate(cases)
    cat(paste(out, collapse = " "))
    cat("\n")
}

main()

# $ cat DATA.lst | rscript nickkar.r
# 8 9 8 7 5 9 3 4 6 8 3 3 7 10 3 10 9 8 8 2 9 8 6 6 10 7 10 9
