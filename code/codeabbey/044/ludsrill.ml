(*
$ ocp-lint --severity-limit 3
  Scanning files in project "."...
  Found '3' file(s)
  Running analyses... 3 / 3
  Merging database...
  == New Warnings ==
    * 0 file(s) were linted
    * 0 warning(s) were emitted:
    * 0 file(s) couldn't be linted
*)

let modulo x y =
  let result = x mod y in
  if result >= 0 then result
  else result + y

let double_dice_roll data =
  match data with
  | a::b::[] -> Printf.printf "%i " (modulo a 6 + 1 + modulo b 6 + 1)
  | _ -> ()

let splitting line =
  String.split_on_char ' ' line

let maybe_read_line () =
  try Some (read_line ()) with
  | End_of_file -> None

let rec get_data acc =
  match maybe_read_line () with
  | Some (line) -> get_data (acc @ [(List.sort compare
                                (List.map int_of_string (splitting line)))])
  | None -> List.iter double_dice_roll (List.tl acc)

let () = get_data []

(*
$ cat DATA.lst | ocaml ludsrill.ml
  7 10 8 3 5 6 10 7 5 9 8 6 4 9 4 9 3 9 4 9 9 3 4 2 7 8 8 8 2
*)
