;; $ clj-kondo --lint bridamo98.clj
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-044
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn calc-double-dice [fst-number snd-number]
  (let [fst-dice (+ (mod fst-number 6) 1) snd-dice (+ (mod snd-number 6) 1)]
    (+ fst-dice snd-dice)))

(defn solve-all[size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (let [args (str/split (core/read-line) #" ")]
          (recur (+ i 1)
          (str result (calc-double-dice
          (edn/read-string (args 0))
          (edn/read-string (args 1))) " ")))
          result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.clj
;; 8 6 8 7 9 2 7 11 4 3 5 5 9 8 10 6 4 8 9 4
