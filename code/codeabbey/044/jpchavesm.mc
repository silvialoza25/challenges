/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, char.

:- pred diceRoll(int::in,int::out) is det.
diceRoll(Rand,(Rand rem 6) + 1).

:- pred add(int::in, int::in, int::out) is det.
add(X, Y, X + Y).

:- pred doubleDiceRoll(list(string)::in,string::in,string::out) is det.
doubleDiceRoll([],FinalResult,FinalResult).
doubleDiceRoll([Line | Tail],PartialResult,FinalResult) :-
(
  string.words_separator(char.is_whitespace,Line) = Input,
  det_to_int(det_index0(Input,0)) = DiceX,
  det_to_int(det_index0(Input,1)) = DiceY,
  map(diceRoll,[DiceX,DiceY],DiceRolls),
  foldl(add,DiceRolls,0,DoubleDiceRoll),
  PartialResult1 = PartialResult ++ " " ++ int_to_string(DoubleDiceRoll),
  doubleDiceRoll(Tail,PartialResult1,FinalResult)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
                   list(string)::in,io::di,io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    list.det_drop(1,FileContents,ComputeList),
    doubleDiceRoll(ComputeList,"",FinalResult),
    io.print_line(FinalResult,!IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  8 6 8 7 9 2 7 11 4 3 5 5 9 8 10 6 4 8 9 4
*/
