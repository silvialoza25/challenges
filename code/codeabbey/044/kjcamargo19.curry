-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import IO
import List
import Read

getData :: IO String
getData = do
  x <- getContents
  return x

getSumPairDice :: Int -> Int -> Int
getSumPairDice numberOne numberTwo = result
  where
    remainderOne = rem numberOne 6
    remainderTwo = rem numberTwo 6
    result = (remainderOne + 1) + (remainderTwo + 1)

getValues :: String -> Int
getValues dataIn = result
  where
    array = words dataIn
    numberOne = readInt (array !! 0)
    numberTwo = readInt (array !! 1)
    result = getSumPairDice numberOne numberTwo

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> show (getValues x)) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 5 4 7 6 9 8 3 9 7 4 3 9 4 6 2 7 8 4 6 3 4 6 12 5 9 7
