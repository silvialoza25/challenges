#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun delimiterp (c)
  (char= c #\Space))

(defun split (string &key (delimiterp #'delimiterp))
  (loop
    :for beg = (position-if-not delimiterp string)
    :then (position-if-not delimiterp string :start (1+ end))
    :for end = (and beg (position-if delimiterp string :start beg))
    :when beg :collect (subseq string beg end)
    :while end))

(defun calc-checksum(size-input data result i)
  (cond ((> i (- size-input 1)) result)
    ((< i size-input) (calc-checksum size-input data
    (mod (* (+ result (parse-integer (nth i data))) 113) 10000007)
    (+ i 1)))))

(defun swap (fst-index snd-index data)
  (let ((aux NIL))
    (setq aux (nth snd-index data))
    (setf (nth snd-index data) (nth fst-index data))
    (setf (nth fst-index data) aux)
    (return-from swap data)))

(defun bubble-sort (data size-input i num-of-swaps)
  (cond ((> i (- size-input 1)) num-of-swaps)
  ((< (parse-integer (nth i data)) (parse-integer (nth (- i 1) data)))
  (bubble-sort (swap (- i 1) i data) size-input (+ i 1) (+ num-of-swaps 1)))
  ((>= (parse-integer (nth i data)) (parse-integer (nth (- i 1) data)))
  (bubble-sort data size-input (+ i 1) num-of-swaps))))

(defun main ()
  (let ((data (split (read-line))) (size-input NIL)
  (num-of-swaps NIL)(checksum NIL))
    (setq size-input (- (length data) 1))
    (setq num-of-swaps (bubble-sort data size-input 1 0))
    (setq checksum (calc-checksum size-input data 0 0))
    (format t "~a ~a" num-of-swaps checksum)))

(main)

#|
  cat DATA.lst | clisp bridamo98.lsp
  41 6096309
|#
