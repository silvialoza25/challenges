/*
  $ rustfmt vmelendez.rs
  $ rustc vmelendez.rs
  $
*/

fn checksum(input: &Vec<i32>) -> i32 {
  let mut result = 0;

  for i in 0..input.len() {
    result = ((result + input[i]) * 113) % 10000007;
  }

  return result;
}

fn main() -> std::io::Result<()> {
  let mut input: Vec<i32> = {
    let mut a = String::new();
    std::io::stdin().read_line(&mut a).unwrap();
    a.split_whitespace()
      .map(|x| x.trim().parse::<i32>().unwrap())
      .collect()
  };

  let mut t;
  let mut s = 0;
  for i in 0..input.len() - 2 {
    if input[i] > input[i + 1] {
      s += 1;
      t = input[i];
      input[i] = input[i + 1];
      input[i + 1] = t;
    }
  }

  let mut new_vec = Vec::new();
  for i in 0..input.len() - 1 {
    new_vec.push(input[i]);
  }

  println!("{} {}", s, checksum(&new_vec));
  Ok(())
}

/*
  $ cat .\DATA.lst | .\vmelendez.exe
  41 1962385
*/
