/*
  $ rustfmt vmelendez.rs
  $ rustc vmelendez.rs
  $
*/

use std::io::BufRead;

fn check_neighbors(
  matrix: &mut [[i32; 11]; 9],
  row: usize,
  column: usize,
  cells_die: &mut [[i32; 11]; 9],
  cells_born: &mut [[i32; 11]; 9],
) {
  let mut neighbors = 0;
  let res = matrix[row + 1][column + 1];
  if res == 1 {
    neighbors -= 1;
  }

  for i in 0..=2 {
    for j in 0..=2 {
      if matrix[row + i + 1 - 1][column + j + 1 - 1] == 1 {
        neighbors += 1;
      }
    }
  }

  if matrix[row + 1][column + 1] == 1 && neighbors < 2 || neighbors > 3 {
    cells_die[row + 1][column + 1] = 1;
  }

  if matrix[row + 1][column + 1] != 1 && neighbors == 3 {
    cells_born[row + 1][column + 1] = 1;
  }
}

fn count_cells(matrix: &mut [[i32; 11]; 9]) -> i32 {
  let mut res = 0;
  for i in 1..=7 {
    for j in 1..=9 {
      if matrix[i][j] == 1 {
        res += 1;
      }
    }
  }
  return res;
}

fn next_matrix(
  matrix: &mut [[i32; 11]; 9],
  cells_die: &mut [[i32; 11]; 9],
  cells_born: &mut [[i32; 11]; 9],
) {
  for i in 1..=8 {
    for j in 1..=10 {
      if cells_born[i][j] == 1 {
        matrix[i][j] = 1;
      }
      if cells_die[i][j] == 1 {
        matrix[i][j] = 0;
      }
    }
  }
}

fn reset_matrix(cells_die: &mut [[i32; 11]; 9], cells_b: &mut [[i32; 11]; 9]) {
  for i in 1..=7 {
    for j in 1..=9 {
      cells_b[i][j] = 0;
      cells_die[i][j] = 0;
    }
  }
}

fn gameoflife(
  matrix: &mut [[i32; 11]; 9],
  cells_die: &mut [[i32; 11]; 9],
  cells_born: &mut [[i32; 11]; 9],
) {
  for i in 0..7 {
    for j in 0..9 {
      check_neighbors(matrix, i, j, cells_die, cells_born);
    }
  }
  next_matrix(matrix, cells_die, cells_born);
}

fn setup_matrix(matrix: &mut [[i32; 11]; 9], fragment: &str, index: usize) {
  let frag = fragment.chars().collect::<Vec<_>>();
  for i in 0..7 {
    if frag[i] == 'X' {
      matrix[index][i + 2] = 1;
    }
  }
}

fn solver(
  mut matrix: &mut [[i32; 11]; 9],
  mut cells_die: &mut [[i32; 11]; 9],
  mut cells_born: &mut [[i32; 11]; 9],
) {
  for _i in 0..5 {
    reset_matrix(&mut cells_die, &mut cells_born);

    gameoflife(&mut matrix, &mut cells_die, &mut cells_born);
    print!("{} ", count_cells(&mut matrix));
  }
}

fn init_matrix() -> [[i32; 11]; 9] {
  let matrix: [[i32; 11]; 9] = [
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
  ];

  return matrix;
}

fn init_cells_die() -> [[i32; 11]; 9] {
  let cells_die: [[i32; 11]; 9] = [
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
  ];

  return cells_die;
}

fn init_cells_born() -> [[i32; 11]; 9] {
  let cells_born: [[i32; 11]; 9] = [
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3],
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3],
  ];

  return cells_born;
}

fn main() -> std::io::Result<()> {
  let mut matrix = init_matrix();
  let mut cells_die = init_cells_die();
  let mut cells_born = init_cells_born();

  let stdin = std::io::stdin();
  for (i, line) in stdin.lock().lines().take(5).enumerate() {
    setup_matrix(&mut matrix, line.unwrap().trim(), i + 2);
  }

  solver(&mut matrix, &mut cells_die, &mut cells_born);
  Ok(())
}

/*
  $ cat DATA.lst | .\vmelendez.exe
  14 11 12 13 13
*/
