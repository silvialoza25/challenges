; clj-kondo --lint juanksepul.cljs
; linting took 338ms, errors: 0, warnings: 0

(ns codeabbey.juanksepul
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.set :as set]))

(defn read-data []
  (str/split-lines (core/slurp core/*in*)))

(defn is-org [char]
  (when (= "X" (str char)) true))

(defn orgs-set [grid]
  (set (for [i (range 5) j (range 7)]
          (when (is-org ((vec (grid i)) j)) [i j]))))

(defn tot-set []
  (set (for [i (range 5) j (range 7)] [i j])))

(defn filt-nil [set-in]
  (set (filter #(not= nil %) set-in)))

(defn process [grid]
  [(set (filt-nil (orgs-set grid)))
   (set/difference (tot-set) (orgs-set grid))])

(defn neighs [pos]
  (let [i (pos 0) j (pos 1)]
    (set (filter #(and (>= (% 0) 0) (>= (% 1) 0)
                  (<= (% 0) 4) (<= (% 1) 6))
            #{[(- i 1) (- j 1)] [(- i 1) j]
              [(- i 1) (+ j 1)] [i (- j 1)]
              [i (+ j 1)] [(+ i 1) (- j 1)]
              [(+ i 1) j] [(+ i 1) (+ j 1)]}))))

(defn cnt-neigh-orgs [pos orgs]
  (count (set/intersection orgs (neighs pos))))

(defn die [orgs]
  (set (filter #(not (or (= (cnt-neigh-orgs % orgs) 3)
                         (= (cnt-neigh-orgs % orgs) 2))) orgs)))

(defn born [spac orgs]
  (set (filter #(= (cnt-neigh-orgs % orgs) 3) spac)))

(defn new-grid [orgs spac]
  (let [new-orgs (set/union (set/difference orgs (die orgs))
                            (born spac orgs))]
    [new-orgs
     (set/difference (tot-set) new-orgs)]))

(defn life-is-simple [n-turns]
  (loop [turn n-turns
         [orgs spac] (process (read-data))]
    (if (= turn 0) (print (count (filt-nil orgs)))
        (do (when (< turn n-turns)
              (print (count (filt-nil orgs)) ""))
            (recur (dec turn) (new-grid orgs spac))))))

(defn !main []
  (life-is-simple 5))

(!main)

; cat DATA.lst | clj juanksepul.cljs
; 10 8 7 3 1
