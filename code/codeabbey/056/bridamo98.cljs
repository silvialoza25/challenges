;; $ clj-kondo --lint bridamo98.cljs
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-056
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str]))

(defn initialize-board []
  (loop [i 0 board []]
    (if (< i 17)
      (if (or (< i 6) (> i 10))
        (recur (+ i 1) (conj board (vec (repeat 19 "x"))))
        (recur (+ i 1) (conj board
        (into [] (concat (repeat 6 "x")
        (str/split (core/read-line) #"") (repeat 6 "x"))))))board)))

(defn nei-amount[board x y]
  (loop [i 0 res 0 nei [-1 -1 -1 0 -1 1 0 -1 0 1 1 -1 1 0 1 1]]
    (if (< i 8)
      (let [n-x (+ x (nei (* i 2))) n-y (+ y (nei (+ (* i 2) 1))) n-nei
      (get-in board [n-x n-y]) amount (if (= (compare n-nei "X") 0) 1 0)]
        (recur (+ i 1) (+ res amount) nei)) res)))

(defn survive-next-generation[board x y]
  (let [current-pos (get-in board [x y])
  n-amount (nei-amount board x y)]
    (if (= (compare current-pos "-") 0) (= n-amount 3)
      (or (= n-amount 3) (= n-amount 2)))))

(defn make-turn [board]
  (for [x (range 1 17)
      y (range 1 19)]
    (if (survive-next-generation board x y) [true x y] [false x y])))

(defn count-surv[surv-list]
  (loop [i 0 res 0]
    (if (< i (count surv-list))
      (let [tuple (nth surv-list i)  is-surv (tuple 0)
      i-res (if is-surv 1 0)]
        (recur (+ i 1) (+ res i-res))) res)))

(defn update-board [board surv-list]
  (loop [i 0 res-board board]
    (if (< i (count surv-list))
      (let [tuple (nth surv-list i) is-surv (tuple 0) x (tuple 1) y (tuple 2)
      new-board (if is-surv (assoc-in res-board [x y] "X")
      (assoc-in res-board [x y] "-"))]
        (recur (+ i 1) new-board)) res-board)))

(defn solve [board]
  (loop [i 0 i-board board result []]
    (if (< i 5)
      (let [surv-list (make-turn i-board)
      new-board (update-board board surv-list)
      i-result (count-surv surv-list)]
        (recur (+ i 1) new-board (conj result i-result))) result)))

(defn main []
  (let [board (initialize-board)]
    (doseq [item (solve board)]
      (print item ""))
      (println)))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;;19 19 26 19 21
