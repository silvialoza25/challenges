;; $ clj-kondo --lint bridamo98.cljs
;; linting took 24ms, errors: 0, warnings: 0

(ns bridamo98-158
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn exp [x n]
  (if (zero? n) 1
    (* x (exp x (dec n)))))

(defn set-cur-parity [parity-list]
  (if (> (count parity-list) 0)
    (nth parity-list 0)
    ""))

(defn set-cur-bit [cur-pos cur-parity msg bit]
  (if (= cur-pos cur-parity)
    bit
    (if (> (count msg) 0)
      (subs msg 0 1)
      "")))

(defn set-nxt-msg[msg]
  (if (> (count msg) 0)
    (subs msg 1 (count msg))
    msg))

(defn set-nxt-par-list [parity-list]
  (if (> (count parity-list) 0)
    (apply list
      (subvec (vec parity-list) 1
        (count parity-list)))
    parity-list))

(defn set-nxt-pos-error [is-enc pos-error cur-par-bit counter]
  (if is-enc
    pos-error
    (if (= cur-par-bit "1")
      (+ pos-error
        (exp 2 counter))
      pos-error)))

(defn set-parity [cur-pos cur-parity parity-list]
  (if (= cur-pos cur-parity)
    (set-nxt-par-list parity-list)
    parity-list))

(defn init-msg-encode [cur-pos msg parity-list]
  (if (or (not= (count msg) 0) (not= (count parity-list) 0))
    (let [cur-parity (set-cur-parity parity-list)
          cur-bit (set-cur-bit cur-pos cur-parity msg "0")
          nxt-msg (if (= cur-pos cur-parity)
                    msg
                    (set-nxt-msg msg))
          nxt-par-list (set-parity cur-pos cur-parity parity-list)]
      (str cur-bit
        (init-msg-encode (+ cur-pos 1) nxt-msg nxt-par-list)))
    ""))

(defn build-parity-check-list [cur-group step max-up-limit]
  (let [down-limit (* step  (+ (* 2 cur-group) 1))
        up-limit (min max-up-limit (* step  (+ (* 2 cur-group) 2)))
        pos-range (range down-limit up-limit)]
    (if (= up-limit max-up-limit)
      pos-range
      (concat pos-range
        (build-parity-check-list (+ cur-group 1) step max-up-limit)))))

(defn build-parity-list [max-up-limit is-enc]
  (loop [x 0 res ()]
    (let [cur-index (exp 2 x)]
      (if (or (and (<= cur-index (+ max-up-limit (count res))) is-enc)
              (<= cur-index max-up-limit))
        (recur (+ x 1) (concat res (list cur-index)))
        res))))

(defn calc-parity [parity-check-list msg cur-res]
  (if (> (count parity-check-list) 0)
    (let [cur-index (- (nth parity-check-list 0) 1)
          cur-bit (subs msg cur-index (+ cur-index 1))
          nxt-res (if (= cur-res cur-bit) "0" "1")
          nxt-par-list (set-nxt-par-list parity-check-list)]
      (calc-parity nxt-par-list msg nxt-res))
    cur-res))

(defn calc-parity-bits [msg parity-list is-enc pos-error counter]
  (if (> (count parity-list) 0)
    (let [cur-index (nth parity-list 0)
          cur-par-bit (calc-parity (build-parity-check-list
                        0 cur-index (+ (count msg) 1)) msg "0")
          nxt-pos-error (set-nxt-pos-error is-enc
                          pos-error cur-par-bit counter)
          nxt-msg (str (subs msg 0 (- cur-index 1))
                    cur-par-bit
                    (subs msg cur-index (count msg)))
          nxt-par-list (set-nxt-par-list parity-list)]
      (calc-parity-bits (if is-enc nxt-msg msg)
        nxt-par-list is-enc nxt-pos-error (+ counter 1)))
    (if (or is-enc (= pos-error 0))
      msg
      (let [char-to-fix (subs msg (- pos-error 1) pos-error)
            fixed-char (if (= char-to-fix "0") "1" "0")]
        (str (subs msg 0 (- pos-error 1)) fixed-char
          (subs msg pos-error (count msg)))))))

(defn delete-parity-bits [cur-pos msg parity-list]
  (if (or (not= (count msg) 0) (not= (count parity-list) 0))
    (let [cur-parity (set-cur-parity parity-list)
          cur-bit (set-cur-bit cur-pos cur-parity msg "")
          nxt-msg (set-nxt-msg msg)
          nxt-par-list (set-parity cur-pos cur-parity parity-list)]
      (str cur-bit (delete-parity-bits (+ cur-pos 1) nxt-msg nxt-par-list)))
    ""))

(defn encode-msg [msg]
  (let [parity-list (build-parity-list (count msg) true)
        initial-enc-msg (init-msg-encode 1 msg parity-list)
        encoded-msg (calc-parity-bits initial-enc-msg parity-list true 0 0)]
    encoded-msg))

(defn decode-msg [msg]
  (let [parity-list (build-parity-list (count msg) false)
        fixed-msg (calc-parity-bits msg parity-list false 0 0)
        decoded-msg (delete-parity-bits 1 fixed-msg parity-list)]
    decoded-msg))

(defn process-msgs [cases-num enc-or-dec]
  (loop [x 0 res ""]
    (if (< x cases-num)
      (let [msg (core/read-line)
            proc-msg (if (= enc-or-dec 0) (encode-msg msg) (decode-msg msg))]
        (recur (+ x 1) (str res proc-msg " ")))
      res)))

(defn main []
  (print (process-msgs (edn/read-string (core/read-line)) 0))
  (print (process-msgs (edn/read-string (core/read-line)) 1))
  (println)
)

(main)

;; $ cat DATA.lst | clj -M bridamo98.cljs
;; 10110100000 1100110 0010110101000001010 1010110110 10011110110111100110110
;; 10001001100101001001000101 111101010010100010111101000110 01001010101101000
;; 0111010110100011011111 0111100110100101101001 10011001000110111011000
;; 1011111101 11101011010 1100111011111011100 001110101000010001001110111100
;; 001101001000011111010101 00001010111100011011 0100010100100000 00011
;; 01110001010111 011001010 0001100110010010001101 001100110001110011
;; 01110000101 00101110100101001011111000 11000 010011100 010111000 00100
;; 10011000001101010010100110 0000100011000 0001110001110111111 1100010
