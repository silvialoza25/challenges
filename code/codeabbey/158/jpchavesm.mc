/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module bool, string, list, int, integer, char.

:- func addParityBit(list(char),int) = list(char).
addParityBit(InputList,Position) = NewList :-
(
  list.det_split_list(Position,InputList,FirstList,SecondList),
  list.append(FirstList,['0'],NewFirstList),
  list.append(NewFirstList,SecondList,NewList)
).

:- func addParityBits(list(char),int) = list(char).
addParityBits(InputList,Exp) = ReturnList :-
(
  if pow(2,Exp)-1 >= length(InputList)
  then ReturnList = InputList
  else
    addParityBit(InputList,pow(2,Exp)-1) = NewList,
    ReturnList = addParityBits(NewList,Exp+1)
).

:- func checkIndexAux(list(char),int) = bool.
checkIndexAux(StrBinIndex,CharPos) = IsSignificant :-
(
  if det_index1(StrBinIndex,CharPos) = '1'
  then IsSignificant = yes
  else IsSignificant = no
).

:- func checkIndex(integer,int) = bool.
checkIndex(Index,Exp) = IsSignificant :-
(
  if
    integer.to_base_string(Index,2) = StrBinIndex,
    to_char_list(StrBinIndex) = BinList,
    length(BinList) - Exp = CharPos,
    CharPos > 0
  then IsSignificant = checkIndexAux(BinList,CharPos)
  else IsSignificant = no
).

%only way the map will work
:- pred add(int::in, int::in, int::out) is det.
add(X, Y, X + Y).

%turns out using char.det_from_int(0) outputs the null char
:- func int2char(int) = char.
int2char(Int) = Char :-
(
  if Int = 1 then Char = '1' else Char = '0'
).
%char.to_int outputs weird results too
:- func char2int(char) = int.
char2int(Char) = Int :-
(
  if Char = '1' then Int = 1 else Int = 0
).

:- func setParityBit(list(char),list(char),int,int) = char.
setParityBit(List,Sublist,Exp,Pos) = Bit :- %1-based position
(
  if Pos > length(List)
  then
    map(char.to_int,Sublist) = NumSubList,
    foldl(add,NumSubList,0,Sum),
    Bit = int2char(Sum mod 2)
  else
  (
    if
      checkIndex(integer(Pos),Exp) = IsSignificant,
      IsSignificant = yes
    then
      list.det_index1(List,Pos,SigChar),
      append(Sublist,[SigChar],NewSubList),
      Bit = setParityBit(List,NewSubList,Exp,Pos+1)
    else Bit = setParityBit(List,Sublist,Exp,Pos+1)
  )
).

:- func setParityBits(list(char),int) = string.
setParityBits(List,Exp) = ParityList :-
(
  if pow(2,Exp) > length(List)
  then
    ParityList = from_char_list(List)
  else
    setParityBit(List,[],Exp,1) = NewChar,
    list.det_replace_nth(List,pow(2,Exp),NewChar,NewList), %1-based index
    ParityList = setParityBits(NewList,Exp+1)
).

:- func checkParityBits(list(char),list(char),int) = string.
checkParityBits(List,ParityList,Exp) = BadParityStr :-
(
  if pow(2,Exp) > length(List)
  then
    BadParityStr = from_char_list(reverse(ParityList))
  else
    setParityBit(List,[],Exp,1) = NewParityBit,
    append(ParityList,[NewParityBit],NewParityList),
    BadParityStr = checkParityBits(List,NewParityList,Exp+1)
).

:- func fixError(string,int) = string.
fixError(InputString,ErrorBit) = OutputString :- %1-based position
(
  if det_index(InputString,ErrorBit-1) = '1'
  then det_set_char('0',ErrorBit-1,InputString,OutputString)
  else det_set_char('1',ErrorBit-1,InputString,OutputString)
).

:- func powersOfTwo(int,int) = list(int).
powersOfTwo(Step,Limit) = %0-based position
(
  if
    pow(2,Step) - 1 = Power,
    Power < Limit
  then [Power] ++ powersOfTwo(Step+1,Limit)
  else []
).

:- func removePadding(string,string,int) = string.
removePadding(InputStr,POriginalStr,Index) = %0-based position
(
  if Index >= length(InputStr)
  then POriginalStr
  else
  (
    if
      powersOfTwo(0,length(InputStr)) = Powers,
      member(Index,Powers)
    then removePadding(InputStr,POriginalStr,Index+1)
    else removePadding(InputStr,
          POriginalStr ++ from_char(det_index(InputStr,Index)),Index+1)
  )
).

:- func unpaddedHamming(string,int) = string.
unpaddedHamming(InputString,ErrorBit) = DecodedString :-
(
  if ErrorBit = 0
  then DecodedString = removePadding(InputString,"",0)
  else
    fixError(InputString,ErrorBit) = FixedString,
    DecodedString = removePadding(FixedString,"",0)
).

:- pred calcHamming(string, string).
:- mode calcHamming(in, out) is det.
calcHamming(InputString,OutputString) :-
(
  to_char_list(InputString) = CharList,
  addParityBits(CharList,0) = PaddedCharList,
  setParityBits(PaddedCharList,0) = OutputString
).

:- pred checkHamming(string, string).
:- mode checkHamming(in, out) is det.
checkHamming(InputString,DecodedString) :-
(
  to_char_list(InputString) = CharList,
  checkParityBits(CharList,[],0) = ErrorBitStr,
  det_base_string_to_int(2,ErrorBitStr) = ErrorBit,
  DecodedString = unpaddedHamming(InputString,ErrorBit)
).

:- pred unwords(string::in, string::in, string::out) is det.
unwords(X, Y, X ++ " " ++ Y).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, "", !IO).

:- pred read_lines(io.text_input_stream::in,string::in,io::di,io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ " " ++ Line,
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    string.words_separator(char.is_whitespace,FileContents) = Input,
    list.det_head(Input) = StrSep,
    string.det_to_int(StrSep) = Separator,
    list.det_split_list(Separator+1,Input,PComputeList,PCheckList),

    list.det_drop(1,PComputeList,ComputeList),
    map(calcHamming,ComputeList,ComputedList),
    foldr(unwords,ComputedList,"",ComputedString),

    list.det_drop(1,PCheckList,CheckList),
    map(checkHamming,CheckList,CheckedList),
    foldr(unwords,CheckedList,"",CheckedString),

    io.print_line(ComputedString ++ CheckedString,!IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  10110100000 1100110 0010110101000001010 1010110110 10011110110111100110110
  10001001100101001001000101 111101010010100010111101000110 01001010101101000
  0111010110100011011111 0111100110100101101001 10011001000110111011000
  1011111101 11101011010 1100111011111011100 001110101000010001001110111100
  001101001000011111010101 00001010111100011011 0100010100100000 00011
  01110001010111 011001010 0001100110010010001101 001100110001110011
  01110000101 00101110100101001011111000 11000 010011100 010111000 00100
  10011000001101010010100110 0000100011000 0001110001110111111 1100010
*/
