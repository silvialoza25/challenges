{-
  $ idris2 jmesa85.idr -o jmesa85
  $
-}

-- CodeAbbey #214: Sick Travellers - Idris2

module Main
import Data.List
import Data.Strings
import Data.Nat

-- No Maybe in this parsing
strToInt : String -> Int
strToInt s =
  case parseInteger {a=Int} s of
    Nothing => 0
    Just j  => j

-- Implemented manually due to an error in the internal library
elemAt : (n : Nat) -> (xs : List String) -> String
elemAt 0 [] = ""
elemAt (S _) [] = ""
elemAt Z (x :: xs) = x
elemAt (S k) (_ :: xs) = elemAt k xs

-- Gets lines in the Stdin
getLines : IO (List String)
getLines = do
  x <- getLine
  if x == "" then pure []
    else do
      xs <- getLines
      pure (x :: xs)

-- Gets a city from a route for a given day
partial getCityByDay : Nat -> List String -> String
getCityByDay day route =
  let cityIndex = modNat day (length route) in
  elemAt cityIndex route

-- Returns a list of  cities with the disease
getCitiesWithDisease : List String -> List String -> List String
getCitiesWithDisease toFilter status =
  let
    withDisease = filter (\(x, y) => y /= "healthy") $ zip toFilter status in
  nub $ map (\(x, y) => x) withDisease

-- Returns a sorted string of travellers with the disease
getTravellersWithDisease : List String -> List String -> String
getTravellersWithDisease names status =
  let
    withDisease = filter (\(x, y) => y /= "healthy") $ zip names status in
  unwords $ sort $ map (\(x, y) => x) withDisease

-- Advances travellers status
-- "sick" -> "recovering" -> "healthy"
partial getTravellersNewStatus :
  List String -> List String -> List String -> List String
getTravellersNewStatus status todayCities sickCities =
  map (\(x, y) => case x of
    "sick" => "recovering"
    "recovering" => "healthy"
    "healthy" => if elem y sickCities then "sick" else "healthy"
    ) $ zip status todayCities

-- Gets the number of sick travellers for a given status list
countSick : Eq String => List String -> Nat
countSick = length . filter (\x => x /= "healthy")

-- Main simulation logic
partial simulate :
  List String -> List String -> List (List String) -> Nat -> String
simulate names status _ 99 =
  (getTravellersWithDisease names status) ++ " " ++ (show 99)
simulate names status routes day =
  let
    todayCities = map (getCityByDay day) routes
    sickCities = getCitiesWithDisease todayCities status
    newStatus = getTravellersNewStatus status todayCities sickCities
    sickCounter = countSick newStatus
    in
  if sickCounter == 0
  then (getTravellersWithDisease names status) ++ " " ++ (show day)
  else simulate names newStatus routes (day + 1)

-- Point of entry
partial main : IO ()
main = do
  -- Read Stdin
  inputData <- getLines
  let
    -- Get whole travellers data
    travellers = map words $ drop 1 inputData
    -- Get travellers names
    names = map (elemAt 0) $ map (take 1) travellers
    -- Get medical conditions
    status = map (elemAt 0) $ map (take 1) $ map (drop 1) travellers
    -- Get route per traveller
    routes =
      map (split (== '-'))
      $ map (elemAt 0)
      $ map (take 1)
      $ map (drop 2) travellers
    -- Simulate since day 0
    results = simulate names status routes 0
  -- Print results
  putStrLn results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  Hal 26
-}
