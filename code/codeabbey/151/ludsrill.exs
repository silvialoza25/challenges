# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.08 seconds (0.02s to load, 0.06s running 55 checks on 1 file)
# 11 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule WordLadders do
  def main do
    all_data = IO.read(:stdio, :all)
    try do
      [data, data_base] = get_data(all_data)
      dictionary = clean_bd(data_base, [])
      Enum.map(data, fn [start, finish] ->
        get_solution(start, finish, dictionary) end)
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_solution(start, finish, dictionary) do
    path(finish, breadth_first_search(dictionary, %{start => "Start"},
      [start], finish, dictionary), 0)
  end

  def path(search, map, count) do
    if search != "Start" do
      path(Map.get(map, search), map, count + 1)
    else
      IO.write("#{count} ")
    end
  end

  def get_data(all_data) do
    data = String.split(all_data, "\n")
            |> Enum.map(&String.split(&1, " "))
            |> Enum.map(&Enum.reject(&1, fn x -> x == "" end))
            |> Enum.reject(&Enum.empty?/1)
    [Enum.slice(data, 1..String.to_integer(hd(hd(data)))),
      Enum.slice(data, String.to_integer(hd(hd(data))) + 1..length(data))]
  end

  def clean_bd([], acc), do: acc
  def clean_bd([head | tail], acc) do
    if String.length(hd(head)) < 5 or String.length(hd(head)) > 5 do
      clean_bd(tail, acc)
    else
      clean_bd(tail, acc ++ head)
    end
  end

  def breadth_first_search([], map, [], _finish, _dictionary), do: map
  def breadth_first_search([], map, queue, finish, dictionary) do
    breadth_first_search(dictionary, map, tl(queue), finish, dictionary)
  end
  def breadth_first_search([head | tail], map, queue, finish, dictionary) do
    aux_head = Enum.reject(String.split(head, ""), fn x -> x == "" end)
    aux_queue = Enum.reject(String.split(hd(queue), ""), fn x -> x == "" end)

    if compare_word(aux_head, aux_queue, 0) == 4 and
      Map.get(map, head) == nil do
      if head != finish do
        breadth_first_search(tail, Map.put(map, head, hd(queue)),
          queue ++ [head], finish, dictionary)
      else
        breadth_first_search([], Map.put(map, head, hd(queue)), [],
          finish, dictionary)
      end
    else
      breadth_first_search(tail, map, queue, finish, dictionary)
    end
  end

  def compare_word(head, queue, count) do
    if length(queue) > 0 do
      if hd(head) == hd(queue) do
        compare_word(tl(head), tl(queue), count + 1)
      else
        compare_word(tl(head), tl(queue), count)
      end
    else
      count
    end
  end
end

WordLadders.main()

# cat DATA.lst words.lst | elixir ludsrill.exs
# 6 7 8 5 9 4 16 13 7 10 14 14
