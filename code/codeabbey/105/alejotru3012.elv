# elvish -compileonly alejotru3012.elv

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn get_triangle_area [s_a s_b s_c]{
  sp_s = (/ (+ $s_a $s_b $s_c) 2)
  area = (math:sqrt (* $sp_s (- $sp_s $s_a) (- $sp_s $s_b) (- $sp_s $s_c)))
  put $area
}

fn get_polygon_area [m_pt pts index sum]{
  if (== $index (count $pts)) {
    echo $sum
  } else {
    side_a = (math:sqrt (+ (math:pow (math:abs (- $pts[$index][0] ^
      $pts[(- $index 1)][0])) 2) (math:pow (math:abs (- $pts[$index][1] ^
      $pts[(- $index 1)][1])) 2)))
    side_b = (math:sqrt (+ (math:pow (math:abs (- $pts[$index][0] $m_pt[0])) ^
      2) (math:pow (math:abs (- $pts[$index][1] $m_pt[1])) 2)))
    side_c = (math:sqrt (+ (math:pow (math:abs (- $pts[(- $index 1)][0] ^
      $m_pt[0])) 2) (math:pow (math:abs (- $pts[(- $index 1)][1] ^
      $m_pt[1])) 2)))
    new_sum = (+ $sum (get_triangle_area $side_a $side_b $side_c))
    get_polygon_area $m_pt $pts (+ $index 1) $new_sum
  }
}

fn round_to [num decimals]{
  mult = (math:pow 10 $decimals)
  aux = (* $num $mult)
  round = (math:round $aux)
  put (/ $round $mult)
}

fn calculate [data]{
  main_point = $data[0]
  get_polygon_area $main_point $data[1..] 1 0
}

echo (round_to (calculate (input_data)) 2)

# cat DATA.lst | elvish alejotru3012.elv
# 61625797.5
