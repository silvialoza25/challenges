###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #105: Convex Polygon Area - Coffeescript

# We can compute area of a polygon using Shoelace formula
calculateElement = (vertices, auxIndex) ->
  # Base case
  if auxIndex == vertices.length
    return 0
  # Recursive case
  if auxIndex == vertices.length - 1
    x1 = vertices[auxIndex][0]
    y1 = vertices[auxIndex][1]
    x2 = vertices[0][0]
    y2 = vertices[0][1]
  else
    x1 = vertices[auxIndex][0]
    y1 = vertices[auxIndex][1]
    x2 = vertices[auxIndex + 1][0]
    y2 = vertices[auxIndex + 1][1]
  (x1 * y2 - x2 * y1) + calculateElement(vertices, auxIndex + 1)

# We can compute area of a polygon using Shoelace formula
calculateArea = (vertices) ->
  Math.abs(calculateElement(vertices, 0)) / 2

# Convert each string to two integers representing x and y
proccessPoint = (valueAsString) ->
  valueAsString.split(' ').map(Number)

main = ->
  # Read data from STDIN
  data = require('fs').readFileSync(0).toString().trim().split('\n')
  # Get points
  vertices = data.slice(1).map(proccessPoint)
  # Calculate area
  result = calculateArea(vertices)
  # Print results
  console.log(result)

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
61261745
###
