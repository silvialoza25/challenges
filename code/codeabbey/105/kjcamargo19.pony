// ponyc -b kjcamargo19

use "format"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun iterator(data: Array[String]): Array[String] =>
          data.slice(1, data.size())

        fun format_answer(value: F64): String =>
          Format.float[F64](value where fmt=FormatFixLarge, prec= 1)

        fun insert_values(data: Array[U32]): Array[U32]? =>
          data.push(data(0)?)
          data.push(data(1)?)
          data

        fun get_area( data: Array[U32], const_i': USize,
                      left':U32, right': U32) =>
          var const_i: USize = const_i'
          var left: U32 = left'
          var right: U32 = right'
          var area: F64 = (left.f64() - right.f64()) * 0.5
          if const_i < data.size() then
            try
              left = left + (data(const_i)? * data(const_i+3)?)
              right = right + (data(const_i+1)? * data(const_i+2)?)
            end
            get_area(data, const_i+2,left,right)
          else
            env.out.print(format_answer(area))
          end

        fun obtain_data(data: Array[String],
                        newArray': Array[U32]): Array[U32]? =>
          var newArray: Array[U32] = newArray'
          if data.size() > 0 then
            var line: Array[String] = data(0)?.split_by(" ")
            newArray.push(line(0)?.u32()?)
            newArray.push(line(1)?.u32()?)
            obtain_data( iterator (data), newArray)?
          end
          newArray

        fun ref apply(data: Array[U8] iso) =>
          var input_data: String = String.from_iso_array(consume data)
          var line_sep: Array[String] = input_data.split_by("\n")
          var real_data: Array[String] = line_sep.slice(1, line_sep.size()-1)

          try
            var size: USize = line_sep(0)?.usize()?.mul(2)
            var array: Array[U32] = obtain_data(real_data, Array[U32](size))?
            get_area(insert_values(array)?,0,0,0)
          end

        fun ref dispose() =>
          None

      end,
      512
    )
// cat DATA.lst | ./kjcamargo19
// 53230518.5
