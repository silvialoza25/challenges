/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, float, char.

:- func str2Vertex(string) = list(float).
str2Vertex(InputStr) = Vertex :-
(
  string.words_separator(char.is_whitespace, InputStr) = StrVertex,
  det_to_float(det_index0(StrVertex, 0)) = PosX,
  det_to_float(det_index0(StrVertex, 1)) = PosY,
  Vertex = [PosX, PosY]
).

:- func triangleArea(list(float), list(float), list(float)) = float.
triangleArea(Vertex1,Vertex2,Vertex3) = Area :-
(
  det_index0(Vertex1, 0, PosX1),
  det_index0(Vertex1, 1, PosY1),
  det_index0(Vertex2, 0, PosX2),
  det_index0(Vertex2, 1, PosY2),
  det_index0(Vertex3, 0, PosX3),
  det_index0(Vertex3, 1, PosY3),
  Area = (PosX1*PosY2 + PosX2*PosY3 + PosX3*PosY1 -
    PosX1*PosY3 - PosX2*PosY1 - PosX3*PosY2) / 2.0
).

:- pred findArea(list(string), list(float), list(float), float, float).
:- mode findArea(in, in, in, in, out) is det.
findArea([], _, _, FinalArea, FinalArea).
findArea([Line | Tail], Vertex1, Vertex2, PartialArea, FinalArea) :-
(
  str2Vertex(Line) = Vertex3,
  triangleArea(Vertex1, Vertex2, Vertex3) = TriangleArea,
  PartialArea1 = PartialArea + TriangleArea,
  findArea(Tail, Vertex1, Vertex3, PartialArea1, FinalArea)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
                   list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1, FileContents, ComputeList),
    %Find the coords of Vertex 1
    det_index0(ComputeList, 0) = FirstLine,
    str2Vertex(FirstLine) = Vertex1,
    %Find the coords of Vertex 2
    det_index0(ComputeList, 1) = SecondLine,
    str2Vertex(SecondLine) = Vertex2,
    %Drop them from the list and compute the polygon area
    det_drop(1, ComputeList, ComputeList1),
    det_drop(1, ComputeList1, ComputeListFinal),
    findArea(ComputeListFinal, Vertex1, Vertex2, 0.0, FinalArea),
    io.write_float(FinalArea, !IO),
    io.nl(!IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  61261745.0
*/
