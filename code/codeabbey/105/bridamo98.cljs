;; $ clj-kondo --lint bridamo98.cljs
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-105
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn ** [x n] (reduce *
(repeat (core/bigint n) (core/bigint  x))))

(defn distance[X1 Y1 X2 Y2]
  (let [fst (** (- X2 X1) 2) snd (** (- Y2 Y1) 2)]
    (Math/sqrt (+ fst snd))))

(defn find-area[X1 Y1 X2 Y2 X3 Y3]
  (let [a (distance X1 Y1 X2 Y2)
  b (distance X2 Y2 X3 Y3)
  c (distance X3 Y3 X1 Y1) s (/ ( + a b c) 2)]
    (Math/sqrt (* s (-  s a) (-  s b) (-  s c)))))

(defn get-data[size-input]
  (loop [i 0 res []]
    (if (< i size-input)
      (let [args (str/split (core/read-line) #" ")
      a (edn/read-string (args 0))
      b (edn/read-string (args 1))]
        (recur (+ i 1) (conj res [a b])))
      res)))

(defn get-mid-point [size-input data-input]
  (loop [i 0 x-res 0 y-res 0]
    (if (< i size-input)
      (let [tuple (data-input i) xi-res (first tuple)
      yi-res (second tuple)]
        (recur (+ i 1) (+ x-res xi-res) (+ y-res yi-res)))
      [(quot x-res i) (quot y-res i)])))

(defn get-poly-area [size-input data-input]
  (let [middle-point (get-mid-point size-input data-input)
  mid-x (first middle-point) mid-y (second middle-point)]
    (loop [i 0 res 0]
      (if (< i (- size-input 1))
        (let [tuple (data-input i) tuple2 (data-input (+ i 1))
        xi-res (first tuple) yi-res (second tuple)
        xi-res2 (first tuple2) yi-res2 (second tuple2)
        ires (find-area mid-x mid-y xi-res yi-res xi-res2 yi-res2)]
          (recur (+ i 1) (+ res ires))
        )
        (+ res (find-area mid-x mid-y (first (data-input i))
        (second (data-input i)) (first (data-input 0))
        (second (data-input 0))))))))

(defn main []
  (let [size-input (edn/read-string (core/read-line))
  data-input (get-data size-input)]
    (println (get-poly-area size-input data-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 6.451329300000001E7
