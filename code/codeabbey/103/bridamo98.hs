{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.Map (fromListWith, toList)
import Data.List
import Control.Monad

main = do
  inputData <- getLine
  let initialState = map read (words inputData) :: [Int]
  secondInputData <- replicateM (length initialState) getLine
  let dependencies = map ((toInt . drop 1) . words) secondInputData
  let posibleActions = replicateM (length initialState) [0, 1]
  let actionSolution = findSolution initialState dependencies posibleActions 0
  let solution = buildSolution (posibleActions !! actionSolution) 0
  putStrLn (unwords solution)

buildSolution :: [Int] -> Int -> [String]
buildSolution [] counter = []
buildSolution action counter = res
  where
    iAction = head action
    res = if iAction == 1
            then show counter : buildSolution (drop 1 action) (counter + 1)
            else buildSolution (drop 1 action) (counter + 1)

findSolution :: [Int] -> [[Int]] -> [[Int]] -> Int -> Int
findSolution initialState dependencies [] counter = counter
findSolution initialState dependencies posibleActions counter = res
  where
    posAction = head posibleActions
    posSolution = validateSolution initialState dependencies posAction
    res = if posSolution
            then counter
            else findSolution initialState dependencies
                 (drop 1 posibleActions) (counter + 1)

validateSolution :: [Int] -> [[Int]] -> [Int] -> Bool
validateSolution initialState [] [] = 1 `notElem` initialState
validateSolution initialState dependencies posAction = res
  where
    iAction = head posAction
    iDependency = head dependencies
    nxtInitialState = if iAction == 1
                        then makeAction initialState iDependency
                        else initialState
    res = validateSolution nxtInitialState
          (drop 1 dependencies) (drop 1 posAction)

makeAction :: [Int] -> [Int] -> [Int]
makeAction initialState [] = initialState
makeAction initialState iDependency = res
  where
    iChange = head iDependency
    nxtVal = if initialState !! iChange == 0
               then 1 :: Int
               else 0 :: Int
    nxtInitialState = take iChange initialState ++
                      [nxtVal] ++ drop (iChange + 1) initialState
    res = makeAction nxtInitialState (drop 1 iDependency)

toInt :: [String] -> [Int]
toInt dat = res
  where
    res = map read dat :: [Int]

{-
$ cat DATA.lst | ./bridamo98
  3 4 5 8 9 17
-}
