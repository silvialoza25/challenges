#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect line)))

(defun combinations (s n)
  (cond ((zerop n) (list nil))
        ((null s) nil)
        (T (nconc (mapcar #'(lambda (c) (cons (car s) c))
                   (combinations (cdr s) (1- n))) (combinations (cdr s) n)))))

(defun split (item)
  (loop for i = 0 then (1+ j)
    as j = (position #\: item :start i)
    when (> i 0)
        collect (subseq item (+ i 1) j)
        while j))

(defun split-by-one-space (item)
  (loop for i = 0 then (1+ j)
    as j = (position #\Space item :start i)
    collect (parse-integer (subseq item i j))
    while j))

(defun organize-data ()
  (let ((data)
        (delete-colon ())
        (organized-data ())
        (egg-straight-line)
        (toggle-egg-color))

    (setq data (read-data))
    (setq egg-straight-line (split-by-one-space (car data)))
    (setq toggle-egg-color (cdr data))

    (dolist (item toggle-egg-color)
      (setq delete-colon (append delete-colon (list (split item)))))

    (dolist (item delete-colon)
      (dolist (touched-egg item)
        (setq organized-data (append organized-data
          (list (split-by-one-space touched-egg))))))
    (return-from organize-data
      (list organized-data egg-straight-line data))))

(defun easter-eggs ()
  (let ((eggs (list 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19))
        (flag 0)
        (eggs-to-toggle ())
        (all-combinations)
        (organized-data-aux)
        (organized-data)
        (egg-straight-line)
        (data))

    (setq organized-data-aux (organize-data))
    (setq organized-data (first organized-data-aux))
    (setq egg-straight-line (second organized-data-aux))
    (setq data (third organized-data-aux))

    (loop for i from 1 to (- (length organized-data) 1)
      do(setq all-combinations (combinations eggs i))
      (dolist (one-combination all-combinations)
        (setq eggs-to-toggle ())

        (dolist (touched-egg one-combination)
          (setq eggs-to-toggle (append eggs-to-toggle
            (elt organized-data touched-egg))))

        (dolist (single-egg eggs-to-toggle)
          (setf (elt egg-straight-line single-egg)
            (cond ((= (elt egg-straight-line single-egg) 0) '1)
                  (T '0))))

        (when (= (reduce #'max egg-straight-line) 0)
          (setq flag 1)
          (loop for i in one-combination
            do(format t "~s " i))
          (return))

        (setq egg-straight-line (split-by-one-space (car data))))

      (if (= flag 1)
          (return)))))

(easter-eggs)

#|
cat DATA.lst | clisp ludsrill.lsp
1 2 4 5 7 8 9 10 11 14 16
|#
