{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.Strings
import Data.Maybe
import Data.List

toIntegerNat : Nat -> Int
toIntegerNat Z = 0
toIntegerNat (S k) = 1 + toIntegerNat k

fromIntegerNat : Int -> Nat
fromIntegerNat 0 = Z
fromIntegerNat n =
  if (n > 0) then
    S (fromIntegerNat (assert_smaller n (n - 1)))
  else
    Z

toInt : String -> Int
toInt dat = fromMaybe 0 (parseInteger {a=Int} dat)

colAt : (n : Nat) -> (xs : List (List Int)) -> List Int
colAt 0 [] = []
colAt (S _) [] = []
colAt Z (x :: xs) = x
colAt (S k) (_ :: xs) = colAt k xs

rowAt : (n : Nat) -> (xs : List Int) -> Int
rowAt 0 [] = 0
rowAt (S _) [] = 0
rowAt Z (x :: xs) = x
rowAt (S k) (_ :: xs) = rowAt k xs

elemAt : Nat -> Nat -> List (List Int) -> Int
elemAt i j matrix = rowAt j (colAt i matrix)

findIndex : List String -> String -> Nat -> Nat
findIndex [] item curIndex = curIndex
findIndex list item curIndex =
  let
    curItem = fromMaybe "" (head' list)
  in
  if curItem == item
    then curIndex
    else findIndex (drop 1 list) item (curIndex + 1)

replaceElement : Nat -> Int -> List Int -> List Int
replaceElement i x xs =
  let
    left = take i xs
    right = drop (i + 1) xs
  in left ++ [x] ++ right

replaceRow : Nat -> List Int -> List (List Int) -> List (List Int)
replaceRow i x xs =
  let
    left = take i xs
    right = drop (i + 1) xs
  in left ++ [x] ++ right

changeElem : Nat -> Nat -> Int -> List (List Int) -> List (List Int)
changeElem row col x xs =
  let
    rowToReplaceIn = colAt row xs
    modifiedRow = replaceElement col x rowToReplaceIn
  in replaceRow row modifiedRow xs

injectValue : a -> IO a
injectValue dat = pure dat

getInputData : IO(List String)
getInputData = do
  line <- getLine
  if line == ""
    then injectValue []
    else do
      xs <- getInputData
      injectValue (line :: xs)

addToList : String -> List String -> List String
addToList element list =
  let
    posElem =
      if elem element list
        then []
        else [element]
  in list ++ posElem

getUniqueStates : List (List String) -> List String -> List String
getUniqueStates [] result = result
getUniqueStates roads result =
  let
    road = fromMaybe [] (head' roads)
    fstState = fromMaybe "" (head' road)
    sndState = fromMaybe "" (last' road)
    nxtResult = addToList sndState (addToList fstState result)
  in getUniqueStates (drop 1 roads) nxtResult

create2dArray : Nat -> Nat -> List (List Int)
create2dArray rows cols = replicate rows (replicate cols 9999999)

initMatrix : List (List String) -> List (List Int) -> List String
  -> Nat -> List (List Int)
initMatrix [] matrix states cont = matrix
initMatrix roads matrix states cont =
  let
    road = fromMaybe [] (head' roads)
    fstState = fromMaybe "" (head' road)
    sndState = fromMaybe "" (last' road)
    fstIndex = findIndex states fstState 0
    sndIndex = findIndex states sndState 0
    nxtMatrix =
      if cont < (length matrix)
        then changeElem cont cont 0 (changeElem sndIndex fstIndex 1
          (changeElem fstIndex sndIndex 1 matrix))
        else changeElem sndIndex fstIndex 1
          (changeElem fstIndex sndIndex 1 matrix)
  in initMatrix (drop 1 roads) nxtMatrix states (cont + 1)

floydwarshall : List (List Int) -> List (List Nat) -> List (List Int)
floydwarshall matrix [] = matrix
floydwarshall matrix indexes =
  let
    iteration = fromMaybe [] (head' indexes)
    k = fromMaybe 0 (head' iteration)
    i = fromIntegerNat (rowAt 1 (map toIntegerNat iteration))
    j = fromMaybe 0 (last' iteration)
    nxtMatrix =
      if (elemAt i j matrix) > ((elemAt i k matrix) + (elemAt k j matrix))
        then changeElem i j ((elemAt i k matrix) + (elemAt k j matrix))
          matrix
        else matrix
  in floydwarshall nxtMatrix (drop 1 indexes)

buildSolution : List (List Int) -> List (List String)
  -> List String -> List String
buildSolution finalMatrix [] states = []
buildSolution finalMatrix problems states =
  let
    problem = fromMaybe [] (head' problems)
    fstState = fromMaybe "" (head' problem)
    sndState = fromMaybe "" (last' problem)
    fstIndex = findIndex states fstState 0
    sndIndex = findIndex states sndState 0
    singleSol = show (elemAt fstIndex sndIndex finalMatrix)
  in [singleSol] ++ buildSolution finalMatrix (drop 1 problems) states

main : IO ()
main = do
  dat <- getInputData
  let
    numOfRoads = toInt (fromMaybe "" (head' dat))
    roads = map words (take (fromIntegerNat numOfRoads) (drop 1 dat))
    problems = map words (drop ((fromIntegerNat numOfRoads) + 2) dat)
    states = sort (getUniqueStates roads [])
    matrix = initMatrix roads
      (create2dArray (length states) (length states)) states 0
    limit = minus (length matrix) 1
    range = [0,1..limit]
    indexes = [[x, y, z]| x<- range, y<- range, z<- range]
    finalMatrix = floydwarshall matrix indexes
    solution = buildSolution finalMatrix problems states
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  2 3 2 5 4 2 2 3 2 2 2
-}
