;; clj-kondo --lint ludsrill.cljs
;; linting took 54ms, errors: 0, warnings: 0

(ns ludsrill.065
    (:gen-class)
    (:require [clojure.string :as str])
    (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn modify-map [map vertex start-vertex]
  (if (or (some #{start-vertex} (first (get map vertex)))
          (= vertex start-vertex))
    map
    (let [auxmap (update map vertex #(vector (conj (first %) start-vertex)
           (conj (second %) 1)))
          finalmap (update auxmap start-vertex #(vector (conj (first %) vertex)
           (conj (second %) 1)))]
    finalmap)))

(defn graph-generator [map pair]
  (if (empty? pair)
    map
    (let [road (first pair)
          start-vertex (first road)
          vertex (nth road 2)
          aux-map (modify-map map vertex start-vertex)]
    (recur aux-map (rest pair)))))

(defn make-hashmap [map roads-names]
  (if (seq roads-names)
    (recur (assoc map (first roads-names) [[][]]) (rest roads-names))
    map))

(defn make-hashmap-distance [map start roads-names]
  (if (seq roads-names)
    (if (= (first roads-names) start)
      (recur (assoc map start 0) start (rest roads-names))
      (recur (assoc map (first roads-names) 99999999) start (rest roads-names)))
    map))

(defn modify-distance-map [vertex weight distance-map start]
  (if (seq vertex)
    (if (< (+ (get distance-map start) (first weight))
           (get distance-map (first vertex)))
      (let [aux (assoc distance-map (first vertex) (+ (get distance-map start)
             (first weight)))]
      (recur (rest vertex) (rest weight) aux start))
      (recur (rest vertex) (rest weight) distance-map start))
    distance-map))

(defn get-following [distance-map new-visited]
  (let [sorted-map (sort-by second distance-map)]
    (if (some #{(first (first sorted-map))} new-visited)
      (recur (rest sorted-map) new-visited)
      (first (first sorted-map)))))

(defn dijkstra [map visited distance-map start roads-names]
  (if (seq roads-names)
    (let [neighbor (get map start)
          new-distance-map (modify-distance-map (first neighbor)
           (second neighbor) distance-map start)
          new-visited (conj visited start)
          following (get-following (into [] new-distance-map) new-visited)]

    (recur map new-visited new-distance-map following (rest roads-names)))
    distance-map))

(defn solve-cases [cases roads-names final-hashmap]
  (when (seq cases)
    (let [start-vertex (first (first cases))
          end-vertex (nth (first cases) 2)
          make-distance-map (make-hashmap-distance {} start-vertex roads-names)
          solution (into (sorted-map) (dijkstra final-hashmap []
            make-distance-map start-vertex roads-names))]
    (print (get solution end-vertex) "")
    (recur (rest cases) roads-names final-hashmap))))

(defn -main []
  (let [data (get-data)
        roads (take (Integer. (first (first data))) (rest data))
        cases (subvec (into (vector) data)
          (+ (Integer. (first (first data))) 2))
        roads-names (remove #(= % "-") (distinct (flatten roads)))
        hashmap (make-hashmap {} roads-names)
        final-hashmap (graph-generator hashmap roads)]
    (solve-cases cases roads-names final-hashmap)))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 3 2 2 2 5 2 4 2 3 2 3 4 2 3 2 4
