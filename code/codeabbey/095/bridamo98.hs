{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Data.Char
import Control.Monad
import Numeric

main = do
  dat <- getLine
  let yearsRange =  convert dat
  let fstY = head yearsRange
  let sndY = yearsRange !! 1
  let n = sndY - fstY + 1
  dat2 <- replicateM n getLine
  let varVectors = getVectors dat2
  let x = head varVectors
  let y = last varVectors
  let xx = map (^ 2) x
  let xy = zipWith (*) x y
  let a = (fromIntegral (sum y * sum xx - sum x * sum xy) :: Double) /
          (fromIntegral (n * sum xx - sum x ^ 2) :: Double)
  let b = (fromIntegral (n * sum xy - sum x * sum y) :: Double) /
          (fromIntegral (n * sum xx - sum x ^ 2) :: Double)
  let solution = [showFullPrecision b, showFullPrecision a]
  putStrLn (unwords solution)

showFullPrecision :: Double -> String
showFullPrecision x = showFFloat Nothing x ""

convert :: String -> [Int]
convert dat = result
  where
    aux = words dat
    result = map read aux :: [Int]

getVectors ::[String] -> [[Int]]
getVectors dat = res
  where
    divDat = map words dat
    filteredDat = map filterData divDat
    xVector = map head filteredDat
    yVector = map last filteredDat
    res = [xVector, yVector]

filterData :: [String] -> [Int]
filterData dat = res
  where
    aux1 = read (dat !! 1) :: Int
    aux2 = read (dat !! 2) :: Int
    res = [aux1, aux2]

{-
$ cat DATA.lst | ./bridamo98
  1.7421983660414455 69.6940912540926
-}
