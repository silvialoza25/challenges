/**
  $ haxelib run checkstyle \
    --exitcode \
    --show-parser-errors \
    --nothreads \
    --default-config \
    -s jmesa85.hx
**/

// CodeAbbey #095: Simple Linear Regression - Haxe

using StringTools;
using Lambda;

class Main {
  static public function main():Void {
    // Read Stdin
    var data = Sys.stdin().readAll().toString().trim().split("\n");
    // Get the test cases
    var testCasesStr:Array<String> = data.slice(1);
    // Format each test case, removing the year
    var pointArr = testCasesStr.map(formatTestCase);
    // Calculate linear regression
    var coefficients = linearRegression(pointArr);
    trace(coefficients.join(" "));
  }

  // Returns x and y values as integers
  static function formatTestCase(testCaseStr:String):Array<Int> {
    var record = testCaseStr.split(":")[1].trim().split(" ");
    var pointX = Std.parseInt(record[0]);
    var pointY = Std.parseInt(record[1]);
    return [pointX, pointY];
  }

  // Returns the coefficients b(slope) and a(y-intercept)
  static function linearRegression(pointArr:Array<Array<Int>>):Array<Float> {
    var count = pointArr.length;
    var sumX = pointArr.fold(
      function(item, acc) return item[0] + acc, 0);
    var sumY = pointArr.fold(
      function(item, acc) return item[1] + acc, 0);
    var sumXY = pointArr.fold(
      function(item, acc) return (item[0] * item[1]) + acc, 0);
    var sumX2 = pointArr.fold(
      function(item, acc) return (item[0] * item[0]) + acc, 0);
    var alpha = (sumY * sumX2 - sumX * sumXY) / (count * sumX2 - sumX * sumX);
    var beta = (count * sumXY - sumX * sumY) / (count * sumX2 - sumX * sumX);
    return [beta, alpha];
  }
}

/**
  $ cp jmesa85.hx Main.hx && cat DATA.lst | haxe -main Main --interp
  1.91898434137153107 110.743615586405724
**/
