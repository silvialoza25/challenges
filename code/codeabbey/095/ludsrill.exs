# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.02s to load, 0.07s running 55 checks on 1 file)
# 5 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule SimpleLinearRegression do

  def main do
    data = get_data()
    linear_regression(data)
  end

  def linear_regression(data) do
    sum_x = Enum.reduce(data, 0, fn [x, _], acumm -> x + acumm end)
    sum_y = Enum.reduce(data, 0, fn [_, y], acumm -> y + acumm end)
    x_dot_y = Enum.reduce(data, 0, fn [x, y], acumm -> x * y + acumm end)
    x_dot_x = Enum.reduce(data, 0, fn [x, _], acumm -> x * x + acumm end)
    a = (sum_y * x_dot_x - sum_x * x_dot_y) /
          (Enum.count(data) * x_dot_x - sum_x * sum_x)
    b = (Enum.count(data) * x_dot_y - sum_x * sum_y) /
          (Enum.count(data) * x_dot_x - sum_x * sum_x)
    IO.write("#{b} #{a}")
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def get_data do
    [_ | tail] = List.delete(String.split(IO.read(:stdio, :all), "\n"), "")
    Enum.map(Enum.map(Enum.map(tail, &String.split(&1, " ")),
      &Enum.slice(&1, 1..(length(&1)))), &to_int/1)
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e SimpleLinearRegression.main
# 1.5752793164576655 69.54675459194061
