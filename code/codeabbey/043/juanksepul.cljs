; clj-kondo --lint juanksepul.cljs
; linting took 170ms, errors: 0, warnings: 0

(ns codeabbey.juanksepul
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn dice-rollling [num]
 (+ 1 (int (* 6 num))))

(defn read-data []
  (str/split-lines (core/slurp core/*in*)))

(defn !main []
  (doseq [item (rest (read-data))]
    (println (dice-rollling (edn/read-string item)))))

(!main)

; cat DATA.lst | clj juanksepul.cljs
; 3
; 1
; 4
; 6
; 4
; 5
; 2
; 3
; 3
; 3
; 6
; 2
; 2
; 2
; 5
; 6
; 1
; 5
; 1
; 3
; 6
; 6
; 3
