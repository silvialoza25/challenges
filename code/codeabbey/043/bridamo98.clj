;; $ clj-kondo --lint bridamo98.clj
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-043
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn calc-dice-point[number]
  (Math/round (+ (Math/floor(* 6 number)) 1)))

(defn solve-all[size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (recur (+ i 1)
        (str result (calc-dice-point (edn/read-string
        (core/read-line))) " "))
        result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.clj
;; 2 4 6 4 3 3 4 4 4 2 6 6 3 1 4 3 1 5 6 3 4 4 6 6
