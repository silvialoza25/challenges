/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, float, int.

:- pred diceRoll(float::in, int::out) is det.
diceRoll(RandNum, floor_to_int(RandNum * 6.0) + 1).

:- pred roundNums(list(string), string, string).
:- mode roundNums(in, in, out) is det.
roundNums([], FinalStr, strip(FinalStr)).
roundNums([Line | Tail], PartialStr, FinalStr) :-
(
  det_to_float(strip(Line)) = Num,
  diceRoll(Num, DiceRoll),
  PartialStr1 = PartialStr ++ " " ++ from_int(DiceRoll),
  roundNums(Tail, PartialStr1, FinalStr)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1,FileContents, InputList),
    roundNums(InputList, "", ResultStr),
    io.print_line(ResultStr, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  3 2 1 5 4 3 2 1 1 3 2 2 1 6 1 1 2 4 6 5 1 5 4 4 6 6
*/
