# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.07 seconds (0.02s to load, 0.05s running 55 checks on 1 file)
# 5 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule DiceRolling do

  def main do
    data = get_data()
    Enum.map(data, &dice_rolling/1)
  end

  def to_float(list) do
    Enum.map(Enum.map(Enum.map(list, &Float.parse/1), &Tuple.to_list/1),
      &List.first/1)
  end

  def get_data do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    [_ | tail] = List.delete_at(all_data, 1 +
                  String.to_integer(Enum.at(all_data, 0)))
    Enum.map(Enum.map(tail, &String.split(&1, " ")), &to_float/1)
  end

  def dice_rolling(item) do
    IO.write("#{Kernel.trunc(:math.floor(hd(item) * 6) + 1)} ")
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e DiceRolling.main
# 4 4 2 5 1 2 2 4 5 1 6 5 6 6 3 1 2 5 1 6 1 1 4 4
