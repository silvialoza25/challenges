/* $ npx eslint paolagiraldo.ts */

process.stdin.setEncoding('utf8');
process.stdin.on('readable', () => {
  const inputData = process.stdin.read();
  if (inputData !== null) {
    const rowsData = inputData.split('\n').slice(1);
    rowsData.forEach((row: string) => {
      const dice = 6;
      process.stdout.write(`${Math.floor(Number(row) * dice) + 1} `);
      return 'Output';
    });
  }
  return 'Data loaded';
});

/* $ tsc paolagiraldo.ts
$ cat DATA.lst | node paolagiraldo.js
3 1 4 6 4 5 2 3 3 3 6 2 2 2 5 6 1 5 1 3 6 6 3
 */
