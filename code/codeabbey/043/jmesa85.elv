# $ elvish -compileonly jmesa85.elv
# $

# CodeAbbey #043: Dice Rolling - Elvish

main = [data]{
  each [item]{
    randomValue = (float64 $item)
    newValue = (+ (* $randomValue 6) 1)
    printf "%.1s " $newValue
  } $data[1:]
}

$main $args

# $ elvish jmesa85.elv $(cat DATA.lst)
# 3 2 1 5 4 3 2 1 1 3 2 2 1 6 1 1 2 4 6 5 1 5 4 4 6 6
