# elvish -compileonly cyberpunker.elv

#!/bin/elvish

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn diceRolling [diceA random]{
  var add = (+ (* $diceA $random) 1)
  var floor = (math:floor $add)
  echo $floor
}

fn playGame [data]{
  each [values]{
    var mRandom = 6
    var valueN = $values[0]
    diceRolling $valueN $mRandom
  } $data
}

echo (playGame (input_data))

# cat DATA.lst | elvish cyberpunker.elv

# 4 3 1 1 3 5 1 5 4 3 1 6 1 4 5 6 6 6 5 1 5 3 3 5 6 1 4 5 4 2
