/*
$ ./golint vmelendez.go
$ go run vmelendez.go
*/
package main

import (
  "bufio"
  "fmt"
  "os"
  "strconv"
  "strings"
)

func josephus(n, k int) int {
  if (n == 1) {
    return 1;
  }
  return (josephus(n - 1, k) + k-1) % n + 1;
}

func main() {
  f, err := os.Open("DATA.lst")
  if err != nil {
    fmt.Printf("error opening file: %v\n", err)
    os.Exit(1)
  }

  scanner := bufio.NewScanner(f)
  var x[1]string
  i := 0
  for scanner.Scan() {
    x[i] = scanner.Text()
    i++
  }

  s := strings.Split(x[0], " ")
  n, _ := strconv.Atoi(s[0])
  k, _ := strconv.Atoi(s[1])

  fmt.Printf("%v", josephus(n, k));
}
/*
$ go run vmelendez.go
54
*/
