;; clj-kondo --lint ludsrill.cljs
;; linting took 33ms, errors: 0, warnings: 0

(ns ludsrill.032
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn new-list [neutrals aux step]
  (let [aux (mod (+ aux (- step 1)) (count neutrals))]
    (remove #{(nth neutrals aux)} neutrals)))

(defn josephus-problem [step neutrals aux]
  (if (> (count neutrals) 1)
    (josephus-problem step (new-list neutrals aux step) (mod (+ aux (- step 1))
      (count neutrals)))
    neutrals))

(defn -main []
  (let [data (first (get-data))
        reach (Integer. (first data))
        step (Integer. (second data))
        neutrals (range 1 (+ reach 1))]
    (println (first (josephus-problem step neutrals 0)))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 3
