// ponyc -b asalgad2

use "collections"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail( array: Array[I64] ): Array[I64] =>
          array.slice(1, array.size())

        fun print_array( array: Array[I64] ) ? =>
          if array.size() >= 1 then
              env.out.print( array(0)?.string() )
              print_array( tail( array ) )?
          end

        fun find_next( array: Array[I64], position: USize, const_n: USize,
                       const_k: USize, const_i: USize ): USize ? =>
          if const_i == const_k then
            return position
          else
            var current: USize = (position + 1) % const_n
            if array(current)? == 0 then
              find_next( array, current, const_n, const_k, const_i+1 )?
            else
              find_next( array, current, const_n, const_k, const_i )?
            end
          end

        fun josephus( array: Array[I64], position: USize,
                      const_n: USize, const_k: USize, const_i: USize ) ? =>

          if const_i < (const_n-1) then
            var pos: USize = find_next(array, position, const_n, const_k, 0)?
            array.update( pos, 1 )?
            josephus( array, pos, const_n, const_k, const_i+1 )?
          end

        fun find_survival( array: Array[I64], position: USize ): USize ? =>
          if array(position)? == 0 then
            position + 1
          else
            find_survival( array, position + 1 )?
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")

          try
            var args: Array[String] = lines(0)?.split(" ")
            var const_n: USize = args(0)?.usize()?
            var const_k: USize = args(1)?.usize()?

            var positions: Array[I64] = Array[I64].init(0, const_n)

            josephus( positions, -1, const_n, const_k, 0 )?
            var result: USize = find_survival( positions, 0 )?
            env.out.print( result.string() )
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// 30
