#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun josephus (people cons-step)
  (if (= people 1)
    people
    (+ (mod (+ (josephus (- people 1) cons-step) (- cons-step 1)) people) 1)))

(defun main ()
  (let ((people (read)) (cons-step (read)))
    (format t "~a~%" (josephus people cons-step))))

(main)

#|
  cat DATA.lst | clisp bridamo98.lsp
  17
|#
