/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#define MAX_SIZE 1024

static int josephus (/*@in@*/ char people[],
  /*@reldef@*/ int k, /*@reldef@*/ int index)
  /*@modifies nothing@*/ {
  int l = (int) strlen(people);
  int i = 0, z = 0;
  int j;
  static char survivors[MAX_SIZE];
  j = index;
  for (i = 0; i < l; i++) {
    if (i == j) { j += k; }
    else { survivors[z] = people[i]; z++; }
  }
  if (z > 1) { i = josephus(survivors, k, j % l); }
  else { printf("%d", (int) survivors[0]); return 1; }
  return 0;
}

int main(void) {
  int n, k;
  int i = 0;
  char people[MAX_SIZE];
  char line[MAX_SIZE];
  if (fgets(line, MAX_SIZE, stdin) == 0) { return 0; }
  if (sscanf(line, "%d %d", &n, &k) == 0) { return 0; }
  for (i = 0; i < n; i++) { people[i] = (char) (i + 1); }
  people[i] = '\0';
  i = josephus(people, k, k-1);
  printf("\n");
  return i;
}

/*
$ cat DATA.lst | ./slayfer1112
46
*/
