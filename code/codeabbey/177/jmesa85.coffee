###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #177: Nim Game - Coffeescript
# Note: The token expires after 1 hour

notifyServer = (url, token, message) ->
  request = require('request')
  request.post(
    { url: url, form: message },
    (e, http, body) -> nimGame(url, token, body)
  )
  return

# Given by the server
parseHeaps = (message) ->
  auxStr = message.split('\n').filter((s) -> s.includes('heaps:'))
  auxStr[0].split(' ').slice(1).map(Number)

# Parse the victory token
parseToken = (message) ->
  auxStr = message.split('\n').filter((s) -> s.includes('end:'))
  auxStr[0].split(' ').slice(1)[0]

# Returns the selected heap and the stones to take
processMove = (heapsArr) ->
  nimAddition = heapsArr.reduce((a, b) -> a ^ b)
  # Find the move that fulfills the condition
  moves = heapsArr.filter((stones) -> (stones ^ nimAddition) < stones)
  fromHeap = heapsArr.indexOf(moves[0])
  stonesToTake = heapsArr[fromHeap] - (heapsArr[fromHeap] ^ nimAddition)
  [fromHeap, stonesToTake]

nimGame = (url, token, status) ->
  if status? # Not null or undefined
    if status.includes('begin')
      # Initial message
      notifyServer(url, token, { token: token })
    else if status.includes('end: game is lost')
      # Print the victory token
      console.log(status)
    else if status.includes('end:')
      # Print the victory token
      victoryToken = parseToken(status)
      console.log(victoryToken)
    else if status.includes('heaps:')
      # Parse the new heaps
      heaps = parseHeaps(status)
      # Process my move
      [heap, stones] = processMove(heaps)
      move = heap + " " + stones
      notifyServer(url, token, { token: token, move: move })
    else
      console.log(status)
  return

main = ->
  url = 'http://open-abbey.appspot.com/interactive/nim-game'
  # Read STDIN
  token = require('fs').readFileSync(0).toString().trim()
  # Init game
  nimGame(url, token, 'begin')
  return

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
VKHM0X0qCeLtaV9px/Ti7DIx
###
