{-
$ ghc -o  frank1030 frank1030.hs
[1 of 1] Compiling Main             ( frank1030.hs, frank1030.o )
Linking frank1030 ...

$ hlint frank1030.hs
No hints
-}

wordsWhen     :: (Char -> Bool) -> String -> [String]
wordsWhen p s =  case dropWhile p s of
  "" -> []
  s' -> w : wordsWhen p s''
    where (w, s'') = break p s'

toint :: [String] -> [Int]
toint = map read

main = do
  n <- getLine
  let numbers = toint (wordsWhen (== ' ') n)
  print (maximum numbers)
  print (minimum numbers)

{-
$ cat DATA.lst | ./frank1030
79767 -79840
-}
