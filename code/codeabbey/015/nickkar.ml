(*
$ ocamlc -w @0..1000 nickkar.ml
*)

open Printf
open List

(* divides the input lines to readeble separated parts *)
let read_input line =
  map int_of_string (String.split_on_char ' ' line)

(* gets the minimum and maximum value in a vector containing 300 elements *)
let rec find_min_max nums index max_num min_num =
  if index == (length nums) then [max_num; min_num]
  else (find_min_max nums (index+1)
  (max max_num (nth nums index)) (min min_num (nth nums index)))

(* prints the values *)
let prnt case =
  printf "%d %d" (nth case 0) (nth case 1)

(* driver code *)
let nums = read_input (read_line())
let() =
  prnt (find_min_max nums 0 min_int max_int);
  print_string "\n"

(*
$ cat DATA.lst | ocaml nickkar.ml
79767 -79840
*)
