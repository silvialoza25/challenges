###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #15: Maximum of array - Coffeescript

getMax = (arr) ->
  arr.reduce((a, b) -> Math.max(a, b))

getMin = (arr) ->
  arr.reduce((a, b) -> Math.min(a, b))

main = ->
  # Read data from STDIN
  data = require('fs').readFileSync(0).toString().trim().split('\n')
  # Get numbers
  numbers = data[0].split(' ').map(Number)
  # Get the maximum
  maxValue = getMax(numbers)
  # Get the minimum
  minValue = getMin(numbers)
  # Print results
  console.log(maxValue + " " + minValue)

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
79025 -79573
###
