--[[
$ luacheck bridamo98.lua
Checking bridamo98.lua                              OK
Total: 0 warnings / 0 errors in 1 file
--]]

local function split(s, delimiter)
    local result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match);
    end
    return result;
end

local function maximum_of_array()
  local numbers = split(io.read("*l"), " ")
  print(math.max(unpack(numbers)).." "..math.min(unpack(numbers)))
end

maximum_of_array()

--[[
$ cat DATA.lst | lua bridamo98.lua
79896 -79091
--]]
