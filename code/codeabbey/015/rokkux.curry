-- $ curry-verify rokkux.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
--------------------------------------------------------------------------
--No properties found in module `rokkux'!

sort :: Prelude.Ord a => [a] -> [a]
sort []     = []
sort (x:xs) = insert x (sort xs)

insert :: Prelude.Ord a => a -> [a] -> [a]
insert x [] = [x]
insert x (y:ys)
  | x <= y    = x:y:ys
  | otherwise = y : insert x ys

main :: Prelude.IO ()
main = do
  input <- getLine
  let   top = head (reverse (sort (map read(words input)::[Int])))
  let   bot = head (sort (map read(words input)::[Int]))
  putStrLn ((show top)++" "++(show bot))

-- $ cat DATA.lst | pakcs :load rokkux :eval main :quit
-- 79734 -79769
