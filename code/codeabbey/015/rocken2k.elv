# elvish -compileonly rocken2k.elv

use math
use str

#This function get the data from the DATA.lst file
fn get_data []{
  put [(each [number]{
    put [(str:split ' ' $number)]
  })][0..] #getting just the first line
}

#This function get the maximum and the minimum value from the data
fn maxmin [numbers]{
  each [number]{
    echo (math:max $@number)
    echo (math:min $@number)
  } $numbers
}

#This output the max and min value
echo (str:join ' ' [(maxmin (get_data))])

# cat DATA.lst | elvish rocken2k.elv
# 79734 -79769
