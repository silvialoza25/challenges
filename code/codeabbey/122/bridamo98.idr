{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.Strings
import Data.Maybe
import Data.List

toInt : String -> Int
toInt dat = fromMaybe 0 (parseInteger {a=Int} dat)

elemAt : (n : Nat) -> (xs : List Int) -> Int
elemAt 0 [] = 0
elemAt (S _) [] = 0
elemAt Z (x :: xs) = x
elemAt (S k) (_ :: xs) = elemAt k xs

setValueAt : List Int -> Nat -> Int -> List Int
setValueAt list index value =
  (take index list) ++ [value] ++ (drop (index + 1) list)

getResponse : (List String, List Int) -> List String
getResponse (response,_) = response

makeOrdering : List Int -> Nat -> Nat -> String -> Int -> (List Int, Nat)
makeOrdering array lt rt dir pivot =
  if lt < rt
    then if dir == "left"
      then if (elemAt rt array) > pivot
        then makeOrdering array lt (minus rt 1) dir pivot
        else makeOrdering (setValueAt array lt (elemAt rt array))
          (lt + 1) rt "right" pivot
      else if (elemAt lt array) < pivot
        then makeOrdering array (lt + 1) rt dir pivot
        else makeOrdering (setValueAt array rt (elemAt lt array))
          lt (minus rt 1) "left" pivot
    else (setValueAt array lt pivot, lt)

partition : List Int -> Nat -> Nat -> (List Int, Nat)
partition array left right =
  let
    lt = left
    rt = right
    dir = "left"
    pivot = elemAt left array
  in
  makeOrdering array lt rt dir pivot

quickSort : List Int -> Nat -> Nat -> (List String, List Int)
quickSort array left right =
  let
    curRes = (show left) ++ "-" ++ (show right)
    (nxtArray, pivotPos) = partition array left right
    (partialRes, partialNxtArray) =
      if minus pivotPos left > 1
        then quickSort nxtArray left (minus pivotPos 1)
        else ([], nxtArray)
    (finalRes, finalNxtArray) =
      if minus right pivotPos > 1
        then quickSort partialNxtArray (pivotPos + 1) right
        else ([], partialNxtArray)
    res = [curRes] ++ partialRes ++ finalRes
  in
  (res, finalNxtArray)

main : IO ()
main = do
  arrSize <- getLine
  let intArrSize = toInt arrSize
  arrDat <- getLine
  let divArrDat = map toInt (words arrDat)
  let orderedArr = quickSort divArrDat 0 (minus (length divArrDat) 1)
  let solution = getResponse orderedArr
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  0-132 0-80 0-55 0-17 0-7 1-7 1-5 2-5 2-3 9-17 9-11 10-11 13-17 13-14 16-17
  19-55 19-24 20-24 22-24 23-24 26-55 26-38 26-28 27-28 30-38 30-34 30-31
  33-34 36-38 37-38 40-55 40-42 41-42 44-55 44-45 47-55 47-53 47-52 47-51
  49-51 50-51 57-80 58-80 58-60 62-80 62-68 62-66 63-66 63-64 70-80 70-79
  70-75 70-71 73-75 74-75 77-79 82-132 82-90 82-88 82-87 84-87 85-87 92-132
  93-132 94-132 94-100 94-95 97-100 97-99 102-132 102-112 102-104 103-104
  106-112 106-110 106-107 109-110 114-132 114-118 116-118 120-132 120-131
  120-128 120-124 120-121 123-124 126-128 126-127 130-131
-}
