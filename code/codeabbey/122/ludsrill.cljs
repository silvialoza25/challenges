;; clj-kondo --lint ludsrill.cljs
;; linting took 54ms, errors: 0, warnings: 0

(ns ludsrill.122
    (:gen-class)
    (:require [clojure.string :as str])
    (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn iterator [aux-left aux-right direction array pivot]
  (if (< aux-left aux-right)
    (if (= direction "left")
      (if (> (nth array aux-right) pivot)
        (iterator aux-left (- aux-right 1) direction array pivot)
        (iterator (+ aux-left 1) aux-right "right"
          (assoc array aux-left (nth array aux-right)) pivot))

      (if (< (nth array aux-left) pivot)
        (iterator (+ aux-left 1) aux-right direction array pivot)
        (iterator aux-left (- aux-right 1) "left"
          (assoc array aux-right (nth array aux-left)) pivot)))
    [array aux-left]))

(defn get-partition [array left right]
  (print (core/format "%s-%s " left right))
  (let [aux-left left
        aux-right right
        direction "left"
        pivot (nth array left)
        new-array (iterator aux-left aux-right direction array pivot)]
    [(first new-array) (second new-array)]))

(defn quicksort [array left right]
  (let [[new-array pivot-pos] (get-partition array left right)]
    (when (> (- pivot-pos left) 1)
      (quicksort new-array left (- pivot-pos 1)))

    (when (> (- right pivot-pos) 1)
      (quicksort new-array (+ pivot-pos 1) right))))

(defn -main []
  (let [data (rest (get-data))
        final-data (into [] (map #(Integer. %) (into [] (first data))))]
    (quicksort final-data 0 (- (count final-data) 1))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 0-132 0-106 0-9 0-1 3-9 4-9 4-5 7-9 11-106 11-73 11-66 11-57 11-51 11-41
;; 11-40 11-12 14-40 14-18 16-18 20-40 20-23 20-22 20-21 25-40 25-29 26-29
;; 26-27 31-40 31-32 34-40 34-38 34-37 34-36 35-36 43-51 44-51 45-51 45-46
;; 48-51 49-51 53-57 53-56 53-54 59-66 60-66 60-65 60-64 61-64 62-64 68-73
;; 68-69 71-73 75-106 75-78 76-78 80-106 80-81 83-106 83-92 84-92 84-86 84-85
;; 88-92 88-89 91-92 94-106 94-101 94-99 95-99 96-99 98-99 103-106 103-105
;; 108-132 108-127 108-120 109-120 110-120 110-115 110-114 110-112 110-111
;; 117-120 117-118 122-127 122-123 125-127 129-132 129-131 130-131
