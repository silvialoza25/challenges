/*
$ rustfmt dantivar.rs
$ rustc dantivar.rs
*/
use std::io;

fn print_result(result: Vec<Vec<u32>>) {
  let mut first_term = true;
  for i in result {
    if first_term {
      first_term = false;
    } else {
      let sum: u32 = i.iter().sum();
      print!("{} ", sum);
    }
  }
}

fn lcg(a: u32, c: u32, m: u32, x0: u32, n: u32) -> Vec<u32> {
  let mut i: u32 = 0;
  let mut result: Vec<u32> = vec![];
  let mut x: u32 = x0;
  loop {
    if i == n * 4 {
      break;
    } else {
      x = ((a * x) + c) % m;
      result.push(x % n + 1);
      i += 1;
    }
  }

  result
}

fn graph_generator(n: u32, x0: u32) -> Vec<Vec<u32>> {
  let mut result = vec![vec![0; (n + 1) as usize]; (n + 1) as usize];
  let pairs: Vec<u32> = lcg(445, 700001, 2097152, x0, n);
  let mut i: u32 = 1;
  let mut j: u32 = 0;

  loop {
    if i >= (n + 1) {
      break;
    } else {
      let v1: u32 = pairs[j as usize];
      let d1: u32 = pairs[(j + 1) as usize];
      let v2: u32 = pairs[(j + 2) as usize];
      let d2: u32 = pairs[(j + 3) as usize];

      if result[i as usize][v1 as usize] == 0 && i != v1 {
        result[i as usize][v1 as usize] = d1;
        result[v1 as usize][i as usize] = d1;
      }
      if result[i as usize][v2 as usize] == 0 && i != v2 {
        result[i as usize][v2 as usize] = d2;
        result[v2 as usize][i as usize] = d2;
      }
      j += 4;
      i += 1;
    }
  }
  result
}

fn main() {
  let stdin = io::stdin();

  let mut input = String::new();
  stdin.read_line(&mut input).expect("Error");

  let input: Vec<&str> = input.split_whitespace().collect();

  let n: u32 = input[0].parse().unwrap_or(0);
  let x0: u32 = input[1].parse().unwrap_or(0);

  print_result(graph_generator(n, x0));
}

/*
$ cat DATA.lst | ./dantivar
41 35 20 43 16 60 19 25 40 53 32 34 12 37 36 27 31 1 16
*/
