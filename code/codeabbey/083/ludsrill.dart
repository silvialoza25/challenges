// dartanalyzer ludsrill.dart
// Analyzing ludsrill.dart...
// No issues found!

import 'dart:core';
import 'dart:io';

void main() {
  List<int> input = getData([]);
  int constantA = 445;
  int constantC = 700001;
  int module = 2097152;
  int seed = input[1];
  int vertex = input[0];
  List<int> randomsValues =
      randomGenerator(constantA, constantC, module, seed, [], vertex);
  List<List<int>> randomGraph =
      divideList(toVertex(randomsValues, vertex), 0, [], []);
  Map<int, List<List<int>>> hashMap = makeHashMap(vertex, {}, 1);
  List<List<int>> finalHashMap =
      graphGenerator(hashMap, randomGraph.last, randomGraph.first, 1, 0)
          .values.map((item) => item.last)
          .toList();
  List<int> solution = finalHashMap
      .map((item) => item.reduce((value, element) => value + element))
      .toList();
  print(solution.join(" "));
}

Map<int, List<List<int>>> graphGenerator(Map<int, List<List<int>>> map,
    List<int> weight, List<int> vertex, int startVertex, int count) {
  if (weight.isEmpty) {
    return map;
  } else if (count < 2) {
    Map<int, List<List<int>>> auxMap =
        modifyMap(map, weight.first, vertex.first, startVertex);
    return graphGenerator(
        auxMap, weight.sublist(1), vertex.sublist(1), startVertex, count  + 1);
  } else {
    return graphGenerator(map, weight, vertex, startVertex + 1, 0);
  }
}

Map<int, List<List<int>>> modifyMap(
      Map<int, List<List<int>>> map, int weight, int vertex, int startVertex) {
  if (map[vertex].first.contains(startVertex) || vertex == startVertex) {
    return map;
  } else {
    map.update(vertex, (value) =>
        [value.first + [startVertex], value.last + [weight]]);
    map.update(startVertex, (value) =>
        [value.first + [vertex], value.last + [weight]]);
    return map;
  }
}

List<int> getData(List<int>input) {
  String getLine = stdin.readLineSync();
  if (getLine == null) {
    return input;
  } else {
    List<String> newList = getLine.split(" ").toList();
    return getData(input + newList.map((item) => int.parse(item)).toList());
  }
}

List<List<int>> divideList(
    List<int> list, int counter, List<int> accVertex, List<int> accWeight) {
  if (counter < list.length) {
    if (counter % 2 == 0) {
      return divideList(
          list, counter + 1, accVertex + [list[counter]], accWeight);
    } else {
      return divideList(
          list, counter + 1, accVertex, accWeight + [list[counter]]);
    }
  } else {
    return [accVertex, accWeight];
  }
}

List<int> randomGenerator(int constantA, int constantC, int module, int random,
    List<int> acc, int vertex) {
  int aux = (constantA * random + constantC) % module;
  if (acc.length <= 4 * vertex - 1) {
    return randomGenerator(
        constantA, constantC, module, aux, acc + [aux], vertex);
  } else {
    return acc;
  }
}

List<int> toVertex(List<int> randoms, int vertex) {
  return randoms.map((item) => item % vertex + 1).toList();
}

Map<int, List<List<int>>> makeHashMap(
      int vertex, Map<int, List<List<int>>> map, int counter) {
  if (counter <= vertex) {
  map.update(counter, (value) => [[],[]], ifAbsent: () => [[],[]]);
  return makeHashMap(vertex, map, counter + 1);
  } else {
    return map;
  }
}

// cat DATA.lst | dart ludarill.dart
// 27 13 28 17 15 19 6 5 7 25 12
