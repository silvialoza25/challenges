#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun calc-rand-numbers(N X0)
  (let ((x-cur X0) (x-next NIL)(i 1) (res '())
  (A 445) (C 700001) (M 2097152) (x-fixed NIL)
  (ant-x-fixed NIL))
    (loop
      (setq x-next (mod (+ (* A x-cur) C) M))
      (setq x-cur x-next)
      (setq x-fixed (+ (mod x-cur N) 1))
      (setq ant-x-fixed x-fixed)
      (setq i (+ i 1))
      (setq res (nconc res (list x-fixed)))
      (when (> i (* N 4))(return res)))))

(defun fill-board (board rand-numbers)
  (let ((i 0) (j 0) (V1 NIL) (D1 NIL) (V2 NIL) (D2 NIL))
    (loop
      (if (and (not (= i 0))(= (mod (+ i 1) 4) 0))
        (progn
          (setq V1 (nth (- i 3) rand-numbers))
          (setq D1 (nth (- i 2) rand-numbers))
          (setq V2 (nth (- i 1) rand-numbers))
          (setq D2 (nth i rand-numbers))
          (if (and (not (= (+ j 1) V1))
          (= (aref board j (- V1 1)) 0))
            (progn
              (setf (aref board j (- V1 1)) D1)
              (setf (aref board (- V1 1) j) D1)))
          (if (and (not (= (+ j 1) V2))
          (= (aref board j (- V2 1)) 0))
            (progn
              (setf (aref board j (- V2 1)) D2)
              (setf (aref board (- V2 1) j) D2)))
          (setq j (+ j 1))))
      (setq i (+ i 1))
      (when (> i (- (length rand-numbers) 1))(return board)))))

(defun print-solution(board N)
  (let ((i 0) (j 0) (sum 0))
    (loop
      (loop
        (setq sum (+ sum (aref board i j)))
        (setq j (+ j 1))
        (when (> j (- N 1))(return NIL)))
      (format t "~a " sum)
      (setq sum 0)
      (setq j 0)
      (setq i (+ i 1))
      (when (> i (- N 1))(return NIL)))))

(defun main ()
  (let ((dimension-list NIL)(board NIL) (N (read))
  (rand-numbers NIL) (solution NIL))
    (setq dimension-list (list N N))
    (setq board (make-array dimension-list :initial-element 0))
    (setq rand-numbers (calc-rand-numbers N (read)))
    (fill-board board rand-numbers)
    (print-solution board N)))

(main)

#|
  cat DATA.lst | clisp bridamo98.lsp
  41 35 20 43 16 60 19 25 40 53 32 34 12 37 36 27 31 1 16
|#
