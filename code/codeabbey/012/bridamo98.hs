{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let intInputData = map convert inputData
  let secsInAMin = 60
  let secsInAHour = secsInAMin * secsInAMin
  let secsInADay = secsInAHour * 24
  let solution = calcDiff intInputData secsInAMin secsInAHour secsInADay
  putStrLn (unwords solution)

calcDiff :: [[Int]] -> Int -> Int -> Int -> [String]
calcDiff [] secsInAMin secsInAHour secsInADay = []
calcDiff dates secsInAMin secsInAHour secsInADay = res
  where
    datesInf = head dates
    day1 = head datesInf
    hour1 = datesInf !! 1
    min1 = datesInf !! 2
    sec1 = datesInf !! 3
    day2 = datesInf !! 4
    hour2 = datesInf !! 5
    min2 = datesInf !! 6
    sec2 = datesInf !! 7
    fstTimestamp = calcSegAmount day1 hour1 min1 sec1
                   secsInAMin secsInAHour secsInADay
    sndTimestamp = calcSegAmount day2 hour2 min2 sec2
                   secsInAMin secsInAHour secsInADay
    timestampDiff = sndTimestamp - fstTimestamp
    diff = calcDateRepre timestampDiff secsInAMin secsInAHour secsInADay
    res = diff : calcDiff (drop 1 dates) secsInAMin secsInAHour secsInADay

calcDateRepre :: Int -> Int -> Int -> Int -> String
calcDateRepre timestamp secsInAMin secsInAHour secsInADay = res
  where
    day = timestamp `quot` secsInADay
    remDays = timestamp `rem` secsInADay
    hour = remDays `quot` secsInAHour
    remHours = timestamp `rem` secsInAHour
    min = remHours `quot` secsInAMin
    sec = remHours `rem` secsInAMin
    res = "(" ++ show day ++ " " ++  show hour ++
          " " ++ show min ++ " " ++ show sec ++ ")"

calcSegAmount :: Int -> Int -> Int -> Int -> Int -> Int -> Int -> Int
calcSegAmount day hour min sec secsInAMin secsInAHour secsInADay = total
  where
    total = sec + min * secsInAMin + hour * secsInAHour + day * secsInADay

convert :: String -> [Int]
convert dat = result
  where
    aux = words dat
    result = map read aux :: [Int]

{-
$ cat DATA.lst | ./bridamo98
  (8 19 11 42) (17 12 30 56) (4 0 25 32) (7 8 32 35) (7 17 11 14)
  (17 6 16 21) (13 8 10 0) (3 19 59 37) (16 2 38 6) (7 2 45 21)
-}
