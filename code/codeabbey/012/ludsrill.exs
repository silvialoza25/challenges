# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.3 seconds (0.1s to load, 0.2s running 55 checks on 1 file)
# 4 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule ModuleAndTime do
  def main do
    all_data = IO.read(:stdio, :all)
    try do
      split_data = String.split(all_data, "\n")
      List.delete_at(split_data, 1 +
        String.to_integer(Enum.at(split_data, 0)))
        |> Enum.map(&String.split(&1, " "))
        |> Enum.map(&to_int/1)
        |> tl()
        |> Enum.map(&modulo_and_time/1)
        |> Enum.map(fn [day, hour, minute, second] ->
            IO.write("(#{day} #{hour} #{minute} #{second}) ") end)
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def modulo_and_time([d1, h1, m1, s1, d2, h2, m2, s2]) do
    total_seconds = (d2 * 86_400 + h2 * 3600 + m2 * 60 + s2) -
                      (d1 * 86_400 + h1 * 3600 + m1 * 60 + s1)
    days = Kernel.floor(total_seconds / 86_400)
    rem_days = rem(total_seconds, 86_400)
    hours = Kernel.floor(rem_days / 3600)
    rem_hours = rem(rem_days, 3600)
    minutes = Kernel.floor(rem_hours / 60)
    seconds = rem(rem_hours, 60)
    [days, hours, minutes, seconds]
  end
end

ModuleAndTime.main()

# cat DATA.lst | elixir ludsrill.exs
# (4 8 47 57) (9 10 7 7) (2 4 40 46) (4 5 41 12) (2 17 38 33) (13 11 55 45)
# (28 21 5 27) (21 18 57 2) (3 17 20 34) (24 21 53 48) (6 14 36 0) (13 7 17 44)
# (4 18 52 41) (6 2 12 54) (26 8 44 29)
