{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.Map (fromListWith, toList)
import Data.Char
import Data.List
import Control.Monad

main = do
  inputData <- getLine
  let greatDiff = 99999999
  let formatedData = sortBy sortGT (countRepetitions inputData)
  let shannonCodes = findShannonRepre formatedData greatDiff ""
                     (replicate (length formatedData) "")
  let asciiCodes = map (show . ord . fst) formatedData
  let solution = mergeLists asciiCodes shannonCodes
  putStrLn (unwords solution)

sortGT (a1, b1) (a2, b2)
  | b1 < b2 = GT
  | b1 > b2 = LT
  | b1 == b2 = compare a1 a2

countRepetitions :: (Ord a) => [a] -> [(a, Int)]
countRepetitions repe = toList (fromListWith (+) [(item, 1) | item <- repe])

findShannonRepre :: [(Char, Int)] -> Int -> String -> [String] -> [String]
findShannonRepre array greatDiff curCode curRes = res
  where
    midIndex = findMidIndex (map snd array) (length array - 1) 0 greatDiff
    nxtCurRes = map (++curCode) curRes
    res = if length array == 1
            then [head nxtCurRes]
            else findShannonRepre (take midIndex array)
                 greatDiff "O" (take midIndex nxtCurRes) ++
                 findShannonRepre (drop midIndex array)
                 greatDiff "I" (drop midIndex nxtCurRes)


findMidIndex :: [Int] -> Int -> Int -> Int -> Int
findMidIndex array curIndex bestIndex bestDiff = res
  where
    realCurIndex = length array - 1 - curIndex
    fstSumPart = sum (take (realCurIndex + 1) array)
    sndSumPart = sum (drop (realCurIndex + 1) array)
    curDiff = abs (fstSumPart - sndSumPart)
    nxtBestDiff = if curDiff < bestDiff
                    then curDiff
                    else bestDiff
    nxtBestIndex = if curDiff < bestDiff
                     then realCurIndex
                     else bestIndex
    res = if curIndex == 0
            then nxtBestIndex + 1
            else findMidIndex array (curIndex - 1) nxtBestIndex nxtBestDiff

mergeLists :: [a] -> [a] -> [a]
mergeLists xs     []     = xs
mergeLists []     ys     = ys
mergeLists (x:xs) (y:ys) = x : y : mergeLists xs ys

{-
$ cat DATA.lst | ./bridamo98
  32 OOO 101 OOI 111 OIOO 110 OIOI 105 OIIO 97 OIII 104 IOOO 114 IOOI
  115 IOIO 116 IOIIO 100 IOIII 108 IIOOO 103 IIOOI 99 IIOIO 121 IIOIIO
  119 IIOIII 109 IIIOOO 44 IIIOOI 117 IIIOIO 34 IIIOIIO 102 IIIOIII 112
  IIIIOOO 73 IIIIOOI 98 IIIIOIO 107 IIIIOII 118 IIIIIOO 45 IIIIIOIO 46
  IIIIIOIIO 63 IIIIIOIII 66 IIIIIIOO 76 IIIIIIOI 83 IIIIIIIO 87 IIIIIIIIO
  120 IIIIIIIII
-}
