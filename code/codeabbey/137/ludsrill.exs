# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.01s to load, 0.08s running 55 checks on 1 file)
# 6 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule ShannonFanoCoding do
  def main do
    all_data = IO.read(:stdio, :all)
    try do
      char_list = get_data(all_data)
      hashmap = make_hashmap(char_list)
      sorted_list = Enum.sort(hashmap,
        &(hd(Tuple.to_list(&1)) <= hd(Tuple.to_list(&2))))
      |> Enum.sort(&(Enum.at(Tuple.to_list(&1), 1) >=
        Enum.at(Tuple.to_list(&2), 1)))
      |> Enum.map(fn item -> hd(Tuple.to_list(item)) end)

      shannon_fano(sorted_list, "", hashmap)
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def shannon_fano(list, base, map) do
    if length(list) == 1 do
      IO.write("#{hd(list)} #{base} ")
    else
      aux = split_list(list, 0, 1_000_000, 1, map)
      shannon_fano(Enum.slice(list, 0..aux - 1), base <> "O", map)
      shannon_fano(Enum.slice(list, aux..length(list) + 1), base <> "I", map)
    end
  end

  def split_list(list, best_slice, current, count, map) do
    if count < length(list)  do
      list_left = Enum.slice(list, 0..count - 1)
      list_right = Enum.slice(list, count..length(list))
      sum_left = get_addition(list_left, map)
      sum_right = get_addition(list_right, map)
      diff = sum_left - sum_right
      if abs(diff) < current do
        split_list(list, count, abs(diff), count + 1, map)
      else
        split_list(list, best_slice, current, count + 1, map)
      end
    else
      best_slice
    end
  end

  def get_addition(list, map) do
    Enum.reduce(list, 0, fn(item, acc) -> acc + Map.get(map, item) end)
  end

  def make_hashmap(list) do
    Enum.reduce(list, %{}, fn item, acc ->
      Map.update(acc, item, 1, &(&1 + 1)) end)
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
    |> Enum.reject(fn item -> item == "" end)
    |> List.flatten()
    |> hd()
    |> String.to_charlist()
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end
end

ShannonFanoCoding.main()

# cat DATA.lst | elixir ludsrill.exs
# 32 OOO 101 OOI 97 OIOO 116 OIOI 104 OIIO 111 OIII 114 IOOO 110 IOOIO
# 115 IOOII 119 IOIO 100 IOIIO 105 IOIII 108 IIOOO 99 IIOOI 112 IIOIO
# 117 IIOIIO 102 IIOIII 109 IIIOOO 121 IIIOOI 73 IIIOIO 98 IIIOII 44 IIIIOO
# 45 IIIIOIO 103 IIIIOII 34 IIIIIOO 46 IIIIIOIO 66 IIIIIOII 87 IIIIIIOO
# 107 IIIIIIOI 113 IIIIIIIO 118 IIIIIIII
