# elvish -compileonly alejotru3012.elv

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn gcd_const [s_prev s_cur t_prev t_cur var_x var_y]{
  var_r = (% $var_x $var_y)
  if (== $var_r 0) {
    put [$var_y $s_cur $t_cur]
  } else {
    var_q = (math:trunc (/ $var_x $var_y))
    s_next = (- $s_prev (* $var_q $s_cur))
    t_next = (- $t_prev (* $var_q $t_cur))
    gcd_const $s_cur $s_next $t_cur $t_next $var_y $var_r
  }
}

fn gcd [var_x var_y]{
  const = (gcd_const 1 0 0 1 $var_x $var_y)
  echo $@const
}

fn main [data]{
  each [values]{
    echo (gcd $@values)
  } $data
}

main (input_data)

# cat DATA.lst | elvish alejotru3012.elv
# 1 14015 -9293
# 2 3466 -10021
# 2 8211 -8108
# 1 11422 -1313
# 1 1770 -1027
# 1 15418 -22907
# 1 -20896 12459
# 1 -9907 14468
# 38 -646 543
# 2 8155 -10692
# 1 -3909 11341
# 2 11191 -7741
# 160 -83 45
# 1 17413 -18812
# 2 2543 -13817
# 7 494 -763
# 1 -16901 5157
# 1 -5894 5937
# 1 -7177 24483
# 1 26369 -5927
# 1 -35457 32872
# 1 -5913 8831
# 6 -4851 5680
# 2 22534 -17713
# 1 -20848 38625
# 2 803 -505
# 1 22523 -13693
# 7 -634 687
