#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun solve-single-problem(
  i x-array y-array s-prev s-cur t-prev t-cur q r aux x-initial y-initial)
  (setq s-prev 1)
  (setq s-cur 0)
  (setq t-prev 0)
  (setq t-cur 1)
  (setq x-initial (aref x-array i))
  (setq y-initial (aref y-array i))
  (loop
    (if (> (aref x-array i) (aref y-array i))
      (progn
        (setq q (floor (aref x-array i) (aref y-array i)))
        (setq r (mod (aref x-array i) (aref y-array i)))
        (setf (aref x-array i) (aref y-array i))
        (setf (aref y-array i) r)
      )
      (progn
        (setq q (floor (aref y-array i) (aref x-array i)))
        (setq r (mod (aref y-array i) (aref x-array i)))
        (setf (aref y-array i) (aref x-array i))
        (setf (aref x-array i) r)
      )
    )
    (if (not(= r 0))
      (progn
        (setq aux s-cur)
        (setq s-cur (- s-prev (* q s-cur)))
        (setq s-prev aux)

        (setq aux t-cur)
        (setq t-cur (- t-prev (* q t-cur)))
        (setq t-prev aux)
      )
      (progn
        (if (> x-initial y-initial)
          (progn
            (setq aux (+ (* s-cur x-initial)(* t-cur y-initial)))
            (format t "~a ~a ~a~%" aux s-cur t-cur)
          )
          (progn
            (setq aux (+ (* t-cur x-initial)(* s-cur y-initial)))
            (format t "~a ~a ~a~%" aux t-cur s-cur)
          )
        )
      )
    )
    (when (= r 0) (return r))
  )
)

(defun solve-all(size-input x-array y-array s-prev s-cur t-prev t-cur q r
  aux x-initial y-initial)
  (loop for i from 0 to (- size-input 1)
    do(solve-single-problem
      i x-array y-array s-prev s-cur t-prev
      t-cur q r aux x-initial y-initial)
  )
)

(defun get-input-line(x number x-array y-array)
  (setq number (read))
  (setf (aref x-array x) number)
  (setq number (read))
  (setf (aref y-array x) number)
)

(defvar size-input (read))
(defparameter x-array (make-array size-input))
(defparameter y-array (make-array size-input))
(defvar number 0)
(defvar s-prev nil)
(defvar s-cur nil)
(defvar t-prev nil)
(defvar t-cur nil)
(defvar q nil)
(defvar r nil)
(defvar aux nil)
(defvar x-initial nil)
(defvar y-initial nil)

(loop for x from 0 to (- size-input 1)
  do(get-input-line x number x-array y-array)
)

(solve-all size-input x-array y-array s-prev s-cur
  t-prev t-cur q r aux x-initial y-initial)

#|
  cat DATA.lst | clisp bridamo98.lsp
  3 -1383 941
  1 -21296 12741
  5 905 -808
  2 5935 -2641
  1 -24691 21609
  1 -18862 4499
  6 -4782 2155
  1 42207 -41467
  1 16485 -21523
  30 -1307 1066
  3 3727 -5321
  232 -41 114
  1 -19079 23388
  1 1118 -1853
  4 -103 265
  1 -1908 1249
  1 -44032 40517
  1 42723 -25244
  2 -7811 1749
  5 3217 -3025
  1 -22833 12377
  32 -472 259
  4 2488 -5211
  1 -17068 22667
|#
