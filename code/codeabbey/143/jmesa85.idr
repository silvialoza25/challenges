{-
  $ idris2 jmesa85.idr -o jmesa85
  $
-}

-- CodeAbbey #143: Extended Euclidean Algorithm - Idris2

module Main
import Data.List
import Data.Strings

-- Helps to get rid of the Maybe
strToInt : String -> Int
strToInt s =
  case parseInteger {a=Int} s of
    Nothing => 0
    Just j  => j

-- Results in tuples (r, a, b)
euclidean :Int -> Int -> Int -> Int -> Int -> Int -> (Int, Int, Int)
euclidean xCur yCur sPrev sCur tPrev tCur =
  if (mod xCur yCur) == 0 then (yCur, sCur, tCur)
    else
      let
        quotient = div xCur yCur
        sNext = sPrev - quotient * sCur
        tNext = tPrev - quotient * tCur
      in
      euclidean yCur (mod xCur yCur) sCur sNext tCur tNext

-- Creates a tuple (x, y) from the test case
partial formatTestCase : String -> (Int, Int)
formatTestCase info =
  let
    [xCur, yCur] = words info
    xPos = strToInt xCur
    yPos = strToInt yCur
  in
  (xPos, yPos)

-- Formats a tuple (r, a, b) for Stdout
formatResult : (Int, Int, Int) -> String
formatResult (yCur, sCur, tCur) =
  (show yCur) ++ " " ++ show (sCur) ++ " " ++ show (tCur)

-- Gets lines in the Stdin
getLines : IO (List String)
getLines = do
  x <- getLine
  if x == "" then pure []
    else do
      xs <- getLines
      pure (x :: xs)

-- Point of entry
partial main : IO ()
main = do
  -- Read STDIN
  inputData <- getLines
  let
    -- Parse test cases to "List (x, y)"
    testCases = map formatTestCase (drop 1 inputData)
    -- Process test cases
    results = map (\(xCur, yCur) => euclidean xCur yCur 1 0 0 1) testCases
  -- Print results
  putStrLn (unlines (map formatResult results))

{-
  $ idris2 jmesa85.idr -o jmesa85 && cat DATA.lst | ./build/exec/jmesa85
  1 29915 -27206
  2 -15162 10819
  1 -14842 17933
  2 -9004 7663
  1 -13531 29454
  1 7237 -21723
  1 21739 -23776
  1 -16838 2717
  1 -32798 42091
  2 -1430 6429
  1 -19972 21315
  1 14745 -3173
  1 -18138 4331
  1 42315 -32258
  5 3058 -1875
  2 -13087 12024
  1 9617 -8761
  1 -20640 19207
  1 -11261 21499
  1 -7435 11087
  1 18161 -9553
  1 13119 -10537
  3 -5554 11921
  1 29431 -6989
  29 -852 851
  1 -15183 19150
  1 -14426 10209
  7 -877 409
  1 -46275 33769
  2 10437 -7261
-}
