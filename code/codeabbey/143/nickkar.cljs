;; $clj-kondo --lint nickkar.cljs
;; linting took 74ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(defn read-input ([n] (read-input n 0 []))
  ([n i out]
    (cond
      (>= i n) out
      :else (read-input n (inc i) (conj out [(core/read) (core/read)])))))

(defn xtnd-GCD [a b]
  (cond
    (= b 0) [a 1 0]
    :else
    (let [[g x y] (xtnd-GCD b (mod a b))]
      [g y (- x (* y (quot a b)))])))

(defn solve-cases ([n numbers] (solve-cases n numbers 0 []))
  ([n numbers i out]
    (cond
      (>= i n) out
      :else
      (let [[a b] (nth numbers i)]
        (solve-cases n numbers (inc i) (conj out (xtnd-GCD a b)))))))

(defn main []
  (let [n (core/read)
        num (read-input n)]
    (apply println (flatten (solve-cases n num)))))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 3 -1383 941 1 -21296 12741 5 905 -808 2 5935 -2641 1 -24691 21609 1 -18862
;; 4499 6 -4782 2155 1 42207 -41467 1 16485 -21523 30 -1307 1066 3 3727 -5321
;; 232 -41 114 1 -19079 23388 1 1118 -1853 4 -103 265 1 -1908 1249 1 -44032
;; 40517 1 42723 -25244 2 -7811 1749 5 3217 -3025 1 -22833 12377 32 -472 259 4
;; 2488 -5211 1 -17068 22667
