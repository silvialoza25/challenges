# elvish -compileonly kjcamargo.elv

use str
use math

#This function read all values from DATA.lst file
fn read_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

#Function to determine a and b integers using Euclidean algorithm
fn euclidean_integer [const_x const_y Sprev Scur Tprev Tcur]{

  if (== $const_y 0) {
    echo $const_x $Sprev $Tprev

  } else {
    var const_r = (% $const_x $const_y)
    var const_q = (math:floor (/ $const_x $const_y))
    var Snext = (- $Sprev (* $const_q $Scur))
    var Tnext = (- $Tprev (* $const_q $Tcur))
    set Sprev = $Scur
    set Scur = $Snext
    set Tprev = $Tcur
    set Tcur = $Tnext
    euclidean_integer $const_y $const_r $Sprev $Scur $Tprev $Tcur
  }
}

#This function apply to every test-case the Euclidean Algorithm
fn evaluate_euclideanAlg [data]{
  each [val]{
    var const_x = $val[0]
    var const_y = $val[1]
    var Sprev = 1
    var Scur = 0
    var Tprev = 0
    var Tcur = 1
    euclidean_integer $const_x $const_y $Sprev $Scur $Tprev $Tcur
  } $data
}

#Output Value
echo &sep="\n" (evaluate_euclideanAlg (read_data))

# cat DATA.lst | elvish kjcamargo19.elv
# 3 -7894 9353
# 1 -2215 3086
# 1 24027 -17917
# 2 4677 -3592
# 1 1648 -1301
# 1 214 -169
# 1 -27237 4969
# 1 -37027 31398
# 39 -97 87
# 3 -11736 11213
# 2 -9001 12490
# 2 -5322 5111
# 2 6703 -1419
# 3 -4833 2452
# 12 99 -164
# 1 9649 -13363
# 3 -6047 5902
# 1 -12133 17011
# 1 -19829 14312
# 1 -12493 5348
# 1 -37213 21321
# 26 458 -1397
# 2 -8498 1083
# 3 -904 1033
# 1 5713 -4647
# 1 8372 -20625
# 15 1439 -2949
# 1 20285 -5143
# 68 -3 4
# 2 -2786 4415
