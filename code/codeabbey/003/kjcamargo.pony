// ponyc -b kjcamargo19

use "collections"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun iterator(data: Array[String]): Array[String] =>
          data.slice(1, data.size())

        fun sum(const_a: U32, const_b: U32): U32 =>
          const_a + const_b

        fun obtain_data(data: Array[String])? =>
          if data.size() > 0 then
            var line: Array[String] = data(0)?.split_by(" ")
            var const_a: U32 = line(0)?.u32()?
            var const_b: U32 = line(1)?.u32()?

            env.out.write(sum(const_a,const_b).string() + " ")
            obtain_data( iterator (data))?
          end

        fun ref apply(data: Array[U8] iso) =>
          var input_data: String = String.from_iso_array(consume data)
          var line_sep: Array[String] = input_data.split_by("\n")
          var real_data: Array[String] = line_sep.slice(1, line_sep.size())

          try
            obtain_data(real_data)?
          end

        fun ref dispose() =>
          env.out.write("")
      end,
      512
    )
// cat DATA.lst | ./kjcamargo19
// 1222739 964160 486885 1159836 923034 595364
// 783399 300009 429242 605965 1000212 1519825 309536
