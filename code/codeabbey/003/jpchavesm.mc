/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, char, int.

:- pred sumsInLoop(list(string)::in,string::in,string::out) is det.
sumsInLoop([],PartialSum,PartialSum).
sumsInLoop([Line | Tail],PartialSum,Sums) :-
  (
    string.words_separator(char.is_whitespace,Line) = Input,
    det_to_int(det_index0(Input,0)) = X,
    det_to_int(det_index0(Input,1)) = Y,
    X + Y = SumXY,
    PartialSum1 = PartialSum ++ " " ++ from_int(SumXY),
    sumsInLoop(Tail,PartialSum1,Sums)
  ).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
                   list(string)::in,io::di,io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    list.det_drop(1,FileContents,SumList),
    sumsInLoop(SumList,"",SumStr),
    io.print_line(SumStr,!IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  1038592 1073423 618423 984034 1297781 673835 1338386 991353 1681649 423793
  671634 988292 1285590 1064744 1193443
*/
