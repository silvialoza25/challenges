# mix credo --strict --files-included jmesa85.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.03 seconds (0.02s to load, 0.01s running 55 checks on 1 file)
# 4 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

# CodeAbbey #003: Sums in Loop - Elixir

defmodule SumsInLoop do

  def main do
    all_data = IO.read(:stdio, :all)
    try do
      get_data(all_data)
        |> Enum.map(&sum(&1))
        |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Format error")
    end
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
    |> Enum.reject(fn item -> item == "" end)
    |> Enum.map(&String.split(&1, " "))
    |> tl()
  end

  def sum([a, b]) do
    String.to_integer(a) + String.to_integer(b)
  end
end

SumsInLoop.main

# cat DATA.lst | elixir jmesa85.exs
# 1038592 1073423 618423 984034 1297781 673835 1338386 991353 1681649 423793
# 671634 988292 1285590 1064744 1193443
