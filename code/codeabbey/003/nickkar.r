# $ lintr::lint('nickkar.r')

options(digits = 12)

sumpairs <- function(cases, out = c(), i=1) {
    if (i > length(cases)) {
        return(out)
    }
    else {
        tmp <- unlist(strsplit(cases[i], " "))
        num1 <- as.double(tmp[1])
        num2 <- as.double(tmp[2])
        sumpairs(cases, (append(out, (num1 + num2))), i + 1)
    }
}

main <- function() {
    data <- readLines("stdin")
    out <- sumpairs(data[2:length(data)])
    print(out)
}

main()

# $ cat DATA.lst | rscript nickkar.r
# [1]   77386  440366 1038645  785728  809727  792139 1001087  708771  676606
# [10] 1286978 1423888
