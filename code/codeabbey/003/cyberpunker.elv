# elvish -compileonly cyberpunker.elv

#!/bin/elvish

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn loop [data]{
  each [number]{
    result = (+ $number[0] $number[1])
    echo $result
  } $data
}

echo (loop (input_data))

# cat DATA.lst | elvish cyberpunker.elv
# 677883 549784 912137 332299 531744 924389 198375 461928
# 1261111 799502 928788 1000189 1643556 1232552 809280
