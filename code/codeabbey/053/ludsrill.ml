(*
$ ocp-lint --severity-limit 3
  Scanning files in project "."...
  Found '3' file(s)
  Running analyses... 3 / 3
  Merging database...
  == New Warnings ==
    * 0 file(s) were linted
    * 0 warning(s) were emitted:
    * 0 file(s) couldn't be linted
*)

let explode s =
  let rec exp i l =
    if i < 0 then l
    else if s.[i] = ' '
      then exp (i - 1) l
    else exp (i - 1) (s.[i] :: l) in
  exp (String.length s - 1) []

let maybe_read_line () =
  try Some (read_line ())
  with End_of_file -> None

let rec get_data acc =
  match maybe_read_line () with
  | Some (line) -> get_data (acc @ [(explode line)])
  | None -> (List.tl acc)

let validation item =
  match item with
  | a::b::c::d::[] -> if a == c || b == d then Printf.printf "%s " "Y"
                      else if abs (int_of_char b - int_of_char d) ==
                              abs (Char.code c - Char.code a)
                        then Printf.printf "%s " "Y"
                      else Printf.printf "%s " "N"
  | _ -> ()

let main () =
  let data = get_data [] in
    List.iter validation data

let () = main ()

(*
$ cat DATA.lst | ocaml ludsrill.ml
  Y Y N N N Y Y Y Y N N N N Y Y N N Y N N N N N Y Y N N
*)
