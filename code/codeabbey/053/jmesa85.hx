/**
  $ haxelib run checkstyle \
    --exitcode \
    --show-parser-errors \
    --nothreads \
    --default-config \
    -s jmesa85.hx
**/

// CodeAbbey #053: King and Queen - Haxe

using StringTools;

class Main {
  static public function main():Void {
    // Read Stdin
    var data = Sys.stdin().readAll().toString().trim().split("\n");
    // Get the test cases
    var testCases:Array<String> = data.slice(1);
    // Process each test case
    var results:Array<String> = testCases.map(processTestCase);
    // Print results
    trace(results.join(" "));
  }

  static private function processTestCase(testCases:String):String {
    // Format data from moves
    var move = testCases.split(' ');
    var king = move[0];
    var queen = move[1];
    var kCol = king.charAt(0).charCodeAt(0);
    var kRow = Std.parseInt(king.charAt(1));
    var qCol = queen.charAt(0).charCodeAt(0);
    var qRow = Std.parseInt(queen.charAt(1));
    // Calculate
    // If column or row are the same...
    if ((kCol == qCol) || (kRow == qRow))
      return "Y";
    // Check diagonal distance
    if (Math.abs(kCol - qCol) == Math.abs(kRow - qRow))
      return "Y";
    else
      return "N";
  }
}

/**
  $ cp jmesa85.hx Main.hx && cat DATA.lst | haxe -main Main --interp
  N Y N Y N N N N Y N Y Y N N N Y Y Y N N Y N Y N N Y N N N
**/
