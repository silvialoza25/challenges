/*
$ clippy-driver villjorge.rs -D warnings
$ rustc villjorge.rs
*/

fn killer_queen(input: &[char]) {
  let x1 = charpos2numpos(&input[0]);
  let y1 = input[1] as i32;
  let x2 = charpos2numpos(&input[2]);
  let y2 = input[3] as i32;
  if (x1 == x2) || (y1 == y2) || ((x2 - x1).abs() == (y2 - y1).abs()) {
    print!("Y ");
  } else {
    print!("N ");
  }
}

fn charpos2numpos(pos: &char) -> i32 {
  match pos {
    'a' => 1,
    'b' => 2,
    'c' => 3,
    'd' => 4,
    'e' => 5,
    'f' => 6,
    'g' => 7,
    'h' => 8,
    _ => 0,
  }
}

fn main() {
  let inputqty: Vec<u32> = {
    let mut strsizevalue = String::new();
    std::io::stdin().read_line(&mut strsizevalue).unwrap();
    strsizevalue
      .split_whitespace()
      .map(|num| num.trim().parse::<u32>().unwrap())
      .collect()
  };

  for _i in 0..inputqty[0] {
    let inputdata: Vec<char> = {
      let mut strch = String::new();
      std::io::stdin().read_line(&mut strch).unwrap();
      strch.chars().collect()
    };
    let posvec = vec![inputdata[0], inputdata[1], inputdata[3], inputdata[4]];
    killer_queen(&posvec);
  }
  println!();
}

/*
$ cat DATA.lst | ./villjorge
Y N Y N N N N N Y N N Y Y Y N N N N N N N
*/
