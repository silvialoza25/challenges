-- $ curry-verify sprintec22.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `sprintec22'!

import IO
import List
import Read
import Float
import Char

getData :: IO String
getData = do
  line <- getContents
  return line

verification :: Char -> Char -> Char -> Char -> String
verification var_a var_b var_c var_d
  | var_b == var_c || var_a == var_c = "Y"
  | abs ((ord var_d - 48) - (ord var_b - 48)) == abs ((ord var_c - 96) -
    (ord var_a - 96)) = "Y"
  | otherwise = "N"

solution :: String -> String
solution dataIn = result
  where
    var_a = head dataIn
    var_b = dataIn !! 1
    var_c = dataIn !! 3
    var_d = dataIn !! 4
    result = verification var_a var_b var_c var_d

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> solution x) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs -q :load sprintec22.curry :eval main :quit
-- N Y N Y N N N N N N Y Y N N N N Y N N N Y N Y N N Y N N N
