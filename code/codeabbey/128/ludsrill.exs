# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.01s to load, 0.08s running 55 checks on 1 file)
# 6 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule CombinationsCounting do
  def main do
    all_data = IO.read(:stdio, :all)
    try do
      get_data(all_data)
        |> Enum.map(fn item -> combinations(item) end)
        |> Enum.map(&IO.write("#{Kernel.round(&1)} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def combinations([all, groups]) do
    factorial(all, 1) / ((factorial(groups, 1)) * (factorial(all - groups, 1)))
  end

  def factorial(number, accumulator) do
    if number == 0 do
      accumulator
    else
      factorial((number - 1), (number * accumulator))
    end
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
    |> Enum.reject(fn item -> item == "" end)
    |> Enum.map(&String.split(&1, " "))
    |> Enum.map(&to_int/1)
    |> tl()
  end
end

CombinationsCounting.main()

# cat DATA.lst | elixir ludsrill.exs
# 84986896995 18466953120 48124511370 52722315984 101841441273 28987537150
# 13442126049 15820024220
