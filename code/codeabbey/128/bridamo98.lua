--[[
$ luacheck bridamo98.lua
Checking bridamo98.lua                              OK
Total: 0 warnings / 0 errors in 1 file
--]]

local function fact (n)
    if n <= 0 then
      return 1
    else
      return n * fact(n-1)
    end
end

local function calc_combinations (n, k)
    return fact(n) / (fact(k) * fact(n - k))
end

local function solve_all(result)
    local n, k = io.read("*number", "*number")
    if n == nil or k == nil then
      return result
    else
      return solve_all(result  .. calc_combinations(n, k) .. " ")
    end
end

local function main()
    local _ = io.read('*number')
    print(solve_all(""))
end

main()

--[[
$ cat DATA.lst | lua bridamo98.lua
38620298376 29248649430 49594720968 2505433700 13442126049 13442126049
9473622444 46627515440 11969016345 10648873950 46627515440
--]]
