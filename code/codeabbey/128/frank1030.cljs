;$ clj-kondo --lint frank1030.cljs
;linting took 211ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str]))

(defn factorial [n]
  (reduce core/*' (range 1 (inc n))))

(defn get-data [index]
  (let [x (atom index) n (core/read) k (core/read) nk (- n k)
    nf (factorial n) kf (factorial k) nkf (factorial nk)
    c (/ nf (* kf nkf)) res (str/split (str c) #"N")]
    (if (> @x 0)
      (do (swap! x dec)
        (print (nth res 0) "")
        (if (not= @x 0)
          (get-data @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (core/read)]
    (get-data index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;53060358690 11050084695 10639125640 28987537150
;10235867928 35607051480 22760723700 65033528560
