# $ lintr::lint('nickkar.r')

# solves N combination K (C(N, K)) meaning the number of different ways K
# ("k" in code) elements from the a set containing N ("n" in code) elements
# can be arranged without repetitons
# Uses a recursive method to avoid factorial big numbers and within it
# integer overflow
# n and k does not represent anything but mathematical variables
combination <- function(n, k, tmpn= (n - k) + 1, tmpk=1, count=1) {
  if (k == 0) {
    return(1)
  }
  if (tmpn == n) {
    return(count * (tmpn / tmpk))
  }
  else {
    combination(n, k, (tmpn + 1), (tmpk + 1), (count * (tmpn / tmpk)))
  }
}

# iterates over every test-case
solvecases <- function(cases, out = c(), index=1) {
  if (index > length(cases)) {
    return(out)
  }
  else {
    tmp <- unlist(strsplit(cases[index], " "))
    n <- as.double(tmp[1])
    k <- as.double(tmp[2])
    solvecases(cases, (append(out, combination(n, k))), index + 1)
  }
}

# Driver code
main <- function() {
  data <- readLines("stdin")
  out <- solvecases(data[2:length(data)])
  cat(out, "\n")
}

main()

# $ cat DATA.lst | rscript nickkar.r
# 53060358690 11050084695 10639125640 28987537150 10235867928 35607051480
# 22760723700 65033528560
