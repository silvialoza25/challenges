#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun delimiterp (c)
  (char= c #\Space))

(defun split (string &key (delimiterp #'delimiterp))
  (loop
    :for beg = (position-if-not delimiterp string)
    :then (position-if-not delimiterp string :start (1+ end))
    :for end = (and beg (position-if delimiterp string :start beg))
    :when beg :collect (subseq string beg end)
    :while end))

(defun initialize-map(map)
  (setf (gethash 0 map) "C")
  (setf (gethash 1 map) "C#")
  (setf (gethash 2 map) "D")
  (setf (gethash 3 map) "D#")
  (setf (gethash 4 map) "E")
  (setf (gethash 5 map) "F")
  (setf (gethash 6 map) "F#")
  (setf (gethash 7 map) "G")
  (setf (gethash 8 map) "G#")
  (setf (gethash 9 map) "A")
  (setf (gethash 10 map) "A#")
  (setf (gethash 11 map) "B"))

(defun find-diff-notes(notes fst-dist-chord
snd-dist-chord notes-per-octave)
  (let ((fst-note (nth 0 notes)) (snd-note (nth 1 notes))
  (thrd-note (nth 2 notes)) (fst-and-snd-diff NIL)
  (snd-and-thrd-diff NIL) (thrd-and-fst-diff NIL))
    (setq fst-and-snd-diff (- snd-note fst-note))
    (setq snd-and-thrd-diff (- thrd-note snd-note))
    (setq thrd-and-fst-diff (+ (- notes-per-octave thrd-note) fst-note))
    (if (or (and (= fst-and-snd-diff fst-dist-chord)
    (= snd-and-thrd-diff snd-dist-chord))
    (and (= fst-and-snd-diff snd-dist-chord)
    (= snd-and-thrd-diff fst-dist-chord)))
      (return-from find-diff-notes (list fst-and-snd-diff fst-note))
      (if (or (and (= snd-and-thrd-diff fst-dist-chord)
      (= thrd-and-fst-diff snd-dist-chord))
      (and (= snd-and-thrd-diff snd-dist-chord)
      (= thrd-and-fst-diff fst-dist-chord)))
        (return-from find-diff-notes (list snd-and-thrd-diff snd-note))
        (if (or (and (= thrd-and-fst-diff fst-dist-chord)
        (= fst-and-snd-diff snd-dist-chord))
        (and (= thrd-and-fst-diff snd-dist-chord)
        (= fst-and-snd-diff fst-dist-chord)))
          (return-from find-diff-notes (list thrd-and-fst-diff thrd-note))
          (return-from find-diff-notes NIL))))))

(defun find-chord-name (numbers-of-notes notes-map fst-dist-chord
snd-dist-chord notes-per-octave)
  (let ((fixed-notes (sort (remove-duplicates
  (mapcar #'(lambda(x) (mod x notes-per-octave)) numbers-of-notes)) '<))
  (diff-notes NIL))
    (setq diff-notes (find-diff-notes fixed-notes
    fst-dist-chord snd-dist-chord notes-per-octave))
    (if diff-notes
      (return-from find-chord-name (concatenate 'string
      (gethash (nth 1 diff-notes) notes-map)
      (if (= (nth 0 diff-notes) fst-dist-chord) "-minor" "-major")))
      (return-from find-chord-name "other"))))

(defun solve-single-problem (line notes-map
fst-dist-chord snd-dist-chord notes-per-octave)
  (let ((number-list (map 'list #'parse-integer (split line))))
    (format t "~a " (find-chord-name number-list notes-map
    fst-dist-chord snd-dist-chord notes-per-octave))))

(defun solve-all (size-input notes-map
fst-dist-chord snd-dist-chord notes-per-octave)
  (loop for i from 0 to (- size-input 1)
    do(solve-single-problem (read-line) notes-map
    fst-dist-chord snd-dist-chord notes-per-octave)))

(defun main ()
  (let ((size-input (read))
  (notes-map (make-hash-table :test 'equal))
  (fst-dist-chord 3) (snd-dist-chord 4) (notes-per-octave 12))
    (initialize-map notes-map)
    (solve-all size-input notes-map fst-dist-chord
    snd-dist-chord notes-per-octave)))

(main)

#|
  cat DATA.lst | clisp bridamo98.lsp
  A-major A#-major G-minor other other D#-minor C#-minor
  G-major other A#-minor A-minor B-major other other
  C-major A#-minor other F#-major F-minor G#-minor C-minor
  C#-major G#-major F#-major A-major F#-minor B-minor
  G#-major F-minor other G-major
|#
