/**
  $ haxelib run checkstyle \
    --exitcode \
    --show-parser-errors \
    --nothreads \
    --default-config \
    -s jmesa85.hx
**/

// CodeAbbey #173: Chords of Music - Haxe

using StringTools;
using Lambda;

class Main {
  static public function main():Void {
    // Read Stdin
    var data = Sys.stdin().readAll().toString().trim().split("\n");
    // Get the test cases
    var testCases:Array<String> = data.slice(1);
    // Process each test case
    var results:Array<String> = testCases.map(processTestCase);
    // Print results
    trace(results.join(" "));
  }

  static private function processTestCase(testCase:String):String {
    // Parse notes in MIDI notation
    var midiArr = testCase.split(" ").map(Std.parseInt);
    // Parse notes to a single octave
    var notesArr = midiArr.map(function(note) return note % 12);
    // Remove duplicate notes
    var chord = filterUniqueNotes(notesArr, 0);
    return getChordName(chord, 0);
  }

  static private function getChordName(chord:Array<Int>, index:Int):String {
    // Base case
    if (index == chord.length)
      return "other";

    // Prepare some vars for the nexts base cases
    var majorShift = 4; // Half steps from the root for major
    var minorShift = 3; // Half steps from the root for minor
    var fullShift = 7;
    var steps = 12; // Number of half steps in a octave
    var note = chord[index];
    var isRightChord:Bool = chord.indexOf((note + fullShift) % steps) != -1;

    // Base case
    if (chord.indexOf((note + majorShift) % steps) != -1 && isRightChord)
      return getNoteName(note) + "-major";

    // Base case
    if (chord.indexOf((note + minorShift) % steps) != -1 && isRightChord)
      return getNoteName(note) + "-minor";

    // Recursive case
    return getChordName(chord, index + 1);
  }

  // Filters the repeated notes
  static private function filterUniqueNotes(notesArr:Array<Int>,index:Int) {
    // Base case
    if (notesArr.length == 3 || notesArr.length == index)
      return notesArr;

    // Recursive case
    var note = notesArr[index];
    var filtered = notesArr.filter(function(item) return item != note);
    return filterUniqueNotes([note].concat(filtered), index + 1);
  }

  static private function getNoteName(index:Int):String {
    return
      ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'][index];
  }
}

/**
  $ cp jmesa85.hx Main.hx && cat DATA.lst | haxe -main Main --interp
  G#-minor F#-major C#-minor G-minor G-major F-minor G#-minor B-major
  D-major other G#-major D#-minor D#-minor G#-major A#-major B-minor
  other E-minor other A-major F-major other A#-minor A-minor E-minor
  G-minor B-minor F-minor F#-major C#-major C#-major G#-major G#-minor
  F-major D#-minor E-major G#-minor
**/
