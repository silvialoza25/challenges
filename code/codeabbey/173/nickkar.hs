-- $ ghc -o nickkar nickkar.hs
-- [1 of 1] Compiling Main ( nickkar.hs, nickkar.o )
-- Linking code ...
-- $ hlint nickkar.hs
-- No hints

import Control.Monad ( replicateM, forM )

findnote:: Int -> String
findnote midi
    | midi > 11 = "other"
    | midi == 0 = "C"
    | midi == 1 = "C#"
    | midi == 2 = "D"
    | midi == 3 = "D#"
    | midi == 4 = "E"
    | midi == 5 = "F"
    | midi == 6 = "F#"
    | midi == 7 = "G"
    | midi == 8 = "G#"
    | midi == 9 = "A"
    | midi == 10 = "A#"
    | midi == 11 = "B"
    | otherwise = "no sense"

mod12:: Int -> Int
mod12 i = mod i 12

findlead2:: [Int] -> [Int] -> Int -> Int -> String
findlead2 leadlist checklist i j

    --going well
    | leadlist!!j - leadlist!!i == 0 && j /=  length leadlist -1
    = findlead2 leadlist (checklist ++[0]) i (j+1)
    | (leadlist!!j - leadlist!!i == 7 || leadlist!!j - leadlist!!i == -5)
    && j /= length leadlist-1 = findlead2 leadlist (checklist ++[7]) i (j+1)
    | (leadlist!!j - leadlist!!i == 3 || leadlist!!j - leadlist!!i == -9)
    && notElem 4 checklist && j /= length leadlist-1
    = findlead2 leadlist (checklist ++[3]) i (j+1)
    | (leadlist!!j - leadlist!!i == 4 || leadlist!!j - leadlist!!i == -8)
    && notElem 3 checklist && j /= length leadlist-1
    = findlead2 leadlist (checklist ++[4]) i (j+1)

    -- if encountered incompatible
    | (leadlist!!j - leadlist!!i == 3 || leadlist!!j - leadlist!!i == -9)
    && elem 4 checklist && i /= length leadlist -1
    = findlead2 leadlist [] (i+1) 0
    | (leadlist!!j - leadlist!!i == 4 || leadlist!!j - leadlist!!i == -8)
    && elem 3 checklist && i /= length leadlist -1
    = findlead2 leadlist [] (i+1) 0

    | (leadlist!!j - leadlist!!i == 3 || leadlist!!j - leadlist!!i == -9)
    && elem 4 checklist && i == length leadlist -1
    = "other" --impossible value
    | (leadlist!!j - leadlist!!i == 4 || leadlist!!j - leadlist!!i == -8)
    && elem 3 checklist && i == length leadlist -1
    = "other" --impossible value

    | leadlist!!j - leadlist!!i /= -5 && leadlist!!j - leadlist!!i /= -9
    && leadlist!!j - leadlist!!i /= -8 && leadlist!!j - leadlist!!i /= 0
    && leadlist!!j - leadlist!!i /= 3 && leadlist!!j - leadlist!!i /= 4
    && leadlist!!j - leadlist!!i /= 7 && i /= length leadlist -1
    = findlead2 leadlist [] (i+1) 0
    | leadlist!!j - leadlist!!i /= -5 && leadlist!!j - leadlist!!i /= -9
    && leadlist!!j - leadlist!!i /= -8 && leadlist!!j - leadlist!!i /= 0
    && leadlist!!j - leadlist!!i /= 3 && leadlist!!j - leadlist!!i /= 4
    && leadlist!!j - leadlist!!i /= 7 && i == length leadlist -1
    = "other" --imposible value

    --returning values
    | (leadlist!!j - leadlist!!i == 7 || leadlist!!j - leadlist!!i == -5)
    && j == length leadlist -1 && elem 4 checklist
    = findnote (leadlist!!i) ++ "-major"
    | (leadlist!!j - leadlist!!i == 7 || leadlist!!j - leadlist!!i == -5)
    && j == length leadlist -1 && elem 3 checklist
    = findnote (leadlist!!i) ++ "-minor"
    | leadlist!!j - leadlist!!i == 0 && j == length leadlist -1
    && elem 4 checklist = findnote (leadlist!!i) ++ "-major"
    | leadlist!!j - leadlist!!i == 0 && j == length leadlist -1
    && elem 3 checklist = findnote (leadlist!!i) ++ "-minor"
    | (leadlist!!j - leadlist!!i == 3 || leadlist!!j - leadlist!!i == -9)
    && notElem 4 checklist && j == length leadlist -1
    = findnote (leadlist!!i) ++ "-minor"
    | (leadlist!!j - leadlist!!i == 4 || leadlist!!j - leadlist!!i == -8)
    && notElem 3 checklist && j == length leadlist -1
    = findnote (leadlist!!i) ++ "-major"

    | otherwise = "other" -- impossible value


convert::String -> [Int]
convert = map read . words

main::IO()
main = do
    n <- readLn :: IO Int
    inputs <- replicateM n getLine
    ca5es <- forM inputs (return . convert)
    chords <- forM ca5es(return . map mod12)
    lead <- forM chords(\a ->
        return(findlead2 a [] 0 0))
    print lead

-- $ cat DATA.lst | ./nickkar
-- ["F#-major","D-minor","C-minor","D#-minor","F-major","G-minor",
-- "G-minor","A#-minor","C-minor","other","C-major","A#-minor","other",
-- "other","E-major","G#-minor","G#-minor","other","E-minor","G-minor","other",
-- "C-minor","other","other","F#-major","G#-major","A-minor","A-major",
-- "F-major","A-minor","other","other","D#-major","A-minor","C#-major"]
