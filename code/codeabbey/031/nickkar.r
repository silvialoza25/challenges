# $ lintr::lint('nickkar.r')

options(digits = 12)

rotatestr <- function(s, ang) {
  if (ang < 0) {
    tmp <- substring(s, nchar(s) + ang + 1, nchar(s))
    return(paste(tmp, sub(tmp, "", s), sep = ""))
  }
  else {
    tmp <- substring(s, 0, ang)
    return(paste(sub(tmp, "", s), tmp, sep = ""))
  }
}

solvecases <- function(cases, out = c(), i=1) {
  if (i > length(cases)) {
    return(out)
  }
  else {
    tmp <- unlist(strsplit(cases[i], " "))
    ang <- as.double(tmp[1])
    s <- tmp[2]
    solvecases(cases, (append(out, rotatestr(s, ang))), i + 1)
  }
}

main <- function() {
  data <- readLines("stdin")
  out <- solvecases(data[2:length(data)])
  cat(out, "\n")
}

main()

# $ cat DATA.lst | rscript nickkar.r
# orynepxvieojansukaylrfxeu qrfodltahpryuvgqf titzexrgasusgrrnyb
# lhikeizyjrjqroeecwvpoxw xmoyxyiryhcbxuoaau dfeoyysnpbesyppupqihudht
# jugomvlafojmymumqeoaatzsb fnifyjjtdkheejgrpce fyrlxavleytybumoe
# lrrkuaruugallgkurcwgbnmd kuaogeybwnbpberp
