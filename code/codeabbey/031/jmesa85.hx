/**
  $ haxelib run checkstyle \
    --exitcode \
    --show-parser-errors \
    --nothreads \
    --default-config \
    -s jmesa85.hx
**/

// CodeAbbey #031: Rotate String - Haxe

using StringTools;

class Main {
  static public function main():Void {
    // Read Stdin
    var data = Sys.stdin().readAll().toString().trim();
    // Get test cases
    var testCases = data.split("\n").slice(1);
    // Process test cases
    var results = testCases.map(processTestCase);
    // Print results
    trace(results.join(" "));
  }

  static private function processTestCase(testCase:String):String {
    var index = Std.parseInt(testCase.split(" ")[0]);
    var original = testCase.split(" ")[1];
    if (index > 0) {
      var sub1 = original.substring(0, index);
      var sub2 = original.substring(index);
      return sub2 + sub1;
    } else {
      var sub1 = original.substring(0, original.length + index);
      var sub2 = original.substring(original.length + index);
      return sub2 + sub1;
    }
  }
}

/**
  $ cp jmesa85.hx Main.hx && cat DATA.lst | haxe -main Main --interp
  gaocfqyoafczfxki uyrlyrdbgybyytz dekhgulhceicdefufletn
  ojjyexuommgzidi ieopasydwuyxiod oeuxopafieeeyihhuwwej
  bdyvatfoznybxmpa vqzeszhtrtcmvgekbywzytq pmyjlszbdtgvnojiblevj
  yyufpbthrqzpfcirchevb lmwaojbazopieoqawxavd eohvouyajouyyvysqiqnomt
  ryygovpnpffetrr
**/
