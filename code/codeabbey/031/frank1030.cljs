;$ clj-kondo --lint frank1030.cljs
;linting took 288ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn])
  (:require [clojure.pprint :refer [cl-format]]))

(defn rotate-string [a b]
  (let [rotate (split-at a b)]
    (print (cl-format nil "~a~a " (apply str (nth rotate 1))
    (apply str (nth rotate 0))))))

(defn nrotate-string [a b]
  (let [index (- (count b) (* a -1)) rotate (split-at index b)]
    (print (cl-format nil "~a~a " (apply str (nth rotate 1))
    (apply str (nth rotate 0))))))

(defn get-data [index]
  (let [x (atom index) list (core/read-line)
    data (str/split list #" ")]
    (if (> @x 0)
      (do (swap! x dec)
        (if (> (edn/read-string (nth data 0)) 0)
          (rotate-string (edn/read-string (nth data 0)) (nth data 1))
          (nrotate-string (edn/read-string (nth data 0)) (nth data 1)))
        (if (not= @x 0)
          (get-data @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (edn/read-string (core/read-line))]
    (get-data index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;orynepxvieojansukaylrfxeu qrfodltahpryuvgqf titzexrgasusgrrnyb
;lhikeizyjrjqroeecwvpoxw xmoyxyiryhcbxuoaau dfeoyysnpbesyppupqihudht
;jugomvlafojmymumqeoaatzsb fnifyjjtdkheejgrpce fyrlxavleytybumoe
;lrrkuaruugallgkurcwgbnmd kuaogeybwnbpberp
