{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let solution = map rotate inputData
  putStrLn (unwords solution)

rotate :: String -> String
rotate dat = result
  where
    aux = words dat
    rot = read (head aux) :: Int
    sentence = aux !! 1
    case1First = drop (length sentence + rot) sentence
    case1Second = take (length sentence + rot) sentence
    case2First = drop rot sentence
    case2Second = take rot sentence
    result = if rot < 0
               then  case1First ++ case1Second
               else  case2First ++ case2Second

{-
$ cat DATA.lst | ./bridamo98
  orynepxvieojansukaylrfxeu qrfodltahpryuvgqf titzexrgasusgrrnyb
  lhikeizyjrjqroeecwvpoxw xmoyxyiryhcbxuoaau dfeoyysnpbesyppupqihudht
  jugomvlafojmymumqeoaatzsb fnifyjjtdkheejgrpce fyrlxavleytybumoe
  lrrkuaruugallgkurcwgbnmd kuaogeybwnbpberp
-}
