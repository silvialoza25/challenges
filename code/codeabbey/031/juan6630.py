"""""pylint Your code has been rated at 10.00/10
Process finished with exit code 0"""

from typing import List


def rotate() -> List[str]:
    """
    Return an list with the rotated strings
    :return:
    """
    count: int = int(input())
    answer: List[str] = []
    for _ in range(count):
        data: List[str] = input().split()
        k: int = int(data[0])
        my_string: str = data[1]
        if k > 0:
            cut: str = my_string[:k]
            answer.append(my_string[k:] + cut)
        else:
            cut = my_string[(len(my_string) - abs(k)):]
            answer.append(cut + my_string[:len(my_string) - abs(k)])
    return answer


print(rotate())

# $ cat DATA.lst | python3 juan6630.py
# ['cojytsnxnyiocnaxu', 'gyoetrhbleobresgnpioyiolv', 'wcfwihejtbxusuhzj',
# 'fhdhyyaaaasoygaruiheq', 'iabysqyzqjozrljfwvkfa', 'ogluebidbeddwwtesgx',
# 'wycbyiejkhaeviqo', 'hgiovoaonisdeqh', 'zzrmewyvaxgwnfzp',
# 'rquxjlqceddzzsahyyywixlzo', 'ruuevdpfzfreilwofeaiu',
# 'siusooxnlaycoiywuulx']
