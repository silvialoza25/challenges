#!/usr/bin/env fsharpi --exec
(*
$ dotnet fsharplint lint frank1030.fs
========== Linting frank1030.fs ==========
========== Finished: 0 warnings ==========
========== Summary: 0 warnings ==========
*)

module Main

  let getData (x:int) y =
    for i in 0..x-1 do
      let  line = stdin.ReadLine()
      Array.set y i line

  let turn (x:int) (y:string[]) =
    for i in 0..x-1 do
      let  index = y.[i].Substring(0,2).Replace(" ","")|>int
      let  word = y.[i].Substring(2).Replace(" ","")
      if index < 0 then
        let cut = word.Substring(word.Length-(index * -1))
        Array.set y i (cut + word.Replace(cut,""))
      if index > 0 then
        let cut = word.Substring(index)
        Array.set y i (cut + word.Replace(cut,""))

  let main =
    let  nd = stdin.ReadLine()|> int
    let words = Array.zeroCreate nd
    getData nd words
    turn nd words
    let turnList = words|> String.concat " "
    printf "%A" turnList

//$ cat DATA.lst | ./frank1030.fs
(*
plvrapuoswkumeiuiqjvrkyv
ewaiwqycfjcveriuoeayi
xshueqpypmpkugamiawlvtuei
kuyzvogfyalthtteyjegwxo
jbfxlbwfwflztjclo
rnoelaouvvwjyyunh
fxoozbuqglvrhtypefkjr
ynnzocjvoygwmeya
qywvxeqfjurhnmwe
*)
