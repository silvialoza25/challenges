-- $ curry-verify juandiegoe.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `juandiegoe'!

import IO
import List
import Read

getLines :: IO String
getLines = do
  dataLine <- getContents
  return dataLine

parseInt :: String -> Int
parseInt str = read str :: Int

getValues :: [[Prelude.Char]] -> [Prelude.Char]
getValues arr = result
  where
    num = parseInt (arr !! 0)
    strWord = (arr !! 1)
    result = moveStr num strWord

moveStr :: Prelude.Int -> [a] -> [a]
moveStr num str = do
  let revStr = reverse str
  if num > 0
    then ((drop num str) ++ (take num str))
    else reverse ((drop (num * (-1)) revStr) ++ (take (num * (-1)) revStr))

main :: IO ()
main = do
  input <- getLines
  let
    values = split ( == '\n') input
    dropData = drop 1 (init values)
    cleanData = map words dropData
    result = map (\x -> (getValues x)) cleanData
  putStrLn (unwords result)

-- cat DATA.lst | pakcs :load juandiegoe.curry :eval main :quit
-- gaocfqyoafczfxki uyrlyrdbgybyytz dekhgulhceicdefufletn ojjyexuommgzidi
-- ieopasydwuyxiod oeuxopafieeeyihhuwwej bdyvatfoznybxmpa
-- vqzeszhtrtcmvgekbywzytq pmyjlszbdtgvnojiblevj yyufpbthrqzpfcirchevb
-- lmwaojbazopieoqawxavd eohvouyajouyyvysqiqnomt ryygovpnpffetrr
