(*
$ ocp-lint --severity-limit 3
  Scanning files in project "."...
  Found '3' file(s)
  Running analyses... 3 / 3
  Merging database...
  == New Warnings ==
    * 0 file(s) were linted
    * 0 warning(s) were emitted:
    * 0 file(s) couldn't be linted
*)

let splitting line =
  String.split_on_char ' ' line

let maybe_read_line () =
  try Some (read_line ())
  with End_of_file -> None

let rec get_data acc =
  match maybe_read_line () with
  | Some (line) -> get_data (acc @ [(List.map float (List.map int_of_string
                                                      (splitting line)))])
  | None -> (List.tl acc)

let solution triangle =
  match triangle with
  | x::xs::[] ->  if x > xs then Printf.printf "%s " "O"
                  else if x < xs then Printf.printf "%s " "A"
                  else Printf.printf "%s " "R"
  | _ -> ()

let pythagorean_theorem data =
  match data with
  | a::b::c::[] -> solution [c; (sqrt ((a *. a) +. (b *. b)))]
  | _ -> ()

let main () =
  let data = get_data [] in
  List.iter pythagorean_theorem data

let () = main ()

(*
$ cat DATA.lst | ocaml ludsrill.ml
  A A A R R A O R R A A O A A A A A R R R R O A A R A O
*)
