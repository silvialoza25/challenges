;; $ clj-kondo --lint nickkar.clj
;; linting took 23ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class))

;; read input and returns a vector with the information
(defn read-cases ([ncases] (read-cases ncases 0 []))
  ([ncases i out]
    (cond
      (>= i ncases) out
      :else
      (read-cases ncases (inc i) (conj out [(read) (read) (read)])))))

;; determines the angle of the triangle based on the relation of the hypotenuse
;; and pythagorean theorem
(defn pick-match [side1 side2 hypotenuse]
  (let [a (Math/pow side1 2)
        b (Math/pow side2 2)
        c (Math/pow hypotenuse 2)]
    (cond
      (> c (+ a b)) "O"
      (< c (+ a b)) "A"
      :else "R")))

;; iterates over every case
(defn solve-cases ([ncases cases] (solve-cases ncases cases 0 []))
  ([ncases cases index out]
   (cond
     (>= index ncases) out
     :else
     (let [[side1 side2 hypotenuse] (nth cases index)
           pick (pick-match side1 side2 hypotenuse)]
       (solve-cases ncases cases (inc index) (conj out pick))))))

;; Driver code
(defn main []
  (let [ncases (read)
        cases (read-cases ncases)]
    (apply println (solve-cases ncases cases))))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; A A A R R A O R R A A O A A A A A R R R R O A A R A O
