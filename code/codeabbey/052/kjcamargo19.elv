# elvish -compileonly kjcamargo.elv

use str
use math

#This function read all values from DATA.lst file
fn read_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

#This function classifies a triangle by means of operations on its legs
fn classify_triangle [data]{
  each [val]{
    var leg_a = (math:pow $val[0] 2)
    var leg_b = (math:pow $val[1] 2)
    var hypotenuse = (math:pow $val[2] 2)

    if (> $hypotenuse (+ $leg_a $leg_b)) {
      echo O
    } elif (< $hypotenuse (+ $leg_a $leg_b)) {
      echo A
    } else {
      echo R
    }
  } $data
}

#Output Value
echo (classify_triangle (read_data))

# cat DATA.lst | elvish kjcamargo19.elv
# A O O A A A R O O A A R A O R O R A R R A R A A R
