{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let intInputData = map convert inputData
  let solution = map solve intInputData
  putStrLn (unwords solution)

solve :: [Int] -> String
solve dat = res
  where
    a = head dat
    b = dat !! 1
    c = dat !! 2
    res
      | c ^ 2 == a ^ 2 + b ^ 2 = "R"
      | c ^ 2 < a ^ 2 + b ^ 2 = "A"
      | otherwise = "O"

convert :: String -> [Int]
convert dat = result
  where
    aux = words dat
    result = map read aux :: [Int]

{-
$ cat DATA.lst | ./bridamo98
  A A A R R A O R R A A O A A A A A R R R R O A A R A O
-}
