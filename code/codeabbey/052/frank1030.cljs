;$ clj-kondo --lint frank1030.cljs
;linting took 1521ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core]))

(defn validator [index]
  (let [x (atom index) a (core/read) b (core/read) c (core/read)
    rc (* c c) rab (+ (* a a) (* b b))]
    (if (> @x 0)
      (do (swap! x dec)
        (if (< rc rab)
          (print "A ")
          (if (= rc rab)
            (print "R ")
            (if (> rc rab)
              (print "O ")
              (print 0))))
        (if (not= @x 0)
          (validator @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (core/read)]
    (validator index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;R O R O O O A A O A A A O R O R O R A O R O O R O R
