#!/usr/bin/env bash
# $ shellcheck jasomo.sh
# $

PLAYER1=0
PLAYER2=0

RESULTS=""

NUMLINES=0
CURRENTLINE=0

cat < "${1:-/dev/stdin}" | while IFS= read -r LINE
do

  if [ $NUMLINES -eq 0 ]
  then
    NUMLINES=$LINE
    continue
  fi
  ((CURRENTLINE=CURRENTLINE+1))

  for MATCH in $LINE
  do
    case $MATCH in
      "RP")
        ((PLAYER2=PLAYER2+1))
        ;;
      "RS")
        ((PLAYER1=PLAYER1+1))
        ;;
      "PR")
        ((PLAYER1=PLAYER1+1))
        ;;
      "PS")
        ((PLAYER2=PLAYER2+1))
        ;;
      "SR")
        ((PLAYER2=PLAYER2+1))
        ;;
      "SP")
        ((PLAYER1=PLAYER1+1))
        ;;
      *)
        ;;
    esac
  done

  if [ "$RESULTS" != "" ]
  then
    RESULTS="$RESULTS "
  fi

  if [ "$PLAYER1" -gt "$PLAYER2" ]
  then
    RESULTS="${RESULTS}1"
  else
    RESULTS="${RESULTS}2"
  fi

  if [ "$CURRENTLINE" -eq "$NUMLINES" ]
  then
    echo "$RESULTS"
  fi

  PLAYER1=0
  PLAYER2=0
done

#$ cat DATA.lst | ./jasomo.sh
#2 2 2 1 2 2 1 2 2 2 1 1 1 2 2 2 2 1 2 2 1 1 2 2 1 1 1
