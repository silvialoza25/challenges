;; $ clj-kondo --lint bridamo98.cljs
;; linting took 73ms, errors: 0, warnings: 0

(ns bridamo98-049
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn get-duel-winner [f-plyr s-plyr]
  (if (not (= (compare f-plyr s-plyr) 0))
    (if (= (compare f-plyr "R") 0)
      (if (= (compare s-plyr "S") 0) 1 -1)
      (if (= (compare f-plyr "P") 0)
        (if (= (compare s-plyr "R") 0) 1 -1)
        (if (= (compare s-plyr "P") 0) 1 -1)))
    0))

(defn get-match-winner [match]
  (loop [i 0 total-sum 0]
    (if (< i (count match))
      (let [duel (get match i) f-plyr (subs duel 0 1) s-plyr (subs duel 1 2)]
        (recur (+ i 1) (+ total-sum (get-duel-winner f-plyr s-plyr))))
      (if (> total-sum 0) 1 2))))

(defn solve-all [size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (let [match (str/split (core/read-line) #" ")]
        (recur (+ i 1)
        (str result (get-match-winner match) " ")))
      result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 2 1 1 1 1 2 1 1 2 1 1 2 2 2 2 2 2 2 2 1 1 2 1 2 2 2 2 2 1 1
