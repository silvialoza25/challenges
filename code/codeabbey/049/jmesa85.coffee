###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #049: Rock Paper Scissors - Coffeescript

# Evaluates an individual match in the test case
evaluateMatch = (match) ->
  if match in ['RS', 'PR', 'SP']
    1 # First player wins partially
  else if match in ['SR', 'RP', 'PS']
    -1 # Second player wins partially
  else
    0 # Draw

# Evaluates the winner for the test case
evaluateFinalScore = (prev, curr) ->
  prev + curr

processTestCase = (testCaseStr) ->
  matchesStr = testCaseStr.split(' ')
  evaluatedArr = matchesStr.map(evaluateMatch)
  matchScore = evaluatedArr.reduce(evaluateFinalScore, 0)
  if matchScore >= 0
    1 # First player wins the game
  else
    2 # Second player wins the game

main = ->
  # Read data from STDIN
  data = require('fs').readFileSync(0).toString().trim().split('\n')
  # Get the test cases
  testCasesAsStrings = data.slice(1)
  # Process each test case
  results = testCasesAsStrings.map(processTestCase)
  # Print results
  console.log(results.join(' '))

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
1 1 1 1 2 2 1 2 1 1 2 2 1 2 1 1 2 2 2 2 1 1 2 1
###
