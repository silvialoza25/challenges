# elvish -compileonly alejotru3012.elv

use str

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn cmp [val1 val2]{
  res = (str:compare $val1 $val2)
  if (== $res 0) {
    put $true
  } else {
    put $false
  }
}

fn play [pairs]{
  n_wins_1 = 0
  n_wins_2 = 0
  each [pair]{
    if (not (cmp $pair[0] $pair[1])) {
      if (and (cmp $pair[0] 'S') (cmp $pair[1] 'P')) {
        n_wins_1 = (+ $n_wins_1 1)
      } elif (and (cmp $pair[0] 'P') (cmp $pair[1] 'R')) {
        n_wins_1 = (+ $n_wins_1 1)
      } elif (and (cmp $pair[0] 'R') (cmp $pair[1] 'S')) {
        n_wins_1 = (+ $n_wins_1 1)
      } else {
        n_wins_2 = (+ $n_wins_2 1)
      }
    }
  } $pairs
  if (> $n_wins_1 $n_wins_2) {
    echo 1
  } else {
    echo 2
  }
}

fn calculate [data]{
  each [values]{
    play $values
  } $data
}

echo (str:join ' ' [(calculate (input_data))])

# cat DATA.lst | elvish alejotru3012.elv
# 2 1 2 2 1 2 2 1 1 1 1 1 1 2 2 1 2 1 2 2 1 2 1 2
