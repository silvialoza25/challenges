#!/usr/bin/ruby
# frozen_string_literal: true

# $ rubocop danimelo.rb
# Inspecting 1 file
# .
# 1 file inspected, no offenses detected

def string_list
  # from string to list
  moves_played = []
  STDIN.each_line do |line|
    moves = line.split(' ')
    moves_played.append(moves)
  end
  moves_played.shift
  moves_played
end

def comparator(play: '')
  # draw or win for 1
  if %w[RR SS PP].include?(play)
    0
  elsif %w[RS PR SP].include?(play)
    1
  end
end

def played(winner: [])
  # determine the winner for each round
  string_list.each do |sublist|
    one = 0
    two = 0
    sublist.each do |play|
      comparator(play: play) == 1 ? one += 1 : two += 1
    end
    one > two ? winner.append('1') : winner.append('2')
  end
  print winner
end

played(winner: [])

__END__

$ cat DATA.lst | ./danimelo.rb
["2", "2", "2", "2", "1", "2", "2", "2", "2", "2", "1",
"2", "2", "2", "2", "2", "2", "2", "2", "2", "2", "2",
"1", "1", "2", "2", "2", "2", "2"]
