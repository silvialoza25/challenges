#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defvar *data*)
(defvar *item*)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect line)))

(defun split-by-one-space (item)
  (loop for i = 0 then (1+ j)
    as j = (position #\Space item :start i)
    collect (subseq item i j)
    while j))

(defun calculate-weight (aux-item-split)
  (let ((weight 0)
        (str (copy-seq aux-item-split)))
        (loop for i from 0 to (- (length aux-item-split) 1)
          do(setq weight (+ weight (* (digit-char-p (elt str i)) (+ i 1)))))
        (format t "~s " weight)))

(setq *data* (cdr (read-data)))

(dolist (*item* *data*)
  (setq item-split (split-by-one-space *item*))
    (loop for aux-item-split in item-split
      do(calculate-weight aux-item-split)))

#|
cat DATA.lst | clisp ludsrill.lsp
3 48 127 111 160 18 5 74 269 19 22 7 51 171 177 42 90 189 189 173 17 40 58 20
26 212 19 50 120 105 239 91 132 33 19
|#
