;; $ clj-kondo --lint bridamo98.cljs
;; linting took 45ms, errors: 0, warnings: 0

(ns bridamo98-013
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn calc-weighted-sum [num]
  (let [size-num (count num)]
    (loop [i 0 result 0]
      (if (< i size-num)
        (recur (+ i 1) (+ result
        (* (+ i 1) (edn/read-string (subs num i (+ i 1))))))
        result))))

(defn solve-all [size-input data-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (recur (+ i 1)
      (str result (calc-weighted-sum (get data-input i)) " "))
      result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))
  data-input (str/split (core/read-line) #" ")]
    (println (solve-all size-input data-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 3 48 127 111 160 18 5 74 269 19 22 7 51 171 177 42 90 189
;; 189 173 17 40 58 20 26 212 19 50 120 105 239 91 132 33 19
