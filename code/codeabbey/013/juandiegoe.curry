-- $ curry-verify juandiegoe.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `juandiegoe'!

import IO
import List
import Read
import Char

getLines :: IO String
getLines = do
  dataLine <- getContents
  return dataLine

parseInt :: String -> Int
parseInt str = read str :: Int

splitData :: [Prelude.Char] -> [Prelude.Int]
splitData digit = map parseInt (words digit)

conv :: [Prelude.Char] -> [Prelude.Int]
conv num = map digitToInt num

calc :: Prelude.Num a => [a] -> a
calc arr = sum (snd (mapAccumL (\num it -> (num + 1,(num * it))) 1 arr))

main :: IO ()
main = do
  input <- getLines
  let
    values = split (== '\n') input
    cleanData = map splitData (drop 1 (init values))
    parseData = map show (concat cleanData)
    convData = map conv parseData
    result = map (\x -> show (calc x)) convData
  putStrLn (unwords result)

-- cat DATA.lst | pakcs :load juandiegoe.curry :eval main :quit
-- 3 48 127 111 160 18 5 74 269 19 22 7 51 171 177 42 90 189 189
-- 173 17 40 58 20 26 212 19 50 120 105 239 91 132 33 19
