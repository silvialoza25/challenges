;; $ clj-kondo --lint nickkar.cljs
;; linting took 77ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(defn read-vector ([input-size] (read-vector 0 input-size []))
  ([i input-size vec]
   (cond
     (>= i input-size) vec
     :else (read-vector (inc i) input-size (conj vec (core/read))))))

(defn vec-ini ([n] (vec-ini n 1 []))
  ([n i vec]
   (cond
     (> i n) vec
     :else (vec-ini n (inc i) (conj vec i)))))

(defn countt [num vec]
  (count (filter #(= % num) vec)))

(defn find-ocurrence [n input-vector]
  (let [initial-vec (vec-ini n)]
    (map #(countt % input-vector) initial-vec)))

(defn main []
  (let [input-size (core/read)
        n (core/read)
        input-vector (read-vector input-size)]
    (apply println (find-ocurrence n input-vector))))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 21 24 32 24 23 25 29 27 25 28 16 19 30
