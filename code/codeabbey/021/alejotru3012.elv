# elvish -compileonly alejotru3012.elv

use str

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })]
}

fn calculate [data]{
  @res = (repeat $data[0][1] 0)
  each [value]{
    index = (- $value 1)
    res[$index] = (echo (+ $res[$index] 1))
  } $data[1]
  put $res
}

echo (str:join ' ' (calculate (input_data)))

# cat DATA.lst | elvish alejotru3012.elv
# 32 39 36 30 35 36 40 34 37
