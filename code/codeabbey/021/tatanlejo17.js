/*
$ eslint tatanlejo17.js
*/

function arrayCounters() {
  process.stdin.setEncoding('utf8');
  process.stdin.on('readable', () => {
    const inData = process.stdin.read();
    if (inData !== null) {
      // Transform the data read to Number
      const values = inData.split(' ').map((oneData) => Number(oneData));
      // Find the greatest value of the array
      const max = Math.max(...values);
      // Create an array with the maximum length
      // fill [] with values from 1 to max
      const sample = Array.from(Array(max).keys()).map((nue) => nue + 1);
      // Navigate through the array and filter the repeats
      sample.forEach((index) => {
        const resp = values.filter((dat) => dat === sample[index - 1]);
        process.stdout.write(`${resp.length} \n`);
        return 0;
      });
    }
    return 0;
  });
  return 0;
}

arrayCounters();

/*
$ cat DATA.lst | node tatanlejo.js
21 24 32 24 23 25 29 27 25 28 16 19 30
*/
