# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.06 seconds (0.02s to load, 0.04s running 55 checks on 1 file)
# 3 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule ArrayCounters do

  def main do
    data = to_int(String.split(hd(Enum.slice(String.split(
                    IO.read(:stdio, :all), "\n"), 1..1)), " "))
    solution = Enum.sort(Enum.frequencies(data))
    Enum.map(solution, fn {_, b} -> IO.write("#{b} ") end)
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e ArrayCounters.main
# 29 33 16 23 15 31 21 23 23 15 20 23 26 24 19 16
