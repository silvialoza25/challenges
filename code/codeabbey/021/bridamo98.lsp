#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun print-solution (results N)
  (let ((i 0))
    (loop
      (format t "~a " (aref results i))
      (setq i (+ i 1))
      (when (> i (- N 1)) (return NIL)))))

(defun main()
  (let ((M (read)) (N (read)) (results NIL))
    (setq results (make-array N :initial-element 0))
    (loop for i from 0 to (- M 1)
      do(let ((num (read)))
      (setf (aref results (- num 1))
      (+ (aref results (- num 1)) 1))))
    (print-solution results N)))

(main)

#|
  cat DATA.lst | clisp bridamo98.lsp
  29 33 16 23 15 31 21 23 23 15 20 23 26 24 19 16
|#
