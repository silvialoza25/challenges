// ponyc -b kjcamargo19

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun iterator(data: Array[String]): Array[String] =>
          data.slice(1, data.size())

        fun get_slope(const_x1: I32, const_x2: I32,
                      const_y1:I32, const_y2:I32): I32 =>
          (const_y2 - const_y1) / (const_x2 - const_x1)

        fun get_const_b(slope: I32, const_x1: I32, const_y1: I32): I32 =>
          const_y1 - (slope * const_x1)

        fun obtain_data(data: Array[String])? =>
          if data.size() > 0 then
            var line: Array[String] = data(0)?.split_by(" ")
            var const_x1: I32 = line(0)?.i32()?
            var const_y1: I32 = line(1)?.i32()?
            var const_x2: I32 = line(2)?.i32()?
            var const_y2: I32 = line(3)?.i32()?

            var slope: I32 = get_slope(const_x1, const_x2, const_y1, const_y2)
            var const_b: I32 = get_const_b(slope, const_x1, const_y1)
            env.out.write("(" + slope.string() + " " + const_b.string() + ")")
            env.out.write(" ")
            obtain_data( iterator (data))?
          end

        fun ref apply(data: Array[U8] iso) =>
          var input_data: String = String.from_iso_array(consume data)
          var line_sep: Array[String] = input_data.split_by("\n")
          var real_data: Array[String] = line_sep.slice(1, line_sep.size()-1)

          try
            obtain_data(real_data)?
          end

        fun ref dispose() =>
          None

      end,
      512
    )

// cat DATA.lst | ./kjcamargo19
// (47 902) (64 -390) (-92 636) (-5 784) (-35 749) (-59 -277) (40 -575)
// (23 -374) (65 -505) (90 338) (-38 -745) (-51 640) (-86 12) (99 -583)
