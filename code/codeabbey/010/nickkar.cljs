;; $ clj-kondo --lint nickkar.cljs
;; linting took 37ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(defn find-a [x1 y1 x2 y2]
  (/ (- y2 y1) (- x2 x1))
  )

(defn find-b [x y m]
  (- y (* m x)))

(defn itera [input-size]
  (loop [x 1]
    (when (< x (+ input-size 1))
      (let [x1 (core/read)
            y1 (core/read)
            x2 (core/read)
            y2 (core/read)
            a (find-a x1 y1 x2 y2)
            b (find-b x1 y1 a)]
        (print (str "(" a " " b ") "))
        )
      (recur (+ x 1))))
  )

(defn main []
  (let [input-size (core/read)]
    (itera input-size)
    (println "")))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; (-24 -183) (-58 -958) (4 982) (-64 916) (4 -488) (-77 -113)
;; (-45 340) (56 -459) (36 295) (34 691) (67 961) (87 39)
