{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let intInputData = map convert inputData
  let solution = map solve intInputData
  putStrLn (unwords solution)

convert :: String -> [Int]
convert dat = result
  where
    aux = words dat
    result = map read aux :: [Int]

solve :: [Int] -> String
solve dat = res
  where
    x1 = head dat
    y1 = dat !! 1
    x2 = dat !! 2
    y2 = dat !! 3
    a = (y2 - y1) `div` (x2 - x1)
    b = y1 - (a * x1)
    res = "(" ++ show a ++ " " ++ show b ++ ")"

{-
$ cat DATA.lst | ./bridamo98
  (-24 -183) (-58 -958) (4 982) (-64 916) (4 -488) (-77 -113) (-45 340)
  (56 -459) (36 295) (34 691) (67 961) (87 39)
-}
