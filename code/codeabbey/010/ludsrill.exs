# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.06 seconds (0.02s to load, 0.04s running 55 checks on 1 file)
# 5 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule LinearFuction do
  def main do
    data = get_data()
    Enum.map(data, &linear_fuction(&1))
  end

  def linear_fuction([x1, y1, x2, y2]) do
    m = div(y1 - y2, x1 - x2)
    b = y1 - x1 * m
    IO.write("(#{m} #{b}) ")
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def get_data do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    [_ | tail] = List.delete_at(all_data, 1 +
                  String.to_integer(Enum.at(all_data, 0)))
    Enum.map(Enum.map(tail, &String.split(&1, " ")), &to_int/1)
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e LinearFuction.main
# (92 -100) (81 -886) (35 -437) (41 549) (-81 70) (10 617) (25 951) (88 -372)
# (53 -673) (-56 -68) (49 172) (-28 -842) (23 -963)
