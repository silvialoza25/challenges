;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))

(defn get-vals [line]
  (let [values (str/split line #" ")
        x1 (read-string (values 0))
        y1 (read-string (values 1))
        x2 (read-string (values 2))
        y2 (read-string (values 3))]
    [x1 y1 x2 y2]))

(defn solution [_ body]
  (doseq [x body]
    (let [[x1 y1 x2 y2] (get-vals x)
          x (/ (- y2 y1) (- x2 x1))
          y (/ (- (* x2 y1) (* x1 y2)) (- x2 x1))
          a (int x)
          b (int y)]
      (print (str "(" a " " b ") ")))))

(defn main []
  (let [[head body] (get-data)]
    (solution head body)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; (1 23751) (2 47286) (1860 37200) (52 102492) (11 13728) (71 191913)
;; (2 1786) (78 323544) (28 100492) (108 19656) (52 146224) (275 22000)
;; (2 1012) (5 190) (1 602) (8 824) (62 335916) (44 11704) (4 12) (1 2577120)
