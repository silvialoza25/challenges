/*
$ dartanalyzer --fatal-infos --fatal-warnings --no-declaration-casts \
 --no-implicit-casts --no-implicit-dynamic --lints samjoo.dart
Analyzing samjoo.dart...
No issues found!
*/

import 'dart:io';

/// Reads and parse [linesNum] lines from the STDIN.
List<List<int>> readData(int linesNum) {
  List<List<int>> lines = List<List<int>>(linesNum);

  for(int i = 0; i < linesNum; i++) {
    lines[i] = [];
    stdin.readLineSync().split(' ').forEach((String elem) {
      lines[i].add(int.parse(elem));
    });
  }

  return lines;
}

/// Finds the median from a list [i] of 3 values.
int findMedian(List<int> i) {
  int res;
 // a = b < a < c || c < a < b
  if((i[1] < i[0] && i[0] < i[2]) || (i[2] < i[0] && i[0] < i[1])) {
    res = i[0];
  // b = a < b < c || c < b < a
  } else if((i[0] < i[1] && i[1] < i[2]) || (i[2] < i[1] && i[1] < i[0])) {
    res = i[1];
  // c = a < c < b || b < c < a
  } else if((i[0] < i[2] && i[2] < i[1]) || (i[1] < i[2] && i[2] < i[0])) {
    res = i[2];
  }
  return res;
}

void main(List<String> args) {
  List<List<int>> data = readData(int.parse(stdin.readLineSync()));

  data.forEach((testCase) {
    int med = findMedian(testCase);
    stdout.write("${med} ");
  });
}

/*
$ cat DATA.lst |dart samjoo.dart
1052 128 10 389 361 98 14 143 14 19 388 766 251
13 108 840 441 854 155 9 857 60 9 8 7
*/
