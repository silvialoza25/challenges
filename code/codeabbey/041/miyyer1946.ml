(*
$ ocamlc -w @0..1000 miyyer1946.ml
*)

let get_median_of_three subList =
  Printf.printf "%i " (List.nth subList 1)

let splitting line =
  String.split_on_char ' ' line

let maybe_read_line () =
  try Some (read_line ()) with
  | End_of_file -> None

let rec get_data data =
  match maybe_read_line () with
  | Some (line) -> get_data (data @ [List.sort compare
                                (List.map int_of_string (splitting line))])
  | None -> List.iter get_median_of_three (List.tl data)

let () = get_data []

(*
$ cat DATA.lst | ./slayfer1112
672 457 974 93 88 98 25 1101 40 57 104 281 81 657
14 952 51 442 1058 900 465 1268 20 7 89 509 240
*)
