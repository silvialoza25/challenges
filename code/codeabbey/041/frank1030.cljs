;$ clj-kondo --lint frank1030.cljs
;linting took 532ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core]))

(defn validator [index]
  (let [x (atom index) a (core/read) b (core/read) c (core/read)]
    (if (> @x 0)
      (do (swap! x dec)
        (if (or (and (> a b) (< a c)) (and (< a b) (> a c)))
          (println a)
          (if (or (and (> b a) (< b c)) (and (< b a) (> b c)))
            (println b)
            (if (or (and (> c b) (< c a)) (and (< c b) (> c a)))
              (println c)
              (print 0))))
        (if (not= @x 0)
          (validator @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (core/read)]
    (validator index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;1052 128 10 389 361 98 14 143 14 19 388
;766 251 13 108 840 441 854 155 9 857 60 9 8 7
