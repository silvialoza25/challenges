# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.03s to load, 0.1s running 55 checks on 1 file)
# 4 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule MedianOfThree do

  def main do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    aux_data = Enum.map(all_data, fn x -> String.split(x, " ") end)
    median_of_three(Enum.map(List.delete_at(aux_data,
                      Enum.count(aux_data) - 1), &to_int/1))
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

 def median_of_three ([_ | tail]) do
    Enum.map(Enum.map(Enum.map(tail, &Enum.sort/1), &Enum.at(&1, 1)),
      &IO.write("#{&1} "))
 end
end

# cat DATA.lst | elixir -r ludsrill.exs -e MedianOfThree.main
# 15 779 161 45 48 804 63 489 81 52 431 1038 6 494 205 1463 77 678 631 664 173
# 11 21 755 119 560 583 1049 82 96
