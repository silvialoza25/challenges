/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, bool, int, char.

:- func findMid(int, int, int) = int.
findMid(Num1, Num2, Num3) =
(
  if
    pred_to_bool(Num1 > Num2) `xor` pred_to_bool(Num1 > Num3) = Mid1,
    Mid1 = yes
  then Num1
  else if
    pred_to_bool(Num2 < Num1) `xor` pred_to_bool(Num2 < Num3) = Mid2,
    Mid2 = yes
  then Num2
  else Num3
).

:- pred findMids(list(string), string, string).
:- mode findMids(in, in, out) is det.
findMids([], FinalMid, strip(FinalMid)).
findMids([Line | Tail], PartialMid, FinalMid) :-
(
  words_separator(char.is_whitespace, Line) = InputList,
  map(det_to_int, InputList) = NumList,
  det_index0(NumList, 0, Num1),
  det_index0(NumList, 1, Num2),
  det_index0(NumList, 2, Num3),
  findMid(Num1, Num2, Num3) = Mid,
  PartialMid1 = PartialMid ++ " " ++ from_int(Mid),
  findMids(Tail, PartialMid1, FinalMid)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1, FileContents, MidList),
    findMids(MidList, "", Mids),
    io.print_line(Mids, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  941 833 54 889 260 883 127 89 39 17 12 1160 56 11 93 948 956 277
  499 1393 533 95 77 43 100
*/
