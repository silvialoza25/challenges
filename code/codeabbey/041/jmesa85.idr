{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #041: Median of Three - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi s = fromMaybe 0 $ parseInteger {a=Int} s

intAt : Int -> List Int -> Int
intAt 0 (x::xs) = x
intAt k (_::xs) = intAt (k - 1) xs

getLines : IO(List String)
getLines = do
  x <- getLine
  if x == "" then pure []
    else do
      xs <- getLines
      pure (x :: xs)

processTestCase : List Int -> Int
processTestCase nums = intAt 1 $ sort nums

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse input array
    testCases = map (map stoi) $ map words inputData
    -- Process data
    results = map processTestCase testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  941 833 54 889 260 883 127 89 39 17 12
  1160 5611 93948 956277 499 1393 533 95 77 43 100
-}
