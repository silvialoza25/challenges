;; $ clj-kondo --lint bridamo98.clj
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-041
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn calc-median[numbers-array]
  (nth (sort numbers-array) 1)
)

(defn solve-all[size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (let [args (str/split (core/read-line) #" ")]
          (recur (+ i 1)
        (str result (calc-median (list (edn/read-string
        (args 0)) (edn/read-string
        (args 1)) (edn/read-string
        (args 2)))) " ")))
        result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.clj
;; 1145 13 3 107 7 1083 33 1983 61 53 876 90 240
;; 34 102 893 899 169 535 426 786 29 111 99 312
