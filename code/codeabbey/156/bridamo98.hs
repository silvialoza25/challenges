{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad
import Data.Maybe

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let solution = map fixCardNumber inputData
  putStrLn (unwords solution)

fixCardNumber :: String -> String
fixCardNumber dat = res
  where
    res = if '?' `elem` dat
            then findSolutionNumber dat (fromJust $ elemIndex '?' dat)
                 0 (True :: Bool)
            else findSolutionNumber dat 0 0 (False :: Bool)

findSolutionNumber :: String -> Int -> Int -> Bool -> String
findSolutionNumber dat index posSol isCorrupted = res
  where
    posFixedNumber = if isCorrupted
                       then replaceElement index (head (show posSol)) dat
                       else swapElements posSol (posSol + 1) dat
    integritySum =  sum (calcIntegritySum (reverse posFixedNumber) 0)
    res = if (integritySum `mod` 10) == 0
            then posFixedNumber
            else findSolutionNumber dat index (posSol + 1) isCorrupted

calcIntegritySum :: String -> Int -> [Int]
calcIntegritySum [] count = []
calcIntegritySum dat count = res
  where
    iDigit = read (take 1 dat) :: Int
    iSum
      | (count `mod` 2) == 0 = iDigit
      | (iDigit * 2) > 9 = (iDigit * 2) - 9
      | otherwise = iDigit * 2
    res = iSum : calcIntegritySum (drop 1 dat) (count + 1)

swapElements :: Int -> Int -> String -> String
swapElements i j xs = res
  where
    elemI = xs !! i
    elemJ = xs !! j
    left = take i xs
    middle = take (j - i - 1) (drop (i + 1) xs)
    right = drop (j + 1) xs
    res = left ++ [elemJ] ++ middle ++ [elemI] ++ right

replaceElement :: Int -> Char -> String -> String
replaceElement i char xs = res
  where
    left = take i xs
    right = drop (i + 1) xs
    res = left ++ [char] ++ right

{-
$ cat DATA.lst | ./bridamo98
  8470548785050115 4917094138105371 3698954039141014 4273374066134611
  2731257899175549 7519859976666055 2208344973401464 7568189479924003
  1114255270098602 7100931161930110 6756127579053541 9244813567211159
  7236544385048374 4262057478255818 8405336869159683 1764915785039606
  3149865565314343 4111556277114608 4176998791391506 3132521273588442
  2399397053851841 1738285105423879 2289484777991088 2834724928975682
  7358394766892616 2467417277207641 5291062879962725 8734125827249401
  1603496862271029 5407980736601668 7684968528225580 4937197720976608
  2730569494734780 4725932108918541 2832569561596747 5224929681514187
  4880312885673992 3718339731140041 7523136673088659 2313441309203150
  4293709892295548
-}
