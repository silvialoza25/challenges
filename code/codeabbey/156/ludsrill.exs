# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.03s to load, 0.07s running 55 checks on 1 file)
# 10 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule LuhnAlgorithm do

  def main do
    data = get_data()
    Enum.map(data, fn(x) -> if Enum.member?(x, "?") do first_solution(x)
                            else second_solution(x) end end)
  end

  def to_int(list) do
    Enum.map(list, fn(x) -> if x != "?" do String.to_integer(x) else x end end)
  end

  def first_solution(list) do
    aux = Enum.find_index(list, &(&1 == "?"))
    solution = List.replace_at(list, aux, correcting_mistake(list,
                luhn_algorithm(Enum.reverse(list), 1)))
    IO.write("#{Enum.join(solution, "")} ")
  end

  def second_solution(list) do
    IO.write("#{Enum.join(swap_error(list, 0), "")} ")
  end

  def get_data do
    all_data = List.delete(String.split(IO.read(:stdio, :all), "\n"), "")
    split_data = Enum.map(all_data, &String.split(&1, ""))
    tl(Enum.map(Enum.map(split_data, &Enum.reject(&1, fn x -> x == "" end)),
      &to_int/1))
  end

  def correcting_mistake(list, luhn) do
    complete_ten = 10 - rem(luhn + 10, 10)
    if rem(Enum.find_index(list, fn(x) -> x == "?" end), 2) == 0 do
      if rem(complete_ten, 2) != 0 do
        div(9 - complete_ten, 2) + complete_ten
      else
        div(complete_ten, 2)
      end
    else
      complete_ten
    end
  end

  def swap_error(list, pointer) do
    new_list = List.replace_at(list, pointer, Enum.at(list, pointer + 1))
    new_list_2 = List.replace_at(new_list, pointer + 1, Enum.at(list, pointer))
    if rem(luhn_algorithm(Enum.reverse(new_list_2), 1), 10) != 0 do
      swap_error(list, pointer + 1)
    else
      new_list_2
    end
  end

  def luhn_algorithm([head | tail], count) do
    if head != "?" do
      if rem(count, 2) != 0 do
        head + luhn_algorithm(tail, count + 1)
      else
        if head >= 5 do
          head * 2 - 9 + luhn_algorithm(tail, count + 1)
        else
          head * 2 + luhn_algorithm(tail, count + 1)
        end
      end
    else
      luhn_algorithm(tail, count + 1)
    end
  end

  def luhn_algorithm([], _) do 0 end
end

# cat DATA.lst | elixir -r ludsrill.exs -e LuhnAlgorithm.main
# 6236414264544673 9811651054957030 4893580175897415 3364835550949518
# 8367645585711403 3698254433810557 7732522440186933 8198238251779212
# 4736150630382774 7371977336595991 7240503357300605 1540067710811328
# 9908486882813590 7605960943032424 7182669566455165 3534840124935923
# 4658487526410945 1847322780948223 5435594789212541 9448689872405726
# 3002429073121991 9926419222907291 6276295971312467 5387713230505307
# 6518757132240406 3007453088317001 3424121975907433 4290949214597337
# 6451540217733415 8607208637073627 5659324808424367 6459538715770287
# 9314229718106500 2852030903133603 8023139284012319 7152345072191505
# 2160471877972096 5320010829299685 4227422017155461 9014711658622367
# 8468522311930193
