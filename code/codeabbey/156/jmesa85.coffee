###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #156: Luhn Algorithm - Coffeescript

# Returns true if checksum is ok, with Luhn Algorithm
luhn = (cardNumber) ->
  digits = cardNumber.split('').map(Number)
  auxSum = digits.map((digit, index) ->
    if (index % 2) is 0
      if (digit * 2) >= 10 then (digit * 2) - 9 else digit * 2
    else digit
    ).reduce((a, b) -> a + b)
  (auxSum % 10) is 0 # True or ralse

fixSwapError = (cardNumber, index) ->
  guess = cardNumber.substr(0, index) + cardNumber[index + 1] +
    cardNumber[index] + cardNumber.substr(index + 2)
  # Base case
  if luhn(guess) then return guess
  # Recursive case
  fixSwapError(cardNumber, index + 1)

fixMissingDigit = (cardNumber, index) ->
  guess = cardNumber.replace('?', index)
  # Base case
  if luhn(guess) then return guess
  # Recursive case
  fixMissingDigit(cardNumber, index + 1)

processTestCase = (testCase) ->
  if testCase.includes('?') then return fixMissingDigit(testCase, 0)
  fixSwapError(testCase, 0)

main = ->
  # Read data from STDIN
  data = require('fs').readFileSync(0).toString().trim().split('\n')
  # Get the test cases
  testCasesAsStr = data.slice(1)
  # Process each test case
  results = testCasesAsStr.map(processTestCase)
  # Print results
  console.log(results.join(' '))

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
  3979657248496042 7647223959187362 8067011659981807
  2459572485662886 1100897851084869 1445751644869652
  5343839279572701 6468185641133534 2429008050561861
  5692950171671827 9837628370117036 8411998760659889
  2115009129209325 6301444365952491 2017302152625247
  2143663823332654 8485571705934369 9829033539406802
  2662366940323173 7169694373875899 5409977872449850
  1350940844875014 9998550025190058 7606670178544717
  9594597611821735 8093031265673844 2261847412380804
  6042229468616992 1857648589974508 7414829637888642
  1094375319463700 5724773338609882 9558521436072732
  1568083652128701 5062000569484748 3415485042751830
  1221791427357400 1859810065380281 1404469166891363
  5629348619978698 1643344961453662 5955372884748887
  2478873782920218 9143899220184034 3023244332303461
  2692359266949097 7027609641907615 9442441575558534
###
