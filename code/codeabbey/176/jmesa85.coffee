###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #176: Say 100 - Coffeescript
# Note: The token expires after 1 hour

notifyServer = (url, token, message) ->
  request = require('request')
  request.post(
    { url: url, form: message },
    (e, http, body) -> say100(url, token, body)
  )
  return

say100 = (url, token, status) ->
  if status? # Not null or undefined
    if status is ''
      # Initial message
      notifyServer(url, token, { token: token })
    else if status.includes('secret')
      # A secret was given, respond with an answer
      secret = parseInt(status.split(' ')[1])
      answer = 100 - secret
      notifyServer(url, token, { token: token, answer: answer })
    else if status.includes('end')
      # Print the victory token
      endToken = status.split(' ')[1]
      console.log(endToken)
  return

main = ->
  url = 'http://open-abbey.appspot.com/interactive/say-100'
  # Read STDIN
  token = require('fs').readFileSync(0).toString().trim()
  # Init game
  say100(url, token, '')
  return

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
8XdSyX6hZNSyV3fjZKhjkjI0
###
