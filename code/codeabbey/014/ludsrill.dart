// Analyzing ludsrill.dart...
// No issues found!

import 'dart:io';

void main() {
  List<List<String>> input = getData([]);
  BigInt solution = modularCalculator(input.sublist(1),
    BigInt.parse(input.first.first));
  print(solution);
}

List<List<String>> getData(List<List<String>>input) {
  String getLine = stdin.readLineSync();
  if (getLine == null) {
      return input;
  } else {
      List<String> newLine = getLine.split(" ");
      return getData(input + [newLine]);
  }
}

BigInt modularCalculator(List<List<String>> values, BigInt acc) {
  switch (values.first.first) {
    case "+":
      return modularCalculator(values.sublist(1), acc +
        BigInt.parse(values.first.last));
    case "*":
      return modularCalculator(values.sublist(1), acc *
        BigInt.parse(values.first.last));
    case "%":
      return acc % BigInt.parse(values.first.last);
      break;
  }
  return acc;
}

// cat DATA.lst | dart ludsrill.dart
// 434
