# $ lintr::lint('nickkar.r')

operate <- function(cases, base, mod, index=1) {
    if (index > length(cases)) {
        return(base)
    }
    tmp <- unlist(strsplit(cases[index], " "))
    num <- as.double(tmp[2])
    newbase <- switch(tmp[1],
    "*" = (base * num) %% mod,
    "+" = (base + num) %% mod,
    "%" = base %% num
    )
    operate(cases, newbase, mod, index + 1)
}

main <- function() {
    data <- readLines("stdin")
    cases <- data[2:length(data)]
    base <- as.double(data[1])
    mod <- as.double(unlist(strsplit(cases[length(cases)], " "))[2])
    out <- operate(cases, base, mod)
    cat(out)
    cat("\n")
}

main()

# $ cat DATA.lst | Rscript nickkar.r
# 2178
