/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, integer, char.

:- func performOperation(integer,string,integer) = integer.
performOperation(X,Op,Y) =
(
 if Op = "*" then X*Y
 else if Op = "+" then X+Y
 else X rem Y
).

:- pred modularCalculator(list(string)::in,integer::in,integer::out) is det.
modularCalculator([],FinalResult,FinalResult).
modularCalculator([Line | Tail],PartialResult,FinalResult) :-
(
  string.words_separator(char.is_whitespace,Line) = Input,
  det_index0(Input,0) = Operator,
  integer(det_to_int(det_index0(Input,1))) = Operand,
  performOperation(PartialResult,Operator,Operand) = NewResult,
  modularCalculator(Tail,NewResult,FinalResult)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
                   list(string)::in,io::di,io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    strip(list.det_head(FileContents)) = NumStr,
    list.det_drop(1,FileContents,ComputeList),
    modularCalculator(ComputeList,integer(det_to_int(NumStr)),Answer),
    io.write_int(det_to_int(Answer),!IO),
    io.nl(!IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  1917
*/
