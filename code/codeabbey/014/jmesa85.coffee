###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #014: Modular Calculator - Coffeescript

getResult = (acc, operation) ->
  sign = operation.split(' ')[0]
  value = BigInt(operation.split(' ')[1])
  if sign is '+'
    acc + value
  else if sign is '*'
    acc * value
  else if sign is '%'
    acc % value

main = ->
  # Read data from STDIN
  data = require('fs').readFileSync(0).toString().trim().split('\n')
  # Get initial seed
  initialInteger = parseInt(data[0])
  # Get the operations
  operationsArr = data.slice(1)
  console.log(operationsArr.reduce(
    getResult, BigInt(initialInteger)).toString())

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
1917
###
