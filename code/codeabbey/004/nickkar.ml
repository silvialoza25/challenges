(*
$ ocamlc -w @0..1000 nickkar.ml
*)

open Printf
open List

let value x = x

let n = read_int()

let splitting line =
  map int_of_string (String.split_on_char ' ' line)

let rec read_num i a =
  if i >= n-1 then [a]
  else a :: read_num (i+1) (splitting (read_line()))

let niter = read_num 0 (splitting (read_line()))

let f elem =
  printf "%d " (min (nth elem 0) (nth elem 1))

let() =
  iter f niter;
  print_string "\n"

(*
$ cat DATA.lst | ocaml nickkar.ml
-9584671 -9967688 -5820444 -6725380 1551823 -2538058 903183 -1925208 -7366848
-1587267 -8848449 -1022875 -2862578 2579266 2835504 -8540895 -6436520 -8508584
-2256964 3642553 -3436033 -6635637 4831591 -9899698 -9629366 7834795 65773
-9421890 -2489951
*)
