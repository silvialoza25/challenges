-- $ curry-verify juandiegoe.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `juandiegoe'!

import Float
import IO
import List
import Read

getLines :: IO String
getLines = do
  x <- getContents
  return x

parseInt :: String -> Int
parseInt s = read s :: Int

getValues :: String -> Int
getValues str = result
  where
    array = words str
    constA = parseInt (array !! 0)
    constB = parseInt (array !! 1)
    result = min (constA) (constB)

main :: IO ()
main = do
  input <- getLines
  let
    values = split (=='\n') input
    dropData = drop 1 (init values)
    result = map (\x -> show (getValues x)) dropData
  putStrLn (unwords result)

-- cat DATA.lst | pakcs :load juandiegoe.curry :eval main :quit
-- -5852338 -6700052 -7768359 4303433 -8111005 -9270183 2907213 -6235754
-- -5419892 -8531413 -4070517 -4114194 -6889067 5760470 4016116 -8143193
-- 1427728 -8067047 -5441859 -1076433 -4632090 1573013 6859946 -1025927
-- 454421 -3300962 -2459536 104709
