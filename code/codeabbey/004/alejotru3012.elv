# elvish -compileonly alejotru3012.elv

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn min_of_two [data]{
  each [values]{
    echo (math:min $@values)
  } $data
}

echo (str:join ' ' [(min_of_two (input_data))])

# cat DATA.lst | elvish alejotru3012.elv
# -13978 -6832899 -3396912 -8375216 -2862867 -1611885 -1651739 3432704
# -6889273 118822 391842 -8381067 -6649714 -9422544 -3209012 22949
# -2039118 3768675 -8437552 -6632715 -4758519 950830 -8221703
# -6458361 1374481 -9395189 421500 -8819772 -8363458
