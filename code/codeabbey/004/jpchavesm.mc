/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, char.

:- func findMin(int, int) = int.
findMin(NumA, NumB) =
(
  if NumA < NumB
  then NumA
  else NumB
).

:- pred findMinimums(list(string), string, string).
:- mode findMinimums(in, in, out) is det.
findMinimums([], Solution, Solution).
findMinimums([Line | Tail], PartialSolution, Solution) :-
(
  string.words_separator(char.is_whitespace, Line) = NumsStr,
  map(det_to_int,NumsStr) = Nums,
  findMin(det_index0(Nums,0), det_index0(Nums,1)) = Answer,

  PartialSolution1 = PartialSolution ++ " " ++ from_int(Answer),
  findMinimums(Tail, PartialSolution1, Solution)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1, FileContents, ComputeList),
    findMinimums(ComputeList, "", Solution),
    io.print_line(Solution,!IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  -4389814 -6785279 2732449 -685285 -4764212 -8763663 -553234 -2869104 -4799525
  -2366445 -7268750 -1613993 -2301050 -1071190 -8198186 2283501 -2588000
  -9855551 3425674 172241 -9271284 175481 -4310213 -6112661 -5060067 767051
  -7940519
*/
