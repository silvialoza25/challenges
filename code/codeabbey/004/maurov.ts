/*
$ npx eslint maurov.ts
$ npx prettier --check "maurov.ts"
Checking formatting...
All matched files use Prettier code style!
*/

function calculateMinimum(processedData: string[]) {
  processedData.forEach((line: string) => {
    const numbers = line.split(' ').map((numStr: string) => Number(numStr));
    process.stdout.write(`${Math.min(...numbers)} `);
    return 'results modified';
  });
  return 'Task finished succesfully';
}

function processData(rawData: string) {
  const processedData = rawData
    .split('\n')
    .filter((numbers: string) => numbers)
    .slice(1);
  calculateMinimum(processedData);
  return 'Data processed';
}

process.stdin.setEncoding('utf8');
process.stdin.on('readable', () => {
  const rawData = process.stdin.read();
  if (rawData !== null) {
    processData(rawData);
  }
  return 'Data read from standard input';
});

/*
$ tsc maurov.ts
$ cat DATA.lst | node maurov.js
586254 6209071 -6824287 -7578938 2536687 -6785022 -8660201 1462649 -1406074 -68
03702 -3391748 1668688 -832977 -9849648 -7385857 -7910251 -9550187 -1052633 -48
72152 -8570407 -1474502
*/
