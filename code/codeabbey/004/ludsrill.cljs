;; clj-kondo --lint ludsrill.cljs
;; linting took 38ms, errors: 0, warnings: 0

(ns ludsrill.004
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn -main []
  (let [data (rest (get-data))]
    (doseq [item data]
      (if (< (Integer. (first item)) (Integer. (second item)))
        (print (core/format "%s " (first item)))
        (print (core/format "%s " (second item)))))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 2810692 -2758103 -5790399 -2458360 -9675141 -3392509 -3521353 -952063
;; -5358071 -7521526 -6014790 -9005257 6281092 -9409423 -3037736 -8891204
;; -1218758 -8204586 -8459632 -2622886
