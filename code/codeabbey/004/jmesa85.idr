{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #004: Minimum of Two - Idris2

module Main
import Data.List
import Data.Strings

-- No Maybe in this parsing
strToInt : String -> Int
strToInt s =
  case parseInteger {a=Int} s of
    Nothing => 0
    Just j  => j

-- Gets lines in the Stdin
getLines : IO (List String)
getLines = do
  x <- getLine
  if x == "" then pure []
    else do
      xs <- getLines
      pure (x :: xs)

partial processTestCase : List Int -> Int
processTestCase [num1, num2] = min num1 num2

-- Point of entry
partial main : IO ()
main = do
  -- Read Stdin
  inputData <- getLines
  let
    -- Get whole travellers data
    testCases = map (map (strToInt)) $ map words $ drop 1 inputData
    -- Process data
    results = map processTestCase testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  -4389814 -6785279 2732449 -685285 -4764212 -8763663 -553234 -2869104
  -4799525 -2366445 -7268750 -1613993 -2301050 -1071190 -8198186 2283501
  -2588000 -9855551 3425674 172241 -9271284 175481 -4310213 -6112661
  -5060067 767051 -7940519
-}
