// ponyc -b asalgad2

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail( array: Array[String] ): Array[String] =>
          array.slice(1, array.size())

        fun min( const_a: I64, const_b: I64 ): I64 =>
          if const_a < const_b then
            const_a
          else
            const_b
          end

        fun sum( lines: Array[String] ) ? =>
          if lines.size() > 0 then
            var values: Array[String] = lines(0)?.split(" ")
            var const_a: I64 = values(0)?.i64()?
            var const_b: I64 = values(1)?.i64()?
            env.out.write( min(const_a, const_b).string() )
            env.out.write( " " )
            sum( tail(lines) )?
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")
          var useful_lines: Array[String] = lines.slice(1, lines.size()-1)

          try
            sum( useful_lines )?
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// -5852338 -6700052 -7768359 4303433 -8111005 -9270183 2907213 -6235754
// -5419892 -8531413 -4070517 -4114194 -6889067 5760470 4016116 -8143193 1427728
// -8067047 -5441859 -1076433 -4632090 1573013 6859946 -1025927 454421 -3300962
// -2459536 104709
