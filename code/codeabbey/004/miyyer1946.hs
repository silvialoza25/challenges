{-
 $ ghc  miyyer1946.hs
   [1 of 1] Compiling Main ( miyyer1946.hs )
   Linking code ...
 $ hlint miyyer1946.hs
   No hints
-}

main = do
  n <- readLn :: IO Int
  readPairs n

readPairs :: Int -> IO ()
readPairs n = do
  let var = 9
  array <- getLine
  let xs = map read $ words array :: [Int]
  if head xs >= (xs !! 1)
    then putStr $ show (xs !! 1)
    else putStr $ show (head xs)
  putStr " "
  readPairs (n - 1)

{-
 $ ./miyyer1946
 -9584671 -9967688 -5820444 -6725380 1551823 -2538058 903183 -9421890
 -2489951 -1925208 -7366848 -1587267 -8848449 -1022875 -2862578
 2579266 2835504 -8540895 -6436520 -8508584 -2256964 3642553
 3436033 -6635637 4831591 -9899698 -9629366 7834795 65773
-}
