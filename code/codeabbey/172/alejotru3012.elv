# elvish -compileonly alejotru3012.elv

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn to_radians [deg]{
  put (/ (* $deg $math:pi) 180)
}

fn get_h [d_1 ang_a ang_b]{
  rad_a = (to_radians $ang_a)
  rad_b = (to_radians $ang_b)
  alpha = (- $math:pi $rad_b)
  beta = (- $math:pi $rad_a $alpha)
  echo (math:round (/ (* $d_1 (math:sin $alpha) ^
    (math:sin $rad_a)) (math:sin $beta)))
}

fn calculate [data]{
  each [values]{
    get_h $@values
  } $data
}

echo (calculate (input_data))

# cat DATA.lst | elvish alejotru3012.elv
# 1264 609 673 1125 998 1451 1186 761 1395 1151 540 1589 617 1074 1149 1958
