-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import Float
import IO
import List
import Read

getData :: IO String
getData = do
  x <- getContents
  return x

strToF :: String -> Float
strToF s = read s :: Float

toRad :: Float -> Float
toRad angle = (angle * pi) / 180

getAltitude :: Float -> Float -> Float -> Int
getAltitude constD1 angleA angleB = altitude
  where
    angleAtoRad = toRad angleA
    angleBtoRad = toRad angleB
    termOne = (tan angleAtoRad) * (tan angleBtoRad)
    termTwo = (tan angleBtoRad) - (tan angleAtoRad)
    altitude = round (constD1 * termOne / termTwo)

getValues :: String -> Int
getValues dataIn = result
  where
    array = words dataIn
    constD1 = strToF (array !! 0)
    angleA = strToF (array !! 1)
    angleB = strToF (array !! 2)
    result = getAltitude constD1 angleA angleB

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> show (getValues x)) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 1045 1387 1993 846 769 687 1034 1557 1888 1544
-- 799 1857 800 1863 907 651 1817 921 1978
