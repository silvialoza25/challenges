;$ clj-kondo --lint frank1030.cljs
;linting took 319ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core]))

(defn cloud-height [index]
  (let [x (atom index) d1 (core/read) a (Math/toRadians (core/read))
    b (Math/toRadians (core/read))
    d2 (/ (* d1 (Math/tan a)) (- (Math/tan b) (Math/tan a)))
    height (* (Math/tan b) d2) res(Math/round height)]
    (if (> @x 0)
      (do (swap! x dec)
        (print res "")
        (if (not= @x 0)
          (cloud-height @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (core/read)]
    (cloud-height index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;1041 1534 1081 928 1622 860 1953 1041 1568 645 772 762 1045 1239 1425
