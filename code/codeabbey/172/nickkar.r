# $ lintr::lint('nickkar.r')

# degrees to radians
to_rads <- function(angle) {
    return(angle * pi / 180)
}

# calculates the height of the airplane/cloud
calculate_height <- function(dist_1, ang_a, ang_b) {
    rad_a <- to_rads(ang_a)
    rad_b <- to_rads(ang_b)
    height <- (tan(rad_a) * dist_1) / (1 - (tan(rad_a) / tan(rad_b)))
    return(round(height))
}

# iterates over every test-case
solve_cases <- function(cases, out = c(), index=1) {
  if (index > length(cases)) {
    return(out)
  }
  else {
    tmp <- unlist(strsplit(cases[index], " "))
    dist_1 <- as.double(tmp[1])
    ang_a <- as.double(tmp[2])
    ang_b <- as.double(tmp[3])
    height <- calculate_height(dist_1, ang_a, ang_b)
    solve_cases(cases, c(out, height), index + 1)
  }
}

# driver code
main <- function() {
  data <- readLines("stdin")
  out <- solve_cases(data[2:length(data)])
  cat(out, "\n")
}

main()

# $ cat DATA.lst | Rscript nickkar.r
# 1731 1044 1069 1743 1866 1971 1185 1925 1352 537 1722 572 1160 1303 1365 837
# 1684 523
