# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.02s to load, 0.1s running 55 checks on 1 file)
# 10 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule EnumeratingCombinations do
  def main do
    data = get_data()
    list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F", "G",
            "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
            "U", "V", "W", "X", "Y", "Z"]
    Enum.map(data, fn [a, b, c] -> combination(a, b, c) end)
      |> Enum.map(&solution(&1, list))
      |> Enum.map(&IO.write("#{&1} "))
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def solution(sol, list) do
    Enum.map(sol, &Enum.at(list, &1))
      |> Enum.join("")
  end

  def get_data do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    [_ | tail] = List.delete_at(all_data, 1 +
                  String.to_integer(Enum.at(all_data, 0)))
    Enum.map(Enum.map(tail, &String.split(&1, " ")), &to_int/1)
  end

  def factorial(n) do
    cond do
      n == 0 -> 1
      n == 1 -> 1
      n > 1 -> factorial(n - 1) * n
    end
  end

  def nck(n, k) do
    if n < k do 0
    else
      Float.round(factorial(n) / (factorial(k) * factorial(n - k)), 1)
    end
  end

  def get_n(a, b, x) do
    if nck(a, b) > x do
      get_n(a - 1, b, x)
    else
      a
    end
  end

  def get_x(n, k, x, a, b, acc) do
      if k > 0 do
        aux = get_n(a - 1, b, x)
        get_x(n, k - 1, (x - nck(aux, b)), aux, b - 1, acc ++ [n - 1 - aux])
      else
        acc
      end
  end

  def combination(n, k , m) do
    get_x(n, k, nck(n, k) - 1 - m, n, k, [])
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e EnumeratingCombinations.main
# 138BEJKMNQVXY 35BEJK 2589ADE 346BFGJLOPQUV 38CD 28B
# 012346789ABCDEFGHIJKLMNOPQRST 178AEHKM 12479CHKLOQRTU 256AEGHJKQSTUVW
# 179AEGMNPQRUV 3ABCDE 235678ADGIKLOPS 2358ACDE 139ABCDEGHIJMNP 1389ABCDEGKMOPV
# 12346789ABD 1569ABGIJOPRSV 2379BCDEHKPQRUV 24589AGHKLNPT
# 012345789ABCDEFGHIJKLMNOPQRSTUVWX 2567ADFGIJRT
