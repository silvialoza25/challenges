-- $ ghc -o smendoz3 smendoz3.hs-
--   [1 of 1] Compiling Main ( smendoz3.hs, smendoz3.o )
--   Linking code ...
-- $ hlint smendoz3.hs
--   No hints

import Control.Monad

main = do
  input <- getLine
  inputs <- replicateM (read input) getLine
  print (unwords (checkAll (map checkString inputs)))

ele = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

checkString :: String -> [Int]
checkString str = map read $ words str :: [Int]

checkAll :: [[Int]] -> [String]
checkAll = map function

function :: [Int] -> String
function arg = do
  let n = head arg
  let k = arg !! 1
  let i = last arg
  enum [0..k-1] n k i 0 []

choose :: Int -> Int -> Int
choose n k =
  if n < k * 2 then
    round (chaux [0..(n - k)-1] n (n - k) 1.0)
  else
    round (chaux [0..k-1] n k 1.0)

chaux :: [Int] -> Int -> Int -> Double -> Double
chaux [] n k ret = ret
chaux [x] n k ret = ret * (fromIntegral (n-x) / fromIntegral (k-x))
chaux (x:xs) n k ret =
  chaux xs n k (ret*(fromIntegral (n-x)/fromIntegral (k-x)))

enum :: [Int] -> Int -> Int -> Int -> Int -> String -> String
enum [x] n k i j str = str ++ [ele !! last (enaux n (k-x) i j)]
enum (x:xs) n k i j str =
  enum xs n k (head (enaux n (k-x) i j))
  (last (enaux n (k-x) i j)+1) (str ++ [ele !! last (enaux n (k-x) i j)])

enaux :: Int -> Int -> Int -> Int -> [Int]
enaux n k i j =
  if choose (n-j-1) (k-1) <= i then
    enaux n k (i - choose (n-j-1) (k-1)) (j+1)
  else
    [i,j]

-- $ cat DATA.lst | ./smendoz3
--   13468ABEHKLPQT 246ACDHIKMSTV 57 25678ABCFHLNTVW 24579ABCEGKLNR
--   345BDEHKLMOSVX 35679DFHMNPQT 234567CDEHI 1279AEGKMPQRST 12369CGHJLMNOR
--   345689FGHLRTX 236FGHILNOQTUV 01345678ABCDFGHIJKLMNOPQRSTUVWXYZ
--   279ABEFGOPRTVW 12479DJKLOPQTU 259FGKLMPRSTW 29BDEFHIMNOPU
--   1457ABCDEFG 01345789ABCDEFGHIJKLMNO 134569BCDEJKMNOQRT
--   12359DHIJNQRST 2378AEIJKNOQRST
