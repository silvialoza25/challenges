{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

toIntegerNat : Nat -> Int
toIntegerNat Z = 0
toIntegerNat (S natNumber) = 1 + toIntegerNat natNumber

toInt : List String -> List Int
toInt dat = map (cast {to=Int}) dat

elemAt : Int -> List a -> a
elemAt 0 (x::xs) = x
elemAt index (_::xs) = elemAt (index - 1) xs

factorial : Int -> Double
factorial 0 = 1.0
factorial number = (cast {to=Double} number) * factorial (number - 1)

getCombinations : Int -> Int -> Double
getCombinations totalElems set = (factorial totalElems) /
  ((factorial set) * (factorial (totalElems - set)))

injectValue : a -> IO a
injectValue dat = pure dat

getInputData : IO(List String)
getInputData = do
  line <- getLine
  if line == ""
    then injectValue []
    else do
      xs <- getInputData
      injectValue (line :: xs)

findIndexes : Int -> Int -> Int -> Int -> Int -> (Int, Int)
findIndexes counter maxValue sizeComb ithComb res =
  let
    combinations = cast {to=Int} (getCombinations counter sizeComb)
    (index, nxtI) =
      if (res + combinations) == ithComb
        then ((maxValue - counter), 0)
        else if (res + combinations) > ithComb
          then ((maxValue - (counter + 1)), ithComb - res)
          else findIndexes (counter - 1) maxValue sizeComb ithComb
            (res + combinations)
  in
  (index, nxtI)

calcCombination : Int -> Int -> Int -> Int -> Int
  -> List String -> String -> String
calcCombination counter maxValue sizeComb ithComb combi alpha res =
  let
    nxtRes =
      if (toIntegerNat (length res)) >= sizeComb
        then res
        else let
          (indx, nxtI) = findIndexes (maxValue - combi - 1)
            (maxValue - combi) (sizeComb - counter - 1) ithComb 0
          iRes = elemAt (combi + indx) alpha
          nxtCombi = combi + indx + 1
        in calcCombination (counter + 1) maxValue sizeComb nxtI nxtCombi alpha
          (res ++ iRes)
  in
  nxtRes

solveSingleProblem : List (List Int) -> List String -> List String
solveSingleProblem [] alpha = []
solveSingleProblem inputData alpha =
  let
    [maxValue, sizeComb, ithComb] = fromMaybe [] (head' inputData)
    iRes = calcCombination 0 maxValue sizeComb ithComb 0 alpha ""
  in
  iRes :: (solveSingleProblem (drop 1 inputData) alpha)

main : IO ()
main = do
  arrSize <- getLine
  dat <- getInputData
  let alpha = ["0","1","2","3","4","5","6","7","8",
    "9","A","B","C","D","E","F","G","H","I","J","K",
    "L","M","N","O","P","Q","R","S","T","U","V","W","X",
    "Y","Z"]
  let inputData = map toInt (map words dat)
  let solution = solveSingleProblem inputData alpha
  putStrLn (unwords solution)

  {-
  $ cat DATA.lst | ./build/exec/bridamo98
  23789ABCDFGJQUV 12346789ACEGIJNOPQT F 24589BCFHKPSTV 4ACEFIMR
  14569BCEHKNPY 3489B 2347ACDEFGMPST 1235CEFGHJKOQRS 245678BEJORSUV
  234678AHIMOPVWY 37CDGIJMOP 9BJ 012346789ABCDEFGHIKLMNOP 379CDEHJLN
  4FHIJORS 0236789ABCDEFGIJKLNPQRVXY 3567AEHINORST
-}
