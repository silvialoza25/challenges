;; $ clj-kondo --lint nickkar.cljs
;; linting took 65ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(defn fact [x]
  (if (<= x 1N) 1N (* x  (fact (- x 1N)))))

(defn comb [n r]
  (/ (fact n) (* (fact r) (fact (- n r)))))

(defn read-cases ([n] (read-cases n 0 []))
  ([n i out]
   (cond
     (>= i n) out
     :else
     (read-cases n (inc i) (conj out [(core/read) (core/read) (core/read)]))
     )))

(defn check-comb ([N K I] (check-comb (dec N) N (dec K) I 0))
  ([i N K I actual]
   (cond
     (= (+ actual (comb i K)) I) [(- N i) 0]
     (> (+ actual (comb i K)) I) [(- N (inc i)) (- I actual)]
     :else (check-comb (dec i) N K I (+ actual (comb i K)))
     )
   ))

(defn find-combination
  ([ca5e alph] (find-combination 0 ca5e alph 0 (nth ca5e 2) ""))
  ([i ca5e alph gap combgap out]
   (cond
     (>= (count out) (nth ca5e 1)) out
     :else
     (let [[N K] ca5e
           [index newcombgap] (check-comb (- N gap) (- K i) combgap)
           combkey (nth alph (+ gap index))
           newgap (+ gap index 1)]
       (find-combination (inc i) ca5e alph newgap newcombgap (str out combkey))
       )
     )
   ))

(defn solve-cases ([cases alph] (solve-cases 0 cases alph []))
  ([i cases alph out]
   (cond
     (>= i (count cases)) out
     :else
     (solve-cases (inc i) cases alph
                  (conj out (find-combination (nth cases i) alph)))
     )
   ))

(defn main []
  (let [n (core/read)
        cases (read-cases n)
        alph "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"]
    (apply println (solve-cases cases alph))
    )
  )

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 135ACEFHJKLMNOPQRV 369AEKOSTUVXZ 123457ABCDFGHJKLMN 034567ABCDE 1345689ACE
;; 356 34678BCHPQRUVY 123456789ABDE 15789 245679HILNSVWXY 13479DEGHLMNQS 5
;; 25CFJMORUVWYZ 13456789BCFGHKPQTUVXY 1349AFGLNRVWXY 2469ACDEHJLNO
;; 367AEGHLMNOX 13567ACDEFGHIJMNOQ 0145689ACEFGHIJ 19ADFGHIJLOQRUV
