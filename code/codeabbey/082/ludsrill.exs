# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.02s to load, 0.1s running 55 checks on 1 file)
# 6 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule LevenshteinDistance do

  def main do
    all_data = IO.read(:stdio, :all)
    try do
      get_data(all_data)
        |> Enum.map(&distance(&1))
        |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
    |> Enum.reject(fn item -> item == "" end)
    |> Enum.map(&String.split(&1, " "))
    |> tl()
  end

  def distance([a, b]) do
    first_word = String.downcase(a)
    |> to_charlist
    |> List.to_tuple

    second_word = String.downcase(b)
    |> to_charlist
    |> List.to_tuple

    len_first_word = tuple_size(first_word)
    len_second_word = tuple_size(second_word)
    costs = get_cost(len_first_word, len_second_word)

    Enum.reduce(0..len_second_word - 1, costs, fn counter_j, acc ->
      Enum.reduce(0..len_first_word - 1, acc, fn counter_i, map ->
        min_lev = get_min(first_word, second_word, map, counter_i, counter_j)
        Map.put(map, {counter_i + 1, counter_j + 1}, min_lev) end) end)
    |> Map.get({len_first_word, len_second_word})
  end

  def get_cost(len_first_word, len_second_word) do
    costs = Enum.reduce(0..len_first_word, %{},
      fn counter_i, acc -> Map.put(acc, {counter_i, 0}, counter_i) end)
    Enum.reduce(0..len_second_word, costs,
      fn counter_j, acc -> Map.put(acc, {0, counter_j}, counter_j) end)
  end

  def get_min(first_word, second_word, map, counter_i, counter_j) do
    if elem(first_word, counter_i) == elem(second_word, counter_j) do
      map[{counter_i, counter_j}]
    else
      Enum.min([map[{counter_i, counter_j + 1}] + 1,
        map[{counter_i + 1, counter_j}] + 1, map[{counter_i, counter_j}] + 1 ])
    end
  end
end

LevenshteinDistance.main

# cat DATA.lst | elixir ludsrill.exs
# 10 8 3 3 7 6 6 4 10 16 7 9 4 7 2 7 7 4 7 9 4 3 8 8 7 9 7 8 8 4 3 9 9 4 7 8
