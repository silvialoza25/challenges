// ponyc -b asalgad2

use "collections"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail_strings( array: Array[String]): Array[String] =>
          array.slice(1, array.size())

        fun get_position( matrix: List[Array[I64]],
                          const_i: USize, const_j: USize ): I64 ? =>
          var array: Array[I64] = matrix(const_i)?
          var value: I64 = array(const_j)?
          value

        fun update_position( matrix: List[Array[I64]], const_i: USize,
                             const_j: USize, value: I64 ): I64 ? =>
          var array: Array[I64] = matrix(const_i)?
          array.update( const_j, value )?
          value

        fun init_columns( matrix: List[Array[I64]],
                          const_j: USize ): List[Array[I64]]? =>
          if const_j == matrix(0)?.size() then
            matrix
          else
            update_position( matrix, 0, const_j, const_j.i64() )?
            init_columns( matrix, const_j+1 )?
          end

        fun init_rows( matrix: List[Array[I64]],
                       const_i: USize ): List[Array[I64]]? =>
          if const_i == matrix.size() then
            matrix
          else
            update_position( matrix, const_i, 0, const_i.i64() )?
            init_rows( matrix, const_i+1 )?
          end

        fun create_matrix( matrix: List[Array[I64]],
                           nrows: USize, ncols: USize): List[Array[I64]] =>
          if matrix.size() == nrows then
            matrix
          else
            var array: Array[I64] = Array[I64].init(0, ncols)
            matrix.push( array )
            create_matrix( matrix, nrows, ncols )
          end

        fun min( a: I64, b: I64 ): I64 =>
          if a < b then
            a
          else
            b
          end

        fun lev_dist( word1: Array[U8] val, word2: Array[U8] val,
                      matrix: List[Array[I64]], const_i: USize,
                      const_j: USize): I64 ? =>
          if const_i < matrix.size() then
            if const_j < matrix(0)?.size() then
              var cost: I64 = 0
              if word1(const_i-1)? != word2(const_j-1)? then
                cost = 1
              end
              var const_a: I64 = get_position(matrix, const_i-1, const_j )? + 1
              var const_b: I64 = get_position(matrix, const_i, const_j-1 )? + 1
              var const_c: I64 = get_position(matrix, const_i-1, const_j-1 )?

              var result: I64 = min( const_a, min( const_b, const_c + cost ) )
              update_position(matrix, const_i, const_j, result)?
              lev_dist( word1, word2, matrix, const_i, const_j+1 )?
            else
              lev_dist( word1, word2, matrix, const_i+1, 1 )?
            end
          else
            get_position( matrix, matrix.size()-1, matrix(0)?.size()-1 )?
          end

        fun iterate( lines: Array[String], results: Array[F64] ) ? =>
          if lines.size() >= 1 then
            var args: Array[String] = lines(0)?.split_by(" ")
            var word1: Array[U8] val = args(0)?.array()
            var word2: Array[U8] val = args(1)?.array()

            var nrows: USize = word1.size() + 1
            var ncols: USize = word2.size() + 1

            var list: List[Array[I64]] = List[Array[I64]]
            var matrix: List[Array[I64]] = create_matrix( list, nrows, ncols )
            init_rows( matrix, 0 )?
            init_columns( matrix, 0 )?

            var dist: I64 = lev_dist( word1, word2, matrix, 1, 1 )?
            env.out.write( dist.string() )
            env.out.write( " " )

            iterate( tail_strings(lines), results )?
          end

        fun ref apply(data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume data)
          var lines: Array[String] = input.split_by("\n")
          var useful_lines: Array[String] = lines.slice(1, lines.size()-1)

          try
            var results: Array[F64] = Array[F64]( useful_lines.size() )
            iterate( useful_lines, results )?
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// 7 10 6 4 8 2 4 5 8 10 7 4 6 12 6 8 4 7 6 4 10 9 10 7 8 3 8 4 13 12 13 7 5 9 4
