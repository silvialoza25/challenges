{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let divInputData = map words inputData
  let solution = map solveAll divInputData
  putStrLn (unwords solution)

solveAll :: [String] -> String
solveAll inputData = res
  where
    fstWord = head inputData
    sndWord = inputData !! 1
    m = length fstWord
    n = length sndWord
    i = [0 .. m]
    j = [0 .. n]
    indexes = [x : [y] | x<- i, y<- j]
    solArr = create2dArray (m + 1) (n + 1)
    res = show (levDistance fstWord sndWord indexes solArr)

levDistance :: String -> String -> [[Int]] -> [[Int]] -> Int
levDistance fstWord sndWord [] solArr = (solArr !! length fstWord)
                                        !! length sndWord
levDistance fstWord sndWord indexes solArr = dist
  where
    iIndex = head indexes
    i = head iIndex
    j = iIndex !! 1
    nxtSolArr
      | i == 0 = changeElem i j j solArr
      | j == 0 = changeElem i j i solArr
      | (fstWord !! (i - 1)) == (sndWord !! (j - 1)) =
        changeElem i j ((solArr !! (i - 1)) !! (j - 1)) solArr
      | otherwise =
        changeElem i j
          (1 +
             minimum
               [(solArr !! i) !! (j - 1), (solArr !! (i - 1)) !! j,
                (solArr !! (i - 1)) !! (j - 1)])
          solArr
    dist = levDistance fstWord sndWord (drop 1 indexes) nxtSolArr

changeElem :: Int -> Int -> Int -> [[Int]] -> [[Int]]
changeElem row col x xs =
  let rowToReplaceIn = xs !! row
      modifiedRow = replaceElement col x rowToReplaceIn
  in replaceElement row modifiedRow xs

replaceElement :: Int -> a -> [a] -> [a]
replaceElement i x xs = res
  where
    left = take i xs
    right = drop (i + 1) xs
    res = left ++ [x] ++ right

create2dArray :: Int -> Int -> [[Int]]
create2dArray rows cols = res
  where
    res = take rows (rowMaker cols)

rowMaker :: Int -> [[Int]]
rowMaker cols = res
  where
    res = replicate cols 0 : rowMaker cols

{-
$ cat DATA.lst | ./bridamo98
  7 2 8 5 8 7 10 10 8 4 9 10 10 4 7 12 12
  11 11 2 8 8 4 9 12 8 5 7 10 6 14 6 4 7
-}
