# elvish -compileonly alejotru3012.elv

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn build_row [p_row c_row top_word row_l]{
  if (== (+ 1 (count $top_word)) (count $c_row)) {
    put $c_row
  } else {
    aux = 0
    if (not (== (str:compare $row_l $top_word[(- (count $c_row) 1)]) 0)) {
      aux = 1
    }
    new_val = (math:min (+ $c_row[(- (count $c_row) 1)] 1) ^
      (+ $p_row[(- (count $c_row) 1)] $aux) (+ $p_row[(count $c_row)] 1))
    build_row $p_row [$@c_row (echo $new_val)] $top_word $row_l
  }
}

fn first_row [size index]{
  if (<= $index $size ) {
    echo $index
    first_row $size (+ $index 1)
  }
}

fn levenshtein_distance [word_a word_b]{
  prev_row = [(first_row (count $word_a) 0)]
  row_index = 1
  each [letter]{
    prev_row = (build_row $prev_row [(echo $row_index)] $word_a $letter)
    row_index = (+ $row_index 1)
  } $word_b
  echo $prev_row[(count $word_a)]
}

fn calculate [data]{
  each [value]{
    levenshtein_distance $value[0] $value[1]
  } $data
}

echo (str:join ' ' [(calculate (input_data))])

# cat DATA.lst | elvish alejotru3012.elv
# 5 5 9 6 10 3 11 5 7 10 7 10 6 5 5 8 7 8 6 4 5 7 9 5 7 7 5 8 7 10 5 3 13 6 5
