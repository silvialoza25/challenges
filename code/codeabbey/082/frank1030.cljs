;$ clj-kondo --lint frank1030.cljs
;linting took 319ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core]))

(defn levenshtein [str1 str2]
  (let [len1 (count str1)
    len2 (count str2)]
    (cond (zero? len1) len2
      (zero? len2) len1
      :else
      (let [cost (if (= (first str1) (first str2)) 0 1)]
        (min (inc (levenshtein (rest str1) str2))
          (inc (levenshtein str1 (rest str2)))
          (+ cost
            (levenshtein (rest str1) (rest str2))))))))

(defn get-dat [index]
  (let [x (atom index) a (str (core/read)) b (str (core/read))]
    (if (> @x 0)
      (do (swap! x dec)
        (print (levenshtein a b))
        (if (not= @x 0)
          (get-dat @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (core/read)]
    (get-dat index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;3 3 11 9 10 7 9 11 7 7 12 8
;8 10 4 4 6 5 5 5 7 13 5 4 8
;11 7 6 8 8 8 4 5 6 8 2 7
