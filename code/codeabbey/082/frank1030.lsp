#|
$ sblint -v frank1030.lsp
[INFO] Lint file frank1030.lsp

$ clisp -c frank1030.lsp
Compiling file /Users/Frank/Desktop/frank/082/frank1030.lsp ...
Wrote file /Users/Frank/Desktop/frank/082/frank1030.fas
0 errors, 0 warnings
Bye.
|#

(defun get-dat()
  (let ((dat))
    (setq dat(read))
  )
)

(defun distance (str1 str2)
  (let ((d (make-array (list (1+ (length str1)) (1+ (length str2)))
    :element-type 'integer)))
    (loop :for i :upto (length str1)
      do (setf (aref d i 0) i)
    )
    (loop :for i :upto (length str2)
      do (setf (aref d 0 i) i)
    )
    (flet ((a (index) (aref str1 (1- index)))
      (b (index) (aref str2 (1- index))))
      (loop :for i :from 1 :upto (length str1) :do
        (loop :for j :from 1 :upto (length str2)
          :for cost = (if (eq (a i) (b j)) 0 1) :do
            (setf (aref d i j)
              (min
                (1+ (aref d (1- i) j))
                (1+ (aref d i (1- j)))
                (+ cost (aref d (1- i) (1- j)))
              )
            )
            (when (and (< 1 i) (< 1 j)
              (eq (a i) (b (1- j)))
              (eq (a (1- i)) (b j)))
              (setf (aref d i j) (min (aref d i j)
                (+ cost (aref d (- i 2) (- j 2))))
              )
            )
        )
      )
    )
    (values (aref d (length str1) (length str2)) d)
  )
)

(defun main()
  (let ((index 0) (st1) (st2) (result 0))
    (setq index(get-dat))
    (loop for i from 1 to index
      do (setq st1(string (get-dat)))
      do (setq st2(string (get-dat)))
      do (setq result (distance st1 st2))
      do (format t "~a " result)
    )
  )
)

(main)

#|
$ cat DATA.lst | clisp frank1030.lsp

3 3 11 9 10 7 9 11 7 7 12 8
8 10 4 4 6 5 5 5 7 13 5 4 8
11 7 6 8 8 8 4 5 6 8 2 7
|#
