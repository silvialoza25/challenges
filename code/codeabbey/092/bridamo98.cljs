;; $ clj-kondo --lint bridamo98.cljs
;; linting took 29ms, errors: 0, warnings: 0

(ns bridamo98-092
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn swap [vect fst-elem snd-elem]
  (core/assoc vect snd-elem (vect fst-elem) fst-elem (vect snd-elem)))

(defn add-to-end [vect item]
  (conj vect item))

(defn drop-lst [vect]
  (vec (drop-last vect)))

(defn get-parent [index]
  (quot (- index 1) 2))

(defn get-left [index]
  (+ (* 2 index) 1))

(defn get-right [index]
  (+ (* 2 index) 2))

(defn fix-heap-from-top [heap cur-index]
  (let [left (get-left cur-index)
        right (get-right cur-index)
        pos-l-small (if (< left (count heap))
                      (if (< (heap left) (heap cur-index))
                        left cur-index)
                      cur-index)
        pos-r-small (if (< right (count heap))
                      (if (< (heap right) (heap pos-l-small))
                        right pos-l-small)
                      pos-l-small)]
    (if (not= cur-index pos-r-small)
      (fix-heap-from-top (swap heap cur-index pos-r-small) pos-r-small)
      heap)))

(defn remove-from-heap [heap]
  (let [not-fixed-heap (drop-lst (swap heap 0 (- (count heap) 1)))]
    (fix-heap-from-top not-fixed-heap 0)))

(defn fix-heap-from-buttom [heap cur-index]
  (let [parent (get-parent cur-index)]
    (if (and (not= cur-index 0) (> (heap parent) (heap cur-index)))
      (fix-heap-from-buttom (swap heap cur-index parent) parent)
      heap)))

(defn insert-to-heap [heap elem]
  (let [not-fixed-heap (add-to-end heap elem)]
    (fix-heap-from-buttom not-fixed-heap (- (count not-fixed-heap) 1))))

(defn make-actions-over-heap[size-input]
  (let [heap (vector)]
    (loop [i 0 cur-heap heap]
      (if (< i size-input)
        (let [action (core/read)
              nxt-heap (if (> action 0)
                         (insert-to-heap cur-heap action)
                         (remove-from-heap cur-heap))]
          (recur (+ i 1) nxt-heap))
        cur-heap))))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (doseq [item  (make-actions-over-heap size-input)]
      (print item ""))
    (println)))

(main)

;; $ cat DATA.lst | clj -M bridamo98.cljs
;; 6 9 14 13 17 21 25 27 16 26 22 29 24 30 31 34 32 20 19 28 33
