{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Data.Char
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  dat <- replicateM intSizeInput getLine
  let moves = map convert dat
  let board = ["**********","*rnbqkbnr*","*pppppppp*","*--------*",
               "*--------*","*--------*","*--------*","*PPPPPPPP*",
               "*RNBQKBNR*","**********"]
  let solution = solveAll board moves
  putStrLn (unwords solution)

solveAll :: [String] -> [[String]] -> [String]
solveAll board [] = []
solveAll board problems = results
  where
    iProblem = head problems
    results = show (solveSingleProblem board iProblem 1) :
              solveAll board (drop 1 problems)

solveSingleProblem :: [String] -> [String] -> Int -> Int
solveSingleProblem board [] cont = 0
solveSingleProblem board moves cont = res
  where
    iMove = head moves
    iIntMove = getMovement iMove
    newBoard = makeMovement board iIntMove
    res = if validateMove board iIntMove
            then solveSingleProblem newBoard (drop 1 moves) (cont + 1)
            else cont

validateMove :: [String] -> [Int] -> Bool
validateMove board move = isValid
  where
    ri = head move
    ci = move !! 1
    rf = move !! 2
    cf = move !! 3
    d1 = ri - rf
    d2 = ci - cf
    cInter = ri - (d1 `div` abs d1)
    pMoved = (board !! ri) !! ci
    emptyDest = ((board !! rf) !! cf) == '-'
    emptyInter = ((board !! cInter) !! cf) == '-'
    same = if ((board !! rf) !! cf) /= '-'
             then isUpper pMoved /= isUpper ((board !! rf) !! cf)
             else 1 == 0
    oneStepCond
      | d2 == 0 = emptyDest
      | abs d2 == 1 = same
      | otherwise = 1 == 0
    twoStepsCond = if d2 == 0
                    then emptyDest && emptyInter && (ri == 2 || ri == 7)
                  else 1 == 0
    isValid
      | pMoved == 'P' =
        if d1 == 1 then oneStepCond else
          if d1 == 2 then twoStepsCond else 1 == 0
      | pMoved == 'p' =
        if d1 == (-1) then oneStepCond else
          if d1 == (-2) then twoStepsCond else 1 == 0
      | otherwise = 1 == 1

convert :: String -> [String]
convert = words

getMovement :: String -> [Int]
getMovement mov = res
  where
    ri = 9 - digitToInt (mov !! 1)
    ci = fromEnum (head mov) - 96
    rf = 9 - digitToInt (mov !! 3)
    cf = fromEnum (mov !! 2) - 96
    res = [ri, ci, rf, cf]

makeMovement :: [String] -> [Int] -> [String]
makeMovement board mov = res
  where
    ri = head mov
    ci = mov !! 1
    rf = mov !! 2
    cf = mov !! 3
    newFstRow = changeRow (board !! ri) ci '-'
    pMoved = (board !! ri) !! ci
    newSndRow = changeRow (board !! rf) cf pMoved
    auxBoard = changeBoard board ri newFstRow
    res = changeBoard auxBoard rf newSndRow

changeRow :: String -> Int -> Char -> String
changeRow oldRow c value = newRow
  where
    fstPart = take c oldRow
    sndPart = drop (c + 1) oldRow
    newRow = fstPart ++ [value] ++ sndPart

changeBoard :: [String ]-> Int -> String -> [String]
changeBoard oldBoard r row = newBoard
  where
    fstPart = take r oldBoard
    sndPart = drop (r + 1) oldBoard
    newBoard = fstPart ++ [row] ++ sndPart

{-
$ cat DATA.lst | ./bridamo98
  0 3 4 5 1 4 5 0 3 0 3 6 5
-}
