// ponyc -b asalgad2

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail( array: Array[String] ): Array[String] =>
          array.slice(1, array.size())

        fun obj_fun( const_a: F64, const_b: F64, const_c: F64,
                     const_x: F64, const_y: F64 ): F64 =>
          var first_sub: F64 = ( const_x - const_a ).pow(2)
          var second_sub: F64 = ( const_y - const_b ).pow(2)

          var first_add: F64 = ( const_x + const_a ).pow(2)
          var second_add: F64 = ( const_y + const_b ).pow(2)

          first_sub + second_sub + (const_c * (- first_add - second_add ).exp())

        fun gradient( const_a: F64, const_b: F64, const_c: F64,
                      const_x: F64, const_y: F64, step: F64 ): Array[F64] val =>
          var const_f: F64=obj_fun(const_a,const_b,const_c,const_x,const_y)
          var const_fx:F64=obj_fun(const_a,const_b,const_c,const_x+step,const_y)
          var const_fy:F64=obj_fun(const_a,const_b,const_c,const_x,const_y+step)
          [ (const_fx - const_f)/step ; (const_fy - const_f)/step ]

        fun iterate( const_a: F64, const_b: F64, const_c: F64,
                     lines: Array[String] ) ? =>
          if lines.size() > 0 then
            var point: Array[String] = lines(0)?.split(" ")
            var const_x: F64 = point(0)?.f64()?
            var const_y: F64 = point(1)?.f64()?
            var grad: Array[F64] val = gradient(const_a, const_b, const_c,
                                                const_x, const_y, 0.0000000001)

            var angle: F64 = grad(1)?.atan2( grad(0)? ) * ( 180 / F64.pi() )
            env.out.write( (180 + angle).round().string() )
            env.out.write( " " )

            iterate( const_a, const_b, const_c, tail(lines) )?
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")
          var points: Array[String] = lines.slice(1, lines.size()-1)

          try
            var parameters: Array[String] = lines(0)?.split_by(" ")
            var const_a: F64 = parameters(1)?.f64()?
            var const_b: F64 = parameters(2)?.f64()?
            var const_c: F64 = parameters(3)?.f64()?
            iterate( const_a, const_b, const_c, points )?
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// 233 288 292 218 253 267 166 242 337 203 283 334 74 269
