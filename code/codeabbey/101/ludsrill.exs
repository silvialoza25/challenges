# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.08 seconds (0.02s to load, 0.06s running 55 checks on 1 file)
# 5 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule GradientCalculation do

  def main do
    [head | tail] = get_data()
    Enum.map(Enum.map(tail, &gradient_calculation(&1, head)),
      &IO.write("#{&1} "))
  end

  def to_float(list) do
    Enum.map(Enum.map(Enum.map(list, &Float.parse/1), &Tuple.to_list/1),
      &List.first/1)
  end

  def get_data do
    all_data = List.delete(String.split(IO.read(:stdio, :all), "\n"), "")
    Enum.map(Enum.map(all_data, &String.split(&1, " ")), &to_float/1)
  end

  def gradient_calculation([x, y], [_, a, b, c]) do
    dt = 0.000000001
    first_eq = :math.pow(x - a, 2) + :math.pow(y - b, 2) + c *
                 :math.exp(- :math.pow(x + a, 2) - :math.pow(y + b, 2))
    second_eq = :math.pow((x + dt) - a, 2) + :math.pow(y - b, 2) + c *
                  :math.exp(- :math.pow((x + dt) + a, 2) - :math.pow(y + b, 2))
    third_eq = :math.pow(x - a, 2) + :math.pow((y + dt) - b, 2) + c *
                 :math.exp(- :math.pow(x + a, 2) - :math.pow((y + dt) + b, 2))

    Kernel.round(:math.atan2(((third_eq - first_eq) / dt),
      ((second_eq - first_eq) / dt)) * 180.0 / :math.pi()) + 180
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e GradientCalculation.main
# 182 237 167 72 245 61 48 116 114 138 129 311 61
