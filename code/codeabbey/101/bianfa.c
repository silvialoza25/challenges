/*
linter:
$ cppcheck --error-exitcode=1 bianfa.c && \
splint -strict -internalglobs -modfilesys -boundsread bianfa.c

Checking bianfa.c ...
Splint 3.1.2 --- 20 Feb 2018

Finished checking --- no warnings

compilation:
$ gcc bianfa.c -o bianfa -lm
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.141592654

static double f(double x, double y, double a, double b, double c)
/*@modifies nothing@*/ {
 double op1 = pow(x - a, 2.0);
 double op2 = pow(y - b, 2.0);
 double op3 = pow(x + a, 2.0);
 double op4 = pow(y + b, 2.0);
 return op1 + op2 + c * exp(-op3  -op4 );
}

/*@relnull@*/static double * g( double x, double y,
double a, double b, double c)
/*@modifies nothing@*/ {
 double dt = 0.000000001, f0, f1, f2;
 double *points = (double *) malloc(sizeof(dt)*2);
 if(points == NULL) {
  return NULL;
 }

 f0 = f(x, y, a, b, c);
 f1 = f(x + dt, y, a, b, c);
 f2 = f(x, y + dt, a, b, c);

 points[0] = (f1 - f0) / dt;
 points[1] = (f2 - f0) / dt;

 return points;

}

static double calculateGradient(double x, double y,
double a, double b, double c)
/*@*/ {
 double *points, result;

 points = g(x, y, a, b, c);

 result = atan2(points[1], points[0]);
 result = result * 180.0/PI + 180.0 + 0.5;

 free(points);

 return result;

}

static int showResult(int i, double x, double y,
double a, double b, double c, int size) /*@*/ {
 int scan, result;
 scan = scanf("%lf %lf", &x, &y);

 result = (int) calculateGradient(x, y, a, b, c);

 printf("%d ", result);

 if(i < size) {
  return showResult(i+1, x, y, a, b, c, size);
 }else{
  printf("\n");
  return scan;
 }

}

int main(void) {
 int size, scan;
 double a, b, c, x = 0.0, y = 0.0;

 scan = scanf("%d %lf %lf %lf", &size, &a, &b, &c);

 return scan * showResult(0, x, y, a, b, c, size);
}

/*
$ cat DATA.lst | ./bianfa
197 134 179 98 135 121 124 136 194 207 98 143 156 146 143 147 144
*/
