-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import Float
import IO
import List

getData :: IO String
getData = do
  x <- getContents
  return x

strToF :: String -> Float
strToF s = read s :: Float

toDeg :: Float -> Float
toDeg angle = (angle * 180) / pi

atan2 :: Float -> Float -> Float
atan2 constY constX
  | constX > 0 = atan (constY / constX)
  | (constX < 0) && (constY > 0) = atan (constY / constX) + pi
  | (constX < 0) && (constY < 0) = atan (constY / constX) - pi

function :: Float -> Float -> Float -> Float -> Float -> Float
function constA constB constC constX constY = result
  where
    expOneNeg = (constX - constA) ^. 2
    expTwoNeg = (constY - constB) ^. 2
    expOnePos = (constX + constA) ^. 2
    expTwoPos = (constY + constB) ^. 2
    expE = -expOnePos - expTwoPos
    termThree = constC * (exp expE)
    result = expOneNeg + expTwoNeg + termThree

calGradient :: Float -> Float -> Float -> Float -> Float -> Float -> Int
calGradient constA constB constC constX constY offset = result
  where
    constF0 = function constA constB constC constX constY
    constFX = function constA constB constC (constX + offset) constY
    constFY = function constA constB constC constX (constY + offset)
    constGX = (constFX - constF0) / offset
    constGY = (constFY - constF0) / offset
    result = round (toDeg (atan2 constGY constGX)) + 180

getGradient :: String -> String -> Int
getGradient dataIn dataCoordinates = result
  where
    array = words dataIn
    coordinates = words dataCoordinates
    constA = strToF (array !! 1)
    constB = strToF (array !! 2)
    constC = strToF (array !! 3)
    constX = strToF (coordinates !! 0)
    constY = strToF (coordinates !! 1)
    offset = 0.0000000001
    result = calGradient constA constB constC constX constY offset

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    firstLine = head dataLines
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> show (getGradient firstLine x)) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 183 181 195 217 145 159 287 195 192 182 160 201 160 273 189 188
