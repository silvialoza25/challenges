#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun get-data (items-amount weights vals)
  (if (< items-amount 1)
    (values weights vals)
    (get-data (- items-amount 1) (append weights
      (list (read))) (append vals (list (read))))))

(defun change-weights(items-amount max-weight weights
  vals table-result item-index weight-index)
  (if (<= weight-index max-weight)
    (progn
      (if (or (= item-index 0) (= weight-index 0))
        (setf (aref table-result item-index weight-index) 0)
        (if (<= (nth (- item-index 1) weights) weight-index)
          (setf (aref table-result item-index weight-index)
            (max (+ (nth (- item-index 1) vals)
            (aref table-result (- item-index 1)
            (- weight-index (nth (- item-index 1) weights))))
            (aref table-result (- item-index 1) weight-index)))
          (setf (aref  table-result item-index weight-index)
            (aref table-result (- item-index 1) weight-index))))
      (change-weights items-amount max-weight weights vals
        table-result item-index (+ weight-index 1)))))

(defun snap-sack(items-amount max-weight weights
  vals table-result item-index weight-index)
  (if (> item-index items-amount)
    (aref table-result items-amount max-weight)
    (progn
      (change-weights items-amount max-weight
        weights vals table-result item-index 0)
      (snap-sack items-amount max-weight weights vals
        table-result (+ item-index 1) weight-index))))

(defun main ()
  (let ((items-amount (read)) (max-weight (read))
    (weights (list)) (vals (list)))
    (multiple-value-bind (weights vals) (get-data items-amount weights vals)
      (format t "~a~%"  (snap-sack items-amount max-weight weights vals
        (make-array (list (+ items-amount 1) (+ max-weight 1))
        :initial-element 0) 0 0)))))

(main)

#|
  cat DATA.lst | clisp bridamo98.lsp
  3862
|#
