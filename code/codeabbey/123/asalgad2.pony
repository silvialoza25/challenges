// ponyc -b asalgad2

use "collections"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun get_position( matrix: List[Array[I64]],
                          const_i: USize, const_j: USize ): I64 ? =>
          var array: Array[I64] = matrix(const_i)?
          var value: I64 = array(const_j)?
          value

        fun update_position( matrix: List[Array[I64]], const_i: USize,
                             const_j: USize, value: I64 ): I64 ? =>
          var array: Array[I64] = matrix(const_i)?
          array.update( const_j, value )?
          value

        fun create_matrix( matrix: List[Array[I64]], nrows: USize, ncols: USize,
                           init_value: I64 ): List[Array[I64]] =>
          if matrix.size() == nrows then
            matrix
          else
            var array: Array[I64] = Array[I64].init(init_value, ncols)
            matrix.push( array )
            create_matrix( matrix, nrows, ncols, init_value )
          end

        fun init_items( items: List[Array[I64]], items_values: Array[String],
                        const_i: USize ): List[Array[I64]]? =>
          if const_i == items.size() then
            items
          else
            var item: Array[String] = items_values(const_i)?.split_by(" ")
            var weight: I64 = item(0)?.i64()?
            var value: I64 = item(1)?.i64()?
            update_position( items, const_i, 0, weight )?
            update_position( items, const_i, 1, value )?
            init_items( items, items_values, const_i+1 )?
          end

        fun max( a: I64, b: I64 ): I64 =>
          if a > b then
            a
          else
            b
          end

        fun solve_knapsack( data: List[Array[I64]], items: List[Array[I64]],
                            const_w: I64 ): I64 ? =>
          var const_i: USize = items.size()
          if const_i == 0 then
            return 0
          end

          var prev_val: I64 = get_position( data, const_i-1, const_w.usize() )?
          if prev_val != -1 then
            return prev_val
          end

          var item: Array[I64] = items(const_i-1)?
          var weight: I64 = item(0)?
          var value: I64 = item(1)?

          var sub_items: List[Array[I64]] = items.take( const_i-1 )
          var val_no: I64 = solve_knapsack( data, sub_items, const_w )?
          if const_w < weight then
            return val_no
          end
          var val_yes: I64 = solve_knapsack(data, sub_items, const_w-weight)?
          max( val_no, val_yes+value )

        fun execute( data: List[Array[I64]], items: List[Array[I64]],
                     const_i: USize, const_w: USize ) ? =>
          if const_i < data.size() then
            if const_w < data(0)?.size() then
              var sub_items: List[Array[I64]] = items.take( const_i+1 )
              var result: I64 = solve_knapsack(data, sub_items, const_w.i64())?
              update_position( data, const_i, const_w, result )?
              execute( data, items, const_i, const_w+1 )?
            else
              execute( data, items, const_i+1, 0 )?
            end
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")
          var items_values: Array[String] = lines.slice(2, lines.size()-1)
          var data: List[Array[I64]] = List[Array[I64]]
          var items: List[Array[I64]] = List[Array[I64]]

          try
            var const_n: USize = lines(0)?.usize()?
            var const_w: USize = lines(1)?.usize()?

            create_matrix(data, const_n, const_w+1, -1)
            create_matrix(items, const_n, 2, 0)

            init_items( items, items_values, 0 )?

            execute( data, items, 0, 0 ) ?
            var result: I64 = get_position( data, const_n-1, const_w )?
            env.out.print( result.string() )
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// 4174
