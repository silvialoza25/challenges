;; $ clj-kondo --lint nickkar.cljs
;; linting took 71ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(defn value-at [vec fil col]
  (nth (nth vec fil) col))

(defn get-items ([input-size] (get-items input-size 1 []))
  ([input-size i col]
   (cond
     (> i input-size) col
     :else (get-items input-size (+ i 1) (conj col [(core/read) (core/read)]))
     )
   )
  )

(declare mdp)

(defn dp [i cap items]
  (cond
    (or (< i 0) (= cap 0)) 0
    (> (value-at items i 0) cap) (mdp (- i 1) cap items)
    :else (let [yes (+ (value-at items i 1)
                       (mdp (- i 1) (- cap (value-at items i 0)) items))
                no (mdp (- i 1) cap items)]
            (max yes no)
            )
    )
  )

(def mdp (memoize dp))

(defn main []
  (let [input-size (core/read)
        weigth (core/read)
        items (get-items input-size)]
    (println (dp (dec input-size) weigth items))
    )
  )

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 4732
