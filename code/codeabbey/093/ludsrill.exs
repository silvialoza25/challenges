# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.01s to load, 0.08s running 55 checks on 1 file)
# 9 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule StarvingPriorityQueue do

  def main do
    all_data = IO.read(:stdio, :all)
    try do
      data = hd(get_data(all_data))
      randoms = generator(data, [])
      IO.puts(Enum.sum(priority_queue(randoms, 0, [], [])))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def priority_queue([], _time, [], discomfort), do: discomfort
  def priority_queue([], time, arrives, discomfort) do
    if rem(time, 2) == 0 and time != 0 do
      sort_starvation = Enum.sort(arrives, &(hd(&1) >= hd(&2)))
      find_first = first_priority(arrives, sort_starvation)
      discomfort_value = hd(hd(sort_starvation)) *
        (time - List.last(find_first))
      new_arrive_list = List.delete(arrives, find_first)
      priority_queue([], time + 1, new_arrive_list, discomfort ++
        [discomfort_value])
    else
      priority_queue([], time + 1, arrives, discomfort)
    end
  end

  def priority_queue(randoms, time, arrives, discomfort) do
    if rem(time, 2) == 0 and time != 0 do
      sort_starvation = Enum.sort(arrives, &(hd(&1) >= hd(&2)))
      find_first = first_priority(arrives, sort_starvation)
      discomfort_value = hd(hd(sort_starvation)) *
        (time - List.last(find_first))
      new_arrive_list = List.delete(arrives, find_first)
      priority_queue(tl(randoms), time + 1, new_arrive_list ++
        [[hd(randoms), time]], discomfort ++ [discomfort_value])
    else
      priority_queue(tl(randoms), time + 1, arrives ++ [[hd(randoms), time]],
        discomfort)
    end
  end

  def first_priority(arrives, sort_starvation) do
    Enum.find(arrives, fn [degree, _time] ->
      hd(hd(sort_starvation)) == degree end)
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
    |> Enum.reject(fn item -> item == "" end)
    |> Enum.map(&String.split(&1, " "))
    |> Enum.map(&to_int/1)
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def generator([n, x], acc) do
    a = 445
    c = 700_001
    m = 2_097_152
    new_x = rem(a * x + c, m)
    if n != 0 do
      generator([n - 1, new_x], acc ++ [new_x])
    else
      Enum.map(acc, &rem(&1, 999) + 1)
    end
  end
end

StarvingPriorityQueue.main()

# cat DATA.lst | elixir ludsrill.exs
# 12015956110
