{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Data.Char
import Control.Monad

main = do
  dat <- getLine
  let intDat =  convert dat
  let n = head intDat
  let x0 = intDat !! 1
  let randNumbers = getRandNumbers 445 700001 2097152 n x0
  let index = [0,1..n]
  let starvationDegrees = map getStarvationDegree randNumbers
  let numAndIndex = getNumAndIndex starvationDegrees index
  let totalStarvation = getTotalStarvation (sortBy sortGT numAndIndex) 0 0
  print (sum totalStarvation)

sortGT (a1, b1) (a2, b2)
  | a1 < a2 = GT
  | a1 > a2 = LT
  | a1 == a2 = compare b1 b2

getNumAndIndex :: [Int] -> [Int] -> [(Int, Int)]
getNumAndIndex arr1 [] = []
getNumAndIndex [] arr2 = []
getNumAndIndex (a:arr1) (b:arr2) = (a, b) : getNumAndIndex arr1 arr2

getTotalStarvation :: [(Int, Int)] -> Int -> Int -> [Int]
getTotalStarvation [] t total = []
getTotalStarvation degrees t totalStervation = total
  where
    nextDegrees = if t `mod` 2 == 0 && t /= 0
                    then drop 1 degrees
                    else degrees
    subTotal = if t `mod` 2 == 0 && t /= 0
                 then fst (head degrees) * t - snd (head degrees)
                 else 0
    total =  subTotal : getTotalStarvation nextDegrees (t + 1) subTotal

getStarvationDegree :: Int -> Int
getStarvationDegree rand = (rand `mod` 999) + 1

convert :: String -> [Int]
convert dat = result
  where
    aux = words dat
    result = map read aux :: [Int]

getRandNumbers :: Int -> Int -> Int -> Int -> Int -> [Int]
getRandNumbers a c m 0 xCur = []
getRandNumbers a c m n xCur = res
  where
    xNext = (a * xCur + c) `mod` m
    res = xNext : getRandNumbers a c m (n - 1) xNext
{-
$ cat DATA.lst | ./bridamo98
  9399184360
-}
