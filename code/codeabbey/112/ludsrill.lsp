#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun search-differents (data)
  (let ((aux ())
        (positions ()))
    (loop for i in data do
      (when (string/= (first i) aux)
        (setq aux (first i))
        (setq positions (append positions (list (first i))))))
    (return-from search-differents positions)))

(defun diff-by-num(item)
  (let ((diff ()))
    (loop for i in item do
      (when (equal (find i diff) NIL)
        (setq diff (append diff (list i)))))

    (if (/= (- (length item) 1) (length diff))
      (return-from diff-by-num '(""))
      (return-from diff-by-num diff))))

(defun latin-dot (matrix org-matrix)

  (let ((aux-matrix ())
        (routes ())
        (aux-first-matrix)
        (aux-second-matrix)
        (row)
        (way))

    (loop for i from 0 to (- (length matrix) 1) do
      (setq row ())
      (loop for j from 0 to (- (length (first org-matrix)) 1) do
        (loop for k from 0 to (- (length (first matrix)) 1) do
          (setq aux-first-matrix (elt (elt matrix i) k))
          (setq aux-second-matrix (first (elt (elt org-matrix k) j)))

          (dolist (item aux-first-matrix)
            (if (or (equal item '("")) (equal aux-second-matrix '("")))
              NIL
              (setq routes (append (list (append item aux-second-matrix)))))))

        (setq way ())
        (if (equal routes NIL)
          (setq way '(("")))
          (dolist (item routes)
            (setq way (append way (list (diff-by-num item))))))

        (when (> (length way) 1)
          (if (= (count '("") way :test #'equal) (length way))
            (setq way (list '("")))
            (setq way (delete '("") way :test #'equal))))

        (setq row (append row (list way)))
        (setq routes ()))
      (setq aux-matrix (append aux-matrix (list row))))
    (return-from latin-dot aux-matrix)))

(defun filter-data ()
  (let ((data)
        (filtered-data))
    (setq data (cdr (read-data)))
    (setq filtered-data ())
    (dolist (item data)
      (if (= (third item) 6)
        (setq filtered-data (append filtered-data (list item)))))
    (return-from filter-data filtered-data)))

(defun replace-city-to-number (positions data)
  (loop for i in data
    collect (list (position (first i) positions)
                  (position (second i) positions))))

(defun initialize-matrix (graph)
  (let ((row ())
        (matrix ()))
    (loop for i from 0 to 99 do
      (loop for j from 0 to 99 do
        (setq row (append row (list '((""))))))
      (setq matrix (append matrix (list row)))
      (setq row ()))
    (dolist (item graph)
      (setf (elt (elt matrix (first item)) (second item)) (list item)))
    (return-from initialize-matrix matrix)))

(defun travelling-salesman-inverted ()
  (let ((data)
        (graph)
        (matrix)
        (org-matrix)
        (output)
        (positions))
    (setq data (filter-data))
    (setq positions (search-differents data))
    (setq graph (replace-city-to-number positions data))

    (setq org-matrix (initialize-matrix graph))
    (setq matrix org-matrix)

    (loop for i from 0 to 93 do
      (print i)
      (setq matrix (latin-dot matrix org-matrix)))

    (dolist (item matrix)
      (dolist (item2 item)
        (setq output ())
        (if (equal item2 '(("")))
          NIL
          (when T
            (dolist (item3 (first item2))
              (setq output (append output (list (elt positions item3)))))
            (print matrix)
            (print output)))))))

(travelling-salesman-inverted)

#|
cat DATA.lst | clisp ludsrill.lsp
XU GO QI ZO WU SU ZI ZU XE TE WA WO WE XI XA QA VI KE VO VE RE PO TA SO WI SA
PI QE VA PE TI RU TU JI XO ZE ZA JO MI KU SI RA VU NE NI DA KI PA NO LA QU RO
HO BO QO TO NA BU RI CE SE FU JA FI HE BA MU LI JE CI MA LU MO BI CU BE FA FO
LO DU GA DI GI ME KO NU DE LE FE JU CA HU KA HA CO DO
|#
