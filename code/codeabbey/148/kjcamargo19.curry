-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import Float
import IO
import Integer
import List
import Read

getData :: IO String
getData = do
  content <- getContents
  return content

floor :: Float -> Int
floor number = intPart
  where
    sep = split (=='.') (show number)
    intPart = readInt (sep !! 0)

modExpo :: Int -> Int -> Int -> Int
modExpo base expo nModulo = result
  where
    result = mod (pow base expo) nModulo

getModExpo :: Int -> Int -> Int -> Int
getModExpo base expo nMod
  | expo == 0 = 1
  | otherwise = mod ((mod base nMod) * modExpo base (expo-1) nMod) nMod
  | even expo
  = mod ((modExpo base (newExpo) nMod) * (modExpo base (newExpo) nMod)) nMod
  where
    newExpo = truncate ((i2f expo) / 2)

getE :: Int -> Int -> Int -> Int -> Int -> Int
getE constN constP constPE constE rmodExpo
  | (constE > constN) || (rmodExpo == constPE) = constE - 1
  | otherwise = getE constN constP constPE (constE + 1) value
  where
    value = getModExpo constP constE constN

decodeMsn :: Int -> Int -> String -> String
decodeMsn constM index initMsn =
  let
    letters = "abcdefghijklmnopqrstuvwxyz"
    message = initMsn ++ [letters !! (mod constM 31)]
    newConstM = floor ((i2f constM) / 31)
  in
    if index > 2 then reverse message
    else decodeMsn newConstM (index + 1) message

getSendWords :: String -> String -> String
getSendWords dataAlice dataBob = decodedWord
  where
    AData = words dataAlice
    BData = words dataBob
    constN = readInt (AData !! 0)
    constP = readInt (AData !! 1)
    constPE = readInt (AData !! 2)
    constPK = readInt (BData !! 0)
    constC = readInt (BData !! 1)
    initPoint = truncate ((i2f constN) * 0.0798)
    constE = getE constN constP constPE initPoint 0
    invPKE = getModExpo constPK (constN - 1 - constE) constN
    constM = mod (constC * invPKE) constN
    decodedWord = decodeMsn constM 0 ""

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    publicInfo = usefulData !! 0
    encryptedWords = tail usefulData
    finalResult = map (\x -> getSendWords publicInfo x) encryptedWords
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- fate ring lock acre john calm soil
