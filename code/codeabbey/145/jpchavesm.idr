{-
  $ idris2 jpchavesm.idr -o jpchavesm
-}

module Main
import Data.Maybe
import Data.Strings

str2int : String -> Integer
str2int elem = fromMaybe 0 $ (parseInteger {a=Integer} elem)

calcmodPow : Integer -> Integer -> Integer -> Integer -> Integer -> Integer
calcmodPow _ _ 1 _ _ = 0
calcmodPow base exponent modulus exp' aprox =
  let aprox' = mod (aprox * base) modulus in
  if exp' < (exponent-1)
    then calcmodPow base exponent modulus (exp'+1) aprox'
    else aprox'

partial modPow : List Integer -> Integer
modPow [base, exponent, modulus] = calcmodPow base exponent modulus 0 1

processFile : IO(List String)
processFile = do
  row <- getLine
  if row == ""
    then pure []
    else do
      nextRow <- processFile
      pure (row :: nextRow)

partial main : IO ()
main = do
  size <- getLine
  strInput <- processFile
  let
    testData = map (map str2int) $ map words strInput
    result = map show $ map modPow testData
  putStrLn (unwords result)

{-
  $ cat DATA.lst | ./build/exec/jpchavesm
  232536700 68788580 18589223 45933761 222324468 107988977 154125816 99895017
  167701060 184537476 114722476 51688950 291189099 21644310 150347983 171251700
  120695821 50091414 213053521 69529357
-}
