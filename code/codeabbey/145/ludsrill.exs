# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.01s to load, 0.08s running 55 checks on 1 file)
# 7 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule ModularExponentiation do
  def main do
    all_data = IO.read(:stdio, :all)
    try do
      get_data(all_data)
        |> Enum.map(&modular_pow(&1, 0))
        |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def modular_pow([_base, 0, _modulus], _aux), do: 1
  def modular_pow([0, _exponent, _modulus], _aux), do: 0
  def modular_pow([base, exponent, modulus], aux) do
    if rem(exponent, 2) == 0 do
      aux = modular_pow([base, div(exponent, 2), modulus], aux)
      solve = rem((aux * aux), modulus)
      rem((solve + modulus), modulus)
    else
      aux = rem(base, modulus)
      solve = (aux * modular_pow([base, exponent - 1, modulus], aux))
      rem((solve + modulus), modulus)
    end
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
    |> Enum.reject(fn item -> item == "" end)
    |> Enum.map(&String.split(&1, " "))
    |> Enum.map(&to_int/1)
    |> tl()
  end
end

ModularExponentiation.main()

# cat DATA.lst | elixir ludsrill.exs
# 852132 22341884 175970467 60231713 117928599 73848811 171438945 321209992
# 45906962 91293249 235255682 90864186 69115508 100623170 20176701 128435654
# 17080072 275050387 58263188 108056097 719801 43745713 178683808
