-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import Integer
import IO
import List
import Read

getData :: IO String
getData = do
  content <- getContents
  return content

getModExpo :: Int -> Int -> Int -> Int
getModExpo base expo nMod
  | expo == 0 = 1
  | even expo =
    let
      termA = getModExpo base (div expo 2) nMod
      prevAns = rem (termA * termA) nMod
    in
      rem (prevAns + nMod) nMod
  | otherwise =
    let
      termA = rem base nMod
      prevAns = termA * (getModExpo base (expo - 1) nMod)
    in
      rem (prevAns + nMod) nMod

getSolution :: String -> Int
getSolution dataIn  = result
  where
    numbers = map readInt (words dataIn)
    base = numbers !! 0
    exponente = numbers !! 1
    moduloM = numbers !! 2
    result = getModExpo base exponente moduloM

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> show (getSolution x)) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- 338361246 62164447 192170116 116106144 167924257 30993749 126974901 2116468
-- 290061851 115589949 91644800 246293961 179655217 194345845 191429694
-- 39848321 118779238 39532367 68192890 50287539 278388613 115978344 86221496
-- 250829660 165713434 333031861 22758950 111290758 62053806
