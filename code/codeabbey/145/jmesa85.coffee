###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #145: Modular Exponentiation - Coffeescript
# Based on "Fast modular exponentiation" article on Khan Academy

# Check Least Significant Bit
checkLsb = (base, exp, modulus, result) ->
  if exp % BigInt(2) == BigInt(1)
    # Use modular multiplication properties for this power of 2
    (result * base) % modulus
  else
    result

modExp = (base, exp, modulus, result) ->
  # Base case
  if exp <= 0
    return result
  # Recursive case
  # Calculate mod for the next power of 2
  partial = checkLsb(base, exp, modulus, result)
  modExp((base * base) % modulus, exp / BigInt(2), modulus, partial)

processTestCase = (testCasesStr) ->
  [base, exp, modulus] = testCasesStr.split(' ').map(BigInt)
  baseExpOne = base % modulus # This is exp 1
  modExp(baseExpOne, exp, modulus, BigInt(1)).toString()

main = ->
  # Read STDIN
  data = require('fs').readFileSync(0).toString().trim().split('\n')
  # Discard number of test cases at index 0
  testCasesAsStrings = data.slice(1)
  # Process each test case
  results = testCasesAsStrings.map(processTestCase)
  # Print results
  console.log(results.join(' '))

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
232536700 68788580 18589223 45933761 222324468 107988977 154125816
99895017 167701060 184537476 114722476 51688950 291189099 21644310
150347983 171251700 120695821 50091414 213053521 69529357
###
