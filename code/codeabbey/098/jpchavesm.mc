/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, float, math, int, char.

:- func deg2rad(int) = float.
deg2rad(Deg) = float(Deg) * pi / 180.0.

:- func azi2deg(float) = int.
azi2deg(Azimuth) = round_to_int(450.0 - Azimuth) mod 360.

:- pred computeStep(float, float, float, float, float, float).
:- mode computeStep(in, in, in, in, out, out) is det.
computeStep(Distance, Azimuth, PrevX, PrevY, NewX, NewY) :-
(
  Theta = deg2rad(azi2deg(Azimuth)),
  NewX = PrevX + (Distance * cos(Theta)),
  NewY = PrevY + (Distance * sin(Theta))
).

:- pred findTreasure(list(string), float, float, string).
:- mode findTreasure(in, in, in, out) is det.
findTreasure([], FinalX, FinalY, FinalStr) :-
(
  IntX = round_to_int(FinalX),
  IntY = round_to_int(FinalY),
  FinalStr = from_int(IntX) ++ " " ++ from_int(IntY)
).
findTreasure([Line | Tail], PrevX, PrevY, FinalStr) :-
(
  words_separator(char.is_whitespace, Line) = Instruction,
  det_to_float(det_index1(Instruction, 2)) = Distance,
  det_to_float(det_index1(Instruction, 6)) = Azimuth,
  computeStep(Distance, Azimuth, PrevX, PrevY, NewX, NewY),
  findTreasure(Tail, NewX, NewY, FinalStr)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1,FileContents, InputList),
    det_split_last(InputList, Instructions, _),
    findTreasure(Instructions, 0.0, 0.0, ResultStr),
    io.print_line(ResultStr, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  261 2850
*/
