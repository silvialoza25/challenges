# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.01s to load, 0.08s running 55 checks on 1 file)
# 7 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule AzimuthAtTreasureIsland do
  def main do
    all_data = IO.read(:stdio, :all)

    try do
      get_data(all_data)
        |> Enum.map(&get_cordinates/1)
        |> solution()
        |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_data(all_data) do
    split_data = String.split(all_data, "\n")
      split_data
      |> Enum.map(&String.split(&1, " "))
      |> Enum.map(&Enum.reject(&1, fn x -> x == "" or String.match?(x,
          ~r/^[[:alpha:], |\!]+$/) end))
      |> Enum.reject(&Enum.empty?/1)
      |> Enum.map(&to_int/1)
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def get_angle(azimuth) do
    cond do
      azimuth >= 0 and azimuth <= 90 ->
        (90 - azimuth) * :math.pi() / 180
      azimuth > 90 and azimuth <= 180 ->
        (90 - (180 - azimuth)) * :math.pi() / 180
      azimuth > 180 and azimuth <= 270 ->
        (90 - (270 - azimuth)) * :math.pi() / 180
      azimuth > 270 and azimuth <= 360 ->
        (90 - (360 - azimuth)) * :math.pi() / 180
    end
  end

  def get_cordinates([distance, azimuth]) do
    x = :math.cos(get_angle(azimuth)) * distance
    y = :math.sin(get_angle(azimuth)) * distance

    cond do
      azimuth >= 0 and azimuth <= 90 -> [x, y]
      azimuth > 90 and azimuth <= 180 -> [x, -y]
      azimuth > 180 and azimuth <= 270 -> [-x, -y]
      azimuth > 270 and azimuth <= 360 -> [-x, y]
    end
  end

  def solution(list) do
    Enum.reduce(list, [0, 0], fn [x, y], acc ->
      [hd(acc) + x, List.last(acc) + y] end)
      |> Enum.map(&Kernel.round/1)
  end
end

AzimuthAtTreasureIsland.main()

# cat DATA.lst | elixir ludsrill.exs
# 261 2850
