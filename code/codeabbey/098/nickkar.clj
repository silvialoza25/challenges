;; $clj-kondo --lint nickkar.clj
;; linting took 38ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.string :as str]))

;; Reads the input and returns a vector with the information
(defn read-input ([] (read-input []))
  ([out]
    (let [msg (read-line)]
      (cond
        (= msg "Dig here!") out
        (= msg "Stand at the pole with the plaque START")
        (read-input out)
        :else
        (let [[fst scnd] (str/split msg #"by")
             dist (Integer. (re-find #"\d+" fst))
             ang (Integer. (re-find #"\d+" scnd))]
         (read-input (conj out [dist ang])))))))

;; transforms degrees to radians
(defn to-rads [deg]
  (* deg (/ Math/PI 180)))

;; calculates the move given in polar coordinates in cartesian coordinates
;; for every move and returns the final value
(defn move ([instr] (move instr 0 0 0))
  ([instr posx posy i]
    (cond
      (>= i (count instr)) [(Math/round posy) (Math/round posx)]
      :else
      (let [[dist ang] (nth instr i)
           rad-angle (to-rads ang)
           newx (+ posx (* dist (Math/cos rad-angle)))
           newy (+ posy (* dist (Math/sin rad-angle)))]
        (move instr newx newy (inc i))))))

;; driver code
(defn main []
  (let [input (read-input)]
    (apply println (move input))))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; 261 2850
