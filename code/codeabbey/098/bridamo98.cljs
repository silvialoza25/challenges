;; $ clj-kondo --lint bridamo98.cljs
;; linting took 33ms, errors: 0, warnings: 0

(ns bridamo98-027
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn dtr [deg]
  (/ (* (Math/PI) deg) 180))

(defn find-treasure-coordinates []
  (loop [x 0 y 0]
    (let [line (str/split (core/read-line) #" ")]
      (if (not (= (compare (line 0) "Stand") 0))
        (if (= (compare (line 0) "go") 0)
          (recur (+ x (* (edn/read-string (line 1))
          (Math/sin (dtr (edn/read-string (line 5))))))
          (+ y (* (edn/read-string (line 1))
          (Math/cos (dtr (edn/read-string (line 5)))))))
          [(int x) (int y)])
        (recur x y)))))

(defn main []
  (doseq [item (find-treasure-coordinates)]
    (print item ""))
    (println))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 603 3286
