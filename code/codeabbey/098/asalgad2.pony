// ponyc -b asalgad2

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail( array: Array[String] ): Array[String] =>
          array.slice(1, array.size())

        fun tail_chars( array: Array[U8] ): Array[U8] =>
          array.slice(1, array.size())

        fun print_array( array: Array[String] ) ? =>
          if array.size() >= 1 then
            env.out.write( array(0)?.string() )
            env.out.write( " " )
            print_array( tail( array ) )?
          end

        fun sum_vector(const_a: Array[F64], const_b: Array[F64]): Array[F64]? =>
          [ const_a(0)? + const_b(0)?; const_a(1)? + const_b(1)? ]

        fun deg_to_rad( angle: F64 ): F64 =>
          angle * ( 3.14159265358979323846 / 180 )

        fun extract_coord( line: String ): Array[F64] ? =>
          var words: Array[String] = line.split(" ")
          var distance: F64 = words(1)?.f64()?
          var angle: F64 = words(5)?.f64()?
          var radians: F64 = deg_to_rad(angle)
          [distance * radians.cos(); distance * radians.sin() ]

        fun iterate( lines: Array[String], start: Array[F64] ) ? =>
          if lines.size() > 0 then
            var line: String = lines(0)?
            var coord: Array[F64] = extract_coord( line )?
            var new_pos: Array[F64] = sum_vector( coord, start )?
            iterate( tail(lines), new_pos )?
          else
            env.out.write( start(1)?.round().string() )
            env.out.write( " " )
            env.out.print( start(0)?.round().string() )
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")
          var useful_lines: Array[String] = lines.slice(1, lines.size()-2)

          try
            iterate( useful_lines, [0;0] )?
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// 163 3189
