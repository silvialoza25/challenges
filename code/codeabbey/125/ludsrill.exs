# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.02s to load, 0.1s running 55 checks on 1 file)
# 9 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule Bogosort do
  def main do
    all_data = IO.read(:stdio, :all)

    try do
      split_data = String.split(all_data, "\n")
      List.delete_at(split_data, 1 +
        String.to_integer(hd(String.split(Enum.at(split_data, 0), " "))))
        |> Enum.map(&String.split(&1, " "))
        |> Enum.map(&to_int/1)
        |> tl()
        |> Enum.map(&shuffling(0, 918_255, &1, Enum.sort(&1), 0))
        |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def complete_number(value, measure) do
    if String.length(value) < measure do
      complete_number("0" <> value, measure)
    else value
    end
  end

  def randomizer(initial_value) do
    value = complete_number(Integer.to_string(initial_value), 6)
    first_value = String.to_integer(first_part(value) <> last_part(value))
    second_value = String.to_integer(last_part(value) <> first_part(value))

    Integer.to_string(first_value * second_value)
      |> complete_number(12)
      |> String.slice(3..8)
      |> String.to_integer()
  end

  def shuffling(pointer, random, array, sorted_array, shuffle_count) do
    cond do
      array != sorted_array and shuffle_count < 5040 ->
        [new_array, random] = cycle(pointer, random, array, sorted_array,
                               shuffle_count)
        shuffling(pointer, random, new_array, sorted_array, shuffle_count + 1)

      shuffle_count == 5040 -> -1
      true -> shuffle_count
    end
  end

  def cycle(pointer, random, array, sorted_array, shuffle_count) do
    if pointer < 7 do
      r = randomizer(random)
      first = Enum.at(array, rem(r, 7))
      second = Enum.at(array, pointer)
      new_array = List.replace_at(array, rem(r, 7), second)
                    |> List.replace_at(pointer, first)

      cycle(pointer + 1, r, new_array, sorted_array, shuffle_count)
    else
      [array, random]
    end
  end

  def first_part(value) do
    String.slice(value, 0..2)
  end

  def last_part(value) do
    String.slice(value, 3..7)
  end
end

Bogosort.main()

# cat DATA.lst | elixir ludsrill.exs
# 2477 3668 -1 268 -1 -1 -1 645 -1 710
