# $ mypy --strict juan6630.py
# $ Success: no issues found in 1 source file

from typing import List, Set


def neumanns() -> None:
    count: int = int(input())
    numbers: List[int] = [int(x) for x in input().split()]
    seen: Set[int] = set()
    for i in range(count):
        seen.clear()
        iterations: int = 0
        seen.add(int(numbers[i]))
        value: int = int(numbers[i])
        while True:
            s_2: int = value ** 2
            s_2_str: str = str(s_2)
            # add leading zeros if needed
            if len(s_2_str) < 8:
                s_2_str = s_2_str.zfill(8)
            s_3: int = int((int(s_2_str) / 100) % 10000)
            iterations += 1
            value = s_3
            if s_3 not in seen:
                seen.add(s_3)
            else:
                break
        print(iterations)


neumanns()

# $ cat DATA.lst | python3 juan6630.py
# 10
# 1054 2808 3017 8459 6068 9348 8781 8096 4481 3453
