###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #024: Neumann's Random Generator - Coffescript

# Generate pseudorandom number
generateRandomNumber = (value) ->
  parseInt((value * value) / 100) % 10000

# Returns the number of iterations with such initial value to come to the loop
findLoopNumber = (currValue, prevValues) ->
  nextValue = generateRandomNumber(currValue)
  # Base case
  if prevValues.indexOf(nextValue) >= 0
    1
  else
    # Recursive case
    findLoopNumber(nextValue, prevValues.concat([nextValue])) + 1

processTestCase = (initialValue) ->
  findLoopNumber(initialValue, [initialValue])

main = ->
  # Read data from STDIN
  data = require('fs').readFileSync(0).toString().trim().split('\n')
  # Get the test cases
  testCasesAsStrings = data.slice(1)[0].split(' ')
  # Process each test case
  results = testCasesAsStrings.map(Number).map(processTestCase)
  # Print results
  console.log(results.join(' '))

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
100 100 99 101 106 100 99 106 106 109 102 97 111
###
