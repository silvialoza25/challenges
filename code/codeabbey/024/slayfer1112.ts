/*
$ eslint slayfer1112.ts && prettier --check slayfer1112.ts
$ tsc \
  --strict \
  --noImplicitAny \
  --noImplicitThis \
  --noUnusedLocals \
  --noUnusedParameters \
  --noImplicitReturns \
  --noFallthroughCasesInSwitch \
  slayfer1112.ts
*/

function checkDigits(value: string): string {
  const requiredDigits = 8;
  if (value.length < requiredDigits) {
    return checkDigits(`0${value}`);
  }
  return value;
}

function neumann(seed: number, arr: number[], counter: number): number {
  const seedSqrt: number = seed * seed;
  const value: string = checkDigits(`${seedSqrt}`);
  const trunc = Number(value.slice(2, 2 + 2 + 2));
  if (arr.indexOf(trunc) < 0) {
    return neumann(trunc, arr.concat(trunc), counter + 1);
  }
  return counter;
}

function solution(entry: string): number {
  const seed = Number(entry);
  const val: number = neumann(seed, [seed], 1);
  process.stdout.write(`${val} `);
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(1, entryArray.length);
    dat.map((value) => value.split(' ').map((val) => solution(val)));
    process.stdout.write('\n');
    return 0;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
106 111 97 101 98 98 105 98 110 100 101
*/
