;; $ clj-kondo --lint nickkar.cljs
;; linting took 69ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(defn pow-n-truncate [a]
  (int (mod (Math/floor (double (/ (Math/pow a 2) 100))) 10000))
)

(defn counter [i record num]
  (cond
    (and (some #{num} record) (> i 0)) i
    :else (counter (+ i 1) (conj record num ) (pow-n-truncate num)))
  )

(defn startcounter [num]
  (counter 0 [] num))

(defn itera [input-size]
  (loop [x 1]
    (when (< x (+ input-size 1))
      (println (startcounter (int (core/read))))
      (recur (+ x 1))))
  )


(defn main []
  (let [input-size (core/read)]
    (itera input-size)))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 102 100 105 102 105 110 104 101 100 98 111 102 102 104
