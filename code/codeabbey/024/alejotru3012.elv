# elvish -compileonly alejotru3012.elv

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1]
}

fn random_generator [array index cur]{
  if (has-value $array $cur) {
    echo $index
  } else {
    step_1 = (echo (math:pow $cur 2))
    if (< (count $step_1) 8) {
      amount = (- 8 (count $step_1))
      step_1 = (str:join '' [(repeat $amount 0) $step_1])
    }
    step_2 = $step_1[2..-2]
    random_generator [$@array $cur] (+ $index 1) $step_2
  }
}

fn calculate [data]{
  each [value]{
    random_generator [] 0 $value
  } $data
}

echo (str:join ' ' [(calculate (input_data))])

# cat DATA.lst | elvish alejotru3012.elv
# 110 107 105 106 98 104 102 100 107 100
