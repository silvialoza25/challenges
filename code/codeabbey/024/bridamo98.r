# $ lintr::lint('bridamo98.r')

format_input <- function(inp) {
  formatted_input <- unlist(lapply
    ((unlist(strsplit(inp, " "))), as.integer))
  return(formatted_input)
}

find_nxt_rand_number <- function(number) {
  large_num <- as.character(number * number)
  size_pad <- 8 - nchar(large_num)
  pad <- paste(replicate(size_pad, "0"), collapse = "")
  pad_num <- paste(pad, large_num, sep = "")
  res <- as.integer(substr(pad_num, 3, 6))
  return(res)
}

find_index_for_loop <- function(number, rand_numbers, i=0) {
  if ((number %in% rand_numbers) & (i > 0)) {
    return(i)
  }
  else {
    nxt_rand_number <- find_nxt_rand_number(number)
    find_index_for_loop(nxt_rand_number, (append
      (rand_numbers, number)), i + 1)
  }
}

solve_all <- function(inp, res = c(), i=1) {
  if (i > length(inp)) {
    return(res)
  }
  else {
    index_for_loop <-
      find_index_for_loop(inp[i], (append(c(), inp[i])))
    solve_all(inp, (append(res, index_for_loop)), i + 1)
  }
}

main <- function() {
  input <- readLines("stdin")
  formatted_input <- format_input(input[2])
  result <- solve_all(formatted_input)
  cat(result, "\n")
}

main()

# $ cat DATA.lst | Rscript bridamo98.r
# 103 100 98 110 109 104 101 101 100 99 110
