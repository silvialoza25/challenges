#!/usr/bin/php
<?php
/*
$ ./vendor/bin/phplint edwinhaq.php --no-configuration --no-cache
phplint 2.0.2 by overtrue and contributors.

No config file loaded.

.

Time: < 1 sec   Memory: 2.0 MiB Cache: No

OK! (Files: 1, Success: 1)
*/

function get_input_data() {
  fgets(STDIN);
  return explode(' ', trim(fgets(STDIN)));
}

function neumanns_random_generator($initial, $history = []) {
  if (count($history) == 0) {
    $myhistory = [$initial];
  } else {
    $myhistory = $history;
  }
  $neumanns = floor(($initial * $initial) / 100) % 10000;
  if (in_array($neumanns, $history)) {
    return count($history);
  }
  return neumanns_random_generator(
    $neumanns,
    array_merge($myhistory, [$neumanns]),
  );
}

function main() {
  foreach (get_input_data() as $initial) {
    print neumanns_random_generator($initial) . ' ';
  }
}

main();

/*
$ cat ./DATA.lst | ./edwinhaq.php
102 100 105 102 105 110 104 101 100 98 111 102 102 104
*/


?>
