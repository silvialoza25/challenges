;; $ clj-kondo --lint bridamo98.clj
;; linting took 79ms, errors: 0, warnings: 0

(ns bridamo98-054
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn pythagorean [s]
 (let [sqrt (Math/sqrt s)]
   (loop [n 1 res 0]
     (if (or (< n sqrt) (= n sqrt))
       (recur (+ n 1)
         (loop [m (+ n 1) n2 n res2 res]
           (if (or (< m sqrt) (= m sqrt))
             (let [a (- (* m m) (* n2 n2)) b (* 2 m n2)
             c (+ (* m m) (* n2 n2))
             nxt-res (if (= (+ a b c) s) (* c c) res2)]
               (recur (+ m 1) n2 nxt-res)) res2))) res ))))

(defn solve-all[size-input]
  (loop [i 0 result ""]
    (if (< i size-input)
      (recur (+ i 1)
        (str result (pythagorean (edn/read-string
        (core/read-line))) " "))
        result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))]
    (println (solve-all size-input))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 43027525654369 75972229602025 64992506374849 49987855303681
;; 47289346725625 37827580070569 84936960015625
