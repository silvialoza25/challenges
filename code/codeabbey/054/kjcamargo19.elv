# elvish -compileonly kjcamargo.elv

use math
use str

#This function read all values from DATA.lst file
fn read_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

#This function find pythagorean triplets
fn pythagoreanTriples_finder [data]{
  each [sum]{
    var from = (math:floor (* $sum[0] (- (math:pow 2 0.5) 1)))
    var to = (+ (math:ceil (/ $sum[0] 2)) 1)
    range $from $to | each [hypotenuse]{
      var term1_eq = (math:pow $hypotenuse 2)
      var term2_eq = (* 2 (* $hypotenuse $sum[0]))
      var term3_eq = (math:pow $sum[0] 2)
      var equation = (+ $term1_eq (- $term2_eq $term3_eq))
      if (> $equation 0) {
        if (== (math:pow $equation 0.5) (math:ceil (math:pow $equation 0.5))) {
          var leg_b = (+ (- $sum[0] $hypotenuse) (/ $equation 2))
          var leg_a = (- $sum[0] (- $leg_b $hypotenuse))
          echo (math:pow $hypotenuse 2)
          break
        }
      }
    }
  } $data
}

# Output variable
echo (pythagoreanTriples_finder (read_data))

# cat DATA.lst | elvish kjcamargo19.elv
# 43897316505025 75903212940025 83115768736225 54340324384921 37827580070569
# 40613102411281 88564455335161 80234029449025 71773242104281
