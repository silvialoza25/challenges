;; clj-kondo --lint ludsrill.cljs
;; linting took 37ms, errors: 0, warnings: 0

(ns ludsrill.054
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
          (cons (str/split input-string #" ") (get-data))
          nil))))

(defn gcd [x y]
  (if (not= (rem x y) 0)
    (recur y (rem x y))
    [y]))

(defn major [k m]
  (if (> k m)
    (gcd k m)
    (gcd m k)))

(defn logic [k m s]
  (let [d (/ (/ s 2) (* k m))
        n (- k m)
        c (* d (+ (* m m) (* n n)))]

  (if (and (< k (* 2 m)) (<= k (/ s (* 2 m))))
    (do
      (when (and (= (rem (/ s (* 2 m)) k) 0.0) (= (first (major k m)) 1))
        (print (core/format "%.0f " (* c c))))
      (logic (+ k 2) m s))
    nil)))

(defn triplet [s m]
  (let [mlimit (Math/ceil (Math/sqrt (/ s 2)))]
    (when (<= m mlimit)
      (if (= (rem (/ s 2) m) 0.0)
        (if (= (rem m 2) 0)
          (if (not= (logic (+ m 1) m s) nil)
            nil
            (triplet s (+ m 1)))
          (if (not= (logic (+ m 2) m s) nil)
            nil
            (triplet s (+ m 1))))
        (recur s (+ m 1))))))

(defn -main []
  (let [data (rest (get-data))]
    (doseq [item data]
      (triplet (* (Integer. (first item)) 1.0) 2))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 45554791826041 42819237536881 38950692620401 46901911159009 95175887290969
;; 47289346725625 46018924875625 59273446155625
