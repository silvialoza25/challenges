{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #054: Pythagorean Triples - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi str = fromMaybe 0 $ parseInteger {a=Int} str

intAt : Int -> List Int -> Int
intAt 0 [] = 0
intAt _ [] = 0
intAt 0 (x::xs) = x
intAt index (_::xs) = intAt (index - 1) xs

getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

-- Given the sum divided by 2, returns the coefficient c
-- a + b + c = sum -> sum = 2m (m + n)
euclid : Int -> Int -> Int -> Int
euclid num numA numB =
  let numC = numA * numA + numB * numB in
    if (numB * numB + numB * numA) < num then euclid num numA (numB + 1)
    else if (numB * numB + numB * numA) == num then numC
    else euclid num (numA + 1) 1

processTestCase : Int -> Int
processTestCase sum =
  let numC = euclid (div sum 2) 1 2 in
    numC * numC -- c**2

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse input array
    testCases = map (intAt 0) $ map (map stoi) $ map words inputData
    -- Process the range needed
    results = map processTestCase testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  95482309965025 71773242104281 62240186670025 5866507563196954497364066289
  88564455335161 60020114980225 31741201048489 43231953160201
-}
