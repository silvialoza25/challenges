# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.09 seconds (0.01s to load, 0.08s running 55 checks on 1 file)
# 6 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule SpaceshipWeightFraud do
  def main do
    all_data = IO.read(:stdio, :all)
    try do
      data = get_data(all_data)
      |> Enum.map(&hex_to_decimal/1)

      min_number = Enum.map(data,
        &fraud_get_min(&1, check_zeros(Enum.sort(&1, :asc)), 0))
      |> Enum.map(&decimal_to_hex/1)
      |> Enum.map(&Enum.join(&1, ""))

      max_number = Enum.map(data,
        &fraud_get_max(&1, Enum.sort(&1, :desc), 0))
      |> Enum.map(&decimal_to_hex/1)
      |> Enum.map(&Enum.join(&1, ""))

      Enum.map(Enum.zip([min_number, max_number]), &Tuple.to_list(&1))
      |> List.flatten()
      |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def check_zeros(list) do
    if hd(list) == 0 do
      List.flatten([0, Enum.reject(list, fn item -> item == 0 end)])
    else
      list
    end
  end

  def fraud_get_min(list, [], _pointer), do: list
  def fraud_get_min(list, ordered_list, pointer) do
    pos_min = length(list) - Enum.find_index(Enum.reverse(list),
      fn item -> item == hd(ordered_list) end) - 1
    if hd(ordered_list) == 0 and pointer == 0 do
      aux = List.flatten([Enum.at(ordered_list, 1) , hd(ordered_list),
        Enum.slice(ordered_list, 2..length(ordered_list))])
      fraud_get_min(list, aux, pointer)
    else
      if hd(ordered_list) < Enum.at(list, pointer) do
        List.replace_at(list, pos_min, Enum.at(list, pointer))
        |> List.replace_at(pointer, hd(ordered_list))
      else
        if length(ordered_list) > 1 do
          if hd(tl(ordered_list)) == 0 do
            pos_zero = length(list) - Enum.find_index(Enum.reverse(list),
              fn item -> item == 0 end) - 1
            List.replace_at(list, pos_zero, Enum.at(list, pointer))
              |> List.replace_at(pointer, 0)
          else
            fraud_get_min(list, tl(ordered_list), 0)
          end
        else
          fraud_get_min(list, tl(ordered_list), 0)
        end
      end
    end
  end

  def fraud_get_max(list, [], _pointer), do: list
  def fraud_get_max(list, ordered_list, pointer) do
    pos_max = length(list) - Enum.find_index(Enum.reverse(list),
      fn item -> item == hd(ordered_list) end) - 1
    if hd(ordered_list) > Enum.at(list, pointer) do
      List.replace_at(list, pos_max, Enum.at(list, pointer))
      |> List.replace_at(pointer, hd(ordered_list))
    else
      fraud_get_max(list, tl(ordered_list), pointer + 1)
    end
  end

  def hex_to_decimal(list) do
    Enum.map(list, &hd(Tuple.to_list(Integer.parse(&1, 16))))
  end

  def decimal_to_hex(list) do
    Enum.map(list, &Integer.to_string(&1, 16))
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
    |> Enum.reject(fn item -> item == "" end)
    |> tl()
    |> Enum.map(&String.split(&1, ""))
    |> Enum.map(&Enum.reject(&1, (fn item -> item == "" end)))
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end
end

SpaceshipWeightFraud.main()

# cat DATA.lst | elixir ludsrill.exs
# 11F78DD4FC5A425B9D F1F78DD45C1A425B9D 1095CCB1F8F3C134909E0E0A220DB F095CCB1F
# 893C134901E0E0A220DB 14E69A35C2DC6AD9E7A06BE65C96 E4E69A35C2DC6AD9E7A06BC6519
# 6 13B9522C579755E027FA200DE73009 F3B9122C579755E0275A200DE73009 1CBEED6B294AF
# 99FC0F63F70EF3E0C FCBEED1B294AF99FC0F63F70E63E0C 199486539E74D1DDDEC46BC2AB59
# B E99486539E74D1DDD6C41BC2AB59B 11C0547A819F6774 F1C0547A81976714 13E25DF77AD
# 06F88C8 F3125DF77AD06E88C8 108921999AED92EC6BBD36B3DAB5 E08921999AED912C6BBD3
# 6B3DAB5 1385B28DC167F3052C7E8 F385B28DC16753012C7E8 1E15CC12364CC0E4DB6 EE15C
# C11364CC024DB6 26737A0F885A76F33 F6737A0F885A76332 167A24A697DFBF F67A24A197D
# FB6 12ED3E6B413B47D2C4A7 E2ED346B413B17D2C4A7 1E8795745E88C6BBD7FE F18795745E
# 88C6BBD7EE 1F182ECD53C6C30AB579DF63 FF182ECD53C6C301B579DA63 1120FCBFE32C0F23
# B8FF82BD9 F120FCBFE32C0F23B8F281BD9 28BF3D05E029F0CBDC F8BF3D05E029B0C2DC 10C
# 80BF26FBBA4 FFC80B1260BBA4 184AE3A0CAA9CE6DE6A67 E84AE3A0CA19CE6DA6A67 19D659
# 190A85508005A2 D98659190A15508005A2 1745FBF536DD332C5CC0E25EA5FF0 F145FBF536D
# D332C5CC0E25EA5F70 1722D2ED7E35F6 F722D2ED7135E6
