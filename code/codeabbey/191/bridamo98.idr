{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

replaceElement : Nat -> a -> List a -> List a
replaceElement index x xs =
  let
    left = take index xs
    right = drop (index + 1) xs
  in left ++ [x] ++ right

elemAt : Nat -> List a -> a
elemAt 0 (x::xs) = x
elemAt index (_::xs) = elemAt (minus index 1) xs

swapElements : Nat -> Nat -> List a -> List a
swapElements fstIndex sndIndex array =
  let
    fstElem = elemAt fstIndex array
    sndElem = elemAt sndIndex array
    res = replaceElement fstIndex sndElem
      (replaceElement sndIndex fstElem array)
  in res

injectValue : a -> IO a
injectValue dat = pure dat

getInputData : IO(List String)
getInputData = do
  line <- getLine
  if line == ""
    then injectValue []
    else do
      xs <- getInputData
      injectValue (line :: xs)

customMax : String -> String -> String
customMax fstNum sndNum =
  if fstNum < sndNum
    then sndNum
    else fstNum

customMin : String -> String -> String
customMin fstNum sndNum =
  if (fstNum > sndNum) && (fromMaybe ' ' (head' (unpack sndNum))) /= '0'
    then sndNum
    else fstNum

getVariations : List Char -> List (List Nat) -> List (List Char)
getVariations number [] = []
getVariations number swapIndexes =
  let
    [fstIndex, sndIndex] = fromMaybe [] (head' swapIndexes)
    res = [swapElements fstIndex sndIndex number]
      ++ (getVariations number (drop 1 swapIndexes))
  in res

getMinAndMax : String -> String
getMinAndMax input =
  let
    range = [0..(minus (length input) 1)]
    indexes = [[x,y]| x<- range, y<- range]
    variations = map pack (getVariations (unpack input) indexes)
    maxVar = foldl customMax "" variations
    minVar = foldl customMin (pack (replicate (length input) 'F')) variations
  in minVar ++ " " ++ maxVar

main : IO ()
main = do
  inputData <- getInputData
  let
    numbers = drop 1 inputData
    solution = map getMinAndMax numbers
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  17D24D5208DC2C DDD24152087C2C 2A0DD6A3E5B07 EA02D6A3D5B07 127F0423DC38FC8
  F27F0423DC318C8 1F0547D586F560B354E66 FF0547D5861560B354E66
  135CB4E93DAAC09C859F5FCB FF5CB4E93DAAC09C859153CB 29FCCFBFC7C4EB58C
  F9FCCF2BC7C4EB58C 1C6C932F87FD65C0CC8 F16C932F87CD65C0CC8
  167E97BBDBC49EBB2D4F74C F61E97BBDBC49EBB2D4774C 12D282376B607 D22281376B607
  1393A84B04A05F6683C38FBEEE F393A84B04A05F6683C183BEEE
  19AD74C63754FA5F8C8CBC9F82986 F9AD74C63751FA5F8C8CBC9482986 4DE704E586857
  EDE7045586847 1F6A50144CD3C208E2284BD43A92 FA6A50144CD3C208E2284BD43192
  1D247972A0A6C259DD35B0DE82 ED247971A0A6C259DD35B0D282
  2FB487D8CCF00A685FDD6B8 FFB482D8CCF00A6857DD6B8 1061940EC014A3FB4B247F7A2FB6
  F061940EC014A3FB4B247F7A26B1 1E4E00D27842D28F049CCC FE4E00D17842D282049CCC
  12861D3CB7D3E8D6899086 E2861D3CB713D8D6899086
-}
