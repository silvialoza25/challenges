# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.05s to load, 0.1s running 55 checks on 1 file)
# 6 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule BullsAndCows do
  def main do
    all_data = IO.read(:stdio, :all)
    try do
      data = get_data(all_data)
      secret_value = String.split(hd(hd(data)), "")
      |> Enum.reject(fn x -> x == "" end)

      guess = Enum.map(hd(tl(data)), &Enum.reject(String.split(&1, ""),
        fn x -> x == "" end))

      Enum.map(guess, &get_hint(secret_value, &1))
      |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
    |> Enum.reject(fn x -> x == "" end)
    |> Enum.map(&String.split(&1, " "))
  end

  def get_hint(secret_value, guess) do
    equal_position = count_equals_position(secret_value, guess, 0)
    equal_guess = count_equals(secret_value, guess, 0)
    Integer.to_string(equal_position) <> "-" <>
      Integer.to_string(equal_guess - equal_position)
  end

  def count_equals(secret_value, guess, count) do
    if Enum.empty?(secret_value) do
      count
    else
      if Enum.any?(guess, fn x -> x == hd(secret_value) end) do
        count_equals(tl(secret_value), guess, count + 1)
      else
        count_equals(tl(secret_value), guess, count)
      end
    end
  end

  def count_equals_position(secret_value, guess, count) do
    if Enum.empty?(secret_value) do
      count
    else
      if hd(secret_value) == hd(guess) do
        count_equals_position(tl(secret_value), tl(guess), count + 1)
      else
        count_equals_position(tl(secret_value), tl(guess), count)
      end
    end
  end
end

BullsAndCows.main()

# cat DATA.lst | elixir ludsrill.exs
# 1-0 1-2 0-3 1-1 0-3 2-0 0-1 1-0 1-0 1-0 0-3 0-0
