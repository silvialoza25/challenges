;; $ clj-kondo --lint nickkar.cljs
;; linting took 92ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]
            [clojure.string :as str]))

(defn read-input [str] (str/split str #" "))

(defn rate-guess
  ([num guess] (rate-guess num guess 0 0 0))
  ([numdig guessdig poscorrect correct i]
   (cond
     (>= i 4) (str poscorrect "-" correct)
     (= (nth numdig i) (nth guessdig i))
     (rate-guess numdig guessdig (inc poscorrect) correct (inc i))
     (some #(= (nth guessdig i) %) numdig)
     (rate-guess numdig guessdig poscorrect (inc correct) (inc i))
     :else
     (rate-guess numdig guessdig poscorrect correct (inc i))
     )
   ))

(defn rate-guesses
  ([number nguesses guesses] (rate-guesses number nguesses guesses 0 []))
  ([number nguesses guesses i out]
   (cond
     (>= i nguesses) out
     :else
     (rate-guesses number nguesses guesses (inc i)
                   (conj out (rate-guess number (nth guesses i))))
     )
   ))

(defn main []
  (let [firstline (str/split (core/read-line) #" ")
        number (nth firstline 0)
        nguess (core/read-string (nth firstline 1))
        guesses (read-input (core/read-line))]
    (apply println (rate-guesses number nguess guesses))
    ))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 0-2 1-2 0-1 0-2 1-2 1-0 1-1 2-0 0-1 0-1 0-1
