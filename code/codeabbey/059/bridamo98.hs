{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Data.Maybe
import Control.Monad

main = do
  initialData <- getLine
  let divInitialData = words initialData
  let secret = head divInitialData
  guesses <- getLine
  let divGuesses = words guesses
  let solution = getAllHints divGuesses secret
  putStrLn (unwords solution)

getAllHints :: [String] -> String -> [String]
getAllHints [] secret = []
getAllHints divGuesses secret = res
  where
    iGuess = head divGuesses
    iRes = getHint secret iGuess 0 0 0
    res = iRes : getAllHints (drop 1 divGuesses) secret

getHint :: String -> String -> Int -> Int -> Int -> String
getHint secret guess samePos diffPos 4 = show samePos ++ "-"
                                         ++ show diffPos
getHint secret guess samePos diffPos count = res
  where
    curChar = guess !! count
    curCharIndexInSecret = fromMaybe 4 (elemIndex curChar secret) :: Int
    nxtSamePos = if count == curCharIndexInSecret
                   then samePos + 1
                   else samePos
    nxtDiffPos = if curCharIndexInSecret /= 4 && nxtSamePos == samePos
                   then diffPos + 1
                   else diffPos
    res = getHint secret guess nxtSamePos nxtDiffPos (count + 1)

{-
$ cat DATA.lst | ./bridamo98
  1-2 0-2 1-1 1-1 0-3 0-2 2-0 0-3 1-3 2-0 1-2 0-1 0-3
-}
