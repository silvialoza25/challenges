-- $ ghc -o nickkar nickkar.hs
-- [1 of 1] Compiling Main ( nickkar.hs, nickkar.o )
-- Linking code ...
-- $ hlint nickkar.hs
-- No hints

import Control.Monad ( replicateM, forM )
import Data.List (intercalate)

dp:: [Int] -> Int -> Int
dp islands i
    | i >= length islands = 0
    | i == length islands-1 || i == length islands-2
    = islands !! i
    | otherwise
    = max (dp islands (i+2)) (dp islands (i+3)) + islands!!i

format:: [Int] -> [String]
format = map show

convert::String -> [Int]
convert = map read . words

main::IO()
main = do
    n <- readLn :: IO Int
    inputs <- replicateM n getLine
    ca5es <- forM inputs (return . convert)
    out <- forM ca5es(\a ->
        return (dp a 0))
    print (unwords (format out))

-- $ cat DATA.lst | ./nickkar
-- 213 195 112 214 170 170 188 225 160 167 186 126 115 147 140 155 200 156
