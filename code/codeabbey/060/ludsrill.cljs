;; clj-kondo --lint ludsrill.cljs
;; linting took 37ms, errors: 0, warnings: 0

(ns ludsrill.060
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn sweetHarvest [xs path]
  (if (seq xs)
    (if (< (count path) 2)
      (sweetHarvest (rest xs) (conj path (first xs)))
      (sweetHarvest (rest xs) (conj path (+ (first xs)
        (reduce max (take 2 (rest (reverse path))))))))
    (last path)))

(defn -main []
  (let [data (rest (get-data))]
    (doseq [item data]
      (print (core/format "%s " (sweetHarvest
                                 (map #(Integer. %) (reverse item)) []))))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 195 162 166 184 174 126 151 194 201 187 160 113 121 163 203 191 161 146 177
