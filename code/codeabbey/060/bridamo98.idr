{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

injectValue : a -> IO a
injectValue dat = pure dat

getInputData : IO(List String)
getInputData = do
  line <- getLine
  if line == ""
    then injectValue []
    else do
      xs <- getInputData
      injectValue (line :: xs)

toInt : List String -> List Int
toInt dat = map (cast {to=Int}) dat

fromIntegerNat : Int -> Nat
fromIntegerNat 0 = Z
fromIntegerNat number =
  if (number > 0) then
    S (fromIntegerNat (assert_smaller number (number - 1)))
  else
    Z

elemAt : Nat -> List a -> a
elemAt 0 (x::xs) = x
elemAt index (_::xs) = elemAt (minus index 1) xs

replaceElement : Nat -> Int -> List Int -> List Int
replaceElement index x xs =
  let
    left = take index xs
    right = drop (index + 1) xs
  in left ++ [x] ++ right

findBestOption : Nat -> List Int -> Int
findBestOption index isles =
  let
    fstOption = elemAt (index + 2) isles
    sndOption = elemAt (index + 3) isles
    bstOption = max fstOption sndOption
    maxIsleVal = (elemAt index isles) + bstOption
  in maxIsleVal

sweetHarvest : List Int -> Nat -> Int
sweetHarvest isles 0 = findBestOption 0 isles
sweetHarvest isles index =
  let
    maxIsleVal =  findBestOption index isles
  in sweetHarvest (replaceElement index maxIsleVal isles)
    (minus index 1)

solveSingleProblem : List Int -> String
solveSingleProblem isles =
  let
    size = length isles
    newLastVal = (elemAt (minus size 1) isles) +
      (elemAt (minus size 3) isles)
    res = show (sweetHarvest (replaceElement
      (minus size 3) newLastVal isles) (minus size 4))
  in res

main : IO ()
main = do
  inputSize <- getLine
  inputData <- getInputData
  let intInputData = map toInt (map words inputData)
  let solution = map solveSingleProblem intInputData
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  200 125 198 194 135 151 186 153 172 136
  178 181 172 173 168 189 133 191 125 160
-}
