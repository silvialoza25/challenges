/*
  $ rustfmt vmelendez.rs
  $ rustc vmelendez.rs
  $
*/

use std::io::Write;

fn main() -> std::io::Result<()> {
  let mut n = String::new();
  let stdin = std::io::stdin();
  stdin.read_line(&mut n).unwrap();

  let mut file =
    std::fs::File::create("static-web-page.txt").expect("create failed");
  file
    .write_all(format!("Secret value is {}", n).as_bytes())
    .expect("write failed");
  println!("http://prov4nd.github.io/data/static-web-page.txt");
  Ok(())
}

/*
  $ cat DATA.lst | .\vmelendez.exe
  http://prov4nd.github.io/data/static-web-page.txt
*/
