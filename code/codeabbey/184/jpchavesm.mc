/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, char.

:- func solveGame(int, int, string) = int.
solveGame(NMatches, MaxPick, Order) =
(
  %Normal
  if Order = "n"
  then NMatches mod (MaxPick + 1)
  %Inverted
  else if NMatches mod (MaxPick + 1) = 0
  then MaxPick
  else (NMatches mod (MaxPick + 1)) - 1
).

:- pred matchGames(list(string), string, string).
:- mode matchGames(in, in, out) is det.
matchGames([], Solution, Solution).
matchGames([Line | Tail], PartialSolution, Solution) :-
(
  string.words_separator(char.is_whitespace, Line) = Game,
  det_to_int(det_index0(Game, 0)) = NMatches,
  det_to_int(det_index0(Game, 1)) = MaxPick,
  det_index0(Game, 2, Order),
  solveGame(NMatches, MaxPick, Order) = Answer,

  PartialSolution1 = PartialSolution ++ " " ++ from_int(Answer),
  matchGames(Tail, PartialSolution1, Solution)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
                   list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1, FileContents, ComputeList),
    matchGames(ComputeList, "", Solution),
    io.print_line(Solution,!IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  0 5 0 5 14 2 4 15 4 7 2 3 1 5 0 5 3 2 0 5 1 0 8 1 8 9 9
*/
