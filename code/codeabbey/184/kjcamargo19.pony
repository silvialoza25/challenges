// ponyc -b kjcamargo19

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun iterator(data: Array[String]): Array[String] =>
          data.slice(1, data.size())

        fun get_first_move(const_m: U32, const_k: U32, tipo: String) =>
          var answer: U32 = const_m % (const_k + 1)
          if (tipo == "n") then
            env.out.write(answer.string())
          else
            if (answer == 0 )then
              env.out.write(const_k.string())
            else
              env.out.write((answer - 1).string())
            end
          end

        fun obtain_data(data: Array[String])? =>
          if data.size() > 0 then
            var line: Array[String] = data(0)?.split_by(" ")
            var const_m: U32 = line(0)?.u32()?
            var const_k: U32 = line(1)?.u32()?
            var rules: String = line(2)?

            get_first_move(const_m, const_k, rules)
            env.out.write(" ")
            obtain_data( iterator (data))?
          end

        fun ref apply(data: Array[U8] iso) =>
          var input_data: String = String.from_iso_array(consume data)
          var line_sep: Array[String] = input_data.split_by("\n")
          var real_data: Array[String] = line_sep.slice(1, line_sep.size()-1)

          try
            obtain_data(real_data)?
          end

        fun ref dispose() =>
          None

      end,
      512
    )
// cat DATA.lst | ./kjcamargo19
// 0 1 3 3 0 7 6 2 15 3 1 8 10 7 19 6 0 1 6 4 3 4 1 10 9 9 1 6
