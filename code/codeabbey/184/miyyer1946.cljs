;; $ clj-kondo --lint miyyer1946.clj
;; linting took 24ms, errors: 0, warnings: 0

(ns miyyer1946-184
  (:gen-class)
  (:require [clojure.core :as core]))

(defn result [a b c]
    (if (== (compare (str c) "n") 0)
        (print (str (mod a (+ b 1)) " "))
        (print (str (mod (- a 1) (+ b 1)) " ")))
)

(defn data-loop [size-input]
  (loop [x 1]
    (when (< x (+ size-input 1))
      (result (core/read) (core/read) (core/read))
      (recur (+ x 1))
    )
  )
)

(defn main []
  (let [size-input (core/read)]
    (data-loop size-input)
  )
)

(main)

;$ cat DATA.lst | clj miyyer1946.cljs
;7 10 10 2 0 0 10 2 4 11 2 2 3 5 0 15 5 5 1 3 0 4 5 3
