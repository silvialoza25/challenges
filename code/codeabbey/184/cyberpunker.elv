# elvish -compileonly cyberpunker.elv

#!/bin/elvish

use str
use math

fn input_data []{
  put [(each [line]{
    put [(str:split ' ' $line)]
  })][1..]
}

fn findMatchesPicking [match game mod]{
  if (eq $game n) {
    put $mod
  } else {
    if (== $mod 0) {
      put $match
    } else {
      put (- $mod 1)
    }
  }
}

fn solveGame [valueM valueK normalRules]{
  var add = (+ $valueK 1)
  var module = (% $valueM $add)
  findMatchesPicking $valueK $normalRules $module
}

fn playGame [data]{
  each [values]{
    var constM = $values[0]
    var constK = $values[1]
    rule = $values[2]
    solveGame $constM $constK $rule
  } $data
}

echo (playGame (input_data))

# cat DATA.lst | elvish cyberpunker.elv
# 11 10 8 3 2 8 9 1 7 1 1 8 5 1 13 0 12 0 3 2 5 5 4 3 13 9 0
