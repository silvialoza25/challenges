###
$ coffeelint jmesa85.coffee
  ✓ jmesa85.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file
###

# CodeAbbey #184: Matches Picking - Coffeescript

# Returns the first move
processTestCase = (testCaseStr) ->
  numbersArr = testCaseStr.split(' ')
  valueM = parseInt(numbersArr[0]) # Total matches
  valueK = parseInt(numbersArr[1]) # Number of matches ot take
  matchType = numbersArr[2] # "Normal" or "Inverted"
  leftMatchesAfterMoves = valueM % (valueK + 1) # Leaving 1 match in the heap
  if matchType is "n"
    leftMatchesAfterMoves
  else if matchType is "i"
    if leftMatchesAfterMoves is 0
      valueK
    else
      leftMatchesAfterMoves - 1

main = ->
  # Read STDIN
  data = require('fs').readFileSync(0).toString().trim().split('\n')
  # Discard number of test cases at index 0
  testCasesAsStrings = data.slice(1)
  # Process each test case
  results = testCasesAsStrings.map(processTestCase)
  # Print results
  console.log(results.join(' '))
  return

main()

###
$ cat DATA.lst | coffee jmesa85.coffee
2 3 5 3 2 5 0 2 9 10 0 12 13 3 3 5 15 5 11 0 0 9 4 6 5
###
