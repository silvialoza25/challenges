# elvish -compileonly alejotru3012.elv

use str

fn input_data []{
  each [x]{
    put [(str:split ' ' $x)]
  }
}

fn matches_picking [data]{
  each [values]{
    num_m = $values[0]
    num_k = $values[1]
    game_type = $values[2]
    res = (% $num_m (+ $num_k 1))
    if (eq $game_type n) {
      echo $res
    } else {
      if (eq $res 0) {
        echo $num_k
      } else {
        echo (- $res 1)
      }
    }
  } $data
}

echo (str:join ' ' [(matches_picking [(input_data)][1..])])

# cat DATA.lst | elvish alejotru3012.elv
# 13 2 0 5 13 1 12 0 3 2 16 14 4 0 2 0 2 8 0 0 2 0 10 4 15 6 8 0 5
