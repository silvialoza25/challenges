// dartanalyzer ludsrill.dart
// Analyzing ludsrill.dart...
// No issues found!

import 'dart:core';
import 'dart:io';

void main() {
  List<List<String>> input = getData([]);
  List<int> solution = input
      .map((item) => (item[2] == "n")
          ? matchesRuleN(int.parse(item[0]), int.parse(item[1]))
          : matchesRuleI(int.parse(item[0]), int.parse(item[1])))
      .toList();
  print(solution.join(" "));
}

int matchesRuleN(int allMatches, int takenMatches) {
  return allMatches % (takenMatches + 1);
}

int matchesRuleI(int allMatches, int takenMatches) {
  int aux = (allMatches % (takenMatches + 1));
  if (aux == 0) {
    return takenMatches;
  } else {
    return aux - 1;
  }
}

List<List<String>> getData(List<List<String>> input) {
  String getLine = stdin.readLineSync();
  if (getLine == null) {
    return input.sublist(1);
  } else {
    List<String> newLine = getLine.split(" ");
    return getData(input + [newLine]);
  }
}

// cat DATA.lst | dart ludsrill.dart
// 4 0 0 0 3 0 14 2 19 0 0 7 0 8 2 3 0 2 14 5 5 0 17 3 1 11 6 12
