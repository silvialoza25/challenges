;; $ clj-kondo --lint nickkar.clj
;; linting took 33ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class))

;; read input and returns a vector with the information
(defn read-cases ([ncases] (read-cases ncases 0 []))
  ([ncases i out]
    (cond
      (>= i ncases) out
      :else
      (read-cases ncases (inc i) (conj out [(read) (read) (read)])))))

;; calculates the number of matches to take needed to win the game, returns
;; 0 if the game can't be won
(defn pick-match [heap max-take type-game]
  (cond
    (= type-game (symbol "n")) (mod heap (inc max-take))
    :else
    (let [rem (mod heap (inc max-take))]
      (cond
        (= rem 0) max-take
        :else (dec rem)))))

;; iterates over every case
(defn solve-cases ([ncases cases] (solve-cases ncases cases 0 []))
  ([ncases cases index out]
   (cond
     (>= index ncases) out
     :else
     (let [[heap max-take type-game] (nth cases index)
           pick (pick-match heap max-take type-game)]
       (solve-cases ncases cases (inc index) (conj out pick))))))

;; Driver code
(defn main []
  (let [ncases (read)
        cases (read-cases ncases)]
    (apply println (solve-cases ncases cases))))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; 7 10 10 2 0 0 10 2 4 11 2 2 3 5 0 15 5 5 1 3 0 4 5 3
