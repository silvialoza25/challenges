{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

%default partial

fromIntegerNat : Int -> Nat
fromIntegerNat 0 = Z
fromIntegerNat number =
  if (number > 0) then
    S (fromIntegerNat (assert_smaller number (number - 1)))
  else
    Z

getBrainFuckCode : Nat -> Nat -> String -> String -> String
getBrainFuckCode fstNum sndNum fwdCellTrans printOp =
  let
    fstNumRep = pack (replicate fstNum '+')
    sndNumRep = pack (replicate sndNum '+')
    res = fstNumRep ++ fwdCellTrans ++ sndNumRep ++ fwdCellTrans
      ++ fstNumRep ++ sndNumRep ++ printOp
  in res

main : IO ()
main = do
  text <- getLine
  let
    numbers = map (cast {to=Int}) (words text)
    fstNum = fromIntegerNat (fromMaybe 0 (head' numbers))
    sndNum = fromIntegerNat (fromMaybe 0 (last' numbers))
    fwdCellTrans = ">"
    bwdCellTrans = "<"
    printOp = ":" ++ bwdCellTrans ++ ":" ++ bwdCellTrans ++ ":"
    solution = [getBrainFuckCode fstNum sndNum
      fwdCellTrans printOp]
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  +++++++++>+++++++++++++++++++++++++>++++++++++++++++++++++++++++++++++:<:<:
-}
