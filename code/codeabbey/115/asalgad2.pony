// ponyc -b asalgad2

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun tail_str( array: Array[String] ): Array[String] =>
          array.slice(1, array.size())

        fun tail( array: Array[F64] ): Array[F64] =>
          array.slice(1, array.size())

        fun copy( destin: Array[F64], origin: Array[String],
                  dest_i: USize, orig_i: USize, size: USize ): Array[F64] ? =>
          if orig_i < size then
            var value: F64 = origin(orig_i)?.f64()?
            destin.update( dest_i, value )?
            copy( destin, origin, dest_i+1, orig_i+1, size )?
          end
          destin

        fun init_system( system: Array[F64], lines: Array[String],
                         const_n: USize, const_i: USize ) ? =>
          if lines.size() > 0 then
            copy( system, lines(0)?.split(" "), const_i*const_n, 0, const_n )?
            init_system( system, tail_str(lines), const_n, const_i+1 )?
          end

        fun create_sys( const_n: USize, lines: Array[String] ): Array[F64] ? =>
          var system: Array[F64] = Array[F64].init(0, const_n*(const_n+1))
          init_system( system, lines, const_n, 0 )?
          system

        fun dot( const_x: Array[F64], const_y: Array[F64] ): F64 ? =>
          if const_x.size() > 0 then
            ( const_x(0)? * const_y(0)? ) + dot( tail(const_x), tail(const_y) )?
          else
            0
          end

        fun objective( const_x: Array[F64], system: Array[F64],
                       const_n: USize, const_i: USize ): F64 ? =>
          if const_i < const_n then
            var start: USize = const_i*const_n
            var const_b: F64 = system( (const_n*const_n) + const_i )?
            var const_f: F64 = dot(const_x, system.slice(start, start+const_n))?
            var result: F64 = (const_f-const_b).pow(2)
            result + objective( const_x, system, const_n, const_i+1 )?
          else
            0
          end

        fun gradient( grad: Array[F64], const_x: Array[F64], system: Array[F64],
                      const_n: USize, const_dx: F64, const_i: USize ) ? =>
          if const_i < const_n then
            var const_f: F64 = objective( const_x, system, const_n, 0 )?
            const_x.update( const_i, const_x( const_i )? + const_dx)?
            var const_fx: F64 = objective( const_x, system, const_n, 0 )?
            const_x.update( const_i, const_x( const_i )? - const_dx)?
            grad.update( const_i, (const_fx - const_f) / const_dx )?
            gradient( grad, const_x, system, const_n, const_dx, const_i + 1 )?
          end

        fun min( const_a: F64, const_b: F64 ): F64 =>
          if const_a < const_b then
            const_a
          else
            const_b
          end

        fun move( const_x: Array[F64], grad: Array[F64],
                    step: F64, const_i: USize ) ? =>
          if const_i < const_x.size() then
            const_x.update(const_i, const_x(const_i)? - (grad(const_i)?*step))?
            move( const_x, grad, step, const_i+1 )?
          end

        fun grad_desc( const_x: Array[F64], system: Array[F64],
                       step: F64, const_n: USize, iter: USize ): USize? =>
          var const_y: F64 = objective( const_x, system, const_n, 0 )?

          if const_y < 0.0001 then
            return iter
          end

          var const_dx: F64 = step / 10
          var grad: Array[F64] = Array[F64].init(0, const_n)
          gradient( grad, const_x, system, const_n, const_dx, 0 )?

          var new_const_x = const_x.clone()
          move( new_const_x, grad, step, 0 )?

          if objective( new_const_x, system, const_n, 0 )? < const_y then
            grad_desc(new_const_x, system, min(0.1,step*1.25), const_n, iter+1)?
          else
            grad_desc(const_x, system, step/1.25, const_n, iter+1)?
          end

        fun iterate( lines: Array[String], n_systems: USize, start: USize ) ? =>
          if n_systems > 0 then
            var step: F64 = 0.01
            var const_n: USize = lines(start)?.usize()?
            var sys_lines: Array[String] = lines.slice(start+1, start+const_n+2)
            var system: Array[F64] = create_sys( const_n, sys_lines )?
            var const_x: Array[F64] = Array[F64].init(0, const_n)
            var iter: USize = grad_desc( const_x, system, step, const_n, 0 )?
            env.out.write( iter.string() )
            env.out.write( " " )

            iterate( lines, n_systems-1, start+const_n+2 )?
          end

        fun ref apply(raw_data: Array[U8] iso) =>
          var input: String = String.from_iso_array(consume raw_data)
          var lines: Array[String] = input.split_by("\n")

          try
            var n_systems: USize = lines(0)?.usize()?
            iterate( lines, n_systems, 1 )?
          end

        fun ref dispose() =>
          env.out.write("")
      end, 4092 )

// cat DATA.lst | ./asalgad2
// 224 84 159 162
