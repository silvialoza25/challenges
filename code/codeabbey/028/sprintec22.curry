-- $ curry-verify sprintec22.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `sprintec22'!

import IO
import List
import Read
import Float

getData :: IO String
getData = do
  line <- getContents
  return line

strToF :: String -> Float
strToF string = read string :: Float

operation :: Float -> Float -> Float
operation weight height = weight / (height * height)

bmi :: Float -> String
bmi value
  | value < 18.5 = "under"
  | value >= 18.5 && value < 25.0 = "normal"
  | value >= 25.0 && value < 30.0 = "over"
  | value >= 30.0 = "obese"

getValues :: String -> String
getValues dataIn = result
  where
    array = words dataIn
    weight = strToF (array !! 0)
    height = strToF (array !! 1)
    value = operation weight height
    result = bmi value

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    finalResult = map (\x -> getValues x) usefulData
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs -q :load sprintec22.curry :eval main :quit
-- under under under normal under under normal normal normal under under
-- normal over over under under under under under obese normal over obese
-- obese obese obese normal over normal under under
