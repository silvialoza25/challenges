#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun calc-bmi(w h)
  (let ((bmi (/ w (* h h))))
    (if (< bmi 18.5)
      (format t "under ")
      (if (< bmi 25.0)
        (format t "normal ")
        (if (< bmi 30.0)
          (format t "over ")
          (format t "obese "))))))

(defun calc-bmis(size-input weights heights)
  (loop for i from 0 to (- size-input 1)
    do(calc-bmi
      (aref weights i)
      (aref heights i))))

(defun get-input-lines (i weights heights)
   (setf (aref weights i) (read))
   (setf (aref heights i) (read)))

(defun main (size-input)
  (let ((weights (make-array size-input)) (heights (make-array size-input)))
    (loop for i from 0 to (- size-input 1)
      do(get-input-lines i weights heights))
    (calc-bmis size-input weights heights)))

(defvar size-input (read))
(main size-input)

#|
  cat DATA.lst | clisp bridamo98.lsp
  over under under obese under obese obese normal obese normal normal
  normal under normal obese obese under over normal obese under normal
  obese obese under over under under normal under over over under under
  normal
|#
