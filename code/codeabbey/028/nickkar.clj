;; $clj-kondo --lint nickkar.clj
;; linting took 20ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class))

(defn read-input ([cases] (read-input cases 0 []))
  ([cases i out]
    (cond
      (>= i cases) out
      :else (read-input cases (inc i) (conj out [(read) (read)])))))

(defn interpreter [BMI]
  (cond
    (< BMI 18.5) "under"
    (and (>= BMI 18.5) (< BMI 25)) "normal"
    (and (>= BMI 25) (< BMI 30)) "over"
    (>= BMI 30) "obese"))

(defn solve-cases ([cases nums] (solve-cases cases nums 0 []))
  ([cases nums i out]
    (cond
      (>= i cases) out
      :else
      (let [[weight height] (nth nums i)
            BMI (/ weight (Math/pow height 2))]
        (solve-cases cases nums (inc i) (conj out (interpreter BMI)))))))

(defn main []
  (let [cases (read)
        nums (read-input cases)]
    (apply println (solve-cases cases nums))))

(main)

;; $ cat DATA.lst | clj -M nickkar.clj
;; over under under obese under obese obese normal obese normal normal normal
;; under normal obese obese under over normal obese under normal obese obese
;; under over under under normal under over over under under normal
