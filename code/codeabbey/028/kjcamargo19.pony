// ponyc -b kjcamargo19

use "collections"

actor Main
  new create(env: Env) =>
    env.input(
      object iso is InputNotify

        fun iterator(data: Array[String]): Array[String] =>
          data.slice(1, data.size())

        fun classify(bmi:F32) =>
          if bmi < 18.5 then
            env.out.write("under")
          elseif (bmi >= 18.5) and (bmi < 25.0) then
            env.out.write("normal")
          elseif (bmi >= 25.0) and (bmi < 30.0) then
            env.out.write("over")
          else
            env.out.write("obese")
          end

        fun calculate_bmi(weight: F32, height:F32):F32 =>
          weight / height.pow(2)

        fun obtain_data(data: Array[String])? =>
          if data.size() > 0 then
            var line: Array[String] = data(0)?.split_by(" ")
            var weight: F32 = line(0)?.f32()?
            var height: F32 = line(1)?.f32()?
            var bmi:F32 = calculate_bmi(weight,height)

            classify(bmi)
            env.out.write(" ")
            obtain_data( iterator (data))?
          end

        fun ref apply(data: Array[U8] iso) =>
          var input_data: String = String.from_iso_array(consume data)
          var line_sep: Array[String] = input_data.split_by("\n")
          var real_data: Array[String] = line_sep.slice(1, line_sep.size())

          try
            obtain_data(real_data)?
          end

        fun ref dispose() =>
          env.out.write("")
      end,
      512
    )
// cat DATA.lst | ./kjcamargo19
// under under under normal under under normal normal normal under under
// normal over over under under under under under obese normal over obese
// obese obese obese normal over normal under under
