{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #028: Body Mass Index - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stod : String -> Double
stod str = fromMaybe 0 $ parseDouble str

-- Gets lines in the Stdin
getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

processTestCase : List Double -> String
processTestCase [weight, height] = let bmi = (weight / (height * height)) in
  if bmi >= 30.0 then "obese"
    else if bmi >= 25.0 then "over"
      else if bmi >= 18.5 then "normal"
        else "under"

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Parse test cases
    testCases = map (map stod) $ map words inputData
    -- Process data
    results = map processTestCase testCases
  -- Print results
  putStrLn $ unwords results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  obese obese under under under under normal normal under under normal obese
  obese normal under over normal normal under obese over obese under normal
  under obese
-}
