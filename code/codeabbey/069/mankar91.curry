-- $ curry-verify mankar91.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `mankar91'!

import Read

fibonacciDivisible :: (Prelude.Integral a, Prelude.Num b) =>
  a -> a -> b -> a -> b
fibonacciDivisible term1 term2 index value
  | sum `mod` value == 0 = index
  | otherwise = fibonacciDivisible term2 sum (index + 1) value
  where
    sum = term1 + term2

main :: Prelude.IO ()
main = do
  getLine
  testcases <- getLine
  let
    testCasesList = map readInt (words testcases)
    result = map (fibonacciDivisible 0 1 2) testCasesList
  putStrLn (unwords (map show result))

-- $ cat DATA.lst | pakcs -q :load mankar91.curry :eval main :quit
-- 900 1914 2700 420 1350 1500 840 1100 1000 414 2569 2100 990 475 80 3684 2928
--  3528 2100
