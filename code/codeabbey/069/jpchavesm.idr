{-
  $ idris2 jpchavesm.idr -o jpchavesm
-}

module Main
import Data.Maybe
import Data.Strings

str2int : String -> Integer
str2int elem = fromMaybe 0 $ (parseInteger {a=Integer} elem)

-- The fibonacci sequence is defined as F_n = F_(n-2) + F_(n-1)
fibDiv : Integer -> Integer -> Integer -> Nat -> Nat
fibDiv f_n2 f_n1 divisor fibIndex =
  let f_n = f_n2 + f_n1 in if (mod f_n divisor) == 0
  then fibIndex
  else fibDiv f_n1 f_n divisor (S fibIndex)

main : IO ()
main = do
  size <- getLine
  strInput <- getLine
  let intArr = map str2int (words strInput)
  let indArr = map (\x => fibDiv 0 1 x 2) intArr
  let result = map show indArr
  putStrLn (unwords result)

{-
  $ cat DATA.lst | ./build/exec/jpchavesm
  900 1914 2700 420 1350 1500 840 1100 1000 414 2569 2100 990 475 80 3684 2928
  3528 2100
-}
