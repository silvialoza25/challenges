#!/usr/bin/perl
# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 bridamo98.pl
# bridamo98.pl source OK
#
# Compile:
# $ perl -c bridamo98.pl
# bridamo98.pl syntax OK

package bridamo98;

use warnings FATAL => 'all';
use strict;
use Math::BigInt;
our ($VERSION) = 1;

my $SPACE = q{ };

sub fibonacci {
  my @data   = @_;
  my $number = $data[0];
  my $first  = 0;
  my $sec    = 1;
  my $result = 2;
  my $current;
  while ( 1 == 1 ) {
    $current = Math::BigInt->new( $first + $sec );
    if ( $current % $number == 0 ) {
      last;
    }
    else {
      $first  = $sec;
      $sec    = $current;
      $result = $result + 1;
    }
  }
  return $result;
}

sub solve_all {
  my @data  = @_;
  my @input = @{ $data[0] };
  my $result;
  foreach my $number (@input) {
    $result = fibonacci($number);
    exit 1 if !print "$result ";
  }
  return;
}

sub main {
  my $sizeInput = int <>;
  my @input     = split $SPACE, <>;
  solve_all( \@input );
  return;
}
main();
exit 1 if !print "\n";

# $ cat DATA.lst | perl bridamo98.pl
# 3564 1144 25 703 600 420 684 3192 396 330 420
# 2100 300 4950 8232 3040 938 3300 3738 900 504
