;$ clj-kondo --lint frank1030.cljs
;linting took 590ms, errors: 0, warnings: 0

;$ lein test
;lein test frank1030.core-test
;Ran 1 tests containing 1 assertions.
;0 failures, 0 errors.

(ns frank1030.core
  (:gen-class)
  (:require [clojure.core :as core]))

(defn fib-div [a b iterator n]
  (let [r (mod (+ a b) n)]
    (if (= r 0)
      (print @iterator "")
      (do (swap! iterator inc)
        (fib-div b r iterator n)))))

(defn get-dat [index]
  (let [x (atom index) n (core/read) iterator (atom 2)]
    (if (> @x 0)
      (do (swap! x dec)
        (fib-div 0 1 iterator n)
        (if (not= @x 0)
          (get-dat @x)
          (print "")))
      (print ""))))

(defn main []
  (let [index (core/read)]
    (get-dat index)))

(main)

;$ cat DATA.lst | clj frank1030.cljs
;900 220 96 1890 36 300 396 1914 3476 669 684
;1000 330 900 938 2844 1144 648 7896 900 72
