/*
$ npx eslint jmesa85.ts
$ npx prettier --check jmesa85.ts
Checking formatting...
All matched files use Prettier code style!
*/

// CodeAbbey #069: Fibonacci Divisibility - Typescript

// Function that returns position of first Fibonacci number
// whose modulo p (divisor) is 0
function findMinZero(
  divisor: number,
  index: number,
  prevFibo: number,
  nextFibo: number
): number {
  // Add previous two remainders and then take its modulo p
  const modFibo = (prevFibo + nextFibo) % divisor;
  // Base case
  if (modFibo === 0) {
    // Found!
    return index;
  }
  // Recursive case
  return findMinZero(divisor, index + 1, nextFibo, modFibo);
}

function processTestCase(testCase: number): number {
  return findMinZero(testCase, 2, 0, 1);
}

function main(): number {
  // Read STDIN
  process.stdin.setEncoding('utf8');
  process.stdin.on('data', (rawData) => {
    // Get the test cases in the second line
    const [testCases] = rawData.toString().trim().split('\n').slice(1);
    // Process results
    const results = testCases.split(' ').map(Number).map(processTestCase);
    // Print result
    process.stdout.write(results.join(' '));
    return 0;
  });
  return 0;
}

main();

/*
$ tsc jmesa85.ts
$ cat DATA.lst | node jmesa85.js
900 1914 2700 420 1350 1500 840 1100 1000 414 2569 2100 990 475 80
3684 2928 3528 2100
 */
