-- $ ghc -o smendoz3 smendoz3.hs
--   [1 of 1] Compiling Main ( smendoz3.hs, smendoz3.o )
--   Linking code ...
-- $ hlint smendoz3.hs
--   No suggestions

main = do
  useless <- getLine
  input <- getLine
  print (unwords (map function (checkString input)))

checkString :: String -> [Int]
checkString str = map read $ words str :: [Int]

function :: Int -> String
function x = show (fibImp 0 1 2 x)

fibImp :: Int -> Int -> Int -> Int -> Int
fibImp a b i n = do
  let c = (a + b) `mod` n
  if c == 0 then
    i
  else
    fibImp b c (i + 1) n

-- $ cat DATA.lst | ./smendoz3
--   2100 4128 25 396 8700 703 490 3528 4700 220
--   750 2928 900 120 1400 420 4296 504
