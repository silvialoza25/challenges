# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.08 seconds (0.02s to load, 0.06s running 55 checks on 1 file)
# 5 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule FibonacciDivisibility do

  def main do
    data = get_data()
    Enum.map(data, &fibonacci_divisibility(&1, 1, 0, 1))
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def get_data do
    all_data = String.split(IO.read(:stdio, :all), "\n")
    [_ | tail] = List.delete_at(all_data, 2)
    Enum.at(Enum.map(Enum.map(tail, &String.split(&1, " ")), &to_int/1), 0)
  end

  def fibonacci_divisibility(item, counter, a, b) do
    modular_fibo = rem(a + b, item)
    if modular_fibo == 0 do
      IO.write("#{(counter + 1)} ")
    else
      fibonacci_divisibility(item, counter + 1, b, modular_fibo)
    end
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e FibonacciDivisibility.main
# 54 350 7728 8700 3476 396 669 3528 330 420 2064 900 4464 3738 576 25 2700
# 2100 2844
