;; $ clj-kondo --lint bridamo98.cljs
;; linting took 71ms, errors: 0, warnings: 0

(ns bridamo98-145
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn abs [n] (max n (- n)))

(defn ** [x n] (reduce *
(repeat (core/bigint n) (core/bigint  x))))

(defn hsqr[num]
  (loop [r (core/bigint (Math/sqrt num))
  d (/ (core/bigint num) r) dif (abs (- r d)) i 0]
    (if (< dif 0.001) (core/bigint (Math/floor (/ (+ r d) 2N)))
      (let [new-r (/ (+ r d) 2N) new-d (/ num new-r)
      new-dif (abs (- new-r new-d))]
        (recur new-r new-d new-dif (+ i 1))))))

(defn calc-pi [k n]
  (let [r (core/bigint (** 10 k))]
    (loop [i 1 D r d (core/bigint (Math/floor (/ D 2N)))
    h (hsqr (- (* r r) (* d d)))
    x (hsqr (+ (* d d) (* (- r h) (- r h))))]
      (if (< i n)
        (let [new-D x new-d (core/bigint (Math/floor (/ new-D 2)))
        new-h (hsqr (- (* r r) (* new-d new-d)))
        new-x (hsqr(+ (* new-d new-d)
        (* (- r new-h) (- r new-h))))]
          (recur (+ i 1)  new-D new-d new-h new-x))
        (Math/floor (/ (* (* 6 (** 2 n)) x) 2))))))

(defn main []
  (let [input-data (str/split (core/read-line) #" ")]
    (println (core/bigint
    (calc-pi (edn/read-string (get input-data 0))
    (edn/read-string (get input-data 1)))))))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 3141592653589793238462157025169875168637912936245643706368
