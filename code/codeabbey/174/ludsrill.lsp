#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defvar *data*)
(setq *read-default-float-format* 'long-float)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect line)))

(defun split-by-one-space (item)
  (loop for i = 0 then (1+ j)
    as j = (position #\Space item :start i)
    collect (subseq item i j)
    while j))

(defun babylonian-square-root (data)
  (let ((x 1000))
    (loop for i from 0 to 1000
      do(setq x (* (/ 1 2) (+ x (floor data x)))))
    (return-from babylonian-square-root x)))

(defun calculate-pi (data)
  (let ((radius)
        (iterations)
        (amount-sides)
        (aux-hick)
        (aux-triangle)
        (new-side)
        (pi-calc)
        (split))
    (dolist (item data)
      (setq split (split-by-one-space item)))

    (setq radius (expt 10 (parse-integer (first split))))
    (setq iterations (parse-integer (second split)))
    (setq amount-sides (* 6 (expt 2 iterations)))
    (setq new-side radius)

    (loop for i from 0 to (- iterations 1)
      do(setq aux-triangle (babylonian-square-root
                            (- (expt radius 2) (expt (floor new-side 2) 2))))
      (setq aux-hick (- radius aux-triangle))
      (setq new-side (babylonian-square-root (+ (expt aux-hick 2)
                                              (expt (floor new-side 2) 2)))))

    (setq pi-calc (floor (* new-side amount-sides) 2))
    (print pi-calc)))

(setq *data* (read-data))
(calculate-pi *data*)

#|
cat DATA.lst | clisp ludsrill.lsp
3141592653589793238462157025169875168637912936305417528806465339392
|#
