-- $ ghc -o nickkar nickkar.hs
-- [1 of 1] Compiling Main ( nickkar.hs, nickkar.o )
-- Linking nickkar ...
-- $ hlint nickkar.hs
-- No hints

import Control.Monad ( replicateM, forM )
import Data.List (intercalate)

--this is done due loss of precision in sqrt (binary search)
findsqrt:: Integer -> Integer -> Integer -> Integer
findsqrt actual goal step
    | (actual^2) < goal && ((actual + 1)^2) > goal
    = actual
    | (actual^2) > goal
    = findsqrt (actual - step) goal (ceiling(fromIntegral step / 2))
    | (actual^2) < goal
    = findsqrt (actual + step) goal (ceiling(fromIntegral step / 2))
    | (actual^2) == goal
    = actual

--this is done due loss of precision in "/" (binary search)
finddiv:: Integer -> Integer -> Integer -> Integer
finddiv actual goal step
    | (actual*2) < goal && ((actual + 1)*2) > goal
    = actual
    | (actual*2) > goal
    = finddiv (actual - step) goal (ceiling(fromIntegral step / 2))
    | (actual*2) < goal
    = finddiv (actual + step) goal (ceiling(fromIntegral step / 2))
    | (actual*2) == goal
    = actual

findPi:: Integer -> Integer -> Integer -> Integer -> Integer
findPi r maxi i side
    | i >= maxi
    = finddiv (floor (fromIntegral (side * (6 * (2^i))) / 2))
    (side * (6 * (2^i)))
    (floor (fromIntegral (side * (6 * (2^i))) / 4))
    | otherwise = findPi r maxi (i+1) x
    where
        dd = floor (fromIntegral side/2)
        d = finddiv dd side (floor (fromIntegral dd / 2))
        firstriang = (r^2) - (d^2)
        dh = floor (sqrt (fromIntegral firstriang))
        h = findsqrt dh firstriang (floor (fromIntegral dh / 2))
        secondtriang = (d^2) + ((r-h)^2)
        dx = floor (sqrt (fromIntegral secondtriang))
        x = findsqrt dx secondtriang (floor (fromIntegral dx / 2))


main::IO()
main = do
    input <- fmap (map read.words) getLine :: IO [Int]
    let r = 10 ^ head input
        maxi = input!!1
    print (
        findPi (fromIntegral r:: Integer)
        (fromIntegral maxi:: Integer)
        0
        (fromIntegral r:: Integer))

-- $ cat DATA.lst | ./nickkar
-- 3141592653589793238462157025169875168637912936305417528806465339392
