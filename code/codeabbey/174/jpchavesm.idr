{-
  $ idris2 jpchavesm.idr -o jpchavesm
-}

module Main
import Data.Maybe
import Data.Strings

str2int : String -> Integer
str2int elem = fromMaybe 0 $ (parseInteger {a=Integer} elem)

infixr 10 ^
(^) : (base : Integer) -> (pow : Integer) -> Integer
_ ^ 0 = 1
0 ^ _ = 0
base ^ pow =
  let
    half_exp = div pow 2
    fastExp = square (base ^ half_exp)
  in if mod pow 2 == 0 then fastExp else base * fastExp
  where
    square : Integer -> Integer
    square number = number * number

--Newton's Method for isqrt(value)
quickSqrtAux : Integer -> Integer -> Integer
quickSqrtAux value x_i =
  let x_i' = div (x_i + div value x_i) 2
  in if abs (x_i' - x_i) < 1 then x_i' else quickSqrtAux value x_i'

quickSqrt : Integer -> Integer
quickSqrt value = quickSqrtAux value 1

calcPiAux : Integer -> Integer -> Integer -> Integer
calcPiAux _ side 0 = side
calcPiAux radius side n =
  let
    d = div side 2
    h = quickSqrt (radius^2 - d^2)
    side' = quickSqrt ((radius-h)^2 + d^2)
  in calcPiAux radius side' (n-1)

--k: Precision of the answer also 10^k is equal to the radius of the polygon
--n: number of steps, also 6*2^n is the number of sides of the final polygon
partial calcPi : List Integer -> Integer
calcPi [k,n] =
  let
    side = calcPiAux (10^k) (10^k) n --in a hexagon r == side
  in div (side*6*(2^n)) 2

processFile : IO(List String)
processFile = do
  row <- getLine
  if row == ""
    then pure []
    else do
      nextRow <- processFile
      pure (row :: nextRow)

partial main : IO ()
main = do
  strInput <- processFile
  let
    testData = map (map str2int) $ map words strInput
    result = map show $ map calcPi testData
  putStrLn (unwords result)

{-
  $ cat DATA.lst | ./build/exec/jpchavesm
  3141592653589793238462157025169875168637912936305417528806465339392
-}
