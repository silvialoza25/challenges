# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.04s to load, 0.1s running 55 checks on 1 file)
# 7 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule PrimeNumbersGeneration do
  def main do
    all_data = IO.read(:stdio, :all)

    try do
      primes = Enum.to_list(1..2_750_131)
              |> Enum.reduce([], fn x, acc -> aux_reduce(x, acc) end)

      get_data(all_data)
      |> Enum.map(&Enum.at(primes, &1 - 1))
      |> Enum.map(&IO.write("#{&1} "))
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
    |> Enum.reject(fn x -> x == "" end)
    |> Enum.map(&String.split(&1, " "))
    |> Enum.map(&to_int/1)
    |> tl()
    |> hd()
  end

  def aux_reduce(input, acc) do
    if length(trial_division(3, check_parity(input, []))) == 1 do
      acc ++ [input]
    else
      acc
   end
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def check_parity(input, accumulator) do
    if rem(input, 2) == 0 do
      check_parity(div(input, 2), accumulator ++ [2])
    else
      [input, accumulator]
    end
  end

  def trial_division(divider, [input, accumulator]) do
    if divider * divider <= input do
      if rem(input, divider) == 0 do
        trial_division(divider, [div(input, divider),
          accumulator ++ [divider]])
      else
        trial_division(divider + 2, [input, accumulator])
      end
    else
      if input != 1, do: accumulator ++ [input], else: accumulator
    end
  end
end

PrimeNumbersGeneration.main()

# cat DATA.lst | elixir ludsrill.exs
# 1564907 1739641 2294807 2224753 2018677 2309551 1354013 1521991 1608571
