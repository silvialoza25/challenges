;; $ clj-kondo --lint nickkar.cljs
;; linting took 82ms, errors: 0, warnings: 0

(ns nickkar
  (:gen-class)
  (:require [clojure.core :as core]))

(def numberlst (range 2 3000000))

(defn read-input ([n] (read-input n 0 []))
  ([n i out]
   (cond
     (>= i n) out
     :else (read-input n (inc i) (conj out (core/read)))
     ))
  )

(defn primelst ([] (primelst 0 numberlst))
  ([i lst]
   (cond
     (>= (nth lst i) 1733) (vec lst)
     :else
     (let [actual (nth lst i)
           newlist (filter #(or (not= (mod % actual) 0) (= % actual)) lst)]
       (primelst (inc i) newlist)
       )
     ))
  )

(defn nth-prime ([n input primes] (nth-prime n input primes 0 []))
  ([n input primes i out]
   (cond
     (>= i n) out
     :else
     (nth-prime
      n input primes (inc i) (conj out (nth primes (dec (nth input i)))))
     )
   ))

(defn main []
  (let [n (core/read)
        input (read-input n)
        primenumlst (primelst)]
    (apply println (nth-prime n input primenumlst))
    ))

(main)

;; $ cat DATA.lst | clj -M nickkar.cljs
;; 1332283 1429553 1761883 2374553 1776967 1370053 2605013 1592761 1828637
