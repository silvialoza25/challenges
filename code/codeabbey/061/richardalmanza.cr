#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 1.63 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

LIMIT = 10_000_000_i64

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_i}

# Sieve of Atkin

s = [1, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 49, 53, 59]
s1 = Set{1, 13, 17, 29, 37, 41, 49, 53}
s2 = Set{7, 19, 31, 43}
s3 = Set{11, 23, 47, 59}

primes = {2.to_i64 => true, 3.to_i64 => true, 5.to_i64 => true}
prime_list = [] of Int64

(0.to_i64..(LIMIT // 60)).each do |n|
  s.each do |x|
    num = (60 * n + x).to_i64
    break if num > LIMIT

    primes[num] = false
  end
end

(1..LIMIT).each do |x|
  xx = 4 * x ** 2
  break if xx > LIMIT

  (1.to_i64..LIMIT).step(2) do |y|
    n = xx + y ** 2
    n = n.to_i64
    break if n > LIMIT

    primes[n] = !primes[n] if s1.includes?(n % 60)
  end
end

(1..LIMIT).step(2).each do |x|
  xx = 3 * x ** 2
  break if xx > LIMIT

  (2..LIMIT).step(2) do |y|
    n = xx + y ** 2
    n = n.to_i64
    break if n > LIMIT

    primes[n] = !primes[n] if s2.includes?(n % 60)
  end
end

(2..LIMIT).each do |x|
  xx = 3 * x ** 2
  break if xx - 1 > LIMIT
  (1..x-1).step(2) do |y|
    n = xx - (x - y) ** 2
    n = n.to_i64
    break if n > LIMIT

    primes[n] = !primes[n] if s3.includes?(n % 60)
  end
end

(0..(LIMIT // 60)).each do |n|
  s.each do |x|
    num = 60 * n + x
    num = num.to_i64
    next if num < 7

    num_2 = num * num
    break if num_2 > LIMIT

    if primes[num]
      (0..(LIMIT // 60)).each do |nn|
        s.each do |xx|
          c = num_2 * (60 * nn + xx)
          c = c.to_i64
          break if c > LIMIT

          primes[c] = false
        end
      end
    end
  end
end

primes.keys.sort.each do |x|
  prime_list << x.to_i64 if primes[x]
end

args.each do |x|
  print "#{prime_list[x - 1]} "
end

puts

# $ ./richardalmanza.cr
# 2529089 2018167 2543621 1800619 1955113 2491117 2684933 1422089 2464349
# 1370053 2224753 1761883
