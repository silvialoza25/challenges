{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #061: Prime Numbers Generator - Idris2

module Main
import Data.List
import Data.Maybe
import Data.Strings

%default partial

stoi : String -> Int
stoi s = fromMaybe 0 $ parseInteger {a=Int} s

intAt : Int -> List Int -> Int
intAt 0 (x::xs) = x
intAt k (_::xs) = intAt (k - 1) xs

-- Variant "Guarded Filters"
-- https://wiki.haskell.org/Prime_numbers
primesToGT : Int -> List Int
primesToGT m = sieve [2..m]
  where
  sieve : List Int -> List Int
  sieve (p::xs) = if (p * p > m) then p :: xs
    else p :: sieve [x | x <- xs, mod x p > 0]

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLine
  let
    -- Parse input array
    testCases = map stoi $ words inputData
    -- Generate prime numbers list
    primes = primesToGT 2750131 -- 199999th prime
    -- Take the indexes we need
    results = map (\index => intAt (index - 1) primes) testCases
  -- Print results
  putStrLn $ unwords $ map show results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  2294807 2605013 2329687 2557111 2543621 1340153
  2212009 2573957 2200867 2390207 2544229 1962473
-}
