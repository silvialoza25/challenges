-- $ curry-verify mankar91.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `mankar91'!

import Read

pass :: (Prelude.Ord a, Prelude.Num b) => [a] -> (b, [a])
pass []      = (0, [])
pass [x]     = (0, [x])
pass (x:y:z)
  | x <= y =
    let result = pass (y:z)
    in (first result, x:second result)
  | otherwise =
    let result = pass (x:z)
    in (1 + first result, y:second result)

first :: (a, b) -> a
first (swaps, _) = swaps

second :: (a, b) -> b
second (_, array) = array

bubbleSort :: (Prelude.Num a, Prelude.Ord b) => a -> a -> [b] -> [a]
bubbleSort count swaps array
  | array == second result
  = [count,swaps]
  | otherwise
  = bubbleSort (count + 1) (swaps + first result) (second result)
  where
    result = pass array

main :: Prelude.IO ()
main = do
  getLine
  list <- getLine
  putStrLn (unwords (map show (bubbleSort 1 0 (map readInt (words list)))))

-- $ cat DATA.lst | pakcs -q :load mankar91.curry :eval main :quit
-- 14 85
