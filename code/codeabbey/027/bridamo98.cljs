;; $ clj-kondo --lint bridamo98.cljs
;; linting took 29ms, errors: 0, warnings: 0

(ns bridamo98-027
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn swap [v i j]
  (core/assoc v j (v i) i (v j))
)

(defn bubble-sort [size-input content-input]
  (loop [i 0 passes 0 content content-input swaps 0 flag 1]
    (if (and (< i size-input) (= flag 1))
        (let [[content swaps flag]
          (loop [j 0 content2 content swaps2 swaps flag2 0]
            (if (< j (- size-input 1))
              (let [previus (edn/read-string (get content2 j))
              current (edn/read-string (get content2 (+ j 1)))]
                (if (> previus current)
                  (recur (+ j 1) (swap content2 j (+ j 1)) (+ swaps2 1) 1)
                  (recur (+ j 1) content2 swaps2 flag2)))
              [content2  swaps2 flag2]))]
        (recur (+ i 1) (+ passes 1) content swaps flag))
        [passes swaps])))

(defn main []
  (let [size-input (edn/read-string (core/read-line))
  content-input (str/split (core/read-line) #" ")]
    (doseq [item (bubble-sort size-input content-input)]
      (print item ""))
    (println)))

(main)

;; $ cat DATA.lst | clj bridamo98.clj
;; 18 102
