#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defvar *data*)
(defvar *aux*)
(defvar *temp*)
(defparameter *pass* 0)
(defparameter *swap* 0)
(defparameter *counter* 1)

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(setq *data* (first (cdr (read-data))))

(loop while (/= *counter* 0)
  do(setq *counter* 0)
  (setq *pass* (+ *pass* 1))

  (loop for i from 0 to (- (length *data*) 2)
    do(setq *temp* (elt *data* (+ i 1)))
      (setq *aux* (elt *data* i))

    (when (> *aux* *temp*)
      (setf (elt *data* i) *temp*)
      (setf (elt *data* (+ i 1)) *aux*)
      (setq *counter* (+ *counter* 1))
      (setq *swap* (+ *swap* 1)))))

(format t "~s " *pass*)
(format t "~s " *swap*)

#|
cat DATA.lst | clisp ludsrill.lsp
18 102
|#
