{-
 $ ghc  miyyer1946.hs
   [1 of 1] Compiling Main ( miyyer1946.hs )
   Linking code ...
 $ hlint miyyer1946.hs
   No hints
-}

main :: IO ()
main =  do
  n <- readLn :: IO Int
  xs <- getLine
  let values = map read $ words xs :: [Int]
  let (passCnt, swapCnt) = bubbleSort values 0 0
  putStrLn $ show passCnt ++ " " ++ show swapCnt

bubbleSort :: [Int] -> Int -> Int -> (Int, Int)
bubbleSort values passes swaps
  | swapsCount == 0 = (passes + 1, swaps)
  | otherwise = bubbleSort (reverse newarray) (passes + 1) (swaps + swapsCount)
  where (swapsCount, newarray) = validation 0 [] values

validation :: Int -> [Int] -> [Int] -> (Int, [Int])
validation swapsCount newarray [] = (swapsCount, newarray)
validation swapsCount newarray [x] = (swapsCount, x:newarray)
validation swapsCount newarray (x:y:xs)
    | x <= y = validation swapsCount (x:newarray) (y:xs)
    | otherwise = validation (swapsCount + 1) (y:newarray) (x:xs)

{-
 $ ./miyyer1946
 14 82
-}
