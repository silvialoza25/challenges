{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.Char
import Control.Monad

main = do
  params <- getLine
  let divParams = map read (words params) :: [Int]
  let sizeInput = head divParams
  let k = divParams !! 1
  inputData <- replicateM sizeInput getLine
  let solution = solveAll inputData k
  putStrLn (unwords solution)

solveAll :: [String] -> Int -> [String]
solveAll [] k = []
solveAll sentences k = res
  where
    iSentence = head sentences
    decryptedSentence = decryptSentence iSentence k
    res = decryptedSentence : solveAll (drop 1 sentences) k

decryptSentence :: String -> Int -> String
decryptSentence [] k = []
decryptSentence iSentence k = res
  where
    iChar = head iSentence
    iDecryChar = if iChar == ' ' || iChar == '.'
                   then  iChar
                   else  decryptChar iChar k
    res = iDecryChar : decryptSentence (drop 1 iSentence) k

decryptChar :: Char -> Int -> Char
decryptChar iChar k = res
  where
    nxtAsciiCode = ord iChar - k
    res = if nxtAsciiCode < 65
            then chr (91 - (65 - nxtAsciiCode))
            else chr nxtAsciiCode

{-
$ cat DATA.lst | ./bridamo98
  A NIGHT AT THE OPERA THE ONCE AND FUTURE KING ARE WONDERS MANY TOLD.
  CARTHAGE MUST BE DESTROYED TO US IN OLDEN STORIES.
  AND SO YOU TOO BRUTUS GREENFIELDS ARE GONE NOW MET A WOMAN AT THE WELL.
  THE DEAD BURY THEIR OWN DEAD AND FORGIVE US OUR DEBTS.
  IN ANCIENT PERSIA THERE WAS A KING THE SECRET OF HEATHER ALE.
  LOVEST THOU ME PETER WHO WANTS TO LIVE FOREVER.
-}
