-- $ curry-verify mankar91.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `mankar91'!

import Read

index :: (Prelude.Eq a, Prelude.Num a) => a -> [[b]] -> [b]
index _ []     = []
index n (x:xs) = if n==1 then x else index (n-1) xs

add :: Prelude.Int -> Prelude.Int -> Prelude.Int
add shift letter
  | letter /= ord(' ') && letter /= ord('.')
  = (letter - 65 - shift) `mod` 26 + 65
  | otherwise
  = letter

decryptCaesar :: [Prelude.Char] -> Prelude.Int -> [Prelude.Char]
decryptCaesar message shift = map chr (map (add shift) (map ord message))

readTestCase :: (Prelude.Eq a, Prelude.Num a) =>
  a -> Prelude.Int -> Prelude.IO ()
readTestCase tests shift = do
  message <- getLine
  putStr (decryptCaesar message shift ++ " ")
  if tests == 1
    then do putChar '\n'
    else readTestCase (tests - 1) shift

main :: Prelude.IO ()
main = do
  input <- getLine
  let
    inputlist = words input
    tests = readInt (index 1 inputlist)
    shift = readInt (index 2 inputlist)
  readTestCase tests shift

-- $ cat DATA.lst | pakcs -q :load mankar91.curry :eval main :quit
-- A NIGHT AT THE OPERA THE ONCE AND FUTURE KING ARE WONDERS MANY TOLD. CARTHAG
-- E MUST BE DESTROYED TO US IN OLDEN STORIES. AND SO YOU TOO BRUTUS GREENFIELD
-- S ARE GONE NOW MET A WOMAN AT THE WELL. THE DEAD BURY THEIR OWN DEAD AND FOR
-- GIVE US OUR DEBTS. IN ANCIENT PERSIA THERE WAS A KING THE SECRET OF HEATHER
-- ALE. LOVEST THOU ME PETER WHO WANTS TO LIVE FOREVER.
