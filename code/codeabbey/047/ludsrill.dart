// dartanalyzer ludsrill.dart
// Analyzing ludsrill.dart...
// No issues found!

import 'dart:core';
import 'dart:io';

void main() {
  List<String> input = getData([]);
  int decoderValue = int.parse(input.first.split(" ").last);
  List<String> solution = input
      .sublist(1)
      .map((item) => caesarShiftCipher(item, decoderValue))
      .toList();

  print(solution.join(" "));
}

List<String> getData(List<String>input) {
  String getLine = stdin.readLineSync();
  if (getLine == null) {
    return input;
  } else {
    return getData(input + [getLine]);
  }
}

String caesarShiftCipher(String list, int decoderValue) {
  List<int> decoded =
      list.codeUnits.map((letter) => makeshift(letter, decoderValue)).toList();
  String decodedString =
      decoded.map((item) => String.fromCharCode(item)).toList().join("");
  return decodedString;
}

int makeshift(int letter, int decoderValue) {
  if (letter != 32 && letter != 46) {
    if (letter - decoderValue < 65) {
      return letter - decoderValue + 26;
    } else {
      return letter - decoderValue;
    }
  } else {
    return letter;
  }
}

// cat DATA.lst | dart ludsrill.dart
// AND FORGIVE US OUR DEBTS CARTHAGE MUST BE DESTROYED. THAT ALL MEN ARE
// CREATED EQUAL WHO WANTS TO LIVE FOREVER. ARE WONDERS MANY TOLD. IN ANCIENT
// PERSIA THERE WAS A KING. CALLED IT THE RISING SUN THE SECRET OF HEATHER ALE.
// THE DEAD BURY THEIR OWN DEAD. GIVE YOUR ROOKS BUT NOT DILARAM.
