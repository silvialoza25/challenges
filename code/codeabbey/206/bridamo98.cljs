;; $ clj-kondo --lint bridamo98.cljs
;; linting took 45ms, errors: 0, warnings: 0

(ns bridamo98-027
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.edn :as edn]))

(defn ** [x n] (reduce * (repeat n x)))

(defn dec-to-bin [dec max]
  (loop [num dec res ""]
    (if (not (< num 1))
      (let [mod (mod num 2)]
        (recur (int (Math/floor (/ num 2))) (str mod res)))
      (str (apply str (repeat (- max (count res)) "0")) res))))

(defn bin-to-dec [bin max]
  (loop [i (count bin) res 0]
    (if (> i 0)
      (let [index (edn/read-string (subs bin (- i 1) i))]
        (recur (- i 1) (+ res (* (** 2 (- (- max 1) (- i 1))) index))))
      res)))

(defn get-bin-msg-enc [msg]
  (loop [i 0 result ""]
    (if (< i (count msg))
      (recur (+ i 1) (str result
      (dec-to-bin (int (.charAt (subs msg i (+ i 1)) 0)) 8)))
      result)))

(defn get-bin-msg-dec [msg base32-dec]
  (loop [i 0 result ""]
    (if (< i (count msg))
      (recur (+ i 1) (str result (dec-to-bin
      (edn/read-string ((keyword (subs msg i (+ i 1))) base32-dec)) 5)))
      result)))

(defn encode [msg base32-enc]
  (let [padding (- 5 (mod (count msg) 5))
  com-msg (str msg (apply str (repeat padding padding)))
  bin-msg (get-bin-msg-enc com-msg)]
    (loop [i 0 result ""]
      (if (< i (int (/ (count bin-msg) 5)))
        (recur (+ i 1) (str result
        ((keyword (str (bin-to-dec
        (subs bin-msg (* i 5) (+ (* i 5) 5)) 5))) base32-enc)))
        result))))

(defn decode [msg base32-dec]
  (let [bin-msg (get-bin-msg-dec msg base32-dec)]
    (loop [i 0 result ""]
      (if (< i (int (/ (count bin-msg) 8)))
        (recur (+ i 1) (str result (char (bin-to-dec
        (subs bin-msg (* i 8) (+ (* i 8) 8)) 8))))
        result))))

(defn solve-all [size-input base32-enc base32-dec]
  (loop [i 0 result ""]
    (if (< i size-input)
      (if (= (mod i 2) 0)
        (let [enc-msg (encode (core/read-line) base32-enc)]
          (recur (+ i 1) (str result enc-msg " ")))
        (let [dec-msg (decode (core/read-line) base32-dec)
        len-msg (count dec-msg) pad (edn/read-string
        (subs dec-msg (- len-msg 1) len-msg))
        dec-msg-pad (subs dec-msg 0 (- len-msg pad))]
          (recur (+ i 1) (str result dec-msg-pad " "))))
      result)))

(defn main []
  (let [size-input (edn/read-string (core/read-line))
  base32-dec (hash-map :A "0", :B "1", :C "2", :D "3", :E "4",
  :F "5", :G "6", :H "7", :I "8", :J "9", :K "10", :L "11", :M "12",
  :N "13", :O "14", :P "15", :Q "16", :R "17", :S "18", :T "19",
  :U "20", :V "21", :W "22", :X "23", :Y "24", :Z "25", :2 "26",
  :3 "27", :4 "28", :5 "29", :6 "30", :7 "31")
  base32-enc (hash-map :0 "A", :1 "B", :2 "C", :3 "D", :4 "E",
  :5 "F", :6 "G", :7 "H", :8 "I", :9 "J", :10 "K", :11 "L", :12 "M",
  :13 "N", :14 "O", :15 "P", :16 "Q", :17 "R", :18 "S", :19 "T",
  :20 "U", :21 "V", :22 "W", :23 "X", :24 "Y", :25 "Z", :26 "2",
  :27 "3", :28 "4", :29 "5", :30 "6", :31 "7")]
    (println (solve-all size-input base32-enc base32-dec))
  )
)

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; MNXW23LVNZUXG5DTGU2TKNJV littorals mid dizziest ONUWK4TSMEQHA2LOOQ
;; QGC3DFOJ2GY6JR crystallizing buzzword MFUCA5DSNFTGYZJANVQWK3DTORZG
;; 63JAMVWWK3TEMVSCAZTMOVXXE2LOMU2DINBU duet clambers prerogative fli
;; cks OJSWO4TFONZWS33OGU2TKNJV hornpipes OVXG4YLNMVSDGMZT disunites
;; OBQXEYLMNRQXQMRS playbills taboo falter indigestibles MZWHK23ZEBZG
;; SYRAOJSW4ZDFPJ3G65LTNFXGOMRS placentae ringleader gestured MFXGK43
;; UNBSXI2L2NFXGOMRS unreasonable ethnologist substation painters OVZ
;; WCYTMMUQGM2LTNBSXE3LBNYQHG3LVMRTWSZLSEBTWK6LTMVZHGMRS rushes mosey
;; progeny MNXW4ZTSN5XHIYLUNFXW44ZANFXGG2LTNF3GK3DZEBWG643TMVZSAYLVMJ
;; 2XE3RAONQWY5TBM5STGMZT nebula brainwashed raging epiglottises mobi
;; lizes MFZGG2DJORSWG5DVOJSSA5DSOVZXG2LOM42DINBU beeches pepsin init
;; ializing NRQW2ZLOORZSAYTBOJSWIMRS inaugurals ONXXK3TEOBZG633GNFXGO
;; IDTNRXXAMRS ninja genies larynges incision millisecond OV2HIZLSMFX
;; GGZLTEBXW66TJNZTSA4DVOJ3GSZLXEB2HEYLHMVSGSYLOEBRW63LNMVZGGZLEGU2TK
;; NJV overpass interrogator fluffing urning
