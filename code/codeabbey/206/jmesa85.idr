{-
  $ idris2 jmesa85.idr -o jmesa85
-}

-- CodeAbbey #206: Base-32 Encoding - Idris2

module Main
import Data.List
import Data.Strings

%default partial

-- Gets lines in the Stdin
getLines : IO(List String)
getLines = do
  line <- getLine
  if line == "" then pure []
    else do
      xs <- getLines
      pure (line :: xs)

-- Gets end padding for encoding
getPadding : Int -> String
getPadding num = case num of
  1 => "1"
  2 => "22"
  3 => "333"
  4 => "4444"
  5 => "55555"

-- Gets a single numeric representation of the message
glueCharCodes : List Int -> Integer
glueCharCodes [] = 0
glueCharCodes (x::xs) = (cast x) + (glueCharCodes xs) * 256

-- Gets the Base32 representation for the numeric value
splitChunck : Integer -> String
splitChunck 0 = ""
splitChunck message =
  let
    ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
    b32Index = cast $ mod message 32
    b32Char = strIndex ALPHABET b32Index
  in
    (splitChunck (div message 32) ) ++ (cast b32Char)

encode : String -> String
encode message =
  let
    mLen = cast $ length message
    pLen = 5 - (mod mLen 5)
    padded = message ++ (getPadding pLen)
    charCodes = reverse $ map ord $ unpack padded
    glued = glueCharCodes charCodes
    encoded = splitChunck glued
  in
    encoded

-- Split the bytes from the numeric value
detachCharCodes : Integer -> List Int
detachCharCodes 0 = []
detachCharCodes codes = let charCode = cast $ mod codes 256 in
  detachCharCodes (div codes 256) ++ [charCode]

-- Gets the numeric representation for the Base32 string
getB32Codes : String -> Integer
getB32Codes "" = 0
getB32Codes message =
  let
    ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
    b32Head = strIndex message 0
    b32Index = cast (length $ fst $ break (== b32Head) ALPHABET)
  in
    b32Index + ((getB32Codes (strTail message)) * 32)

decode : String -> String
decode message =
  let
    glued = getB32Codes $ reverse message
    charCodes = detachCharCodes glued
    padded = map chr charCodes
    noPadding = filter (\c => isDigit c == False) padded
  in
    pack noPadding

processTestCase : Int -> String -> String
processTestCase index testCase =
  if index `mod` 2 == 1
    then encode testCase
    else decode testCase

main : IO()
main = do
  -- Read Stdin
  _ <- getLine -- Dicarded
  inputData <- getLines
  let
    -- Add an index to the test cases
    testCases = zip [1..(length inputData)] inputData
    -- Process data
    results = map (\(x, y) => processTestCase (cast x) y) testCases
  -- Print results
  putStrLn $ unwords $ results

{-
  $ cat DATA.lst | ./build/exec/jmesa85
  NVQW443MMF2WO2DUMVZCA4TBOR2GSZLTOQ2DINBU misjudged humbugging
  ON2HS3LZGU2TKNJV blighted footrest correlative
  MVQXG2LFOIQG25LOMNUGSZLTEBWGKY3UOVZGK4TTGU2TKNJV unaffected
  ON2GSY3LOBUW4IDEN5TWO33OMVZTGMZT cliques mildewed scrimped creaked tremolo
  N5RHG5DFORZGSY3BNQQHI4TJOBWGSZLEEBUW4ZDFMVSHGMRS inclinations chum deft woe
  narcissuses ON2GK53BOJSGK43TEBRHE5LOMNUGS3THGU2TKNJV cording entitled
  upstream ON2WE5DPORQWY4ZANFXGI2LWNFSHKYLMEBYGS4DJOQ2DINBU refute landmasses
  astonishment punter
  N5RWG3DVONUW63RANFXHIZLSMNSXA5DTEBZXIYLUMVUG65LTMVZSA3DJNZ2GK3DTGU2TKNJV
  idiots hub foreleg fur sultan
  NZXXE5DIMVZG4IDTORZGK4DUN5RW6Y3DNEQGEYLMMRXGK43TGU2TKNJV quirk commercially
  instrumentation teapot interlarded
  MZXXEZ3JOZUW4ZZAMZQW43TZEBZXI4TJOB2GKYLTMVSDGMZT budgie naturalized
  kerchiefing clouding ditching OJUW65DTGU2TKNJV violin discriminated
  parlance gyrated OZUXGYLTEBZG65DFMQQG2ZLENFQXIZLEEBUW443FOBQXEYLCNR4TGMZT
-}
