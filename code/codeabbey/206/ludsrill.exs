# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.03s to load, 0.1s running 55 checks on 1 file)
# 13 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule Base32Encoding do

  def main do
    data = get_data()
    completed_data = Enum.map(data, fn(x) ->
                      if rem(find_index(x, data), 2) == 0
                      do complete_with_numbers(x, 5 - rem(length(x), 5))
                      else x end end)
    Enum.map(solution(completed_data), &IO.write("#{&1} "))
  end

  def solution(data) do
    table = to_base32_table()
    Enum.map(data, fn(x) ->
      if rem(find_index(x, data), 2) == 0 do
        to_base32(List.flatten(Regex.scan(~r/.{1,5}/, to_binary(x))), table,
          "")
      else
        solve = Base.decode32!(Enum.join(x, ""))
        String.slice(solve, 0..(String.length(solve) - 1 -
                      String.to_integer(String.last(solve))))
      end end)
  end

  def find_index(x, data) do
    Enum.find_index(data, fn y -> y == x end)
  end

  def to_int(list) do
    Enum.map(list, &String.to_integer/1)
  end

  def get_data do
    all_data = List.delete(String.split(IO.read(:stdio, :all), "\n"), "")
    split_data = Enum.map(all_data, &String.split(&1, ""))
    tl(Enum.map(split_data, &Enum.reject(&1, fn x -> x == "" end)))
  end

  def to_binary([head | tail]) do
    to_complete(Integer.to_string(hd(String.to_charlist(head)), 2)) <>
      to_binary(tail)
  end

  def to_binary([]) do
    ""
  end

  def to_base32([head | tail], table, acummulator) do
    aux = hd(Tuple.to_list(Integer.parse(head, 2)))
    to_base32(tail, table, acummulator <> Map.get(table, aux))
  end

  def to_base32([], _, acummulator) do
    acummulator
  end

  def to_complete(binary) do
    if String.length(binary) < 8 do
      to_complete("0" <> binary)
    else
      binary
    end
  end

  def to_base32_table do

    %{0 => "A", 9 => "J", 18 => "S", 27 => "3",
      1 => "B", 10 => "K", 19 => "T", 28 => "4",
      2 => "C", 11 => "L", 20 => "U", 29 => "5",
      3 => "D", 12 => "M", 21 => "V", 30 => "6",
      4 => "E", 13 => "N", 22 => "W", 31 => "7",
      5 => "F", 14 => "O", 23 => "X",
      6 => "G", 15 => "P", 24 => "Y",
      7 => "H", 16 => "Q", 25 => "Z",
      8 => "I", 17 => "R", 26 => "2"}
  end

  def complete_with_numbers(list, add) do
    if add == 0 do
      new_list = list ++ ["5"]
      if rem(length(new_list), 5) != 0 do
        complete_with_numbers(new_list, add)
      else
        new_list
      end

    else
      new_list = list ++ [Integer.to_string(add)]
      if rem(length(new_list), 5) != 0 do
        complete_with_numbers(new_list, add)
      else
        new_list
      end
    end
  end
end

# cat DATA.lst | elixir -r ludsrill.exs -e Base32Encoding.main
# OJQWG23FORSWK4TJNZTSAZDFMZXWOZ3FOJZSAZDFNRSWOYLUMVZTGMZT telegraphic
# luxuriates decrepitude MJSWM5LEMRWGKZBAO5XXIMRS subside expatiate
# mortification blazoning NRQWE6LSNFXHI2BAOJSXG4DPNZZWSYTJNRUXI6JR nonchalantly
# kowtowing clearings OVZGEYLONF5GKIDKN53GSYLMGU2TKNJV italicizing toddy
# sandman nearing NFXHG5LMMF2GS3THEBZWQ5LUMV4WKMRS whelk properties baggy
# OZSXE5DFMJZGCZJAMNQXMZLBOR2GKZBR relocatable NRUWYYLDEBSWYZLDORZGSZTZNFXGOIDC
# MVWGY6LCOV2HI33OOMQHEZLTORZGC2LONFXGOIDTMNUG63DBOJZTGMZT electricity forks
# enrolled impales panhandling ORZGC2LUOMQGM33VNZSGS3THGU2TKNJV smallish
# bumpkin emailing catechise MJUWG5LTOBUWIIDQOJUW64TTEBWWS43BOBYHEZLIMVXHG2LPNZ
# ZTGMZT resinous roach reforestation sunflowers NFXGGYLSNZQXIZJAON2HE33OM5SXEI
# DJNZRWY5LTNFXW44ZAOJSWU33JNZSGK4TTEB2HS4TBNZXGSY3BNQ2DINBU landowners NFXHG5L
# MMF2G64TTEBZXC5LBOR2GK4TFMQQGC3LQNFXGOIDUNBQXIY3IMVZTGMZT dubious sinfully
# sincerely N53WKZBAORZGCZDVMNSSAZDSMVQWI3TPOVTWQ5DTEBZGK5TFOJRGK4TBORSXGIDKN5T
# TGMZT
