{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  let intSizeInput = read sizeInput :: Int
  inputData <- replicateM intSizeInput getLine
  let formattedInputData = map formatData inputData
  let comission = 0.01
  let sharesVolatile = findVolatile formattedInputData comission
  let solution = map fst (filter snd sharesVolatile)
  putStrLn (unwords solution)

findVolatile :: [(String, [Double])] -> Double -> [(String, Bool)]
findVolatile [] comission = []
findVolatile dat comission = res
  where
    share = head dat
    shareStdDeviation = calcStandardDeviation (snd share)
    shareComission = calcComission (snd share) comission
    volatile = if shareStdDeviation >= shareComission * 4
             then True :: Bool
             else False :: Bool
    iRes = (fst share, volatile)
    res = iRes : findVolatile (drop 1 dat) comission

formatData :: String -> (String, [Double])
formatData inputData = res
  where
    divDat = words inputData
    shareName = head divDat
    sharePrices = drop 1 divDat
    intSharePrices = map read sharePrices :: [Double]
    res = (shareName, intSharePrices)

calcStandardDeviation :: [Double] -> Double
calcStandardDeviation prices = res
  where
    sizePrices = fromIntegral (length prices) :: Double
    meanPrices = sum prices / sizePrices
    diffWithMeanToSquare = map (((** 2) . (+ meanPrices)) . (* (-1))) prices
    meanDiff = sum diffWithMeanToSquare / sizePrices
    stdDeviation = sqrt meanDiff
    res = stdDeviation

calcComission :: [Double] -> Double -> Double
calcComission prices comission = res
  where
    sizePrices = fromIntegral (length prices) :: Double
    meanPrices = sum prices / sizePrices
    res = meanPrices * comission

{-
$ cat DATA.lst | ./bridamo98
  JEOP INSX MYTH CKCL BLEP JABA JOOG MARU YUKA VDKL
-}
