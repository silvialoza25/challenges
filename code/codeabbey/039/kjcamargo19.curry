-- $ curry-verify kjcamargo19.curry
-- -----------------------------------------------------------------------
-- curry-verify: Curry programs -> Verifiers (Version 2.0.0 of 02/01/2019)
-- -----------------------------------------------------------------------
-- No properties found in module `kjcamargo19'!

import Float ((^.), i2f, sqrt, truncate)
import IO (getContents)
import List (init, sum, split)

getData :: IO String
getData = do
  content <- getContents
  return content

strToF :: String -> Float
strToF text = read text :: Float

meanPrices :: [Float] -> Float
meanPrices dailyPrices = sum dailyPrices / i2f (length dailyPrices)

quadraticDistance :: Float -> Float -> Float
quadraticDistance mean value = (mean - value) ^. 2

meanDistance :: [Float] -> Float
meanDistance distances = sum distances / i2f (length distances)

getSolution :: String -> String
getSolution dataIn =
  let
    finalData = words dataIn
    prices = map strToF (tail finalData)
    stockName = head finalData
    meanNumber = meanPrices prices
    mDistanc = meanDistance (map (\x -> quadraticDistance meanNumber x) prices)
    standardDeviation = sqrt mDistanc
    brokerComission = meanNumber * 0.01
  in
    if standardDeviation > 4 * brokerComission
      then stockName
      else
        "noVolatile"

main :: IO ()
main = do
  input <- getData
  let
    dataLines = split (=='\n') input
    usefulData = drop 1 (init dataLines)
    answer = map (\x -> getSolution x) usefulData
    finalResult = filter (\x -> x /= "noVolatile") answer
  putStrLn (unwords finalResult)

-- cat DATA.lst | pakcs :load kjcamargo19.curry :eval main :quit
-- BLEP SUGR IMIX FANT ZEOD GOLD FLNT JEOP MYTH ASDF SAKK
