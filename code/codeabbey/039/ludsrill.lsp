#|
$ sblint -v ludsrill.lsp
[INFO] Lint file ludsrill.lsp

clisp -c ludsrill.lsp
Compiling file /home/.../ludsrill.lsp ...
Wrote file /home/.../ludsrill.fas
0 errors, 0 warnings
Bye.
|#

(defun read-data (&optional (read-line))
  (declare (ignore read-line))
  (let (*read-eval*)
    (loop for line = (read-line NIL NIL)
      while line
      collect (read-from-string (concatenate 'string "(" line ")" )))))

(defun share-price-volatility ()
  (let ((data)
        (accumulated)
        (aux-standard-deviation)
        (aux-data)
        (average)
        (comission)
        (standard-deviation))

    (setq data (cdr (read-data)))
    (dolist (item data)
      (setq accumulated 0)
      (setq aux-standard-deviation 0)
      (setq aux-data (subseq item 1))
      (loop for i in aux-data do
        (setq accumulated (+ accumulated i)))
      (setq average (/ accumulated (length aux-data)))
      (setq comission (* 0.01 average))
      (loop for i in aux-data do
        (setq aux-standard-deviation
          (+ (expt (- i average) 2) aux-standard-deviation)))
      (setq standard-deviation
        (sqrt (/ aux-standard-deviation (length aux-data))))
      (when (>= standard-deviation (* comission 4))
        (format t "~s " (first item))))))

(share-price-volatility)

#|
cat DATA.lst | clisp ludsrill.lsp
MYTH OBAM SUGR FANT SLVR GEEK ASDF ZEOD SAKK JOOG DALG GOLD BLEP FOTA
|#
