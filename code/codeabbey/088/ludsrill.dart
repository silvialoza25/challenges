// dartanalyzer ludsrill.dart
// Analyzing ludsrill.dart...
// No issues found!

import 'dart:core';
import 'dart:io';
import 'dart:math';

void main() {
  List<String> input = getData([]);
  List<String> notes = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#",
      "G", "G#"];
  List<int> solution = input.map((note) => getFrequencie(notes, note)).toList();
  print(solution.join(" "));
}

List<String> getData(List<List<String>>input) {
  String getLine = stdin.readLineSync();
  if (getLine == null) {
    return input.sublist(1).first;
  } else {
    List<String> newLine = getLine.split(" ");
    return getData(input + [newLine]);
  }
}

int getFrequencie(List<String> notes, String input) {
  String note = getNote(input);
  int octave = getOctave(input);
  int getIndex = notes.indexOf(note);

  if (octave >= 4 && exceptions(note, octave)) {
    if (note == "A" || note == "A#" || note == "B") {
      return (440 * pow(1.059463, semitones(octave, 4, getIndex, "UP")))
          .round();
    } else {
      return (440 * pow(1.059463, semitones(octave, 5, getIndex, "UP")))
          .round();
    }
  } else if (note == "A" || note == "A#" || note == "B") {
    return (440 * pow(1.059463, semitones(octave, 3, getIndex, "DOWN")))
        .round();
  } else {
    return (440 * pow(1.059463, semitones(octave, 4, getIndex, "DOWN")))
        .round();
  }
}

String getNote(String input) {
  if (input.length == 3) {
    return input.substring(0, 2);
  } else {
    return input.substring(0, 1);
  }
}

int getOctave(String input) {
  return int.parse(input.substring(input.length - 1));
}

bool exceptions(String note, int octave) {
  if (octave == 4 && (note == "A" || note == "A#" || note == "B")) {
    return true;
  } else if (octave > 4) {
    return true;
  } else {
    return false;
  }
}

int semitones(int octave, int octaveNormalizer, int index, String direction) {
  if (direction == "UP") {
    return 12 * (octave - octaveNormalizer) + index;
  } else {
    return 12 * (octave - octaveNormalizer) + index - 12;
  }
}

// cat DATA.lst | dart ludsrill.dart
// 39 82 73 196 98 247 554 117 262 46 208 494 49 104 131 440 659 156 740 62 932
// 44 92
