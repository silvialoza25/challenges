{-
$ ghc bridamo98.hs
  [1 of 1] Compiling Main ( bridamo98.hs, bridamo98.o )
  Linking bridamo98 ...
$ hlint bridamo98.hs
  No hints
-}

import Data.List
import Control.Monad

main = do
  sizeInput <- getLine
  inputData <- getLine
  let c1NoteFreq = 32.7031956625
  let stepsPerOctave = 12
  let solution = findNoteFrequency (words inputData)
                 c1NoteFreq stepsPerOctave
  putStrLn (unwords solution)

findNoteFrequency :: [String] -> Float -> Int -> [String]
findNoteFrequency [] c1NoteFreq stepsPerOctave = []
findNoteFrequency notes c1NoteFreq stepsPerOctave = res
  where
    note = head notes
    noteDeviation = getNoteDeviation note stepsPerOctave
    curRes = show (customRound
             (calcNoteFrequency noteDeviation
             c1NoteFreq stepsPerOctave))
    res = curRes :
          findNoteFrequency (drop 1 notes)
          c1NoteFreq stepsPerOctave

calcNoteFrequency :: Int -> Float -> Int -> Float
calcNoteFrequency noteDeviation c1NoteFreq stepsPerOctave = res
  where
    res = c1NoteFreq *
          (2 ** ((fromIntegral noteDeviation :: Float) /
          (fromIntegral stepsPerOctave :: Float)))

getNoteDeviation :: String -> Int -> Int
getNoteDeviation note stepsPerOctave = res
  where
    charNote = take (length note - 1) note
    numOctave = read (drop (length note - 1) note) :: Int
    octaveDeviation
      | charNote == "C" = 0
      | charNote == "C#" = 1
      | charNote == "D" = 2
      | charNote == "D#" = 3
      | charNote == "E" = 4
      | charNote == "F" = 5
      | charNote == "F#" = 6
      | charNote == "G" = 7
      | charNote == "G#" = 8
      | charNote == "A" = 9
      | charNote == "A#" = 10
      | otherwise = 11
    res = octaveDeviation + stepsPerOctave * (numOctave - 1)

customRound :: Float -> Int
customRound dat =
    if (dat -  fromIntegral(floor dat)) < 0.5
      then floor dat
      else ceiling dat

{-
$ cat DATA.lst | ./bridamo98
  156 139 622 262 330 78 554 349 233 82 294
  73 196 39 880 110 208 131 659 370 698 37
-}
