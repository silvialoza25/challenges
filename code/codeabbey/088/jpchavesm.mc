/*
  $ mmc -f jpchavesm.mc
  $ mmc --make jpchavesm
*/

:- module jpchavesm.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is det.
:- implementation.
:- import_module string, list, int, math, float, char.

:- func innerHalfsteps(string) = int.
innerHalfsteps(Note) = InnerHalfsteps :-
(
  if Note = "C" then InnerHalfsteps = -9
  else if Note = "C#" then InnerHalfsteps = -8
  else if Note = "D" then InnerHalfsteps = -7
  else if Note = "D#" then InnerHalfsteps = -6
  else if Note = "E" then InnerHalfsteps = -5
  else if Note = "F" then InnerHalfsteps = -4
  else if Note = "F#" then InnerHalfsteps = -3
  else if Note = "G" then InnerHalfsteps = -2
  else if Note = "G#" then InnerHalfsteps = -1
  else if Note = "A" then InnerHalfsteps = 0
  else if Note = "A#" then InnerHalfsteps = 1
  else InnerHalfsteps = 2 %B
).

:- func findHalfsteps(string) = int.
findHalfsteps(Note) = Distance :-
(
  split(Note, length(Note)-1, JustNote, OctaveStr),
  det_to_int(OctaveStr) = Octave,
  InnerHalfsteps = innerHalfsteps(JustNote),
  Distance = (Octave - 4) * 12 + InnerHalfsteps
).

:- pred note2frequency(string::in, int::out) is det.
note2frequency(Note, Frequency) :-
(
  RefNote = 440.0, %A4, the international reference set at 440 Hz
  SingleStep = math.pow(2.0, 1.0 / 12.0),
  Halfsteps = findHalfsteps(Note),
  FFrequency = RefNote * float.pow(SingleStep, Halfsteps),
  Frequency = round_to_int(FFrequency)
).

:- pred findFrequencies(list(string), string, string).
:- mode findFrequencies(in, in, out) is det.
findFrequencies([], FinalFreq, strip(FinalFreq)).
findFrequencies([Note | Tail], PartialFreq, FinalFreq) :-
(
  note2frequency(Note, Frequency),
  PartialFreqs1 = PartialFreq ++ " " ++ from_int(Frequency),
  findFrequencies(Tail, PartialFreqs1, FinalFreq)
).

main(!IO) :-
  io.stdin_stream(Stdin, !IO),
  read_lines(Stdin, [], !IO).

:- pred read_lines(io.text_input_stream::in,
  list(string)::in, io::di, io::uo) is det.

read_lines(InFile, FileContents, !IO) :-
  io.read_line_as_string(InFile, Result, !IO),
  (
    Result = ok(Line),
    FileContents1 = FileContents ++ [Line],
    read_lines(InFile, FileContents1, !IO)
  ;
    Result = eof,
    det_drop(1, FileContents, NotesList),
    det_index0(NotesList, 0, NotesStr),
    words_separator(char.is_whitespace, NotesStr) = Notes,
    findFrequencies(Notes, "", Frequencies),
    io.print_line(Frequencies, !IO)
  ;
    Result = error(IOError),
    Msg = io.error_message(IOError),
    io.stderr_stream(Stderr, !IO),
    io.write_string(Stderr, Msg, !IO),
    io.set_exit_status(1, !IO)
  ).

/*
  $ cat DATA.lst | ./jpchavesm
  440 554 39 46 165 73 880 98 698 311 988 41 262 185 415
  147 156 37 139 82 104 587 659
*/
