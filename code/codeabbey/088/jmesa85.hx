/**
  $ haxelib run checkstyle \
    --exitcode \
    --show-parser-errors \
    --nothreads \
    --default-config \
    -s jmesa85.hx
**/

// CodeAbbey #088: Pitch and Notes - Haxe

using StringTools;

class Main {
  static public function main():Void {
    // Read Stdin
    var data = Sys.stdin().readAll().toString().trim().split("\n");
    // Get the test cases
    var testCases:Array<String> = data[1].split(" ");
    // Process each test case
    var results:Array<String> = testCases.map(getFreq);
    // Print results
    trace(results.join(" "));
  }

  // Returns the rounded frequency, in Hertz
  static private function getFreq(note:String):String {
    var notation = [
      'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'];
    var octave = Std.parseInt(note.charAt(note.length - 1));
    var name = note.substring(0, note.length - 1);
    var position = notation.indexOf(name) + 1;
    var halfSteps = ((4 - octave) * 12 + 10 - position) * -1;
    var freq = Math.round(440 * (Math.pow(Math.pow(2, 1 / 12), halfSteps)));
    return Std.string(freq);
  }
}

/**
  $ cp jmesa85.hx Main.hx && cat DATA.lst | haxe -main Main --interp
  440 554 39 46 165 73 880 98 698 311 988
  41 262 185 415 147 156 37 139 82 104 587 659
**/
