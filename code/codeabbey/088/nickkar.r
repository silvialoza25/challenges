# $ lintr::lint('nickkar.r')

#returns the input in a readable vector
formatinput <- function(cases) {
  return(unlist(strsplit(cases, " ")))
}

# notes frequency in 0 octave
base_freq <- function(note) {
  freq <- switch(note,
  "C" = 16.35160, "C#" = 17.32391, "D" = 18.35405, "D#" = 19.44544,
  "E" = 20.60172, "F" = 21.82676, "F#" = 23.12465, "G" = 24.49971,
  "G#" = 25.95654, "A" = 27.50000, "A#" = 29.13524, "B" = 30.86771)
  return(freq)
}

# finds the frequency of the notes based on the note itself and the octave
solve_cases <- function(cases, index=1, out=c()) {
  if (index > length(cases)) {
    return(out)
  }
  else {
    note <- cases[index]
    if (nchar(note) == 3) {
      basenote <- substr(note, 1, 2)
      scale <- as.double(substr(note, 3, 3))
      hz <- round(base_freq(basenote) * (2 ^ scale))

      solve_cases(cases, index + 1, c(out, hz))
    }
    else {
      basenote <- substr(note, 1, 1)
      scale <- as.double(substr(note, 2, 2))
      hz <- round(base_freq(basenote) * (2 ^ scale))

      solve_cases(cases, index + 1, c(out, hz))
    }
  }
}

# Driver code
main <- function() {
    data <- readLines("stdin")
    notes <- formatinput(data[2])
    out <- solve_cases(notes)
    cat(out, "\n")
}

main()

# $ cat DATA.lst | Rscript nickkar.r
# 156 139 622 262 330 78 554 349 233 82 294 73 196 39 880 110 208 131 659 370
# 698 37
