;; clj-kondo --lint ludsrill.cljs
;; linting took 28ms, errors: 0, warnings: 0

(ns ludsrill.046
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [clojure.core :as core]))

(defn get-data []
  (let [input-string (core/read-line)]
    (lazy-seq
      (if (seq input-string)
        (cons (str/split input-string #" ") (get-data))
        nil))))

(defn aux [x i]
  (cond (and (= (get x "1") (get x "2")) (= (get x "1") (get x "3"))
          (not= (get x "1") "-")) [i]
        (and (= (get x "4") (get x "5")) (= (get x "6") (get x "4"))
          (not= (get x "4") "-")) [i]
        (and (= (get x "7") (get x "8")) (= (get x "9") (get x "7"))
          (not= (get x "7") "-")) [i]
        (and (= (get x "1") (get x "4")) (= (get x "7") (get x "1"))
          (not= (get x "1") "-")) [i]
        (and (= (get x "2") (get x "5")) (= (get x "8") (get x "2"))
          (not= (get x "2") "-")) [i]
        (and (= (get x "3") (get x "6")) (= (get x "9") (get x "3"))
          (not= (get x "3") "-")) [i]
        (and (= (get x "1") (get x "5")) (= (get x "9") (get x "1"))
          (not= (get x "1") "-")) [i]
        (and (= (get x "3") (get x "5")) (= (get x "7") (get x "3"))
          (not= (get x "3") "-")) [i]
        :else nil))

(defn make-board [data]
  (let [board (into (sorted-map) (map (fn [x] (hash-map x "-")) data))]
    (loop [i 0 item data board board]
      (let [result board
            solve (aux result i)]
        (if (and (< i 9) (= solve nil))
          (if (even? i)
            (recur (inc i) (rest item) (assoc board (first item) "x"))
            (recur (inc i) (rest item) (assoc board (first item) "o")))
          (if (nil? (first solve))
            (print "0 ")
            (print (core/format "%s " (first solve)))))))))

(defn -main []
  (let [all-data (get-data)
        times (Integer. (first (first all-data)))
        data (rest all-data)]
    (loop [x 0]
      (when (< x times)
        (make-board (nth data x))
        (recur (inc x))))))

(-main)

;; cat DATA.lst | clj ludsrill.cljs
;; 5 9 8 6 0 7 6 8 0 5 9 5
