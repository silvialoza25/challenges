;; $ clj-kondo --lint bridamo98.cljs
;; linting took 83ms, errors: 0, warnings: 0

(ns bridamo98-133
  (:gen-class)
  (:require [clojure.core :as core])
  (:require [clojure.string :as str])
  (:require [clojure.edn :as edn]))

(defn abs [n] (max n (- n)))

(defn ** [x n] (reduce * (repeat n x)))

(defn update-pos-vel [pos vel R limit dt vel-pro]
  (let [next-vel (* vel vel-pro)]
    (if (< pos R)
      [(+ (+ pos (* 2 (- R pos))) (* (* vel -1) dt)) (* next-vel -1)]
      (if (> pos (- limit R))
        [(+ (- pos (* 2 (- R (- limit pos)))) (* (* vel -1) dt))
        (* next-vel -1)]
        [(+ pos (* vel dt)) next-vel]))))

(defn find-final-position [input-data]
  (let [W (edn/read-string (get input-data 0))
  H (edn/read-string (get input-data 1))
  x-i (edn/read-string (get input-data 2))
  y-i (edn/read-string (get input-data 3))
  R (edn/read-string (get input-data 4))
  v-x (edn/read-string (get input-data 5))
  v-y (edn/read-string (get input-data 6))
  A (* (edn/read-string (get input-data 7)) 1) dt 0.01]
    (loop [x x-i y y-i vx v-x vy v-y]
      (let [vel-mag (Math/sqrt (+ (** vx 2) (** vy 2)))]
        (if (< vel-mag 0.1)
          [(int x) (int y)]
          (let [vel-pro (/ (- vel-mag (* A dt)) vel-mag)
          x-pos-vel (update-pos-vel x vx R W dt vel-pro)
          y-pos-vel (update-pos-vel y vy R H dt vel-pro)]
            (recur (get x-pos-vel 0) (get y-pos-vel 0)
            (get x-pos-vel 1) (get y-pos-vel 1))))))))

(defn main []
  (let [input-data (str/split (core/read-line) #" ")]
    (doseq [item (find-final-position input-data)]
      (print item ""))
      (println)))

(main)

;; $ cat DATA.lst | clj bridamo98.cljs
;; 136 176
