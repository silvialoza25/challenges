# mix credo --strict --files-included ludsrill.exs
# Generated credo app
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.02s to load, 0.1s running 55 checks on 1 file)
# 9 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule SnakeArcade do
  def main do
    all_data = IO.read(:stdio, :all)
    snake = %{1 => 3, 2 => 2, 3 => 1}
    try do
      data = get_data(all_data)
      board = List.flatten(Enum.slice(data, 0..12))
      movements = ["R"] ++ List.last(data)
      food = Enum.reduce(1..273, [], fn item, acc ->
        if Enum.at(board, item - 1) == "$" do acc ++ [item] else acc end end)
      solution = arcade(snake, movements, "", food, 0)
      head_snake = Map.get(hd(solution), 1)
      x_position = rem(Map.get(hd(solution), 1) - 1, 21)
      y_position = div(head_snake, 21)
      steps = List.last(solution)
      IO.write("#{x_position} #{y_position} #{steps}")
    rescue
      _ -> IO.puts("Unexpected entry")
    end
  end

  def arcade(snake, [], _direction, _food, counter), do:  [snake, counter]
  def arcade(snake, movements, direction, food, counter) do
    if (hd(movements) == "U" or hd(movements) == "D" or
        hd(movements) == "R" or hd(movements) == "L") do
      arcade(snake, tl(movements), hd(movements), food, counter)
    else
      head_aux = String.to_integer(hd(movements))
      [new_snake, new_food, steps] =
        make_movements(snake, food, head_aux, direction, head_aux)
      max_map_values = found_colission(new_snake)
      if max_map_values >= 2 do
        [new_snake, counter + 1]
      else
        arcade(new_snake, tl(movements), direction, new_food, counter + steps)
      end
    end
  end

  def define_direction(food, snake, steps, value, count, direction) do
    [new_food, new_snake] = update_snake(food, snake, steps)
    max_map_values = found_colission(new_snake)
    if max_map_values >= 2 do
      [new_snake, new_food, value - count]
    else
      make_movements(new_snake, new_food, count - 1, direction, value)
    end
  end

  def make_movements(snake, food, count, direction, value) do
    if count == 0 do
      [snake, food, value - count]
    else
      case direction do
        "R" -> define_direction(food, snake, 1, value, count, direction)
        "L" -> define_direction(food, snake, -1, value, count, direction)
        "U" -> define_direction(food, snake, -21, value, count, direction)
        "D" -> define_direction(food, snake, 21, value, count, direction)
      end
    end
  end

  def found_colission(new_snake) do
    Enum.max(Map.values(Enum.reduce(Map.values(new_snake), %{},
      fn item, acc -> Map.update(acc, item, 1, &(&1 + 1)) end)))
  end

  def update_snake(food, snake, change) do
    if Enum.find(food, fn item -> item == Map.get(snake, 1) + change end) do
      [List.delete(food, Map.get(snake, 1) + change),
      Enum.reduce(map_size(snake) + 1..1, %{},
        fn item, acc -> if item != 1 do
          Map.update(acc, item, Map.get(snake, item - 1),
            fn _position -> Map.get(snake, item - 1) end)
        else
          Map.put(acc, item, Map.get(snake, 1) + change) end end)]
    else
      [food,
      Enum.reduce(map_size(snake)..1, %{},
        fn item, acc -> if item != 1 do
          Map.update(acc, item, Map.get(snake, item - 1),
            fn _position -> Map.get(snake, item - 1) end)
        else
          Map.put(acc, item, Map.get(snake, 1) + change) end end)]
    end
  end

  def get_data(all_data) do
    String.split(all_data, "\n")
    |> Enum.reject(fn item -> item == "" end)
    |> Enum.map(&String.split(&1, " "))
  end
end

SnakeArcade.main

# cat DATA.lst | elixir ludsrill.exs
# 10 1 113
