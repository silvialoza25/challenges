# mix credo --strict --files-included ludsrill.exs
# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.05 seconds (0.02s to load, 0.03s running 55 checks on 1 file)
# 2 mods/funs, found no issues.

# Use `mix credo explain` to explain issues, `mix credo --help` for options.

defmodule MatchingWords do

  def main do
    data = String.split(IO.read(:stdio, :all), " ")
    hashmap = (Enum.sort(Enum.reduce(data, %{},
                fn x, acc -> Map.update(acc, x, 1, &(&1 + 1)) end)))
    Enum.map(hashmap, fn {a, b} -> if b != 1 do IO.write("#{a} ") end end)
  end

end

# cat DATA.lst | elixir -r ludsrill.exs -e MatchingWords.main
# bif bih bip biq bot buc daq dek duc dut dyq gep gic gis got gup guq gyf gyk
# jap jaq jep jip jyp jyq jyx lep lif lup lyc lyf mah mas meh mep mih mip miq
# moh myx noq nuh nyk nys rac rof ryc vat vax vif voh zax zet zik zuq
