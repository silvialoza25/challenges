{-
  $ idris2 bridamo98.idr -o bridamo98
-}

module Main
import Data.List
import Data.Maybe
import Data.Strings
import Prelude

rmdups : Eq a => List a -> List a
rmdups [] = []
rmdups (x::xs) =
  let
    res =
      if x `elem` xs
        then rmdups xs
        else x :: rmdups xs
  in res

numTimesFound : Eq a => a -> List a -> Nat
numTimesFound x xs = (length . filter (== x)) xs

matchingWords : List String -> List String -> List String
matchingWords fullText [] = []
matchingWords fullText filterText =
  let
    curWord = fromMaybe "" (head' filterText)
    repetitions = numTimesFound curWord fullText
    res =
      if repetitions > 1
        then curWord :: (matchingWords fullText (drop 1 filterText))
        else matchingWords fullText (drop 1 filterText)
  in res

main : IO ()
main = do
  text <- getLine
  let
    divText = words text
    filterText = rmdups divText
    solution = sort (matchingWords divText filterText)
  putStrLn (unwords solution)

{-
  $ cat DATA.lst | ./build/exec/bridamo98
  bah bic bop buc buk byc byk byq bys deh dif dip dok dop gat gek gep
  goc gof gut gyt jak jih jip jiq juh jyq leh liq lis lus lyf lyp mak
  meq mic mik nac nax neh nuh ret rih roq ros vah vas vek vet vic vot
  vuf vux zef zeh zek zex zih zik zux
-}
