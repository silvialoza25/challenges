#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "Usage: container.sh port"
  else
    docker run --rm -d --name juiceshop -p "$1:3000" -t bkimminich/juice-shop
fi
