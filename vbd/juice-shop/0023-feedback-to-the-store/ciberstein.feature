## Version 1.4.1
## language: en

Feature:
  TOE:
    OWASP Juice Shop
  Location:
    http://localhost:3000/#/contact
  CWE:
    CWE-20: Improper Input Validation
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Alter the requirements for send the feedback at the site
  Recommendation:
    Use validation for inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows         | 10.0.10240    |
    | Opera           | 57.0.3098.110 |
  TOE information:
    Given I'm running Juice Shop standalone jar application
    And using Node.js 10.15.0 (x64) to run it
    And I run the following command in the same ToE folder
    """
    > npm start
    """
    Then I go to the url
    """
    http://localhost:3000/#/contact
    """
    And I'm accessing the Juice Shop site through my browser

  Scenario: Normal use case
  The site allows the user to write a review.
    Given The access to the website url
    Then I write the review
    Then I can see that the site not allow send reviews without rating

  Scenario: Static detection
  Inspect source code
    When I look at the code
    Then I can see that the input button is disabled
    """
    <button _ngcontent-c17 color="primary" id="submitButton"
    mat-raised-button style="margin-top:5px;" type="submit"
    class="mat-raised-button mat-primary" disabled>
    """
    Then I can conclude that this code can be altered

  Scenario: Dynamic detection
  Change the parameters
    Given the website review form
    Then I write a comment
    """
    Hola Mundo
    """
    Then I delete the parameter "disable" from the source code
    Then I can conclude that the input is not validated

  Scenario: Exploitation
  Sending without rating
    Given I know that input is not validated
    Then I proceed to send the comment
    Then I saw that the comment is sent successfully
    And I see the comment in the "About" page [evidence](feed.png)
    Then I can conclude that this site requires some conditional in the input.

  Scenario: Remediation
  Use conditionals in the form
    Given that the rating is required
    Then create a condition that validate the input data
    """
    (Example)
    <?php
    if(rating =! '')
      submit
    else
      alert("Please rating");
    ?>
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.7/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:C/C:N/I:N/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.5/10 (Medium) - E:F/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    0.0/10 (None) - IR:L/AR:L/MAV:N/MPR:N/MUI:R/MC:N/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-01-09