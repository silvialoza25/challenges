## Version 1.4.1
## language: en

Feature:
  TOE:
    Firing Range
  Category:
    Injection Flaws
  Location:
    firing-Range/angular/angular_body_raw_post/1.6.0 - q (parameter)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Execute arbitrary JS
  Recommendation:
    Sanitize user input

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to https://firing-range/angular/angular_body_raw_post/1.6.0
    Then I seng a query
    And I see it reflected

  Scenario: Static detection
  No input sanitization
    When I look at "firing-range/src/tests/angular/Angular.java"
    """
    53  if (containsChars(echoedParam, '<', '>')) {
    54      Responses.sendError(response, "Invalid param.", 400);
    55      return;
    56    }
    57    String template;
    58    try {
    59      template = generateTemplate(
    60          pathInfo.split("/")[0], pathInfo.split("/")[1], echoedParam);
    """
    Then I see it doesn't sanitize input beyond "<>"

  Scenario: Dynamic detection
  Angular fuzzing
    Given I send a query with an Angular XSS Payload
    """
    {{constructor.constructor('alert(1)')()}}
    """
    Then I get an alert
    Then the page is vulnerable to XSS

  Scenario: Exploitation
  There is no way to meaningfully exploit vulnerabilities in this ToE

  Scenario: Remediation
  Sanitize input
    Given I patch the code like this
    """
    import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;
    ...
    53  if (containsChars(echoedParam, '<', '>')) {
    54      Responses.sendError(response, "Invalid param.", 400);
    55      return;
    56    }
    57    String template;
    58    try {
    59      template = generateTemplate(
    60          pathInfo.split("/")[0], pathInfo.split("/")[1],
    escapeHtml(echoedParam));
    ...
    """
    Then it's not vulnerable to XSS anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.7/10 (Low) - AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.5/10 (Low) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.5/10 (Low) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-17
