## Version 1.4.1
## language: en

Feature:
  TOE:
    born2root-2
  Location:
    http://192.168.123.129/joomla
  CWE:
    CWE-264: Permissions, Privileges, and Access Controls
    https://cwe.mitre.org/data/definitions/264.html
    CWE-434: Unrestricted Upload of File with Dangerous Type
    https://cwe.mitre.org/data/definitions/434.html
    CWE-200: Exposure of Sensitive Information to an Unauthorized Actor
    https://cwe.mitre.org/data/definitions/200.html
  Rule:
    R265. Restrict access to critical processes
    https://fluidattacks.com/products/rules/list/265/
    R261. Avoid exposing sensitive information
    https://fluidattacks.com/products/rules/list/261/
    R186. Use the principle of least privilege
    https://fluidattacks.com/products/rules/list/186/
  Goal:
    Escalate privileges to root and capture the flag
  Recommendation:
    Keep control over sensitive data exposure and patch CMS vulnerabilities

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Parrot OS       | 4.11      |
    | Google Chrome   | 89.0.4    |
    | Nmap            | 7.9.1     |
    | Vmware 16 pro   | 16.1.0    |
    | Netcat          | 1.10-46   |
    | Dirb            | 2.2.2     |
    | Joomscan        | 0.0.7     |

  TOE information:
    Given the Born2rootv2 OVA file
    When I run it on Vmware
    And I see only the Born2Root session [evidence](1.png)
    Then I check the Virtual Machine IP address [evidence](2.png)
    """
    sudo netdiscover -i vmnet 8
    192.168.123.129
    """
    Then I scan the IP with Nmap [evidence](3.png)
    """
    22/tcp open ssh OpenSSH 6.7p1 Debian 5+deb8u4 (protocol 2.0)
    80/tcp open http Apache httpd 2.4.10 ((Debian))
    111/tcp open rpcbind 2-4 (RPC #100000)
    43842/tcp open status 1 (RPC #100024)
    """
    And I can see 4 open ports
    And I realize that it is a Linux Debian machine

  Scenario: Normal use case
    Given the open port "80/tcp"
    When I visit the IP address "192.168.59.129"
    And I realize that it is a website [evidence](4.png)

  Scenario: Static detection
    Given the website
    When I check the directories using dirb [evidence](5.png)
    """
    dirb http://192.168.123.129/
    """
    Then I found a Joomla website [evidence](6.png)
    And I scan the website with Joomscan [evidence](7.png)
    """
    joomscan -u http://192.168.123.129/joomla/
    """
    And I found some vulnerabilities
    When I try with default credentials [evidence](8.png)
    """
    username:admin
    password:travel,football,music
    """
    And I got access to Joomla admin user [evidence](9.png)

 Scenario: Dynamic detection
    Given the Joomla admin user
    When I go to "joomla/administrator"
    And I enter the same credentials
    And I got access to the Joomla administrator website [evidence](10.png)
    And I go to templates session [evidence](11.png)
    And I see some files that can be modified

 Scenario: Exploitation
    Given the templates website
    When I modify "error.php" with a PHP reverse shell [evidence](12.png)
    Then I put the terminal into listen mode
    """
    nc -nlvp 1234
    """
    And I go to the page "error.php"
    And I got shell access [evidence](13.png)
    Then I put the shell to interactive mode
    """
    python -c 'import pty;pty.spawn("/bin/bash");'
    """
    And I start to navigate into the folders
    """
    ls -la
    cd opt/scripts
    """
    When I found a Python file "fileshare.py"
    And I see the content file [evidence](14.png)
    """
    cat fileshare.py
    """
    And I realize about some credentials
    When I try to login with the credentials
    """
    user:tim
    pass:lulzlol
    """
    And I got access to "tim" user [evidence](15.png)
    Then I realize that I have all permissions [evidence](16.png)
    When I run "sudo su" [evidence](17.png)
    And I got root user
    And I go to the home directory
    """
    cd $HOME
    """
    Then I list all the files "ls -la" [evidence](18.png)
    And I realize about the file "flag.txt"
    When I see the file content
    And I got the flag [evidence](19.png)

  Scenario: Remediation
    When I do security test to the Web Apps sensitive data
    Then I can enforce the environment hardening
    And I can enforce a user and password control
    And I try to keep update CMS version
    And I try to keep update Plugins and Templates
    Then I avoid many vulnerabilities that could be attack vectors

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.0/10 (Critical) - AV:N/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.1/10 (Critical) - E:F/RL:O/RC:U/
  Environmental: Unique and relevant attributes to a specific user environment
    5.1/10 (Critical) - CR:M/IR:M/AR:X/MAV:X/MAC:X/MPR:X/

  Scenario: Correlations
    No correlations have been found to this date 2021-04-12
