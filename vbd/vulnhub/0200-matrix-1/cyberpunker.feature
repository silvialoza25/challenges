## Version 1.4.1
## language: en

Feature:
  TOE:
    matrix-1
  Location:
    192.168.123.132
  CWE:
    CWE-200: Exposure of Sensitive Information to an Unauthorized Actor
    https://cwe.mitre.org/data/definitions/200.html
    CWE-264: Permissions, Privileges, and Access Controls
    https://cwe.mitre.org/data/definitions/264.html
    CWE-78: OS Command Injection
    https://cwe.mitre.org/data/definitions/78.html
  Rule:
    R261. Avoid exposing sensitive information
    https://fluidattacks.com/products/rules/list/261/
    R186. Use the principle of least privilege
    https://fluidattacks.com/products/rules/list/186/
    R173. Discard unsafe inputs
    https://fluidattacks.com/products/rules/list/173/
  Goal:
    Escalate privileges to root and get the flag
  Recommendation:
    Keep control over sensitive data exposure and third-party applications

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Parrot OS       | 4.11      |
    | Google Chrome   | 89.0.4    |
    | Nmap            | 7.9.1     |
    | Vmware 16 pro   | 16.1.0    |
    | OpenSSH         | 8.5p      |
    | Crunch          | 3.6       |
    | Hydra           | 9.1       |
    | Netdiscover     | 0.7       |

  TOE information:
    Given the Matrix OVA file
    When I run it on Vmware
    And I see only the Login session [evidence](1.png)
    Then I check the Virtual Machine IP address [evidence](2.png)
    """
    sudo netdiscover -i vmnet 8
    192.168.123.132
    """
    Then I scan the IP with Nmap [evidence](3.png)
    """
    22/tcp open ssh OpenSSH 7.7 (protocol 2.0)
    80/tcp open http SimpleHTTPServer 0.6 (Python 2.7.14)
    31337/tcp open http SimpleHTTPServer 0.6 (Python 2.7.14)
    """
    And I can see 3 open ports
    And I realize that it is a Linux machine

  Scenario: Normal use case
    Given the open port "80/tcp"
    When I visit the IP address "192.168.123.132"
    And I realize that it is a website [evidence](4.png)
    Then I go to the IP address with the port "31337/tcp"
    And I see another website [evidence](5.png)

  Scenario: Static detection
    Given the website
    When I check the source code [evidence](6.png)
    And I found an encrypted message
    And I realize that the message encryption is "Base64"
    Then I decrypted the code [evidence](7.png)
    And I got a file "Cypher.matrix" [evidence](8.png)
    When I see the file content [evidence](9.png)
    And I realize that it is "BrainFuck" message
    And I decoded the message [evidence](10.png)
    And I see another message with a tip
    """
    You can enter into the matrix as a guest, with password k1ll0rXX
    Note: Actually, I forget last two characters so I have replaced
    with XX try your luck and find correct string of password
    """
    Then I created a dictionary for the last two characters [evidence](11.png)

  Scenario: Dynamic detection
    Given the dictionary password
    When I try to brute-forcing the SSH service with the dictionary
    """
    hydra -l guest -P dir.txt 192.168.123.132 ssh
    """
    And I got the correct password [evidence](12.png)

  Scenario: Exploitation
    Given the password
    When I try to enter to the SSH service
    Then I got access to the SSH service [evidence](13.png)
    And I check the PATH environment [evidence](14.png)
    """
    -rbash: /home/guest/prog:
    """
    Then I check the executables allowed in the environment [evidence](15.png)
    """
    -rbash: /home/guest/prog/vi
    /bin/rbash
    """
    And I realize about VIM is allowed
    And I try to gain a SHELL with VIM [evidence](16.png)
    And I exported the environment variables
    """
    export SHELL=/bin/bash:$SHELL
    export PATH=/usr/bin:$PATH
    export PATH=/bin:$PATH
    """
    Then I try to gain root access with "sudo su"
    And I got root access [evidence](17.png)
    Then I go to the root directory
    And I see the file "flag.txt" [evidence](18.png)
    When I see the file content
    And I got the flag [evidence](19.png)

  Scenario: Remediation
    When I do security test to the Web Apps sensitive data
    Then I can enforce the environment hardening
    And I can enforce a user and password control
    And I can patch third party applications
    Then I avoid many vulnerabilities that could be attack vectors

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.5/10 (Critical) - AV:N/AC:H/PR:H/UI:N/S:C/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.2/10 (Critical) - E:P/RL:X/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.3/10 (Critical) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:L/

  Scenario: Correlations
    No correlations have been found to this date 2021-04-13
