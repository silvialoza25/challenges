## Version 1.4.1
## language: en

Feature:
  TOE:
    vulnhub
  Location:
    http://pwned.vm
  CWE:
    CWE-288: Authentication Bypass Using an Alternate Path or Channel
  Rule:
    REQ.R122: R122. Validate credential ownership
  Goal:
    Gain root access
  Recommendation:
    Manage docker group members and permissions

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Debian          |   stretch   |
    | Chrome          |     84      |
    | netdiscover     |  0.3-beta7  |
    | nmap            |    7.40     |
    | gobuster        |    3.0.1    |
    | VirtualBox      |   6.1.12    |
    | ftp             |     1.0     |
    | ssh             |     1.0     |
  TOE information:
    Given I am accessing the site 192.168.1.12
    And I added "pwned.vm" to my hosts file:
    """
    192.168.1.12   pwned.vm
    """
    And is running on Debian 8 and is on VirtualBox

  Scenario: Normal use case
    Given I access "pwned.vm" I see an html page with some text

  Scenario: Static detection
    Given I just have access to the html code
    Then I try to brute-force some URIs using "gobuster"
    And I find a path called "/hidden_text"
    When I inspect the page source code I find some FTP credentials

  Scenario: Dynamic detection
    Given that I have some FTP credentials I try to connect
    And for that I use FTP protocol and the mentioned credentials
    Then I find a private key for SSH connection and a username
    And I connect through SSH protocol

  Scenario: Exploitation
    Given that I have access to the system
    Then I execute the following command:
    """
    $ id
    """
    Then I get the output:
    """
    uid=1001(selena) gid=1001(selena) groups=1001(selena),115(docker)
    """
    Then I see that the user can execute docker commands
    Then I execute the following line in the shell:
    """
    $ docker run -v /:/mnt --rm -it alpine chroot /mnt sh
    """
    And I have gained root access

  Scenario: Remediation
    Given well manage of group members and permissions

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.8/10 (High) - AV:L/AC:H/PR:L/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 (High) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.2/10 (High) - CR:H/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date {2020-08-24}
