#!/bin/bash
filename='DATA.lst'
n=1
while read line; do
  if openssl rsa -in cipher.key -out out.key -passin pass:"$line" 2>/dev/null
    then
      echo $line
      break
    else
      echo "" > /dev/null
  fi
  n=$((n+1))
done < $filename
