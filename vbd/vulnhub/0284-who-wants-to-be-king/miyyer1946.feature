## Version 1.4.1
## language: en

Feature:
  TOE:
    who-wants-to-be-king
  Category:
    Improper Access Control
  Location:
    http://192.168.0.13:80
  CWE:
    CWE-284: Improper Access Control
  Rule:
    REQ.043: https://fluidattacks.com/web/rules/043/
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Do not allow downloading of files if they are not needed

  Background:
  Hacker Software:
    | <Software name>               | <Version> |
    | Kali Linux                    |  2020.3   |
    | Nmap                          |  7.80     |
    | VirtualBox                    |  6.1      |
    | Firefox                       |  79.0     |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on Virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    Then I proceed to make a scan with nmap on my network with the command
    """
    kali@kali:~$ nmap -sn 192.168.0.1/24

    Starting Nmap 7.80 ( https://nmap.org ) at 2021-01-13 09:07 EST
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.0017s latency).
    Nmap scan report for 192.168.0.5 (192.168.0.5)
    Host is up (0.027s latency).
    Nmap scan report for kali (192.168.0.11)
    Host is up (0.00078s latency).
    Nmap scan report for osboxes (192.168.0.13)
    Host is up (0.00031s latency).
    Nmap done: 256 IP addresses (4 hosts up) scanned in 3.10 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.13

  Scenario: Normal use case
    Given I access the site http://192.168.0.13:80/
    And the Index of / homepage is displayed [evidences](02.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the website does not show any vulnerabilities with the naked eye
    When I Download the file skeylogger
    Then I read the file it is unreadable and the strings tool is used
    """
    kali@kali:~/Downloads$ strings skeylogger
    """
    Given the results I observe a string in base64
    Then I decode [evidences](03.png)
    """
    kali@kali:~$ echo "ZHJhY2FyeXMK" | base64 -d
    dracarys
    """
    Given the result I using SSH access the server User:daenerys
    """
    kali@kali:~$ ssh daenerys@192.168.0.13
    daenerys@192.168.0.13's password:
    Last login: Tue Dec  1 11:38:40 2020 from 192.168.0.105
    """
    Then I check for the Inappropriate Access Control vulnerability
    And the Remote Code Execution vulnerability is confirmed

  Scenario: Exploitation
    Given the vulnerability and accessed the system
    Then I look for vulnerable files for a possible privilege escalation
    """
    daenerys@osboxes:~$  ls
    Desktop  Documents  Downloads  Music  Pictures  Public  secret  Templates
    daenerys@osboxes:~$ cat secret

    find home, pls
    daenerys@osboxes:~$ cd .local/share/
    daenerys@osboxes:~/.local/share$ ls
    daenerys.zip  evolution  flatpak  gnote  nano
    daenerys@osboxes:~/.local/share$ unzip daenerys.zip
    Archive:  daenerys.zip
    extracting: djkdsnkjdsn
    daenerys@osboxes:~/.local/share$ cat djkdsnkjdsn
    /usr/share/sounds/note.txt

    daenerys@osboxes:~/.local/share$ cat /usr/share/sounds/note.txt
    I'm khal.....
    """
    Given it is not clear what the password is for the root user
    Then I try to google the given clue "I'm khal ....." [evidences](04.png)
    And the results show a possible password
    """
    daenerys@osboxes:~/.local/share$ su root
    Password:
    root@osboxes:/home/daenerys/.local/share# cd /root
    root@osboxes:~# ls
    nice.txt
    root@osboxes:~# cd nice.txt
    bash: cd: nice.txt: Not a directory
    root@osboxes:~# cat nice.txt
    ¡Congratulation!


    You have a good day!



    aHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1nTjhZRjBZZmJFawo=

    root@osboxes:~# id
    uid=0(root) gid=0(root) groups=0(root)
    """
    Given the flag could be read and the root user was obtained
    And the challenge is completed successfully

  Scenario: Remediation
    Given root permissions, the apache2.conf file is reviewed
    """
    <Directory /var/www/>
            Options Indexes FollowSymLinks
            AllowOverride None
            Require all granted
    </Directory>
    """
    When I observe that the file allows access to the path /var/www/html
    Then a skeylogger file access vulnerability is identified
    And which was subject to vulnerability
    """
    <Directory /var/www/>
            Options Indexes FollowSymLinks
            AllowOverride None
            Require all denied
    </Directory>
    """
    Given the permissions of the path /var/www/html are denied
    Then the vulnerability is corrected [evidences](05.png)
    And another option is to remove the file from the path /var/www/html
    Then the vulnerable file is not available for download [evidences](06.png)

  Scenario: Scoring
   Severity scoring according to CVSSv3 standard
   Base: Attributes that are constants over time and organizations
     10/10 (high) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
   Temporal: Attributes that measure the exploit's popularity and fixability
     7/10 (high) - E:H/RL:O/RC:C
   Environmental: Unique and relevant attributes to a specific user environment
     7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2021-01-13
