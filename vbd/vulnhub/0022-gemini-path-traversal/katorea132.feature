## Version 1.4.1
## language: en

Feature:
  TOE:
    gemini
  Category:
    Path Traversal
  Location:
    http://192.168.1.132/Portal/index.php?view=about-us.html
  CWE:
    CWE-22: Improper Limitation of a Pathname to a Restricted Directory
  Rule:
    REQ.137: Discard unsafe inputs
  Goal:
    Get the user and root flags
  Recommendation:
    Properly sanitize inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      | 2020.4      |
    | Firefox         | 78.3.0esr   |
    | nmap            | 7.91        |
    | nikto           | 2.1.6       |
    | dirbuster       | 1.0         |
    | metasploit      | 6.0.16-dev  |
    | wappalizer      | 6.5.17      |
    | arp-scan        | 1.9.7       |

  TOE information:
    Given I get a .OVA file
    And I use it in VirtualBox
    When it prompts for a user and password
    Then root and blank password do not give access
    When I configure both networks to local network
    Then I proceed to look for the ip with arp-scan
    """
    sudo arp-scan -l
    Interface: eth0, type: EN10MB, MAC: 08:00:27:22:55:93, IPv4: 192.168.1.72
    Starting arp-scan 1.9.7 with 256 hosts
    192.168.1.132  08:00:27:18:c6:8f  PCS Systemtechnik GmbH

    1 packets received by filter, 0 packets dropped by kernel
    """
    And discover the server IP for this case is 192.168.1.132
    Then I scan with nmap for open ports[evidences](openports.png)
    """
    nmap -T4 -p- -A 192.168.1.132
    PORT    STATE SERVICE     VERSION
    21/tcp  open  ftp         vsftpd 3.0.3
    22/tcp  open  ssh         OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
    80/tcp  open  http        Apache httpd 2.4.38 ((Debian))
    139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
    445/tcp open  netbios-ssn Samba smbd 4.9.5-Debian (workgroup: WORKGROUP)
    Service: Host: GEMINI; OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
    """
    And it found 5 open ports
    And not so useful samba information[evidences](scripts.png)

  Scenario: Normal use case
    Given I can access to the site
    When nothing reacts to the user input
    And there is nothing to do with it besides watching.

  Scenario: Static detection
    Given I don't have access to the source code
    Then this can't be done

  Scenario: Fail: Dynamic detection in ftp port
    Given the port 21 is open with the ftp service
    Then I try to access it but it prompts for user and password
    And I can't get anything out of it [evidences](ftp.png)

  Scenario: Fail: Dynamic detection in ssh port
    Given the port 22 is open with the ssh service
    When I try to access it with root username and no password
    Then I can't get access out of it

  Scenario: Fail: Dynamic detection in smb ports
    Given the ports 139 and 455 are open with the smb service
    When I use smbclient to explore it
    Then the passwords are requested
    """
    smbclient -L 192.168.1.132
    Enter WORKGROUP\katorea's password:

        Sharename       Type      Comment
        ---------       ----      -------
        print$          Disk      Printer Drivers
        IPC$            IPC       IPC Service (Samba 4.9.5-Debian)
    Reconnecting with SMB1 for workgroup listing.

        Server               Comment
        ---------            -------

        Workgroup            Master
        ---------            -------
        WORKGROUP            GEMINI

    smbclient \\\\192.168.1.132\\print
    Enter WORKGROUP\katorea's password:
    tree connect failed: NT_STATUS_BAD_NETWORK_NAME

    smbclient \\\\192.168.1.132\\IPC
    Enter WORKGROUP\katorea's password:
    tree connect failed: NT_STATUS_BAD_NETWORK_NAME
    """
    And I can't get anything out of it

  Scenario: Success: Dynamic detection with php url parameters
    Given the port 80 is open with the http service
    When I decide to directory bust it for more clues
    Then I use the dirbuster tool[evidences](dirbuster.png)
    When I find a interesting path "/Portal/index.php"
    Then I access "http://192.168.1.132/Portal/index.php"
    And  I can interact with a button which uses a url parameter "view"
    When I try to change "/index.php?view=about-us.html" the filename
    Then it fails to display anything[evidences](failparam.png)
    Then I attempt to avoid the .php append with a "../../../../etc/passwd/."
    And it worked[evidences](etcpasswd.png)

  Scenario: Success: Exploitation of url parameter
    When I attempt to get ssh key from non system user william
    Then it does contain a private key[evidences](idrsa.png)
    And I try again with root folder doesn't work[evidences](rootidrsa.png)
    Then it's all I can find through this vulnerability

  Scenario: Success: Extracting first flag
    Given I have a private ssh key belonging to the user "william"
    When I connect to the target machine through ssh
    Then I manage to access the machine[evidences](sshwilliam.png)
    And I retrieve the first flag
    | <file>                 | <output>                    | <evidence>     |
    | /home/william/user.txt | srLbBhLRK****************   | userflag.png   |

  Scenario: Success: Privilege Escalation
    Given I have access to the machine as the user "william"
    And I have no access to the root folder[evidences](nothome.png)
    When I look for files that can be used as root
    Then I find that /etc/passwd is writeable by anyone[evidences](passwd.png)
    Then I make a new password for root with OpenSSL[evidences](newpass.png)

  Scenario: Success: Exctracting the last flag
    Given that I have root access
    When I go to the root home folder
    Then I cat the root.txt file
    And I obtain the second flag
    | <file>         | <output>                    | <evidence>     |
    | /root/root.txt | vD1JA8mze****************   | rootflag.png   |

  Scenario: Remediation
    Given that PHP isn't escaping dangerous characters
    And it is capable of path traversing
    Then to fix it, turn on "magic_quotes_gpc" in the php.ini file
    """
    Based on https://stackoverflow.com/a/3508217
    And https://ctf-wiki.github.io/ctf-wiki/web/php/php
    This php function checks if such option is turned on (this is deprecated
    And not recommended if the php version is at or over 5.3.0)
    <?php
        if (get_magic_quotes_gpc()) {
            echo "magic quotes is on! it is escaping special characters";
        } else {
            echo "Please turn on magic quotes";
        }
    ?>
    """
    Then to fix the privilege escalation, fix permission on files
    """
    chmod 600 /home/william/.ssh/id_rsa # This must be done with the
    owner of the file, in this case, william
    chmod 644 /etc/passwd # root is the owner of the file
    """

  Scenario:
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.6/10 (Critical) - AV:N/AC:L/PR:N/UI:R/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.9/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.3/10 (Critical) - CR:L/IR:H/AR:H/MAV:N/MAC:L/MPR:L/MUI:N

  Correlations:
    No correlations have been found to this date {2020-11-19}
