## Version 1.4.1
## language: en

Feature: An accessible file contains the admin credential
   TOE:
    It's October: 1
  Category:
    Exposure of Resource to Wrong Sphere
  Location:
    http://192.168.1.205/backend/cms
  CWE:
    CWE-22: Improper Limitation of a Pathname to a Restricted Directory
  Rule:
    REQ.280: https://fluidattacks.com/web/rules/280/
  Goal:
    Get the root flag of the target.
  Recommendation:
    Don't allow external access to sensitive data

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Ubuntu (Bionic) | 18.04.4 LTS       |
    | Google Chrome   | 80.0.3987.132     |
  TOE information:
    Given access to admin panel
    And I'm in CMS section

  Scenario: Normal use case
    Given the access I could administer all the pages
    And other features of October CMS

  Scenario: Static detection
    Given there isn't a source code
    Then there isn't a static detection scenario either

  Scenario: Dynamic detection
    Given access to sources of the [page sections](cms)
    When I found it has lines with "{{ }}"
    And I wrote '{{ "7" * 7 }}'
    And the web page showed me ['49'](49.png)
    Then I confirmed October CMS use Twig

  Scenario: Exploitation
    Given The function 'source' I read in Twig docs
    Then I use ["{{ source('/etc/passwd') }}"](passwd.png)
    And I got the Users list in [TOE](users.png)

  Scenario: Remediation
    Given there are many ways to remediate this information leak
    And these solutions could be to manage permissions
    And use ACLs to a detailed management
    And isolate the service using jails or container
    But I don't have access to the TOE's root
    Then I couldn't perform any of these solutions yet

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.9/10 (Medium) - AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.7/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.0/10 (Low)     - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-13
