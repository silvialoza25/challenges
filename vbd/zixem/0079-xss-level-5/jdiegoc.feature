## Version 1.4.1
## language: en

Feature:
  TOE:
    zixem
  Category:
    Cross Site Scripting
  Location:
    http://zixem.altervista.org/XSS/5.php?name=zxm&action=5.php - user (field)
  CWE:
    CWE-80: Improper Neutralization of Script-Related HTML Tags
    in a Web Page (Basic XSS)
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Execute an alert with message 1337
  Recommendation:
    Encode the URL and use a filter function for HTML tags

  Background:
  Hacker's software:
    | <Software name>     | <Version>       |
    | Windows 10          | 1809            |
    | Google Chrome       | 75.0.3770.100   |
  TOE information:
    Given I am accessing the site
    When The page displays a message
    Then I realize the URL has a parameter
    When I change that parameter
    Then The message changes with my own parameter
    And A possible vulnerability with XSS could be exposed

  Scenario: Normal use case
    Given The page just displays a message
    When I read it
    Then I can conclude that it is only its functionality

  Scenario: Static detection
    Given There is not access to source code
    Then It is not possible a static detection

  Scenario: Dynamic detection
  Detecting XSS based on DOM
    When The page displays a message
    Then I realize the URL has a parameter
    When I change that parameter: [evidence](evidence.png)
    When I try to enter HTML tags, they are represented correctly
    And I can conclude that the site is vulnerable to cross-site scripts

  Scenario: Exploitation
    When I entered the following string
    """
    name=zxm&action=javascript:alert(1337);
    """
    Then the site executes the JavaScript code: [evidence](evidence1.png)
    And I can conclude that site execute JavaScript code

  Scenario: Remediation
    Given An input filter we can know when an HTML tag is entered in the URL
    Then The filtered HTML tag is passed as only text

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.6/10 (Medium) - E:P/RL:U/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    4.3/10 (High) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-01-16
