## Version 1.4.1
## language: en

Feature:
  TOE:
    Security Tweets
  Category:
    Cross Site Scripting
  Location:
    http://testhtml5.vulnweb.com/#/popular - redir (field)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
      https://cwe.mitre.org/data/definitions/79.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit cross site scripting
  Recommendation:
    Validate malicious URL's from users

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Microsoft Windows | 10              |
    | Mozilla Firefox   | 69.0.3 (64 bit) |
  TOE information:
    Given I access the main page
    And entered a HTML5 website with some news from twitter
    And the page uses Angular 1.0.6
    And Twitter Bootstrap 2.3.1

  Scenario: Normal use case
    When I click on the first headline
    Then a link is activated
    """
    http://testhtml5.vulnweb.com/#/redir?url=
    https://twitter.com/bradhoylman/status/991408461203279872
    """
    And I am redirected to an external website

  Scenario: Static detection
    Given I do not have access to the original 'redir' source code
    Then I can't perform static detection

  Scenario: Dynamic detection
    Given I copy the link of the first headline
    Then I add a piece of javascript code at the end of the link
    """
    http://testhtml5.vulnweb.com/#/redir?url=
    javascript:alert(%22hacked%22)
    """
    When I enter this link in the browser
    Then the alert is displayed
    And I am not redirected to another website
    Then the website is vulnerable to XSS injection

  Scenario: Exploitation
    Given I copy the link of the first headline
    Then I add a piece of javascript at the end of the link
    """
    http://testhtml5.vulnweb.com/#/redir?url=
    javascript:alert(document.cookie)
    """
    When I enter this link in the browser
    Then there is an alert displaying the username information
    And [evidence](evidence.png)
    And this website is exploited with XSS via URL

  Scenario: Remediation
    When there are redirections to external sites
    And URL's exist inside the client side code
    Then URL's should be validated through whitelisting methods
    And the code can block injections with 'javascript:' strings

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.0/10 (Medium) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.0/10 (Medium) - CR:M/IR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-10-25
