## Version 1.4.1
## language: en

Feature:
  TOE:
    Security Tweets
  Category:
    Access Credentials
  Location:
    http://testhtml5.vulnweb.com/#/popular - Username (field)
    http://testhtml5.vulnweb.com/#/popular - Password (field)
  CWE:
    CWE-20: Improper Input Validation
      https://cwe.mitre.org/data/definitions/20.html
  Rule:
    REQ.229 Request access credentials
      https://fluidattacks.com/web/rules/229/
  Goal:
    Check basic security access
  Recommendation:
    Validate input data from the client side

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Microsoft Windows | 10              |
    | Mozilla Firefox   | 69.0.3 (64 bit) |
  TOE information:
    Given I access the main page
    And entered a HTML5 website with some news from twitter
    And the page uses Angular 1.0.6
    And Twitter Bootstrap 2.3.1

  Scenario: Normal use case
    When I click on the login button
    Then a login form is displayed
    And the username 'admin' is displayed on the field

  Scenario: Static detection
    When I check the source code
    Then I can see there is an input tag at line 122
    """
    <input type="text" id="username" name="username"
    placeholder="" class="input-xlarge" value="admin">
    """
    And the input tag has 'admin' as value
    Then I check the password input at line 130
    """
    <input type="password" id="password" name="password"
    placeholder="" class="input-xlarge">
    """
    And I see the password field is not required
    And the password can be ignored
    Then the HTML is only requiring the username input

  Scenario: Dynamic detection
    Given I access the login form from the website
    Then I submit the form only using 'admin' as username
    And the login is succesful
    And the site displays the username on the top bar
    And 'admin' is shown on the top [evidence](evidence.png)
    Then the website is allowing any user

  Scenario: Exploitation
    Given I access the login form from the website
    When I submit the form using my name
    """
    Username: felipe
    """
    And I don't input any password
    Then the login is successful [evidence](evidence2.png)
    And I can access the website with any username
    And the website can be controlled by any user

  Scenario: Remediation
    Given the username and password input fields
    Then value 'admin' must be deleted from username tag
    """
    <input type="text" id="username" name="username"
    placeholder="" class="input-xlarge">
    """
    And password field must be required
    """
    <input type="password" id="password" name="password"
    placeholder="" class="input-xlarge" required>
    """
    And inputs should not use obvious names
    Then basic validation is working
    And now the website has basic security

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.1/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.8/10 (Medium) - IR:H/MAV:N/MAC:L/MUI:N/MI:L

  Scenario: Correlations
    No correlations have been found to this date 2019-10-24
