## Version 1.4.1
## language: en

Feature:
  TOE:
   Bricks Login Form 3
  Location:
   http://localhost/bricks/login-3/ - username or password (field)
  CWE:
   CWE-0564: Allows remote attackers to perform arbitrary operations
   via unspecified vectors
  Rule:
   REQ.173 Discard unsafe inputs
  Goal:
   Detect and exploit vuln Insecure SQL query input
  Recommendation:
   Use parameterized queries with known tools

  Background:
    Hacker's software:
     | <Software name> | <Version>     |
     | Windows 10 Pro  | 2024          |
     | Google Chrome   | 87.0.4280.88  |
     | WAMPSERVER      | 3.2.3         |
     | PHP             | 5.6.40        |
  TOE information:
    Given I set up the OWASP Bricks in my PC
    Then I entered a PHP site which uses SQL requests and database MySql
    And I get access to http://localhost/bricks/

  Scenario: Normal use case
    Given I access to http://localhost/bricks/
    When I click on "Login Pages" from the "bricks" dropdown menu
    Then I access to http://localhost/bricks/login-pages.html
    And I click in "Login #3"
    Then I access to http://localhost/bricks/login-3/
    And I can see a login form

  Scenario: Static detection
    When I open the source code of the page from the browser inspector
    Then I can see that form tag uses the POST method from "index.php"
    And as I have the site running locally in my PC
    Then I can see the following POST method from www/bricks/login-3/index.php
    """
    if(isset($_POST['submit'])) {
      $username=$_POST['username'];
      $pwd=$_POST['passwd'];
      $sql="SELECT * FROM users WHERE name=('$username') and
        password=('$pwd') LIMIT 0,1";
      $result=mysql_query($sql);
      $count=mysql_num_rows($result);
      ...
    }
    """
    Given that the method doesn't use parameterized queries
    Then I can detect a SQLi vulnerability in the form

 Scenario: Dynamic detection
    Given I access to http://localhost/bricks/login-3/
    And knowing that the database is MySql
    When I write the following SQL syntax in username and any password
    """
    ' or 1=1 #
    """
    Then OWASP shows me the sql instruction with which I try to log in
    And this it can be seen in [evidence](img1.png)
    And I can conclude that the form has SQLi vulnerability

  Scenario: Exploitation
    Given I access to http://testphp.vulnweb.com/login.php/
    When I write the following SQL syntax only in username and any password
    """
    ') or 1=1 #
    """
    Then I can log in succesfully
    And this it can be seen in [evidence](img2.png)
    When I write the following SQL syntax in username field
    """
    ') or 1=('1
    """
    And I write the following SQL syntax in password field
    """
    ') or 1=('1
    """
    Then I can log in again
    And this it can be seen in [evidence](img3.png)

  Scenario: Remediation
    When Use parameterized queries in the back
    Then the program can succesfully avoid most of the known SQLi
    Given know that the site is PHP and database is MySql
    Then can use MySqli as tools of parameterized queries
    And can seen a example in the following line
    """
    ...
    $sql = "SELECT email FROM users WHERE username = ? AND password = ?"
    $sqlQuery = mysqli_prepare($connection, $sql);
    $result = mysqli_stmt_bind_param($sqlQuery, "ss", $userValue, $passValue);
    $result = msqli_stmt_execute($sqlQuery);

    if ($result) {
      $result = mysqli_stmt_bind_result($sqlQuery, $email);
      if(mysqli_stmt_fetch($sqlQuery)) {
        echo $email;
      }
    }
    ...
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.2/10 (Critical) - AV:L/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.8/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.4/10 (Critical) - CR:L/IR:L/MC:L/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-11-25
