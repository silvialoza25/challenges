## Version 1.4.1
## language: en

Feature:
  TOE:
    diwa
  Location:
    http://172.17.0.2/upload.php
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    REQ.040 Contrast File Format and Extension
  Goal:
    Upload PHP files
  Recommendation:
    Forbid the upload of PHP files

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.07.01  |
    | Brave           | 1.10.97     |
    | Docker          | 19.03.12-ce |
  TOE information:
    Given I am accessing the site at http://172.17.0.2/
    And it runs in a docker container

  Scenario: Normal use case
    Given I'm logged as admin
    And in the upload section
    And I can either upload a new file or delete already uploaded files

  Scenario: Static detection
    When I look at the code at "upload.php"
    Then I can see that the vulnerability is being caused by line 35
    """
    ...
    32  $uploaddir = ROOT_PATH . '/files/';
    33  $uploadfile = $uploaddir . basename($_FILES['file']['name']);
    34
    35  if (!move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
    ...
    """
    And I can conclude the uploader is vulnerable

  Scenario: Dynamic detection
    Given that I know the uploader does not perform any validation
    When I try to upload a PHP file: "test.php"
    Then I can see it gets uploaded [evidence](uploaded.png)
    And I can conclude that the site is vulnerable

  Scenario: Exploitation
    Given that the system is vulnerable
    Then I can upload a malicious PHP file
    """
    <?php
      phpinfo();
    ?>
    """
    And the file gets uploaded
    Then I can conclude that I can upload any PHP file.
    But with this vulnerability alone I can not execute it

  Scenario: Remediation
    Given I have patched the code by adding the code
    """
    34  $name = basename($_FILES['file']['name']);
    35
    36  if (preg_match("/^[a-zA-Z0-9_-]*\.txt$/", $name)) {
    37    $uploadfile = $uploaddir . $name;
    38    if (!move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
    39      $uploaded = false;
    40    } else {
    41      $uploaded = true;
    42    }
    43  } else {
    44    die("file not allowed");
    45  }
    """
    Then I only allow "txt" files with letters, numbers and "-", "_"
    And I added a ".htaccess" to avoid php execution inside uploads folder
    """
    <FilesMatch ".*\.(php|php.*)$">
      Order Allow,Deny
      Deny from all
    </FilesMatch>
    """
    When I try to upload a PHP file again
    Then I get a "file not allowed" [evidence](not.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.2 (High) - AV:N/AC:L/PR:H/UI:N/S:C/C:H/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.0 (High) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.9 (High) - MAV:N/MAC:L/MPR:H/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    vbd/diwa/0098-index
      Given I can exploit the LFI as in "0098-index"
      Then now I can execute the malicious PHP uploaded files
      And if I upload
      """
      <?php
        system("ls");
      ?>
      """
      When I exploit the LFI going to
      """
      http://172.17.0.2/?page=../files/test
      """
      Then I can see [evidence](executed.png)
      And a more powerful attack can be performed
