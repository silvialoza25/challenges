## Version 1.4.1
## language: en
Feature:
  TOE:
    Metasploitable2
  Location:
    192.168.2.129:21 TCP
  CWE:
    CWE-0078: OS Command Injection
  Rule:
    REQ.142 Change system default credentials
  Goal:
    Get remote shell on the machine
  Recommendation:
    Update vsftpd service

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-linux      | 2020.4      |
    | VMware          | 16.1.0      |
    | Nmap            | 7.70        |
    | Metasploit      | 5.0.4       |

  TOE information:
    Given I am running Metasploitable 2 on VMware Workstation 15
    And I am using kali-linux

  Scenario: Normal use case
    Given I access using the ip from metasploitable 2
      """
      192.168.199.130
      """
    And I see a list from the web applications

  Scenario: Static detection
    Given I don't have access to the source code
    Then This can't be done

  Scenario: Dynamic detection - Service Enumeration
    Given I know the machine have some open services
    When I scan with Nmap for open ports
      """
      $ nmap -A -T4 -p- 192.168.199.130
      21/tcp open ftp    vsftpd 2.3.4
      Service Info: OS: Unix
      """
    Then I can conclude that anyone can get access remotely

  Scenario: Success: Metasploit backdoor command execution
    Given The port 21 run an outdated version of vsftpd
    When I search for 'vsftpd 2.3.4' on google
    Then I find out this version contains a backdoor which opens a shell
    When I open metasploit and search for 'vsftpd 2.3.4' [evidence](msf5.png)
    And I use exploit/unix/ftp/vsftpd_234_backdoor
    Then I got access as root through port 6200 [evidence](root.png)

  Scenario: Remediation
    Given The outdated version of vsftpd running on the machine
    And The official correction from the 3rd of july 2011
    Then To fix it, an updated version must be downloaded

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.8/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      9.4/10 (Critical) - E:H/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      8.5/10 (High) - CR:L/IR:M/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2021-02-10
