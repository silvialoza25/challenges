## Version 1.4.1
## language: en
Feature:
  TOE:
    Metasploitable2
  Location:
    192.168.199.130:23 TCP
  CWE:
    CWE-0307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.142 Change system default credentials
  Goal:
    Get remote shell on the machine
  Recommendation:
    Implement a timeout

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-linux      | 2020.4      |
    | VMware          | 16.1.0      |
    | Nmap            | 7.70        |
    | Metasploit      | 5.0.4       |

  TOE information:
    Given I am running Metasploitable 2 on VMware Workstation 15
    And I am using kali-linux

  Scenario: Normal use case
    """
    Users can login into the telnet service to control the machine
    """
    When I have the username and password to the service
    Then I can manage the server

  Scenario: Static detection
    Given I don't have access to the source code
    Then This can't be done

  Scenario: Dynamic detection
    Given I know the machine have some open services
    When I scan with Nmap for open ports
      """
      $ nmap -A -T4 -p- 192.168.199.130
      23/tcp  open  telnet   Linux telnet
      """
    Then I can conclude that anyone can get access remotely

  Scenario: Exploitation
    Given The port 23 is open
    When I open msfconsole
    And I search for 'telnet'
    Then I get the results [evidence](search.png)
    And I use auxiliary/scanner/telnet/telnet_login
    When I run the exploit
    Then I get the error [evidence](fail.png)
    When I search for common user and password list
    Then I decide to use the ones included with Kali in
    """
    /usr/share/wordlists/metasploit
    """
    When I run the exploit
    Then I got a session with access to the service [evidence](telnet.png)

  Scenario: Remediation
    Given The telnet service running on the machine
    Then To fix it, implement a timeout system

  Scenario: Scoring
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.1/10 (High) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2021-03-25
