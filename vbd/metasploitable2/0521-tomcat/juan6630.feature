## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable2
  Location:
    192.168.2.129:8180 TCP
  CWE:
    CWE-0521: Weak Password Requirements
  Rule:
    REQ.142 Change system default credentials
  Goal:
    Get remote shell on the machine
  Recommendation:
    Change service default credentials

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-linux      | 2020.4      |
    | VMware          | 16.1.0      |
    | Mozilla Firefox | 64.4.0      |
    | Nmap            | 7.70        |
    | Metasploit      | 5.0.4       |
    | Burp Suite      | 2020.11.3   |

  TOE information:
    Given I am running Metasploitable 2 on VMware Workstation 15
    And I am using kali-linux

  Scenario: Normal use case
    Given I can access the site on port 8180
    Then I try to open the manager page
    But It ask for username and password

  Scenario: Static detection
    Given I don't have access to the source code
    Then This can't be done

  Scenario: Dynamic detection - Service Enumeration
    Given I know the machine have some open services
    When I scan with Nmap for open ports
    """
    $ nmap -A -T4 -p- 192.168.2.129
    8180/tcp open http    Apache Tomcat/coyote JSP engine 1.1
    http-server-header: Apache-Coyote/1.1
    http-title: Apache Tomcat/5.5
    """
    Then I can conclude that anyone can get access remotely

  Scenario: Success: Default credentials payload
    Given The port 8180 is open
    When I open the web page at 192.168.2.129:8180
    And I open Burp Suite
    And configure the proxy on firefox
    When I try to open the manage page
    Then Tomcat ask for the user and password of the admin
    And I try with admin:admin
    When Send the intercepted data to the decoder in Burp Suite
    And Decode the authorization as Base64
    When I search for 'tomcat default credentials' in google
    And Code the user:password list as Base64 in the terminal
    When I paste the results as payload in Burp Suite
    And Set the payload mark at the Authorization line
    Then I get the user:password [evidence](credentials.png)

  Scenario: Fail: Reverse shell on Tomcat
    Given The Tomcat username and password
    When I search for 'war file reverse shell'
    And Open a sheet cheat by NETSEC
    Then I use a msfvenom web payload
    And I upload and deploy the shell in the tomcat manager
    When I set a listener at my system
    Then It says that I connect but I don't have a shell

  Scenario: Success: Metasploit reverse shell
    Given The Tomcat username and password
    When I open metasploit and search for tomcat
    And Use exploit/multi/tomcat_mgr_deploy
    Then I got access as tomcat55 not root
    When I create a shell to check the pwd / ls
    But Don't find anything useful
    When I search for 'privilege escalation on linux'
    And Find udev_netlink
    When I use it on msf5
    And Set SESSION to the one I have
    Then I access as root and have a shell [evidence](root.png)

  Scenario: Remediation
    Given That the Apache Tomcat default credentials are in use
    Then To fix it, demand usage of strong passwords
    And updating to a newer release of Tomcat

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.1/10 (High) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.9/10 (Medium) - CR:L/IR:M/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-12-10
