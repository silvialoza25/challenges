## Version 1.4.1
## language: en

Feature: Exploit Tomcat Server
  TOE:
    My-Tomcat-Host-1
  Category:
    Default Credentials
  Location:
    http://10.10.10.4
  CWE:
    CWE-453: Insecure Default Variable Initialization
  Rule:
    REQ.142 Change system default credentials
  Goal:
    Obtain root access using Tomcat as the entrypoint
  Recommendation:
    Change the default credentials of the Axis2 Management WebApp

  Background:
  Hacker's software:
    | <Software name>      | <Version> |
    | Kali Linux           | 5.4.0     |
    | Nmap                 | 7.8.0     |
    | Firefox              | 68.6.0esr |
    | Nikto                | 2.1.6     |
    | Ant                  | 1.9.14    |
    | Java Development Kit | 8         |
    | Python               | 2.7.17    |
  TOE information:
    Given that the target machine is running a Tomcat server

  Scenario: Normal use case
    Given that I use "nmap" to scan the target ports
    And I find the Tomcat server unning on port 8080
    Then I visit the site using the browser
    And I see the Tomcat default site
    And I notice Tomcat's documentation and examples are accessible
    And the Manager panel is not

  Scenario: Static detection
    Given there is not access to the source code
    Then a static detection cannot be performed

  Scenario: Dynamic detection
    Given the server I found on port 8080
    Then I use "nikto" to find more information about it
    And I notice the result metions a path "/axis2"
    Then I decide to browse it
    And find a link to access the Axis2 Management WebApp
    And see an authentication form in order to access it

  Scenario: Exploitation
    When I investigate about the default credentials for this portal
    And I decide to give them a try
    Then I get access to the portal [evidence](axis2-admin.png)
    And I can upload my own Axis2 service
    When I investigate about Axis2 services
    Then I realize I can upload a malicious one using the portal
    And I find one with several interesting functions at
    """
    https://github.com/CaledoniaProject/AxisInvoker

    It won't work out of the box, apply the changes from
    https://stackoverflow.com/questions/35720181/the-serviceclass-object-does-
    not-implement-the-required-method-in-the-following/37864267
    and build again using Ant
    """
    Then I deploy the malicious service
    Given I acquired the ability to execute commands
    And save files in the target machine
    And I prepare a python script to get a reverse shell based on
    """
    http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet
    """
    And I set up a simple Python HTTP server to access that script
    When I save that script in the target machine
    And I execute it from the service
    Then I get a reverse shell with the user "tomcat"
    Given that the user I got has permissions to use Java as root
    Then I prepare a Java script to create a new reverse shell based on
    """
    https://gist.github.com/caseydunham/53eb8503efad39b83633961f12441af0
    """
    And I use the Python server to save the script in the target machine
    When I execute the Java script with "sudo"
    Then I get a new shell with root permissions [evidence](root-access.png)
    And complete the challenge

  Scenario: Remediation
  Change the default credentials of the Axis2 Management WebApp
    Given that the default user was changed
    And a passphrase was used to secure it
    Then I could not login to the Axis2 Management Console
    And could not compromise the service

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (Critical) - AV:A/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    10/10 (Critical) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-07
