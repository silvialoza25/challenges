## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnerable Web Application
  Category:
    XSS
  Location:
    http://192.168.1.90:9991/www/XSS/XSS_level2.php - username (field)
  CWE:
    CWE-184: Incomplete Blacklist
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Reflect JavaScript code injected via user input
  Recommendation:
    Escape special characters and use an encoding library

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali            | 4.19.0    |
    | Firefox         | 60.6.1esr |
  TOE information:
    Given The site
    When I enter into it
    Then The server is running LAMPP

  Scenario: Normal use case
    Given The page
    When I access it
    Then I see a name field on the page
    When I enter a name
    Then it echoes it back

  Scenario: Static detection
    When I look at the code "XSS_level2.php"
    Then I can see only the tag "<script>" is banned
    """
    <?php
      if (isset($_GET["username"])) {
       $user = str_replace("<script>", "",$_GET["username"]);
      echo "Your name is "."$user";
      }
    ?>
    """
    Then I can conclude that I can insert JS code with some precautions

  Scenario: Dynamic detection
    Given The username field
    And The previous static analysis
    When I insert a "<h1>" tag
    Then I can see it render in the page response [evidence](img1.png)
    And I can conclude that I can still inject code on the page

  Scenario: Exploitation
    Given The username field
    When I insert my name as
    """
    <SCRIPT>alert('1');</script>
    """
    Then I see the alert render on the screen [evidence](img2.png)
    And I can conclude that I bypassed the filter successfully

  Scenario: Remediation
    Given The source code
    When I add change the vulnerable code to
    """
    <?php
      if (isset($_GET["username"])) {
        $user = str_replace("<", "", $_GET["username"]);
        echo "Your name is "."$user";
      }
    ?>
    """
    When I try to input my name as a "script" tag
    Then it outputs [evidence](img3.png)
    """
    Your name:Your name is SCRIPT>alert( '1' );/script>
    """
    And I can confirm the vulnerability was patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.2/10 (Medium) - AV:L/AC:L/PR:L/UI:N/S:C/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.8/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.8/10 (Medium) - MAV:L/MAC:L/MPR:L/MUI:N/MS:C/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-10-01
