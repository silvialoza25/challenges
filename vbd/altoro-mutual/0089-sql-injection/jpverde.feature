## Version 1.4.1
## language: en

Feature:
  TOE:
    Altoro Mutual
  Category:
    SQL injection
  Location:
    https://demo.testfire.net/login.jsp - username (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used
    in an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject SQL to get valuable information
  Recommendation:
    Use prepared statements with variable bindings

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0.1      |
  Machine information:
    Given A link for a webpage
    """
    http://demo.testfire.net/index.jsp
    """
    And A web app simulating a bank app

  Scenario: Normal use case
    Given A user tries to login
    When User types credentials
    Then User get access to the account

  Scenario: Static detection
    When I don't have acces to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    When Looking at the field
    Then I try to login via SQL injection with this command
    """
    ' OR '1'='1'
    """
    And I write  a random password
    When I prees the login button
    Then The web app shows this message
    """
    Syntax error: Encountered "asd" at line 1, column 73.
    """
    And I realize that it recognizes SQL queries but i'm missing something
    And [evidence](image1.png)

  Scenario: Exploitation
    When I input the following query in username field, and a random password
    """
    ' OR '1'='1' --
    """
    Then The site redirects me to the administrator account logged in
    And [evidence](image2.png)
    And It gives me total access to the app as administrator
    And [evidence](image3.png)

  Scenario: Remediation
    When A developer make a login form
    Then He can use variable binding to make the login fields safer
    And If an user tries to write SQL code the field recongnize it as a string
    When User types, like me: ' OR '1'='1' --
    Then The query will look for an username with this values instead of
    And adding the input to the query
    And The vulnerability could be fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.1/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 (High) - E:H/RL:W/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    9.0/10 (Critical) - CR:H/IR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:C/MI:H/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-25
