## Version 2.0
## language: en

Feature: SQL Injection Level 24
  TOE:
    SQLI-labs
  Category:
    SQL Injection -  POST-Second-Order-Injections-Stored-Injections
  Location:
    http://localhost/sqlilabs/Less-24/ - id (field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Check the website vulnerability against SQL Injection
  Recommendation:
    Prepare the inputs to be safely used

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  TOE information:
    Given I am accessing to the website
    And the server is running MySQL version 5.7.28
    And PHP version 5.6.40
    And is running on localhost

  Scenario: Normal use case
    Given I am on the login page
    And I can see a form with "Username" and "Password"
    When I attempt to log in without filling the "Username" and "Password"
    Then A new message appears
    """
    BUG OFF YOU SILLY DUMB HACKER
    """
    When I click on the link "New User click here?"
    Then A new form appears with "Desired Username", "Password" and "Retype Password"
    When I fill the form and press the register button
    Then The website is redirected to the login page
    And A button appears to delete the cookies
    When I log in with the new credentials
    Then A new form appears to change the "Password"
    When I set "Current Password", "New Password" and "Retype Password"
    And I press the reset button
    Then I can see the message
    """
    Password successfully updated
    """

  Scenario: Static detection
    Given I access to the source code
    When I look for the related code
    Then I find the related query
    """
    $username= $_SESSION["username"];
    $curr_pass= mysql_real_escape_string($_POST['current_password']);
    $pass= mysql_real_escape_string($_POST['password']);
    $re_pass= mysql_real_escape_string($_POST['re_password']);
    $sql = "UPDATE users SET PASSWORD='$pass' where username='$username' and password='$curr_pass' ";
    $res = mysql_query($sql) or die('You tried to be smart, Try harder!!!! :( ');
    """
    When I analyze the code
    Then The "$username" variable is not prepared to be safely used in a query
    And The query is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given I am on the login page
    When I attempt to create a new user who has "admin" as "Username"
    Then An alert appears with the message
    """
    The username Already exists, Please choose a different username
    """
    When I create a new user with the "Desired Username" as
    """
    newuser' #
    """
    Then The website is redirected to the login page
    When I log in on the website with the new user
    Then A form appears to change the "Password"
    When I attempt to update the password
    Then A new message appears
    """
    BUG OFF YOU SILLY DUMB HACKER
    """
    When I create a new user with the "Desired Username" as
    """
    newuser
    """
    Then The website is redirected to the login page
    When I log in with the new user
    And I attempt to update the password
    Then A new message appears
    """
    Password successfully updated
    """
    When I attempt to log in again
    Then The new password does not work but the old password works
    And The query to update the user's password is vulnerable to SQL injection

  Scenario: Exploitation
    Given The website is vulnerable
    When I attempt to log in with "admin" as "Username" and "1234" as "Password"
    Then A new message appears
    """
    BUG OFF YOU SILLY DUMB HACKER
    """
    When I create a new user who has "admin' #" as "Username"
    Then The website is redirected to the login page
    When I log in with the new credentials
    Then A new form appears to change the "Password"
    When I attempt to change the "password" to "1234" for the current user
    Then A new message appears
    """
    Password successfully updated
    """
    When I log in with "admin" as "Username" and "1234" as "Password"
    Then The login is successful [evidence](evidence.png)
    And I could change the password for the admin
    And The website is vulnerable to SQL injection

  Scenario: Remediation
    Given The web site is vulnerable against SQL injection
    And There is a variable that is not prepared to be safely used
    When The web site is using the vulnerable query
    Then The code must be replaced with
    """
    $username= mysql_real_escape_string($_SESSION["username"]);
    """
    And The query could be shielded against SQL injection

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.2/10 (High) - E:H/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      5.5/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-02-26
