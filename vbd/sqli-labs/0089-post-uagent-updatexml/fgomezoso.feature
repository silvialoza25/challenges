## Version 1.4.1
## language: en

Feature:
  TOE:
    SQLI-labs
  Category:
    SQL Injection - POST-Header Injection-Uagent field-Error Based
  Location:
    http://localhost/sqlilabs/Less-18/ - uagent(field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
    CWE-643: Improper Neutralization of Data within XPath Expressions
      https://cwe.mitre.org/data/definitions/643.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Verify if the site is vulnerable to SQL Injection
  Recommendation:
    Use Prepared Statements

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Microsoft Windows | 7               |
    | Chrome            | 78.0.3904.70    |
    | Wamp server       | 3.1.9 32 bit    |
  TOE information:
    Given I access the main page
    And the page is made with PHP
    And MySQL is the Database Management System
    And the website is running in localhost

  Scenario: Normal use case
    Given I can use a username and password beforehand
    When I access the main page
    Then I see a form with "Username" and "Password" fields
    And there is a message below the form
    """
    Your IP ADDRESS is: ::1
    """
    When I enter the user's credentials
    """
    Username:Dumb
    Password:Dumb
    """
    Then the website updates the previous message
    """
    Your IP ADDRESS is: ::1
    Your User Agent is: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36
    (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36
    Succesfully Logged In
    """

  Scenario: Static detection
    Given I access the backend code at
    """
    sqlilabs\Less-18\index.php
    """
    When I check the source code
    Then I find a SQL insert query related to the user agent
    """
    $insert="INSERT INTO `security`.`uagents` (`uagent`, `ip_address`
    , `username`) VALUES ('$uagent', '$IP', $uname)";
    mysqli_query($con, $insert);
    """
    And the variable $insert is storing a query string
    When there aren't prepared statements in the code
    And the query is stored as a single string
    Then the id parameter is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given I use Chrome tamper in order to modify request headers
    When I submit the form using the username "Dumb" and password "Dumb"
    Then a new window shows up with the request details
    When I add a single quote (') to the window's user agent field
    Then I send the request by clicking "OK"
    And I can see a new error message at the center of the screen
    """
    You have an error in your SQL syntax; check the manual that
    corresponds to your MySQL server version for the right syntax to use
    near '::1', 'Dumb')' at line 1
    """
    When the query's interpretation is broken by my input in the URL
    Then the site is vulnerable to SQL Injection

  Scenario: Exploitation
    When I check the previous SQL syntax error
    Then I modify the user agent with a new SQL injection
    """
    Hello',1,2)-- -
    """
    When I send the request with the new injection
    Then the query isn't generating errors
    And the website message is modified
    """
    Your IP ADDRESS is: ::1
    Your User Agent is: Hello',1,2)-- -
    """
    And I can see that my injection is reflected on the screen
    When I try to inject a SQL function inside the insert query
    Then I find out that the query is literally printed on the screen
    """
    Your User Agent is: Hello',(select version()),2)-- -
    """
    And I can't perform basic SQL injections in the current website
    And I need to test another type of SQL injection
    When I choose to test an Xpath injection as the attack
    Then I use the XMLUpdate function from the Xpath language
    """
    updatexml(null,concat(0x3a,(select version())),null)
    """
    When the colon's hex value "0x3a" is used inside the UpdateXML syntaxis
    Then the query must generate a syntax error
    And the XPath error should display the value of the injected SQL function
    When I write an injection using the previous UpdateXML attack
    """
    1',updatexml(null,concat(0x3a,(select version())),null),2)-- -
    """
    Then I get sensible data from the error [evidence](img.png)
    And the website is vulnerable to SQL injection via Xpath error

  Scenario: Remediation
    When the application is executing queries in MySQL
    Then the query code must use prepared statements
    """
    $stmt = $mysqli->prepare("SELECT * FROM users WHERE id='$id' LIMIT 0,1")

    $id = $_POST['id']
    $stmt->bind_param("i", $id)

    $stmt->execute()
    """
    And the code can prevent SQL Injection
    When the application receives data from the HTTP header
    """
    $uagent = $_SERVER['HTTP_USER_AGENT'];
    """
    Then variables like "$uagent" should be validated
    And characters like "(" and ")" should be filtered
    And the XPath errors can be mitigated

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.7/10 (High) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    7.1/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.8/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L

  Scenario: Correlations
    No correlations have been found to this date 2019-12-18
