## Version 1.4.1
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Session Management
  Location:
    / - Session
  CWE:
    CWE-0384: Session Fixation -compound-
      https://cwe.mitre.org/data/definitions/384.html
    CWE-0472: External Control of Assumed-Immutable Web Parameter -base-
      https://cwe.mitre.org/data/definitions/472.html
    CWE-0642: External Control of Critical State Data -class-
      https://cwe.mitre.org/data/definitions/642.html
    CWE-1019: Validate Inputs -category-
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-060: Reusing Session IDs (aka Session Replay) -detailed-
      http://capec.mitre.org/data/definitions/60.html
    CAPEC-593: Session Hijacking -standard-
      http://capec.mitre.org/data/definitions/593.html
    CAPEC-021: Exploitation of Trusted Credentials -meta-
      http://capec.mitre.org/data/definitions/21.html
  Rule:
    REQ.225: https://fluidattacks.com/web/es/rules/255/
  Goal:
    Steal a user's session and navigate as them while they are on
  Recommendation:
    Forbid concurrent sessions

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site normally

  Scenario: Static detection
  No session concurrency related code

  Scenario: Dynamic detection
  Logging in twice
    Given I log into the site with my account
    And log in again from a second device
    Then I'm logged in in both devices and can perform actions no problem
    Then I know there's no session concurrency control


  Scenario: Exploitation
  Login as another user while they are still working
    Given I know the victim is on the site
    Given I have obtained the victims credentials or have another way to login
    Then I log in as the victim
    Then I can use the site as the victim at the same time as them
    Then my malicious actions are going to blend with their legitimate ones

  Scenario: Remediation
  Forbid concurrent sessions
    Given I store a unique token generated when users log in
    And overwrite each time they log in again
    And only accept requests that contain the current token
    Then concurrent sessions aren't possible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-28
