## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Session Management
  Location:
    / - Session
  CWE:
    CWE-0613: Insufficient Session Expiration -base-
      https://cwe.mitre.org/data/definitions/613.html
    CWE-0287: Improper Authentication -class-
      https://cwe.mitre.org/data/definitions/287.html
    CWE-1018: Manage User Sessions -category-
      https://cwe.mitre.org/data/definitions/1018.html
  CAPEC:
    CAPEC-060: Reusing Session IDs (aka Session Replay) -detailed-
      http://capec.mitre.org/data/definitions/60.html
    CAPEC-593: Session Hijacking -standard-
      http://capec.mitre.org/data/definitions/593.html
    CAPEC-021: Exploitation of Trusted Credentials -meta-
      http://capec.mitre.org/data/definitions/21.html
  Rule:
    REQ.023: https://fluidattacks.com/web/es/rules/023/
  Goal:
    Hijack another user's session while they're gone
  Recommendation:
    Expire inactive sessions after a prudential time

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No input validation for unusual XSS payloads
    When I look at the configuration at "/dvna/server.js"
    """
    ...
    app.use(session({
      secret: 'keyboard cat',
      resave: true,
      saveUninitialized: true,
      cookie: { secure: false }
    }))
    ...
    """
    Then I see no settings for sesion expiration

  Scenario: Dynamic detection
  Wait and see
    Given I login to the platform with my own account
    And I idle for a long time not interacting with the site
    Then I refresh the site
    And I'm still logged in
    Then I know sessions are not timing out

  Scenario: Exploitation
  Hijack another user's session
    Given obtain another user's session cookies by whatever means
    And use them to access the site
    Then I have control of their account until they log out manually

  Scenario: Remediation
  Expire sessions after a set time
    Given I add this to "web.xml"
    """
    ...
    app.use(session({
      secret: 'keyboard cat',
      resave: true,
      saveUninitialized: true,
      cookie: { secure: false },
      expires: new Date(Date.now() + (5 * 60 * 1000)
    }))
    ...
    """
    Then when a user idles for more than 5 minutes
    Then their session expires
    And can't be abused later

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.5/10 (Low) - AV:P/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.3/10 (Low) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.3/10 (Low) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-28

