# OVERVIEW

## Number of Bugged Files: 5

## Number of Vulnerabilities: 31

In the following table you will find
each vulnerability exposed by each scanner
or manually, linked with it's respective
[Common Weakness Enumeration (CWE)](https://cwe.mitre.org/)
classification.

* **LGTM:** Instances of a particular vulnerability detected by LGTM
* **SkipFish:** Instances of a particular vulnerability detected by Skipfish
* **LGTM Escapes:** Operation in the following form: webgoat(#) - LGTM(#)
* **LGTM Escapes(%):** Operation in the following form: LGTMescapes/webgoat
* **SkipFish Escapes:** Operation in the following form: webgoat(#) - SkipFish(#)
* **SkipFish Escapes(%):** Operation in the following form: SFescapes/webgoat

| # | Vuln. Detected | LGTM | LGTM Escapes | LGTM Escapes (%) | SkipFish | SkipFish Escapes | SkipFish Escapes (%) |
| :---: | --- | :---: | :---: | :---: | :---: | :---: | :---: |
|1| [Command Injection](https://cwe.mitre.org/data/definitions/78.html) | 1 | 0 | 0 | 0 | 1 | 100 |
|2| [Cross-Site Request Forgery](https://cwe.mitre.org/data/definitions/352.html) | 3 | 0 | 0 | 1 | 2 | 66 |
|3| [URL Redirection to Untrusted Site ('Open Redirect')](https://cwe.mitre.org/data/definitions/601.html) | 1 | 0 | 0 | 0 | 1 | 100 |
|4| [Use of Hard-coded Cryptographic Key](https://cwe.mitre.org/data/definitions/321.html) | 1 | 0 | 0 | 0 | 1 | 100 |
|5| [Dead Code](https://cwe.mitre.org/data/definitions/561.html) | 2 | 0 | 0 | 0 | 2 | 100 |
|6| [Security Misconfiguration](https://cwe.mitre.org/data/definitions/933.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|7| [Improper Neutralization of Input During Web Page Generation ('Cross-site Scripting')](https://cwe.mitre.org/data/definitions/79.html) | 0 | 3 | 100 | 0 | 3 | 100 |
|8| [Weak Password Requirements](https://cwe.mitre.org/data/definitions/521.html) | 0 | 2 | 100 | 0 | 2 | 100 |
|9| [Improper Access Control](https://cwe.mitre.org/data/definitions/284.html) | 0 | 2 | 100 | 0 | 2 | 100 |
|10| [Gestionar cuentas de usuarios](https://fluidattacks.com/web/es/rules/034/) | 0 | 1 | 100 | 0 | 1 | 100 |
|11| [Establecer acciones de usuario seguras](https://fluidattacks.com/web/es/rules/237/) | 0 | 1 | 100 | 0 | 1 | 100 |
|12| [Improper Restriction of XML External Entity Reference ('XXE')](https://cwe.mitre.org/data/definitions/611.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|13| [Cleartext Transmission of Sensitive Information](https://cwe.mitre.org/data/definitions/319.html) | 0 | 1 | 100 |
|14| [Session Fixation](https://cwe.mitre.org/data/definitions/384.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|15| [Uncontrolled Resource Consumption](https://cwe.mitre.org/data/definitions/400.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|16| [Deserialization of Untrusted Data](https://cwe.mitre.org/data/definitions/502.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|17| [Weak Password Recovery Mechanism for Forgotten Password](https://cwe.mitre.org/data/definitions/640.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|18| [Authorization Bypass Through User-Controlled Key](https://cwe.mitre.org/data/definitions/639.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|19| [Improper Restriction of Rendered UI Layers or Frames](https://cwe.mitre.org/data/definitions/1021.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|20| [Insufficient Logging](https://cwe.mitre.org/data/definitions/778.html) | 0 | 1 | 100 | 0 | 1 | 100 |
|21| [Using Components with Known Vulnerabilities](https://www.owasp.org/index.php/Top_10-2017_A9-Using_Components_with_Known_Vulnerabilities) | 0 | 1 | 100 | 0 | 1 | 100 |
|22| [Sensitive Cookie in HTTPS Session Without 'Secure' Attribute](https://cwe.mitre.org/data/definitions/614.html) | 0 | 1 | 100 | 1 | 0 | 0 |
| - | Total | 8 | 21 | 67,7 | 2 | 29 | 93,5 |

## Graphical Analysis through Venn's Diagram

![Diagram](venn-diagram.png "Venn's Diagram")

This diagram's purpose is to illustrate
in an elegant way the overall results
from the LGTM scan vs the manual tests
conducted by our analysts.

# Vulnerability List
* **Type**: Type of input,
it can be one of the following:
  * **Get|Post Parameter**
  * **Get|Post Header**
  * **TCP|UDP Port**
  * **Cookie**
  * **Code** When there is a code smell, not necessarily an input
  * **URL:** When the vulnerability
  does not has a particular input
  migth be in URL without parameters.
  * **Other**
* **Input:** It refers to the particular name of
  the vulnerable Type.
* **Vulnerability:** Name of the vulnerability found.
* **Detected by:** By which fuzzer was detected.
* **Status:** Can take up to three values:
  * **False positive**
  * **Pending confirmation**
  * **Writeup:** If it is this one, it should
  point to a gherkin file in this repo
  https://gitlab.com/fluidattacks/writeups

If a vulnerabilty is found manually, but on table doesn't exist, or no scanner
find it, that field should be filled with the **manual** keyword instead of
dash(-), only if both, detected by auth/no-auth fields are empty
or with the dash(-).

| # | URL | Type | Input | Vulnerability | Status | LGTM Link |
| :---: | --- | :---: | :---: | :---: | :---: | :---: |
|1| appHandler.js | Parameter | address | Command Injection | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0077-command-injection/jcardonaatfluid.feature) | [Link](https://lgtm.com/projects/g/appsecco/dvna/snapshot/36d5a48082a437f376121d0cabd70ba33652e0df/files/core/appHandler.js#x131031fc7176b618:1) |
|2| server.js | Parameter | csrfToken | CSRF | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0352-csrf/simongomez95.feature) | [Link](https://lgtm.com/projects/g/appsecco/dvna/snapshot/36d5a48082a437f376121d0cabd70ba33652e0df/files/server.js#xcdd593f219358217:1) |
|3| appHandler.js | Parameter | url | Unvalidated Redirect | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0601-unvalidated-redirects/simongomez95.feature) | [Link](https://lgtm.com/projects/g/appsecco/dvna/snapshot/36d5a48082a437f376121d0cabd70ba33652e0df/files/core/appHandler.js#xbb1c79c97102e045:1) |
|4| server.js | Secret | secret | Hard-coded Secret | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0321-hardcoded-secret/simongomez95.feature) | [Link](https://lgtm.com/projects/g/appsecco/dvna/snapshot/36d5a48082a437f376121d0cabd70ba33652e0df/files/server.js#xab2ec259175a2049:1) |
|5| index.js | Code Smell | Line 6 | Dead Code | Pending Confirmation | [Link](https://lgtm.com/projects/g/appsecco/dvna/snapshot/36d5a48082a437f376121d0cabd70ba33652e0df/files/models/index.js#x11d2c9ac1cc28324:1) |
|6| server.js | Code Smell | Line 5 | Dead Code | Pending Confirmation | [Link](https://lgtm.com/projects/g/appsecco/dvna/snapshot/36d5a48082a437f376121d0cabd70ba33652e0df/files/server.js#xa40bec79ba258973:1) |
|7| package.json | Env Variable | NODE_ENV | Security Misconfiguration | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0016-securty-misconfig/simongomez95.feature) | Not Found |
|8| products.ejs | Parameter | name | Cross Site Scripting (Reflected) | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0079-xss/simongomez95.feature) | Not Found |
|9| adminusers.ejs | Parameter | register_name | Cross Site Scripting (Stored) | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0079-xss-register/simongomez95.feature) | Not Found |
|10| products.ejs | Parameter | register_name | Cross Site Scripting (Stored) | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0079-xss-stored/simongomez95.feature) | Not Found |
|11| passport.js | Logic | none | Weak Password Requirements | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0262-password-expiry/simongomez95.feature) | Not Found |
|12| app.js | Feature | Access Control | Improper Access Control | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0284-broken-access-control/simongomez95.feature) | Not Found |
|13| app.js | Feature | Account Management | Gestionar cuentas de usuarios | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0286-account-management/simongomez95.feature) | Not Found |
|14| none | Feature | Access Control | Establecer acciones de usuario seguras | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0287-secure-actions/simongomez95.feature) | Not Found |
|15| none | Server Configuration | HTTPS | Cleartext Transmission of Sensitive Information | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0319-cleartext-transmission/simongomez95.feature) | Not Found |
|16| none | Cookie | Session | Session Fixation | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0384-session-fixation/simongomez95.feature) | Not Found |
|17| appHandler.js | Parameter | file | Uncontrolled Resource Consumption | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0400-unrestricted-size/simongomez95.feature) | Not Found |
|18| appHandler.js | Parameter | file | Deserialization of Untrusted Data | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0502-insecure-deserialization/simongomez95.feature) | Not Found |
|19| appHandler.js | Feature | Credentials Management | Weak Password Requirements | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0521-precedent-passwords/simongomez95.feature) | Not Found |
|20| appHandler.js | Parameter | login | SQL Injection | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0564-sql-injection/jcardonaatfluid.feature) | Not Found |
|21| app.js | Parameter | file | XML External Entity | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0611-xxe/simongomez95.feature) | Not Found |
|22| server.js | Cookie | session | Insufficient Session Expiration | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0613-session-expiration/simongomez95.feature) | Not Found |
|23| authHandler.js | Parameter | token | Weak Password Recovery Mechanism for Forgotten Password | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0620-forgot-password/jcardonaatfluid.feature) | Not Found |
|24| app.js | Parameter | id | Authorization Bypass Through User-Controlled Key | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0639-idor/simongomez95.feature) | Not Found |
|25| app.js | Header | X-Frame-Options | Clickjacking | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0693-clickjacking/simongomez95.feature) | Not Found |
|26| appHandler.js | Feature | Logging | Insufficient Logging | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0778-insufficient-logging/simongomez95.feature) | Not Found |
|27| appHandler.js | Parameter | ping_address | Using Components with Known Vulnerabilities | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0915-vulnerable-components/simongomez95.feature) | Not Found |
|28| appHandler.js | Parameter | ping_address | Sensitive Cookie in HTTPS Session Without 'Secure' Attribute | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/1004-cookie-sec-attr/simongomez95.feature) | Not Found |
|29| app.js | Feature | Access Control | Improper Access Control | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/1029-sensitive-data-exposure/simongomez95.feature) | Not Found |
|30| passport.js | Parameter | csrfToken | CSRF | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0352-csrf-login/simongomez95.feature) | [Link](https://lgtm.com/projects/g/appsecco/dvna/snapshot/36d5a48082a437f376121d0cabd70ba33652e0df/files/server.js#xcdd593f219358217:1) |
|31| app.js | Parameter | csrfToken | CSRF | [Writeup](https://gitlab.com/fluidattacks/writeups/raw/master/vbd/dvna/0352-csrf-user/simongomez95.feature) | [Link](https://lgtm.com/projects/g/appsecco/dvna/snapshot/36d5a48082a437f376121d0cabd70ba33652e0df/files/server.js#xcdd593f219358217:1) |