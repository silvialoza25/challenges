## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Session Management
  Location:
    / - Accounts
  CWE:
    CWE-0286: Incorrect User Management -class-
      https://cwe.mitre.org/data/definitions/286.html
    CWE-1011: Authorize Actors -category-
      https://cwe.mitre.org/data/definitions/1011.html
  CAPEC:
    CAPEC-194: Fake the Source of Data -standard-
      https://capec.mitre.org/data/definitions/194.html
    CAPEC-151: Identity Spoofing -meta-
      https://capec.mitre.org/data/definitions/151.html
  Rule:
    REQ.034: https://fluidattacks.com/web/es/rules/034/
  Goal:
    Use an old account to access the platform
  Recommendation:
    Give the admin an option to block accounts

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
  TOE information:
    Given I am running DVNA on http://localhost:8000

  Scenario: Normal use case
    Given I go to "http//localhost:8000" and navigate the site
    Then I can login from another location and still be logged in in the first

  Scenario: Static detection
  No code available

  Scenario: Dynamic detection
  Logging in after leaving
    Given I have an account with certain privilege
    And there is no way for the admin to revoke access
    Then I will still have access to the system after I'm not supposed to

  Scenario: Exploitation
  Use a stale session to impersonate another user
    Given a user has an account
    And they are banned from the application for any reason
    Then they still have their account and there's no way for admin to remove it

  Scenario: Remediation
  Implement account management
    Given I implement an option that lets the admin remove accounts
    Then the vulnerability is fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.9/10 (Medium) - AV:P/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.5/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.5/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-28