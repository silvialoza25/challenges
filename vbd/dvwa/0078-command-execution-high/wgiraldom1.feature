## Version 1.4.1
## language: en

Feature:
  TOE:
    Damn Vulnerable Web Application - Command Execution
  Location:
    http://127.0.0.1/dvwa/vulnerabilities/exec - IP field
  Category:
    Web
  CWE:
    CWE-0078: Improper Neutralization of Special
    Elements used in an OS Command ('OS Command Injection')
      https://cwe.mitre.org/data/definitions/78.html
    CWE-94: Improper Control of Generation of Code ('Code Injection')
      https://cwe.mitre.org/data/definitions/94.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Execute arbitrary shell code
  Recommendation:
    Sanitize user input.

  Background:
  Machine information:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Apache Web Server |    2.4.1   |
    | PHP               |    7.1.1   |
    | MySQL             |   10.4.6   |
  Hacker Software:
    | Google Chrome     |  76.0.3809 |
    | Python            |    3.7.4   |
  TOE information:
    Given I'm accessing the site through http://localhost
    When the Web and MySQL Servers are running,
    Then I will start analysis.

  Scenario: Normal use case
    When logged in, I go to the Command Execution page
    Then a field appears, where I must write the IP address I want to ping.
    And the backend answers with the response of the ping command.

  Scenario: Static detection
    Given the algorithm in "/vulnerabilities/exec/sources/high.php" is
    """
    ...
    05:  $target = trim($_REQUEST[ 'ip' ]);
    06:
    07:  // Set blacklist
    08:  $sub = array(
    09:   '&'  => '',
    10:   ';'  => '',
    11:   '| ' => '',
    12:   '-'  => '',
    13:   '$'  => '',
    14:   '('  => '',
    15:   ')'  => '',
    16:   '`'  => '',
    17:   '||' => '',
    18:  );
    19:
    20:  // Remove any of the charactars in the array (blacklist).
    21:  $target = str_replace( array_keys( $sub ), $sub, $target );
    22:
    23:  // Determine OS and execute the ping command.
    24:  if( stristr( php_uname( 's' ), 'Windows NT' ) ) {
    25:   // Windows
    26:   $cmd = shell_exec( 'ping  ' . $target );
    27:  }
    ...
    """
    When a $target has any of the blacklisted characters,
    Then the shell command won't work as intended.

  Scenario: Dynamic detection
    Given It is clear from the source that a pipe character is allowed
    Given the sanitized characters related are "| " (line 11)
    And not the pipe character only ("|")
    When we use any input succeded by |dir
    Then the command gets executed [evidence](img1.png)

  Scenario: Exploitation
    Given we can know do almost anything in our shell,
    And we have full directory access,
    When we introduce a sensitive command,
    Then we could grab sensitive data. For example
    """
    Let $target be "a|type C:\xampp\htdocs\index.php"
    """
    And it will retrieve our installation main page source code.
    But also, we can produce more robust shell instructions
    Given we can write files.
    And we could also copy remote files into the server.

  Scenario: Extraction
    Given we have shell access
    When we use an utility like mysqldump,
    Then we could download all the databases
    Given the server has a default configuration
    Then we won't need to use '-' character.

  Scenario: Denial of service
    Given we got Apache user privileges,
    When we use the command line,
    Then we can stop all the related services.

  Scenario: Remediation
    Given we need this functionality only to ping hosts,
    And we only accept IPv4 addresses,
    When we sanitize the inputs, the problem will be solved.
    But we can also make the blacklist bigger,
    And perform sanitization on a per-octet basis.
    And it would look like this
    """
    ...
    04: $bytes = explode(".", $target);
    05: $nums = [];
    06: if(count($bytes) == 4){
    07:     foreach($bytes as $b){
    08:         if(!is_numeric($b)) return false;
    09:         $v = intval($b);
    10:         if($v > 255 || $v < 0) return false;
    11:         $nums[] = $v;
    12:     }
    13:     $target = implode(".", $nums);
    14: } else return false;
    ...
    """
    And after trying, only numbers with any amount of whitespace
    And succeded with a dot are allowed.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.0/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.7/10 (Medium) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.8/10 (Medium) - CR:M/IR:L/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MC:N/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-08-16
