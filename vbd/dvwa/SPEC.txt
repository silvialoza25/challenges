Name:   Damn Vulnerable Web Application - DVWA
Tech:   PHP
Site:   http://www.dvwa.co.uk/
Repo:   https://github.com/ethicalhack3r/DVWA
Author: RandomStorm
From:   https://www.owasp.org/index.php/OWASP_Vulnerable_Web_Applications_Directory_Project#tab=Off-Line_apps