## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Location:
   http://testphp.vulnweb.com - username (field)
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Detect and exploit vuln Insecure SQL query input
  Recommendation:
    Use parameterized queries with known tools

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Kali Linux      | 2020.3        |
    | Firefox         | 68.10.0esr    |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com
    Then I entered a PHP site which uses SQL requests

  Scenario: Normal use case
    Given I access to http://testphp.vulnweb.com/
    When I click on "singup"
    Then I access to http://testphp.vulnweb.com/login.php/
    And I can see a new page of login
    When I click on "signup here"
    Then I access to http://testphp.vulnweb.com/signup.php
    And this can be seen in [evidence](img1.png)

  Scenario: Static detection
    Given I access to http://testphp.vulnweb.com/signup.php
    When I open the code of the page from the browser inspector
    Then I can't see the website PHP code
    And I can't find possible vulnerabilities

  Scenario: Dynamic detection
    Given I access to http://testphp.vulnweb.com/signup.php
    When I change the "Username" input value from form
    """
    '
    """
    And this can be seen in [evidence](img2.png)
    Then I click on signup
    And the website shows me a MySQL SQL syntax error
    And this can be seen in [evidence](img3.png)
    Then I can conclude that the "Username" input has SQLi vulnerability
    And that the website database is MySQL

  Scenario: Exploitation
    Given I access to http://testphp.vulnweb.com/signup.php
    When I write the following SQL syntax as a value in "Username" input
    """
    test
    """
    Then the website shows me that the user "test" already exist
    And this can be seen in [evidence](img4.png)
    When I write the following SQL syntax as a value in "Username" input
    """
    test' -- -
    """
    Then the website shows me that the user "test' -- -" already exist
    And this can be seen in [evidence](img5.png)
    And I conclude that the website checks that there is the same "Username"
    And if not the website shows me exactly the same signup data
    When I write nothing as a value in "Username" input
    And I write anything in the others inputs
    And this can be seen in [evidence](img6.png)
    Then the website doesn't register me
    When I write the following SQL syntax as a value in "Username" input
    """
    ' -- -
    """
    And I write anything in the others inputs
    Then the website skips the user check
    And it shows me signup data with successfully
    And this can be seen in [evidence](img7.png)

  Scenario: Remediation
    When I use parameterized queries in the back
    Then the program can successfully avoid most of the known SQLi
    Given I know that the site is PHP and database is MySql
    Then I can use MySqli as tools of parameterized queries
    And I can see an example in the following line
    """
    ...
    $sql = "SELECT email FROM users WHERE username = ? AND password = ?"
    $sqlQuery = mysqli_prepare($connection, $sql);
    $result = mysqli_stmt_bind_param($sqlQuery, "ss", $userValue, $passValue);
    $result = msqli_stmt_execute($sqlQuery);

    if ($result) {
      $result = mysqli_stmt_bind_result($sqlQuery, $email);
      if(mysqli_stmt_fetch($sqlQuery)) {
        echo $email;
      }
    }
    ...
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10.0/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.4/10 (Critical) - CR:L/IR:L/MC:H/MI:H/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2021-03-25
