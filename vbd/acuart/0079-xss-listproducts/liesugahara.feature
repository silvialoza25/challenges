## Version 1.4.1
## language: en

Feature:
  TOE:
    acuart
  Category:
    Validate Inputs
  Location:
    http://testphp.vulnweb.com - artist
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject HTML code through XSS
  Recommendation:
    Sanitize URLs before returning the response

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU        | 2019.2      |
    | firefox         | 60.8.0      |
    | Burp Suite CE   | 2.1.01      |
  TOE information:
    Given I access http://testphp.vulnweb.com/listproducts.php
    And the server is running ngxix version 1.4.1

  Scenario: Normal use case
    When I open http://testphp.vulnweb.com/listproducts.php?artist=1
    Then I can see all of the artist's products

  Scenario: Static detection
    When I do not have access to the source code
    Then I am not able to do static detection

  Scenario: Dynamic detection
    When I use Burp Suite to intercept requests
    And I try to modify it
    And I add HTML code [evidence](req.png)
    Then I can see that the code is executed
    And I can conclude that it is vulnerable to xss attack

  Scenario: Exploitation
    When I open the url
    And I intercept
    Then I change the artist id to the following
    """
    artist=<script>alert(Document.cookie)</script>
    """
    Then I see a popup [evidence](xss.png)
    And I can conclude that the XSS attack was successful

  Scenario: Remediation
    When request is sent
    Then url should be sanitized before returning the response

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.1/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.8/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:N/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-08-09
