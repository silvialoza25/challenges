## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Location:
   http://testphp.vulnweb.com - username (field)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Detect and exploit vuln Insecure input fields
  Recommendation:
    Use parameterized queries with known tools

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Kali Linux      | 2020.3        |
    | Firefox         | 68.10.0esr    |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com
    Then I entered a PHP site which uses SQL requests

  Scenario: Normal use case
    Given I access to http://testphp.vulnweb.com/
    When I click on "singup"
    Then I access to http://testphp.vulnweb.com/login.php/
    And I can see a new page of login
    When I click on "signup here"
    Then I access to http://testphp.vulnweb.com/signup.php
    And this can be seen in [evidence](img1.png)

  Scenario: Static detection
    Given I access to http://testphp.vulnweb.com/signup.php
    When I open the source code of the page from the browser inspector
    Then I can see that form tag uses the POST method from
    """
    secured/newuser.php
    """
    And it is not possible to access to the source code PHP of this file
    Then I can't find possible vulnerabilities in that file

  Scenario: Dynamic detection
    Given I access to http://testphp.vulnweb.com/signup.php
    When I change the "Username" input value from form
    """
    <script>alert(1)</script>
    """
    And this can be seen in [evidence](img2.png)
    Then I click on signup
    And the website run the script
    And this can be seen in [evidence](img3.png)
    Then I conclude that the declaration does not validate input data

  Scenario: Exploitation
    Given I access to http://testphp.vulnweb.com/signup.php
    When I write the following script as a value in "Username" input
    """
    <script>alert("injecting code")</script>
    """
    Then the website run the script
    And it shows me the alert as can see in [evidence](img4.png)

  Scenario: Remediation
    When the website validate the data made from the input
    Then the program can successfully avoid code injections
    Given I know that the site is PHP
    Then I can use PHP filters to sanitize the input
    And I can see an example in the following line
    """
    ...
    $name_sanitized = filter_var($_GET['name'], FILTER_SANITIZE_STRING,
     FILTER_FLAG_STRIP_HIGH, FILTER_FLAG_STRIP_LOW);
    ...
    """
    And the patch applied sanitizes the "name" input

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.8/10 (Critical) - CR:L/IR:L/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2021-03-25
