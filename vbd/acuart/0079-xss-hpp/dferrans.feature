## Version 1.4.1
## language: en

Feature:
  TOE:
    acuart
  Category:
    Information exposure
  Location:
    http://testphp.vulnweb.com/hpp/params.php?p=valid&pp=16
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit php script using cross-site scripting
  Recommendation:
    Sanitize php input before printing the output

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | firefox         | 67.0(x64)   |
    | dirbuster       | 0.9.12      |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com/hpp/params.php
    And the server is running Ngxix version 1.4.1
    And PHP version 5.3.10

  Scenario: Normal use case
    When I open http://testphp.vulnweb.com/
    Then I can see the page

  Scenario: Static detection
    When I do not have access to the source code
    Then I am not able to do static detection

  Scenario: Dynamic detection
    When I use dirbuster to try detect unprotected directories
    Then I get the  result url
    Then I get try inject code using xss
    Then I can see the the script exploited file[evidence](xss-attack.png)
    And I can conclude that the php script is vulnerable to xss attack

  Scenario: Exploitation
    When I open  the url
    Then I pass some parameters to the url:
    """
    params.php?p=<script>alert("hacked")</script>&pp=16
    """
    Then I get a pop up that confirms the xss attack.

  Scenario: Remediation
    When the php script sanitize the get input.
    Then xss attack is not posible.
    """
    $var = filter_var($_GET['p'], FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_HIGH
                     | FILTER_FLAG_STRIP_LOW);
    """
    Then If I re-open the URL:
    """
    http://testphp.vulnweb.com/hpp/params.php
    ?p=<script>alert("hacked")</script>
    """
    Then I the page with with text sanitized
    Then I can conclude that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.0/10 (Medium) - E:F/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.0/10 (Medium) - MAV:N

  Scenario: Correlations
    No correlations have been found to this date {2019-06-07}
