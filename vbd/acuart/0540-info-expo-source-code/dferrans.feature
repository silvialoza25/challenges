## Version 1.4.1
## language: en

Feature:
  TOE:
    acuart
  Category:
    Information Exposure Through Source Code
  Location:
    http://testphp.vulnweb.com/_mmServerScripts/MMHTTPDB.php - opCode,TableName
  CWE:
    CWE-540: Information Exposure Through Source Code
  Rule:
    REQ.006: https://fluidattacks.com/web/rules/006/
    REQ.033: https://fluidattacks.com/web/rules/033/
    REQ.050: https://fluidattacks.com/web/rules/050/
    REQ.127: https://fluidattacks.com/web/rules/127/
  Goal:
    Get database information from php script
  Recommendation:
    Do not store admin scripts in public locations.

  Background:
  Hacker's software:
  | <Software name> | <Version>   |
  | ubuntu          | 18.04.2     |
  | firefox         | 67.0(x64)   |
  | burpsite        | 2.1.01      |
  | google search   | n/a         |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com
    And the server is running nginx version 1.4.1
    And PHP version 5.3.10

  Scenario: Normal use case
    When I open http://testphp.vulnweb.com/
    Then I can see the page

  Scenario: Static detection
    When I look at the code MMHTTPDB.php
    Then I am able to see that this file accepts parameters to connect to a db
    And I am able to see functions to execute in the database
    """
    if ($_POST['Type'] == "MYSQL")
    {
     require("./mysql.php");
     $oConn = new MySqlConnection($_POST['ConnectionString'],
     $_POST['Timeout'], $_POST['Host'], $_POST['Database'], $_POST['UserName'],
     $_POST['Password']);
    }
    ..
    if($_POST['opCode'] == "GetTables")echo($oConn->GetTables());
    elseif ($_POST['opCode'] == "GetColsOfTable")
    echo($oConn->GetColumnsOfTable($_POST['TableName']));
    """
    Then I conclude that I can do multiple operations in the db with the script

  Scenario: Dynamic detection
    When I use google to search for directories in the testphp.vulnweb.com
    Then I get the folder where the script is located
    When I try to get the script contents of /_mmServerScripts/MMHTTPDB.php
    Then I am not able to get any information about the script using the url
    When I use a reported local file inclusion vulnerability
    """
    0098-php-local-file-inclusion
    """
    Then I am able to get the file content of these files
    """
    /_mmServerScripts/MMHTTPDB.php
    /_mmServerScripts/mysql.php
    database_connect.php
    """
    Then I am able to get the username and password from the database
    And I am able to see all the script options and functions
    Then I am able know how the script works with the available parameters.

  Scenario: Exploitation
    When I open the url
    And I send the required parameters using POST
    """
    opCode=GetTables&Type=MYSQL&Host=127.0.0.1&Database=acuart
    &UserName=acuart&Password=trustno1&TableName=users
    """
    Then I am able to get all tables in db file[evidence](dblist.png)
    When I use the GetColsOfTable method in the script as post parameter
    Then I Am able to get the table structure file[evidence](dbstruc.png)
    When I use the ExecuteSQL method in the script as post parameter
    And I use the following url encoded query
    """
    SELECT uname, pass, cc, address FROM users
    """
    Then I am able to see records in users table file[evidence](dbstruc.png)
    Then I can conclude that the script allow me to extract DB information
    And I can extract any record from the database.

  Scenario Outline: Extraction
  Exposed files
    When I pass post parameters to MMHTTPDB.php
    Given I can use ExecuteSQL to execute sql using post parameter
    And I can use SQL param to send sql query
    Then I can access <table> and read <output>
      | <table>      | <output> | <evidence>     |
      | users        |   OUTPUT | lusr.png       |
      | carts        |   OUTPUT | carts.png      |
      | products     |   OUTPUT | products.png   |
    Then I conclude that all tables can be extracted using this script.

  Scenario: Remediation
    When the MMHTTPDB.php is removed form the server
    Then I will not be able to send post parameters to query the database
    Then the server will answer with a not 404 not found error.
    And I can conclude that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.9/10 (Medium) - AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.4/10 (Medium) - E:P/RL:W/RC:C/CR:X/IR:X/AR:X/
  Environmental: Unique and relevant attributes to a specific user environment
    5.4/10 (Medium) - MAV:N

  Scenario: Correlations
    vbd/acuart/0098-php-local-file-inclusion
    Given I can get files using local file inclusion
    And I can Download MMHTTPDB.php with dependencies
    When I send parameters using post method
    Then I get the database information.
