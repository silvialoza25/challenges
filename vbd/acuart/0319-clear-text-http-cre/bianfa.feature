## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Location:
   http://testphp.vulnweb.com:80 TCP
  CWE:
    CWE-0319: Cleartext Transmission of Sensitive Information
  Rule:
    REQ.177 Avoid caching and temporary files
  Goal:
    Detect and exploit vuln Insecure from HTTP
  Recommendation:
    Use SSL for HTTP requests at least

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Kali Linux      | 2020.3        |
    | Firefox         | 68.10.0esr    |
    | OWASP ZAP       | 2.9.0         |
  TOE information:
    Given I am accessing the site http://testphp.vulnweb.com
    Then I entered a PHP site which uses SQL requests

  Scenario: Normal use case
    Given I access to http://testphp.vulnweb.com/
    When I click on "singup"
    Then I access to http://testphp.vulnweb.com/login.php
    And I can see a new page of login

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't perform a static detection

  Scenario: Dynamic detection
    Given I access to http://testphp.vulnweb.com/login.php
    When I load this page of login
    Then I click on the padlock icon to get site information
    And the browser says me that the connection is not secure
    And this can be seen in [evidence](img1.png)

  Scenario: Exploitation
    Given I access to http://testphp.vulnweb.com/login.php
    When I set up the proxy of my browser with OWASP ZAP
    Then I can see all the HTTP requests that are made in the browser
    And I can see in clear text the requests of the insecure connections
    Then I try to login with "test" as username and "test" as password
    And this can be seen in [evidence](img2.png)
    Then I can see the requests that were made in OWASP ZAP
    And this can be seen in [evidence](img3.png)
    And I open the request of POST method
    And I see the credentials in clear text
    And this can be seen in [evidence](img4.png)

  Scenario: Remediation
    When I set up the HTTP service with a SSL certificate in the server
    Then the website can successfully avoid sniffing attacks
    And the HTTP requests travel safer
    And also I recommend using encryption within the same website

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.8/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.4/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.4/10 (Critical) - CR:H/IR:H/MC:H/MI:H/MA:X

  Scenario: Correlations
    No correlations have been found to this date 2021-03-26
