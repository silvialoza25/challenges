## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Session Management
  Location:
    bodgeit/ - JSESSIONID (Cookie)
  CWE:
    CWE-0384: Session Fixation -compound-
      https://cwe.mitre.org/data/definitions/384.html
    CWE-0472: External Control of Assumed-Immutable Web Parameter -base-
      https://cwe.mitre.org/data/definitions/472.html
    CWE-0642: External Control of Critical State Data -class-
      https://cwe.mitre.org/data/definitions/642.html
    CWE-1019: Validate Inputs -category-
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-060: Reusing Session IDs (aka Session Replay) -detailed-
      http://capec.mitre.org/data/definitions/60.html
    CAPEC-593: Session Hijacking -standard-
      http://capec.mitre.org/data/definitions/593.html
    CAPEC-021: Exploitation of Trusted Credentials -meta-
      http://capec.mitre.org/data/definitions/21.html
  Rule:
    REQ.225: https://fluidattacks.com/web/es/rules/255/
  Goal:
    Steal a user's session and navigate as them while they are on
  Recommendation:
    Forbid concurrent sessions

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/password.jsp
    Then I can change my password

  Scenario: Static detection
  No session concurrency control
    When I look at the configuration at "/WEB-INF/web.xml"
    """
    <?xml version="1.0" encoding="ISO-8859-1"?>
    <web-app xmlns="http://java.sun.com/xml/ns/j2ee"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/
        xml/ns/j2ee/web-app_2_4.xsd"
        version="2.4">

        <servlet>
            <servlet-name>InitServlet</servlet-name>
            <display-name>The BodgeIt Store Init Servlet</display-name>
            <!-- servlet-class>org.zaproxy.bodgit.servlet.InitServlet</servlet-c
            lass-->
            <jsp-file>/init.jsp</jsp-file>
            <load-on-startup>1</load-on-startup>
        </servlet>
        <servlet-mapping>
            <servlet-name>InitServlet</servlet-name>
            <url-pattern>/servlet/InitServlet</url-pattern>
        </servlet-mapping>

      <welcome-file-list>
          <welcome-file>home.jsp</welcome-file>
          <welcome-file>home.html</welcome-file>
        </welcome-file-list>

        <!-- error-page>
          <error-code>404</error-code>
          <location>/axis2-web/Error/error404.jsp</location>
        </error-page-->

        <!-- error-page>
            <error-code>500</error-code>
            <location>/axis2-web/Error/error500.jsp</location>
        </error-page-->
    </web-app>
    """
    Then I see it doesn't have any settings for session concurrency

  Scenario: Dynamic detection
  Logging in twice
    Given I log into the site with my account
    And log in again from a second device
    Then I'm logged in in both devices and can perform actions no problem
    Then I know there's no session concurrency control


  Scenario: Exploitation
  Login as another user while they are still working
    Given I know the victim is on the site
    Given I have obtained the victims credentials or have another way to login
    Then I log in as the victim
    Then I can use the site as the victim at the same time as them
    Then my malicious actions are going to blend with their legitimate ones

  Scenario: Remediation
  Forbid concurrent sessions
    Given I add this to "web.xml"
    """
    <beans:beans xmlns="http://www.springframework.org/schema/security" ... />
    <http>
      <session-management>
        <concurrency-control
          max-sessions="1"
          expired-url="/your-page-here" />
      </session-management>
    </http>
    """
    Then when the user logs in while I'm logged in as them
    Then my session will be closed and i'll have to relogin

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0089-lgin-as-mlbx-at-domain-com
      Given I can login as a user via SQLi
      Then I can do so and perform actions on the site while the user is also on
      Then my actions are harder to trace and the user is none the wiser
