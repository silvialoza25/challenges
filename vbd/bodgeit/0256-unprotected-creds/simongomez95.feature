## Version 1.4.2
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Insecure Communication
  Location:
    bodgeit/password.jsp - Password Storage
  CWE:
    CWE-0256: Unprotected Storage of Credentials -variant-
      https://cwe.mitre.org/data/definitions/256.html
    CWE-0522: Insufficiently Protected Credentials -base-
      https://cwe.mitre.org/data/definitions/522.html
    CWE-0287: Improper Authentication -class-
      https://cwe.mitre.org/data/definitions/287.html
    CWE-1010: Authenticate Actors -category-
      https://cwe.mitre.org/data/definitions/1010.html
  CAPEC:
    CAPEC-066: SQL Injection -standard-
      http://capec.mitre.org/data/definitions/66.html
    CAPEC-248: Command Injection -meta-
      http://capec.mitre.org/data/definitions/248.html
  Rule:
    REQ.127: https://fluidattacks.com/web/en/rules/127/
  Goal:
    Dump password
  Recommendation:
    Always hash passwords in the db

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Wireshark             | 2.6.5     |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/search.jsp
    Then I can search for products

  Scenario: Static detection
  No precedent password validation
    Given I see the code at "bodgeit/root/password.jsp"
    """
    ...
    27  ResultSet rs = null;
        28  try {
        29      stmt.executeQuery("UPDATE Users set password= '" + password1 +
        "'
    where name = '" + username + "'");
    ...
    """
    Then I see it stores passwords in plaintext

  Scenario: Dynamic detection
  No dynamic detection

  Scenario: Exploitation
  Finding plaintext passwords
    Given I find a way to access the application database
    Then I can query the Users table
    And obtain the usernames and passwords of the users

  Scenario: Remediation
  Use a hashing function before storing password
    Given I patch the code like this
    """
    ...
    27  ResultSet rs = null;
        28  try {
        29      stmt.executeQuery("UPDATE Users set password= '" + password1 +
        "' where name = '" + username + "'");
    ...
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.8/10 (Medium) - AV:A/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.3/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-22
