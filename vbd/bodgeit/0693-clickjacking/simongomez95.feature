## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Other Bugs
  Location:
    bodgeit/admin.jsp - Access
  CWE:
    CWE-1021: Improper Restriction of Rendered UI Layers or Frames -base-
      https://cwe.mitre.org/data/definitions/1021.html
    CWE-0441: Unintended Proxy or Intermediary ('Confused Deputy') -class-
      https://cwe.mitre.org/data/definitions/441.html
    CWE-0442: Web Problems -category-
      https://cwe.mitre.org/data/definitions/442.html
  CAPEC:
    CAPEC-222: iFrame Overlay -detailed-
      https://capec.mitre.org/data/definitions/222.html
    CAPEC-103: Clickjacking -standard-
      https://capec.mitre.org/data/definitions/103.html
    CAPEC-173: Action Spoofing -meta-
      https://capec.mitre.org/data/definitions/173.html
  Rule:
    REQ.175: https://fluidattacks.com/web/es/rules/175/
  Goal:
    Make a user click on a site action without knowing
  Recommendation:
    Implement a Content Security Policy

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/
    Then I can login and buy products as my own user

  Scenario: Static detection
  No user validation in admin section
    When I look at the code at "/bodgeit/root/WEB-INF/web.xml"
    """
    ...
    03  <web-app xmlns="http://java.sun.com/xml/ns/j2ee"
    04  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    05  xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/
    xml/ns/j2ee/web-app_2_4.xsd"
    06  version="2.4">
    07
    08  <servlet>
    09  <servlet-name>InitServlet</servlet-name>
    10  <display-name>The BodgeIt Store Init Servlet</display-name>
    11  <!-- servlet-class>org.zaproxy.bodgit.servlet.InitServlet</servlet-class
    -->
    12  <jsp-file>/init.jsp</jsp-file>
    13  <load-on-startup>1</load-on-startup>
    14  </servlet>
    15  <servlet-mapping>
    16  <servlet-name>InitServlet</servlet-name>
    17  <url-pattern>/servlet/InitServlet</url-pattern>
    18  </servlet-mapping>
    19
    20  <welcome-file-list>
    21  <welcome-file>home.jsp</welcome-file>
    22  <welcome-file>home.html</welcome-file>
    23  </welcome-file-list>
    24
    25  <!-- error-page>
    26  <error-code>404</error-code>
    27  <location>/axis2-web/Error/error404.jsp</location>
    28  </error-page-->
    29
    30  <!-- error-page>
    31  <error-code>500</error-code>
    32  <location>/axis2-web/Error/error500.jsp</location>
    33  </error-page-->
    34
    35  </web-app>
    ...
    """
    Then I see it doesn't have a CSP or X-Frame-Options filter

  Scenario: Dynamic detection
  Request Analysis
    Given I use Burp to intercept a request to the password reset page
    And the response doesn't contain a CSP header
    """
    HTTP/1.1 200 OK
    Server: Apache-Coyote/1.1
    Content-Type: text/html;charset=ISO-8859-1
    Content-Length: 2460
    Date: Wed, 16 Jan 2019 20:16:52 GMT
    Connection: close
    """
    Then I know it's injectable in an iFrame and I can clickjack the page

  Scenario: Exploitation
  Clickjacking to get a victim to change their password
    Given I know the page is vulnerable to clickjacking
    Then I create a fake page that promises prizes for entering "bunnies" twice
    And overlap it on an iFrame of the password change page
    Then I send it to an user
    And they unsuspectingly input "bunnies" and click submit
    Then I have their new password and can take over their account

  Scenario: Remediation
  Only rendering admin page if user is admin
    Given I add this to the "web.xml"
    """
    ...
    <filter>
    <filter-name>ResponseHeaderFilter</filter-name>
    <filter-class>com.web.filter.responseheaderfilter.ResponseHeaderFilte
    r</filter-class>
    </filter>
    <filter-mapping>
    <filter-name>ResponseHeaderFilter</filter-name
    <url-pattern>*</url-pattern>
    </filter-mapping>
    ...
    """
    And this to a new file "response-header-filter.xml"
    """
    <?xml version="1.0" encoding="UTF-8" ?>
    <response-header-mapper>
    <!-- generic rule for all html requests -->
    <mapping url="(.*).html">
    <default>
    <response-headers
    <header key="X-Frame-Options" value="DENY"/
    <header key="Content-Type" value="text/html"/>
    <!-- cache all the html pages for one hour -->
    <header key="Cache-Control" value="private, max-age=3600"/>
    </response-headers>
    </default>
    </response-header-mapper>
    """
    Then the "X-Frame-Options: DENY" header is added
    And the pages can't be injected into iFrames anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.9/10 (Medium) - AV:N/AC:H/PR:N/UI:R/S:U/C:L/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.5/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.5/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-16
