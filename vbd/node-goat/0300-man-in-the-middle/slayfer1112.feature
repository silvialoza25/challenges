## Version 1.4.1
## language: en

Feature: Man in the middle
  TOE:
    node-goat
  Category:
    Channel Accessible by Non-Endpoint('Man in the middle')
  Location:
    http://localhost:4000/
  CWE:
    CWE-300: Channel Accessible by Non-Endpoint
    https://cwe.mitre.org/data/definitions/300.html
  Rule:
    REQ.228: Authenticate Using Standard Protocols
    https://fluidattacks.com/web/rules/228/
    REQ.234: Protect authentication credentials
    https://fluidattacks.com/web/rules/234/
  Goal:
    Get the username and password of the user
  Recommendation:
    Encrypt the data in all requests and validate the origin.

  Background:
  Hacker's software:
    | <Software name> |    <Version>    |
    | Windows         |       10        |
    | Chrome          |  80.0.3987.122  |
    | Burp Suite      |      2020.1     |
  TOE information:
    Given I am accessing the site http://localhost:4000
    And the server is running MongoDB version 2.1.18
    And Nodejs version 10.15.3

  Scenario: Normal use case
    Given I access to "http://localhost:4000/"
    And I login in with a new user
    When I was redirected to the dashboard
    Then I can manage my account

  Scenario: Static detection
    Given The source code
    When I see the code in "/app/routes/session.js"
    """
    this.handleLoginRequest = function(req, res, next) {
    var userName = req.body.userName;
    var password = req.body.password;
    """
    Then I go to the route "/app/views/login.html"
    """
    <form method="post" role="form" method="post" id="loginform">
      <div class="form-group">
        <label for="userName">User Name</label>
        <input type="text" class="form-control" id="userName" name="userName"
               value="{{userName}}" placeholder="Enter User Name">
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password"
               name="password" value="{{password}}"
               placeholder="Enter Password">
      </div>
    """
    And I see that the request don't crypt the parameters
    When I know that I suspect that an evil user can perform a MitM attack
    Then The evil user with the MitM can get the parameters
    And If the evil user gets the login parameters
    Then He can log in with valid credentials
    And This can be vulnerable to MitM

  Scenario: Dynamic detection
    Given I'm in the login page
    And I'm using Burp Proxy
    When I was in the login page I can type userName and password
    Then If I send the login form without params I see a response
    """
    Invalid Username/Password
    """
    And If I try again but with "intercept on" in Burp I get the request
    """
    POST /login HTTP/1.1
    Host: 192.168.1.184:3000
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0)
                Gecko/20100101 Firefox/74.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,
            image/webp,*/*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 62
    Origin: https://192.168.1.184:3000
    Connection: close
    Referer: https://192.168.1.184:3000/login
    Cookie: connect.sid=s%3Anh51nLTkepM-mfWpjZ_VAaRvClQUaTNX.
            7QzrJtxolgDhb5H35WOSaDD1uM9b9U4lUJuoafhE4Es
    Upgrade-Insecure-Requests: 1

    userName=&password=&_csrf=6hQX4beJ-zPuA92b2YSN7_9y5o6G52FnL22U
    """
    When I see the request
    Then I can see the parameters for userName and password
    And If any user try to login with their credentials
    Then I can get the username and password of that user
    And I found a vulnerability

  Scenario: Exploitation
    Given I make a man in the middle in a Cybercafe
    Given I can get all request that the user can send
    When I install my MitM I only need to wait that the user try to login
    Then I use Burp with the "Intercept off"
    And I use "Intercept off" because I don't want the user to notice anything
    When The user use the Cybercafe and try to login [evidence](img1)
    Then The user can log in and make some things in his account
    And The user leave the Cybercafe
    When I see that the user left I see my MitM with Burp
    Then I go to the tab "proxy/HTTP History" and see all the request
    And I order by host to search the host that I'm interested
    When I see the host I can see a method POST in login URL
    Then I click on it to see the request [evidence](img2)
    And I can see the userName and Password with a token param
    When I go to the web and try to login with this credentials
    Then I see that the login is successful
    And I exploit the vulnerability

  Scenario: Remediation
    Given The source code
    When I want to make a safe request
    Then I think that encoding the request is a safe way
    And I try to make a unique encrypt each time when send request
    When I think to use javascript to encode the username and password
    Then I need to decode the data in the server to evaluate
    And I create a new file in "/app/assets/js/script/slayfer1112.js"
    When I create the file I write a simple encode function relation the time
    Then I write the following code
    """
    var input = document.getElementById("password");

    input.addEventListener("keyup", function (event) {
      if (event.keyCode === 13) {
        event.preventDefault();
        docu ment.getElementById("Submit").click();
      }
    });

    document.getElementById("Submit").onclick = crypt;

    function crypt() {
      var user = document.getElementById("userName").value;
      var pass = document.getElementById("password").value;
      var z = new Date();
      var ye = z.getFullYear();
      var mo = z.getMonth();
      var d = z.getDay();
      var h = z.getHours();
      var m = z.getMinutes();
      var t = ye - mo - d - h - m;
      x = user + " " + t;
      y = pass + " " + t;
      x = btoa(x);
      y = btoa(y);
      document.getElementById("userName").value = x;
      document.getElementById("password").value = y;

      document.getElementById("loginform").submit();

      document.getElementById("div user").innerHTML = '<label for="userName">
                      User Name</label><label class="form-control"></label>';
      document.getElementById("div pass").innerHTML = '<label for="password">
                       Password</label><label class="form-control"></label>';

      return console.log("Finish");
    };
    """
    And This code means
    """
    In this code I use the time without seconds and miliseconds because the
    request can have a delay, for that reason I use until minutes, I make
    a little operation with some numbers and add it in the userName and
    password value, then I encode it in base64 and submit the form
    (I use base64 for this, but I recommend to use a stronger encryptation)
    """
    When I finish the encrypt I need to found a way how hidden the script
    Then I use to obfuscate the script the following web
    """
    https://www.javascriptobfuscator.com/Javascript-Obfuscator.aspx
    """
    And I see the result [evidence](img3)
    When I obfuscate the code I know that it makes harder to read
    Then I make that because I want that the attacker can't read the script
    And I need to found a way to decode the params in the back-encode
    When I go to "/app/routes/session.js"
    Then I add a decode function
    """
    var z = new Date();
    var ye = z.getFullYear();
    var mo = z.getMonth();
    var d = z.getDay();
    var h = z.getHours();
    var m = z.getMinutes();
    var t = ye - mo - d - h - m;

    var userName = req.body.userName;
    var password = req.body.password;

    userName = userName.toString();
    password = password.toString();

    var buff1 = new Buffer(userName,'base64');
    var buff2 = new Buffer(password,'base64');

    userName = buff1.toString('ascii');
    password = buff2.toString('ascii');

    userName = userName.split(" ");
    password = password.split(" ");

    var x = userName[0];
    var y = password[0];
    if (userName[1]==t.toString()||userName[1]==(t+1).toString()){
        userName = userName[0];
        password = password[0];
    } else {
        userName = "bad request in the user: " + userName[0];
        password = "bad request in the user: " + userName[0];
        console.log("bad request in the user: " + userName[0]);
    }
    """
    And If the var "t" match with the request the userName and password pass
    And If the params pass, can be eval with the database
    When I save the changes I try to make the attack again
    Then I get the request in Burp [evidence](img4)
    And I see that the userName and password was encrypted
    When I try to use this params in the userName and Password fields
    Then I see a message that says "Invalid username" [evidence](img5)
    And I try to send the encrypted request with Burp
    When I send the request I can find 2 ways
    """
    The first way is status 500 internal error because the csrf token expire

    The second way is if I get a valid csrf token and send
    the request encrypted
    """
    Then If I get the second way I see the following response [evidence](img6)
    And I see in the server console the following message
    """
    bad request in the user: <username>
    """
    When I found it I know that the MitM was mitigated
    And I fix the vulnerability

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      5.9/10 (Medium) - AV:N/AC:H/PR:N/UI:R/S:U/C:H/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      5.7/10 (Medium) - E:H/RL:W/RC:C/CR:H/IR:L
    Environmental: Unique and relevant attributes to a specific user environment
      6.9/10 (Medium) - MAV:N/MAC:H/MPR:N/MUI:R/MS:U/MC:H/MI:L/MA:N


  Scenario: Correlations
    No correlations have been found to this date 2020-04-03
