## Version 1.4.1
## language: en

Feature:
  TOE:
    rails-goat
  Category:
    Broken Access Control
  Location:
    http://localhost:3000/admin/1/dashboard - ID
  CWE:
    CWE-285: Improper Access Control
  Rule:
    REQ.096: https://fluidattacks.com/web/rules/096/
    REQ.097: https://fluidattacks.com/web/rules/097/
    REQ.300: https://fluidattacks.com/web/rules/300/
  Goal:
    Access the admin dashboard.
  Recommendation:
    Restrict access by validating if the current user has admin privileges.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU        | 2019.2      |
    | firefox         | 60.7.2      |
  TOE information:
    Given I am accessing the site http://localhost:3000
    And enter a Ruby on Rails site that allows me to create a new user
    And the server is running Ruby version 2.3.5
    And Rails version 5.1.4
    And MariaDB version 10.1.29
    And docker-compose version 1.22.0

  Scenario: Normal use case
    Given I access http://localhost:3000
    When I log in
    Then I can't see any admin tools

  Scenario: Static detection
    When I look at the admin controller in "app/controllers/"
    Then I can see that it is skipping the admin validation
    And it can be found in this file in line 1.
    """
    1 before_action :administrative, :if => :admin_param, :except =>[:get_user]
    """
    When I look at the admin_param method from line 64 to line 66
    """
    64 def admin_param
         params[:admin_id] != '1'
    66 end
    """
    Then I can conclude that the admin validation is always being skipped.

  Scenario: Dynamic detection
    Given I access http://localhost:3000
    And I log in with any user
    When I try to access the admin panel typing in the URL
    """
    http://localhost:3000/admin/1/dashboard
    """
    Then I can conclude that it does not validate if the user is administrator.

  Scenario: Exploitation
    Given the app does not validate if the user is admin
    When I access http://localhost:3000/admin/1/dashboard
    Then I see a list of the users with action buttons [evidence](admin.png)
    And I can conclude it can be accessed by unauthorized users.

  Scenario: Remediation
    Given I have patched the code by removing the admin_param method
    And the validations in the before_action
    And added the following to the application_controller.rb from line 35 to 36
    """
    35 flash[:error] = "Sorry, you are not authorized"
    36 redirect_to home_dashboard_index_path
    """
    When I try to access the admin panel
    Then I get an error message [evidence](no-auth.png)
    And I can confirm that the vulnerability was successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.4/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.1/10 (High) - CR:H/MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-12
