#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "Usage: container.sh port"
  else
    docker run --rm -d --name xvwa -p "$1:80" -t tuxotron/xvwa
fi
