# Version 1.4.1
## language: en

Feature:
  TOE:
    xvwa
  Location:
    http://localhost/xvwa/vulnerabilities/xpath/
  CWE:
    CWE-643: Improper Neutralization of Data within XPath
    Expressions ('XPath Injection')
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Generate a proper input to get the XML data structure.
  Recomentation:
    Sanitise inputs for the xml query. In order to avoid XPath flaw

  Background:
  Hacker's software:
    | <software name>      | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 74.0.3729.131  |
  TOE information:
    Given I'm entering the site http://localhost/xvwa
    And I access a site which has been build in php and uses sql

  Scenario: Normal use case
    Given I enter http://localhost/xvwa/vulnerabilities/xpath/
    And I write something in Coffe search input

  Scenario: Static detection
    Given I enter http://localhost/xvwa/vulnerabilities/xpath/
    And I have access to the machine
    Then I get it that path is on the url
    And I go to this path and see home.php in xpath directory
    Then I saw the vulnerable code
    """
    2 <?php
    3  $result = '';
    4  if(isset($_POST['submit'])){
    5  $doc = new DOMDocument;
    6  $doc->load('coffee.xml');
    7  $xpath = new DOMXPath($doc);
    8  $input = $_POST['search'];
    9  $query = "/Coffees/Coffee[@ID='".$input."']";
    10  $result = isset($xpath->query($query)) ? $xpath->query($query) : '';
    11  $result = $xpath->query($query);
    12  }
    13  ?>
    """
    And I see that prepares a not proper handled xpath query

  Scenario: Dynamic detection
    Given I enter http://localhost/xvwa/vulnerabilities/xpath/
    And I write a valid Xpath injection query "'" in the search input
    Then it shows an Invalid predicate xml error
      """
    Warning: DOMXPath::query(): Invalid predicate in
    /var/www/html/xvwa/vulnerabilities/xpath/home.php on line 11
    Warning: DOMXPath::query(): Invalid expression in
    /var/www/html/xvwa/vulnerabilities/xpath/home.php on line 11
    """
    Then I came to the obvious conclusion that the input is not sanitised

  Scenario: Extraction
    Retrieve all items from the Database
    Given I enter "' or '1'='1" in the field
    And I click the search button
    Then I get an outcome as html with all the records
    And Ergo can be seen in [evidence](evidence2.png)

  Scenario: Remediation
    When the page has been developed it should sanitise the search input
    And It should be solved with this code
    """
    //input validation
    //of the searh parameter
    $input = filter_var($_POST['search'],FILTER_SANITIZE_STRING);
    """
    Then it solves the issue

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.6/10 (Low) - AV:L/AC:L/PR:N/UI:R/S:C/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.2/10 (Low) - E:F/RL:O/RC:R
  Environmental: Unique and relevant attributes to a specific user environment
    2.4/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been discovered to this date 2019-05-08