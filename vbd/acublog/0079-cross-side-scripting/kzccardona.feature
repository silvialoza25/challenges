## Version 1.4.1
## language: en

Feature:
  TOE:
    Acublog
  Category:
    Cross Side Scripting
  Location:
    http://testaspnet.vulnweb.com/Comments.aspx?id=3 - comment (field)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
      https://cwe.mitre.org/data/definitions/79.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit cross side scripting
  Recommendation:
    Always do input validations

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Microsoft Windows | 10.0.17763.437  |
    | Firefox           | 69.0 (64 bit)   |
  TOE information:
    Given I am accessing the site http://testaspnet.vulnweb.com/
    And Entered a .NET site which uses SQL requests
    And IIS version 8.5
    And ASP.NET 2.0.5

  Scenario: Normal use case
    Given I access http://testaspnet.vulnweb.com
    When I click in Comments link
    Then it's displayed the comments session

  Scenario: Static detection
    When static detection requires access to source code, asp.net in this case
    And I only have access to the HTML code through elemnt inspect
    Then I can't perform a static detection

  Scenario: Dynamic detection
    Given I access to comments session
    When I try to verify if the page saves html code
    Then I use html tags to write a word in bold in the input
    """
    <p><b>Hello</b></p>
    """
    And I click on send button
    Then the page reloads the comments and shows my comment
    And I recognize this input isn't sanitized

  Scenario: Exploitation
    Given I access http://testaspnet.vulnweb.com/Comments.aspx?id=3
    When I wrote javascript code in comment input
    """
    <script>alert('This is XSS')</script>
    """
    And I click on send button
    Then the page reloads the comments and shows my alert
    """
    This is XSS
    """
    And I can see my alert every time the page is reloaded
    Then I can conclude that is possible to exploit stored XSS

  Scenario: Remediation
    When design the apps sanitize inputs and outputs
    And use frameworks designed to escape XSS
    Then the program can prevent XSS vulnerabilities

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.0/10 (Medium) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.0/10 (Medium) - CR:M/IR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-09-13
