## Version 1.4.1
## language: en

Feature:
  TOE:
    xss-quiz
  Category:
    Cross-Site Scripting
  Location:
    https://xss-quiz.int21h.jp/stage--5.php
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute Javascript code performing an XSS attack
  Recommendation:
    Encode correctly user input

  Background:
  Hacker Software:
    | Firefox            |   79.0     |
    | Windows            |   10.0     |

  TOE information:
    Given I access the site by its URL
    Then I see an input box to search something [evidences](site.png)

  Scenario: Normal use case
    Given I access the site [evidences](normal.png)
    When I enter any string in the input box
    Then I can search that string
    And if it does not find the string it returns the following message
    """
    No results for your Query. Try again:
    """

  Scenario: Static detection
    Given I can't access to the backend source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the site allows user interaction
    When I try trigger the input box for any XSS bug
    """
    <script>alert(1337)</script>
    """
    Then I can only enter 15 characters [evidences](limitedxss.png)
    """
    <script>alert(1
    """
    When I inspect the HTML source code [evidences](source.png)
    Then I realize that the input only allows 15 characters
    """
    <form action="?sid=d127fc5f83f2427185c2a3eaf962d0b70141854e" method="post">
        <hr class=red>Search:
            <input type="text" name="p1" maxlength="15" size="30" value="">
            <input type="submit" value="Search">
        <hr class=red>
    </form>
    """
    Given only allows 15 characters my idea is use "Inspect Element" of Firefox
    And increase the input limit [evidences](modified.png)(works.png)
    """
    Rigth click on input box -> Inspect Element -> Change maxlength value
    """
    When I alter this the next thing is to trigger an XSS
    """
    "><script>alert(1337)</script>
    """
    Then this way I could execute JS code on the site [evidences](1337.png)

  Scenario: Remediation
    Given The idea is not to interpret user input as code
    Then I could implement a function that correctly encodes the HTML code tags
    """
    function htmlEntities(input) {
        return String(input).replace(/&/g, '&amp;').
               replace(/</g, '&lt;').replace(/>/g, '&gt;').
               replace(/"/g, '&quot;');
    }
    """
    And this allows taking the input as raw text

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      6.1/10 (Medium) - /AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      5.9/10 (Low) - /E:H/RL:T/RC:C
    Environmental:Unique and relevant attributes to a specific user environment
      5.2/10 (Low) - /IR:L/MAV:N/MAC:L

  Scenario: Correlations
    No correlations have been found to this date 2020-10-23
