## Version 1.4.1
## language: en

Feature:
  TOE:
    xss-quiz
  Category:
    Cross-Site Scripting
  Location:
    https://xss-quiz.int21h.jp/stage_09.php
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute Javascript code performing an XSS attack
  Recommendation:
    Encode correctly user input

  Background:
  Hacker Software:
    | Firefox            |   79.0     |
    | Windows            |   10.0     |

  TOE information:
    Given I access the site by its URL
    Then I see an input box to search something [evidences](site.png)

  Scenario: Normal use case
    Given I access the site [evidences](normal.png)
    When I enter any string in the input box
    Then I can search that string
    And if it does not find the string it returns the following message
    """
    No results for your Query. Try again:
    """

  Scenario: Static detection
    Given I can't access to the backend source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the site allows user interaction
    When I try trigger the input box for any XSS bug
    """
    <script>alert(1337)</script>
    """
    Then I see that the input is inside double quotes [evidences](dynamic.png)
    """
    <input type="text" name="p1" size="50" value="<script>alert(133)</script>">
    """
    When I think I could escape the double quotes with the following trick
    """
    trick to escape = "> payload
    """
    And I was not able to escape from them [evidences](noescaped.png)
    """
    <input type="text" name="p1" size="60"
    value=""><script>alert(1337)</script>">
    """
    And I notice a hint with the message "UTF-7 XSS" [evidences](hint.png)
    When I realize that this encoding is old
    Then I will need to exploit the site like in the old days

  Scenario: Exploitation
    Given the idea is to run JS code on the site
    When I see the source code then I notice a hint [evidences](source.png)
    """
    <b>Hint:</b> <span id="hide">UTF-7 XSS</span>
    """
    And seeing the hint "UTF-7"
    And knowing that this encoding is not supported by the new browsers
    """
    - https://bugzilla.mozilla.org/show_bug.cgi?id=414064
    Remove support for UTF-7 (and others) per HTML5 spec
    - https://security.stackexchange.com/questions/47489/
    utf-7-xss-attacks-in-modern-browsers
    """
    When the exploitation would be by inspecting the element
    And typing the payload by console [evidences](console.png)
    """
    alert(1337);
    """
    Then in this way to exploit the XSS and recover the navigation cookies
    """
    [evidences](cookie.png)
    """

  Scenario: Remediation
    Given The idea is not to interpret user input as code
    Then I could implement a function that correctly encodes the HTML code tags
    """
    function escapeHtml(str) {
        return String(str)
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;")
            .replace(/\//g, "&#x2F;")
    }
    """
    And this allows taking the input as raw text

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      6.1/10 (Medium) - /AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      5.9/10 (Low) - /E:H/RL:T/RC:C
    Environmental:Unique and relevant attributes to a specific user environment
      5.2/10 (Low) - /IR:L/MAV:N/MAC:L

  Scenario: Correlations
    No correlations have been found to this date 2020-11-10
