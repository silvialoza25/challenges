## Version 1.4.1
## language: en

Feature:
  TOE:
    xss-quiz
  Category:
    Cross-Site Scripting
  Location:
    https://xss-quiz.int21h.jp/stage-4.php
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute Javascript code performing an XSS attack
  Recommendation:
    Encode correctly user input

  Background:
  Hacker Software:
    | Firefox            |   79.0     |
    | Windows            |   10.0     |
    | Burp Suite         |   9.2      |

  TOE information:
    Given I access the site by its URL
    Then I see an input box to find a place in a country
    And a select box to select the country [site.png)

  Scenario: Normal use case
    Given I access the site [evidences](normal.png)
    Then I can enter any place in the input box
    And select any of the four countries
    """
    Japan
    Germany
    USA
    United Kingdom
    """
    Then if it does not find the place it returns the following message:
    """
    We couldn't find any places called "any place" in "any country".
    """

  Scenario: Static detection
    Given I can't access to the backend source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the site allows user interaction
    When I try trigger the input box for any XSS bug
    """
    <script>alert(1337)</script>
    """
    And I inspect the HTML source code [evidences](dynamic.png)
    Then I don't see the text entered in the source code
    When looking at the form I notice three parameters sent by the POST method
    """
    p1, p2 & p3
    """
    And one of these, p3, is hidden [evidences](analysis.png)
    When I decide to intercept this parameter with Burp Suite
    And modify it with a HTML tag [evidences](burp.png)
    """
    <h1>hackme</h1>
    """
    When I realize that it is interpreted as code inside double quotes
    Then if I can escape the quotes I could perform an XSS

  Scenario: Exploitation
    Given the idea is to execute Javascript code
    When I use Burp Suite to modify the value of "p3" [evidences](modify.png)
    And using the POST method I send my payload
    """
    p1=hola&p2=Japan&p3="><script>alert(1337)</script>
    """
    Then this way I could execute JS code on the site [evidences](1337.png)

  Scenario: Remediation
    Given The idea is not to interpret user input as code
    Then I could implement a function that correctly encodes the HTML code tags
    """
    function htmlEntities(input) {
        return String(input).replace(/&/g, '&amp;').
               replace(/</g, '&lt;').replace(/>/g, '&gt;').
               replace(/"/g, '&quot;');
    }
    """
    And this allows taking the input as raw text

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      6.1/10 (Medium) - /AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:N/A:L
    Temporal: Attributes that measure the exploit's popularity and fixability
      5.1/10 (Low) - /E:P/RL:T/RC:U
    Environmental:Unique and relevant attributes to a specific user environment
      5.1/10 (Low) - /IR:L/MAV:N/MAC:L

  Scenario: Correlations
    No correlations have been found to this date 2020-10-22
