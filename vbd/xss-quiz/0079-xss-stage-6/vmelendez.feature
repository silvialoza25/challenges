## Version 1.4.1
## language: en

Feature:
  TOE:
    xss-quiz
  Category:
    Cross-Site Scripting
  Location:
    https://xss-quiz.int21h.jp/stage-no6.php
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute Javascript code performing an XSS attack
  Recommendation:
    Encode correctly user input

  Background:
  Hacker Software:
    | Firefox            |   79.0     |
    | Windows            |   10.0     |

  TOE information:
    Given I access the site by its URL
    Then I see an input box to search something [evidences](site.png)

  Scenario: Normal use case
    Given I access the site [evidences](normal.png)
    When I enter any string in the input box
    Then I can search that string
    And if it does not find the string it returns the following message
    """
    No results for your Query. Try again:
    """

  Scenario: Static detection
    Given I can't access to the backend source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the site allows user interaction
    When I try trigger the input box for any XSS bug
    """
    <script>alert(1337)</script>
    """
    Then I see that the input is inside double quotes [evidences](dynamic.png)
    """
    <input type="text" name="p1" size="50" value="<script>alert(133)</script>">
    """
    When I think I could escape the double quotes
    And I realize that the characters "<" and ">"
    Then are escaped correctly but the rest are not [evidences](escaped.png)
    """
    <input type="text" name="p1" size="50" value=""
    &gt;&lt;script&gt;alert(1337)&lt;=""
    script&gt;"="">
    """
    Then this can allow to execute Javascript code

  Scenario: Exploitation
    Given the idea is to escape the double quotes
    And avoid using the characters "<" and ">"
    Then with these conditions I could execute JS code with "EventHandlers"
    """
    The term event handler may be used to refer to: any function or object
    registered to be notified of events, or, more specifically, to the
    mechanism of registering event listeners via on... attributes in HTML or
    properties in web APIs, such as <button onclick="alert(this)"> or window.
    """
    Then I decided to use "onfocus=" handler and my payload is the following
    """
    1337" onfocus="alert(1337)"
    """ [evidences](1337.png)

  Scenario: Remediation
    Given The idea is not to interpret user input as code
    Then I could implement a function that correctly encodes the HTML code tags
    """
    function escapeHtml(str) {
        return String(str)
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;")
            .replace(/\//g, "&#x2F;")
    }
    """
    And this allows taking the input as raw text

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      6.1/10 (Medium) - /AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      5.9/10 (Low) - /E:H/RL:T/RC:C
    Environmental:Unique and relevant attributes to a specific user environment
      5.2/10 (Low) - /IR:L/MAV:N/MAC:L

  Scenario: Correlations
    No correlations have been found to this date 2020-10-27
