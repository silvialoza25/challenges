## Version 1.4.1
## language: en

Feature:
  TOE:
    xss-quiz
  Category:
    Cross-Site Scripting
  Location:
    https://xss-quiz.int21h.jp/stage008.php
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute Javascript code performing an XSS attack
  Recommendation:
    Encode correctly user input

  Background:
  Hacker Software:
    | Firefox            |   79.0     |
    | Windows            |   10.0     |

  TOE information:
    Given I access the site by its URL
    Then I see an input box to search something [evidences](site.png)

  Scenario: Normal use case
    Given I access the site [evidences](normal.png)
    When I enter any string in the input box
    Then I can enter an URL, example "aaa"
    And if it does not find the URL it returns the following message
    """
    Not Found

    The requested URL /aaa was not found on this server.
    """

  Scenario: Static detection
    Given I can't access to the backend source code
    Then I can't do static detection

  Scenario: Dynamic detection
    Given the site allows user interaction
    When I try trigger the input box for any XSS bug
    """
    <script>alert(1337)</script>
    """
    Then I see a 404 error [evidences](notxss.png)
    """
    Not Found

    The requested URL /<script>alert(137)</script> was not found on this server
    """
    When I think I could trigger a XSS
    And I see in the source code the following hint:
    """
    <b>Hint:</b> <span id="hide">the 'javascript' scheme.</span>
    """
    And seeing the word 'javascript' in quotes I tried the following payload
    """
    javascript:alert(1337)
    """
    Given the input is taken as a "href" of "<a />" tag
    """
    <a href="$input">$input</a>
    """
    And browsers can interpret Javascript code via links
    Then I was able to trigger the XSS with this payload [evidences](1337.png)

  Scenario: Exploitation
    Given the site is vulnerable to XSS
    When I see that there are many ways to exploit the XSS, for example
    """
    Cookie stealing
    Credential Theft
    RCE
    Data Leakage
    etc
    """
    And to test I will exploit the XSS to steal cookies from the site
    """
    <a href="javascript(document.cookie)">javascript(document.cookie)</a>
    """
    Then so I could exploit the XSS [evidences](cookie.png)

  Scenario: Remediation
    Given The idea is not to interpret user input as code
    Then I could implement a function that correctly encodes the HTML code tags
    """
    function escapeHtml(str) {
        return String(str)
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;")
            .replace(/\//g, "&#x2F;")
            .replace(/(/g,  "&#3a";")
    }
    """
    And this allows taking the input as raw text

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      6.1/10 (Medium) - /AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      5.9/10 (Low) - /E:H/RL:T/RC:C
    Environmental:Unique and relevant attributes to a specific user environment
      5.2/10 (Low) - /IR:L/MAV:N/MAC:L

  Scenario: Correlations
    No correlations have been found to this date 2020-11-06
