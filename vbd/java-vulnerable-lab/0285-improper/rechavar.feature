## Version 1.4.1
## language: en

Feature:

  TOE:
    JavaVulnerableLab
  Location:
    http://http://127.0.0.1:8080/JavaVulnerableLab/
  CWE:
    CWE-0285: Improper Authorization
  Rule:
    R141. Force re-authentication
  Goal:
    Change user email
  Recommendation:
    Add password input and validation

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Ubuntu          |     20.4    |
    | Mozilla firefox |     80.0    |
    | OWASP ZAP       |     2.9.0   |
  TOE information:
    Given I am accessing locally to the site 127.0.0.1:8080/JavaVulnerableLab
    And I enter the site
    When it shows me a message
    """
    Welcome to Java Vulnerable Lab!

    A Deliberately vulnerable Web Application built on JAVA designed to
    teach Web Application Security.
    """
    And the copyrights belong to Cyber Security & Privacy Foundation
    Then I can see that uses MySQL
    And I can conclude that query chaining is not possible

  Scenario: Normal use case
    Given I access to the site
    And I see a buttons bar with six different options
    """
    Home, Vulnerability, Forum, Login, Register, Contact
    """
    Then I place the cursor on Vulnerability and a dropdown list is displayed
    And I navigate through the option to finally end up in modify email ID
    """
    Vulnerability > A4 Insecure Direct Object References > Modify email ID
    """
    Then I can see [evidence](image1.png)
    When I try to change my email
    """
    New Email ID: testing@gmail.com
    """
    And I see [evidence](image2.png)
    Then I search click on "My Profile" button
    And I see [evidence](image3.png)
    """
    Java Vulnerable Lab

    UserName : rechavar
    Email : testing@gmail.com
    About : BigBoy

    No Card Details Found: Add Card

        Change Description
        Change Password
        Change Email
        Messages
        Send Message

    Return to Forum >>
    """

  Scenario: Static detection
    When I see the URL
    And I conclude that the page loads from change-email.jsp
    """
    http://127.0.0.1:8080/JavaVulnerableLab/vulnerability/idor/change-email.jsp
    """
    Then I go to the file
    And I can see that the vulnerability is caused from line 27 to line 36
    """
    String email=request.getParameter("email");
    String id=request.getParameter("id");
    if(email!=null && !email.equals("") && id!=null)
    {
      Statement stmt = con.createStatement();
      stmt.executeUpdate("Update users set email='"+email+"' where id="+id);
      out.print("<b class='success'>email Changed</b>");
    }
    """
    Then I conclude that the form only needs user id to change email address

  Scenario: Dynamic detection
    Given I access the site
    And I open OWASP ZAP
    Then I change my email to see what is sending
    When I open the post request I see[evidence](image4.PNG)
    And I conclude that It is vulnerable if I change the body request

  Scenario: Exploitation
    Given the change password site
    And OWASP ZAP
    Then I create a breakpoint [evidence](image5.PNG)
    When I request to change the email address
    And ZAP intercept it [evidence](image6.PNG)
    Then I change the body of the request
    """
    email=hacked%40hacked.com&id=7&change=Change to:
    email=hacked%40hacked.com&id=2&change=Change
    """
    When I use a previous vulnerability to log as victim
    And I can see that email was changed [evidence](image7.PNG)

  Scenario: Remediation
    Given I add an input field to get the user password [evidence](image8.PNG)
    """
    Enter the New Email:<br/><br/>
    <form action="change-email.jsp" method="POST">
    New Email ID: <input type="text" name="email" value=""/>
    <input type="hidden" name="id" value="<%
                                out.print(session.getAttribute("userid"));%>"/>
    <br>
    Password: <input type="text" name="password" value=""/>
    <br/><br/><input type="submit" name="change" value="Change"/>
    </form>
    <br/>

    """
    And I add a password validation at the backend code
    """
    String password = request.getParameter("password");
    String email=request.getParameter("email");
    String id=request.getParameter("id");
    Statement stmt0 = con.createStatement();
    rs=stmt0.executeQuery("select password from users where id='"+id"');
    if(rs == password){
      if(email!=null && !email.equals("") && id!=null)
      {
      Statement stmt = con.createStatement();
      stmt.executeUpdate("Update users set email='"+email+"' where id="+id);
      out.print("<b class='success'>email Changed</b>");
      }
      out.print("<br/><br/><a href='"+path+"/myprofile.jsp?id=
                              "+session.getAttribute("userid")+
                              "'>Return to Profile Page &gt;&gt;</a>");
    }
    else
    {
      out.print("Invalid password");
    }
    """
    When I apply the same payload I see [evidence](image9.PNG)
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.9/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.3/10 (High) - CR:M/IR:H/AR:X/

  Scenario: Correlations
    No correlations have been found to this date {2020-09-14}
