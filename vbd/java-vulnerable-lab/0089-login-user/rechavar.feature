## Version 1.4.1
## language: en

Feature:

  TOE:
    JavaVulnerableLab
  Location:
    http://http://127.0.0.1:8080/JavaVulnerableLab/
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in SQL command
    CWE-0287: Improper Authentication
  Rule:
    Rule.169 Use parametrized queries
    Rule.173 Discard unsafe inputs
    Rule.122 Validate credential ownership
  Goal:
    Login as different users using SQL injection vulnerability
  Recommendation:
    Sanitize SQL queries to avoid injections
    Validate location or IP address to avoid improper authentication

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Ubuntu          |     20.4    |
    | Mozilla firefox |     80.0    |
  TOE information:
    Given I am accessing locally to the site 127.0.0.1:8080/JavaVulnerableLab
    And I enter the site
    When it shows me a message
    """
    Welcome to Java Vulnerable Lab!

    A Deliberately vulnerable Web Application built on JAVA designed to
    teach Web Application Security.
    """
    And the copyrights belong to Cyber Security & Privacy Foundation
    Then I can see that uses MySQL
    And I can conclude that query chaining is not possible


  Scenario: Normal use case
    Given I access to the site
    And I see a buttons bar with six different options
    """
    Home, Vulnerability, Forum, Login, Register, Contact
    """
    Then I place the cursor on Vulnerability and a dropdown list is displayed
    Then I navigate through the options to end up in authentication bypass
    """
    Vulnerability > A1 injection > SQL injection > Authentication bypass
    """
    Then I can see [evidence](image1.png)
    And I can see the login page
    When I try to access with my username and password
    Then it displays a message[evidence](image2.png)
    """
    Hello rechavar1, Welcome to Java Vulnerable Lab!

    A Deliberately vulnerable Web Application built on JAVA
    designed to teach Web Application Security.
    """
    And the buttons bar has a new option "My Profile"
    Then I click on it
    And it displays my user information

  Scenario: Static detection
    Given I inspect the login button
    Then I can conclude that the login uses a post method
    And it calls for a controller named "LoginValidator"[evidence](image3.png)
    Then I search this controller to see how it works
    And I conclude that the problem goes from line 43 to line 52
    """
    String user=request.getParameter("username").trim();
    String pass=request.getParameter("password").trim();
      try
      {
      Connection con=new DBConnect().connect(
        getServletContext().getRealPath("/WEB-INF/config.properties"));
        if(con!=null && !con.isClosed())
                    {
                        ResultSet rs=null;
                        Statement stmt = con.createStatement();
                        rs=stmt.executeQuery("select * from users
                        where username='"+user+"' and password='"+pass+"'");
    """
    Then I can conclude that the inputs 'user' and 'pass' are not Validate
    And The query is not parametrized
    Then I conclude that is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given I access at the site
    Then I try some SQL injections additionals to my user credentials
    When I fill the form in this way:
    """
    UserName: rechavar1' AND '1' = '1
    Password: 0000
    """
    Then it throws me an error [evidence](image4.png)
    """
    Something went wrong
    """
    And I see the buttons bar
    When I see that the "My Profile" button is there [evidence](image4.png)
    Then I click on it
    And I can see my user information
    Then I realize that my login was successful
    And I try to fill the form differently:
    """
    UserName: rechavar1' AND '1' = '2
    Password: 0000
    """
    Then I see a different error message[evidence](image5.png)
    """
    Invalid UserName or Password
    """
    And I don't see the "My Profile" button
    Then I try the same injection in the Password input
    When I see the same behavior
    Then I conclude that UserName and Password are vulnerable to SQL injection

  Scenario: Exploitation
    Given the forum page
    Then I can see different users names [evidence](image6.png)
    And I open the register page
    When I see only one username input [evidence](image7.png)
    Then I conclude that posting and login use the same username
    And I use inline comments to exploit this vulnerability
    Then I try the following:
    """
    UserName: admin'; #
    Password: 0
    """
    And it shows me an error[evidence](image8.PNG)
    """
    Something went wrong
    """
    Then I see the buttons bar
    And I realize that the "My Profile" button is there
    Then I click on it
    And I can see admin's information [evidence](image9.PNG)
    Then I try with another username
    """
    UserName: neo'; #
    Password: 0
    """
    When I see the same error
    """
    Something went wrong
    """
    And I see the "My Profile" button
    Then I click on it
    And I see neo's information [evidence](image10.PNG)

  Scenario: Remediation
    Given I add a function with a regex to validate alphanumeric strings
    And it avoids SQL injection on 'user' and 'pass' variables
    """
    public static bool isValid(string text)
    {
      RegEx r = new Regex("^[A-Za-z0-9]{16}$");
      return r.isMatch(text);
    }
    """
    Then I add an if statement to validate 'user' and 'pass' variables
    """
    if(isValid(user) && isValid(pass))
    {
      ResultSet rs=null;
      Statement stmt = con.createStatement();
      rs=stmt.executeQuery("select * from users
                            where username='"+user+"'
                            and password='"+pass+"'");

      if(rs != null && rs.next()){
      HttpSession session=request.getSession();
      session.setAttribute("isLoggedIn", "1");
      session.setAttribute("userid", rs.getString("id"));
      session.setAttribute("user", rs.getString("username"));
      session.setAttribute("avatar", rs.getString("avatar"));
      Cookie privilege=new Cookie("privilege","user");
      response.addCookie(privilege);
      if(request.getParameter("RememberMe")!=null)
      {
        Cookie username=new Cookie("username",user);
        Cookie password=new Cookie("password",pass);
        response.addCookie(username);
        response.addCookie(password);
      }
      response.sendRedirect(
        response.encodeURL("ForwardMe?location=/index.jsp"));
      }
      else
      {
        response.sendRedirect(
          "ForwardMe?location=/login.jsp&err=Invalid Username or Password");
      }
    else
    {
      response.sendRedirect(
        "ForwardMe?location=/login.jsp&err=Invalid Username or Password");
    }
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.1/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.8/10 (High) - E:F/RL:O/RC:U/
  Environmental: Unique and relevant attributes to a specific user environment
    8.4/10 (High) - CR:H/IR:H/AR:M/

  Scenario: Correlations
    No correlations have been found to this date {2020-09-02}
