## Version 1.4.1
## language: en

Feature:
  TOE:
    Zero Bank
  Category:
    Information exposure
  Location:
    http://zero.webappsecurity.com/admin
  CWE:
    CWE-0200: Infomation exposure
  Rule:
    REQ.033: Restrict Administrative access
  Goal:
    Obtain the credentials of all the registered users of the site
  Recommendation:
    Restrict the access to the admin page only to the SysAdmin

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  TOE information:
    Given I am accessing the site http://zero.webappsecurity.com

  Scenario: Normal use case:
    Given I access http://zero.webappsecurity.com/
    Then I obtain the normal result from the page

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I search the url zero.webappsecurity.com
    Then I can see there are two results related to the site
    But one of them is marked as the admin page
    Then I clicked on the link marked and entered in the site
    Then I have full access to the administration site

  Scenario: Exploitation:
    Given I access http://zero.webappsecurity.com/admin/users.html
    Then I hava a total access to the users data
    And I can see the password for each one of the users
    Then I conclude that there is a total loss of confidentiality
    And it could also lead to a loss of integrity and availability
    Given we can now do what we want

  Scenario: Remediation:
    Given the deployment of the website
    Then the administration page should not be deployeed with the site
    And there should be security meassures such as a login
    And that way, avoid unauthorized visualizations

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10.0/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    9.5 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.4 (Critical) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:H/MA:H/

  Scenario: Correlations
    No correlations have been found to this date 2019-05-17
