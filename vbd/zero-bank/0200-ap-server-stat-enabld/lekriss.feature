## Version 1.4.1
## language: en

Feature:
  TOE:
    Zero Bank
  Category:
    Information exposure
  Location:
    http://zero.webappsecurity.com/server-status
  CWE:
    CWE-0200: Infomation exposure
  Rule:
    REQ.033: Restrict Administrative access
  Goal:
    Obrain information of the server trough its status screen
  Recommendation:
    Disable the server_status option on apache or restrict the
    users which can access logs

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  TOE information:
    Given I am accessing the site http://zero.webappsecurity.com

  Scenario: Normal use case:
    Given I access http://zero.webappsecurity.com/
    And I write search.php in url bar
    Then I can see this http://zero.webappsecurity.com/search.php
    And the normal results page of the web site

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I access http://zero.webappsecurity.com/
    Then I can write a no-sense value at the end of the url
    And I get an error message which shows me the server being used
    Then I can see that the server running is Apache
    Then I researched a little about that server
    And I found that sometimes the server-status section is not restricted
    Then I writed server-status at the end of the url like this
    """
    http://zero.webappsecurity.com/server-status
    """
    Then I got full access to the server data

  Scenario: Exploitation:
    Given I access http://zero.webappsecurity.com/server-status
    Then I can look for some usefull information
    And I can see the SO running the server is a WIN32
    Then I conclude that is possible to refine some other attacks
    Given we now know what SO we are facing and the server version

  Scenario: Remediation:
    When the server is delopyed
    Then the SysAdmin should restrict which users can see the server-status
    And that way avoid unathorized visualizations

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.1 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.1 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U

  Scenario: Correlations
    No correlations have been found to this date 2019-05-17
