## Version 1.4.1
## language: en
Feature:
  TOE:
    Metasploitable3
  Location:
    192.168.199.131:8080 TCP
  CWE:
    CWE-0937: Using Components with Known Vulnerabilities
  Rule:
    REQ.262: Verify third-party components
  Goal:
    Get remote shell on the machine
  Recommendation:
    Update software when critical fixes are available

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-linux      | 2020.4      |
    | VMware          | 16.1.0      |
    | Nmap            | 7.70        |
    | Metasploit      | 5.0.4       |
  TOE information:
    Given I am running Metasploitable 3 ubuntu on VMware Workstation 15
    And I am using kali-linux

  Scenario: Normal use case
    Given I am the administrator of the continuum service
    When I access the port 8080 with my credentials
    Then I am able to configure continuum

  Scenario: Static detection
    Given I don't have access to the source code
    Then This can't be done

  Scenario: Dynamic detection
    Given I know the machine has some open services
    When I scan with Nmap for open ports
    """
    $ nmap -A -T4 -p- 192.168.199.131
    8080/tcp  open  http   Jetty 8.1.7.v20120910
    _http-server-header: Jetty(8.1.7.v20120910)
    _http-title: Error 404 - Not Found
    """
    Then I search for 'Jetty 8.1' on Google
    And I found the page for CVE-2019-10247
    """
    https://www.cvedetails.com/cve/CVE-2019-10247/
    """
    But The vulnerability does not grant access
    When I check the website on port 8080 [evidence](login.png)
    And I open the link with the continuum logo
    Then I can see the actual version <<1.4.2>>
    And A message from Apache
    """
    2016/05/18 - Apache Continuum has been retired
    """
    When I search for 'continuum 1.4.2'
    And I found an exploit that allows the injection of commands
    Then I can conclude that anyone can access remotely

  Scenario: Exploitation
    Given The port 8080 is open and running an outdated service
    When I open msfconsole
    And I search for 'continuum'
    Then I use
    """
    exploit/linux/http/apache_continuum_cmd_exec
    """
    And I set the RHOST
    When I run the exploit
    Then I got a session with access to the service [evidence](shell.png)

  Scenario: Remediation
    Given Apache continuum has been retired
    And The latest release <<1.4.2>> is vulnerable
    Then The service should not be used

  Scenario: Scoring
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 (High) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.3/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2021-03-30
