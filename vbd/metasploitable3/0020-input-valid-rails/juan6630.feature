## Version 1.4.1
## language: en
Feature:
  TOE:
    Metasploitable3
  Location:
    192.168.199.131:3500 TCP
  CWE:
    CWE-0020: Improper Input Validation
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Get remote shell on the machine
  Recommendation:
    Upgrade the Rails packages

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-linux      | 2020.4      |
    | VMware          | 16.1.0      |
    | Nmap            | 7.70        |
    | Metasploit      | 5.0.4       |
  TOE information:
    Given I am running Metasploitable 3 ubuntu on VMware Workstation 15
    And I am using kali-linux

  Scenario: Normal use case
    When I access the port 3500 on my browser
    Then I see the default configuration page for Rails

  Scenario: Static detection
    Given I don't have access to the source code
    Then This can't be done

  Scenario: Dynamic detection
    Given I know the machine have some open services
    When I scan with Nmap for open ports
      """
      $ nmap -A -T4 -p- 192.168.199.131
      3500/tcp  open  http   WEBrick http 1.3.1 (Ruby 2.3.8(2018-10-18))
      _/
      _http-server-header: WEBrick/1.3.1 (Ruby/2.3.8/2018-10-18)
      _http-title: Ruby on Rails: Welcome aboard
      """
    Then I check the website on port 3500
    And I find extra details of the version running [evidence](rails.png)
    When I change the url to <<192.168.199.131:3500/config>>
    And I get an error with the Ruby on Rails path [evidence](path.png)
    Then I conclude that anyone can get access remotely

  Scenario: Exploitation
    Given The port 3500 is open
    When I open msfconsole
    And I search for 'rails'
    Then I decide to use
    """
    exploit/multi/http/rails_double_tap
    """
    And I select the target <<Ruby on Rails 5.2 or prior>>
    When I run the exploit
    Then I get an error [evidence](fail.png)
    When I decide to use
    """
    exploit/multi/http/rails_actionpack_inline_exec
    """
    And I run the exploit
    Then I got a session with access to the service [evidence](shell.png)

  Scenario: Remediation
    Given The compromised version of Rails running on the machine
    And The official fix in versions 4.2.5.2 and 3.2.22.2
    Then To fix it, an updated version must be downloaded

  Scenario: Scoring
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.1/10 (High) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2021-03-29
