## Version 1.4.1
## language: en
Feature:
  TOE:
    Metasploitable3
  Location:
    192.168.199.131:80 TCP
  CWE:
    CWE-0078: OS Command Injection
  Rule:
    REQ.262: Verify third-party components
  Goal:
    Get remote shell on the machine
  Recommendation:
    Limit the writing access to CGI directories to trusted users

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-linux      | 2020.4      |
    | VMware          | 16.1.0      |
    | Nmap            | 7.70        |
    | Metasploit      | 5.0.4       |
  TOE information:
    Given I am running Metasploitable 3 ubuntu on VMware Workstation 15
    And I am using kali-linux

  Scenario: Normal use case
    Given Apache 2.4.7 is running on the machine
    When An user connects with the machine
    Then The server delivers web content

  Scenario: Static detection
    Given I don't have access to the source code
    Then This can't be done

  Scenario: Dynamic detection
    Given I know the machine has some open services
    When I scan with Nmap for open ports
    """
    $ nmap -A -T4 -p- 192.168.199.131
    PORT    STATE SERVICE VERSION
    80/tcp  open  http    Apache http 2.4.7
    | http-ls: Volume /
    | SIZE  TIME              FILENAME
    | -     2020-10-29 19:37  chat/
    | -     2011-07-27 20:17  drupal/
    | 1.7k  2020-10-29 19:37  payroll_app.php
    | -     2013-04-06 12:06  phpmyadmin/
    | 83    2021-04-06 14:20  vwAxRY.php
    |_
    |_http-server-header: Apache/2.4.7 (Ubuntu)
    |_http-title: Index of /
    """
    And I go to the website on port 80 [evidence](index.png)
    Then I search for 'Apache 2.4.7' on Google
    And I found the page for CVE-2014-6271
    """
    https://www.cvedetails.com/cve/CVE-2014-6271/
    """
    When I see that there are 10 Metasploit modules available
    Then I can conclude that anyone can access remotely

  Scenario: Exploitation
    Given The port 80 is open and running an Apache server
    When I open msfconsole
    And I search for 'cgi_bash'
    Then I use
    """
    exploit/multi/http/apache_mod_cgi_bash_env_exec
    """
    When I start setting up the options [evidence](options)
    And I need the <<Path to CGI script>>
    Then I search for it on Google
    And I use <</cgi-bin/>> based on apache documentation
    """
    https://httpd.apache.org/docs/2.4/howto/cgi.html
    """
    And I check the existence of the folder with Firefox [evidence](cgi.png)
    When I run the script
    Then I got a session with access to the service [evidence](shell.png)

  Scenario: Remediation
    Given The Apache web server uses CGI to handle document requests
    And The Apache configuration is vulnerable to Shellshock attacks
    Then To fix it, implement an "accept known good" input validation strategy
    And Ensure only trusted users can write on CGI directories

  Scenario: Scoring
  Base: Attributes that are constants over time and organizations
    9.8/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.1/10 (Critical) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.1/10 (Critical) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2021-04-07
