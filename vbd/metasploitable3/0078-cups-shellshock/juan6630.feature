## Version 1.4.1
## language: en
Feature:
  TOE:
    Metasploitable3
  Location:
    192.168.199.131:80 TCP
  CWE:
    CWE-0078: OS Command Injection
  Rule:
    REQ.262: Verify third-party components
  Goal:
    Get remote shell on the machine
  Recommendation:
    Install update from vendor's website

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-linux      | 2020.4      |
    | VMware          | 16.1.0      |
    | Nmap            | 7.70        |
    | Metasploit      | 5.0.4       |
  TOE information:
    Given I am running Metasploitable 3 ubuntu on VMware Workstation 15
    And I am using kali-linux

  Scenario: Normal use case
    Given CUPS 1.7.2 is running on the machine
    When The administrator of the service add a printer
    Then Any authorized user can print from any application

  Scenario: Static detection
    Given I don't have access to the source code
    Then This can't be done

  Scenario: Dynamic detection
    Given I know the machine has some open services
    When I scan with Nmap for open ports
    """
    $ nmap -A -vv -T4 -p- 192.168.199.131
    PORT    STATE SERVICE REASON          VERSION
    631/tcp  open  ipp    syn-ack ttl 64  CUPS 1.7
    | http-robots.txt: 1 disallowed entry
    |_/
    |_http-server-header: CUPS/1.7 IPP/2.1
    |_http-title: Home - CUPS 1.7.2
    """
    And I search for scripts on Nmap [evidence](nmap.png)
    """
    ls -l /usr/share/nmap/scripts/*cups*
    """
    But None of them work for me
    When I go to the website on port 631 [evidence](page.png)
    Then I search for 'CUPS 1.7.2' on Google
    And I found the page for the cybersecurity-help report
    """
    https://www.cybersecurity-help.cz/vdb/SB2014041714
    """
    And The CVE ID
    """
    CVE-2014-2707
    """
    When I see that there are 10 Metasploit modules available
    And I see one module about CUPS filter bash
    Then I can conclude that anyone can access remotely

  Scenario: Exploitation
    Given The port 631 is open and running an IPP service
    When I open msfconsole
    And I search for 'cups' [evidence](metasploit.png)
    Then I use
    """
    exploit/multi/http/cups_bash_env_exec
    """
    When I start setting up the options
    And I run the script
    Then I got a session [evidence](shell.png)

  Scenario: Remediation
    Given The machine is running an outdated version of CUPS
    Then To fix it, update to the version 1.8.3-2ubuntu3.5 or higher

  Scenario: Scoring
  Base: Attributes that are constants over time and organizations
    9.8/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.1/10 (Critical) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.1/10 (Critical) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2021-04-09
