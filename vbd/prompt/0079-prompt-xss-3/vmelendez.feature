## Version 1.4.1
## language: en

Feature:
  TOE:
    Prompt
  Category:
    Cross-Site Scripting
  Location:
    http://prompt.ml/3
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute javascript code performing an XSS attack
  Recommendation:
    Encode correctly user input

  Background:
  Hacker Software:
    | Firefox            |   79.0     |
    | Windows            |   10.0     |

  TOE information:
    Given I access the site by its URL
    Then I see a sandbox environment to interact with the site
    And two sections to see the site's responses [evidences](site.png)

  Scenario: Normal use case
    Given I access the site
    Then I can enter something in the input box
    And it will be reflected in the sandbox web viewer [evidences](normal.png)

  Scenario: Static detection
    Given the site provides an area called "Text Viewer"
    And which is basically the source code of the relevant function
    Then I could analyze this function
    """
    function escape(input) {
        // filter potential comment end delimiters
        input = input.replace(/->/g, '_');

        // comment the input to avoid script execution
        return '<!-- ' + input + ' -->';
    }
    """
    And it's simple takes an input as an argument
    Then replace the character sequence "->" with "_"
    And comment the input to avoid script execution
    Then it is possible to execute javascript code
    When the input is not interpreted as a comment

  Scenario: Dynamic detection
    Given the site operates within a sandbox
    And I can see the behavior of the site
    Then I can test payload to determine bugs on the site
    When I enter the following script in it
    """
    aaaaa <script>alert(1)</script>
    """
    Then I can see that the site interprets it as javascript code but
    And this JS code it is inside a comment
    Then I should find a way to get this script out of there

  Scenario: Exploitation
    Given the idea is to execute javascript code
    Then I should find a way to end the comment before the input
    And without using "->" that is, if I enter something like this as input:
    """
    --> my input
    """
    Then the function would interpret the input as
    """
    <!-- -_ my input -->
    """
    And since the idea is to finish the initial comment "<!--"
    Then I could bypass the filter ending the comment with the following string
    """
    --!> : another way to close comments
    """
    Then it is to enter the payload to execute the alert
    """
    --!> <script>alert(1337)</script>
    """
    And I manage to execute javascript code arbitrarily [evidences](1337.png)

  Scenario: Remediation
    Given The idea is not to interpret user input as code
    Then I could implement a function that correctly encodes the HTML code tags
    """
    function htmlEntities(input) {
        return String(input).replace(/&/g, '&amp;').
               replace(/</g, '&lt;').replace(/>/g, '&gt;').
               replace(/"/g, '&quot;');
    }
    """
    And this allows taking the input as raw text

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      4.3/10 (Medium) - /AV:A/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      3.8/10 (Low) - E:P/RL:U/RC:U
    Environmental:Unique and relevant attributes to a specific user environment
      3.2/10 (Low) - IR:L/MAV:A/MAC:L

  Scenario: Correlations
    No correlations have been found to this date 2020-09-28
