## Version 1.4.1
## language: en

Feature:
  TOE:
    Prompt
  Category:
    Cross-Site Scripting
  Location:
    http://prompt.ml/7
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute Javascript code performing an XSS attack
  Recommendation:
    Encode correctly user input

  Background:
  Hacker Software:
    | Firefox            |   79.0     |
    | Windows            |   10.0     |

  TOE information:
    Given I access the site by its URL
    Then I see a sandbox environment to interact with the site
    And two sections to see the site's responses [evidences](site.png)

  Scenario: Normal use case
    Given I access the site
    Then I can enter something in the input box
    And it will be reflected in the sandbox web viewer [evidences](normal.png)

  Scenario: Static detection
    Given the site provides an area called "Text Viewer"
    And which is basically the source code of the relevant function
    Then I could analyze this function
    """
    function escape(input) {
      // pass in something like dog#cat#bird#mouse...
      var segments = input.split('#');
      return segments.map(function(title) {
        // title can only contain 12 characters
        return '<p class="comment" title="' + title.slice(0, 12) + '"></p>';
      }).join('\n');
    }
    """
    And it's simple takes an input as argument
    Then split the titles with the "#" character
    And the title must have a maximum of 12 characters
    Then it is possible to execute javascript code
    When the split is interpreted as a comment or as a constant string

  Scenario: Dynamic detection
    Given the site operates within a sandbox
    And I can see the behavior of the site
    Then I can test payload to determine bugs on the site
    When I enter the following script in it [evidences](char12.png)
    """
    <script>alert(1)</script>
    """
    Then I can see that the site can interprets it as Javascript code but
    And this JS code it is inside a "tittle=" value
    Then I should find a way to get this script out of there
    And bypass the characters restriction

  Scenario: Exploitation
    Given the idea is to execute Javascript code
    Then I should find a way to escape of the title value
    And this is possible by closing the tags [evidences](closetag.png)
    """
    <p class="comment" title="">"></p>
    """
    Then in this same line I initialize my payload
    And I do split for the next fragment of the payload ("#")
    """
    <p class="comment" title=""><img src=#"></p>
    """
    Then the code would be this way
    """
    <p class="comment" title=""><img src="></p>
    <p class="comment" title=""></p>
    """
    And this indicates that I should close the double quote of
    """
    src="...
    """
    Then I would have control of the attributes of the img tag
    And the payload would be like this: [evidences](initcontrol.png)
    """
    "><img src=#"
    """
    Then the most important is missing (the Javascript code)
    And for this I split again and I comment the spliterated value
    Then the complete payload would look like this:
    """
    "><img src=#"onerror='/*#*/alert("")'/>
    """ [evidences](1337.png)

  Scenario: Remediation
    Given The idea is not to interpret user input as code
    Then I could implement a function that correctly encodes the HTML code tags
    """
    function htmlEntities(input) {
        return String(input).replace(/&/g, '&amp;').
               replace(/</g, '&lt;').replace(/>/g, '&gt;').
               replace(/"/g, '&quot;');
    }
    """
    And this allows taking the input as raw text

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      4.3/10 (Medium) - /AV:A/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      3.8/10 (Low) - E:P/RL:U/RC:U
    Environmental:Unique and relevant attributes to a specific user environment
      3.2/10 (Low) - IR:L/MAV:A/MAC:L

  Scenario: Correlations
    No correlations have been found to this date 2020-10-06
