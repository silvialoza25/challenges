## Version 1.4.1
## language: en

Feature:
  TOE:
    Prompt
  Category:
    Cross-Site Scripting
  Location:
    http://prompt.ml/1
  CWE:
    CWE-079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute javascript code performing an XSS attack
  Recommendation:
    Encode correctly user input

  Background:
  Hacker Software:
    | Firefox            |   79.0     |
    | Windows            |   10.0     |

  TOE information:
    Given I access the site by its URL
    Then I see a sandbox environment to interact with the site
    And two sections to see the site's responses [evidences](site.png)

  Scenario: Normal use case
    Given I access the site
    Then I can enter something in the input box
    And it will be reflected in the sandbox web viewer [evidences](normal.png)

  Scenario: Static detection
    Given the site provides an area called "Text Viewer"
    And which is basically the source code of the relevant function
    Then I could analyze this function
    """
    function escape(input) {
        // tags stripping mechanism from ExtJS library
        // Ext.util.Format.stripTags
        var stripTagsRE = /<\/?[^>]+>/gi;
        input = input.replace(stripTagsRE, '');

        return '<article>' + input + '</article>';
    }
    """
    And it's simple takes an input as an argument and using regular expressions
    Then will replace everything inside the HTML tags "<",">"
    And finally it will show the input inside the "<article />" tag
    And if there is any way to bypass the regex filter
    Then Javascript code could be executed

  Scenario: Dynamic detection
    Given the site operates within a sandbox
    Then I can't do dynamic detection

  Scenario: Exploitation
    Given the site may be vulnerable to XSS if I can bypass the regex filter
    Then the idea is to try to bypass it
    And execute a "prompt(1);"
    Then the first thing is to see how the regular expression works
    And to understand it better I use a cool page for regex
    """
    https://regex101.com/
    """
    Then I can parse each component of the expression
    """
    var stripTagsRE = /<\/?[^>]+>/gi;

    <    : matches the character < literally (case insensitive)
    \/   : matches the character / literally (case insensitive)
    ?    : Quantifier — Matches between zero and one times, as many
           times as possible, giving back as needed
    [^>] : Match a single character not present in the set
    +    : Quantifier — Matches between one and unlimited times, as many
           times as possible, giving back as needed

    Note: \ is used to escape the legitimate regex instruction "/"
          to take it literally
    """
    Then I see that it just takes into account the characters inside "<>"
    And except ">" [evidences](testre.png) and to bypass this
    Then I will need a tag that allows me to execute javascript code
    And without the need to close the tag ">", example:
    """
    <BODY BACKGROUND="javascript:alert('XSS')
    <IMG DYNSRC="javascript:alert('XSS')">
    <IMG SRC=X ONERROR=PROMPT(1)
    <svg/onload=alert('XSS')
    """
    Then I use the escape character "\\"
    And the payload it would be this way [evidences](won.png)
    """
    <img src=x onerror=prompt(1) \
    """

  Scenario: Remediation
    Given The idea is not to interpret user input as code
    Then I could implement a function that correctly encodes the HTML code tags
    """
    function htmlEntities(input) {
        return String(input).replace(/&/g, '&amp;').
               replace(/</g, '&lt;').replace(/>/g, '&gt;').
               replace(/"/g, '&quot;');
    }
    """
    And this allows to show to display the input without
    Then the browser reading it as HTML

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      4.3/10 (Medium) - /AV:A/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      3.8/10 (Low) - E:P/RL:U/RC:U
    Environmental:Unique and relevant attributes to a specific user environment
      3.2/10 (Low) - IR:L/MAV:A/MAC:L

  Scenario: Correlations
    No correlations have been found to this date 2020-09-25
