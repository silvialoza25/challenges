## Version 1.4.2
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Insecure Communication
  Location:
    WebGoat/ - Secure Actions
  CWE:
    CWE-287: Improper Authentication
  Rule:
    REQ.237: https://fluidattacks.com/web/es/rules/237/
  Goal:
    Bruteforce authentication
  Recommendation:
    Implement a captcha

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Wireshark             | 2.6.5     |
  TOE information:
    Given I am running Webgoat in a docker container at
    """
    http://localhost:8000/WebGoat/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/WebGoat
    Then I can navigate the site

  Scenario: Static detection
  No static detection

  Scenario: Dynamic detection
  Bruteforcing login
    Given I go to http://localhost:8000/WebGoat
    And try to login many times with different credentials
    Then the application doesn't do anything to try and stop me

  Scenario: Exploitation
  Bruteforcing login
    Given I have a combolist for users of the application
    Then I can try credentials successively until I get one right

  Scenario: Remediation
  Implement captcha
    Given I put a captcha after 3 failed login attempts
    And before registering
    And before changing the password
    Then I comply with this rule

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.5/10 (Low) - AV:P/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.3/10 (Low) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.3/10 (Low) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-23
