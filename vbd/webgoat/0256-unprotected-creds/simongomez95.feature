## Version 1.4.2
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Insecure Communication
  Location:
    WebGoat/register - Password Storage
  CWE:
    CWE-256: Unprotected Storage of Credentials
  Rule:
    REQ.127: https://fluidattacks.com/web/en/rules/127/
  Goal:
    Dump password
  Recommendation:
    Always hash passwords in the db

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Wireshark             | 2.6.5     |
  TOE information:
    Given I am running WebGoat in a docker container at
    """
    http://localhost:8000/WebGoat/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/WebGoat
    Then I can navigate the site

  Scenario: Static detection
  No precedent password validation
    Given I see the code at "owasp/webwolf/user/WebGoatUser.java"
    """
    ...
    42  public void createUser() {
    43      this.user = new User(username, password, getAuthorities());
    44  }
    ...
    """
    Then I see it stores passwords in plaintext

  Scenario: Dynamic detection
  No dynamic detection

  Scenario: Exploitation
  Finding plaintext passwords
    Given I find a way to access the application database
    Then I can query the Users table
    And obtain the usernames and passwords of the users

  Scenario: Remediation
  Use a hashing function before storing password
    Given I hash the password before storing it
    Then it isnt stored in plaintext anymore
    Then the vulnerability is fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.8/10 (Medium) - AV:A/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.3/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-22
