Name:   Hackademic Challenges Project
Tech:   PHP
Site:   https://www.owasp.org/index.php/OWASP_Hackademic_Challenges_Project
Repo:   https://github.com/Hackademic/hackademic/
Author: OWASP
From:   https://www.owasp.org/index.php/OWASP_Vulnerable_Web_Applications_Directory_Project#tab=Off-Line_apps