## Version 1.4.1
## language: en

Feature:
  TOE:
    OWASP Security Shepherd
  Category:
    Insecure Direct Object Reference
  Location:
    https://localhost/ - IDOR 1
  CWE:
    CWE-0639: Authorization Bypass Through User-Controlled Key
  Rule:
    REQ.181: Transmit data using secure protocols
  Goal:
    Intercept and edit request to get hidden message
  Recommendation:
    Use secure data transmision protocols, validate inputs

  Background:
  Hacker's software:
    | Ubuntu          | 18.04.2 LTS |
    | Mozilla Firefox | 67.0        |
    | Burp Suite      | 1.7.36      |
  TOE information:
    Given I am accessing the site https://localhost/
    And entered one of its pages which has users messages

  Scenario: Normal use case
    When I access https://localhost/
    Then I can see the names of several users
    And I can select among these usernames to see a message

  Scenario: Static detection
    When I inspect the page looking for the source code
    Then I recognize the html code that handles user IDs
    """
    27 <select id='userId' style='width: 300px;' multiple>
    28     <option value="1">Paul Bourke</option>
    29     <option value="3">Will Bailey</option>
    30     <option value="5">Orla Cleary</option>
    31     <option value="7">Ronan Fitzpatrick</option>
    32     <option value="9">Pat McKenana</option>
    33 </select>
    """
    And also the related javascript code
    """
    49 var optionValue = $("#userId").val();
    54 data: {
    55        userId: optionValue
    56       },
    """
    Then I can conclude that the selected input needs contextual validation

  Scenario: Dynamic detection
    When I access https://localhost/
    Then I can choose any user
    And click on "Show this Profile"
    Then I check the page request with the Burp Suite program
    And I can see a value named "userId%5B%5D"
    Then I try the different users to analize the value
    And I also try editing the value
    And I see that I can manipulate the displayed user

  Scenario: Exploitation
    When I access https://localhost/
    Then I choose any user
    And click on "Show this Profile"
    Then I check the page request with the Burp Suite program
    And I can see a value named "userId%5B%5D"
    Then I edit the value with the number "11" using Burp Suite
    And I get a message from a user I should not be able to access

  Scenario: Remediation
    When the website is in its development stage
    Then developers should write code validating input like this
    """
    function checkinput(){
        if (optionValue !== (range based on context)) {
            alert('Invalid input');
            return false;
        }
    }
    """
    And also use better data transmision protocols for the website
    And this way the exploit can be prevented

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.9/10 (Medium) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.9/10 (Medium) - CR:H/MAC:L/MUI:N/MC:L

  Scenario: Correlations
    No correlations have been found to this date 2019/06/06
