## Version 2.0.1
## language: en

Feature:
  TOE:
    securityshepherd
  Category:
    Security misconfiguration
  Location:
    http://localhost/
    4d8d50a458ca5f1f7e2506dd5557ae1f7da21282795d0ed86c55fefe41eb874f.jsp
  CWE:
    CWE-016: Configuration
  Rule:
    REQ.142 Change system default credentials
  Goal:
    Avoid the default credentials
  Recommendation:
    Change default credentials like usernames and password

 Background:
 Hacker's software:
   | <software name>      | <version>      |
   | Microsoft Windows 10 | 10.0.17763.437 |
   | Docker               | 2.0.0.3        |
 TOE information:
   Given I'm entering the site https://localhost/
   And I access a site which has been build in java and mysql

 Scenario: Normal use case
   Given I enter https://localhost/
   Then the browser redirect me to https://localhost/login.jsp
   Then I enter the user credentials given by OWASP Security Shepherd
   """
   https://github.com/OWASP/SecurityShepherd
   """
   Then I go to the site and click on the link Get the next Challenge

 Scenario: Static detection
   Given I enter https://localhost/login.jsp
   And I click to the next challenge link
   Then I saw that the site is build on java and I have access
   And I just look for webapp directory where tomcat deploy the apps
   Then I find ROOT.war and unzip it
   And I see the folder
   Then I saw link with the id of challenge
   """
   fe04648f43cdf2d523ecf1675f1ade2cde04a7a2e9a7f1a80dbb6dc9f717c833.jsp
   """
   Then I realise that challenge is based on security misconfiguration
   And I stop right there because there's no need to inspect code

 Scenario: Dynamic detection
   Given I enter https://localhost/login.jsp
   And I click the get the next challenge link
   Then I saw the description
   """
   Security misconfiguration can happen in any part of an application,
   from the database server, third-party libraries to custom code settings.
   A security misconfiguration is any configuration which can be exploited
   by an attacker to perform any action they should not be able to.
   The impact of these issues vary from which configuration is being exploited.
   """
   Then I saw two inputs one with username and other with the password and
   Then I must enter the admin credentials of the site
   And these credentials are related with the security misconfiguration
   Then there is a sign in button and result key input

 Scenario: Exploitation
   Given the challenge link
   Then we enter admin keys that were provided by
   """
   https://github.com/OWASP/SecurityShepherd
   """
   Then the site gives us the keys to capture the flag
   And this way a system misconfiguration can lead to gain admin access

 Scenario: Remediation
   When page has been developed
   Then we should change the default usernames and passwords
   Then it would be more dificult to the attacker to guess a default username

 Scenario: Scoring
 Severity scoring according to CVSSv3 standard
 Base: Attributes that are constants over time and organizations
   9.8/10 (Low) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
 Temporal: Attributes that measure the exploit's popularity and fixability
   9.8/10 (Low) - E:F/RL:O/RC:R
 Environmental: Unique and relevant attributes to a specific user environment
   9.8/10 (Medium) - CR:L/IR:L/AR:L

 Scenario: Correlations
   No correlations have been discovered to this date 2019-05-17