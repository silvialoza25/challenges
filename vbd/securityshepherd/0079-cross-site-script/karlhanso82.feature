## Version 2.0.1
## language: en

Feature:
  TOE:
    securityshepherd
  Category:
    untrusted data
  Location:
    https://localhost/
    zf8ed52591579339e590e0726c7b24009f3ac54cdff1b81a65db1688d86efb3a.jsp
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Exploit the xss vulnerability to gain the key
  Recommendation:
    Use a library to avoid this kind of harmful inputs

 Background:
 Hacker's software:
   | <software name>      | <version>      |
   | Microsoft Windows 10 | 10.0.17763.437 |
   | Docker               | 2.0.0.3        |
 TOE information:
   Given I'm entering the site https://localhost/
   And I access a site which has been build in java and mysql

 Scenario: Normal use case
   Given I enter https://localhost/
   Then the browser redirect me to https://localhost/login.jsp
   Then I enter the user credentials given by OWASP Security Shepherd
   """
   https://github.com/OWASP/SecurityShepherd
   """
   Then I go to the site and click on the link Get the next Challenge
   And I saw an input box with the name search term
   And a button with the name Get this user
   And input box for submitting the key of the challenge

Scenario: Static detection
   Given I enter https://localhost/login.jsp
   And I click to the next challenge link
   Then I saw that the site is build on java and I have access
   And I just look for webapp directory where tomcat deploy the apps
   Then I find ROOT.war and unzip it
   And I see the folder
   Then I saw link with the id of challenge
   """
   https://localhost/
   zf8ed52591579339e590e0726c7b24009f3ac54cdff1b81a65db1688d86efb3a.jsp
   """
   Then I saw the code that triggers the post request
   """
   118        var ajaxCall = $.ajax({
   119     type: "POST",
   120 url:"zf8ed52591579339e590e0726c7b24009f3ac54cdff1b81a65db1688d86efb3a",
   121       data: {
   122        searchTerm: theSearchTerm,
   123        csrfToken: "<%= csrfToken %>"
   124        },
   125   async: false
   126 });

   """
   Then with these clues I decided to look for the backend code
   """
   String searchTerm = request.getParameter("searchTerm");
          log.debug("User Submitted - " + searchTerm);
          String htmlOutput = new String();
          if(FindXSS.search(searchTerm))
          {
            log.debug("XSS Lesson Completed!");
            htmlOutput = "<h2 class='title'>" +
            bundle.getString("result.wellDone") + "</h2>" +
                "<p>" + bundle.getString("result.youDidIt") + "<br />" +
                "" + bundle.getString("result.resultKey") +
        Hash.generateUserSolution(
        Getter.getModuleResultFromHash(
        getServletContext().getRealPath(""), levelHash),
                 (String)ses.getAttribute("userName"));
          }
   """
   Then I spot that searchterm is not x

Scenario: Dynamic detection
   Given I enter https://localhost/login.jsp
   And I click the get the next challenge link
   Then I saw the description
   """
   Cross-Site Scripting, or XSS, issues occur when an application
   uses untrusted data in a web browser without sufficient
   validation or escaping.If untrusted data contains
   a client side script,the browser will execute
   the script while it is interpreting the page.
   """
   Then I saw an input box with the name Search term
   And the button get this user
   And I look the input if handles some validation
   Then I see the parameter is not clean from the front-end in the form-data
   """
   searchTerm: <SCRIPT></SCRIPT>
   csrfToken: 40866380018152044257598999135944076032
   """
   And it concludes the detection

Scenario: Exploitation
   Given the challenge link
   And I send a xss code to the searchterm input
   """
   <script>alert(1)</script>
   """
   Then it displays the popup
   And later appears the  result key of the challenge
   And these kind of flaws leads to session hijacking
   And also leads to web defacement

Scenario: Remediation
   When the page has been developed
   Then it should escape these special characters
   And the injection attack tries to change the meaning of a command
   And another way to prevent the attack is use a framework
   And another way is Java Encoder Project from OWASP like the code below
   """
   import org.owasp.encoder.Encode;
   String searchTerm = Encode.forHtml(request.getParameter("searchTerm"));
          log.debug("User Submitted - " + searchTerm);
          String htmlOutput = new String();
          if(FindXSS.search(searchTerm))
          {
            log.debug("XSS Lesson Completed!");
            htmlOutput = "<h2 class='title'>" +
            bundle.getString("result.wellDone") + "</h2>" +
                "<p>" + bundle.getString("result.youDidIt") + "<br />" +
                "" + bundle.getString("result.resultKey") +
          Hash.generateUserSolution(
          Getter.getModuleResultFromHash(
          getServletContext().getRealPath(""), levelHash),
                 (String)ses.getAttribute("userName"));
          }
   """

   Then this is going to prevent render malicious code at the backend

Scenario: Scoring
 Severity scoring according to CVSSv3 standard
 Base: Attributes that are constants over time and organizations
   6.1/10 (Low) - AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N
 Temporal: Attributes that measure the exploit's popularity and fixability
   6.1/10 (Low) - E:F/RL:O/RC:R
 Environmental: Unique and relevant attributes to a specific user environment
   6.1/10 (Medium) - CR:L/IR:L/AR:L

 Scenario: Correlations
   No correlations have been discovered to this date 2019-05-23