## Version 1.4.1
## language: en

Feature:
  TOE:
    web-scanner-test-site
  Category:
    SQL Injection
  Location:
    http://webscantest.com/datastore/getimage_by_id.php - id (field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Exploit SQL Injection vulnerability
  Recommendation:
    Sanitize inputs
    Use of Prepared Statements
    Use of Stored Procedures
    Escaping all User Supplied Input

  Background:
  Hacker's software:
    | Name          | Version       |
    | Ubuntu        | 16.04         |
    | Firefox       | 72.0.02       |
  TOE information:
    Given I am accesing the webpage
    """
    http://webscantest.com/datastore/getimage_by_id.php
    """
    And is runing on Ubuntu 14.04.2 LTS
    And is running Apache/2.4.7 (Ubuntu)
    And is powered by PHP/5.5.9

  Scenario: Normal use case
  Users can use the webapp to search images using an id
    When I input a tool id
    Then I get the respective tool image

  Scenario: Static detection
  No code given

  Scenario: Dynamic detection
    When I access http://192.168.100.36/sqli/example6.php?id=2
    Then I begin testing the url input "id" field checking for errors
    And I see that the parameter "id=1 AND 1=2" return error
    """
    Invalid Product
    """
    And Based on this result I conclude there exists a SQLi
    And I can conclude that the service does not sanitize inputs

  Scenario: Exploitation
    When I access to Normal use case
    Then I test the type of injection by injecting a boolean expression
    """
    id=2 and 1=1
    """
    And the query isn't generating errors
    And injection should work on this website
    When I try to get the number of columns from the query
    Then I inject an "order by" clause on the query using 3 as the parameter
    """
    id=1 OR 1=1 order by 3
    """
    And Query isn't getting any error
    When I inject an "order by" clause using 5 as the parameter
    Then I get an error:
    """
    Invalid Product
    Error 1054: Unknown column '5' in 'order clause' of
    SELECT * FROM inventory WHERE id = 1 AND 1=1 ORDER BY 5
    """
    Then I found out that the query table has 4 columns
    But none of them are visible
    And the website is vulnerable to SQL injection

  Scenario: Remediation
    Given The site is susceptible to SQL injection attacks
    When I make use of Prepared Statements or Stored Procedures
    Then the webpage can prevent SQL Injection

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.1/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.1/10 (medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
  No correlations have been found to this date 2020-04-15
