## Version 1.4.1
## language: en

Feature:
  TOE:
    Web scanner test site
  Category:
    Improper Neutralization of Special Elements in Output Used by a
    Downstream Component ('Injection')
  Location:
    http://www.webscantest.com/crosstraining/aboutyou2.php
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page
    Generation ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject Cross site scripting
  Recommendation:
    protect each text box

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Ubuntu (Bionic) | 18.04.4 LTS       |
    | Google Chrome   | 80.0.3987.132     |
  TOE information:
    Given I am accessing the site
    """
    http://www.webscantest.com/crosstraining/products.php
    """
    Then I can see a list of products
    And I access to last product

  Scenario: Normal use case:
    Given I access ../crosstraining/product.php?id=9
    And I see information about "economy broom"
    And a link to submit my own description about the broom
    Then I access to that link
    And fill the fields
    """
    name: richard
    email: email@email.com
    title: Garbage
    description: you should make your own broom
    """
    And after submitted it and check again the product page, I [see](1.png)

  Scenario: Static detection
    Given I couldn't access to source code
    Then there isn't a static detection scenario either

  Scenario: Dynamic detection
    Given I access .../product.php?id=9
    Then I filled each field with "<script>alert(1)</script>"
    And in the information page there was just one alert
    And I checked the [page source code](t1-s.png)
    And It looks like email, name and title fields are protected
    But in title field seems is only filtering "<"
    When I tried with single and double quote
    And I saw name field is only [protected against double quote](t2-s.png)
    Then I concluded we can inject javascript using all fields together

  Scenario: Exploitation
    Given information I got from dynamic detection
    And before "name" is "email" inside an "a" tag
    Then I tried to use all fields to perform a javascript injection
    """
    name: `;alert(2);a = `
    email: " onmouseover='alert(1);var a = `
    title: `; alert(3); a=`
    description: `;alert(4)'> Let's play a game </a>
    """
    And [all alerts](alerts.png) showed up, this is the [view-source](a-s.png)
    And I concluded this page is completly exploitable

  Scenario: Remediation
    The shorter and easiest way is to use htmlspecialchars() function to
    protect all fields

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.5/10 (Low)     - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-20
