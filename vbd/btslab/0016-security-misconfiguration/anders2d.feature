## Version 1.4.1
## language: en

Feature:
  TOE:
    bts lab
  Category:
    Security Misconfiguration
  Location:
    http://localhost/btslab/setup.php - content (text)
  CWE:
    CWE-16: Configuration
  Rule:
    REQ.179 Define backup frequency
    REQ.183 Delete sensitive data securely
  Goal:
    Search if setup page run after install
  Recommendation:
    Remove setup page after installation

  Background:
  Hacker's software:
    | <Software name>|    <Version>    |
    | Chromer        | 76.0.3809.132   |
    | Windows        | 10.0.1809 (x64) |
  TOE information:
    Given I am accessing the site http://localhost/btslab/
    And the server is running MariaDB version 10.1.36
    And PHP version 5.6.38
    And Apache version 2.4.34
    And is running on Windows 10 with XAMPP 5.6

  Scenario: Normal use case
    When I am have access to http://localhost/btslab/setup.php
    Then I can install the webpage for firstime

  Scenario: Static detection
    Given ToE source code
    When I inspect 'setup.php'
    Then I see the vulnerability is being caused by lines 4 to 8
    """
    4    include($_SERVER['DOCUMENT_ROOT'].'/btslab/config.php');
    5    if(isset($_POST['install']))
    6    {
    7    if($_POST['install']==1)
    8    {
    """
    Then I see the setup file is not forbidden to run if database exist
    And I notice that anyone can use setup.php.

  Scenario: Dynamic detection
    When I install for firstime the website
    Then I am going to setup page again
    Then I notice that I can view the setup page again

  Scenario: Exploitation
    Given I install the web page for firstime
    When I install the page for secondtime
    Then I notice that the database is restored
    Then I conclude that I can damage the database

  Scenario: Remediation
    Given I patched the session fixation vulnerability with the next functions
    """
    include(configFile);
    mysql_connect($db_server,$db_user,$db_password)
    mysql_select_db($db_name,$con);
    mysql_query(query to check if table exists)
    file_exists(file)
    """
    And I add code in 'header.php' as seen:
    """
    173 include($_SERVER['DOCUMENT_ROOT'].'/btslab/config.php');
    174 $dbExist=true;
    175 $con=mysql_connect($db_server,$db_user,$db_password) or
        die("Connection Failure: ".mysql_error());
    176 mysql_select_db($db_name,$con);
    177 $result=mysql_query("select 1 from users limit 1;") or $dbExist=false;
    178 if ($dbExist and file_exists("setup.php")){
    179     ?>
    180     <br>
    181     <h1> Please delete setup.php file to normal use</h1>
    182     <form action="index.php">
    183         <button type="submit">Continue</button>
    184     </form>
    185 <?php
    186 exit();
    187 }
    """
    When I try to use the page after installation
    Then the web page show me a alert message (evidence)[evidence1.png]
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.5/10 (High) - E:H/RL:O/RC:R/
  Environmental: Unique and relevant attributes to a specific user environment
    8.5/10 (High) - CR:H/IR:M/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:N/MI:N/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-09-04
