## Version 1.4.1
## language: en

Feature:
  TOE:
    badstore
  Category:
    Weak Password Requirements
  Location:
    http://192.168.1.213
  CWE:
    CWE-521: Weak Password Requirements
  Rule:
    REQ.142: https://fluidattacks.com/web/rules/142/
  Goal:
    Login to mysql server using default password.
  Recommendation:
    Use Strong passwords

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | nmap            | 7.60        |
    | mysql-client    | 14.14       |
  TOE information:
    Given I have access to the url http://192.168.1.213
    And the server is running on a virtual machine
    And the machine is in the local network
    And I run nmap to scan all ports

  Scenario: Normal use case
    When I open http://192.168.1.213
    Then I can see the badstore site

  Scenario: Static detection
    When I do not have access to the source code
    Then I am not able to do static detection.

  Scenario: Dynamic detection
    When I use nmap to detect open ports file[evidence](ports.png)
    And I try to login to mysql server using default credentials
    Then I am able to login to mysql server with root access
    Then I conclude that mysql server has weak credentials.

  Scenario: Exploitation
    When I try to login to mysql with default user and password
    Then I am able to login as root file[evidence](root-login.png)
    Then I conclude mysql server has weak credentials

  Scenario Outline: Extraction
    When I login to mysql server as root
    And I select the available database.
    Then I am able to download all the database.
    | <file>     | <output> | <evidence>             |
    | FULLDB.sql |   OUTPUT | [evidence](full-db.png)|
    Then I can conclude that I have all privileges with this user
    Then that I can download the complete database.

  Scenario: Remediation
    When I user secure password for mysql users
    And I do not allow remote connections to mysql server
    And the script forces to read only from an specific folder
    Then I will not be able to login to mysql server
    Then I will not be able to dump the database from the server.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.2/10 (High) - CVSS:3.0CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.8/10 (High) - E:F/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.8/10 (High) - MAV:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-17
