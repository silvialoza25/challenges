{ fetchNixpkgs
, ...
}:
{
  inputs = {
    nixpkgs = fetchNixpkgs {
      rev = "f88fc7a04249cf230377dd11e04bf125d45e9abe";
      sha256 = "1dkwcsgwyi76s1dqbrxll83a232h9ljwn4cps88w9fam68rf8qv3";
    };
  };
  formatBash = {
    enable = true;
    targets = [
      "/makes"
    ];
  };
  formatNix = {
    enable = true;
    targets = [
      "/makes"
    ];
  };
  lintNix = {
    enable = true;
    targets = [
      "/makes"
    ];
  };
  lintBash = {
    enable = true;
    targets = [
      "/makes"
    ];
  };
}
