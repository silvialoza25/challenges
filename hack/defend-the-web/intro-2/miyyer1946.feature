## Version 2.0
## language: en

Feature: Defend-the-web-intro-2

  Site:
    https://defendtheweb.net/
  Category:
    Hacking
  User:
    miyyer1946
  Goal:
    Find the login username and password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows          | 10           |
    | Microsoft Edge   | 84.0.522.63  |

  Scenario: Fail: unknown username and password
    Given the login form [evidences](01.png)
    Then I validate with different generic values
    And I also validate a possible vulnerability of sql injection
    Then by a GET request sent an sql instruction [evidences](02.png)
    """
    https://defendtheweb.net/playground/intro2?id=' or '1'='1
    Invalid login details
    """
    Then I can't start session

  Scenario: Successful: use the web browser's code inspection tool
    Given a web form, the access user and password is required
    Then I use the microsotf-edge inspection tool
    Then I find two HTML tags that are not visible on the page
    """
    <span style="color: rgba(0,0,0,0)">is xxxxxxxxxx</span>
    <span style="color: rgba(0,0,0,0)">is xxxxxxxxxx</span>
    """
    And because of its font color it is not reflected in session start form
    Then I really read the user and the access password [evidences](03.png)
    And I conclude that the code inspection worked
    And I solved the challenge [evidences](04.png)
