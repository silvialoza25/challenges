## Version 2.0
## language: en

Feature: Defend-the-web-intro-1

  Site:
    https://defendtheweb.net/
  Category:
    Hacking
  User:
    miyyer1946
  Goal:
    Decrypt the password using the HTML source code

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows          | 10           |
    | Microsoft Edge   | 84.0.522.63  |

  Scenario: Fail: unknown username and password
    Given the login form [evidences](01.png)
    Then I validate with generic values
    """
    user:admin
    password:admin
    """
    And I also validate a possible vulnerability of sql injection
    """
    user: 'or 1=1;#
    password: 'or 1=1;#
    """
    Then I can't start session

  Scenario: Successful: use the option to view microsoft-edge page source code
    Given a web form, the access user and password is required
    Then I access the html source code inspection
    When I get the source code, I find a comment with the user and password
    Then I really read the user and the access password [evidences](02.png)
    And I conclude that the code inspection worked
    And I solved the challenge [evidences](03.png)
