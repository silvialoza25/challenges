## Version 2.0
## language: en

Feature: encryption9
  Site:
    Brainquest
  Category:
    crypto
  User:
    Katorea132
  Goal:
    Reverse the encryption of the flag

  Background:
    Hacker's software:
        | <Software name> | <Version>   |
        | Windows         |     10      |
        | Firefox         |   82.0.3    |

  Machine information:
    Given I have access to Valhalla
    And I have a Brainquest account
    And I have Windows 10 as OS
    And I have internet access

  Scenario: Fail: Substitution cipher tool
    Given I have a encrypted message
    """
    Pbeerpg nafjre vf OenvaVaUnaq
    """
    When I guess it's some kind of substituon cipher
    And I try to solve it with boxentriq automatic solver
    Then I get a string that seems like the key [evidences](criptogram.png)
    """
    https://www.boxentriq.com/code-breaking/cryptogram
    correct answer is frainingany
    """
    And I try it as the flag
    Then it failed

  Scenario: Success: Brute force rotations
    Given I have a encrypted message
    """
    Pbeerpg nafjre vf OenvaVaUnaq
    """
    And I see the name of the challenge
    """
    Even you, Brutus?
    """
    When I guess it's a form of Caesar Cipher
    And I try to solve it brute forcing rotations
    When using the dcoder online tool [evidences](brutrot.png)
    Then the 13 rotations string makes sence while the others don't
    """
    https://www.dcode.fr/caesar-cipher
    Correct answer is ***********
    """
    When I try to put it as the key
    Then it succeeds
