## Version 2.0
## language: en
Feature: starters-www.tryhackme.com
  Site:
    www.tryhackme.com
  Category:
    Starters
  User:
    juan6630
  Goal:
    Retrieve the password.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows 10      | 20H2        |
    | Mozilla Firefox | 85.0.2      |
    | Nmap            | 7.80        |
    | GitTools        | 1.0         |
  Machine information:
    Given I start the challenge with the following IP address
    """
    10.10.48.140
    """
    When I access a machine via VPN
    Then I see a login page

  Scenario: Fail: Source code search
    Given I have access to the challenge's webside
    When I try using admin:admin as user and password
    And An "invalid username or password" message appear
    When I decide to search for information inside the source code
    Then I don't get an interesting result

  Scenario: Success: Git dumping
    Given I have a website
    When I scan with Nmap
    """
    nmap -A -T4 10.10.48.140
      PORT    STATE SERVICE VERSION
      80/tcp  open  http    nginx 1.14.0
      | http-git:
      |   10.10.48.140:80/.git/
      |     Git repository found!
    """
    And I open the git URL
    Then I see [evidence](repository.png)
    And I can download and see the files
    When I search for "download git repository from webserver"
    Then I found GitTools
    And I use the dumper [evidence](gitdumper.png)
    When I check all the downloaded files
    And I don't get any extra information
    Then I check the git documentation to see the commit history
    When I use <<git log>> on the folder
    Then I get the commit history [evidence](commits.png)
    And I see hints in some of them
    """
    -They want me to use something called 'SHA-512'
    -Obfuscated the source code.
    -Made the login page, boss!
    """
    When I check the documentation for Git again
    And I use <<git log -p>>
    Then I can see the changes from each commit [evidence](flag.png)
    And I solve the challenge
