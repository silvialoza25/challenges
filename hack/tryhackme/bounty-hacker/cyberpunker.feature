## Version 2.0
## language: en

Feature: Bounty-Hacker

  Site:
    www.tryhackme.com
  User:
    thecyberpunker
  Goal:
    Get root and user flags

 Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Parrot OS       | 4.11      |
    | Google Chrome   | 89.0.4    |
    | Ftp             | 2.2       |
    | Nmap            | 7.9.1     |
    | Openvpn         | 2.5.1     |
    | Hydra           | 9.1       |
    | OpenSSH         | 8.4p1     |

 Machine information:
    Given I am accessing the challenge through the URL [evidence](1.png)
    """
    https://tryhackme.com/room/cowboyhacker
    """
    And I got an ip address "10.10.72.232"
    Then I start a Nmap service enumeration
    """
    nmap 10.10.72.232
    """
    And I got some open ports [evidence](2.png)
    """
    PORT STATE SERVICE
    21/tcp open ftp
    22/tcp open ssh
    80/tcp open http
    """
    Then I open the IP in the browser [evidence](3.png)
    And I realize that there is a website chat room
    And I see the source code [evidence](4.png)
    And I get the users to create a wordlist [evidence](5.png)

 Scenario: Fail: Try to access to FTP service
    Given I have some information about FTP open port
    When I try to access to the FTP service "anonymous"
    """
    ftp 10.10.72.232
    anonymous >> service not known
    """
    And I got some problems
    When I search for the problem
    And I realize that there is a problem with my Firewall

 Scenario: Success: Try to access to FTP service
    Given I have a FTP service running in the target machine
    When I try to connect with an Anonymous FTP
    And I got access [evidence](6.png)
    When I list the files
    And I found two files [evidence](7.png)
    Then I copy the files to my machine
    """
    mget locks.txt task.txt
    """
    When I see the content of "locks.txt" file
    And I realize that the file is a wordlist [evidence](8.png)
    When I see the content of "task.txt" file
    And I realize that I found a new username
    Then I add that username to my "user.txt" file [evidence](9.png)


 Scenario: Success: Bruteforce the SSH service
    Given I have some wordlist
    Then I can perform a bruteforce to the SSH service
    And I start to bruteforce the SSH service with the wordlist
    """
    hydra ssh://10.10.72.232 -L users.txt -P locks.txt
    """
    And I got the first user password [evidence](10.png)
    Then I login with the password to the SSH service
    """
    ssh lin@10.10.72.232
    """
    And I got access to the SSH service
    When I list the files
    And I found a file "user.txt"
    When I see the file content [evidence](11.png)
    And I realize that it is the user flag
    Then I try to list the root permissions [evidence](12.png)

 Scenario: Success: Try Privilege Escalation
    Given I have some clues
    And I realize that I can perform Privilege Escalation to "/bin/tar"
    And I search for an exploit
    Then I perform Privilege Escalation
    """
    tar -cf /dev/null /dev/null --checkpoint=1 --checkpoint-action=exec=/bin/sh
    """
    And I got root access [evidence](13.png)
    Then I go to the root directory "cd /root"
    And I found a file "root.txt"
    When I read the file
    Then I found the root flag [evidence](14.png)
    And I validate in the website challenge [evidence](15.png)
