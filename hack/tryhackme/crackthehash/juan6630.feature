## Version 2.0
## language: en
Feature: www.tryhackme.com
  Site:
    www.tryhackme.com
  User:
    juan6630
  Goal:
    Retrieve the passwords cracking the hashes.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows 10      | 20H2        |
    | kali-linux      | 2020.4      |
    | Mozilla Firefox | 85.0.2      |
    | John the Ripper | 1.9.0       |
    | Hashcat         | 6.1.1       |
    | hashID          | 3.1.4       |
    | hash-identifier | 1.2         |
  Machine information:
    Given I start the challenge with the following hashes
    """
    Task 1:
      Hash 1 - 48bb6e862e54f2a795ffc4e541caed4d
      Hash 2 - CBFDAC6008F9CAB4083784CBD1874F76618D2A97
      Hash 3 - 1C8BFE8F801D79745C4631D09FFF36C82AA37FC4CCE4FC946683D7B336B63032
      Hash 4 - $2y$12$Dwt1BZj6pcyc3Dy1FWZ5ieeUznr71EeNkJkUlypTsgbX1H68wsRom
      Hash 5 - 279412f945939ba78ce0758d3fd83daa

    Task 2:
      Hash 1 - F09EDCB1FCEFC6DFB23DC3505A882655FF77375ED8AA2D1C13F640FCCC2D0C85
      Hash 2 - 1DFECA0C002AE40B8619ECF94819CC1B
      Hash 3 - $6$aReallyHardSalt$6WKUTqzq.UQQmrm0p/T7MPpMbGNnzXPMAXi4bJMl9be.
               cfi3/qxIf.hsGpS41BqMhSrHVXgMpdjS6xeKZAs02
        Salt - aReallyHardSalt
      Hash 4 - e5d8870e5bdd26602cab8dbe07a942c8669e56d6
        Salt - tryhackme
    """

  Scenario: Success: MD5 hash crack
    Given The hash for the task 1.1
    When I use hashID to identify the type of hashing
    """
    hashid -j '48bb6e862e54f2a795ffc4e541caed4d'
    """
    Then I get the result
    """
    MD5 [JtR Format: raw-md5]
    """
    When I save the hash in the <<flag1>> file
    And I use john with
    """
    john --format=raw-md5 flag1
    """
    Then I get the result
    """
    Proceeding with incremental:ASCII
    ****              (?)
    Session completed
    """
    And I solve the task

  Scenario: Success: SHA-1 hash crack
    Given The hash for the task 1.2
    When I use hash-identifier
    """
    hash-identifier -j
    ------------------------------------------------
    HASH: CBFDAC6008F9CAB4083784CBD1874F76618D2A97
    """
    Then I get the result
    """
    Possible Hashs:
    [+] SHA-1
    [+] MySQL5 - SHA-1
    """
    When I save the hash in the <<flag2>> file
    And I use john with
    """
    john --format=raw-sha1 --wordlist=/usr/share/wordlists/rockyou.txt flag2
    """
    Then I get the result [evidence](john.png)
    """
    ***********         (?)
    Session completed
    """
    And I solve the task

  Scenario: Success: SHA-256 hash crack
    Given The hash for the task 1.3
    When I use hash-identifier [evidence](identifier.png)
    """
    hash-identifier -j
    ------------------------------------------------
    HASH: 1C8BFE8F801D79745C4631D09FFF36C82AA37FC4CCE4FC946683D7B336B63032
    """
    Then I get the result
    """
    Possible Hashs:
    [+] SHA-256
    [+] Haval-256
    """
    When I save the hash in the <<flag3>> file
    And I use john with
    """
    john --format=raw-sha256 --wordlist=/usr/share/wordlists/rockyou.txt flag3
    """
    Then I get the result
    """
    Will run 2 OpenMP threads
    *******         (?)
    Session completed
    """
    And I solve the task

  Scenario: Fail: bcrypt with john
    Given The hash for the task 1.4
    When I use hash-identifier
    """
    hashid -j '$2y$12$Dwt1BZj6pcyc3Dy1FWZ5ieeUznr71EeNkJkUlypTsgbX1H68wsRom'
    """
    Then I get the result
    """
    [+] Blowfish(OpenBSD) [JtR Format: bcrypt]
    [+] Woltlab Burning Board 4.x
    [+] bcrypt [JtR Format: bcrypt]
    """
    When I save the hash in the <<flag4>> file
    And I use john with
    """
    john --format=bcrypt --wordlist=/usr/share/wordlists/rockyou.txt flag4
    """
    But It was too slow to solve the challenge in a reasonable time

  Scenario: Success: bcrypt with hashcat
    Given The hash and hash type for the task 1.4
    When I use hashcat with
    """
    hashcat -m 3200 flag4 /usr/share/wordlists/rockyou.txt
    """
    Then I get the result
    """
    $2y$12$Dwt1BZj6pcyc3Dy1FWZ5ieeUznr71EeNkJkUlypTsgbX1H68wsRom:****
    Session.......: hashcat
    Status........: Cracked
    Hash.Name.....: bcrypt $2*$
    """
    And I solve the task

  Scenario: Success: Online tool
    Given The hash for the task 1.5
    When I use hash-identifier
    But I don't get a clear result
    Then I use hashes.com
    """
    https://hashes.com/en/decrypt/hash
    """
    And I get the result
    """
    279412f945939ba78ce0758d3fd83daa:**********
    """
    And I solve the task

  Scenario: Success: SHA-256 with hashcat
    Given The hash for the task 2.1
    When I use hash-identifier
    """
    hash-identifier -j
    ------------------------------------------------
    HASH: F09EDCB1FCEFC6DFB23DC3505A882655FF77375ED8AA2D1C13F640FCCC2D0C85
    """
    Then I get the result
    """
    Possible Hashs:
    [+] SHA-256
    [+] Haval-256
    """
    When I save the hash in the <<flag21>> file
    And I search for the hashcat formats with <<hashcat -h>>
    And I use hashcat with
    """
    hashcat -m 1400 flag21 /usr/share/wordlist/rockyou.txt
    """
    Then I get the result
    """
    F09EDCB1FCEFC6DFB23DC3505A882655FF77375ED8AA2D1C13F640FCCC2D0C85:*****
    Session............: hashcat
    Status.............: Cracked
    Hash.Name..........: SHA2-256
    """
    And I solve the task

  Scenario: Success: NTLM with hashcat
    Given The hash for the task 2.2
    When I use hashes.com to identify the type
    """
    https://hashes.com/en/tools/hash_identifier
    """
    Then I get the result
    """
    1DFECA0C002AE40B8619ECF94819CC1B - Possible algorithms: NTLM
    """
    When I save the hash in the <<flag22>> file
    And I use hashcat with
    """
    hashcat -m 1000 flag22 /usr/share/wordlist/rockyou.txt
    """
    Then I get the result
    """
    1DFECA0C002AE40B8619ECF94819CC1B:************
    Session............: hashcat
    Status.............: Cracked
    Hash.Name..........: NTLM
    """
    And I solve the task

  Scenario: Success: SHA512crypt with hashcat
    Given The hash for the task 2.3
    When I use hash-identifier
    """
    hashid -j '$6$aReallyHardSalt$6WKUTqzq.UQQmrm0p/T7MPpMbGNnzXPMAXi4bJMl9be.
    cfi3/qxIf.hsGpS41BqMhSrHVXgMpdjS6xeKZAs02'
    """
    Then I get the result
    """
    [+] SHA-512 Crypt [JtR Format: sha512crypt]
    """
    When I save the hash in the <<flag23>> file
    And I use hashcat with
    """
    hashcat -m 1800 flag23 /usr/share/wordlist/rockyou.txt
    """
    Then I get the result [evidence](hashcat.png)
    """
    $6$aReallyHardSalt$6WKUTqzq.UQQmrm0p/T7MPpMbGNnzXPMAXi4bJMl9be.cfi3/qxIf.
    hsGpS41BqMhSrHVXgMpdjS6xeKZAs02:******
    Session............: hashcat
    Status.............: Cracked
    Hash.Name..........: sha512crypt $6$
    """
    And I solve the task

  Scenario: Success: HMAC-SHA1 with hashcat
    Given The hash for the task 2.4
    And The hint <<HMAC-SHA1>>
    When I save the hash and the salt in the <<flag24>> file
    And I use hashcat with
    """
    hashcat -m 160 flag24 /usr/share/wordlist/rockyou.txt
    """
    Then I get the result
    """
    e5d8870e5bdd26602cab8dbe07a942c8669e56d6:tryhackme:***********
    Session............: hashcat
    Status.............: Cracked
    Hash.Name..........: HMAC-SHA1 (key = $salt)
    """
    And I solve the task
    And I solve the challenge [evidence](solve.png)
