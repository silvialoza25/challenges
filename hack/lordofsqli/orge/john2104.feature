## Version 2.0
## language: en

Feature: orge-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/orge_bad2f25db233a7542be75844e314e9f3.php
    """
    When I open the url with Chrome
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_orge where id='guest' and pw=''
    """

   Scenario: Success:Sqli-boolean-exploitation
    Given that I inspected the code
    And I see that they validate "or/and" clauses
    And also to pass the challenge I need the actual admin password
    """
    if(preg_match('/or|and/i', $_GET[pw])) exit("HeHe");
    $_GET[pw] = addslashes($_GET[pw]);
    $query = "select pw from prob_orge where id='admin' and pw='{$_GET[pw]}'";
    if(($result['pw']) && ($result['pw'] == $_GET['pw'])) solve("orge");
    """
    Then I used the following to bypass the and clause
    And to know the length of the admin password
    """
    ?pw=1' || (id='admin' %26%26 LENGTH(pw)=8)--+
    """
    When I knew the length of the password
    Then I created a python script to find it using substr [exploit.py]
    And I get the admin password
    Then I solve the challenge
