## Version 2.0
## language: en

Feature: 998-G33K-enigmes-a-thematiques
  Code:
    998
  Site:
    enigmes-a-thematiques
  Category:
    G33K
  User:
    thecyberpunker
  Goal:
    Get the IPv6

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |
    | Nslookup        | 1.1.1         |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/998
    """
    And I see a French message [evidence](1.png)
    When I translate the message
    And I realize about an URL
    """
    www.eat.cloudns.org
    """
    Then I try to go to the URL
    And I got an error message [evidence](2.png)

  Scenario: Fail: Try to reach the URL
    Given the information of the article
    When I check the source code
    And I got nothing relevant
    Then I try to Ping the website
    """
    ping www.eat.cloudns.org
    """
    And I got an error message [evidence](3.png)

  Scenario: Fail: Try to reach the IPv6 address
    Given the message information
    When I read it again
    Then I realize about to get an IPv6 address
    When I try to do a Nslookup
    """
    nslookup  www.eat.cloudns.org
    """
    And I got in return the domain information [evidence](4.png)
    And I realize about the IPv6 address
    """
    Address: 2001:<FLAG>:336b
    """
    When I try to enter the IPv6 address as an answer
    And I got a bad response [evidence](5.png)

  Scenario: Success: Try to decrypt the IPv6 address
    Given the information from the text
    And I realize about the IPv6 address looks like a Hexadecimal code
    When I try to decode the IPv6 address
    And I got a possible flag [evidence](6.png)
    Then I go to validate that flag as an answer
    And I got a message "Bravo thecyberpunker!"
    And I solve the challenge [evidence](7.png)
