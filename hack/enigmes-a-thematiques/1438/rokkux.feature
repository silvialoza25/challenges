## Version 2.0
## language: en

Feature: 1438-H4CK1NG-enigmes-a-thematiques
  Code:
    1438
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Find the key "<flag>" for the provided username

  Background:
  Hacker's software:
    |         <Software name>       |            <Version>            |
    |  Kali Linux                   | 20.04 LTS                       |
    |  Firefox                      | 88.0.1(64-bit)                  |
    |  Ghidra                       | 9.1                             |

  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/front/enigme/1438
    """
    And the message
    """
    Vous êtes un hacker, et vous devez réussir à vous connecter sur le
    système d'identification d'EAT. Retrouvez la clé pour votre
    nom : "Hacker".
    """
    And the link "système d'identification d'EAT"

  Scenario: Fail: Open the executable and try to login with random password
    Given the web page [evidence](01.png)
    When I read the message
    Then I see that I have to login into a system
    And I should use the name "Hacker"
    And I see my goal is to find the "key" for my name, "Hacker"
    When I click on the link inside the message
    Then I see a box allowing me to download a file named [evidence](02.png)
    """
    executablelvl3.exe
    """
    And I see it is a "DOS/Windows executable" file
    When I open it with "wine" [evidence](03.png)
    """
    wine executableLevel3.exe
    """
    Then I see a box with the title "Crackme" [evidence](04.png)
    And the text input fields for login in
    And a button for validation
    And a blank rectangle for login "status" message
    When I input "Hacker" in the "Nom" field
    And I input any value in "Cle d'activation" field
    And I click "Identification" button for validation [evidence](05.png)
    Then I get the message "Bad!" on the "login status" field
    And I couldn't capture the flag
    And I conclude I can't use this method

  Scenario: Fail: Use Ghidra to analyze the .exe file
    Given the information from the previous scenario
    When I analyze the file's code on "Ghidra"
    Then I realize the ".exe" was developed in "delphi" [evidence](06.png)
    And I see it is a "x86" file
    And I can see a lot of functions inside it
    When I find the entry function [evidence](07.png)
    And I read it
    Then I can't manage to find the "<flag>" nor a hint inside it
    And I couldn't get the "<flag>"
    And I conclude I can't use this method

Scenario: Fail: Use DelphiDecompiler, ghidra and x32dbg to analyze .exe
  Given the information on previous scenarios
    When I do some research on google about [evidence](08.png)
    """
    How to view source code of an .exe file
    """
    Then I find out a way to do so is via the use of "Decompilers"
    When I search for "Delphi decompiler" [evidence](09.png)
    Then I see my first search result is a solution for a hacking challenge
    """
    https://backtrackacademy.com/articulo/
    resolver-el-reto-engendrito-con-el-decompilador-de-delphi
    """
    And I see it is similar to the one I am trying to solve right now
    And I read it
    And I see they use tools as "x32dbg"
    And I see they use tools as "DelphiDecompiler.exe"
    And I search for this software and download it
    When I try using "DelphiDecompiler.exe"
    And I analyze the ".exe" following the steps on the page mentioned before
    And I focus my attention on the "TFORM1" form
    And I focus my attention on the "Button1Click" event
    Then I find out the actual function that refers to the "Bad!" message
    And I conclude there should be a function which outputs a "Good!" message
    And I find out the function that refers to the "Good!" message
    And I see their possible functions reference [evidence](10.png)
    When I look for these functions on ghidra
    Then I don't find any valuable information
    And I decide to use "x32dbg"
    And I see "x32dbg" is a debugger for windows
    When I open it
    And I upload the ".exe" from the challenge
    Then I see nothing happens
    When I research for a solution to this
    Then I find out it may be a registry problem
    """
    https://github.com/x64dbg/x64dbg/issues/2535
    """
    When I try to solve the issue with the way proposed in the above link
    Then I don't get the expected result
    And I couldn't capture the flag
    And I conclude I can't use this method

  Scenario: Success: Use OllyDbg to debug the .exe file
    Given the information from the previous scenarios
    When I do some research on "x32 .exe debuggers"
    Then I learn about the existence of the software "OllyDbg"
    And I download it
    When I run it as "sudo"
    And I upload the ".exe" file from the challenge
    Then I get the debugging information of the ".exe" file [evidence](11.png)
    And I search for the function that outputs the "Bad!" message
    And I put a breakpoint on its line of code
    When I run the ".exe" with the "play" button above [evidence](12.png)
    Then the ".exe" starts running
    And I input "Hacker" in the username field
    When I click for validation
    Then I see I can follow its processing step-by-step
    And I see the windows on "OllyDbg" change [evidence](13.png)
    And I see some ASCII values on the bottom right of the screen
    And I see the second of them is above the string
    """
    Cle d'activation
    """
    And I see the last value is the same I use for username
    And I think this second value could be the "<flag>"
    When I go back to the challenge's page
    And I type the "<flag>" into the validation field
    And I click for validation
    Then Page displays the message "Bravo RokkuX !"
    And I solved the challenge
    And I captured the flag
