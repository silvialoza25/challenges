## Version 2.0
## language: en

Feature: 1438-H4CK1NG-enigmes-a-thematiques
  Code:
    1438
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
    | Ghidra          | 10.0                   |
    | OllyDBG         | 1.10                   |
    | wine            | 5.0.3                  |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1438
    """
    And the page let me download an executable located in the next link
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/executableLevel3.exe
    """

  Scenario: Fail: Try to use reverse engineering with Ghidra
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1438
    """
    And I download the file
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/executableLevel3.exe
    """
    When I can analyze the executable using the following command
    """
    file executableLevel3.exe
    """
    Then I can see that the executable is for windows
    """
    executableLevel3.exe: PE32 executable (GUI) Intel 80386, for MS Windows
    """
    And I know that to execute this program I can use the tool wine
    When I execute it using the following command
    """
    wine executableLevel3.exe
    """
    Then I can see the following window [evidence](1.png)
    When I press the button "Identification"
    Then I can see that the third field displays the following message
    """
    Bad !
    """
    When I think about using the tool Ghidra
    Then I open the executable with the tool [evidence](2.png)
    When I press the search button
    And I search for strings [evidence](3.png)
    Then I can see the part of the password field [evidence](4.png)
    And I can see the conflicts sign of that field
    When I click on the selected row [evidence](5.png)
    Then I cannot see the text that I saw before
    """
    Cle d'activation
    """
    When I search for the value 'Bad !'
    Then I can see that it has not conflicts [evidence](6.png)
    When I click on the selected row
    Then I can see where and how is located [evidence](7.png)
    When I right-click on the address '00466260'
    And I select the References button
    And I select the 'Show References To Address' option [evidence](8.png)
    Then I can see the following text [evidence](9.png)
    """
    References to 00466260 - 0 locations
    """
    When I notice that the '0 locations' has a relation with the conflicts
    Then I know I will not be able to solve the challenge using Ghidra
    And I have not completed the challenge

  Scenario: Success: Debug the program using wine and OllyDBG
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1438
    """
    And I download the file
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/executableLevel3.exe
    """
    When I analyze the executable using the following command
    """
    file executableLevel3.exe
    """
    Then I can see that the executable is for windows
    """
    executableLevel3.exe: PE32 executable (GUI) Intel 80386, for MS Windows
    """
    And I know that to execute this program I can use the tool wine
    When I execute it using the following command
    """
    wine executableLevel3.exe
    """
    Then I can see the following window [evidence](1.png)
    When I press the button "Identification"
    Then I can see that the third field displays the following message
    """
    Bad !
    """
    When I think about using the tool OllyDBG
    Then I open the executable with the tool [evidence](10.png)
    When I right-click the window located in the upper left corner
    And I select the 'Search for' option
    And I select the 'All references text strings' option [evidence](11.png)
    Then the program displays a new window
    And I right-click on the window
    And I select the 'Search for text' option [evidence](12.png)
    When I search the following text
    """
    Bad !
    """
    Then I can see the text along with the following texts [evidence](13.png)
    """
    -EAT
    Good !
    """
    When I decide to right-click the 'Bad !' text
    And I select the 'Follow in disassembler' button
    Then I can see where the texts are used in the code [evidence](14.png)
    When I put a breakpoint in the text 'Bad !' section [evidence](15.png)
    And I press the run button
    And I change the password value to 'pass' to make the code easier to read
    And I press the identification button
    Then I can see in the lower right window my input
    And I can see the expected input [evidence](16.png)
    When I change my input to the expected input
    Then the program displays the message 'Good !' [evidence](17.png)
    And I know that I can use this technique to log in as the user 'Hacker'
    When I repeat the process changing the user to Hacker
    Then I can see the expected input [evidence](18.png)
    When I change my input to the expected input
    And I press the 'Identification' button
    Then the program displays 'Good !'
    And I know that the password is the flag
    When I submit the flag
    Then the web page displays the following message [evidence](19.png)
    """
    Bravo J!
    """
    And I have completed the challenge
