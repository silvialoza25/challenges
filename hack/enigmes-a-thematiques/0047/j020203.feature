## Version 2.0
## language: en

Feature: 30-H4CK1NG-enigmes-a-thematiques
  Code:
    47
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find and decode the password and fill the textfield

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/front/enigme/47
    """
    And I see a textfield that require a password

  Scenario: Success: Use browser developer tools to inspect web page elements
    Given The textfield requesting the password [evidence](1.png)
    When I right click the textfield
    And I press the inspect option
    Then I can see the document tag
    When I expand that tag
    Then I can see the head and body elements inside the html element
    When I expand the head element
    Then I can see the script element of type javascript
    When I expand the script element
    And I can see the password function
    And I can recognize that part of the code is url encoded [evidence](2.png)
    Then I copy that part of the code
    And I open the following url decoder website
    """
    https://www.urldecoder.org/
    """
    When I decode the password url encoded
    Then I can see the decoded password required [evidence](3.png)
    When I fill the textfield with the decoded password
    Then the web page displays a popup with the message [evidence](4.png)
    """
    Mot de passe correct, tu peux maintenant valider l'épreuve !
    """
    And I have completed the challenge
