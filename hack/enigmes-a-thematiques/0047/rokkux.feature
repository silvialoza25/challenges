## Version 2.0
## language: en

Feature: 47-H4CK1NG-enigmes-a-thematiques
  Code:
    47
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Find and validate Flag

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    | Kali Linux      | 20.04 LTS     |
    | Firefox         | 88.0.1(64-bit)|
  Machine information:
    Given the challenge's flag validation page

  Scenario: Fail: run password from web script in the validation box
    Given I inspect the webpage [evidence](01.png)
    And I find the function for password validation
    When I see the variable for password validation
    Then I type it into the validation box and click for validation
    And I get the message
    """
    Ce n\'est pas le bon mot de passe...
    """
    And I could not capture the flag [evidence](02.png)
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Fail: run password function in JavaScript console
    Given I inspect the webpage
    And I find the function for password validation
    When I call it from the Javascript console
    Then I get error messages from the console [evidence](03.png)
    And I could not capture the flag
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Success: decode unescaped string
    Given I inspect the webpage
    And I find the function for password validation
    And I notice the string inside the "unescape()" method
    When I google "unescape()"
    Then I find out it's a method for decoding an encoded string
    When I decode the string as a URL, US-ASCII with [evidence](04.png)
    """
    https://coderstoolbox.net/string/
    #!encoding=url&action=decode&charset=us_ascii
    """
    Then I get the "<flag>"
    When I return to the challenge's page
    And Input the password for validation in the first "prompt box"
    Then I see a "prompt box" with the message
    """
    Mot de passe correct ! Tu peux entrer ce mot pour valider l'énigme.
    """
    When I submit the password for finishing the challenge
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
