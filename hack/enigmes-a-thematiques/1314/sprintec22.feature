## Version 2.0
## language: en

Feature: 1314-H4CK1NG-enigmes-a-thematiques
  Code:
    1314
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    sprintec
  Goal:
    Find the directory and name of an image in EAT

  Background:
  Hacker's software:
    | <Software name> | <Version>          |
    | Kali Linux      | 2021.2             |
    | Firefox         | 79.7.0esr (64 bit) |
    | Exiftool        | 10.15              |

  Machine information:
    Given I open the following challenge
    """
    https://enigmes-a-thematiques.fr/front/enigme/1314
    """
  Scenario: Fail: Investigate the metadata of the image
    Given the image of the challenge
    When I download the image
    Then I parse file metadata with exiftool
    And I did not find anything relevant [evidence](01.png)

  Scenario: Success: Search the directory based on the source code
    Given the image of the challenge
    When I analyze the source code of the other challenge [evidence](02.png)
    Then I found the directory of this challenge [evidence](03.png)
    """
    https://enigmes-a-thematiques.fr/web/enigmes/epreuve1426/index_v3.php
    """
    When I replace the challenge number with 1314
    """
    https://enigmes-a-thematiques.fr/web/enigmes/epreuve1314/
    """
    Then I got the 1314 challenge directory [evidence](04.png)
    Then I opened the image [evidence](05.png)
    Then I check that it is the same image of the challenge
    Then I look at the name of the image that is the challenge flag
    And I proceeded to validate the flag [evidence](06.png)
