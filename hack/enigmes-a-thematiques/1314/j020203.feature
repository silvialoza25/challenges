## Version 2.0
## language: en

Feature: 1314-H4CK1NG-enigmes-a-thematiques
  Code:
    1314
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find the original image

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
    | DIRB            | 2.22                   |
    | Gobuster        | 3.1.0                  |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1314
    """
    And I know that the challenge's title is the following one
    """
    Directory listing 1
    """
    And the page displays the following message
    """
    Vous reconnaissez cette image ? Non ? Pourtant, je l'ai trouvée quelque
    part sur EAT et recopiée ici sous un autre nom, à vous de retrouver
    l'originale...
    """

  Scenario: Fail: Search PNG images in the images' directory ctf page
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1314
    """
    And I know that the challenge's title is the following one
    """
    Directory listing 1
    """
    When I think about using the fuzzing tool DIRB
    And I look for directories and files
    Then I use the following command [evidence](1.png)
    """
    dirb https://enigmes-a-thematiques.fr/web/enigmes/images/
    """
    When I read the HTTP status codes
    Then I know that those files are not available
    When I decide to use DIRB to look for PNG images on the ctf page
    Then I use the following command to do it [evidence](2.png)
    """
    dirb https://enigmes-a-thematiques.fr/web/enigmes/images/ -X .png
    """
    When I read the HTTP status codes
    Then I know that those files are not available
    When I read the directories found in the first search [evidence](3.png)
    Then I know that inside those directories could be the image
    When I open the first directory
    """
    https://enigmes-a-thematiques.fr/web/enigmes/images/backup/
    """
    And I open the second page
    """
    https://enigmes-a-thematiques.fr/web/enigmes/images/tmp/
    """
    Then the first page has a list of links with images
    And the Names has some random characters
    And the second page redirects me to a non-existent page
    """
    https://enigmes-a-thematiques.fr/membre.php
    """
    When I close the second page
    And I open the first page
    """
    https://enigmes-a-thematiques.fr/web/enigmes/images/backup/
    """
    And I read the categories that allow me to order the links
    Then I notice that I can filter with the size of the file
    When I open the following image on a new page
    And I use the developers' tools to see its size [evidence](4.png)
    Then I search in the page with the links listed the following size
    """
    7.4K
    """
    And I see one result
    When I open the image
    Then I realize that the image does not correspond to the ctf
    And I have not completed the challenge

  Scenario: Fail: Search the image on the ctf main page
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/
    """
    And I know that the challenge's title is the following one
    """
    Directory listing 1
    """
    When I think about using the fuzzing tool DIRB
    And I look for PNG images using the following command
    """
    dirb https://enigmes-a-thematiques.fr/ -X .png
    """
    Then I read the results [evidence](5.png)
    When I notice that the results are the same as the previous image search
    Then I will be not able to find a visible image as the copy of the ctf
    And I have not completed the challenge

  Scenario: Success: Search for a hidden file
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/
    """
    And I know that the challenge's title is the following one
    """
    Directory listing 1
    """
    And I can't find any visible file related to the ctf
    When I think about using the fuzzing tool Gobuster
    And I focus on the main ctf page
    And I know that there is a lot of forbidden and private pages
    And I look for directories and files using the following commands
    """
    wordlist="/usr/share/wordlists/dirb/common.txt"
    url="https://enigmes-a-thematiques.fr/"
    gobuster dir -w $wordlist -u $url -b "404,403,301,307,429"
    """
    Then I read the results [evidence](6.png)
    And I know that can be hidden using the robots.txt file
    When I read the content of the robot.txt file
    And I can see that there is a hidden challenge
    And I notice that any file inside the directory is hidden [evidence](7.png)
    Then I try to copy the path of the hidden challenge
    And I change the number of the challenge to the number of this challenge
    """
    1314
    """
    When I open the URL created
    Then I can see the hidden PNG image [evidence](8.png)
    And I can see the flag
    When I submit the flag
    Then the web page displays the following message [evidence](9.png)
    """
    Bravo J!
    """
    And I have completed the challenge
