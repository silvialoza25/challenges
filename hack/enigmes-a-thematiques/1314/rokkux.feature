## Version 2.0
## language: en

Feature: 1314-H4CK1NG-enigmes-a-thematiques
  Code:
    1314
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Find the original name of the image (flag)

  Background:
  Hacker's software:
    |         <Software name>       |            <Version>            |
    |  Kali Linux                   | 20.04 LTS                       |
    |  Firefox                      | 88.0.1(64-bit)                  |
    |  imagemagick [identify]       | 6.9.11-60 Q16 x86_64 2021-01-25 |
    |  dirsearch                    | 0.4.2                           |
    |  Google Chrome                | 91.0.4472.114                   |
    |  Tab Save                     | 1.4.0.2                         |

  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/front/enigme/1314
    """
    And the image [evidence](01.png)
    And the text
    """
    Vous reconnaissez cette image ? Non ? Pourtant, je l'ai trouvée quelque
    part sur EAT et recopiée ici sous un autre nom, à vous de retrouver
    l'originale...
    """

  Scenario: Fail: Check for metadata inside the image
    Given the web page
    When I inspect the web page
    Then I see the image source [evidence](02.png)
    And I see its name is "ep1314.png"
    When I load the page corresponding to the image source
    Then I get a page displaying only the image
    And I download the image
    When I use "identify" from "ImageMagick" on the downloaded image
    """
    identify -verbose ep1314.png
    """
    Then I don't find any valuable information [evidence](03.png)
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Fail: Use google dorks to check for images inside web site
    Given the web page
    When I realize I should look for the same image inside the web Page
    And I notice the flag should have the format "name.extension"
    And I see the image has the extension .png
    And I use the following search criteria on "google.com"
    """
    site: enigmes-a-thematiques.fr filetype:png
    """
    Then I don't get any results [evidence](04.png)
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Fail: Check all of the images related to challenges in the page
    Given the web page
    And the image name
    And the image source link
    When I check another challenge that has an image on it
    And I see the path to the image on this other challenge [evidence](11.png)
    Then I realize this path is similar to the one on the challenge I'm solving
    When I see the only difference it's the name of the picture
    And use the following format
    """
    ep#### (where #### is the number of the challenge)
    """
    Then I realize I can start to look for all of the images inside the page
    And I realize I can download them using the urls
    """
    https://enigmes-a-thematiques.fr/web/enigmes/images/ep####.png
    """
    And I realize I can semi-automate the process using online tools
    And I can use google sheets to generate the other links [evidence](12.png)
    And I use "https://httpstatus.io/" to check for url status codes
    And I only use the "http 200" status urls [evidence](13.png)
    And Google Chrome's "Tab Save" extension [evidence](14.png)
    When I download all of the images [evidence](15.png)
    Then I check if any of them is similar to the image of the challenge
    When I see none of them is similar to the image of the challenge
    Then I conclude I cannot use this method
    And I couldn't capture the flag

  Scenario: Success: Check for existing directories in the web page
    Given the web page
    When I use "dirsearch" to check for hidden paths and files on
    """
    enigmes-a-thematiques.fr
    """
    Then I see there is a robots.txt file [evidence](05.png)
    When I load enigmes' page with the path to robots.txt [evidence](06.png)
    Then I see that there is a list of URL paths
    And these paths do not allow robots to track the contents inside these urls
    When I check each one of these urls
    And I see one of them accesses a file of a challenge .bot with extension
    And I see this url points to a challenge folder with the format
    """
    epreuve#### (number of the challenge)
    """
    Then I go to that url
    When I load the page
    Then I see some characters [evidence](07.png)
    And I realize these characters correspond to the challenge in the url
    And I realize that if I delete the file from the url, I will get its parent
    When I delete the file from the urls
    And I reload the page
    Then I get the main page of "enigmes-a-thematiques.fr"
    When I go back to the previous page
    And I see the number of the challenge inside the url
    And I search for the challenge inside enigme's page [evidence](08.png)
    Then I see the page for that challenge doesn't have any embedded files
    And I realize I should try modifying the url with another challenge code
    And I should use the code of a challenge that has a file or image displayed
    When I load the url, taking into account everything said above as follows:
    """
    https://enigmes-a-thematiques.fr/web/enigmes/epreuve1314/
    """
    Then I get the index for the challenge number 1314
    And I see there is a file with extension ".png"
    When I click it
    Then I get a page with the same image of the challenge I am trying to solve
    And I realize the name of this newly displayed picture it's the "<flag>"
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
