## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    cryptography
  User:
    AlejandroTL (wechall)
  Goal:
    Get the message

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
    | Exiftool        | 11.88       |
  Machine information:
    Given an image [evidence](01.png)

  Scenario: Fail: Checking metadata
    Given the image
    When I use exiftool to check metadata
    Then I get nothing useful [evidence](02.png)
    And I can't solve the challenge

  Scenario: Success: Analyzing symbols
    Given the image
    And I notice it looks like sign language
    When I search sign language alphabet on google [evidence](03.png)
    And I replace each symbol with its corresponding letter
    Then I get a message
    And I try to submit it as the answer
    And I solve the challenge [evidence](04.png)
