## Version 2.0
## language: en

Feature: 152-G33K-enigmes-a-thematiques
  Code:
    152
  Site:
    enigmes-a-thematiques
  Category:
    G33K
  User:
    thecyberpunker
  Goal:
    Get the file from the hidden folder

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |
    | Gobuster        | 3.1.0         |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/21
    """
    And I see a French message [evidence](1.png)
    When I translate the message
    And I realize about to find a secret folder

  Scenario: Fail: Try to find the secret folder
    Given the information of the message
    When I try to find the secret folder
    And I put some aleatory folder names
    And I got a failure message [evidence](2.png)

  Scenario: Success: Try to perform a Directory Scanner
    Given the default URL
    And I realize about to perform a Directory Scanner
    Then I go to "Gobuster"
    And I perform a Gobuster Scanner
    """
    gobuster dir -u example/epreuve21/
    -w example.txt -t 50
    """
    And I got the hidden Directory [evidence](3.png)
    Then I go to the Directory
    And I found the secret file with the password [evidence](4.png)
    When I go to the website to validate
    And I enter the password
    And I got a message "Bravo thecyberpunker!"
    And I solve the challenge [evidence](5.png)
