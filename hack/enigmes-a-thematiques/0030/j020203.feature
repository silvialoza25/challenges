## Version 2.0
## language: en

Feature: 30-H4CK1NG-enigmes-a-thematiques
  Code:
    30
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Decode the password to access authentication page

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve30/admin.php
    """
    And I know that there is a hidden password

  Scenario: Success: Use browser developer tools to inspect web page elements
    Given I open the corresponding web page
    And I see a popup requesting the password [evidence](1.png)
    When I click the "Accept" button without typing anything in the textfield
    Then I see a new popup with the message "Essaye encore !"
    When I click the "Accept" button again
    Then the web page reloads
    And I see just a background image
    When I use the browser developer tools pressing the "F12" button
    And I decide to expand the script tag in the code in the Elements tab
    Then I can see the password required [evidence](2.png)
    When I reload the web page
    Then the popup shows again
    When I use the previous password
    Then I see a new popup with the message
    """
    Mot de passe correct, tu peux maintenant valider l'épreuve !
    """
    And I have completed the challenge
