## Version 2.0
## language: en

Feature: 30-H4CK1NG-enigmes-a-thematiques
  Code:
    30
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Find password for authentication page

  Background:
  Hacker's software:
    | <Software name> |    <Version>   |
    | Kali            | 2021.1         |
    | Firefox         | 78.10.0(64-bit)|
  Machine information:
    Given the web page
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve30/admin.php
    """

  Scenario: Success: check web page's source code
    Given I am on the provided web page
    And I see a "Prompt box" requesting a password
    And I do not fill the whitespace
    When I click for submit
    Then a new "Prompt box" appears with the message "Essaye encore !"
    When I click the "Aceptar" button
    Then the page loads again
    And only shows a background
    When I inspect the page to read its source code
    And I read inside the only "<body>" tag
    Then I see the code for the "Prompt box"
    And I notice the variable "password" with its value [evidence](1.png)
    When I reload the page
    And I see again the "Prompt box" requesting a password
    When I type the value of the variable password
    And I click on "Aceptar" button
    Then a new "Prompt box" displays the message
    """
    enigmes-a-thematiques.fr dice Mot de passe correct,
    tu peux maintenant valider l'épreuve !
    """
    When I return to the challenge's page
    And I input the password for validation
    Then page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
