## Version 2.0
## language: en

Feature: 1102-H4CK1NG-enigmes-a-thematiques
  Code:
    1102
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    Mankar
  Goal:
    Solve an invisible in-memory maze based on javascript code.

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows         | 10            |
    | Chrome          | 91.0.4472.101 |
  Machine information:
    Given I am accessing to the website
    And website shows a bull head image

  Scenario: Fail: Inspect webpage source code and understand puzzle
    Given I have clicked randomly on the webpage with no result
    When I inspect the webpage using browser Dev Tools
    Then I find an embedded javascript code right next to the bull image
    When I try to understand and debug the code
    Then I notice there's an EventListener for 'keydown' events
    When I analyze which keys are accepted by the javascript code
    Then I conclude the accepted keys correspond to the arrows in keyboard
    When I begin pressing arrow keys randomly
    Then I can see a message [evidence](01.png)
        """
        Vous avez parcouru 1 mètres
        (You have walked 1 meter)
        """
    When I examine closely the code when an arrow is pressed
    Then I find that there's a data structure that represents a maze
    And it is composed by 0s and 1s
    And I deduce the keys that are pressed are used to walk through the maze
    When I examine further the code
    Then I notice there's an encrypted message the code prints at some point
    And I deduce that message should be the flag
    And I conclude I need to solve the maze in order to catch it

  Scenario: Success: Print maze, find the correct path and enter correct steps
    Given I inspect the webpage source code
    When I modify it to print the maze with a console.log() statement
    Then I obtain several binary strings [evidence](02.png)
    When I copy the result to a text editor
    And I transform it into a square matrix of 0s and 1s
    Then I try to find a path that traverses the maze from first to last point
    And I draw a path using an image editor [evidence](03.png)
    When I press the corresponding key sequence in the website
    Then I get a message with the solution [evidence](04.png)
    And I caught the flag
