## Version 2.0
## language: en

Feature: 55-cryptographie-enigmes-a-thematiques
  Code:
    55
  Site:
    enigmes-a-thematiques
  Category:
    cryptographie
  User:
    thecyberpunker
  Goal:
    Decrypt the text

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/55
    """
    When I see a French message [evidence](1.png)
    Then I realize about an encrypted text
    """
    OV NLG WV KZHHV VHG NRILRI
    """

  Scenario: Success: Try to decrypt the text
    Given the information from the text
    When I try to identify the type of encryption
    And I got Atbash encryption as result [evidence](2.png)
    Then I go to decrypt the text with Atbash decryption
    And I got a successful decryption [evidence](3.png)
    When I go to validate the decrypted text
    And I got a message "Bravo thecyberpunker!"
    And I solve the challenge [evidence](4.png)
