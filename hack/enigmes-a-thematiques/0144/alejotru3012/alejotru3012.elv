# elvish -compileonly alejotru3012.elv

use str

fn main []{
  @list = (each [line]{
    i_a = (str:index $line "a")
    i_b = (str:index $line "b")
    i_c = (str:index $line "c")
    if (and (> $i_a -1) (< $i_a $i_b $i_c)) {
      echo $line
    }
  })
  echo (order $list)
}

main

# cat dico.txt | elvish alejotru3012.elv
