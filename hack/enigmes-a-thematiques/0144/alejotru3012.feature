## Version 2.0
## language: en

Feature:
  Code:
    alejotru3012.elv
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    Geek
  User:
    AlejandroTL (wechall)
  Goal:
    Get words with "a", "b" and "c" in that order

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
    | VS code         | 1.55.2      |
  Machine information:
    Given the dictionary "dico.txt"

  Scenario: Fail: Using VS code to find words
    Given the dictionary
    When I open it in VS code
    And I press "ctrl + f" to search "abc"
    Then I get a word with "abc" in the file [evidence](01.png)
    And I try to submit it as the answer
    And I can't solve the challenge

  Scenario: Success: Using a script to get words
    Given the dictionary
    When I create an elvish script to get words with "a", "b" and "c"
    And I execute it using
    """
    cat dico.txt | elvish alejotru3012.elv
    """
    Then I get the words [evidence](02.png)
    And I try to submit them as the answer
    And I solve the challenge [evidence](03.png)
