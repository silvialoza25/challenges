## Version 2.0
## language: en

Feature: 48-H4CK1NG-enigmes-a-thematiques
  Code:
    48
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    andrewmi95
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |

  Scenario: Fail: password error
    Given I am on the main page
    And I can see a form with a field and a "Valider" button
    When I do not fill the field
    And I press the "valider" button
    Then I can see an alert with a message
    """
    Ce n'est pas le bon mot de passe...
    """
    When I inspect into the source code [evidences](01.png)
    Then I see the related script
    """
    function password()
    {
    var nombre = 15;
    nombre = (nombre.toString(2)*3).toString(2).substring(2,7);
    if (document.toto.ep48.value == nombre)
        alert("Mot de passe correct ! Tu peux entrer
        ce mot pour valider l\'énigme.");
    else
        alert ("Ce n\'est pas le bon mot de passe...");
    }
    """
    Then I can see the string
    """
    var nombre = 15;
    """
    When I fill the field of the form with the string
    Then I can not capture the flag

  Scenario: Success: conversion string
    Given the code inspection
    Then I replicate the string conversion code "name" into javascript compiler
    """
    var nombre = 15;
    nombre = (nombre.toString(2)*3).toString(2).substring(2,7);
    """
    Then the output of the javascript compiler is validated [evidences](02.png)
    When I fill the field with the "<FLAG>"
    Then A new alert appears with a message
    """
    Mot de passe correct ! Tu peux entrer ce mot pour valider l'énigme.
    """
    When I enter the "<FLAG>" into the challenge form
    And I press the "Valider" button
    Then I can see the message "Bravo" [evidences](03.png)
    And I capture the flag
