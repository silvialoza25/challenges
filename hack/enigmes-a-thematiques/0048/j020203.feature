## Version 2.0
## language: en

Feature: 48-H4CK1NG-enigmes-a-thematiques
  Code:
    48
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find the correct text

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/front/enigme/48
    """
    And I see a textfield that require a text

  Scenario: Success: Use developer tools to inspect the textfield
    Given The textfield requesting the password
    When I right click the textfield
    And I press the inspect option
    Then I can see the input tag
    And I know that the javascript code is in the head element
    When I expand the head element
    Then I can see the script element of type javascript
    When I expand the script element
    And I can see the password function [evidence](1.png)
    Then I can see the variable 'nombre'
    And that variable is compared to the text inside the textfield named 'ep48'
    And I copy the two first lines of the code
    And I open the following url that compiles javascript
    """
    https://paiza.io/es/projects/new
    """
    When I paste the two lines copied previously
    And I add the 'console.log()' function to print the variable 'nombre'
    And I execute the code
    Then I can see the decoded password required [evidence](2.png)
    When I fill the textfield with the correct text
    Then the web page displays a popup with the message [evidence](3.png)
    """
    Mot de passe correct, tu peux maintenant valider l'épreuve !
    """
    And I have completed the challenge
