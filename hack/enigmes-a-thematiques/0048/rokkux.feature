## Version 2.0
## language: en

Feature: 48-H4CK1NG-enigmes-a-thematiques
  Code:
    48
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Find and validate Flag

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    | Kali Linux      | 20.04 LTS     |
    | Firefox         | 88.0.1(64-bit)|
  Machine information:
    Given the challenge's flag validation page

  Scenario: Success: manipulate variable to generate password
    Given I inspect the web page
    And I find the function for password validation
    When I see the variable for password "<nombre>"
    Then I see the code for manipulating "<nombre>" [evidence](01.png)
    And I copy the code from "var nombre" to "(2,7);"
    When I go to a JavaScript online console
    """
    https://jsconsole.com/
    """
    And I paste the code I copied
    And I run the code
    Then I get the output, which is the "<flag>" [evidence](02.png)
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
