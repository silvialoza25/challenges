## Version 2.0
## language: en

Feature: 41-cryptographie-enigmes-a-thematiques
  Code:
    41
  Site:
    enigmes-a-thematiques
  Category:
    cryptography
  User:
    thecyberpunker
  Goal:
    Decode the string

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/41
    """
    Then I see a link to the code [evidence](1.png)
    """
    Décoder...
    """
    When I open the link
    And I see a type of encoding string [evidence](2.png)

  Scenario: Fail: Try to decode with UTF-8 decoder
    Given the string of the website
    When I try to identify the string
    And I realize about UTF-8 encoding
    When I go to decode the string
    And I can not decode [evidence](3.png)

  Scenario: Success: Try to decode with HTML Entity
    Given the string from the website
    And I realize about the type of string is HTML Entity
    Then I go to "Mothereff"
    """
    https://mothereff.in/html-entities
    """
    And I put the string to decode
    Then I decode the string [evidence](5.png)
    When I go to the website to validate the decoded string
    And I enter the decoded string
    And I got a message "Bravo thecyberpunker!"
    And I solve the challenge [evidence](5.png)
