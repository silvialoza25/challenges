## Version 2.0
## language: en

Feature: 994-H4CK1NG-enigmes-a-thematiques
  Code:
    994
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Obtain the flag

  Background:
  Hacker's software:
    |    <Software name>  |   <Version>   |
    |  Kali Linux         | 20.04 LTS     |
    |  Firefox            | 88.0.1(64-bit)|
  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/front/enigme/994
    """

  Scenario: Success: Inspect web page and change the input's attribute
    Given the provided webpage
    When I see a form and the message
    """
    Signez la pétition pour l'interdiction des épreuves de H4CK1NG sur EAT !
    Ah zut, mais où est passé ce bouton d'envoi ???
    """
    Then I inspect the web page to see its source code
    And I see the code for the form [evidence](01.png)
    And I see the tag for the input and its style attribute
    """
    style="visibility:hidden"
    """
    When I change the "hidden" value to "visible"
    Then the "Signer" or "Sign" button gets displayed [evidence](02.png)
    When I input "<anything>" in the "Nom" & "Prenom" fields [evidence](03.png)
    And I click on "Signer"
    Then I see the message [evidence](04.png)
    """
    Bravo <anything>, le mot de passe est <flag> !
    """
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
