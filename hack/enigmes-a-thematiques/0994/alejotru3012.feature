## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    hacking
  User:
    AlejandroTL (wechall)
  Goal:
    Sign the petition and get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given a form [evidence](01.png)

  Scenario: Fail: Fill the form and press enter
    Given the form
    When I fill fields
    And I try to sign it pressing enter
    Then nothing happens [evidence](02.png)
    And I can't solve the challenge

  Scenario: Success: Editing source code
    Given the form
    And I notice there is a phrase
    And it says "send" button is missing [evidence](03.png)
    When I check the source code
    Then I see the button properties
    """
    <input type="submit" name="test" value="Signer" style="visibility:hidden">
    """
    And I notice its style indicates visibility hidden
    When I remove
    """
    style="visibility:hidden"
    """
    Then the button appears [evidence](04.png)
    When I press the button
    Then I see a message which indicates a password [evidence](05.png)
    And I try to submit it as the flag
    And I solve the challenge [evidence](06.png)
