## Version 2.0
## language: en

Feature: 994-H4CK1NG-enigmes-a-thematiques
  Code:
    994
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find a way to discover what is hidden and obtain the flag

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/front/enigme/994
    """
    And I know that there is something hidden

  Scenario: Success: Reveal the hidden element
    Given I open the web page
    When I right click the textfield
    And I press the 'Inspect' button
    Then I can see the elements tab
    And I realize that the page has a hidden element [evidence](1.png)
    When I change the hidden element type value to 'show'
    Then I can see the hidden element [evidence](2.png)
    When I fill the two textields with random chars [evidence](3.png)
    And I press the 'Signer' button to submit the request
    Then the page shows a message with the flag [evidence](4.png)
    When I open the ctf page
    And I submit the answer
    Then the web page displays the message [evidence](5.png)
    """
    Bravo J!
    """
    And I have completed the challenge
