# elvish -compileonly alejotru3012.elv

use str

fn main []{
  counter = 0
  each [line]{
    if (str:contains $line "a") {
      counter = (+ $counter 1)
    }
  }
  echo $counter
}

main

# cat dico.txt | elvish alejotru3012.elv
