## Version 2.0
## language: en

Feature:
  Code:
    alejotru3012.elv
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    Geek
  User:
    AlejandroTL (wechall)
  Goal:
    Get the number of words with "a"

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
    | VS code         | 1.55.2      |
  Machine information:
    Given the dictionary "dico.txt"

  Scenario: Fail: Using VS code to count
    Given the dictionary
    When I open it in VS code
    And I press "ctrl + f" to search "a"
    Then I get the amount of "a" in the file [evidence](01.png)
    And I try to submit it as the flag
    And I can't solve the challenge

  Scenario: Success: Using a script to count
    Given the dictionary
    When I create an elvish script to count words with "a"
    And I execute it using
    """
    cat dico.txt | elvish alejotru3012.elv
    """
    Then I get a number [evidence](02.png)
    And I try to submit it as the answer
    And I solve the challenge [evidence](03.png)
