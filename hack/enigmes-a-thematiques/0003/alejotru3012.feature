## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    Geek
  User:
    AlejandroTL (wechall)
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given the web page https://enigmes-a-thematiques.fr/front/enigme/3

  Scenario: Fail: Checking cookies
    Given the web page
    When I open the inspector
    And I go to application tab
    And I select cookies
    Then I can't see any relevant [evidence](01.png)
    And I can't solve the challenge

  Scenario: Success: Checking source code
    Given the web page
    When I open the inspector
    And I check the source code
    Then I get a comment which indicates a number [evidence](02.png)
    And I try to submit it as the flag
    And I solve the challenge [evidence](03.png)
