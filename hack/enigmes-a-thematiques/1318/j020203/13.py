#!/bin/env python
import json
from urllib.request import urlopen

url = 'https://enigmes-a-thematiques.fr/back/src/public/stats/resolutions/'
url += '2011'
ranking2011 = json.loads(urlopen(url).read())['detail']

for i in range(len(ranking2011)):
    ans = f'{i+1}. {ranking2011[i]["eatien"]} {ranking2011[i]["nb"]}'
    if ranking2011[i]['eatien'] == 'Barbabulle':
        print('\033[;34m' + ans)
    elif i in [1,2,3]:
        print('\033[;32m' + ans)
    elif i == 49:
        print('\033[;33m' + ans)
