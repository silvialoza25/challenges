#!/bin/env python
import json
from operator import itemgetter
from tqdm import tqdm
from urllib.request import urlopen

statsResp = urlopen('https://enigmes-a-thematiques.fr/web/json/stats.json')
stats = json.loads(statsResp.read())
userProfile = 'https://enigmes-a-thematiques.fr/back/src/public/user/'

users2011 = []

print('This program will print the ranking of EAT in 2011\n')
print('\033[;33m' + 'Analizing authors:')

for users in tqdm(stats['stats_par_auteur'], desc='Users computed: '):
    userUrl = urlopen(userProfile + users['id'] + '/resolutions')
    userSols = json.loads(userUrl.read())
    numSols = 0
    for i in range(len(userSols)):
        if '2011' in userSols[i]['instant']:
            numSols += 1
    if numSols != 0:
        users2011.append({'User':users['username'], 'Score':numSols})

ranking = sorted(users2011, key=itemgetter('Score'), reverse=True)
print('\033[;37m' + 'User ranking 2011:\n')

for i in range(len(ranking)):
    ans = f'{i+1}. {ranking[i]["User"]} {ranking[i]["Score"]}'
    if ranking[i]['User'] == 'Barbabulle':
        print('\033[;34m' + ans)
    elif i in [1,2,3]:
        print('\033[;32m' + ans)
    else:
        print('\033[;37m' + ans)
