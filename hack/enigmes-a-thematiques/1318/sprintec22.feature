## Version 2.0
## language: en

Feature: 1318-H4CK1NG-enigmes-a-thematiques
  Code:
    1318
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    sprintec
  Goal:
    Find the name of the podium players and find the player in position 50th

  Background:
  Hacker's software:
    | <Software name> | <Version>          |
    | Kali Linux      | 2021.2             |
    | Firefox         | 79.7.0esr (64 bit) |
    | Excel           | 2019 MSO           |

  Machine information:
    Given I open the following challenge
    """
    https://enigmes-a-thematiques.fr/front/enigme/1318
    """
  Scenario: Fail: Gather information manually
    Given I looked the clue of the challenge 1318
    """
    During the year 2011, Barbabulle solved nearly 1000 puzzles! To be precis
    , he validated 999 ... Behind him we have a nice trio: three players who
    have solved more puzzles than anyone in that same year. It's up to you
    to find this podium!
    """
    When I went to the Authors section of the ETA statistics page
    """
    https://enigmes-a-thematiques.fr/front/statistiques
    """
    Then I realized that each user has a link [evidence](1.png)
    When I proceeded to click on each author's link
    Then I checked if the author has resolutions in 2011 [evidence](2.png)
    And I counted the solutions in 2011 [evidence](3.png) [evidence](4.png)
    Then I realized that there are 109 authors
    And I accounted everything in an excel sheet [evidence](5.png)
    Then I ordered decently according to the solutions 2011 [evidence](6.png)
    And I already have the 3 players behind Barbabulle in the excel sheet
    Then I already have the flag that is the name of these 3 players
    And I proceeded to validate the flag with these 3 names [evidence](7.png)
    When I enter the flag a new question came up
    """
    Now that you know the top4, can you tell me who is 50th in this ranking
    and their number of validations in 2011? Enter your answer in the form
    Barbabulle999.
    """
    Then I found the information of the player 50 with the excel data
    When I proceeded to enter the flag in the format Barbabulle999
    Then I realized that the answer was not correct

  Scenario: Success: Gather the information manually and manual brute force
    Given I looked the clue of the challenge 1318
    """
    During the year 2011, Barbabulle solved nearly 1000 puzzles! To be precis
    , he validated 999 ... Behind him we have a nice trio: three players who
    have solved more puzzles than anyone in that same year. It's up to you
    to find this podium!
    """
    When I went to the Authors section of the ETA statistics page
    """
    https://enigmes-a-thematiques.fr/front/statistiques
    """
    Then I realized that each user has a link [evidence](1.png)
    When I proceeded to click on each author's link
    Then I checked if the author has resolutions in 2011 [evidence](2.png)
    And I counted the solutions in 2011 [evidence](3.png) [evidence](4.png)
    Then I realized that there are 109 authors
    And I accounted everything in an excel sheet [evidence](5.png)
    Then I ordered decently according to the solutions 2011 [evidence](6.png)
    And I already have the 3 players behind Barbabulle in the excel sheet
    Then I already have the flag that is the name of these 3 players
    And I proceeded to validate the flag with these 3 names [evidence](7.png)
    When I enter the flag a new question came up
    """
    Now that you know the top4, can you tell me who is 50th in this ranking
    and their number of validations in 2011? Enter your answer in the form
    Barbabulle999.
    """
    Then I realized that it is the 50th position in the general classification
    And I realized that in ETA there isn't information of this classification
    When I performed a manual brute force attack on ETA using my excel players
    Then I got the flag thanks to small manual brute force [evidence](8.png)
