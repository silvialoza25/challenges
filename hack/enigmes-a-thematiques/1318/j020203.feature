## Version 2.0
## language: en

Feature: 1318-H4CK1NG-enigmes-a-thematiques
  Code:
    1318
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find the flag hidden in the website

  Background:
  Hacker's software:
    |  <Software name>  |        <Version>       |
    | Kali Linux        | 2021.2                 |
    | Chrome            | 91.0.4472.106 (64-bit) |
    | Python            | 3.9.2                  |
  Machine information:
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1318
    """
    And I can read the following text
    """
    Au cours de l'année 2011, Barbabulle a résolu près de 1000 énigmes !
    Pour être précis, il en a validé 999...
    Derrière lui, nous avons un beau trio : trois joueurs qui ont
    résolu plus d'énigmes que quiconque au cours de cette même année.
    À vous de retrouver ce podium !
    """

  Scenario: Fail: Look for a general ranking in 'Classements'
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1318
    """
    And I can read the following text
    """
    Au cours de l'année 2011, Barbabulle a résolu près de 1000 énigmes !
    Pour être précis, il en a validé 999...
    Derrière lui, nous avons un beau trio : trois joueurs qui ont
    résolu plus d'énigmes que quiconque au cours de cette même année.
    À vous de retrouver ce podium !
    """
    When I read the following part of the text
    """
    À vous de retrouver ce podium !
    """
    Then I know that I have to look for a global ranking
    When I click the 'Divers' button
    Then I can see the 'Classements' section
    When I enter each subsection inside
    Then I notice that none page loads anything [evidence](1.png)
    And I have not completed the challenge

  Scenario: Fail: Search the last solutions of 2011 in the Auteur's ranking
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1318
    """
    And I can read the following text
    """
    Au cours de l'année 2011, Barbabulle a résolu près de 1000 énigmes !
    Pour être précis, il en a validé 999...
    Derrière lui, nous avons un beau trio : trois joueurs qui ont
    résolu plus d'énigmes que quiconque au cours de cette même année.
    À vous de retrouver ce podium !
    """
    When I read the following part of the text
    """
    À vous de retrouver ce podium !
    """
    Then I know that I have to look for a global ranking
    When I click the 'Divers' button
    Then I can see the 'Statistiques' section [evidence](2.png)
    And I open that web page
    """
    https://enigmes-a-thematiques.fr/front/statistiques
    """
    When I click the 'Auteurs' tab
    Then I can see a ranking [evidence](3.png)
    When I click on the username
    Then I can see their profile page and their solutions
    When I open the solutions section
    Then I notice that is ordered by dates [evidence](4.png)
    When I open the following page
    """
    https://enigmes-a-thematiques.fr/front/statistiques
    """
    And I try to use the developer tools to extract the usernames
    Then I notice that there are not in the source code
    When I open the 'Network' tab
    And I filter the XHR files
    Then I can see the 'stats.json' file
    And the file is located on the following page [evidence](5.png)
    """
    https://enigmes-a-thematiques.fr/web/json/stats.json
    """
    When I open that page
    Then I can see the variable where the users are located
    And I can read the usernames
    And I can see their corresponding ID [evidence](6.png)
    When I open the 'Statistiques' web page
    And I open the link of a username to access their profile page
    And I expand the solutions section
    And I open the developers' tools
    And I open the 'Network' tab
    Then I can see the 'resolution' file
    And the file is located on the following page [evidence](7.png)
    """
    https://enigmes-a-thematiques.fr/back/src/public/user/<userID>/
    resolutions
    """
    When I read the first part of the page [evidence](8.png)
    Then I notice that is a JSON interpreted file that contains a list
    And the list contains the solutions and their corresponding date
    When I know what I have to do
    Then I decided to code a Python program with the previous conditions
    When I finish the program [evidence](9.py)
    Then I can get the three usernames
    And I put them together in the format flag requested
    """
    joueur1 joueur2 joueur3
    """
    When I fill the textfield with the flag on the ctf web page
    Then the web page displays the following text [evidence](10.png)
    """
    Maintenant que tu connais le top4, peux-tu me dire qui est 50e de ce
    classement et son nombre de validations en 2011 ? Entre ta réponse
    sous la forme Barbabulle999.
    """
    When I read that the real flag is the user in the fiftieth position
    And I can see that the format is the following one
    """
    <Username><Score>
    """
    Then I know that my program's output is the ranking list
    And I can see in the output their username and its score
    When I fill the textfield with the real flag on the ctf web page
    Then I can that is not accepted
    And I have not completed the challenge

  Scenario: Success: Use the solutions file of 2011 in the 'Résolutions' part
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1318
    """
    And I can read the following text
    """
    Au cours de l'année 2011, Barbabulle a résolu près de 1000 énigmes !
    Pour être précis, il en a validé 999...
    Derrière lui, nous avons un beau trio : trois joueurs qui ont
    résolu plus d'énigmes que quiconque au cours de cette même année.
    À vous de retrouver ce podium !
    """
    When I read the following part of the text
    """
    À vous de retrouver ce podium !
    """
    Then I know that I have to look for a global ranking
    When I click the 'Divers' button
    Then I can see the 'Statistiques' section [evidence](2.png)
    And I open that web page
    """
    https://enigmes-a-thematiques.fr/front/statistiques
    """
    And I expand the 'Résolutions' tab
    When I read that there is an annual solutions section [evidence](11.png)
    Then I think about where is this section pulling the information
    When I open the developers' tools
    And I open the 'Network' tab
    And I filter the XHR files
    Then I can see the '2021' file
    And the file is located on the following page [evidence](12.png)
    """
    https://enigmes-a-thematiques.fr/back/src/public/stats/resolutions/2021
    """
    When I change the URL to the following one
    """
    https://enigmes-a-thematiques.fr/back/src/public/stats/resolutions/2011
    """
    Then I can read the ranking of the 2011 year
    When I decided to code a Python program with the previous conditions
    And I finish the program [evidence](13.py)
    Then I execute it
    And I can see the values needed to complete the challenge
    When I fill the textfield with the real flag in the ctf web page
    Then the web page displays the following text [evidence](14.png)
    """
    Bravo J !
    """
    And I have completed the challenge
