## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    Geek
  User:
    AlejandroTL (wechall)
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given the web page
    """
    https://enigmes-a-thematiques.fr/web/enigmes/epreuve45/source.php
    """

  Scenario: Fail: Checking cookies
    Given the web page
    When I open the inspector
    And I go to application tab
    And I select cookies
    Then I can't see any relevant [evidence](01.png)
    And I can't solve the challenge

  Scenario: Fail: Searching for metadata
    Given the page
    And I download the background as image
    When I check it for something useful in metadata with
    """
    exiftool matrix.jpg
    """
    Then I didn't find anything [evidence](02.png)

  Scenario: Success: Checking source
    Given the web page
    When I open the inspector
    And I check the source
    And I open "matrix2.css" style sheet
    Then I get a comment that indicates a password [evidence](03.png)
    And I try to submit it as the flag
    And I solve the challenge [evidence](04.png)
