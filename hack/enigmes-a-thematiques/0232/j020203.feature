## Version 2.0
## language: en

Feature: 232-H4CK1NG-enigmes-a-thematiques
  Code:
    232
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find a way to obtain the flag showing what is hidden

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep232.php
    """
    And I see the requirements to obtain the flag

  Scenario: fail: Change the value of rep element to answer
    Given I open the web page
    When I remove the value of the textfield [evidence](1.png)
    And I fill the textfield with the answer of the equation above
    And I press the 'Valider' button to submit the request
    Then the page shows the following message [evidence](2.png)
    """
    En effet.
    """
    And I have not completed the challenge

  Scenario: Success: Change the value of the hidden element to the answer
    Given I open the web page
    When I press the 'F12' button to display the developer tools
    Then I can see in the elements tab that the page has a hidden element
    When I change the hidden element type value to show
    Then I can see the hidden element [evidence](3.png)
    And I know its name is 'answer'
    When I remove the value of the answer element
    And I fill it with the answer of the equation above [evidence](4.png)
    And I press the 'Valider' button to submit the request
    Then the page shows a message with the flag [evidence](5.png)
    When I open the ctf page
    And I submit the answer
    Then the web page displays the message [evidence](6.png)
    """
    Bravo J!
    """
    And I have completed the challenge
