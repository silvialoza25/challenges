## Version 2.0
## language: en

Feature: 232-H4CK1NG-enigmes-a-thematiques
  Code:
    232
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Obtain the flag

  Background:
  Hacker's software:
    |    <Software name>  |   <Version>   |
    |  Kali Linux         | 20.04 LTS     |
    |  Firefox            | 88.0.1(64-bit)|
  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep232.php
    """

  Scenario: Fail: Send password via Authentication button
    Given the provided webpage
    And I see the text box with the number "123" and the message
    """
    Combien font 6+3*8-13 ?
    """
    When I write the answer, "17"
    And I click on "Authentification" button to send
    Then I see the message "En effet" [evidence](01.png)
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Success: Inspect web page and change the hidden input attribute
    Given the provided webpage
    When I inspect the web page in the browser
    Then I see its source code
    And I see the code for input and its attributes
    And I see the forms' HIDDEN input attribute value [evidence](02.png)
    """
    response
    """
    When I change it to "17"
    And click the "Valider" button
    Then I see the message [evidence](03.png)
    """
    En effet.Le mot de passe est <flag>.
    """
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
