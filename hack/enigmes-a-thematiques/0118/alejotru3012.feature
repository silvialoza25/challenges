## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    cryptography
  User:
    AlejandroTL (wechall)
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given an image [evidence](01.png)
    """
    01101100 01100101 00100000 01101101 01101111 01110100 00100000 01100100
    01100101 00100000 01110000 01100001 01110011 01110011 01100101 00100000
    01100101 01110011 01110100 00100000 01100010 01101001 01101110 01100001
    01110010 01111001 00110011
    """

  Scenario: Fail: Decoding from base64
    Given the message
    When I access to http://icyberchef.com/
    And I put the message as input
    And I use "From Base64" operation
    Then I can't decrypt the message [evidence](01.png)
    And I can't solve the challenge

  Scenario: Success: Decoding from Binary
    Given the message
    When I access to http://icyberchef.com/
    And I put the message as input
    And I use "From Binary" operation
    Then I get a message which indicates a password [evidence](02.png)
    And I try to submit it as the flag
    And I solve the challenge [evidence](03.png)
