## Version 2.0
## language: en

Feature: 1451-H4CK1NG-enigmes-a-thematiques
  Code:
    1451
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    Mankar
  Goal:
    Find the correct input string for a program written in Whitespace language

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu          | 20.04.2 LTS   |
    | Google Chrome   | 91.0.4472.114 |
  Machine information:
    Given I am accessing the website
    And the website shows a description of the Whitespace programming language

  Scenario: Fail: Inspect webpage source code and execute challenge program
    Given I use the Google Translate browser feature
    When I read the English translation of the challenge
    Then I find no hint for solving it
    When I inspect the webpage source code
    Then I find nothing of relevance
    When I do a little research about the Whitespace programming language
    Then I find that it is composed only of tabs, spaces, and newlines
    And I realize the translation must be discarding a Whitespace program
    When I disable automatic translation
    And I check again the source code
    Then I find that the text is full of extra whitespace characters
    And I deduce that they correspond to a program [evidence](01.png)
    When I do a Google search to find a tool that translates this program
    Then I find a website called "dcode.fr"
    When I insert the Whitespace code on this website
    Then it asks for an input
    When I press random keys and send them as an answer
    Then the left panel of the website echoes the same input
    And it prints the word "Bad" [evidence](02.png)
    And I deduce that there must be an input that produces another message

  Scenario: Fail: Try to find an online tool that translates the program
    Given I have retrieved the Whitespace program from the challenge
    When I do a Google search to find more tools that help me understand it
    Then I find a GitHub repository that collects a lot of projects
    But most of them are compilers of the language, not transpilers
    When I examine the tools that are referenced
    Then I select one that transpiles a Whitespace program to Assembly
    """
      https://github.com/kmyk/whitespace-translater
    """
    And it is composed of an HTML and a Javascript code
    When I open it with my web browser [evidence](03.png)
    Then I notice it only transforms from Whitespace to Assembly
    When I do a little more research
    Then I realize I won't find a better tool
    And I decide to debug the Assembly code generated from this tool

  Scenario: Success: Manually debug Assembly program to catch the flag
    Given I have converted the original program to Assembly [evidence](04.asm)
    And I have manually checked that the transpilation is correct
    When I analyze the program
    Then I start searching for the block that prints the word "Bad"
    And I find a subroutine that is called multiple times
    When I look for a similar code that prints another output
    Then I find a subroutine that prints the word "Good"
    And I deduce it is printed when the input is correct
    When I analyze the program further
    Then I notice that every input character is saved in a heap
    And the program checks them one at a time before reaching the "Good" block
    When I check what characters the program expects
    Then I take note of them
    When I execute the Whitespace program again on the dcode.fr website
    And I enter the found string
    Then the left panel prints "Good" [evidence](05.png)
    When I enter the string to the challenge webpage
    Then a congratulations message appears [evidence](06.png)
    And I caught the flag
