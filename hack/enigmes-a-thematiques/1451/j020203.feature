## Version 2.0
## language: en

Feature: 1451-H4CK1NG-enigmes-a-thematiques
  Code:
    1451
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find the flag hidden in the hidden code

  Background:
  Hacker's software:
    |  <Software name>  |        <Version>       |
    | Kali Linux        | 2021.2                 |
    | Chrome            | 91.0.4472.106 (64-bit) |
    | Whitespace        | 1.0.0b8                |
  Machine information:
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1451
    """
    And I can read the following text
    """
    Le whitespace est un langage de programmation exotique inventé le 1er
    avril 2003 par Edwin Brady et Chris Morris. Comme caractères, il
    utilise les espaces, les tabulations et les retours à la ligne pour
    générer un programme dont le code est invisible. Il s'agit, bien sûr,
    plus d'une plaisanterie de potaches informaticiens que d'un véritable
    langage de programmation opérationnel. Ce pseudo-langage, basé sur une
    pile, un peu comme le Forth (empiler et dépiler), est limité à quelques
    opérateurs simplistes et le minimum vital concernant les entrées/sorties.
    Il est très facile de mettre en place un interpréteur Whitespace.
    Même s'il s'agit d'une plaisanterie, l'étude du code peut se révéler
    profitable pour ceux ou celles qui se lanceraient dans l'écriture d'un
    interpréteur NPI (notation polonaise inversée) ou RPI à la Forth. Perl
    proposait dès 2001 le module Acme::Bleach pour programmer avec des
    espaces.
    """

  Scenario: Success: Use a debugger to inspect the code
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1318
    """
    And I can read the following text
    """
    Au cours de l'année 2011, Barbabulle a résolu près de 1000 énigmes !
    Pour être précis, il en a validé 999...
    Derrière lui, nous avons un beau trio : trois joueurs qui ont
    résolu plus d'énigmes que quiconque au cours de cette même année.
    À vous de retrouver ce podium !
    """
    When I open the developers' tools
    And I open the 'Network' tab
    And I filter the results to get just the XHR files
    Then I can see a file named 'body'
    When I open that file
    Then I can see the previous text
    And I notice that the end has special chars [evidence](1.png)
    When I open the URL that contains that file
    Then I can see it better redacted [evidence](2.png)
    When I copy the value of the result variable in the body file
    Then I search for a compiler
    And I find it on the following page [evidence](3.png)
    """
    https://pypi.org/project/whitespace/
    """
    When I paste it into a file named
    """
    whitespace.sw
    """
    Then I use the whitespace compiler with the following command
    """
    whitespace whitespace.sw
    """
    And I can see that the program is waiting for an input
    When I enter a random input
    Then I can see that it is a program that verifies the input
    And I get the 'Bad' error [evidence](4.png)
    When I think about doing reverse engineering
    Then I search for a debugger
    And I find it on the following page [evidence](5.png)
    """
    http://vii5ard.github.io/whitespace/
    """
    When I paste the code on the debugger page
    Then I can analyze the program behavior
    When I enter the following input [evidence](6.png)
    """
    abcd
    """
    And I select the 'Memory' tab
    Then I can see the stack and heap
    And I analyze the behavior using the step option [evidence](7.png)
    When I notice that the first input pass through the stack
    And the program cleans the stack
    And the final stack is stored in the heap [evidence](8.png)
    Then I realize that after starting the verification part
    When I read the first verification [evidence](9.png)
    Then I notice that the two last values are subtracted from each other
    And the stack removes the two last values
    And the stack appends the result of the subtraction [evidence](10.png)
    When the second verification finish
    Then the program jumps to the label_1 function [evidence](11.png)
    And the program ends with the 'Bad' error
    And I realize that the program just verify the two first characters
    And the program ignores everything else after the second character
    When I know that the characters are being processed as numbers
    Then I can think it is possible that are in ASCII code
    When I read part of the debug window
    And I know that the fail section is in the label_1
    Then I grab the numbers
    And I compare it with the ASCII codes located on the following page
    """
    https://ascii.cl/es/
    """
    When I convert the ASCII codes to text
    Then I can see that every number corresponds to the input in ASCII code
    """
    100     d
    97      a
    66      B
    """
    And I can read it contains the error message reversed
    """
    Bad
    """
    When I think about the substracting part of the verification
    Then I realize that the program compares the two values
    And the program validates through the subtraction between the numbers
    And the program checks the result
    And the result must be zero to return True
    When I try the same previous input
    Then I take note of the two codes that are compared with the input
    And I use the table to take the symbol of the ASCII code value
    When I use the compiler whitespace
    And I enter the correct input
    Then I can see the following output [evidence](12.png)
    """
    Good
    """
    When I fill the textfield with the flag in the ctf web page
    Then the web page displays the following text [evidence](13.png)
    """
    Bravo J !
    """
    And I have completed the challenge
