## Version 2.0
## language: en

Feature: 26-H4CK1NG-enigmes-a-thematiques
  Code:
    26
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find the admin password to access the authentication page

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/web/enigmes/epreuve26/admin.php?password=f
    alse
    """

  Scenario: Success: Change value of password in URL
    Given I open the corresponding web page
    And I see a textfield that require a password
    And I see the false value in the URL [evidence](1.png)
    When I change that false value of the password parameter to true
    And I reload the web page
    Then the web page change showing "Accès autorisé !"
    And the web page shows the admin password [evidence](2.png)
    When I paste the password in the textfield of the challenge page
    Then the page displays the message "Tu as déjà résoli cette énigme !"
    When I open the ctf page
    And I submit the flag
    Then the web page displays the following message
    """
    Bravo J!
    """
    And I have completed the challenge
