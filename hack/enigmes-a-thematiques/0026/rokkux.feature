## Version 2.0
## language: en

Feature: 26-H4CK1NG-enigmes-a-thematiques
  Code:
    26
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Find the admin password

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    | Kali Linux      | 20.04 LTS     |
    | Firefox         | 88.0.1(64-bit)|
  Machine information:
    Given the web page
    """
    https://enigmes-a-thematiques.fr/web/enigmes/epreuve26/
    admin.php?password=false
    """

  Scenario: Success: check web page's source code
    Given I access the provided web page
    When I see a "Box" requesting a password
    And a button with the message "Authentification"
    And the message "Accès refusé !"
    Then I inspect the page
    When I check the source code
    And I read the BODY tag
    Then I see the form action for authentication "<code>"
    And I see the page's URL has this same text [evidence](01.png)
    When I change the boolean value inside "<code>"
    And I reload the page with the last change applied
    Then I get the message [evidence](02.png)
    """
    Accès autorisé ! Le mot de passe de l'admin est : <pass>
    """
    When I return to the challenge's page
    And input the obtained password for validation
    Then the page displays the message "Tu as déjà résoli cette énigme !"
    And I solved the challenge
    And I captured the flag
