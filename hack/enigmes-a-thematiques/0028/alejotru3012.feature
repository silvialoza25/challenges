## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    cryptography
  User:
    AlejandroTL (wechall)
  Goal:
    Sign the petition and get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given the message
    """
    45°34'50"N
    1°32'39"E
    """

  Scenario: Fail: Trying magic operation in Cyberchef
    Given the message
    When I access to http://icyberchef.com/
    And I put the message as input
    And I use "Magic operation"
    Then I can't decrypt the message [evidence](01.png)
    And I can't solve the challenge

  Scenario: Success: Coordinates
    Given the message
    And I notice it looks like coordinates
    When I put them in google maps
    Then I get a place [evidence](02.png)
    And I try to submit it as the flag
    And I solve the challenge [evidence](03.png)
