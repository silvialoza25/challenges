## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    Cryptography
  User:
    AlejandroTL (wechall)
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given a message
    """
    pa rzo ta nfgga ago fppvdfozh
    """

  Scenario: Fail: Using the magic operation
    Given the message
    When I access to http://icyberchef.com/
    And I put the message as input
    And I use "Magic" operation
    Then I can't decrypt the message [evidence](01.png)
    And I can't solve the challenge

  Scenario: Success: Adjusting substitution
    Given the message
    And I know solutions in this page use to start with
    """
    LE MOT DE PASSE EST
    """
    When I access to https://www.dcode.fr/monoalphabetic-substitution
    And I put the message as input
    And I use "ATTACK WITH PROBABLE WORD/KNOWN PLAIN TEXT" option
    And I use the known start message as a parameter
    Then I get [evidence](02.png)
    When I access to Google translate
    And I try to get something coherent and readable [evidence](03.png)
    And I use it to adjust the rest of the substitution
    Then I get a message which indicates a password [evidence](04.png)
    And I try to submit it as the flag
    And I solve the challenge [evidence](05.png)
