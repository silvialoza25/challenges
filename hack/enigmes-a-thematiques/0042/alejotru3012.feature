## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    Geek
  User:
    AlejandroTL (wechall)
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given the message [evidence](01.png)

  Scenario: Fail: From decimal
    Given the message
    When I access to http://icyberchef.com/
    And I put the message as input
    And I use "From Decimal" option
    Then I can't decrypt the message [evidence](02.png)
    And I can't solve the challenge

  Scenario: Fail: ASCII to text
    Given the message
    When I access to http://www.unit-conversion.info/texttools/ascii/
    And I put the message as input
    And I use "ASCII numbers to text" option
    Then I can't decrypt the message [evidence](03.png)
    And I can't solve the challenge

  Scenario: Success: From HTML Entity
    Given the message
    When I access to http://icyberchef.com/
    And I put the message as input
    And I use "From HTML Entity"
    Then I get a message that indicates a password [evidence](04.png)
    And I try to submit it as the flag
    And I solve the challenge [evidence](05.png)
