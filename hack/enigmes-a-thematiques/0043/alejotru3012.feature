## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    cryptography
  User:
    AlejandroTL (wechall)
  Goal:
    Sign the petition and get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given an encrypted message
    """
    .-.. . / -- --- - / -.. . / .--. .- ... ... . / . ... - / ... --
    - ..- ... / ...- --- ... / -.-- . ..- -..- .-.-.-
    """

  Scenario: Fail: Trying magic operation in Cyberchef
    Given a message
    When I access to http://icyberchef.com/
    And I put the message as input
    And I use "Magic operation"
    Then I can't decrypt the message [evidence](01.png)
    And I can't solve the challenge

  Scenario: Success: Morse code
    Given the message
    And I notice it looks like morse code
    When I access to https://morsedecoder.com/
    And I put the message to decode it
    Then I get a message which indicates a password [evidence](02.png)
    And I try to submit it as the flag
    And I solve the challenge [evidence](03.png)
