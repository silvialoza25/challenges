## Version 2.0
## language: en

Feature: 80-G33K-enigmes-a-thematiques
  Code:
    80
  Site:
    enigmes-a-thematiques
  Category:
    G33K
  User:
    thecyberpunker
  Goal:
    Get the right answer

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/80
    """
    And I see a French message [evidence](1.png)
    When I translate the message
    Then I realize about a lot of information
    """
    programming language, pascal, likes, hobbies, email
    """

  Scenario: Fail: Try to find the answer
    Given the information of the article
    When I check the source code [evidence](2.png)
    And I got nothing relevant

  Scenario: Fail: Reach the email
    Given the message information
    When I read it again
    Then I realize about to try to send an email
    When I send the email to "geekarobaseentouteslettres@gmail.com"
    And I got a return email with some information [evidence](3.png)
    And I got a curious message
    """
    Le mot de passe est la fois une ville de Sicile et une conjecture célèbre
    """
    And I realize about to find a password related to a city and a conjecture
    When I start to search cities related to Sicile on Wikipedia
    And I found some cities with the indication [evidence](4.png)
    And I try to enter the cities as passwords
    And I got bad responses [evidence](5.png)

  Scenario: Success: Try to get the correct city
    Given the information from the email
    When I search for Sicile cities related to conjectures
    And I got one city related to a Mathematical problem [evidence](6.png)
    """
    The <FLAG> conjecture in mathematics
    """
    Then I go to validate that city as an answer
    And I got a message "Bravo thecyberpunker!"
    And I solve the challenge [evidence](7.png)
