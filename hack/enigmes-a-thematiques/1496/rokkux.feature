## Version 2.0
## language: en

Feature: 1496-H4CK1NG-enigmes-a-thematiques
  Code:
    1496
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Obtain the flag

  Background:
  Hacker's software:
    |         <Software name>       |     <Version>     |
    |  Kali Linux                   | 20.04 LTS         |
    |  Firefox                      | 88.0.1(64-bit)    |

  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/front/enigme/1496
    """

  Scenario: Fail: Inspect the web page and input combinations of words as flags
    Given the web page
    When I load it
    Then I see there is a form asking for Username and Passsword
    And I see there is also a button for validation
    When I inspect the web page
    Then I see that there is a function for validating the inputted info
    When I check this function [evidence](01.png)
    Then I realize the correct info to be inputted is
    """
    Username: Julien
    Password: Ville
    """
    When I click on "Check!" [evidence](02.png)
    Then I get the message "Bien joue, fais le lien entre les deux maintenant"
    When I translate it
    Then I realize it means
    """
    Well played, make the connection between the two now
    """
    And I see that the page says that the flag has 15 characters
    When I google "Julien" and "Ville"
    Then I get results like "Ville de Saint Julien"
    When I input one by one the following as flags
    """
    Ville de Saint Julien, Ville de st Julien, le ville de julien,
    la ville du julien
    """
    And I click for validation
    Then Page displays the message "n'est pas la bonne réponse !"
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Success: Check for possible flags inside enigmes web page
    Given the previous instruction "fais le lien entre les deux maintenant"
    When I see that the name of the author of the page is "Julien"
    And on the lower side of the page his name is displayed
    And his name is a link
    And I click it
    Then I see the Julien's personal web page [evidence](03.png)
    And I realize "Julien" is the author
    And "Ville" must be the city where he lives
    When I go to the CONTACT page
    Then I see there is no information about the city
    When I got to the CV page
    Then I see the city "<flag>" [evidence](04.png)
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then the page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
