## Version 2.0
## language: en

Feature: 1496-H4CK1NG-enigmes-a-thematiques
  Code:
    1496
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find the way and obtain the flag

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1496
    """

  Scenario: Fail: Wrong understanding about 'link' the values
    Given I open the web page
    And I can see two textfields that require a username and a password
    When I right click the 'check' button
    And I press the 'Inspect' button
    Then I can see the elements tab
    And I realize that the button has a javascript code [evidence](1.png)
    When I copy the values that are compared to the input
    """
    "julien"
    "ville"
    """
    And I fill the corresponding textfields with the values [evidence](2.png)
    And I click the 'check' button
    Then the page displays a popup with the message
    """
    Bien joue, fais le lien entre les deux maintenant!
    """
    When I read the message displayed
    Then I think I have to make the values a 'link'
    And I try to use the values in different ways
    And I add at the end the '.com' extension [evidence](3.png)
    When I see that did not work
    Then I try to combine the values in different ways [evidence](4.png)
    When I notice that did not work either
    Then I search the previous values in google
    And I add the context of the title [evidence](5.png)
    """
    France
    """
    When I realize that is an actual place located in France
    And I see the results I can understand the 'ville' part [evidence](6.png)
    Then I know that 'ville' means 'village' or 'city'
    When I see the Saint-Julie Part [evidence](7.png)
    Then I know what could be the flag
    When I open the ctf page
    And I submit the possible answers
    Then the web page displays the message [evidence](8.png)
    And I have not completed the challenge

  Scenario: Success: Find the relation between the name and the author
    Given I open the web page
    And I can see two textfields that require a username and a password
    And I know the flag reffers to a place in France
    When I right click the 'check' button
    And I press the 'Inspect' button
    Then I can see the elements tab
    And I realize that the button has a javascript code [evidence](1.png)
    When I copy the values that are compared to the input
    """
    "julien"
    "ville"
    """
    And I fill the corresponding textfields with the values [evidence](2.png)
    And I click the 'check' button
    Then the page displays a popup with the message
    """
    Bien joue, fais le lien entre les deux maintenant!
    """
    When I search the previous values in google
    And I add the context of the title [evidence](5.png)
    """
    France
    """
    Then I can see that the 'ville' part refers to a city [evidence](6.png)
    When I open the development tools in the ctf challenge website
    And I search the word 'julien' in the elements tab
    Then I can see the function that validate the textfields
    And I can see at the end of the web page the name of the creator
    When I realize that the name of the creator of the challenge is 'Julien'
    And I can see that the name has a link to a web page [evidence](9.png)
    Then I understand that 'ville' refers to the city where Julien is from
    When I open the web page that the name contains
    """
    http://julien.marmin.free.fr/
    """
    And I start to explore the site
    And I know I have to look where I can find personal information
    Then I can see three pages that may have any personal information
    """
    ACCUEIL
    CV
    CONTACT
    """
    When I notice that the CV corresponds to the curriculum
    And I know that in that type of documents there is a lot of information
    Then I decide to open the CV web page
    """
    http://julien.marmin.free.fr/cv.php
    """
    When I see the header of the CV
    Then I can see an address above the email [evidence](10.png)
    And I know that the flag has 15 letters [evidence](11.png)
    When I know what could be the flag
    Then I open the ctf page
    And I submit the answer
    When the web page displays the following message [evidence](12.png)
    """
    Bravo J!
    """
    Then I have completed the challenge
