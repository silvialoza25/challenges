## Version 2.0
## language: en

Feature: 32-H4CK1NG-enigmes-a-thematiques
  Code:
    32
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find the password of the executable

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the url where I can find the code of the executable
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve32/executable_lvl1
    """
    And I do not know the password

  Scenario: Succes: Look for printable strings in the executable
    Given I open the linux terminal
    And I use the following command to download the executable
    """
    wget https://enigmes-a-thematiques.fr/epreuves/epreuve32/executable_lvl1
    """
    And I use the following command to make the file an executable
    """
    chmod +x executable_lvl1
    """
    When I use the following command
    And I can identify the type of file is the executable [evidence](1.png)
    """
    file executable_lvl1
    """
    And I know the executable's type is due to the following output
    """
    executable_lvl1: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV),
    dynamically linked, interpreter /lib/ld-linux.so.2, stripped
    """
    Then I notice that the executable is 32-bit
    And I decide to see the strings of the executable before run it
    When I use the following command to see the strings in the program
    """
    strings executable_lvl1
    """
    Then I can read some words that are in French
    And I translate the words to English
    When I read the following text
    """
    This is the correct password! To validate the test, you just have to enter
    this as a password.
    ________________________________________________________
    Welcome to the 'Executable lvl 1' test ...
    ________________________________________________________
    Password :
    ####
    This is the wrong password!
    """
    Then I know what is the password required [evidence](2.png)
    When I open the ctf page
    And I submit the answer
    Then the web page displays the message [evidence](3.png)
    """
    Bravo J!
    """
    And I have completed the challenge
