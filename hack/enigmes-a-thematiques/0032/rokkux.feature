## Version 2.0
## language: en

Feature: 32-H4CK1NG-enigmes-a-thematiques
  Code:
    32
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Find the flag

  Background:
  Hacker's software:
    |   <Software name>  |   <Version>   |
    | Kali Linux         | 20.04 LTS     |
    | Firefox            | 88.0.1(64-bit)|
    | Visual Studio Code | 1.55.2        |
  Machine information:
    Given the downloaded file
    """
    executable_lvl1
    """

  Scenario: Fail: Open the executable with Visual Studio Code
    Given I downloaded the file in my computer
    When I open it with Visual Studio Code
    Then I see the file is hard to read and comprehend [evidence](01.png)
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Success: Use Kali Linux's "string" command
    Given I downloaded the file in my computer
    When I use "strings" command on the file inside the terminal
    Then I get the text inside the file
    And I see the text containing the flag [evidence](02.png)
    """
    ________________________________________________________
        Bienvenue sur l'epreuve 'Executable lvl 1'...
    ________________________________________________________
        Mot de passe :
    <flag>
    """
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
