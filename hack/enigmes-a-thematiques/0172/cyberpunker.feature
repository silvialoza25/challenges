## Version 2.0
## language: en

Feature: 172-cryptographie-enigmes-a-thematiques
  Code:
    172
  Site:
    enigmes-a-thematiques
  Category:
    cryptographie
  User:
    thecyberpunker
  Goal:
    Get the IPv6

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |
    | Nslookup        | 1.1.1         |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/172
    """
    And I see a Latin text [evidence](1.png)
    When I translate the text
    And I do not get something relevant [evidence](2.png)

  Scenario: Success: Try to get more information
    Given the information of the text
    When I search part of the text on Google
    And I got information about the text
    Then I realize about it is a ciphertext [evidence](3.png)
    """
    les ave maria de tritheme
    """

  Scenario: Fail: Decrypt the text
    Given the information about the type of encryption
    When I search in "dcode.fr"
    And I got a decoder tool [evidence](4.png)
    When I try to decrypt the text
    And I got a possible flag [evidence](5.png)
    When I try to enter the flag as an answer
    And I got a bad response [evidence](6.png)

  Scenario: Success: Try to understand the decrypted text
    Given the information from the decrypted text
    And I realize about the last part of the decrypted text
    Then I go to validate that part of the text as an answer
    And I got a message "Bravo thecyberpunker!"
    And I solve the challenge [evidence](7.png)
