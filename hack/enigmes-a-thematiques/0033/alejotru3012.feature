## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    Geek
  User:
    AlejandroTL (wechall)
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given the message
    """
    £3 |\/|07 Ð3 p4$$3 Ð3 (3773 3p®3µ\/3 3$7 4£p|-|4ß374.
    """

  Scenario: Fail: Using Cyberchef
    Given the message
    When I access to http://icyberchef.com/
    And I put the message as input
    And I try different operations like "Magic", "XOR" or "ROT47"
    Then I can't decrypt the message [evidence](01.png)
    And I can't solve the challenge

  Scenario: Success: Analyzing symbols
    Given the message
    And I notice its symbols could be representing letters
    When I open Google translate
    And I use it to get a coherent message from those symbols
    Then I get a phrase which indicates a password [evidence](02.png)
    And I try to submit it as the flag
    And I solve the challenge [evidence](03.png)
