## Version 2.0
## language: en

Feature: 1426-H4CK1NG-enigmes-a-thematiques
  Code:
    1426
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    Mankar
  Goal:
    Find the combination to open a safe box based on Javascript code

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu          | 20.04.2 LTS   |
    | Google Chrome   | 91.0.4472.114 |
    | Python          | 3.8.5         |
  Machine information:
    Given I am accessing the website
    And the website shows a picture of a safe box
    And the website shows ten buttons labeled from 0 to 9

  Scenario: Fail: Try the solution used on the previous challenge
    Given I have solved two versions of this challenge
    When I inspect the webpage using browser Dev Tools [evidence](01.png)
    Then I find there's a Javascript code that validates the combination
    And that every button triggers a function
    And the function parameters change the value of a variable with every click
    And I realize its behavior is equal to the previous challenges
    When I run the Python program I used to solve the previous challenges
    Then the program doesn't find any solution
    When I inspect the Javascript code further
    Then I notice the solution must have a maximum length of 8 digits
    And I conclude that not all of the ten buttons have to be pressed

  Scenario: Fail: Update the program to find the combination
    Given I have found the formula that describes the challenge
    And I also have found the length of the solution
    When I modify the program used in previous challenges
    And I fix it so it searches for a combination with a length equal to 8
    And I execute it
    Then it doesn't find any solutions
    When I remember that not all buttons have to be pressed
    Then I suspect it may be allowed to press any button multiple times
    When I update the program removing this restriction [evidence](02.py)
    And I execute it
    Then it finds a correct combination with the specified length
    When I map the obtained sequence to the buttons of the safe box
    And I click the buttons in that order
    Then the safe box shows an authorization message
    And an image of an open safe appears [evidence](03.png)
    When I enter the sequence to the solution field
    Then an indication appears [evidence](04.png)
    """
      What is the 'password' corresponding to the code opening the safe?
    """
    And I realize I still need to transform the numbers into the flag

  Scenario: Success: Try different approaches to transform the combination
    Given I have found the combination that opens the safe box
    When I try to map the numbers to letters of the alphabet
    Then I obtain a random word
    And it isn't the flag
    When I modify the program to find a new sequence that yields that number
    Then it doesn't return any sequence
    When I try reversing the combination and executing the program again
    Then it doesn't return any sequence
    When I stare at the numbers of the combination and the safe box buttons
    Then I realize that these numbers can be seen as upside-down letters
    And I immediately can read a (French) word from that perspective
    When I enter this word into the solution field
    Then a congratulations message appears [evidence](05.png)
    And I conclude this word is the flag that solves the challenge
