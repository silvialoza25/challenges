## Version 2.0
## language: en

Feature: 1426-H4CK1NG-enigmes-a-thematiques
  Code:
    1426
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    sprintec
  Goal:
    Discover the box code and password

  Background:
  Hacker's software:
    | <Software name> | <Version>           |
    | Kali Linux      | 2021.2              |
    | Firefox         | 78.10.0esr (64-bit) |
    | John the Ripper | 1.9.0-jumbo-1       |
    | Python          | 3.9.2               |
  Machine information:
    Given I open the following challenge
    """
    https://enigmes-a-thematiques.fr/front/enigme/1426
    """
  Scenario: Fail: Brute force with python and crack with johntheripper
    Given the JavaScript code of the challenge
    When I went to the html page of the iframe
    """
    https://enigmes-a-thematiques.fr/web/enigmes/epreuve994/index_v3.php
    """
    Then I read the function "code(str)" to understand the verification
    And I did a brute force script to retrieve the code [evidence](01.py)
    Then I managed to discover the code [evidence](02.png)
    When I entered it this code a new question came up [evidence](03.png)
    """
    What is the 'password' corresponding to the code opening the safe?
    """
    Then I thought the code was a hash
    When I looked for what kind of hash it was [evidence](04.png)
    Then I thought the code was a CRC-32 hash
    When I proceeded to use johntheripper to crack the hash [evidence](05.png)
    Then I found out that the password was "m0hrh1n"
    And I entered as a flag but it was not correct

  Scenario: Success: Brute force with python and intuition
    Given the JavaScript code of the challenge
    When I went to the html page of the iframe
    """
    https://enigmes-a-thematiques.fr/web/enigmes/epreuve994/index_v3.php
    """
    Then I read the function "code(str)" to understand the verification
    And I did a brute force script to retrieve the code [evidence](01.py)
    Then I managed to discover the code [evidence](02.png)
    When I entered it this code a new question came up [evidence](03.png)
    """
    What is the 'password' corresponding to the code opening the safe?
    """
    Then I realized that the code was not a hash
    And I looked for various ciphers on the internet
    Then I realized that this code was not about any encryption
    When I tried to match the code numbers with the letters
    Then I discovered that it was a word with inverted numbers
    And I got the flag back [evidence](06.png)
