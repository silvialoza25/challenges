## Version 2.0
## language: en

Feature: 153-H4CK1NG-enigmes-a-thematiques
  Code:
    153
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Decipher the hash

  Background:
  Hacker's software:
    |    <Software name>  |   <Version>   |
    |  Kali Linux         | 20.04 LTS     |
    |  Firefox            | 88.0.1(64-bit)|
    |  John the Ripper    | 1.9.0-jumbo-1 |
  Machine information:
    Given the text
    """
    40FD6820DE5FD55BAAD3B435B51404EE
    """

  Scenario: Fail: Decipher as MD5 Hash with online tools
    Given the provided text
    When I analyse the hash in
    """
    https://www.tunnelsup.com/hash-analyzer/
    """
    Then I see that the hash type is MD5 [evidence](01.png)
    When I try to decipher it with
    """
    https://www.md5online.org/md5-decrypt.html
    """
    Then I get the message [evidence](02.png)
    """
    No result found in our database
    """
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Fail: Decipher as MD2 Hash
    Given the provided text
    When I analyse the hash with hashid
    Then I see the various types of hash the text can be [evidence](03.png)
    When I try to decipher it as MD2 with
    """
    icyberchef.com
    """
    Then I get the output "<outp>" [evidence](04.png)
    When I go back to the challenge's page
    And I type the "<outp>" into the validation box and click for validation
    Then Page displays the message "<outp> n'est pas la bonne réponse !"
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Fail: Decipher as MD4 Hash
    Given the provided text
    When I analyse the hash with hashid
    Then I see that the various types of hash the text can be
    When I try to decipher it as MD4 with
    """
    icyberchef.com
    """
    Then I get the output "<outp>" [evidence](05.png)
    When I go back to the challenge's page
    And I type the "<outp>" into the validation box and click for validation
    Then Page displays the message "<outp> n'est pas la bonne réponse !"
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Fail: Decipher as Double MD5 Hash
    Given the provided text
    When I analyse the hash with hashid
    Then I see that the various types of hash the text can be
    When I try to decipher it as Double MD5 with
    """
    icyberchef.com
    """
    Then I get the output "<outp>" [evidence](06.png)
    When I go back to the challenge's page
    And I type the "<outp>" into the validation box and click for validation
    Then Page displays the message "<outp> n'est pas la bonne réponse !"
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Success: Decipher with John the Ripper as LM Hash
    Given the provided text
    And I save it to a file
    When I decipher as LM Hash using John the Ripper [evidence](07.png)
    """
    john --format=LM [file]
    """
    Then I obtain the flag "<flag>"
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
