## Version 2.0
## language: en

Feature: 153-H4CK1NG-enigmes-a-thematiques
  Code:
    153
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    thecyberpunker
  Goal:
    Crack the Hash

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |
    | Hash            | 1.2           |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/153
    """
    Then I see a Hash code and a form validation
    """
    40FD6820DE5FD55BAAD3B435B51404EE
    """
    And I see a message
    """
    Entrer la réponse:
    """

  Scenario: Fail: Try to identify and decode the Hash
    Given the Hash from the website
    When I try to identify the Hash [evidence](1.png)
    And I got MD5 possible Hashs
    When I go to decode the Hash in Hashcat
    And I can not decode [evidence](2.png)

  Scenario: Success: Try to decode the Hash
    Given the Hash from the website
    And I realize about the type of the Hash
    Then I go to Crackstation
    """
    https://crackstation.net/
    """
    And I determine that the Hash type is LM
    Then I crack the Hash in the same website
    And I got the result code [evidence](4.png)
    When I go to the website to validate the Hash
    And I enter the Hash
    And I got a message "Bravo thecyberpunker!"
    And I solve the callenge [evidence](5.png)
