## Version 2.0
## language: en

Feature: 153-H4CK1NG-enigmes-a-thematiques
  Code:
    153
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Crack the hash

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/153
    """
    And I can see the following hash
    """
    40FD6820DE5FD55BAAD3B435B51404EE
    """

  Scenario: Success: Use Crackstation web page to look for the hash cracked
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/153
    """
    And I know that before cracking the hash I have to look for it online
    And I know that I can use the following webpage
    """
    https://crackstation.net/
    """
    When I copy the hash
    And I paste it on the Crackstation page
    And I click the 'Crack Hashes' button
    Then I can see that the web page recognize the hash
    And I can see that the page displays the cracked hash [evidence](1.png)
    When I submit the flag
    Then the web page displays the following message [evidence](2.png)
    """
    Bravo J!
    """
    And I have completed the challenge
