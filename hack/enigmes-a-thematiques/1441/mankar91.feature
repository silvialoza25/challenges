## Version 2.0
## language: en

Feature: 1441-H4CK1NG-enigmes-a-thematiques
  Code:
    1441
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    Mankar
  Goal:
    Find the combination to open a safe box based on Javascript code

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu          | 20.04.2 LTS   |
    | Google Chrome   | 91.0.4472.114 |
    | Python          | 3.8.5         |
  Machine information:
    Given I am accessing the website
    And the website shows a picture of a safe box
    And the website shows 16 buttons labeled with hexadecimal digits

  Scenario: Fail: Inspect source code and try to brute-force the solution
    Given the challenge webpage
    When I inspect the source code using browser Dev Tools [evidence](01.png)
    Then I find a Javascript code that validates the combination
    And the combination is passed as a parameter to a function "val()"
    And its result is compared to the hex string "79A1419A"
    When I check an external script referenced from the HTML
    """
    https://enigmes-a-thematiques.fr/web/enigmes/epreuve1441/francisque.js
    """
    Then I find it contains the "val()" function definition
    When I analyze this function
    Then I deduce it calculates the CRC-32 value of the safe combination
    When I write a simple program that brute-forces the code to find the CRC
    Then I realize it won't work because the combination is an 8-digit hex
    And it will take too long to find the answer

  Scenario: Fail: Search for an online tool that reverses the CRC code
    Given I have found the CRC code of the safe combination
    When I search for a way to reverse the CRC code
    Then I find an online tool
    """
    https://md5hashing.net/hash/crc32
    """
    When I enter the hex string to the reverse lookup field
    Then it doesn't return a solution
    And I assume it only tries with previously-stored computations
    And the combination I'm looking for is a (possibly random) hex string

  Scenario: Fail: Search for an algorithm to reverse the CRC code
    Given I have found the CRC code of the safe combination
    When I search for an algorithm to reverse the CRC code
    Then I find several articles explaining the algorithm in full detail
    But I find them very technical and without a practical solution
    When I try to search for a way to find CRC-32 collisions
    Then I find a couple of methods described in forums
    """
    https://stackoverflow.com/questions/1515914/crc32-collision
    """
    When I test these methods with the Javascript code of the challenge
    Then I don't find a hex string that yields the searched CRC code
    When I find a C program that calculates CRC-32 collisions
    """
    https://ideone.com/Wfjfum
    """
    And I modify it and execute it using the searched CRC code
    Then I notice it returns different CRC codes for known inputs
    When I find a Python program that reverses CRC-32 codes
    """
    https://github.com/matrosov/pyTools/blob/master/crc32.py
    """
    And I execute it
    Then it returns a 4-byte hex string [evidence](02.png)
    When I enter this sequence into the safe box
    Then it doesn't open it
    And I realize I should not look for a 4-byte hex, but an 8-byte string
    And this string must be composed of 8 hex characters

  Scenario: Success: Modify the Python program to find an 8-byte solution
    Given I have found the CRC code of the safe combination
    And I have found a Python program that reverses CRC-32 codes
    When I modify the program so it returns 8-byte sequences
    Then I obtain an endless list of strings [evidence](03.png)
    When I reduce the list of permitted characters to just the hex digits
    Then I obtain a single solution [evidence](04.png)
    And I immediately notice it corresponds to a word in leet language
    When I enter this string into the safe box
    Then the input field shows an authorization message
    And an image of an open safe appears [evidence](05.png)
    When I enter the sequence to the solution field
    Then it shows a fail message
    When I replace the numbers in the string with alphabetic characters
    Then a congratulations message appears [evidence](06.png)
    And the challenge is solved
