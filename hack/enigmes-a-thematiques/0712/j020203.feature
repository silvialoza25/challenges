## Version 2.0
## language: en

Feature: 1496-H4CK1NG-enigmes-a-thematiques
  Code:
    1496
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find a way to login as an administrator user and obtain the flag

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve712/index.php
    """
    And I can see the source code in the following page
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve712/index.txt
    """

  Scenario: Fail: Try to bypass the login with special characters
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve712/index.php
    """
    When I see the login page [evidence](1.png)
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve712/index.php?action=login
    """
    And I can see two textfields for the user and its corresponding password
    Then I decide to identify the error message
    When I fill the textfields with the text 'test' [evidence](2.png)
    Then I press the 'Connexxion' button
    And the page shows the following message [evidence](3.png)
    """
    Nom d'utilisateur ou mot de passe incorrect
    """
    When I try to use some special characters in both textfields
    And I press the 'Connexxion' button [evidence](4.png)
    Then I expect that the page throws a server error
    And I can not see any error
    And the page just reloads
    When the page just displays the same previous message
    Then I know that this type of special character is not taken into account
    When I decide to try the same technique with different special characters
    """
    ' / \ - # " . ; ,
    """
    Then the page keeps displaying the same message
    And I have not completed the challenge

  Scenario: Fail: Try to trick the registration algorithm
    Given I open the read the page where is the source code
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve712/index.txt
    """
    And I read the login verification part
    When I notice that the page leaks the 'admin' user [evidence](5.png)
    Then I open the registration page [evidence](6.png)
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve712/index.php?action=
    register
    """
    When I try a basic user registration trick to obtain the admin account
    """
    Identifiant : ' admin'
    Mot de passe : '123'
    """
    Then the page displays the following message
    """
    Vous �tes maintenant enregistr�
    """
    When I open the login page
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve712/index.php?action=login
    """
    And I try to login with the following input
    """
    Identifiant : 'admin'
    Mot de passe : '123'
    """
    Then the page display the following message
    """
    Nom d'utilisateur ou mot de passe incorrect
    """
    And I have not completed the challenge

  Scenario: Success: Use the structure that the code use to save the accounts
    Given I open the web page
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve712/index.php
    """
    And I read the source code in the following web page
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve712/index.txt
    """
    When I read the register part in the code
    And I can see how the accounts are stored [evidence](7.png)
    """
    fwrite($db,$_POST['login'].'||'.$_POST['passwd'].'||'."\r\n");
    """
    Then I understand that the accounts are stored with the following structure
    """
    user||password||\r\n
    """
    When I open the registration page
    And I fill the textfields with the following input [evidence](8.png)
    """
    Identifiant : 'admin||password||\r\n'
    Mot de passe : '123'
    """
    Then the page displays the following message
    """
    Vous �tes maintenant enregistr�
    """
    When I open the login page
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve712/index.php?action=login
    """
    And I try to login with the following input
    """
    Identifiant : 'admin'
    Mot de passe : 'password'
    """
    Then I have login as the user admin
    And the page displays the flag [evidence](9.png)
    When I open the ctf page
    And I submit the answer
    Then the web page displays the following message [evidence](10.png)
    """
    Bravo J!
    """
    And I have completed the challenge
