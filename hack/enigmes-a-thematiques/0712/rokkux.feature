## Version 2.0
## language: en

Feature:712 -H4CK1NG-enigmes-a-thematiques
  Code:
    712
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Obtain the flag

  Background:
  Hacker's software:
    |    <Software name>  |     <Version>     |
    |  Kali Linux         | 20.04 LTS         |
    |  Firefox            | 88.0.1(64-bit)    |
  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve712/index.php
    """
    And the Code
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve712/index.txt
    """

  Scenario: Success: Register as admin using special character
    Given the message
    """
    Le but de cette énigme est de se connecter en admin sur la page suivante :
    index.php. Inutile de bruteforcer sur cette énigme, le mot de passe de
    l'admin est bien trop long et trop compliqué. En fait, il y a une faille
    de conception dans le code, à vous de la trouver !
    Bon courage. ;)
    """
    When I read it
    Then I realize the user to use in the challenge is "admin"
    When I click on "code" link
    Then I get a page with the source code for the authentication form
    When I check the page [evidence](01.png)
    Then I conclude that the login uses a .txt file as a database
    And the code turns every line inside it into an array
    And the array uses "||" as a separator
    And the code checks if the values in the db and inputted by the user match
    And the code checks if the user is "admin"
    And if the user is "admin", the code returns the flag
    When I see the code for user registration
    Then I see there are scenarios for new user, used username and blank input
    And I see that when a new user is added, its data is appended to the db
    And the code joins the user name and password with "||"
    And at the end of this join, also appends """||.\r\n"""
    And I realize that if I register with "admin||<passwd>" as username
    And use "<passwd>" as password in the password field
    And the code will give me admin access as if I was creating a new admin user
    When I click on "index.php" link
    Then I get the page for authenticating
    When I click on "S'enregister"
    Then I get the page for registering
    When I register using "admin||<passwd>" as username
    And I use "<passwd>" as password in the password field [evidence](02.png)
    Then I get the confirmation for my registration process [evidence](03.png)
    When I go back to the authentication page
    And I use "admin" as username
    And I use "<passwd>" as password
    Then I see a page with a message and the "<flag>" on it [evidence](04.png)
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
