## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    Cryptography
  User:
    AlejandroTL (wechall)
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given the message
    """
    55533 66668 333 727777777733 333 222338833 337777338888833
    3377778 55533 77882877744433633 66668 333 222338833 7447772777733
    3366 7277782668 333 5552 33344466
    """

  Scenario: Fail: Magic operation
    Given the message
    When I access to http://icyberchef.com/
    And I put the message as input
    And I use "Magic" option
    Then I can't decrypt the message [evidence](01.png)
    And I can't solve the challenge

  Scenario: Success: Multi-tap cipher
    Given the message
    When I access to https://www.dcode.fr/multitap-abc-cipher
    And I put the message as input
    And I use "dCode FRENCH Dictionary (common words)" option
    Then I get a message that indicates a password [evidence](02.png)
    And I try to submit it as the flag
    And I solve the challenge [evidence](03.png)
