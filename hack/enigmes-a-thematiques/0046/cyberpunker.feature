## Version 2.0
## language: en

Feature: 46-G33K-enigmes-a-thematiques
  Code:
    46
  Site:
    enigmes-a-thematiques
  Category:
    G33K
  User:
    thecyberpunker
  Goal:
    Get the right answer

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/46
    """
    And I see a Latin article [evidence](1.png)
    When I translate the article
    Then I realize about to find a correct answer

  Scenario: Fail: Try to find the answer
    Given the information of the article
    When I check the source code
    And I got stuck on it
    And I do not get any clue [evidence](2.png)

  Scenario: Success: Try to understand the text
    Given the source code
    When I read it again
    Then I realize about a different part of the code
    And I try to understand the text [evidence](3.png)
    When I translate the message [evidence](4.png)
    And I got a curious quote "neither is there anyone easily recalls"
    And I realize about another quote that can be the answer
    When I go to validate the answer
    And I got a message "Bravo thecyberpunker!"
    And I solve the challenge [evidence](5.png)
