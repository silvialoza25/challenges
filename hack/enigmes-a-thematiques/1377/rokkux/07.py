import itertools

array=[1,5,7,11,13,17,19,23,29,31]
perm = itertools.permutations(array)

for val in perm:
     total = 0
     for valind in val:
          total = (total+valind)*valind
     print (val)
     print (total)
     if total == 485942270584:
          print ("The correct sequence is:")
          print (val)
          break
