import itertools
from typing import Tuple

primes = [1,5,7,11,13,17,19,23,29,31]
perms = list(itertools.permutations(primes))

def eval(perm: Tuple[int, ...], index: int, result: int) -> int:
  if (index == len(perm)):
    return result
  else:
    return eval(perm, index + 1, (result + perm[index]) * perm[index])

def sequence(perm: Tuple[int, ...]) -> bool:
  value = eval(perm, 0, 0)
  #print(p, value)
  if value == 485942270584:
    print(p, value)
    return True
  return False

for p in perms:
  if (sequence(p)):
    break
