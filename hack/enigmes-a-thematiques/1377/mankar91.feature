## Version 2.0
## language: en

Feature: 1377-H4CK1NG-enigmes-a-thematiques
  Code:
    1377
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    Mankar
  Goal:
    Find the combination to open a safe based on Javascript code

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu          | 20.04.2 LTS   |
    | Google Chrome   | 91.0.4472.114 |
    | Python          | 3.8.5         |
  Machine information:
    Given I am accessing to the website
    And the website shows a row of ten checkboxes

  Scenario: Fail: Inspect webpage source code and calculate the result manually
    Given I have clicked randomly on the checkboxes with no result
    When I inspect the webpage using browser Dev Tools
    Then I find that every checkbox triggers a Javascript function
    And every checkbox sends a different parameter to the function
    When I analyze the code that is executed
    Then I find that all checkboxes must be True in order to catch the flag
    And I find a variable called 'total' that changes with every click
    And that it must have a certain value to open the safe [evidence](01.png)
    When I review the parameters that every checkbox is sending
    Then I notice that they add a term that depends on a prime number (or "1")
    When I state a formula that describes the program behavior
    And I try to find the combination that yields the searched value manually
    Then I conclude that there are too many possibilities
    And I give up on this manual approach

  Scenario: Fail: Write a program that finds the terms starting from the result
    Given I have found the formula that describes the challenge
    When I notice there's no limit on the number of times a box can be checked
    Then I write a program that tries to find the values that yield the result
    And this program starts from the result and test every possible number
    And it divides and subtracts the prime numbers starting from the result
    And it allows a number to be tested multiple times
    When I execute the program
    Then it crashes because it gets to the recursion depth limit
    When I notice that a checkbox adds a term that depends on number 1
    Then I deduce that the searched sequence shouldn't have repeated terms
    And I decide to rewrite the program

  Scenario: Success: Calculate all possible permutations to find the result
    Given I notice that every checkbox should be clicked only once
    When I write a program that calculates all possible permutations
    And it brute-forces the solution by evaluating every one of them
    And I execute it [evidence](02.py)
    Then I obtain the desired sequence [evidence](03.png)
    When I map the order of the sequence to the checkboxes
    And I click the boxes in that order in the webpage
    Then the website shows a congratulations message
    And an image of an open safe appears [evidence](04.png)
    And I caught the flag
