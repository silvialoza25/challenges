## Version 2.0
## language: en

Feature: 1496-H4CK1NG-enigmes-a-thematiques
  Code:
    1496
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Calculate the combination and obtain the flag

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1377
    """
    And I can see ten checkboxes

  Scenario: Fail: Modify the values of every checkbox and total to 0
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1377
    """
    When I right click any checkbox
    And I select the inspect button
    Then I can see in the sources tab of the developer tools the code
    And I can see that after a checkbox is selected a function is called
    And every function call has a different value [evidence](1.png)
    And I know there is javascript code in the head element [evidence](2.png)
    When I read this part of the condition in the code
    """
    if (... && total == 485942270584 && ...) {...}
    """
    Then I modify one checkbox value
    And the previous condition to 0 [evidence](3.png)
    When I select the checkbox modified
    Then nothing happens
    And I have not completed the challenge

  Scenario: Success: Code a program to decipher the combination
    Given I open the web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1377
    """
    When I right click any checkbox
    And I select the inspect button
    Then I can see in the sources tab of the developer tools the code
    And I can see that after a checkbox is selected a function is called
    And every function call has a different value [evidence](1.png)
    And I know there is javascript code in the head element [evidence](2.png)
    When I read this part of the condition in the code
    """
    if (... && total == 485942270584 && ...) {...}
    """
    And I understand how the function works
    Then I make a list with every value
    """
    [1, 5, 7, 11, 13, 17, 19, 23, 29, 31]
    """
    And I realize that the list has a pattern
    When I realize that the list is composed of odd numbers
    And I can see that the list only has prime numbers
    And I search the pattern in the browser
    Then I find out that the list has the first ten prime odd numbers but three
    And the list corresponds to the Anthony Hill's Prime Rhythms
    """
    [1, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, ...]
    """
    When I read the request of the flag
    """
    Quel est le code du coffre-fort ?
    """
    Then I know that I have to discover the correct combination
    And I make a program to calculate all the permutations without repetition
    When I finish the code in Javascript [evidence](4.js)
    And I run the program
    Then I can get the correct combination [evidence](5.png)
    When I select the checkboxes in the right order
    Then the page displays a popup [evidence](6.png)
    When I click the accept button
    Then the page displays an image [evidence](7.png)
    When I submit the flag
    Then the web page displays the following message [evidence](8.png)
    """
    Bravo J!
    """
    And I have completed the challenge
