const numsOriginal = [1, 5, 7, 11, 13, 17, 19, 23, 29, 31]
let nums = [1, 5, 7, 11, 13, 17, 19, 23, 29, 31]
let counter = 0;
let total = 0;
let iterations = 0;
let combination = "";
let permutations = [];
let temp = [];

function t(val)
{
  total = (total+val)*val;
  if(total == 485942270584)
  {
    msgpart1="Bravo, utilise l\'ordre dans lequel tu as coché les checkbox ";
    msgpart2="pour valider.";
    console.log(msgpart1+msgpart2)
    console.log(nums);
    nums.forEach(readArray);
    permutations = [...new Set(temp)]
    console.log("Num of permutations: " + permutations.length + "/3628800")
    console.log("The combination is " + combination);
    counter++;
  }
}

function logArrayElements(element, index, array) {
    t(element);
}

function readArray(element, index, array) {
    combination += (numsOriginal.indexOf(element) + 1).toString();
}

while (total != 485942270584 && counter < 1) {
    let shuffled = nums
        .map((a) => ({sort: Math.random(), value: a}))
        .sort((a, b) => a.sort - b.sort)
        .map((a) => a.value)
    nums = shuffled
    temp.push(nums)
    nums.forEach(logArrayElements);
    total = 0
    iterations++;
    if (iterations % 500000 == 0){
      console.log("More than " + iterations + " iterations have been done.");
    }
}
