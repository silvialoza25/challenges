## Version 2.0
## language: en

Feature: 1377-H4CK1NG-enigmes-a-thematiques
  Code:
    1377
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Find the correct order sequence of value-inputs to obtain the flag

  Background:
  Hacker's software:
    |         <Software name>       |            <Version>            |
    |  Kali Linux                   | 20.04 LTS                       |
    |  Firefox                      | 88.0.1(64-bit)                  |
    |  Python                       | 3.9.2                           |
    |  VScode                       | 1.57.1                          |

  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/front/enigme/1377
    """
    And 10 checkboxes
    And the text
    """
    Quel est le code du coffre-fort ?
    """

  Scenario: Fail: Click the checkboxes
    Given the web page
    When I inspect the page's source code [evidence](01.png)
    And focus on the checkboxes' code
    Then I see that to get the flag, all checkboxes must be clicked
    And there is the equation
    """
    total = (total+val)*val;
    """
    And each checkbox has a numeric value
    """
    1,5,7,11,13,17,19,23,29 and 31
    """
    And the message
    """
    Bravo, utilise l\'ordre dans lequel tu as coché les checkbox pour valider.
    """
    And I see that this message is displayed if the value inside "total" equals
    """
    485942270584
    """
    When I click on all of the checkboxes in any order
    Then I see nothing happens
    And I conclude I can't use this method
    And I could not capture the flag

    Scenario: Fail: Click checkboxes taking into account "total" value
    Given the web page
    And the information from the previous scenario
    When I copy the checkboxes' javascript code and HTML
    And I paste it into "https://jsfiddle.net/" [evidence](02.png)
    And I add alert instructions to print the actual value of "total"
    And I add alert instructions to print the actual value of a clicked checkbox
    And I click some checkboxes
    Then I see that the value inside "total" changes [evidence](03.png)
    And I see the value changes according to the equation on the first scenario
    When I click on all of the checkboxes in any order
    Then I see nothing special happens
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Success: Check for existing directories in the web page
    Given the web page
    And the information obtained from previous scenarios
    When I realize I can brute-force the order of clicks with permutations
    And I can brute-force it with python
    When I code my solution using "itertools" for permutations
    And I use the equation on the "js" to simulate the clicks
    And I use the values of each checkbox inside an array
    Then I execute the code [evidence](04.png) [evidence](07.py)
    And I get the correct order sequence for clicking the checkboxes
    When I go back to the challenge's page
    And I click the checkboxes in the obtained order
    Then I get the message [evidence](05.png)
    """
    Bravo, utilise l'ordre dans lequel tu as coché les checkbox pour valider
    """
    And the picture of an opened safe box [evidence](06.png)
    When I type the value of each button in the correct order
    And I click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
