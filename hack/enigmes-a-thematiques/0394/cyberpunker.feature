## Version 2.0
## language: en

Feature: 394-cryptographie-enigmes-a-thematiques
  Code:
    394
  Site:
    enigmes-a-thematiques
  Category:
    cryptographie
  User:
    thecyberpunker
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/394
    """
    And I see a picture with a ship and some flags [evidence](1.png)
    Then I realize about the flags are a type of encryption
    When I search the type of encryption
    """
    Navy Signals Code
    """
    And I found a possible decoder [evidence](2.png)

  Scenario: Fail: Try to decrypt the picture
    Given the information of the decoder type
    When I try to put flag by flag
    And I got a decoded message [evidence](3.png)
    """
    NOMS <FLAG>
    """
    Then I try to put multiples times that message as a flag in different ways
    And I got a bad response [evidence](4.png)

  Scenario: Success: Try to do a deep search
    Given the information from the decrypted picture
    And I realize about a clue in the text
    """
    NOMS 8 BATEAUX 4
    """
    When I search the clue in Google
    And I realize about two ships involved in warfare [evidence](5.png)
    Then I copy the name of the two ships
    When I go to validate the names as answer
    And I got a message "Bravo thecyberpunker!"
    And I solve the challenge [evidence](6.png)
