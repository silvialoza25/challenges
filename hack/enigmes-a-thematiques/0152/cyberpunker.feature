## Version 2.0
## language: en

Feature: 152-cryptographie-enigmes-a-thematiques
  Code:
    152
  Site:
    enigmes-a-thematiques
  Category:
    cryptography
  User:
    thecyberpunker
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/152
    """
    And I see a type of encoding numbers [evidence](1.png)

  Scenario: Fail: Try to decode with Cyberchef
    Given the numbers of the website
    When I try to identify the encoding of the numbers
    And I go to "Cyberchef"
    """
    https://cyberchef.immersivelabs.online/
    """
    When I try to decode the numbers with Magic Operations
    And I got nothing [evidence](2.png)

  Scenario: Success: Try to decode with Prime Numbers encoding
    Given the numbers from the website
    And I realize about the type of encoding is Prime Numbers Cipher
    Then I go to "Dcode.fr"
    """
    https://www.dcode.fr/prime-numbers-cipher
    """
    And I put the numbers to decode
    Then I decode the numbers [evidence](3.png)
    When I go to the website to validate
    And I enter the decoded message
    And I got a message "Bravo thecyberpunker"
    And I solve the challenge [evidence](4.png)
