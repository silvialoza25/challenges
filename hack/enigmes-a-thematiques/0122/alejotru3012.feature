## Version 2.0
## language: en

Feature: cryptographie-de-base - crypto - enigmes-a-thematiques
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    cryptography
  User:
    AlejandroTL (wechall)
  Goal:
    Decrypt the message

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given an encrypted message
    """
    TGUgbW90IGRlIHBhc3NlIGVzdCBiYXNlNjQ=
    """

  Scenario: Fail: Substitution
    Given the message
    When I use "http://icyberchef.com/" to decode it
    And I put the message as input
    And I use "Substitute" option
    Then I get in the output the result [evidence](01.png)
    And it doesn't work as flag
    And I can't solve the challenge

  Scenario: Fail: Decoding from Base64
    Given the message
    And it has a '=' character at the end
    And I notice it is very common in Base64 encrypted messages
    When I use "http://icyberchef.com/" to decode it
    And I put the message as input
    And I use "From Base64" option
    Then I get readable phrase as output [evidence](02.png)
    And I try to submit as flag
    And I can't solve the challenge [evidence](03.png)

  Scenario: Success: Translating the message
    Given the previous output
    And I notice it is in another language
    When I use google translator to know what it says
    Then I see that it indicates a password
    And I try to submit it as flag
    And I solve the challenge [evidence](03.png)
