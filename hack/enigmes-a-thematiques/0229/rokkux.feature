## Version 2.0
## language: en

Feature: 229-H4CK1NG-enigmes-a-thematiques
  Code:
    229
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Obtain the flag

  Background:
  Hacker's software:
    |    <Software name>  |   <Version>   |
    |  Kali Linux         | 20.04 LTS     |
    |  Firefox            | 88.0.1(64-bit)|
  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep229.php
    """

  Scenario: Fail: Change the value (amount) of money sent
    Given the provided webpage
    When I inspect the web page in the browser
    Then I see its source code
    And I see the options for sending "money" to the author
    When I see the amount of money on
    """
    Un chequè de 10€
    """
    And I change it to
    """
    Un chequè de 1234€
    """
    And I click on "Envoyer le cadeau" button to send
    Then I see the message [evidence](01.png)
    """
    Merci. J'apprécie beaucoup ton geste !
    Mais tu dois me donner davantage si tu veux le mot de passe !
    """
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Success: Change the value (actual value) sent to the server
    Given the provided webpage
    When I inspect the web page in the browser
    Then I see its source code
    And I see the options for sending "money" to the author
    When I see the variable "value" inside the form
    And I change the value="1" to value="1234" [evidence](02.png)
    And I click on "Envoyer le cadeau" button to send
    Then I see the message [evidence](03.png)
    """
    Bien joué. :) Le mot de passe est <flag>.
    """
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
