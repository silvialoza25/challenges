## Version 2.0
## language: en

Feature: 229-H4CK1NG-enigmes-a-thematiques
  Code:
    229
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find a way to obtain the flag

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep229.php
    """
    And I see the requirements to obtain the flag

  Scenario: fail: Change web request from post to get
    Given I open the web page
    And I read the hint to obtain the flag [evidence](1.png)
    When I select the first element of the list
    And I press the 'Envoyer le cadeau' button to submit the request
    Then the page shows the following message [evidence](2.png)
    """
    Merci. J'apprécie beaucoup ton geste ! Mais tu dois me donner davantage
    si tu veux le mot de passe !
    """
    When I press the 'F12' button to display the developer tools
    Then I can see in the elements tab that the page is using a post method
    When I change the post method to get method [evidence](3.png)
    And I press the 'Envoyer le cadeau' button to submit the request
    Then the url change [evidence](4.png)
    And I can see the parameters that are required
    When I change the prix value
    And press the ENTER button to submit the request
    Then the page doesn't change
    And I realize that the web page doesn't accept get requests
    And I have not completed the challenge

  Scenario: Success: Change the value of the list to "5"
    Given I open the web page
    And I read the hint to obtain the flag [evidence](1.png)
    When I press the 'F12' button to display the developer tools
    Then I can see in the elements tab that the page is using a post method
    When I expand the form element
    And I expand the select element
    Then I can see the options of the list
    And I can see the corresponding values of the options [evidence](5.png)
    When I change the value of the first option to "5" [evidence](6.png)
    And I press the 'Envoyer le cadeau' button to submit the request
    Then the page shows the following message [evidence](7.png)
    """
    Bien joué. :) Le mot de passe est #####.
    """
    When I open the ctf page
    And I submit the answer
    Then the web page displays the message [evidence](8.png)
    """
    Bravo J!
    """
    And I have completed the challenge
