## Version 2.0
## language: en

Feature: 136-H4CK1NG-enigmes-a-thematiques
  Code:
    136
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find a way to obtain the flag

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/136
    """
    And I can open the challenge page on the following web page
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep136.php
    """

  Scenario: Fail: Change the return value to true
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep136.php
    """
    When I read the text [evidence](1.png)
    Then I know that the password is the following text
    """
    foobar
    """
    When I fill the password textfield
    And I press the 'Authentication' button
    Then I can see that nothing happens
    When I right-click the 'Authentication' button
    And I press the 'Inspect' button
    Then I can see that there is a form
    When I see the false value [evidence](2.png)
    Then I change it to true
    When I press the 'Identification' button
    Then I can see that nothing happens
    And I have not completed the challenge

  Scenario: Success: Send the request modifying the URL
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep136.php
    """
    When I read the text [evidence](1.png)
    Then I know that the password is the following text
    """
    foobar
    """
    When I fill the password textfield
    And I press the 'Authentication' button
    Then I can see that nothing happens
    When I right-click the 'Authentication' button
    And I press the 'Inspect' button
    Then I notice that the form is sent to a new webpage [evidence](3.png)
    And I can add it to the URL
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep136.php/admin.php
    """
    When I realize that I can send the value through the URL
    And I know that this is possible due to is a PHP page
    Then I know that I need to send what is inside the textfield
    When I read the line in the source code where is located the element
    """
    <input type="text" name="rep">
    """
    Then I can see that the name of the textfield is 'rep'
    When I add the following text to the URL to send the data
    """
    ?rep=foobar
    """
    And I enter the URL
    Then I can see a new web page [evidence](4.png)
    When I fill the textfield with the flag in the ctf web page
    Then the web page displays the following text [evidence](5.png)
    """
    Bravo J !
    """
    And I have completed the challenge
