## Version 2.0
## language: en

Feature: 136-H4CK1NG-enigmes-a-thematiques
  Code:
    136
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Obtain the flag

  Background:
  Hacker's software:
    |    <Software name>  |   <Version>   |
    |  Kali Linux         | 20.04 LTS     |
    |  Firefox            | 88.0.1(64-bit)|
  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep136.php
    """

  Scenario: Fail: Send password via Authentication button
    Given the provided webpage
    And I see the message
    """
    Bonjour, si tu veux pénétrer dans ce domaine, c'est très facile :
    le mot de passe est : foobar.
    Seulement, mon serveur fait des siennes et le bouton d'authentification
    n'a pas l'air de vouloir fonctionner...
    Accès refusé !
    """
    When I write the password in the text box
    And I click on "Authentification" button to send
    Then I see the page doesn't do anything, the button doesn't work
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Fail: Change parameter "onsubmit" in form's source code
    Given the provided webpage
    When I inspect the web page in the browser [evidence](01.png)
    Then I see its source code
    And I see the code for authentication process
    And I see the button it's actually a form
    When I see the value of the "onsubmit" attribute
    """
    onsubmit="return false"
    """
    And I change it to
    """
    onsubmit="return true"
    """
    And I write the password in the text box [evidence](02.png)
    And I click on "Authentification" button to send
    Then I see get an Error 404 [evidence](03.png)
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Success: Change parameter "onsubmit" and add /admin.php to URL
    Given the provided webpage
    When I add "/admin.php" to the end of URL"
    And I press enter
    Then I see the same page as before, but the URL has "/admin.php" on it
    When I inspect the web page in the browser
    Then I see its source code
    And I see the code for authentication process
    And I see the button it's actually a form
    When I see the value of the "onsubmit" attribute
    """
    onsubmit="return false"
    """
    And I change it to
    """
    onsubmit="return true"
    """
    And I write the password in the text box
    And I click on "Authentification" button to send
    Then I see the message [evidence](04.png)
    """
    Accès autorisé !
    Le mot de passe pour valider l'épreuve est : <flag>
    """
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
