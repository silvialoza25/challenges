## Version 2.0
## language: en

Feature: 34-G33K-enigmes-a-thematiques
  Code:
    34
  Site:
    enigmes-a-thematiques
  Category:
    G33K
  User:
    thecyberpunker
  Goal:
    Get the IP variable

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/34
    """
    And I see a French message [evidence](1.png)
    When I translate the message
    And I found my IP Address
    And I see some information

  Scenario: Fail: Try to find the IP variable through source code
    Given the information of the message
    When I try to find through source code analysis
    And I do not find anything relevant

  Scenario: Success: Try to get the IP variable with the documentation
    Given the message
    And I realize about the documentation
    Then I go to the PHP documentation
    """
    https://www.php.net/manual/en/reserved.variables.server.php
    """
    And I search the keywords "remote, ip, address"
    And I got some documentation [evidence](2.png)
    Then I go to the website to validate
    And I enter the IP variable found
    And I got a message "Bravo thecyberpunker!"
    And I solve the challenge [evidence](3.png)
