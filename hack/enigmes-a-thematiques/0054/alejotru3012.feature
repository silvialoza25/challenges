## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    Cryptography
  User:
    AlejandroTL (wechall)
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given the message
    """
    53 668 33 72773 378 258646486
    """

  Scenario: Fail: Magic operation
    Given the message
    When I access to http://icyberchef.com/
    And I put the message as input
    And I use "Magic" option
    Then I can't decrypt the message [evidence](01.png)
    And I can't solve the challenge

  Scenario: Success: T9 decoding
    Given the message
    And I know solutions start with
    """
    LE MOT DE PASSE EST ...
    """
    When I access to https://www.dcode.fr/t9-cipher
    And I put the message as input
    And I use "dCode FRENCH Dictionary (common words)" option
    Then I get a message that indicates a password [evidence](02.png)
    And I try to submit it as the flag
    And I solve the challenge [evidence](03.png)
