## Version 2.0
## language: en

Feature: 228-H4CK1NG-enigmes-a-thematiques
  Code:
    228
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Obtain the flag

  Background:
  Hacker's software:
    |    <Software name>  |     <Version>     |
    |  Kali Linux         | 20.04 LTS         |
    |  Firefox            | 88.0.1(64-bit)    |
    |  Postman for web    | 8.6.1-210609-1800 |
  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep228.php
    """

  Scenario: Fail: Add admin path to URL
    Given the provided webpage
    When I load the page [evidence](01.png)
    Then I see the message
    """
    Bonjour, si tu veux pénétrer dans ce domaine, c'est très facile.
    Le mot de passe mdp est : sck8dnj33.
    Accès refusé !
    """
    And I inspect its source code
    When I add "/admin.php" to the page's URL
    And I press enter to load the page
    Then I see the same page as result [evidence](02.png)
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Fail: Add admin path to URL with authentication data
    Given the provided webpage
    When I load the page
    Then I see the message
    """
    Bonjour, si tu veux pénétrer dans ce domaine, c'est très facile.
    Le mot de passe mdp est : sck8dnj33.
    Accès refusé !
    """
    And I inspect its source code
    When I add "/admin.php" to the page's URL
    And I add "?user=admin&password=sck8dnj33" to load it with admin data
    And I press enter to load the page
    Then I see the same page as a result [evidence](03.png)
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Fail: Use Postman to send data as parameters to server
    Given the provided webpage
    When I load the page
    Then I see the message
    """
    Bonjour, si tu veux pénétrer dans ce domaine, c'est très facile.
    Le mot de passe mdp est : sck8dnj33.
    Accès refusé !
    """
    When I check the network monitor
    Then I see that the first request was a POST
    When I go to "https://web.postman.co"
    And  I create the POST request adding as the parameters
    """
    user:admin
    password:sck8dnj33
    """
    And I send the request
    Then I get a response with the same HTML code of the original page
    And see that postman added the parameters to the url (as I tried before)
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Success: Use Postman to send data inside Body request to server
    Given the provided webpage
    When I load the page
    Then I see the message
    """
    Bonjour, si tu veux pénétrer dans ce domaine, c'est très facile.
    Le mot de passe mdp est : sck8dnj33.
    Accès refusé !
    """
    When I realize the redundancy in "le mot de passe" and "mdp"
    Then I decide to use "mdp" and its value in the request
    When I check the network monitor
    And I check the only existing POST method
    Then I see that no authentication data was sent in the request's headers
    When I research about Http requests
    Then I conclude I should send the data inside the request's body
    When I go to "https://web.postman.co"
    And  I create the POST request adding as Body -> form-data
    """
    mdp=sck8dnj33
    """
    And I send the request
    Then I get a response with the message [evidence](05.png)
    """
    Le mot de passe pour valider l'épreuve est : <flag>
    """
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
