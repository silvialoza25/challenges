## Version 2.0
## language: en

Feature: 228-H4CK1NG-enigmes-a-thematiques
  Code:
    228
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    thecyberpunker
  Goal:
    Get the admin pass

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |
    | Curl            | 7.74          |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/228
    """
    And I see a French message and a button [evidence](1.png)
    When I translate the message
    Then I realize about to find an admin password
    And I press the button "Accepter la mission"
    When I go to another website [evidence](2.png)
    And I see a message that contains an admin password

  Scenario: Fail: Try to validate the password
    Given the information of the message
    When I go to the main website to validate the password
    And I try to enter the password
    And I got a failure message [evidence](3.png)

  Scenario: Fail: Try to get admin access
    Given the information of the second website
    When I check the source code [evidence](4.png)
    And I realize about a "../templates/" folder
    When I go to the folder
    And I found a file "matrix2.css"
    Then I check the file [evidence](5.png)
    And I got the password
    When I go to validate
    And I got a failure message [evidence](6.png)

  Scenario: Success: Try to perform a POST request method
    Given the information of the first message
    And I have a user password
    Then I realize about the PHP method POST
    And I perform a POST request method
    """
    curl -d 'user=password&action=message' url -X POST
    """
    Then I got a message "Accès autorisé!"
    And I got the correct password [evidence](7.png)
    When I go to validate the password
    And I got a message "Bravo thecyberpunker!"
    And I solve the challenge [evidence](8.png)
