## Version 2.0
## language: en

Feature: 228-H4CK1NG-enigmes-a-thematiques
  Code:
    228
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find a way to obtain the flag

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
    | GNU Wget        | 1.21                   |
  Machine information:
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/228
    """
    And I can see the following text
    """
    Bonjour, Voici une mission faisable m'a-t-on dit mais je n'ai toujours
    pas réussi à comprendre la subtilité du message... Alors je compte sur
    toi pour m'aider à trouver le mot de passe de l'admin !
    Bon courage, je compte sur toi pour m'aider !
    """
    And I can open the challenge page on the following web page
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep228.php
    """

  Scenario: Fail: Add the parameter in the URL
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep228.php
    """
    When I read the following text
    """
    Bonjour, si tu veux pénétrer dans ce domaine, c'est très facile.
    Le mot de passe mdp est : sck8dnj33.
    """
    Then I know that there is a parameter called 'mdp'
    And I know that its value is 'sck8dnj3'
    When I read the PHP extension in the URL [evidence](1.png)
    Then I think about adding the parameter and its corresponding value
    When I change the URL [evidence](2.png)
    And I enter the link
    Then I can see that nothing happens
    And I have not completed the challenge

  Scenario: Success: Send a post web request with corresponding data
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep228.php
    """
    When I read the following text
    """
    Bonjour, si tu veux pénétrer dans ce domaine, c'est très facile.
    Le mot de passe mdp est : sck8dnj33.
    """
    Then I know that there is a parameter called 'mdp'
    And I know that its value is 'sck8dnj3'
    When I open the developers' tools
    And I open the 'Network' tab
    And I select the page request
    Then I can see that the page needs a 'POST' request [evidence](3.png)
    And I know that the page needs the following specific data
    """
    mdp
    sck8dnj33
    """
    When I execute the following commands to send a post request
    """
    url="https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep228.php"
    params="mdp=sck8dnj33"
    wget -qO- --post-data $params $url
    """
    Then I can see the flag [evidence](4.png)
    When I fill the textfield with the flag in the ctf web page
    Then the web page displays the following text [evidence](5.png)
    """
    Bravo J !
    """
    And I have completed the challenge
