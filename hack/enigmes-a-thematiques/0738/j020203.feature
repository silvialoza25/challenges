## Version 2.0
## language: en

Feature: 0738-H4CK1NG-enigmes-a-thematiques
  Code:
    0738
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Crack the files and get the flag

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
    | JohnTheRipper   | 1.9.0-jumbo-1          |
    | Hashcat         | 6.1.1                  |
    | pdfcrack        | 0.19                   |
    | xlcrack         | 1.2                    |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/front/enigme/738
    """
    And the page let me download a zip file located in the next link
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve738/80N_4NN1F_347.zip
    """

  Scenario: Fail: Try to crack the password using JohnTheRipper
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/738
    """
    And I download the file
    """
    80N_4NN1F_347.zip
    """
    When I unzip the file using the following command
    """
    unzip 80N_4NN1F_347.zip
    """
    Then I can see a PDF file with the following name
    """
    protected.pdf
    """
    When I try to open it
    Then I can see a popup requesting a password [evidence](1.png)
    When I think about using the cracker tool JohnTheRipper
    And I know that with this tool I can crack the password
    Then I use the following command [evidence](2.png)
    """
    pdf2john.pl protected.pdf > hashPdfJohn
    """
    And I have created a file with the hash of the file in john format
    When I try to crack the password using the following command
    """
    john hashPdfJohn
    """
    And I wait one hour
    Then I know that the password possibly is not in the dictionary
    And I have not completed the challenge

  Scenario: Fail: Try to crack the password using Hashcat
    Given I have downloaded the following file
    """
    80N_4NN1F_347.zip
    """
    And I have unzipped the file using the following command
    """
    unzip 80N_4NN1F_347.zip
    """
    And I have a PDF file with the following name
    """
    protected.pdf
    """
    And I know that this file requires a password
    And I know that a dictionary attack did not work
    And I have created the hash file with john format with the next command
    """
    pdf2john.pl protected.pdf > hashPdfJohn
    """
    When I think about using the cracker tool Hashcat
    And I know that this tool can crack the password with a brute-force attack
    Then I try to change the john format to a Hashcat format
    And I remove the following part of the file
    """
    protected.pdf:
    """
    And I use the following command to identify the file's type
    """
    file protected.pdf
    """
    When I read the following output
    """
    protected.pdf: PDF document, version 1.4
    """
    And I use the following command to search in Hashcat the mode
    """
    hashcat --help | grep -i 'pdf'
    """
    Then I read this part of the results
    """
    10500 | PDF 1.4 - 1.6 (Acrobat 5 - 8)                    | Documents
    """
    And I know what Hashcat command use
    When I use the following command to crack the password
    """
    hashcat -a 3 -m 10500 hashPdfJohn
    """
    Then I read the following part of the output
    """
    Hashfile 'hashPdfJohn' ... ($pdf$1...4e2463618d): Token length exception
    """
    And I realize that Hashcat does not support this type of hash
    And I have not completed the challenge

  Scenario: Success: Use pdfcrack and xlcrack to crack the files
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/738
    """
    And I download the file
    """
    80N_4NN1F_347.zip
    """
    When I unzip the file using the following command
    """
    unzip 80N_4NN1F_347.zip
    """
    Then I can see a PDF file with the following name
    """
    protected.pdf
    """
    When I try to open it
    Then I can see a popup requesting a password [evidence](1.png)
    When I realize I can use the pdfcrack tool to crack the password
    Then I use the following command to do it
    """
    pdfcrack -f protected.pdf
    """
    When the tool finishes
    Then I can read the password [evidence](3.png)
    When I use the password to open the PDF file
    And I read the content
    Then I can see two images
    And I can see the following text [evidence](4.png)
    """
    ./[Nom de famille des deux sœurs qui ont
    composé la chanson dont l’air
    « Joyeux anniversaire » est inspiré].xls
    """
    When I read the last word of the text
    Then I know that it refers to a file
    When I search the text without the extension on the internet
    Then I can read the following text
    """
    La mélodie de Joyeux Anniversaire provient de Good Morning to All,
    chanson enfantine écrite et composée en 1893 par deux sœurs américaines,
    Patty et Mildred Hill.
    """
    When I read the text
    Then I know that the filename is the following one
    """
    hill.xls
    """
    When I think about using the link where I downloaded the ZIP file
    And I modify the URL to download the hidden file
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve738/hill.xls
    """
    Then I can download the hidden file [evidence](5.png)
    When I use the following command to identify the file
    """
    file hill.xls
    """
    And I read the result [evidence](6.png)
    Then I notice that the date of the file is the following one
    """
    Sun Mar 27 17:55:21 2011
    """
    When I read the year
    Then I use the following command
    """
    hashcat --help | grep -i 'office'
    """
    And I verify if Hashcat or JohnTheRipper can crack the password
    When I read the results
    And I can see the following text [evidence](7.png)
    """
    9600 | MS Office 2013                                   | Documents
    """
    Then I try to get the hash in a john format
    And I use the following command
    """
    office2john.py hill.xls
    """
    When I read the following output [evidence](8.png)
    """
    XOR obfuscation detected
    """
    Then I know that neither JohnTheRipper nor Hashcat will be able to crack it
    When I think about using the external tool xlcrack
    And I install it using the instructions on the web page
    """
    https://github.com/x2q/dotbootstrap/blob/master/_posts/
    2010-01-05-crack-or-recover-password-protected-excel-spreadsheet.markdown
    """
    Then I use the following command
    """
    ./xlcrack hill.xls
    """
    And I can see the password [evidence](9.png)
    When I open the file
    And I enter the password
    Then I can see the file content
    When I select the print view [evidence](10.png)
    Then I can see a sentence that indicates the flag
    When I open the second page of the file
    """
    Feuil 2
    """
    And I open the print view
    Then I can see the flag [evidence](11.png)
    When I submit the flag
    Then the web page displays the following message [evidence](12.png)
    """
    Bravo J!
    """
    And I have completed the challenge
