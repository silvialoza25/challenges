## Version 2.0
## language: en

Feature: 738-H4CK1NG-enigmes-a-thematiques
  Code:
    738
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Find the flag by applying cracking passwords methods

  Background:
  Hacker's software:
    |         <Software name>       |            <Version>            |
    |  Kali Linux                   | 20.04 LTS                       |
    |  Firefox                      | 88.0.1(64-bit)                  |
    |  JohnTheRipper                | 1.9.0-jumbo-1                   |
    |  pdfcrack                     | 0.19                            |
    |  crackxls2003                 | 0.4                             |
    |  xlcrack                      | 1.2                             |

  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/front/enigme/738
    """

  Scenario: Fail: Use JohnTheRipper to crack pdf's password
    Given the web page
    When I load the page
    Then I see there is a link [evidence](01.png)
    """
    80N 4NN1F 347
    """
    When I click this link
    Then I get the option to download a ".zip" file named
    """
    80N_4NN1F_347.zip
    """
    And I download this file
    When I open the downloaded .zip
    Then I see a pdf named "protected.pdf" inside it [evidence](02.png)
    And I extract the .pdf out of its .zip
    When I open the file
    Then I am asked for a password to open it
    When I use JohnTheRipper to crack the pdf's password
    Then I see it takes too long to crack [evidence](03.png)
    And I couldn't capture the flag
    And I conclude I can't use this method

  Scenario: Fail: Change the URL on enigme's page
    Given the information from the previous scenario
    When I research for tools that do pdf cracking
    Then I find I can use "pdfcrack"
    When I use "pdfcrack" to crack the pdf's password
    Then I get the pdf's password [evidence](04.png)
    When I open the pdf [evidence](05.png)
    And I am asked for the password to open it
    And I use the password I obtained with "pdfcrack"
    Then I see the file contains the picture of a cat doing maths
    And I see the file contains the message
    """
    ./[Nom de famille des deux sœurs qui ont
    composé la chanson dont l’air
    « Joyeux anniversaire » est inspiré].xls
    """
    And I see the file contains the picture of a cake
    And I translate the message to english
    """
    ./[Last name of the two sisters who have
    composed the song whose tune
    "Happy Birthday" is inspired].xls
    """
    And I notice the message ends with ".xls"
    And I realize this is the extension for spreadsheet files
    And I notice I must look for the name of the ".xls" file
    And I realize that the name must comply with the instruction given
    """
    Last name of the two sisters who have
    composed the song whose tune
    "Happy Birthday" is inspired
    """
    And I notice the message on the pdf begins with "./"
    And I realize this has the same meaning as in Linux "actual path"
    And I realize I should use the actual url of the challenge
    And I should add the name of the ".xls" file to the url
    When I google "who composed the happy birthday song" [evidence](06.png)
    Then I get it was "Patty Hill" and "Mildred J. Hill", the "Hill" sisters
    And I go back to the challenge's page
    When I modify the challenge's url and change it to
    """
    https://enigmes-a-thematiques.fr/front/enigme/738/hill.xls
    """
    And I press ENTER to load the page
    Then I get a blank page with the enigmes-a-thematiques template
    And I couldn't capture the flag
    And I conclude I can't use this method

  Scenario: Fail: Use online tool for cracking .xls file
    Given the information from the previous scenarios
    When I inspect the challenge's page
    Then I see the source url of the ".zip" file [evidence](07.png)
    """
    /epreuves/epreuve738/80N_4NN1F_347.zip
    """
    When I change the "80N_4NN1F_347.zip" of the url to "hill.xls"
    And I put it in my browser as
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve738/hill.xls
    """
    Then I get the option to download a .zip file named [evidence](08.png)
    """
    hill.xls
    """
    And I download the file
    When I open "hill.xls" file
    Then I see it also asks for a password to be opened
    When I try to crack the password with an online tool such as
    """
    https://www.lostmypass.com/file-types/ms-excel/
    """
    Then I get the error message [evidence](09.png)
    """
    Error: Unsupported file format or file is not password protected *
    """
    And I couldn't capture the flag
    And I conclude I can't use this method

  Scenario: Fail: Use crackxls2003 for cracking .xls file
    Given the information from the previous scenarios
    When I try to crack the ".xls" file with "crackxls2003"
    Then the software starts trying to crack the ".xls"
    And the process takes too long [evidence](10.png)
    And I couldn't capture the flag
    And I conclude I can't use this method

  Scenario: Success: Use xlcrack for cracking .xls password
    Given the information from the previous scenarios
    When I try to crack the ".xls" file with "xlcrack"
    And I type in the terminal
    """
    ./xlcrack [path to hill.xls]
    """
    Then I get its password [evidence](11.png)
    When I open the ".xls" file
    And I use the password provided by "xlcrack" to open it
    Then I see the ".xls" opens and shows the message "Bien joué ;)"
    And I see this message is displayed on the first cell inside the file
    And I save the file with another name and without password protection
    When I use "cat" on the newly created file [evidence](12.png)
    Then I can see the "<flag>" among the displayed text
    When I go back to the challenge's page
    And I click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
