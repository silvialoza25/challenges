## Version 2.0
## language: en

Feature: 915-H4CK1NG-enigmes-a-thematiques
  Code:
    915
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Obtain the flag

  Background:
  Hacker's software:
    |         <Software name>       |     <Version>     |
    |  Kali Linux                   | 20.04 LTS         |
    |  Firefox                      | 88.0.1(64-bit)    |
    |  Strings                      |                   |
    |  Adobe Flash Player Projector | 32,0,0,465        |
    |  swfmill                      | 0.3.6             |
  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/front/enigme/915
    """

  Scenario: Fail: Check the .swf and solve the challenge according to its name
    Given the web page
    When I load it
    Then I see there is nothing special displayed on it
    When I inspect the web page
    Then I see that there is in fact an embedded file on it
    And  I see this file is a ".swf" file [evidence](01.png)
    And I realize that I can access the file by adding the PATH to the URL
    """
    /epreuves/flash/ep915.swf
    """
    And I in the end the URL has this form
    """
    https://enigmes-a-thematiques.fr/epreuves/flash/ep915.swf
    """
    When I load the previous URL
    Then I am asked to download the file, I accept
    When I open the file with Adobe Flash Player Projector
    Then I see a dark screen
    When I hover my mouse over the dark screen, I start seeing figures
    Then these figures are a disassembled dice [evidence](02.png),(03.png)
    And a normal dice [evidence](04.png)
    When I see that the challenge's name is "Ça éblouit"
    And I translate it to english
    Then I see it means "It dazzles"
    When I input words like the next ones in englih and french
    """
    Light, lantern, flash, bulb, movie
    """
    Then I see none of them is the flag
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Fail: Check the strings in the .swf file
    Given the .swf file
    When I check the strings inside it with the tool "strings"
    Then I see the data generated is unreadable [evidence](05.png)
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Fail: Turn the .swf file into a XML and read it, then type <flag>
    Given the .swf file
    When I use "sfwmill" to convert the .swf to a XML with the command
    """
    swfmill swf2xml ep915.swf
    """
    Then I see that the generated XML displays the text [evidence](06.png)
    """
    <StackString value="lemotdepasseest<flag>"/>
    """
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "n'est pas la bonne réponse !"
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Success: Turn the .swf file into a XML and read it
    Given the .swf file
    When I use "sfwmill" to convert the .swf to a XML with the command
    """
    swfmill swf2xml ep915.swf
    """
    Then I see that the generated XML displays the text
    """
    <StackString value="lemotdepasseest<flag>_mc"/>
    """
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
