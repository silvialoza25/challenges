## Version 2.0
## language: en

Feature: 915-H4CK1NG-enigmes-a-thematiques
  Code:
    915
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find the flag hidden in the program

  Background:
  Hacker's software:
    |    <Software name>    |          <Version>         |
    | Kali Linux            | 2021.2                     |
    | Chrome                | 91.0.4472.106 (64-bit)     |
    | Flashpoint Launcher   | 10.0.0 (linux-x64)         |
    | jpexs-decompiler      | 14.4.0 nightly build 1939  |
  Machine information:
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/915
    """
    And I know that Adobe Flash is no longer available
    And I can download the challenge's program on the following web page
    """
    https://enigmes-a-thematiques.fr/epreuves/flash/ep915.swf
    """

  Scenario: Fail: Execute the program using Flashpoint Launcher
    Given I have downloaded the flash program from the next web page
    """
    https://enigmes-a-thematiques.fr/epreuves/flash/ep915.swf
    """
    When I use the following command to identify the file's type
    """
    file ep915.sfw
    """
    Then I can see that is a flash program
    """
    ep915.swf: Macromedia Flash data (compressed), version 9
    """
    When I use Flashpoint Launcher to execute the program
    Then I can test the program [evidence](1.png)
    When I test it I notice that there is no secret message or any hint
    Then I have not completed the challenge

  Scenario: Success: Use a decompiler to read the source code
    Given I have downloaded the flash program from the next web page
    """
    https://enigmes-a-thematiques.fr/epreuves/flash/ep915.swf
    """
    When I use the following command to identify the file's type
    """
    file ep915.sfw
    """
    Then I can see that is a flash program
    """
    ep915.swf: Macromedia Flash data (compressed), version 9
    """
    When I execute the decompiler jpexs-decompiler
    And I open the challenge's program
    Then I can expand the 'scripts' directory
    And I can expand the 'frame 1' directory
    When I open the 'DoAction' script
    Then I can read inside a function
    And I can read the flag [evidence](2.png)
    """
    this.onLoad = function()
    {
      lemotdepasseest<flag>_mc.startDrag(true);
    };
    """
    When I fill the textfield with the flag in the ctf web page
    Then the web page displays the following text [evidence](3.png)
    """
    Bravo J !
    """
    And I have completed the challenge
