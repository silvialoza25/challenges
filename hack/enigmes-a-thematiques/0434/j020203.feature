## Version 2.0
## language: en

Feature: 0434-H4CK1NG-enigmes-a-thematiques
  Code:
    0434
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
    | Ghidra          | 10.0                   |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/front/enigme/434
    """
    And I already solved the previous challenge Exécutable lvl1
    And I can get the executable from the following page
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve434/executable_lvl2
    """

  Scenario: Fail: Use the same technique that I used to solve Exécutable lvl1
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/434
    """
    When I use the following command in the terminal
    And I can download the executable
    """
    wget https://enigmes-a-thematiques.fr/epreuves/epreuve434/executable_lvl2
    """
    Then I can use the following command to identify the type of file
    """
    file executable_lvl2
    """
    When I execute the previous command
    Then I can see the following output
    """
    executable_lvl2: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV),
    dynamically linked, interpreter /lib/ld-linux.so.2, too large section
    header offset 60928
    """
    When I notice is a 32-bit program
    Then I need to read the printable strings in the executable
    And I use the following command to do it
    """
    strings executable_lvl2
    """
    When I see the following string in the output
    """
    dommageee...
    """
    Then I copy it thinking is the flag
    When I open the ctf page
    And I submit the flag
    Then the web page displays the following message [evidence](1.png)
    And I have not completed the challenge

  Scenario: Success: Find the password using a decompiler
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/434
    """
    When I use the following command in the terminal
    And I can download the executable
    """
    wget https://enigmes-a-thematiques.fr/epreuves/epreuve434/executable_lvl2
    """
    Then I can use the following command to identify the type of file
    """
    file executable_lvl2
    """
    When I execute the previous command
    Then I can see the following output
    """
    executable_lvl2: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV),
    dynamically linked, interpreter /lib/ld-linux.so.2, too large section
    header offset 60928
    """
    When I notice is a 32-bit program
    Then I try to just read the printable strings in the executable
    And I use the following command to do it
    """
    strings executable_lvl2
    """
    When I see the following string in the output
    """
    Entrer le mot de passe :
    """
    Then I use the reversing engineering tool Ghidra
    And I open the executable with the tool [evidence](2.png)
    When I search the previous string found [evidence](3.png)
    And I select the 'Search All' option
    Then I can see one coincidence
    When I select this option [evidence](4.png)
    Then I can see where is located the coincidence
    And I can see the function reference [evidence](5.png)
    When I open the function
    And I press the decompiler button [evidence](6.png)
    Then I can see the code [evidence](7.png)
    And I can see the flag
    When I submit the flag
    Then the web page displays the following message [evidence](8.png)
    """
    Bravo J!
    """
    And I have completed the challenge
