## Version 2.0
## language: en

Feature: 434-H4CK1NG-enigmes-a-thematiques
  Code:
    434
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Find the flag by using reverse engineering methods on an executable file

  Background:
  Hacker's software:
    |         <Software name>       |            <Version>            |
    |  Kali Linux                   | 20.04 LTS                       |
    |  Firefox                      | 88.0.1(64-bit)                  |
    |  Ghidra                       | 9.1                             |

  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/front/enigme/434
    """

  Scenario: Fail: Check the contents of the file with "strings" command
    Given the web page
    When I load the page
    Then I see there is a link [evidence](01.png)
    """
    Télécharger l'exécutable
    """
    When I click this link
    Then I get the option to download a file named
    """
    executable_lvl2
    """
    And I download this file
    When I use the "strings" command on this file [evidence](03.png)
    Then I get among the outputted text, a specific text that says
    """
    Entrer le mot de passe :
    dommageee...
    """
    When I go back to the challenge's page
    And I type "dommageee..."
    And I click for validation
    Then I get the message
    """
    n'est pas la bonne réponse !
    """
    And I couldn't capture the flag
    And I conclude I can't use this method

  Scenario: Success: Check the contents of the file with "strings" command
    Given the information from the previous scenario
    When I do some research
    Then I learn about "Ghidra"
    And I learn about its reverse engineering capabilities
    When I create a new project in "Ghidra"
    And I add the downloaded file for analysis
    And I check it with the codebrowser [evidence](04.png)
    Then I find the function "FUN_08048453" [evidence](05.png)
    And I see this function evaluates "char" variables
    And these variables are evaluated inside and "if" statement
    And these variables are evaluated in a particular order
    When I get each value the variables are evaluated with
    And put them in the same order as in the "if" statement
    Then I get the "<flag>"
    When I go back to the challenge's page
    And type the "<flag>"
    And I click for validation [evidence](06.png)
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
