## Version 2.0
## language: en
Feature: 66-G33K-enigmes-a-thematiques.fr
  Code:
    66
  Site:
    www.enigmes-a-thematiques.fr
  Category:
    G33K
  User:
    juan6630
  Goal:
    Solve the question.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows 10      | 20H2        |
    | Mozilla Firefox | 85.0.2      |
  Machine information:
    Given I start the challenge with the following URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/66
    """
    And I see a question
    """
    Jusqu'à combien peut-on compter avec les doigts de ses deux mains ?
    """

  Scenario: Fail: Decimal guess
    Given the challenge's question
    When I try with '10'
    Then I get a message telling me it's wrong [evidence](01.png)

  Scenario: Fail: Binary guess
    Given the challenge's question
    When I assume the answer is related to the binary system
    And I try with '2'
    Then I get a fail message

  Scenario: Success: Binary guess
    Given the challenge's question
    When I assume it's binary with each finger as a bit
    Then I solve the challenge [evidence](02.png)
