const numsOriginalPart1 = [1, 5, 7, 11, 13, 17, 19, 23, 31, 43, 47];
const numsOriginalPart2 = [53, 59, 61, 67, 71, 73, 79, 83, 89];
const numsOriginal = numsOriginalPart1.concat(numsOriginalPart2);
let nums = numsOriginalPart1.concat(numsOriginalPart2);
let counter = 0;
let total = 0;
let iterations = 0;
let combination = "";
let temp = [];

function t(val)
{
  total = (total+val)*val;
  if(total == 4806479949511721651869051424302)
  {
    msgpart1="Bravo, utilise l\'ordre dans lequel tu as coché les checkbox ";
    msgpart2="pour valider.";
    console.log(msgpart1+msgpart2)
    console.log(nums);
    nums.forEach(readArray);
    console.log("The combination is " + combination);
    counter++;
  }
}

function logArrayElements(element, index, array) {
    t(element);
}

function readArray(element, index, array) {
    combination += (numsOriginal.indexOf(element) + 1).toString();
}

while (total != 4806479949511721651869051424302 && counter < 1) {
    let shuffled = nums
        .map((a) => ({sort: Math.random(), value: a}))
        .sort((a, b) => a.sort - b.sort)
        .map((a) => a.value)
    nums = shuffled
    temp.push(nums)
    nums.forEach(logArrayElements);
    if (total > 4800000000000000000000000000000) console.log(total);
    total = 0
    iterations++;
    if (iterations % 500000 == 0){
      console.log("More than " + iterations + " iterations have been done.");
    }
}
