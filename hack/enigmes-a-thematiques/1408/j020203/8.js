const orderInputPart1 = [1, 5, 7, 11, 13, 17, 19, 23, 31, 43, 47];
const orderInputPart2 = [53, 59, 61, 67, 71, 73, 79, 83, 89];
const orderInput = orderInputPart1.concat(orderInputPart2);
const total = BigInt("4806479949511721651869051424302");
let combination = "";
let solution = [ ];

function findPerm(orderInput, total) {
  const format = BigInt;
  const possibleSolution = [ ];
  total = format(total);

  for(let i in orderInput) {
    const num = format(orderInput[i]);
    if(!(total % num)) {
      const temp = {
        possibleSolution: [num],
        total: format(total / num - num),
        orderInput: [...orderInput]
      };
      temp.orderInput.splice(i, 1);
      possibleSolution.push(temp);
    }
  }

  while(possibleSolution.length) {
    const copy = [...possibleSolution];
    for(let sequence of copy) {
      for(let j in sequence.orderInput) {
        const num = format(sequence.orderInput[j]);
        if(!(sequence.total % num)) {
          const temp = {
            possibleSolution: [...sequence.possibleSolution, num],
            total: format(sequence.total / num - num),
            orderInput: [...sequence.orderInput]
          };
          temp.orderInput.splice(j, 1);
          possibleSolution.push(temp);
          if(!temp.total) return format === parseInt
            ? temp.possibleSolution.reverse()
            : temp.possibleSolution.reverse().map(Number);
        }
      }
      possibleSolution.splice(possibleSolution.indexOf(sequence), 1);
    }
  }
  return "No solution found.";
}

function readArray(element, index, arr) {
  combination += (orderInput.indexOf(element) + 1).toString();
}

function main() {
  solution = findPerm(orderInput, total);
  solution.forEach(readArray);
  console.log(`The correct permutation is: ${JSON.stringify(solution)}\n` +
              `And the combination is: ${combination}`);
}

main();
