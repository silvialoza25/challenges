## Version 2.0
## language: en

Feature: 1408-H4CK1NG-enigmes-a-thematiques
  Code:
    1408
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    Mankar
  Goal:
    Find the combination to open a safe based on Javascript code

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu          | 20.04.2 LTS   |
    | Google Chrome   | 91.0.4472.114 |
    | Python          | 3.8.5         |
  Machine information:
    Given I am accessing to the website
    And the website shows a row of twenty checkboxes

  Scenario: Fail: Inspect webpage source code and try to brute-force the result
    Given I have solved the first version of this challenge
    When I inspect the webpage using browser Dev Tools
    Then I find that it works the same as the other challenge did
    But this time the searched value is a very long integer [evidence](01.png)
    When I update the Python program I used to solve the first challenge
    And I execute it
    Then my machine freezes due to a lack of memory
    And I realize I can't brute-force the result this time

  Scenario: Success: Write a more efficient algorithm to solve the formula
    Given I have found the formula that describes the challenge
    When I try to fix my original code to solve the problem
    Then I manage to design a more efficient algorithm [evidence](02.py)
    When I execute the program
    Then I obtain the correct sequence
    When I execute the program using the data from the first challenge
    Then it shows the correct answer
    And I confirm the program actually can solve both challenges
    When I map the obtained sequence for this problem to the checkboxes
    And I click the boxes in that order on the webpage
    Then the website shows a congratulations message
    And an image of an open safe appears [evidence](03.png)
    And I caught the flag
