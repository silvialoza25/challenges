## Version 2.0
## language: en

Feature: 1408-H4CK1NG-enigmes-a-thematiques
  Code:
    1408
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Calculate the combination and obtain the flag

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1408
    """
    And I already solved the previous challenge Coffre-fort
    And I can see twenty checkboxes

  Scenario: Fail: Use the same program that I coded to solve Coffre-fort
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1408
    """
    When I right-click any checkbox
    And I select the inspect button
    Then I can see in the sources tab of the developer tools the code
    And I can see that after a checkbox is selected a function is called
    When I open the following web page to see the source code
    """
    https://enigmes-a-thematiques.fr/web/enigmes/epreuve1408/index_v3.php
    """
    And I press the buttons' combination Ctrl + u
    Then I can see the source code of the challenge
    And every function call has a different value [evidence](1.png)
    When I read this part of the condition in the code
    """
    if(bigInt(total).toString() == "4806479949511721651869051424302") {...}
    """
    Then I modify the values in the program that I coded before
    And I changed a few things to make it work properly [evidence](2.png)
    When I run the code
    Then I notice it takes too long
    And I think it will never achieve its goal
    When I decide to debug the code
    Then I realize that the total is denoted with scientific notation
    And I Know that never is going to be the total requested [evidence](3.png)
    And I have not completed the challenge

  Scenario: Success: Code a program to decipher the combination
    Given I open the web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/1408
    """
    When I right-click any checkbox
    And I select the inspect button
    Then I can see in the sources tab of the developer tools the code
    And I can see that after a checkbox is selected a function is called
    When I open the following web page to see the source code
    """
    https://enigmes-a-thematiques.fr/web/enigmes/epreuve1408/index_v3.php
    """
    And I press the buttons' combination Ctrl + u
    Then I can see the source code of the challenge
    And every function call has a different value [evidence](1.png)
    When I read this part of the condition in the code
    """
    if(bigInt(total).toString() == "4806479949511721651869051424302") {...}
    """
    And I make a list with the values
    """
    [1,5,7,11,13,17,19,23,31,43,47,53,59,61,67,71,73,79,83,89]
    """
    Then I notice that the number is too big that needs to identify as BigInt
    And I realize that the list has a pattern
    And I realize that the list is composed of odd numbers
    And I can see that the list only has prime numbers
    When I read the algorithm that calculates to compare the total
    """
    total = (bigInt(total).add(val)).times(val);
    """
    Then I can interpret that corresponds to the following equation
    """
    total = (total + val) * val
    """
    When I think about making a code to calculate the correct permutation
    And I try to calculate the permutation using the final total
    And I focus on creating an algorithm to make the total value zero
    Then I isolate the total variable on the right side
    And I have the following equation
    """
    total = (total / val) - val
    """
    When I think about the previous equation
    Then I realize that the order matters
    And the total has to be divisible between any value of the permutation
    When I notice the complexity of the algorithm
    Then I decide to try with the following input
    """
    list = [1, 5, 7, 11]
    total = 2596
    """
    And I can analyze the problem easily
    When I extract the minimum value from the list
    And I calculate the total
    Then I can get the maximum total
    When I extract the maximum value from the list
    Then I can get the minimum total
    And I get this output [evidence](4.js)
    When I create a copy of this output
    And I repeat the process getting maximum and the minimum
    Then I get this output [evidence](5.js)
    And I know that the lists created are possible solutions
    When I repeat the process until getting only one possible solution
    Then I get this output [evidence](6.js)
    When I reverse the solution list
    And I try this equation
    """
    total = (total + val) * val
    """
    And I know that this compares the total of the list with the total requested
    Then I get the correct permutation
    When I use the values in the order of the correct permutation
    And I get each index of those values in the original list
    Then I can get the combination [evidence](7.js)
    When I finish the code in Javascript [evidence](8.js)
    And I run the program with the original list and the requested total
    Then I can get the correct combination [evidence](9.png)
    When I select the checkboxes in the right order
    Then the page displays a popup [evidence](10.png)
    When I click the accept button
    Then the page displays an image [evidence](11.png)
    When I submit the flag
    Then the web page displays the following message [evidence](12.png)
    """
    Bravo J!
    """
    And I have completed the challenge
