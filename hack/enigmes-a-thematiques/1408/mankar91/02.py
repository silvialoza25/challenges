from typing import List

def sequence(prime_list: List[int], value: int, index: int, \
sequence_list: List[int]) -> List[int]:

  if value == 0:
    sequence_list.reverse()
    return sequence_list

  if index == len(prime_list):
    return []

  current_list = prime_list[:]
  current_sequence = sequence_list[:]
  prime = current_list[index]

  if value % prime == 0:

    current_list.remove(prime)
    current_sequence.append(prime)

    result = sequence(current_list, (value // prime) - prime, \
      0, current_sequence)

    if len(result) == 0:
      return sequence(prime_list, value, index + 1, sequence_list)
    return result

  else:
    return sequence(current_list, value, index + 1, current_sequence)

primes = [1,5,7,11,13,17,19,23,31,43,47,53,59,61,67,71,73,79,83,89]
value = 4806479949511721651869051424302
print(sequence(primes[:], value, 0, []))
