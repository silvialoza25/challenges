## Version 2.0
## language: en

Feature: 32-H4CK1NG-enigmes-a-thematiques
  Code:
    32
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Find the flag

  Background:
  Hacker's software:
    |    <Software name>  |   <Version>   |
    |  Kali Linux         | 20.04 LTS     |
    |  Firefox            | 88.0.1(64-bit)|
    | User-Agent Switcher | 1.4.1         |
  Machine information:
    Given the web page
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep35.php
    """

  Scenario: Fail: Change User-Agent with browser add-on, wrong configuration
    Given the provided web page shows the message
    """
    Laisse-moi deviner le navigateur que tu utilises... : Mozilla/5.0 Firefox.
    Bon choix ! Bon, maintenant, voici en quoi consiste ton épreuve...
    Il paraît qu'il existe un navigateur super-méga-giga bien qui s'appelle
    Safachromox et qui est téléchargeable sur un serveur des îles Caïman.
    Ta mission, c'est de le télécharger et d'ouvrir cette page avec ce
    navigateur du futur. Si tu arrives à faire cette mission, grâce à ce
    navigateur, tu pourras voir le mot de passe de cette épreuve !
    """
    Then I install a browser add-on to allow User-Agent change
    """
    User-Agent Switcher
    """
    When I configure the add-on's User-Agent options
    And I fill in the blanks with the information [evidence](01.png)
    """
    Label: Safachromox
    Category:
    String:
    """
    And I reload the Page
    Then I see the same message as above in a blank page [evidence](02.png)
    And I could not capture the flag
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Success: Change User-Agent with browser add-on, right configuration
    Given the provided web page shows the message
    """
    Laisse-moi deviner le navigateur que tu utilises... : Mozilla/5.0 Firefox.
    Bon choix ! Bon, maintenant, voici en quoi consiste ton épreuve...
    Il paraît qu'il existe un navigateur super-méga-giga bien qui s'appelle
    Safachromox et qui est téléchargeable sur un serveur des îles Caïman.
    Ta mission, c'est de le télécharger et d'ouvrir cette page avec ce
    navigateur du futur. Si tu arrives à faire cette mission, grâce à ce
    navigateur, tu pourras voir le mot de passe de cette épreuve !
    """
    When I properly configure the add-on's User-Agent options
    And I fill in the blanks with the complete information [evidence](03.png)
    """
    Label: Safachromox
    Category: Desktop
    String: Safachromox
    """
    And I reload the Page
    Then I see the message [evidence](04.png)
    """
    Wahou ! T'as vu comment il roxx ton nouveau navigateur !?!
    :p Pour valider l'épreuve, il te suffit de rentrer le mot '"<flag>"'.
    """
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
