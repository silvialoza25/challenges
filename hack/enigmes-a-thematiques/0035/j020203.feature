## Version 2.0
## language: en

Feature: 35-H4CK1NG-enigmes-a-thematiques
  Code:
    35
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find a way to obtain the flag

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep35.php
    """
    And I see the requirements to obtain the flag

  Scenario: Success: Modify the User-Agent in the web request
    Given I open the web page
    And I read the hint to obtain the flag [evidence](1.png)
    When I realize that the page displays my User-Agent Information
    Then I know I have to modify this parameter to the required one
    And I see that the User-Agent required is 'Safachromox'
    When I use the tool cUrl to change the User-Agent
    And I open the linux terminal
    And I use the following commands
    """
    url=""https://enigmes-a-thematiques.fr/epreuves/ep_hack/ep35.php
    curl -A "Safachromox" $url
    """
    Then I can see in the output the answer the flag [evidence](2.png)
    When I open the ctf page
    And I submit the answer
    Then the web page displays the message
    """
    Bravo J!
    """
    And I have completed the challenge
