## Version 2.0
## language: en

Feature: 35-H4CK1NG-enigmes-a-thematiques
  Code:
    35
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    andres568
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  Machine information:
    Given I am accessing the website

  Scenario: Fail:search-browser
    Given I am on the main page
    And I can see the message
    """
    Il paraît qu'il existe un navigateur super-méga-giga bien qui s'appelle
    Safachromox et qui est téléchargeable sur un serveur des îles Caïman.
    Ta mission, c'est de le télécharger et d'ouvrir cette page avec ce
    navigateur du futur.
    """
    When I search for the "Safachromox" browser
    Then I do not find the browser on the internet
    And I can not capture the flag

  Scenario: Success:change-http-headers
    Given I am on the main page
    When I inspect the website with the DevTool of Chrome
    And I look out for the browser information into the header of the request
    Then The header contains the following field
    """
    user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36
    (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36
    """
    When I search for a way to change the "user-agent" parameter
    Then I find the tool Network condition inside Chrome
    When I set "Safachromox"  as the customs value for "user-agent"
    And I refresh the website
    Then I can see the "<FLAG>" to pass the challenge
    When I enter the "<FLAG>" into the challenge form
    And I press the "Valider" button
    Then I can see the message "Bravo"
    And I capture the flag
