## Version 2.0
## language: en

Feature:
  Site:
    https://enigmes-a-thematiques.fr/front/enigmes
  Category:
    cryptography
  User:
    AlejandroTL (wechall)
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given a message [evidence](01.png)

  Scenario: Fail: Using magic operation
    Given the message
    When I access to http://icyberchef.com/
    And I put the message as input
    And I use "Magic" operation
    Then I can't decrypt the message [evidence](02.png)
    And I can't solve the challenge

  Scenario: Success: Using Ook decoder
    Given the message
    And I notice it looks like ook language
    When I access to https://www.splitbrain.org/_static/ook/
    And I put the message as input [evidence](03.png)
    And I press the button "Ook! to Text"
    Then I get a message which indicates a password [evidence](04.png)
    And I try to submit it as the flag
    And I solve the challenge [evidence](05.png)
