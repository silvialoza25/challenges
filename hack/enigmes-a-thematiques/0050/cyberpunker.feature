## Version 2.0
## language: en

Feature: 50-cryptographie-enigmes-a-thematiques
  Code:
    50
  Site:
    enigmes-a-thematiques
  Category:
    cryptography
  User:
    thecyberpunker
  Goal:
    Decode the string

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Parrot OS       | 4.11          |
    | Chrome          | 89.0.4        |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://enigmes-a-thematiques.fr/front/enigme/50
    """
    And I see a type of encoding string [evidence](1.png)

  Scenario: Fail: Try to decode with Cyberchef
    Given the string of the website
    When I try to identify the string
    And I realize about BrainFuck encoding
    When I go to "Cyberchef"
    """
    https://cyberchef.immersivelabs.online/
    """
    And I try to decode the string
    And I can not decode [evidence](2.png)

  Scenario: Success: Try to decode with BrainFuck Interpreter
    Given the string from the website
    And I realize about the type of string is BrainFuck
    Then I go to "Dcode.fr"
    """
    https://www.dcode.fr/brainfuck-language
    """
    And I put the string to decode
    Then I decode the string [evidence](3.png)
    When I go to the website to validate the decoded string
    And I enter the decoded string
    And I got a message "Bravo thecyberpunker!"
    And I solve the challenge [evidence](4.png)
