## Version 2.0
## language: en

Feature: 79-H4CK1NG-enigmes-a-thematiques
  Code:
    79
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Find password to get the flag

  Background:
  Hacker's software:
    |   <Software name>  |   <Version>   |
    | Kali Linux         | 20.04 LTS     |
    | Firefox            | 88.0.1(64-bit)|
    | HTTrack            | 3.49-2        |
    | Visual Studio Code | 1.55.2        |
  Machine information:
    Given the webpage
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve79/admin.php
    """

  Scenario: Fail: run password value from web page script
    Given the page shows a "prompt box" asking for a password
    And I don't type anything into it
    And I press "OK"
    Then the page displays a "prompt" box with the message
    """
    Ce n'est pas le bon mot de passe !
    """
    When I click "OK"
    Then the page loads a background
    When I inspect the webpage
    Then I find the function for password validation [evidence](01.png)
    When I see the value of the variable for password validation
    And I reload the page
    Then I type it into the validation box and click for validation
    And I get the message
    """
    Ce n\'est pas le bon mot de passe...
    """
    And I could not capture the flag
    And I conclude I can't use this method
    And I could not capture the flag

  Scenario: Success: Analyze page's code locally
    Given the page shows a "prompt box" asking for a password
    And I don't type anything into it
    And I press "OK"
    Then the page displays a "prompt" box with the message
    """
    Ce n'est pas le bon mot de passe !
    """
    When I click "OK"
    Then the page loads a background
    When I download the page to my computer using HTTrack
    And I see there is a .js file amog the downloaded files [evidence](02.png)
    And I open the .js file with Visual Studio Code
    Then I see the code for password validation [evidence](03.png)
    And I see that the value of the variable password is different
    When I paste this new value into the "prompt box" of the reloaded page
    Then I see the displayed message [evidence](04.png)
    """
    Félicitations !
    Le mot de passe <flag>
    """
    When I go back to the challenge's page
    And I type the "<flag>" into the validation box and click for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
