## Version 2.0
## language: en

Feature: 079-H4CK1NG-enigmes-a-thematiques
  Code:
    079
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    slayfer1112
  Goal:
    Use the knowledge about javascript to get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
  Machine information:
    Given I am accessing to the website
    And the website show a form for authenticating

  Scenario: Fail:use-script-html-content
    Given I am on the challenge page
    When I see a field to write the password
    Then I try writing anything
    And I see a popup that say "Ce n'est pas le bon mot de passe..."
    And I press "Aceptar"
    When I try to inspect the page
    Then I see that the page have an script with a password
    And I try the password in the popup
    When I see that the password fail

  Scenario: Success:search-script-in-source
    Given The first script don't have the real flag
    When I think that the popup appear before the content of the body
    And I try to see if i can found any clue in "<head>"
    When I see some scripts in "<head>"
    Then I try to search in the sources of the page
    And I found one of the scripts "javascript.js"
    When I see the script i found the password
    Then I reload the page and try again with this password
    And I was redirect to another page "vdsnsd.php" that congrats me
    When I see in this page a message with the flag
    And I read "Le mot de passe <FLAG> à l'envers : <GALF>..."
    Then I translate it and read the message
    And I understand that the password is reading from right to left
    When I go to the authentication page
    Then I write the password in the authenticating form
    And I se a message with "Bravo slayfer1112 !"
    And I capture the flag
