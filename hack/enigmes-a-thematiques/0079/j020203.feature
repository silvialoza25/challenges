## Version 2.0
## language: en

Feature: 79-H4CK1NG-enigmes-a-thematiques
  Code:
    79
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Find the correct text

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following page
    """
    https://enigmes-a-thematiques.fr/epreuves/epreuve79/admin.php
    """
    And I see a popup that require a password

  Scenario: Fail: View the web page source code
    Given the popup that require a password [evidence](1.png)
    And I do not know the password
    When I click the 'Cancel' button
    Then the page displays a new popup with the message [evidence](2.png)
    """
    Ce n'est pas le bon mot de passe !
    """
    When I click the 'Accept' button
    Then the page loads the background image
    When I right click the page
    And I select the 'View Page Source' option
    Then I can see the following javascript code [evidence](3.png)
    """
    <script language="JavaScript">/*
    password=prompt('Mot de passe :')
    if(password=='fausse_apparence')
      alert("Mot de passe correct, tu peux maintenant valider l'épreuve !")
    else
      alert("Essaye encore !")
    */</script>
    """
    Then I copy 'fausse_apparence'
    When I reload the web page
    Then the web page displays the popup that require the password
    When I paste the text previously copied [evidence](4.png)
    And I click the 'Accept' button
    Then the page displays the same popup [evidence](5.png)
    And I have not completed the challenge

  Scenario: Succes: Use developer tools to inspect the web page sources
    Given the popup that require a password [evidence](1.png)
    And I do not know the password
    When I click the 'Cancel' button
    Then the page displays a new popup with the message [evidence](2.png)
    """
    Ce n'est pas le bon mot de passe !
    """
    When I click the 'Accept' button
    Then the page loads the background image
    When I use the developers tools
    And I open the sources tab
    Then I expand the 'epreuve79' folder [evidence](6.png)
    And I can see files that the folder contains
    And I see another file besides the main page file 'admin.php'
    When I open the 'javascript.js' file
    Then I can see the following javascript code [evidence](7.png)
    """
    <!--
    var password;
    password=prompt("Mot de passe :","");
    if (password=="...")
    {
      ...
    }
    else
    {
      alert("Ce n'est pas le bon mot de passe !");
    }
    //-->
    """
    When I copy the text with which password is compared
    And I reload the web page
    Then the web page displays the popup that require the password
    When I paste the text previously copied [evidence](8.png)
    And I click the 'Accept' button
    Then the web page redirects itself to a secret web page [evidence](9.png)
    And I can see the password required
    When I open the ctf page
    And I submit the answer
    Then the web page displays the message [evidence](10.png)
    """
    Bravo J!
    """
    And I have completed the challenge
