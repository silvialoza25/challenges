## Version 2.0
## language: en

Feature: 31-H4CK1NG-enigmes-a-thematiques
  Code:
    31
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    RokkuX
  Goal:
    Decrypt text

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    | Kali Linux      | 20.04 LTS     |
    | Firefox         | 88.0.1(64-bit)|
  Machine information:
    Given the text
    """
    ff9830c42660c1dd1942844f8069b74a
    """

  Scenario: Success: discover text's hash type and decypher
    Given I check the text structure
    And I see it may be a hash
    When I analyze it online with
    """
    https://www.tunnelsup.com/hash-analyzer/
    """
    Then I see that the hash type is MD5 or MD4
    When I decypher the hash with [evidence](01.png)
    """
    https://www.md5online.org/md5-decrypt.html
    """
    Then I obtain the answer "<FLAG>" [evidence](02.png)
    When I return to the challenge's page
    And Input the password for validation
    Then Page displays the message "Tu as déjà résolu cette énigme !"
    And I solved the challenge
    And I captured the flag
