## Version 2.0
## language: en

Feature: 31-H4CK1NG-enigmes-a-thematiques
  Code:
    31
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    J
  Goal:
    Decrypt the encoded text

  Background:
  Hacker's software:
    | <Software name> |        <Version>       |
    | Kali Linux      | 2021.2                 |
    | Chrome          | 91.0.4472.106 (64-bit) |
  Machine information:
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/31
    """

  Scenario: Fail: Use the online decrypter CyberChef
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/31
    """
    When I copy the encrypted text [evidence](1.png)
    And I use the following page to identify the encryption type
    """
    https://gchq.github.io/CyberChef/
    """
    And I select the Magic module
    And the page analyze the encrypted text
    Then I see the results [evidence](2.png)
    And I know that the page is not going to decrypt correctly the text
    And I have not completed the challenge

  Scenario: Success: Use the online decrypter dCode
    Given I open the following web page
    """
    https://enigmes-a-thematiques.fr/front/enigme/31
    """
    When I copy the encrypted text [evidence](1.png)
    And I use the following page to identify the encryption type
    """
    https://www.dcode.fr/cipher-identifier
    """
    And the page analyze the encrypted text
    Then I can see that the most possible is MD5 encryption [evidence](3.png)
    When I use the following page to decrypt the MD5 hash
    """
    https://www.dcode.fr/md5-hash
    """
    Then I can see the decoded password required [evidence](4.png)
    When I fill the textfield with the decoded password in the ctf web page
    Then the web page displays a rectangle with the message [evidence](5.png)
    """
    Mot de passe correct, tu peux maintenant valider l'épreuve !
    """
    And I have completed the challenge
