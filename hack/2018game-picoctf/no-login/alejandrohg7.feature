## Version 2.0
## language: en

Feature: no-login -web-explotation -2018game-picoctf
  Site:
    2018game.picoctf.com
  Category:
    web-explotation
  User:
    ahgonzalez7b
  Goal:
    log to the site without login page

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Firefox         | 69.0        |

  Machine information:
    Given I am accessing the web-page using the browser
    And I access to the challenge page

  Scenario: success : cookie manipulation
    Given I am logged in the challenge page
    When I look the source code of the challenge page
    Then I can see the cookies stored in my browser
    Given there is some interesting cookie called
    """
    {5998b1e2-aa8d-44cc-93d3-f361feee5df8}"
    """
    When I try to change the name of the cookie to "admin"
    Then I click the flag button
    And I get the flag
