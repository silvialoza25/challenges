# language: en

Feature: Solve the challenge 4
  From the hacksithissite.org website
  From the extbasic category
  With my username siegfrieg94

  Background:
    Given a script in Pearl with a security flaw 
    And I have to detect the flaw and fix it
    And I have access to internet

  Scenario: Successful Solution
    When I read the script
    Then I do not know what is the sintaxis in Pearl
    Then I read about Pearl sintaxis in internet
    Then I compare the given code with the internet information
    Then I find that compare string variables in Perl is with "eq" and not with "=="
    Then I rewrite the flaw line
    And I solve the challenge

