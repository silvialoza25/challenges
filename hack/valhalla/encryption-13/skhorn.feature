# language: en

Feature: Solve Encryption challenge 13
  From site Valhalla
  From Encryption Category
  With my username Skhorn

  Background:
    Given the fact i have an account on Valhalla site
    And i have Debian 9 as Operating System
    And i have internet access

  Scenario: Failed attempt
    Given the link to the challenge
    And a statement about vikings in 1940
    And a text in a non-known language
    And a latin phrase
    And i search for the first word on google
    And i see it is related with the navajo cipher
    And i translate each navajo word into its representation
    And i get an english phrase
    Then i use this phrase as the answer
    And i fail

  Scenario: Succesful Solution
    Given the link to the challenge
    And a statement about vikings in 1940
    And a text in a non-known language
    And a latin phrase
    And i search for the first word on google
    And i see it is navajo
    And i translate each navajo word into its representation
    And i get an english phrase
    And search for it in google
    And i see it is related with a latin phrase
    Then i use the latin phrase as answer
    And i solve the challenge
