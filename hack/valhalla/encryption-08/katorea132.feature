## Version 2.0
## language: en

Feature: encryption9
  Site:
    Valhalla
  Category:
    crypto
  User:
    Katorea132
  Goal:
    Reverse the encryption of the flag

  Background:
    Hacker's software:
        | <Software name> | <Version>   |
        | Windows         |     10      |
        | Firefox         |   82.0.3    |

  Machine information:
    Given I have access to Valhalla
    And I have a Valhalla account
    And I have Windows 10 as OS
    And I have internet access

  Scenario: Fail: Use cypher tool
    Given I have a encrypted message
    And I have access to some of the plain text
    """
    Crypt Text: 112 422123 22 243243242
    Given Text: the answer is *********
    """
    When I try to use automatic tool to solve
    And it is called cipher identifier by boxentriq
    Then it says its octetal code
    When I separate it in groups of 2 and 3 digits
    Then transform it octal to text cryptii tool
    And returns a string only readable with UTF-8 encoding
    """
    112 422 123 222 432 432 42
    JĒSĚĚ\"
    """
    When I try it as a flag
    And it fails

  Scenario: Success: Analyze the Encryption
    Given I see the encrypted message
    And I have access to some of the plain text message
    """
    Crypt Text: 112 422123 22 243243242
    Given Text: the answer is *********
    """
    When I see a pattern with numbers corresponding to repetitions of letters
    And I can start replacing the already known letters on each number
    When I get *ar*ar*a*
    Then I try to place the remaining known letters
    And get *ar*arian
    Then I search for 9 letter words ending with rian in google
    And I know that the missing 2 letters are in fact the same letter
    When the search result shows only 1 match to these conditions
    Then I select it as the flag[evidence](google.png)
    And it was the flag[evidence](solution.png)
