# language: en

Feature: Solve Debugging challenge 2
  From site Valhalla
  From Debugging Category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am using VIM - Vi IMproved 8.0
  And I am using Python 2.7.14+
  Given a web site with a Python code to debugg
  And the download link of the source code
  """
  URL: https://halls-of-valhalla.org/beta/challenges/debugging5
  Message: What the hell? My Java code breaks when I try to run
  it? What's wrong and how can I fix it?
  Objective 1: Find the error and fix it
  Evidence: debugging2.java
  """

Scenario: Revieweing and patching the code
  Given the source code of the challenge
  And I need to find out why it is not working as expected
  And I compile/execute to see its behavior
  """
  # Compiling & Executing
  $ javac debuggin2.java
  $ java -Xmx128M -Xms16M debugging2
  Exception in thread "main" java.lang.NullPointerException
    at debugging2.main(debugging2.java:40)
  $ gcc -o debugging1 debugging1.c
  """
  When I look at the error
  And I see there is a NullPointerException involved
  And it is related with a variable not initializated
  """
  35  public static void main(String[] args)
  36  {
  37    Thingy[] thingyLog;
  38    thingyLog = new Thingy[3];
  39
  40    thingyLog[0].increaseWatermelon(17);
  """
  Then I see Thingy[] vector, has not been properly initialized
  And I fix this by initializing each position of the vector
  And using the object is referencing
  """
  37    Thingy[] thingyLog;
  38    thingyLog = new Thingy[3];
  39
  40    thingyLog[0] = new Thingy();
  41    thingyLog[1] = new Thingy();
  42    thingyLog[2] = new Thingy();
  """
  Then I compile/execute to see its behavior
  """
  # Compiling & Executing
  $ javac ThingyMain.java
  $ java -Xmx128M -Xms16M ThingyMain
  Watermelon: 17 Cantaloupe: -3
  Watermelon: 13 Cantaloupe: 2
  Watermelon: 234 Cantaloupe: 33
  """
  And now its executing in the intended way
  And I solve the challenge
