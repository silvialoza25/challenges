## Version 2.0
## language: en

Feature: 07-stegano-valhalla
  Site:
    halls-of-valhalla.org
  Category:
    Steganography
  User:
    ununicornio
  Goal:
    Find the flag hidden in the given image

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows         | 10            |
    | Google Chrome   | 69.0.3497.100 |
    | HxD Hex Editor  | 2.1.0         |

  Scenario: Fail:Exif-data
    Given I downloaded the challenge image
    And I open it with the Extract EXIF tool found in CyberChef
    """
    https://gchq.github.io/CyberChef
    """
    Then I get the output
    """
    Found 3 tags.
    ModifyDate: 1364582999
    undefined: 1
    XPComment: 80,0,97,0,115,0,115,0,119,0,111,0,114,0,100,0,58,0,32,0,101,0,97,
    0,115,0,121,0,0,0
    """
    Then I see nothing interesting

  Scenario: Success:Hex-data
    Given I downloaded the challenge image
    And I open it with HxD
    Then I get the Hex dump of the image
    """
    ...
    50 00 61 00 73 00 73 00 77 00 6F 00 72 00 64 00 P�a�s�s�w�o�r�d�
    3A 00 20 00 65 00 61 00 73 00 79 00 00 00 FF E1 :� �e�a�s�y���ÿá
    08 DD 68 74 74 70 3A 2F 2F 6E 73 2E 61 64 6F 62 Ýhttp://ns.adob
    65 2E 63 6F 6D 2F 78 61 70 2F 31 2E 30 2F 00 3C e.com/xap/1.0/�<
    3F 78 70 61 63 6B 65 74 20 62 65 67 69 6E 3D 27 ?xpacket begin='
    EF BB BF 27 20 69 64 3D 27 57 35 4D 30 4D 70 43 ï»¿' id='W5M0MpC
    65 68 69 48 7A 72 65 53 7A 4E 54 63 7A 6B 63 39 ehiHzreSzNTczkc9
    64 27 3F 3E 0D 0A 3C 78 3A 78 6D 70 6D 65 74 61 d'?><x:xmpmeta
    20 78 6D 6C 6E 73 3A 78 3D 22 61 64 6F 62 65 3A  xmlns:x="adobe:
    6E 73 3A 6D 65 74 61 2F 22 3E 3C 72 64 66 3A 52 ns:meta/"><rdf:R
    44 46 20 78 6D 6C 6E 73 3A 72 64 66 3D 22 68 74 DF xmlns:rdf="ht
    74 70 3A 2F 2F 77 77 77 2E 77 33 2E 6F 72 67 2F tp://www.w3.org/
    31 39 39 39 2F 30 32 2F 32 32 2D 72 64 66 2D 73 1999/02/22-rdf-s
    79 6E 74 61 78 2D 6E 73 23 22 2F 3E 3C 2F 78 3A yntax-ns#"/></x:
    78 6D 70 6D 65 74 61 3E 0D 0A 20 20 20 20 20 20 xmpmeta>
    ...
    """
    And I see """P.a.s.s.w.o.r.d.:.e.a.s.y"""
    Then I input """easy""" as my challenge answer
    Then I pass the challenge.
