# language: en

Feature: Solve Timed 1
  From site Valhalla
  From Timed Category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am using python Python 2.7.14+
  And I am using VIM - Vi IMproved 8.0
  And I am running Burp Suite Community edition v1.7.30
  Given a web site with description, details + one hint
  """
  URL: https://halls-of-valhalla.org/challenges/timed/timed1.php
  Message: You have a total of 2 seconds to solve this challenge
  Details: bubble Sort, comparison and swaps count
  Objective: Find x in less than 5 seconds
  Evidence: - php site
            - BubbleSort pseudocode
            - Messy number array
            - Less than 2 seconds to answer
  """

Scenario: Sorting
As stated before, the total time to achieve this, is in less
than 2 seconds
  Given the libraries <beautifulSoup> and <requests> from python
  Then I code a <connect> function:
  """
  Source: valhalla/timed/timed-1/skhorn.py
  20  def connect(self, url, cookie, payload="", proxies=""):
  25    response = requests.post(url,
  26                           cookies=cookie,
  27                           data=payload,
  28                           verify=False).text
  29    return response
  """

  And I use it, to retrieve data from the web page
  But I need to prepare connection parameters:
  """
  Source: valhalla/timed/timed-1/skhorn.py
  17  url = [ 'halls-of-valhalla.org/challenges/timed/timed1.php' ]
  23  cookie = { cookies } => Dict session cookies, better include them allx
  """

  And I perform the first request
  Then I get as a response, the entire html of the challenge:
  """
  <pre style='width:400px; text-align:left;'>Your task is to calculate the
  number comparisons and moves this algorithm performs on the data below.
  A 'comparison' in this instance is the check done on line 5. A 'move' is
  the process of swapping two array items (lines 7-9). You have 2 seconds.
  </pre>
  <table border='1'>
  <tr>
   <td>46</td>
    ...
    ...
    ...
  """
  And I need to get the array numbers
  And I search the <table[tr]> using <beautifulSoup>
  Then I get the table containing the numbers
  And I process it to fill an array with them
  """
  # Proccesing & Extraction
  Source: valhalla/timed/timed-1/skhorn.py
  77  chall_array = []
  78  for item in td_array:
  79    chall_array.append(item.get_text().encode('ascii'))
  """

  And I get an array:
  # Messy array
  |-------------------------------------------------------------------|
  | array = ['2', '19', '14', '4', '24', '14', '3', '22', '44', '35'] |
  |-------------------------------------------------------------------|

  Then I need to sort the array
  And I need to count comparisons + swaps
  And I use the following algorithm
  """
  # Bubble Sort function
  Source: valhalla/timed/timed-1/skhorn.py
  46  move = 0
  47  comp = 0
  49  for i in range(len(data_array)):
  50    for index in range(len(data_array)-i-1):
  51        comp = comp + 1
  52        if int(data_array[index+1]) < int(data_array[index]):
  53            tmp = data_array[index+1]
  54            data_array[index+1] = data_array[index]
  55            data_array[index] = tmp
  56            move = move + 1
  59  return [comp, move]
  """

  When I execute the sorting algorithm
  Then I get the following:
  # Sorting
  |-----------------------------------------------------------|
  | ['2', '3', '4', '14', '14', '19', '22', '24', '35', '44'] |
  |-----------------------------------------------------------|
  | COMPARISON DONE  45                                       |
  | MOVES DONE       12                                       |

  Then I need to craft another request
  And I include the result values as a payload
  # Payload dict
  """
  Source: valhalla/timed/timed-1/skhorn.py
  84  payload = {
  85      'comparisons': solution[0],
  86      'moves': solution[1],
  87      'submit': 'Check',
  88  }
  """
  And I send the new crafted request
  Then I solve the challenge
  """
  # Response
  </pre>
  <p>Correct! You have received 50 points.</p>
  <div style='width:400px; text-align:left'>
    ...
    ...
  """
