
## Version 2.0
## language: en

Feature: gif-animado-i
  Site:
    https://wow.sinfocol.org/?page=retos&id=42#/retos/id=42
  User:
    alestorm980 (wechall)
  Goal:
    Find the flag in the GIF file

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04       |
    | Python          | 2.7.15+     |
  Machine information:
    Given The challenge description
    Then I see a ".gif" file
    And I download the file "gif_animado_i.gif"

  Scenario: Fail: Command strings in the file
    Given The file
    When I run the command "strings" in the file
    """
    $ strings gif_animado_i.gif
    """
    Then I get useless information

  Scenario: Success: Extract frame from the GIF
    Given The file
    When I see something strange in one frame
    Then I decide to extract each frame
    And Try to analyse them
    When I create a Python script "frames.py"
    Then I iterate over each frame
    And I extract it
    """
    $ python frames.py
    Images saved
    """
    When I have all the frames
    Then I can read a sentence inside the "4.gif" file
    And I get the flag
