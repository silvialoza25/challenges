## Version 2.0
## language: en

Feature: 34 - steganos - world of wargame
  Code:
    34
  Site:
    https://wow.sinfocol.org/
  Category:
    steganos
  User:
    idleryan
  Goal:
    Find the password hidden with Alternate Data Streams (ADS)

  Background:
  Hacker's software:
    | <Software name>     | <Version>         |
    | Windows             | 10.0.2004 (x64)   |
    | Chromium            | 85.0.4240.99      |
    | WinRAR              | 5.91              |
    | AlternateStreamView | 1.56 (x64)        |
  Machine information:
    Given I am accessing the challenge site through my browser
    And i use my credentials to start the challenge

  Scenario: Fail:.txt saved as .png
    Given an "educacion_fundamental.txt" file on "flujo_de_datos.rar" file
    When I extract .rar in a folder with the same name
    Then I open command prompt to display ADS in folder
    And I get the output
    """
    ...
    08/14/2009        330,550 educacion_fundamental.txt
                       59,301 educacion_fundamental.txt:Zone.Identifier:$DATA
    ...
    """
    When I try to access ADS by using the command
    """
    notepad educacion_fundamental.txt:Zone.Identifier
    """
    Then I save the ADS found in .png format
    But the image does not open
    And the challenge remains unsolved

  Scenario: Success:Using AlternateStreamView to export hidden .png
    Given an "educacion_fundamental.txt" file on "flujo_de_datos.rar" file
    When I extract .rar in a folder with the same name
    Then I scan the folder with AlternateStreamView
    And I get the stream "Zone.Identifier:$DATA" located in .txt file
    When I export the previous stream in .png format
    Then I get the image where the password remains[evidence](juaxor.png)
    And I found a possible flag
    And the challenge is solved
