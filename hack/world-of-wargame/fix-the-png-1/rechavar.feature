## Version 2.0
## language: en

Feature: Steganos - fix the PNG 1 - World of Wargame

  Site:
    https://wow.sinfocol.org/
  Category:
    Steganos
  User:
    rechavar
  Goal:
    Fix the PNG

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          |    20.04    |
    | Google-chrome   |84.0.4147.125|
    | Notepad++       |    7.8.9    |

  Machine information:
    Given The challenge URL
    """
    https://wow.sinfocol.org/#/retos/id=27
    """
    And The challenge statement
    """
    Encontramos varios archivos PNG en una carpeta secreta de un terrorista.
    Las imágenes parecen almacenar información sobre las respuestas que los
    investigadores buscan. La siguiente imagen fue la primera que encontramos,
    parece que la cabecera está un poco perdida!
    """
  Scenario: fail: adding by hand
    Given a broken image
    Then I open it in a new tap
    And I see nothing
    When I download it
    And I open it with Notepad++ I can see [evidence](image1.png)
    Then I type
    """
    %PNG

    IHDR...
    """
    When I try to open it I get the following message
    """
    Fatal error reading PNG image file: PNG file corrupted by ASCII conversion
    """

  Scenario: Success: hexa editor
    Given a broken image
    Then I open it in a new tap
    And I see nothing
    When I download it
    And I open it with Notepad++ I can see [evidence](image1.png)
    Then I realized that the header is missing
    And I search on google for a hex editor
    When I find 'hexed.it' [evidence](image2.png)
    Then I upload the broken PNG
    And I add the missing 8 bytes for header[evidence](image3.png)
    """
    89 PNG 0D 0A 1A 0A....
    """
    When I download it
    And I open it I see the flag [evidence](image4.png)
    Then I submit the answer
    And I pass the challenge
