## Version 2.0
## language: en

Feature: 93 - steganos - world of wargame
  Code:
    93
  Site:
    https://wow.sinfocol.org/
  Category:
    steganos
  User:
    idleryan
  Goal:
    Find the password hidden in the .doc file with Word

  Background:
  Hacker's software:
    | <Software name>     | <Version>         |
    | Windows             | 10.0.2004 (x64)   |
    | Chromium            | 85.0.4240.99      |
    | MS Word 365         | 16.0.13231.20372  |
  Machine information:
    Given I am accessing the challenge site through my browser
    And i use my credentials to start the challenge

  Scenario: Fail:Check hidden text
    Given an "ofimatico.doc" file
    When I open the file in Word
    Then I see a lot of suspicious blank space
    When I select the whole document
    And I select the black font color fot the whole selected text
    Then I get the hidden text at beginning[evidence](hiddeninit.png)
    And also at the end[evidence](hiddenend.png)
    But the hidden text is joking at me
    And the challenge remains unsolved

  Scenario: Fail:Adjust brightness and saturation parameters to images
    Given an "ofimatico.doc" file
    When I open the file in Word
    Then I see a image located at the header
    And I see another image located nearly at the end of file
    When I edit the parameters of the images with arbitrary values
    Then I get the following edited images[evidence](params.png)
    But I can't see any relevant data in the images
    And the challenge remains unsolved

  Scenario: Success:Reset picture option to header image
    Given an "ofimatico.doc" file
    When I open the file in Word
    Then I see a image located at the header
    And I select the option 'Reset picture' to the header image
    And I found a possible hidden flag[evidence](passwd.png)
    And the challenge is solved
