## Version 2.0
## language: en

Feature: 56 - reconimg - world of wargame
  Code:
    56
  Site:
    https://wow.sinfocol.org/
  Category:
    reconimg
  User:
    idleryan
  Goal:
    Find the oriental symbol name

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.5 LTS |
    | Mozilla Firefox | 80.0.1      |
  Machine information:
    Given I am accessing the challenge site through my browser
    And i use my credentials to start the challenge

  Scenario: Fail:Obvious Answer
    Given an oriental symbol associated with yin-yang
    Then I take as obvious the answer by typing 'yin yang'
    But the answer is wrong
    And the flag is not yet caught

  Scenario: Fail:Internet Search (First Attempt)
    Given an oriental symbol associated with yin-yang
    Then I search on Google about the meaning of yin-yang
    And I found a possible answer 'tai chi'
    But the answer is wrong,
    And the flag is not yet caught

  Scenario: Success:Internet Search (Second Attempt)
    Given an oriental symbol associated with yin-yang
    Then I search on Google about the meaning of yin-yang
    And I found a possible answer 'taijitu'
    Then the challenge is solved
    And I caught the flag
