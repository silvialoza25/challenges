## Version 2.0
## language: en

Feature: 10 - steganos - world of wargame
  Code:
    10
  Site:
    https://wow.sinfocol.org/
  Category:
    steganos
  User:
    idleryan
  Goal:
    Find the flag hidden in the small bmp image

  Background:
  Hacker's software:
    | <Software name>     | <Version>         |
    | Windows             | 10.0.2004 (x64)   |
    | Chromium            | 85.0.4240.99      |
    | Paint               | Classic           |
  Machine information:
    Given I am accessing the challenge site through my browser
    And i use my credentials to start the challenge

  Scenario: Fail:Image in binary (0-1) text
    Given an image in .bmp format[evidence](dm.png)
    When I convert the image to binary text in the page
    """
    https://www.dcode.fr/binary-image
    """
    Then I get the following text
    """
    00000000000000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000000000000000000000000000000000
    01100000000000000000110000001100000000000000000000001100000000000000000110
    00000000000000000000000000001100000000000000000000001100000000000000000110
    00000000000000000000000000001100000011000000000000001100000000000000000110
    00000000000000000000000000001100000011000000000000001100000000000000000110
    00000000000000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000000000000000000000000000000000
    00000000000000000000000000000000000000000000000000000000000000000000000000
    """
    But I can't see any relevant data in the text
    And the challenge remains unsolved

  Scenario: Success:Using Paint to diff pixels
    Given an image in .bmp format[evidence](dm.png)
    When I open the image with Paint
    Then I use the bucket to fill with red color any black pixel
    And I found a possible hidden flag[evidence](uncovered.png)
    And the challenge is solved
