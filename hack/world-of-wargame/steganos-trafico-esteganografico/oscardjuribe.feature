## Version 2.0
## language: en

Feature: steganos-trafico-esteganografico
  Site:
    https://wow.sinfocol.org/#/retos/id=96
  User:
    alestorm980 (wechall)
  Goal:
    Get the secret message from the network capture

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali            | 4.19        |
    | Wireshark       | 2.6.8       |
  Machine information:
    Given the challenge description
    When I get a ".rar" file
    Then I decompress the file
    And I get a pcap file

  Scenario: Fail: Strings the pcap file
    Given a pcap file
    When I open a terminal
    Then I try the command "strings reto4.pcap | grep secret"
    And nothing happens

  Scenario: Success: Open the pcap on wireshark
    Given a pcap file
    When I open it with wireshark
    Then I find a ICMP package
    When I use the command to dump it
    """
    tshark -Y icmp -T fields -e data.data -r reto4.pcap |
    perl -pe 's/([0-9a-f]{2})/chr hex $1/gie' | tr -d :
    """
    Then I find some user data
    """
    el administrador nunca encontra este texto asi que para poder
    entrar en la pagina debe escribir la respuesta la cual es  estenografiaenred
    """
    And I get the password of the challenge
