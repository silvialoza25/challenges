## Version 2.0
## language: en

Feature: 23 - reconoce la imagen - world of wargame
  Code:
    23
  Site:
    https://wow.sinfocol.org/
  Category:
    Reconoce la imagen
  User:
    samjoo
  Goal:
    Find the exact location of the image given in the callenge to build the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows10       | 2004        |
    | Chromium        | 85.0        |
  Machine information:
    Given I am accessing the challenge through a web browser
    And I see a description section with an image and the following text
    """
    La escultura que podrán ver a continuación está hecha de una pizarra
    roja y verde, quarzo blanco, madera petrificada, imán natural y cobre.
    Cómo se llama la escultura y en qué estado de america del norte se
    encuentra?
    """
    Then I start the challenge

  Scenario: Fail:Find the image based on keywords
    Given I am in the challegen page
    """
    https://wow.sinfocol.org/?page=retos&id=23
    """
    And I see a few keywords in the challenge description
    Then I search in google those keywords
    """
    https://www.google.com/search?q=north+america+white+quartz+
    copper+natural+magnet+petrified+wood
    """
    And I could not find the image

  Scenario: Success:Find the image using a reverse image search
    Given I am in the challenge page
    And I see the image I need to search
    Then I take a screenshoot of it
    Then I use google's reverse image feature with it
    And I see google gives me back a Wikipedia page
    Then I enter this page and see the image I am looking for
    And I see the name of the sculpture and the location of it
    And I can build the flag with that information
    """
    kryptos_virginia
    """
