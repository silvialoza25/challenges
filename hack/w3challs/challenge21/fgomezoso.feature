## Version 2.0
## language: en

Feature: Challenge 21 - /dev/null - w3challs
  Code:
    21
  Site:
    www.w3challs.com
  Category:
    /dev/null
  User:
    fgomezoso
  Goal:
    Find the terms of a number series and use them as the key

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 10           |
     | Chrome          | 76.0.3809.132|
     | python          | 3.7.4        |
  Machine information:
    Given I am accessing the challenge site via browser
    And I am logged in as user in the website

  Scenario: Success:Programming the series
    Given the number series
    """
    0-1-1-2-3-5-8-13-...
    """
    And two specific terms of the number series
    """
    74
    150
    """
    When I think in the series logic
    Then I realize the 3rd number is the add of the two previous numbers
    And the next numbers follow the same pattern
    And the series is actually the Fibonacci sequence
    When I code the script "fgomezoso.py" that generates the series in python
    Then I get the values for both terms
    """
    | Terms | Values                         |
    | 74    | 806515533049393                |
    | 150   | 6161314747715278029583501626149|
    """
    When I enter the key as a pair of numbers
    """
    806515533049393-6161314747715278029583501626149
    """
    Then I solved the challenge
