## Version 2.0
## language: en

Feature: crypto-shadow-w3challs
  Site:
    w3challs
  Category:
    Cryptography
  User:
    jasomo
  Goal:
    Crack the root password using the provided /etc/shadow

  Background:
  Machine information:
    | <Software name>   |  <Version> |
    | VMWare ESX        |     6.5    |
    | Windows           |      10    |
    | Debian VM         |   10.4.6   |
    | Kali VM           |   2020.2   |
  Hacker Software:
    | JohnTheRipper     |    1.9.0   |

  Machine information:
    Given the challenge URL
    """
    https://w3challs.com/challenges/crypto/shadow
    """
    And the challenge information
    """
    During his last penetration testing session, one of our consultants
    successfully obtained root privileges on a Linux server using a local
    privilege escalation vulnerability, and retreived the /etc/shadow below.
    Your mission is to crack the root password.
    A good wordlist might be useful...
    """
    And the shadow file data
    """
    root:$1$.rquYmlo$yFWfaSKplZmp1Id2VZ6iT1:15758:0:99999:7:::
    daemon:*:14971:0:99999:7:::
    bin:*:14971:0:99999:7:::
    sys:*:14971:0:99999:7:::
    sync:*:14971:0:99999:7:::
    ...
    """
    And the field to submit the flag

  Scenario: Fail:John the Ripper
    Given the shadow file data
    Then I save it into a file on Kali called shadow.txt
    Then I run john without extra parameters but the file to crack
    """
    john shadow.txt
    """
    And it did not manage to crack the password

  Scenario: Success:John the Ripper
    Given the previous attempt did not work
    Then I try to look for a good password list
    And I download a password list file called rockyou.txt
    Then I run john specifying that file as wordlist
    """
    $ john shadow.txt --wordlist=rockyou.txt
    """
    Then I obtain the following result
    """
    $ john shadow --wordlist=rockyou.txt
    Using default input encoding: UTF-8
    Loaded 1 password hash (md5crypt, crypt(3) $1$ (and variants)
    [MD5 128/128 AVX 4x3])
    Press 'q' or Ctrl-C to abort, almost any other key for status
    sexybitch123     (root)
    1g 0:00:00:02 DONE (2020-08-14 09:10) 0.3802g/s 29630p/s
    29630c/s 29630C/s sexykelly..sept07
    """
    Then I conclude the password was cracked
    And I use the obtained password to catch the flag
    And I solve the challenge
