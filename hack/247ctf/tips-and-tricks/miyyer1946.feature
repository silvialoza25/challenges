## Version 2.0
## language: en

Feature: tips-and-tricks
  Site:
    https://247ctf.com/
  Category:
    Hack
  User:
    miyyer1946
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Windows         |      10     |
    | Raspberry pi OS |     5.4     |
    | Python          |    3.8.3    |
    | Curl            |    7.64.0   |

  Scenario: Fail: flag encoded in the server API-REST
    Given an API-REST connection socket to send requests
    Then I sent a request to the server using curl[evidences](01.py)
    """
    curl d2eafd7c038c5205.247ctf.com:50169

    Welcome to the 247CTF addition verifier!
    If you can solve 500 addition problems, we will give you a flag!
    What is the answer to 196 + 62?
    """
    Then it shows how to get the flag
    Given a large number of simultaneous connections "500"
    And the need to send the results of the sum of two parameters
    Then the steps to get the flag must be automated by means of a script
    Given curl does not allow to calculate the sum of the response parameters
    And also requires a programming language to interact 500 connections
    Then the error to get the flag persists

  Scenario: Success: sending 500 responses to the API-REST server with pyhton
    Given python's HTTP connection library
    Then I write a script for handling requests to the API-REST server
    And the 500 send responses to the api-rest [evidences](tips-and-tricks.py)
    Then I run the python script
    Then I can actually read the flag [evidences](02.png)
    And I conclude that the execution of the python script worked correctly
    And I solved the challenges
