from pwnlib.tubes.remote import remote
import re

address = "d2eafd7c038c5205.247ctf.com"
port = 50169
r = remote(address, port)

for _ in range(500):
    s = r.recv().decode("utf-8")
    string = s.split(" ")
    string2 = string[-1]
    a = string[-3]
    b = string2[0:-3]
    result = int(a) + int (b)
    response = bytes("{}\r\n".format(result), 'utf-8')
    r.send(response)

print(r.recv().decode("utf-8"))
