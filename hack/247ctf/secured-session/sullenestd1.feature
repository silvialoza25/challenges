## Version 2.0
## language: en

Feature: Web-247ctf
  Site:
    247ctf.com
  Category:
    Web
  User:
    Sullenestdust1
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Arch Linux      | 5.7.9-arch1-1 |
    | Chrome          | 85.0.4183.26  |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge statement, it says
    """
    If you can guess our random secret key, we will tell you the flag securely
    stored in your session.
    """
    Then I click the link to start the challenge
    And redirects me to
    """
    https://c3791f75e30ce35e.247ctf.com/
    """

  Scenario: Success: Decode Session Cookie
    Given I am in the challenge page
    When I see the screen, it looks like a python file
    """
    import os
    from flask import Flask, request, session
    from flag import flag

    app = Flask(__name__)
    app.config['SECRET_KEY'] = os.urandom(24)

    def secret_key_to_int(s):
        try:
            secret_key = int(s)
        except ValueError:
            secret_key = 0
        return secret_key

    @app.route("/flag")
    def index():
        secret_key = secret_key_to_int(request.args['secret_key']) if
         'secret_key' in request.args else None
        session['flag'] = flag
        if secret_key == app.config['SECRET_KEY']:
        return session['flag']
        else:
        return "Incorrect secret key!"
    @app.route('/')
    def source():
        return "
        %s
        " % open(__file__).read()
    ...
    """
    Then I notice that Flask is managing a "SECRET_KEY"
    """
    app = Flask(__name__)
    app.config['SECRET_KEY'] = os.urandom(24)
    """
    And also, using "/flag" with "secret_key" parameter will give some output
    Given What I know about the challenge
    When I search information about Flask "SECRET_KEY"
    Then I found that is usually related to the management of session cookies
    And I procede to look for my session cookie in Chrome, but found nothing
    When I notice there is no session cookie, I try the following
    """
    https://c3791f75e30ce35e.247ctf.com/flag?secret_key=123
    """
    Then I get the following output
    """
    Incorrect secret key!
    """
    And I look again at my cookies and now a session one appears with the value
    """
    eyJmbGFnIjp7IiBiIjoiTWpRM1ExUkdlMlJoT0RBM09UVm1PR0UxWTJGaU1tVXdNemRrTnpNNE5
    UZ3dOMkk1WVRreGZRPT0ifX0.Xxe5Sg.3gK0904moG1WnOxOTSuAngLKOgk
    """
    Given That I have a session cookie
    When I search for how is encrypted, I found that the first part is Base64
    """
    eyJmbGFnIjp7IiBiIjoiTWpRM1ExUkdlMlJoT0RBM09UVm1PR0UxWTJGaU1tVXdNemRrTnpNNE5
    UZ3dOMkk1WVRreGZRPT0ifX0
    """
    Then I procede to decode that string with
    """
    https://www.base64decode.org/
    """
    And I get the following output
    """
    {"flag":{" b":"MjQ3Q1RGe2RhODA3OTVmOGE1Y2FiMmUwMzdkNzM4NTgwN2I5YTkxfQ=="}}
    """
    Given That decoded string
    When I carefully read it
    Then I found out another string that is similar to the previous one
    """
    MjQ3Q1RGe2RhODA3OTVmOGE1Y2FiMmUwMzdkNzM4NTgwN2I5YTkxfQ==
    """
    And I try to decode it like the previous one, getting the following output
    """
    247CTF{da80795f8a5cab2e037d7385807b9a91}
    """
    And I have captured the flag
