import os
import OpenSSL.crypto
from Crypto.Util import asn1

crypto = OpenSSL.crypto

with open('cert.crt','rb') as f:
    x = f.read()
cert = crypto.load_certificate(crypto.FILETYPE_ASN1,x)

pub = cert.get_pubkey()
for filename in os.listdir('keys/'):
    if filename.endswith(".key"):
        with open('keys/' + filename, 'r') as w:
            key = w.read()
        priv = crypto.load_privatekey(crypto.FILETYPE_PEM,key)

        pub_asn1 = crypto.dump_privatekey(crypto.FILETYPE_ASN1, pub)
        priv_asn1 = crypto.dump_privatekey(crypto.FILETYPE_ASN1, priv)

        pub_der = asn1.DerSequence()
        pub_der.decode(pub_asn1)
        priv_der = asn1.DerSequence()
        priv_der.decode(priv_asn1)

        pub_modulus = pub_der[1]
        priv_modulus = priv_der[1]

        if pub_modulus == priv_modulus:
            print('Found')
            print(f'The correct key is {filename}')
            break
        else:
            print('Not this one')
