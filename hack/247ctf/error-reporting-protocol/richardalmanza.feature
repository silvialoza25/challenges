## Version 2.0
## language: en

Feature:  Networking-247ctf
  Site:
    247ctf
  Category:
    Networking
  User:
    richardalmanza
  Goal:
    Get the flag from encrypted file

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu (Bionic) | 18.04.4 LTS   |
    | Google Chrome   | 80.0.3987.132 |
    | Wireshark       | 2.6.10        |
  Machine information:
    Given introduction to the challenge
    """
    Can you identify the flag hidden within
    the error messages of this ICMP traffic?
    """
    And I downloaded the pcap file

  Scenario: Fail: find flag using wireshark
    Given challenge file
    Then I used wireshark to open it
    When I tried to find the flag using the filter
    """
    frame contains flag
    """
    And it showed me only the request [package](pkg-req.png)
    Then I tried again with 'frame contains CTF'
    And 'frame contains ctf'
    But There was no result

  Scenario: Success: export and join data from packages
    Given the [first reply package](pkg-rep-f.png)
    And the [last reply package](pkg-rep-l.png)
    Then I noticed those packages opens and closes a jpg file respectively
    And I looked for a way to export those data fields
    And I used the command below
    """
    $ tshark -r error_reporting.pcap -T fields -e data > out
    """
    When I saw the [output](data.png)
    And noticed it had the request package data and linebreaks
    Then I wrote the following program in Crystal
    """
    #! /usr/bin/crystal

    img = File.read(ARGV[0]).split[1..].join

    slice = img.hexbytes

    File.open(ARGV[1],"wb") do |f|
      f.write slice
    end
    """
    And I used it
    """
    $ ./hexstringtobin.cr out ctf.jpg
    """
    And mission completed, I got the flag
    And [I passed the challenge](completed.png)
