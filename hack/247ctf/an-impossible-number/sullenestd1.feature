## Version 2.0
## language: en

Feature: Miscellaneous-247ctf
  Site:
    247ctf.com
  Category:
    Miscellaneous
  User:
    Sullenestdust1
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>      |
    | Arch Linux      | 5.7.10-arch1-1 |
    | Chrome          | 86.0.4209.2    |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge statement, it says
    """
    Can you think of a number which at the same time is one more than itself?
    """
    Then I download the challenge file
    """
    26a4ffbf6fb2d2d01d70b92cef36e92222906e3c.zip
    """
    And I click the link to start the challenge
    And The site gives me the following address
    """
    tcp://9feb3d3a6d9d452d.247ctf.com:50070
    """

  Scenario: Success: Consider Integer Overflow
    Given The downloaded file
    """
    26a4ffbf6fb2d2d01d70b92cef36e92222906e3c.zip
    """
    When I extracted the contents, I can see the following file
    """
    impossible_number.c
    """
    Then I open it and see
    """
    #include <stdio.h>
    int main() {
        int impossible_number;
        FILE *flag;
        char c;
        if (scanf("%d", &impossible_number)) {
            if (impossible_number > 0 && impossible_number >
            (impossible_number + 1)) {
                flag = fopen("flag.txt","r");
                while((c = getc(flag)) != EOF) {
                    printf("%c",c);
                }
            }
        }
        return 0;
    }
    """
    And I notice the condition that should be true to get the flag
    """
    impossible_number > 0 && impossible_number > (impossible_number + 1)
    """
    When I look more closely, I notice the line
    """
    int impossible_number
    """
    Then I search for the properties of "int" in C and found
    """
    Type        Typical Bit Width            Typical Range
    signed int        4bytes             -2147483648 to 2147483647
    """
    And Considering the overflow, I think that the number I am looking for is
    """
    2147483647
    """
    When I open
    """
    tcp://9feb3d3a6d9d452d.247ctf.com:50070
    """
    Then I get a blank page
    And I must search how to communicate with the address
    When I was searching for progams to do that, I found
    """
    GNU Netcat
    """
    Then I used it
    """
    $ nc 9feb3d3a6d9d452d.247ctf.com 50070
    """
    And I got a blank prompt
    When I enter
    """
    2147483647
    """
    Then I get the following output
    """
    247CTF{38f5daf742a4b3d74b3a7575bf4d7d1e}
    """
    And I have captured the flag
