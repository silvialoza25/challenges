## Version 2.0
## language: en

Feature: Miscellaneous-247cft
  Site:
    247ctf
  Category:
    Miscellaneous
  User:
    SullenesDust1
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Arch Linux      | 5.7.8-arch1-1 |
    | Chrome          | 85.0.4181.8   |
    | Python          | 3.8.3         |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge statement, it says
    """
    We created a hidden painting which will lead you to the flag. Can you
    connect the dots and piece it back together?
    """
    Then I download the challenge file
    """
    3e0172573df899935babaef74cb6e2f2a98afe56.zip
    """

  Scenario: Fail: Check for clues in the file
    Given The downloaded file
    When I extracted its contents, I found the file
    """
    secret_map.txt
    """
    Then I open it and found two hex digits in each line
    """
    0x4b 0x9d0
    0x44 0x974
    0x33 0x92
    0x2f 0x660
    0x48 0xe1
    0x47 0x59c
    0x5b 0x4cf
    0x7c 0x489
    ...
    """
    Given those hex digits
    When I check the file using
    """
    https://hexed.it/
    """
    Then I found no relevant information
    When I check the file using
    """
    exiftool
    """
    Then I get the following output
    """
    ExifTool Version Number         : 12.00
    File Name                       : secret_map.txt
    Directory                       : .
    File Size                       : 669 kB
    ...
    File Type                       : TXT
    File Type Extension             : txt
    MIME Type                       : text/plain
    MIME Encoding                   : us-ascii
    Newlines                        : Unix LF
    Line Count                      : 62833
    Word Count                      : 125666
    """
    And I can confirm that the file is indeed a text file


  Scenario: Success: Coordinates in the file
    Given The file with hex values
    When I check the values in decimal with
    """
    https://www.convzone.com/hex-to-decimal/
    """
    Then I found that most of those values are not that big
    And That maybe they can be plotted somehow
    Given That i need a way to plot those values
    When I search for various libraries like
    """
    Matplotlib, seaborn and pygame
    """
    Then I decide to try
    """
    Pygame
    """
    And I start processing all the values with Python
    Given the hex values in the file
    When I transform them to decimal
    Then I use them to plot in a canvas with
    """
    Pygame
    """
    And Found a message, but is not completely visible
    Given That message
    When I flip the canvas
    Then I Found a string of characters
    And The flag is that string
