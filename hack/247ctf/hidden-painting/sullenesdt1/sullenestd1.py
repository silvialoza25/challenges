import pygame

def main() -> None:
    pygame.init()
    screen = pygame.display.set_mode((3500, 200))
    red = (255,0,0)
    white = (255,255,255)
    screen.fill(white)

    while True:
        event = pygame.event.poll()
        if event.type == pygame.QUIT:
            break

        with open("secret_map.txt", "r") as f:
            data = f.readlines()

        for line in data:
            coord = line.strip().split()
            x=int(coord[1],16)
            y=int(coord[0],16)
            pygame.draw.line(screen, red , (x,y), (x,y), 1)

        pygame.display.flip()

    pygame.quit()

if __name__ == "__main__":
    main()
