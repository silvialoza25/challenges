## Version 2.0
## language: en

Feature: Reversing-247ctf
  Site:
    247ctf.com
  Category:
    Reversing
  User:
    SullenestDust1
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>      |
    | Arch Linux      | 5.7.10-arch1-1 |
    | Chrome          | 86.0.4209.2    |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge statement, it says
    """
    One byte is great. But what if you need more? Can you find the flag hidden
    in this binary?
    """
    Then I click the link to start the challenge
    And downloads the following file
    """
    c3ae7d493a1dc7d4bbfc889129024f4c6bc9da13.zip
    """

  Scenario: Success: Check file in hex editor
    Given The downloaded file
    When I extract it, I get the following file
    """
    the_more_the_merrier
    """
    Then I check the file with
    """
    exiftool
    """
    And I get the following output
    """
    $ exiftool the_more_the_merrier
    ExifTool Version Number         : 12.00
    File Name                       : the_more_the_merrier
    Directory                       : .
    File Size                       : 5.9 kB
    File Modification Date/Time     : 2019:09:06 10:24:06-05:00
    File Access Date/Time           : 2020:07:27 09:28:35-05:00
    File Inode Change Date/Time     : 2020:07:27 09:28:34-05:00
    File Permissions                : rwxr-xr-x
    File Type                       : ELF shared library
    File Type Extension             : so
    MIME Type                       : application/octet-stream
    CPU Architecture                : 64 bit
    CPU Byte Order                  : Little endian
    Object File Type                : Shared object file
    CPU Type                        : AMD x86-64
    """
    And I can conclude is an ELF file
    When I check for interesting strings in the file with
    """
    Strings
    """
    Then I found nothing interesting or useful
    When I check the file with
    """
    https://hexed.it/
    """
    Then I can see the file in hex
    And I found the flag hidden in some bytes [evidence](evidence.png)
