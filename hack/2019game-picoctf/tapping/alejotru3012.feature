## Version 2.0
## language: en

Feature:
  Site:
    https://play.picoctf.org/practice
  User:
    Aletroz
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Firefox         | 84.0.2      |
    | Netcat          | v1.10-46    |
  Machine information:
    Given the command
    """
    nc jupiter.challenges.picoctf.org 21610
    """

  Scenario: Fail: Decoding all the message
    Given a Netcat command
    Then I execute it
    And I get
    """
    .--. .. -.-. --- -.-. - ..-. { -- ----- .-. ... ...-- -.-.
    ----- -.. ...-- .---- ... ..-. ..- -. ...-- ----. ----- ..---
    ----- .---- ----. ..... .---- ----. }
    """
    And I notice it looks like Morse code
    Then I use an online Morse decoder https://morsedecoder.com/es/
    And I get
    """
    PICOCTF#M0RS3C0D31SFUN3902019519#
    """
    And I try to use it as the flag but it fails [evidence](01.png)

  Scenario: Success: Split the message and ignore "{" and "}"
    Given the previous message
    Then I separate into 2 parts where the first is outside the brackets
    And the second one is between brackets
    Then I decode both using https://morsedecoder.com/es/
    And I replace the result in the message
    And I get
    """
    PICOCTF{M0RS3C0D31SFUN3902019519}
    """
    Then I try it as a flag
    And I solve the challenge [evidence](02.png)
