## Version 2.0
## language: en

Feature: Insp3ct0r - web - picoCTF
  Code:
    Insp3ct0r
  Site:
    2019game.picoctf.com
  Category:
    web
  User:
    lovecraft
  Goal:
    capture 3 parts of a flag

  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Ubuntu          | 18.04           |
    | Chrome          | 78.0.3904.108   |
    | None            | None            |

  Machine information:
    Given I am accessing to the challenge via browser
    """https://2019shell1.picoctf.com/problem/9509/"""
    When I enter to the page I'll see a title that say "Inspect me"
    Then I can assume that the challenge consist in inspect the source page

 Scenario: Success:Inspect the Source Code
    When I press ctrl + U
    Then I'll see the first part of a flag "<FLAG 1/3>"
    And also I'll see 2 other files named "mycss.css" and "myjs.js"
    And enter to the first and see the second part of the flag "<FLAG 2/3>"
    And go back and enter the second file
    And I'll see the final part of the flag "<FLAG 3/3>"
    Then I'll get the complete flag
    """<FLAG 3/3>"""
    And now I can solve the challenge
