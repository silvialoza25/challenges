## Version 2.0
## language: en

Feature:
  Site:
    play.picoctf.org
  Category:
    RE
  User:
    bianfa
  Goal:
    Get the password from the source code

  Background:
  Hacker's software:
    | <Software name> | <Version>            |
    | Kali Linux      | 2020.3               |
    | Firefox         | 68.10.0esr           |
    | cat             | 8.30                 |
  Machine information:
    Given the challenge URL
    """
    https://play.picoctf.org/practice/challenge/12?category=3&
    originalEvent=1&page=1
    """
    When I open this URL with Firefox
    Then I click on "VaultDoor1.java"
    And this can be seen in [evidence](img1.png)
    And I get a file called "VaultDoor1.java"

  Scenario: Fail: I try to use only the password with which it is compared
    Given the file "VaultDoor1.java"
    When I run "cat ./VaultDoor1.java"
    Then I can see the source code from file
    And I see that the user input is compared in the checkPassword method
    And this can be seen in [evidence](img2.png)
    Then I see that the password is compared by characters
    And also it is compared disorderly to make it hard to read
    And this can be seen in [evidence](img3.png)
    Then I order the password by characters
    Then I use the password ordered as a flag
    And it is rejected

  Scenario: Success: I try to use the concatenated password
    Given the file "VaultDoor1.java"
    When I run "cat ./VaultDoor1.java"
    Then I can see the source code from the "VaultDoor1.java" file
    And I see that before the password is compared the user input is cut
    And this can be seen in [evidence](img4.png)
    Then I conclude that the program expects user input to start with
    """
    picoCTF{
    """
    And I suppose that the program expects user input to end with
    """
    }
    """
    And I see that the password is compared by characters
    And also it is compared disorderly to make it hard to read
    Then I order the password by characters
    And I use the password ordered as a part of the flag
    Then I try to use "picoCTF{<password>}" as the flag
    And it is accepted
