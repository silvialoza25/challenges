## Version 2.0
## language: en

Feature:
  Site:
    play.picoctf.org
  Category:
    RE
  User:
    bianfa
  Goal:
    Get the password from the source code

  Background:
  Hacker's software:
    | <Software name> | <Version>            |
    | Kali Linux      | 2020.3               |
    | Firefox         | 68.10.0esr           |
    | cat             | 8.30                 |
  Machine information:
    Given the challenge URL
    """
    https://play.picoctf.org/practice/challenge/71?category=3&
    originalEvent=1&page=1
    """
    When I open this URL with Firefox
    Then I click on "VaultDoor4.java"
    And this can be seen in [evidence](img1.png)
    And I get a file called "VaultDoor4.java"

  Scenario: Fail: I try to use only the password with which it is compared
    Given the file "VaultDoor4.java"
    When I run "cat ./VaultDoor4.java"
    Then I can see the source code from file
    And I see that the user input is compared in the checkPassword method
    And this can be seen in [evidence](img2.png)
    Then I see that the password is compared with a bytes array
    And also every byte is represented in different number systems
    And this can be seen in [evidence](img3.png)
    Then I create a script of Java that print the same bytes array in string
    And this can be seen in [evidence](bianfa.java)
    And I get the password in string
    Then I use the password as the flag
    And it is rejected

  Scenario: Success: I try to use the concatenated password
    Given the file "VaultDoor4.java"
    When I run "cat ./VaultDoor4.java"
    Then I can see the source code from the "VaultDoor4.java" file
    And I see that before the password is compared the user input is cut
    And this can be seen in [evidence](img4.png)
    Then I conclude that the program expects user input to start with
    """
    picoCTF{
    """
    And I suppose that the program expects user input to end with
    """
    }
    """
    Then I see that the password is compared with a bytes array
    And also every byte is represented in different number systems
    And this can be seen in [evidence](img3.png)
    Then I create a script of Java that print the same bytes array in string
    And this can be seen in [evidence](bianfa.java)
    And I get the password in string
    Then I use the password as the flag
    Then I try to use "picoCTF{<flag>}" as the flag full
    And it is accepted
