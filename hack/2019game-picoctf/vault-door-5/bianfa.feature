## Version 2.0
## language: en

Feature:
  Site:
    play.picoctf.org
  Category:
    RE
  User:
    bianfa
  Goal:
    Get the password from the source code

  Background:
  Hacker's software:
    | <Software name> | <Version>            |
    | Kali Linux      | 2020.3               |
    | Firefox         | 68.10.0esr           |
    | cat             | 8.30                 |
  Machine information:
    Given the challenge URL
    """
    https://play.picoctf.org/practice/challenge/77?category=3&
    originalEvent=1&page=1
    """
    When I open this URL with Firefox
    Then I click on "VaultDoor5.java"
    And this can be seen in [evidence](img1.png)
    And I get a file called "VaultDoor5.java"

  Scenario: Fail: I try to use the string with which it is compared the input
    Given the file "VaultDoor5.java"
    When I run "cat ./VaultDoor5.java"
    Then I can see the source code from the "VaultDoor5.java" file
    And I see that before the password is compared the user input is cut
    And this can be seen in [evidence](img2.png)
    Then I conclude that the program expects user input to start with
    """
    picoCTF{
    """
    And I suppose that the program expects user input to end with
    """
    }
    """
    And I see that the user input is compared in the checkPassword method
    And this can be seen in [evidence](img3.png)
    Then I see that the password is compared with a string
    And this can be seen in [evidence](img4.png)
    Then I use the string as the flag
    And I try to use "picoCTF{<flag>}" as the flag full
    And it is rejected

  Scenario: Success: I get the password real
    Given the file "VaultDoor5.java"
    When I run "cat ./VaultDoor5.java"
    Then I can see the source code from the "VaultDoor5.java" file
    And I see that the user input is compared in the checkPassword method
    And this can be seen in [evidence](img3.png)
    Then I see that the password is compared with a string
    And this can be seen in [evidence](img4.png)
    And I see that before the user input is compared, it is encode by URL
    And after is encode by Base64
    And this can be seen in [evidence](img5.png)
    Then I decode the string by Base64 using https://www.base64decode.org/
    And this can be seen in [evidence](img6.png)
    And I decode the result by URL using https://www.urldecoder.org/
    And this can be seen in [evidence](img7.png)
    And I use this result as the flag
    And I try to use "picoCTF{<flag>}" as the flag full
    And it is accepted
