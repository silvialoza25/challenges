## Version 2.0
## language: en

Feature:
  Site:
    https://play.picoctf.org/practice
  User:
    Aletroz
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | VS Code         | 1.54.3      |
    | Netcat          | v1.10-46    |
  Machine information:
    Given this url and port
    """
    jupiter.challenges.picoctf.org 7480
    """

  Scenario: Fail: Searching flag on terminal
    Given the url and port
    Then I connect to them using Netcat
    """
    nc jupiter.challenges.picoctf.org 7480
    """
    And I get a repetitive output like
    """
    I don't think this is a flag either
    Again, I really don't think this is a flag
    Not a flag either
    Not a flag either
    Again, I really don't think this is a flag
    This is defintely not a flag
    This is defintely not a flag
    Again, I really don't think this is a flag
    This is defintely not a flag
    This is defintely not a flag
    Not a flag either
    Again, I really don't think this is a flag
    Not a flag either
    """
    Then I notice the output is too long
    And The terminal can't show it completely
    And I can't find the flag

  Scenario: Success: Saving Netcat output in file
    Given I need to find the flag in the output
    Then I save the output in a file called out.txt using
    """
    nc jupiter.challenges.picoctf.org 7480 > out.txt
    """
    And I wait for a moment before to terminate the connection
    Then I open the file with a text editor
    And I search the word "pico" which indicates the flag
    Then I found the flag [evidence](01.png)
    And I submit the flag
    And I solve the challenge [evidence](02.png)
