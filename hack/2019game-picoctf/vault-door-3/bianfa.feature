## Version 2.0
## language: en

Feature:
  Site:
    play.picoctf.org
  Category:
    RE
  User:
    bianfa
  Goal:
    Get the password from the source code

  Background:
  Hacker's software:
    | <Software name> | <Version>            |
    | Kali Linux      | 2020.3               |
    | Firefox         | 68.10.0esr           |
    | cat             | 8.30                 |
  Machine information:
    Given the challenge URL
    """
    https://play.picoctf.org/practice/challenge/60?category=3&
    originalEvent=1&page=1
    """
    When I open this URL with Firefox
    Then I click on "VaultDoor3.java"
    And this can be seen in [evidence](img1.png)
    And I get a file called "VaultDoor3.java"

  Scenario: Fail: I try to use the password altered
    Given the file "VaultDoor3.java"
    When I run "cat ./VaultDoor3.java"
    Then I can see the source code from the file
    And I see that the user input is compared in the checkPassword method
    And this can be seen in [evidence](img2.png)
    And I see that before the password is compared the user input is cut
    And this can be seen in [evidence](img3.png)
    Then I conclude that the program expects user input to start with
    """
    picoCTF{
    """
    And I suppose that the program expects user input to end with
    """
    }
    """
    Then I use the password of the checkPassword method as the flag
    And this can be seen in [evidence](img4.png)
    And I try to use "picoCTF{<password>}" as the flag full
    And it is rejected

  Scenario: Success: I use the password unaltered
    Given the file "VaultDoor3.java"
    When I run "cat ./VaultDoor3.java"
    Then I can see the source code from the file
    And I see that the user input is compared in the checkPassword method
    And I see that the password compared is altered
    And this can be seen in [evidence](img5.png)
    Then I create a script that does the reverse of the checkPassword method
    And this can be seen in [evidence](bianfa.js)
    And I get the unaltered password which is the flag
    Then I try to use "picoCTF{<flag>}" as the flag full
    And it is accepted
