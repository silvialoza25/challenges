const checkPassword = (password = "jU5t_a_sna_3lpm12g94c_u_4_m7ra41") => {
 if (password.length != 32) {
  return false;
 }
 let buffer = new Array(32);
 let i;
 for (i = 0; i < 8; i++) {
  buffer[i] = password.charAt(i);
 }
 for (; i < 16; i++) {
  buffer[23 - i] = password.charAt(i);
 }
 for (; i < 32; i += 2) {
  buffer[46 - i] = password.charAt(i);
 }
 for (i = 31; i >= 17; i -= 2) {
  buffer[i] = password.charAt(i);
 }
 console.log(buffer.join(""));
};

checkPassword();
