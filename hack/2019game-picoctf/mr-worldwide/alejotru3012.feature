## Version 2.0
## language: en

Feature:
  Site:
    https://play.picoctf.org/practice
  User:
    Aletroz
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given the file message.txt which have an encrypted flag

  Scenario: Fail: Decoding with Cyberchef
    Given the encrypted message
    When I access to http://icyberchef.com/
    And I try to decode using the "Magic" option [evidence](01.png)
    Then I notice I can't decode it

  Scenario: Success: Using coordinates
    Given the encrypted message
    Then I notice that the message looks like coordinates
    And I think I can take the first letter of the place that they indicate
    When I search the coordinates in Google Maps
    Then I get for example [evidence](02.png)
    """
    Letter   Place
    K        Kyoto
    O        Odesa
    D        Dayton
    I        Istanbul
    A        Abu Dhabi
    K        Kuala Lumpur
    _
    A        Addis Ababa
    L        Loja
    A        Amsterdam
    S        Sleepy Hollow
    K        Kodiak
    A        Alexandria
    """
    When I replace the coordinates with the letters
    Then I found the flag
    """
    picoCTF{KODIAK_ALASKA}
    """
    And I submit the flag
    And I solve the challenge [evidence](03.png)
