import java.util.*;

public class Main {
 public static void main(String[] args) throws Exception {
  int[] x = { 1096770097, 1952395366, 1600270708,
  1601398833, 1716808014, 1734293296, 842413104, 1684157793 };

  byte[] hexBytes = new byte[32];

  for(int i=0; i < 8; i++ ) {
   hexBytes[i*4] = (byte) (x[i] >> 24);
   hexBytes[i*4+1] = (byte) (x[i] >> 16 << 24 >> 24);
   hexBytes[i*4+2] = (byte) (x[i] >> 8 << 24 >> 24);
   hexBytes[i*4+3] = (byte) (x[i] << 24 >> 24);
  }

  System.out.println(new String(hexBytes));
 }
}
