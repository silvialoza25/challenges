## Version 2.0
## language: en

Feature:
  Site:
    play.picoctf.org
  Category:
    RE
  User:
    bianfa
  Goal:
    Get the password from the source code

  Background:
  Hacker's software:
    | <Software name> | <Version>            |
    | Kali Linux      | 2020.3               |
    | Firefox         | 68.10.0esr           |
    | cat             | 8.30                 |
  Machine information:
    Given the challenge URL
    """
    https://play.picoctf.org/practice/challenge/65?category=3&
    originalEvent=1&page=1
    """
    When I open this URL with Firefox
    Then I click on "VaultDoor7.java"
    And this can be seen in [evidence](img1.png)
    And I get a file called "VaultDoor7.java"

  Scenario: Fail: I use the string I get from converting the int array
    Given the file "VaultDoor7.java"
    When I run "cat ./VaultDoor7.java"
    Then I can see the source code from the "VaultDoor7.java" file
    And I see that before the password is compared the user input is cut
    And this can be seen in [evidence](img2.png)
    Then I conclude that the program expects user input to start with
    """
    picoCTF{
    """
    And I suppose that the program expects user input to end with
    """
    }
    """
    And I see that the user input is compared in the checkPassword method
    And this can be seen in [evidence](img3.png)
    Then I see that the password is compared with a int array
    And this can be seen in [evidence](img4.png)
    When I convert the int array to a string
    But I don't get a clean string to use as a flag
    And this can be seen in [evidence](img5.png)

  Scenario: Success: I get the password real
    Given the file "VaultDoor7.java"
    When I run "cat ./VaultDoor7.java"
    Then I can see the source code from the "VaultDoor7.java" file
    And I see that the user input is compared in the checkPassword method
    And this can be seen in [evidence](img3.png)
    Then I see that the password is compared with a byte array
    And this can be seen in [evidence](img4.png)
    And I see that the user input is converted to a byte array
    And I see that every 4 bytes are stored in an integer
    And this can be seen in [evidence](img6.png)
    When I create a script that does the reverse of passwordToIntArray method
    Then this can be seen in [evidence](bianfa.java)
    And I use the output of this script as the flag
    Then I try to use "picoCTF{<flag>}" as the flag full
    And it is accepted
