## Version 2.0
## language: en

Feature:
  Site:
    https://play.picoctf.org/practice
  User:
    Aletroz
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Chromium        | 89.0        |
  Machine information:
    Given the image flag.png

  Scenario: Fail: Searching for metadata
    Given the image
    When I check for something useful in metadata with
    """
    exiftool flag.png
    """
    Then I didn't find anything [evidence](01.png)

  Scenario: Success: Searching symbols meaning
    Given the image has a sequence of symbols that look like flags
    And there are present "{" and "}" [evidence](02.png)
    And they are a standard for picoCTF flags
    When I search in internet for flags like those
    Then I find they are maritime signal flags
    And each one represents a letter
    And can be found in
    """
    https://en.wikipedia.org/wiki/International_maritime_signal_flags
    """
    When I replace the flags in the image with the letters
    Then I find the flag
    """
    PICOCTF{F1AG5AND5TUFF}
    """
    And I submit the flag
    And I solve the challenge [evidence](03.png)
