## Version 2.0
## language: en

Feature: 277-Seguridad-yashira
  Code:
    277
  Site:
    yashira
  Category:
    Seguridad
  User:
    Mankar
  Goal:
    Intercept and modify communications using HTTPS protocol

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Kali Linux      | 2020.1        |
    | Mozilla Firefox | 89.0.1        |
    | Wireshark       | 3.2.1         |
    | Bettercap       | 2.23          |
    | Postman         | 7.36.5        |
  Machine information:
    Given I am accessing to the website
    And it shows a message encouraging to intercept an encrypted communication
    And it shows a Submit button

  Scenario: Fail: Inspect webpage source code and get connection request
    Given I click on the Begin button
    When I inspect the response using browser Dev Tools
    Then I can see a response HTTP header called 'X-Pista' [evidence](01.png)
    And I deduce this challenge works as the previous MITM challenge
    And I conclude the hint value corresponds to a port number
    When I allow port-forwarding in my home router
    And I redirect traffic received on that port to my Machine
    And I use Netcat command to listen on given port
    Then I find that the server is sending an HTTP CONNECT request
    And I deduce I need to handle this request with a proxy-based tool

  Scenario: Fail: Redirect request with HTTP proxy tool
    Given I have allowed port-forwarding in my home router
    When I set up a web HTTP proxy software to handle the requests
    And I press the Submit button again from the challenge website
    And I analyze my network traffic with Wireshark
    Then I find an HTTP 403 response
    And there are no changes in challenge website
    And I deduce it's because the proxy can't handle HTTPS communications

  Scenario: Success: Intercept communications with Bettercap and SSL stripping
    Given I have allowed port-forwarding in my home router
    When I start Bettercap from my Kali Linux machine
    And I enable net.sniff module with net.sniff.local parameter enabled
    And I enable https.proxy module with SSL stripping enabled
    And I press the Submit button again from the challenge website
    Then I intercept HTTP CONNECT request again
    And I also intercept an HTTP POST request [evidence](02.png)
    When I analyze my network traffic with Wireshark [evidence](03.png)
    Then I find the request body contains a key called 'llavesecreta'
    When I use Postman to send the HTTP POST request manually
    And I include the secret key found in the intercepted request
    Then the server responds with a password [evidence](04.png)
    And I caught the flag
