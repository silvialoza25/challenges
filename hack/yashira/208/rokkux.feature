## Version 2.0
## language: en

Feature: 208-yashira
  Code:
    208
  Site:
    yashira
  Category:
    retos
  User:
    RokkuX
  Goal:
    Find the probability value

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    | Kali Linux      | 20.04 LTS     |
    | Firefox         | 88.0.1(64-bit)|
  Machine information:
    Given the proposed problematic
    """
    If we have a box with 20 items inside, and 8 of them have defects.
    In order to accept a new box, it has been decided that 3 items at random
    will be checked without replacement, in case none of the items
    have defects, the box is accepted.
    What are the odds of accepting the complete box?
    Use 3 rounded decimals.
    """

  Scenario: Success: Calculate probability without replacement
    Given I see the problematic [evidence](01.png)
    Then I identify the data
    """
    Total number of items: 20
    Number of items with defects: 8
    Number of items without defects: 12
    Is replacement used?: NO
    Number of items to check: 10
    """
    When I organize the data according to probability without replacement
    And I get the probability of checking a non-defective item
    And I do it for the first time
    And I don't do replacement
    And I get the probability of checking a non-defective item
    And I do it for the second time
    And I don't do replacement
    And I get the probability of checking a non-defective item
    And I do it for the third time
    And I don't do replacement
    And I multiply the three previous calculated probabilities
    """
    (12/20)*(11/19)*(10/18)
    """
    Then I get the answer "<answer>"
    And I round it to 3 decimals
    When I submit the "<answer>" for finishing the challenge
    Then Page displays the message "Correcta tu respuesta" [evidence](02.png)
    And I solved the challenge
