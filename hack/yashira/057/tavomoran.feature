## Version 2.0
## language: en

Feature: 057-criptography-yashira
  Code:
    057
  Site:
    yashira.org
  Category:
    criptography
  User:
    jagios
  Goal:
    decode a text

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | debian          | sid         |
    | Firefox         | 80.0.1      |
  Machine information:
    Given that I am accessing the yashira webpage through webrowser
    And I am login
    Then in the 057 challenge it shows me the text
    """
    |V|'/ /-((0|_|||7 | 5|6|| 0|_|7
    """
    And the challenge is to decrypt it

  Scenario: Fail: automated decrypt
    Given I am logged in yashira
    And I get the text challenge
    Then I search in google '733t decode'
    And found the site
    """
    https://md5decrypt.net/en/Leet-translator
    """
    When I try to decrypt
    """
    |V|'/ /-((0|_|||7 | 5|6|| 0|_|7
    """
    And get the same input [evidence](fail_decrypt.png)
    Then I conclude that the cypher text must be fixed manually

  Scenario: Success: manually decode
    Given I am logged in yashira
    And I have the text challenge
    Then I go to
    """
    http://www.robertecker.com/hp/research/leet-converter.php?lang=en
    """
    And I select the mode 'customized leet(enter)(see setting below)'
    When I replace the letters because are close to the draw letters
    """
    M = |V|
    Y = '/
    A = /-
    C = (
    C = (
    O = 0
    U = |_|
    N = ||
    T = 7
    I = |
    S = 5
    I = |
    G = 6
    N = ||
    O = 0
    U = |_|
    T = 7
    """
    Then I get the flag
    And solved the challenge
