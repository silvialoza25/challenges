## Version 2.0
## language: en

Feature: 276-Seguridad-yashira
  Code:
    276
  Site:
    yashira
  Category:
    Seguridad
  User:
    Mankar
  Goal:
    Intercept and modify communications using HTTP protocol

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows         | 10            |
    | Chrome          | 91.0.4472.101 |
    | Wireshark       | 3.4.6         |
    | SoapUI          | 5.6.0         |
  Machine information:
    Given I am accessing to the website
    And website shows a message encouraging to intercept "something"
    And website shows a Submit button

  Scenario: Fail: Inspect webpage source code and server response
    Given I click on the Begin button
    When I inspect the response using browser Dev Tools
    Then I can see a response HTTP header called 'X-Pista' [evidence](01.png)
    And I deduce its value corresponds to a port number
    When I try to intercept the request and response using a proxy-based tool
    Then I find nothing of relevance
    When I try to analyze my network traffic using Wireshark
    Then I don't find anything associated with the given clue

  Scenario: Fail: Allow port-forwarding and read connection request
    Given I have inspected the server response
    And I have collected a presumed port number from a clue
    When I allow port-forwarding in my home router
    And I redirect traffic received on that port to my Machine
    And I press the Submit button again from the challenge website
    And I analyze my network traffic again with Wireshark
    Then I can see there's a connection coming to that exact port
    When I use nslookup to find Yashira's IP
    Then I confirm that incoming connection is coming from Yashira's server
    When I use Netcat command to listen on given port
    Then I find that the server is sending an HTTP post request
    And it is directed to an inexistent host
    And it gives a clue about 'making that host existent and valid'
    And this can be seen in [evidence](02.png)
    And I deduce this request is triggered by the original HTTP submission
    And that it requires receiving a valid HTTP response to deliver the flag

  Scenario: Success: Send an HTTP response to server
    Given I have allowed port-forwarding to my machine on given port
    When I create a mock webserver with SoapUI, that listens on that same port
    And this web server sends an HTTP response with status 200
    And I trigger submission on challenge website
    Then my mock server sends a valid response to the incoming connection
    And challenge website shows a congratulations message with a password
    And I caught the flag [evidence](03.png)
