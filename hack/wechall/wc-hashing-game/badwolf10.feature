# Version 2.0
# language: en

Feature: WC Hashing Game - Cracking - WeChall
  Site:
    WeChall
  Category:
    Cracking
  User:
    badwolf10
  Goal:
    Crack 2 lists of hashes


  Background:
  Hacker's software
    | <Software name>    |       <Version>      |
    | Ubuntu             | 16.04 LTS (x64)      |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |

  Scenario: Success: Crack hashed list
    Given the challenge URL
    """
    http://www.wechall.net/challenge/hashgame/index.php
    """
    And two lists of hashes "WC3, WC4"
    And the two algorithms to generate each list
    And the goal description
    """
    Your mission is to crack 2 lists of hahes.
    The first list is using the WC3 hashing algorithm, which uses some fixed
    salt md5.
    The second list is using the WC4 hashing algorithm, which uses salted
    sha1.
    The solution is the 2 longest plaintexts of each list, concatenated with
    a comma.
    Example solution: wordfrom1,wordfrom1,wordfrom2,wordfrom2.
    """
    Then I search online for a dictionary for a dictionary attack
    And I find one at "https://wiki.skullsecurity.org/Passwords"
    And I download one dictionary file called "cain.txt"
    Then I write a Python code with the encryption algorithms for both lists
    And I write a code to test each word in the encryption algorithms
    Then I run the code
    And I obtain several words whose hashes are in the lists
    Then I obtain the two longest words for each list as required
    """
    coincidence,subversion,triangulation,orthography
    """
    Then I solve the challenge
