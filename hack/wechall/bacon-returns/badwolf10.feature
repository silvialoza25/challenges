# Version 2.0
# language: en


Feature:
  Solve Choose your path II
  From wechall site
  Category Stegano, Encoding, Crypto, Training

  Background:
    Given I am running Ubuntu 16.04 (64Bit)
    And I am using Mozilla Firefox 63.0
    And I am using Visual Studio Code 1.29.1

  Scenario:
    Given the challenge URL
    """
    https://www.wechall.net/challenge/training/encodings/bacon2/index.php
    """
    And the challenge statement
    """
    This is the level11 mini challenge found in /home/level/11/ on the
    warchall box. The sequel to Choose your Path.
    You can view the source here.
    """

  Scenario: Failed Solution
    Given the message to decode
    Then I use VSCode with regular expressions to remove non-word characters
    Then I replace letter A to lower case letters
    And I replace letter B to upper case letter
    Then I split the reulting string in words of 5 letters
    Then I translate each word into a letter using the bacon table found at
    """
    https://en.wikipedia.org/wiki/Bacon%27s_cipher
    """
    And I found no distiguishable message

  Scenario: Successful solution
    Given the message to decode
    Then I use VSCode with regular expressions to remove non-word characters
    Then I remove lower case letters
    Then I replace the letter from A to M ("[A-M]") with A
    And I replace the letter from N to Z ("[N-Z]") with B
    Then I split the reulting string in words of 5 letters
    Then I translate each word into a letter using the bacon table found at
    """
    https://en.wikipedia.org/wiki/Bacon%27s_cipher
    """
    And I find a distiguishable message
    """
    you can read the hidden message so i will tell you the solution which
    is twelve random letters anhmbhiaprib
    """
    Then I solve the challenge

