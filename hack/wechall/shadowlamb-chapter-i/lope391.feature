## Version 2.0
## language: en

Feature: Shadowlamb_Chapter_I-Fun-Wechall
  Site:
    https://www.wechall.net/
  Category:
    Fun
  User:
    lope391
  Goal:
    Find the secret answer

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | WeeChat         | 1.91        |
    | Linux Mint      | 19.3(Tricia)|
    | Mozilla Firefox | 72.0.2      |
  Machine information:
    Given a set of cryptic instructions
    """
    Ugah made game.
    You play game.
    You #use ScrollOfWisdom.
    You play in IRC.
    The game in english
    """
    And an incoherent string of characters
    """
    VG8gcGxheSB5b3Ugd2lsbCBuZWVkIGFuIElSQyBjbGllbnQgYW5kIGNvbm5lY3QgdG8gaXJj
    LndlY2hhbGwubmV0IG9uIHBvcnQgNjY2OCBvciBwb3J0IDY2OTcgZm9yIFNTTC4KVGhlIGNo
    YW5uZWwgaXMgI3NoYWRvd2xhbWI=
    """
    And an IRC(Internet Relay Chat) application in weechat
    And it is used to join the chat channel
    And its running on Mint 19.3 with kernel 18.04.3

  Scenario: Fail: Did not find the secret answer
    Given the string
    When I input the string in a base64 decoder
    Then I get back the URL of the IRC server and channel to connect to
    And I get a general port and a SSL port
    """
    To play you will need an IRC client and connect to irc.wechall.net on
    port 6668 or port 6697 for SSL.
    The channel is #shadowlamb
    """
    When I open weechat
    Then I add the shadow lamb server using SSL
    And make it autojoin the channel shadowlamb
    """
    /server add shadowlamb irc.wechall.net
    /set irc.server.shadowlamb.addresses "irc.wechall.net/6697"
    /ser irc.server.shadowlamb.ssl on
    """
    When I try to log into the IRC server
    """
    /connect shadowlamb
    """
    Then I get an error of key validation
    And I can not connect to the game
    When I go to the challenge community page
    Then I find the source code for the game
    And I look through the source code
    When I Find the source code of the ScrollOfWisdom
    But I realize the challenge solution not stored statically
    Then I realize I have to play the game to find the answer
    And I have not solved the challenge

  Scenario: Success: Found the secret answer
    Given The decoded base64 string from the failed scenario
    When I open weechat
    Then I add the shadow lamb server using the general port
    """
    /server add shadowlamb irc.wechall.net
    """
    When I try to log into the IRC server
    """
    /connect shadowlamb
    """
    Then I get into chat server
    When I join the shadowlamb channel within the chat server
    """
    /join shadowlamb
    """
    Then I get into the game and start playing
    When I start Exploring the map
    Then I find a store with an Item called ScrollOfWisdom
    But I don't have enough money for the ScrollOfWisdom
    Then I keep exploring to find more money
    And I go back to the store with the ScrollOfWisdom
    When I buy the ScrollOfWisdom
    Then I use the ScrollOfWisdom
    And I get the solution to the challenge
    """
    The scroll reads:  `Congrats! Enter "{FLAG}" without the quotes`.
    """
    When I input the solution into the wechall website
    Then I solve the challenge
