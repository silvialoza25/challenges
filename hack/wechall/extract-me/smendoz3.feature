## Version 2.0
## language: en

Feature: eXtract Me - Encoding - WeChall
  Site:
    https://www.wechall.net/
  Category:
    Stegano
  User:
    smendoz3
  Goal:
    Find secret answer

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
    | gzip            | 1.6         |
    | XZ Utils        | 5.1.0 alpha |
    | 7-Zip           | 9.20        |
    | ZZip            | 0.36b       |
    | UnRAR           | 5.30 beta   |
    | zoo             | 2.1         |
    | rZip            | 2.1         |
    | ARJ32           | 3.10        |
    | GHex            | 3.18.0      |
  Machine information:
    Given A "r.zip" file
    And description from Author
    """
    Yo dog, I heard you like zips so we put a zip in your zip
    so you can unzip unzipped zips. Enjoy!
    """

  Scenario: Fail: Infinite recursion
    Given The "r.zip" file
    When I extract its content
    Then I get a new folder with another "r.zip" inside
    When I repeat the same process
    Then I dont get anything useful

  Scenario: Fail: Wrong Path
    Given The "r.zip" file
    When I google about infinite zip files
    Then I find is often called a zip quine
    And it happens because the file headers loop themselves
    When I compare the given "r.zip" with it first unzipped folder
    Then I notice they have different amount of data [evidence](1.png)
    And I compare each file header with "xxd" command
    And They have the first 440 bytes in common
    When I slice the hexdump equivalent of first 440 bytes
    And eval result "file zip" to know the file data type
    Then I see a compressed data of 16 bits [evidence](2.png)
    And rename for "file.Z" since ".Z" is the compressed extension
    When I "uncompress file.Z"
    Then I get a "file.xar" file
    When I extract the ".xar" file using "7z x file.xar"
    Then I get a '8' file
    When I use "file 8" to know the file data type
    Then I can see "8: compressed data of 16 bits" output
    And I rename it "8.Z"
    When I extract its content using "uncompress 8.Z"
    Then I get "8.rar" file
    When I extract its content using "unrar 8.rar" file
    Then I get a 'A' file
    When I use "file A" to know the file data type
    Then I can see "A: Zoo archive data" output
    And I rename it "A.zoo"
    When I extract its content using "zoo e A.zoo" file
    Then I get a '4' file
    When I use "file 4" to know the file data type
    Then I can see "4: rzip compressed data"
    And I rename it "4.rz"
    When I extract its content using "rzip -d 4.rz"
    Then I get another '4' file
    When I use "file 4" again to know the file data type
    Then I can see "4: ZZip archive data"
    And I rename it "4.zz"
    When I extract its content using zzip lib from ddebin [evidence](3.png)
    Then I get a '0' file
    When I use "file 0" to know the file data type
    Then I can see "0: gzip compressed data"
    And I rename it "0.gz"
    When I extract its content using "gunzip 0.gz"
    Then I get another '0' file
    When I use "file 0" again to know the file data type
    Then I can see "0: ARJ archive data"
    And I rename it "0.arj"
    When I extract its content using "arj e 0.arj"
    Then I get a '3' file
    When I use "file 3" to know the file data type
    Then I can see "3: compress'd data 16 bits"
    And I rename it "3.Z"
    When I extract its content using "uncompress 3.Z"
    Then I get another '3' file
    When I use "file 3" again to know the file data type
    Then I can see "3: 7-zip archive data"
    And I rename it "3.7z"
    When I extract its content using "7z e 3.7z"
    Then I get a 'F' file
    When I use "file F" to know the file data type
    Then I can see "F: bzip2 compressed data"
    And I rename it "F.bz2"
    When I extract its content using "bzip2 -d F.bz2"
    Then I get a plain text that say "L0LYouThInkiTSh0uldB3SoEasY?"
    And I dont get the answer

  Scenario: Success: Found secret word
    Given The "r.zip" file
    When I reach the 4th decompress step in the ".rar" file
    Then I discover It have a comment [evidence](3.png)
    """
    5d 00 00 80 00 ff ff ff ff ff ff ff ff 00 29 18 4a 42 10 52 65 b9 2c
    a3 92 67 35 29 06 05 61 69 77 63 e2 00 9d a0 f3 6b e9 64 f5 bd d4 47
    c9 ee a3 52 2d 2e 7c 8a 06 74 b2 c1 c0 4a 56 c3 f8 51 0e 42 70 57 82
    90 da 0b b5 74 99 34 b9 20 24 27 79 e2 ad cd fc 5e 1e 58 f5 a3 bf 6d
    c9 43 18 22 37 89 e0 3d b4 2d c4 1c b6 a5 b0 65 79 fc fa 28 72 93 39
    1c f2 fc e3 36 da d8 2a e5 b1 c2 81 de e9 b9 81 76 a7 4a eb 47 c4 a9
    f5 3c 16 86 d1 98 4c 6f 56 e6 5e fa d6 8e 22 56 38 24 d1 47 2a 4f d4
    88 1c d3 ff ff f5 d1 00 00
    """
    And I convert Hexadecimal into file binary
    And I get a LZMA compressed data file
    When I extract its content using "xz --decompress file.lzma"
    Then I get a "file" output
    When I use "file file" to know the file data type
    Then I see "file: RAR archive data"
    And I rename it "file.rar"
    When I try extract the rar
    Then I notice it has a password
    But I remember the first route string "L0LYouThInkiTSh0uldB3SoEasY?"
    And It unlocks the "file.rar"
    And I see a "solution.txt" file
    And It contains a long string
    And I solve the challenge
