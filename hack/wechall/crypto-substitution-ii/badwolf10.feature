# Version 2.0
# language: en


Feature:
  Solve Crypto - Substitution II
  From wechall site
  Category Crypto, Training

  Background:
    Given I am running Ubuntu 16.04 (64Bit)
    And I am using Mozilla Firefox 63.0
    And I am using Gedit 3.18

  Scenario:
    Given the challenge URL
    """
    https://www.wechall.net/challenge/training/crypto/simplesub2/index.php
    """
    Then I access the website with the url on the browser
    And I see the challenge statement
    """
    I have created an advanced version of the simple substitution cipher.
    It can now use chars in range from 0-255, but that should not stop you.
    The ciphertext is in the language of this text, and uses correct
    punctuation and case-sensitivity.

    7F 78 40 6B 0B 9F CF 60 7C 9F CF CC 78 40 86 25
    7D 21 22 CC 86 7D 78 40 1C 7D E8 9F 86 7D 22 9F
    0B 33 1C 0B 6D 7D 23 60 CF 7D 8C 78 60 7D 6B 78
    CF 7D CC CF 25 7D 9B 1C 0B 8C 7D E8 1C 7C 7C 7D
    33 78 40 1C 7D 48 1C 7C 7C 78 E8 7D 22 9F D4 A3
    1C 0B 25 7D 21 22 1C 7D F9 0B 78 23 7C 1C F6 7D
    E8 CC CF 22 7D CF 22 CC 86 7D D4 CC F9 22 1C 0B
    7D CC 86 7D CF 22 9F CF 7D CF 22 1C 7D A3 1C 8C
    7D CC 86 7D F9 0B 1C CF CF 8C 7D 7C 78 40 6B 25
    7D 1A 7D E8 CC 7C 7C 7D D4 78 F6 1C 7D 60 F9 7D
    E8 CC CF 22 7D 9F 7D 23 1C CF CF 1C 0B 7D 1C 40
    D4 0B 8C F9 CF CC 78 40 7D 86 22 1C F6 1C 7D 9F
    40 8C 7D 86 78 78 40 25 7D A7 78 60 0B 7D 86 78
    7C 60 CF CC 78 40 7D CC 86 68 7D F9 33 F9 48 0B
    48 D4 7C 48 1C 6B 78 25
    """

  Scenario: Successful solution
    Given the code to decrypt
    Then I start searching for the most repeated number
    Then I found it was 7D
    And I assumed this is the space character
    And I replaced it in the code with the text editor
    Then I searched for short words that could be guessed
    Then I noticed the first line was a word and it was long
    And I noticed the repetition and order of some vowels
    Then I guessed the first line was "Congratulations."
    Then I replaced the respective letter codes in the rest of the message
    Then I guessed some other words
    And I kept replacing the respective codes for the new letters found
    And I kept doing the process of guessing and replacing
    Then I fount out the contents of the message
    """
    Congratulations.
    This one washa
    rder,but you go
    t it.Very well
    done fellow hack
    er.The problem
    with this cipher
    is that thekey
    is pretty long.
    I will come up
    with a better en
    cryption shemea
    ny soon.Your so
    lution is:pdpfr
    fclfego.
    """
    And I entered on the site the suggested keyword "pdpfrfclfego"
    Then I solved the challenge
