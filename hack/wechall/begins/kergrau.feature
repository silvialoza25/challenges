Feature:
  Solve the Beginnning
  From wechall site
  Category Realistic, Shell, Linux, Warchall

Background:
  Given I am running Ubuntu Xenial Xerus 16.04 (amd64)
  And I am using Mozilla Firefox Quantum 63.0 (64-bit)
  And I am using OpenSSH_7.2p2 Ubuntu-4ubuntu2.5
  And I am using GNU nano 2.9.8

Scenario: Successful scenario
  Given the challenge URL
  """
  http://www.wechall.net/challenge/warchall/begins/index.php
  """
  Then I opened that URL with Mozilla Firefox
  And I read the problem statement
  """
  You are now becoming a linux superhacker
  Create an SSH account with the form below.
  Then enter the 6 solutions to level 0-5 seperated by comma.
  Example: bitwarrior,Solution1,Solution2,Solution3,Solution4,Solution5

  Proudly presented by the The Warchall(tm) Staff
  Thanks and shouts to xd-- for idea, motivation and inspiration!
  """
  Then I read it I filled a fields to my shh account
  Then I opened a console and access for my ssh account
  Then I typed ls command and I see level directory and WELCOME.txt file
  Then I opened WELCOME.txt file with nano
  And the text says that I can find the levels in
  """
  /home/level
  or
  /home/user/yournick/level
  """
  And I went to first address and I saw the 0-3 levels
  Then I went to 0 directory and I found a README.txt file
  And I opened it with nano and I found the answer level 0 which is
  """
  bitwarrior
  """
  Then I returned to /home/level directory
  And I opened 1 directory and type ls command
  And I saw the content directory
  Then I opened README.txt but I did not find the answer.
  Then I surfed in blue directory
  And its subdirectories but I did not find answer
  Then I returned to /home/level/1 directory
  And typed ls -a command and I saw .bash_history file
  And I opened it with nano
  Then I saw a list of typed commands
  And by logic I deduced that the answer to level 1 is
  """
  LameStartup
  """
  Then I returned to /home/level directory
  And I opened 2 directory and type ls -a command
  And I saw the content directory
  And I saw .bash_history file and opened with nano
  And found the follow command nano .porb/.solution
  Then I closed editor and I went to .porb directory
  And I typed ls -a and I saw .solution file and I opened
  And I found the answer to level 2 which is
  """
  HiddenIsConfig
  """
  Then I returned to /home/level directory
  And I opened 3 directory and type ls -a command
  And I saw the content directory
  And I saw .bash_history file and opened with nano
  And I found the answer to level 3 which is
  """
  RepeatingHistory
  """
  Then I went to /home/user/synapkg/level directory
  And I found the directories of 4 and 5 level
  And I went to 4 directory and typed ls -a
  And I saw a README.txt file
  Then I tried to opened it but I couldn't do it
  Then I typed to ls -l command to check permisions
  And I discover that there was no permisions
  Then I added persmissions for my user I could opened it
  And I found the answer to level 4 which is
  """
  AndIknowchown
  """
  Then I went to /home/user/synapkg/level directory
  And I went to 5 directory and typed ls -a
  And I opened README.txt with nano and says the following
  """
  Protect your /home/user/synapkg/level directory from other users.
  Then wait 5 minutes
  """
  Then I typed chmod 700 /home/user/synapkg/level
  And I typed ls -a and I saw solution.txt file
  Then I opened it with nano
  And I found the answer to level 5 which is
  """
  OhRightThePerms
  """
  And lastly the answer to challenge is
  """
  bitwarrior,LameStartup,HiddenIsConfig,RepeatingHistory,AndIknowchown,
  OhRightThePerms
  """
