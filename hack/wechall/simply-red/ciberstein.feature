Feature:
  Solve Simply Red
  From wechall site
  Category Stegano, Image

Background:
  Given I am running Windows 7 (64bit)
  And I am using Google Chrome Version 70.0.3538.77 (Official Build) (64-bit)

Scenario:
  Given the challenge URL
  """
  https://www.wechall.net/challenge/anto/simply_red/index.php
  """
  Then I opened that URL with Google Chrome
  And I see the problem statement
  """
  Find the sentence hidden in me or I'll have to destroy you.
  "No sacrifice is too great in the service of freedom."
  """
  Then I download the image in statement
  And I analized the image with the hexadecimal text editor
  And I found several words without meaning
  And I put this words as answer but the answer was wrong.

Scenario: Failed Attempt number 1
  Given the challenge url
  Then I open it with Google Chrome
  And I tried to put the word visible in the image as answer
  Then I see that the answer is wrong.

Scenario: Failed Attempt number 2
  Given the challenge url
  Then I open it with Google Chrome
  And I analized the image with the exiftool.exe version 11.1.6.0
  Then I I did not find anything out of the ordinary
  And I put the name of the image as answer but the answer was wrong.

Scenario: Successful solution
  Given the challenge url
  Then I open it with Google Chrome
  And I used a python script to alter the RGB values ​​of the image
  """
  https://ghostbin.com/paste/zheye/raw
  """
  And I see that the script returns a modified image
  Then I see that the image reveals a hidden phrase
  """
  the answer is PrimalOffset
  """
  Then I put as answer PrimalOffset and the answer is correct.
  Then I solved the challenge.
