## Version 1.0
## language: en

Feature: chacking-sha1-password
  Site:
    https://www.mysterytwisterc3.org
  User:
    juanes9327
  Goal:
    Find the last German word in plaintext given a ciphertext with Caesar
  Background:
  Hacker's software:
    |<version>             | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | google chrome        | 75.0  (64 bit) |
  Machine information:
    Given I am accessing the web page using google chrome browser
    And Windows 10 as OS
    And ip 192.168.1.5

  Scenario: fail: Shifting Caesar
    Given I opened te following url
    """
    https://cryptii.com/pipes/caesar-cipher
    """
    Then I shift the ciphertext one place
    And I did not find anything

  Scenario: Success: Searching in the Web
    Given I find a Caesar Decrypt tool
    """
    https://cryptii.com/pipes/caesar-cipher
    """
    Then I put the ciphertext there by shifting two places
    Then I find the last German plaintext
    And  I solved the challenge
