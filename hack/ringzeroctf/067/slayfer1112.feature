## Version 2.0
## language: en

Feature: CHALLENGE-Ringzer0ctf
  Site:
    Ringzer0ctf
  Category:
    CHALLENGE
  User:
    slayfer1112
  Goal:
    Found the flag passing the login form.

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
  Machine information:
    Given I am accessing to the website
    Then I choose a challenge
    And I was redirect to a login form

  Scenario: Fail:Type-a-generic-password
    Given I'm on the challenge page
    And I'm on a login form that ask a username and password
    When I see the login form
    Then I was thinking in use a generic credentials to access
    And I type "admin" as username and password
    When I submit the form
    Then I receive a message that say "Wrong password sorry" [evidence](img1)
    And I fail to found the flag

  Scenario: Success:Inspect-code-to-see-the-script
    Given I'm on the challenge page
    And The generic credentials didn't work
    When I think a way to pass the challenge
    Then I inspect the code searching any hint
    And I found an interesting "<script>"
    """
    $(".c_submit").click(function(event) {
      event.preventDefault();
      var k =
       CryptoJS.SHA256("\x93\x39\x02\x49\x83\x02\x82\xf3\x23\xf8\xd3\x13\x37");

      var u = $("#cuser").val();
      var p = $("#cpass").val();
      var t = true;

      if(u == "\x68\x34\x78\x30\x72") {
        if(!CryptoJS.AES.encrypt(p,
        CryptoJS.enc.Hex.parse(k.toString().substring(0,32)),
        { iv: CryptoJS.enc.Hex.parse(k.toString().substring(32,64)) })
        == "ob1xQz5ms9hRkPTx+ZHbVg==")
        {
          t = false;
        }
      } else {
        $("#cresponse").html("<div class='alert alert-danger'>
          Wrong password sorry.</div>");
        t = false;
      }
      if(t) {
        if(document.location.href.indexOf("?p=") == -1) {
          document.location = document.location.href + "?p=" + p;
                   }
      }
    });
    """
    When I read that I see that "u" us the username
    Then I see that "u" is matching with "\x68\x34\x78\x30\x72"
    And I try to decrypt it [evidence](img2)
    When I decrypt it i know that the username is "<Username>"
    And I need to know what is the password
    When I see that the password encrypted match with
    """
    ob1xQz5ms9hRkPTx+ZHbVg==
    """
    Then I see the script and the script take "k" and encode it
    And Make an encrypted string with "p" and "k"
    When I see this I think that I can use the same library to decrypt
    Then I write the following lines
    """
    var k =
     CryptoJS.SHA256("\x93\x39\x02\x49\x83\x02\x82\xf3\x23\xf8\xd3\x13\x37");

    var M = 'ob1xQz5ms9hRkPTx+ZHbVg==';

    var decrypt = CryptoJS.AES.decrypt(
      M,
      CryptoJS.enc.Hex.parse(k.toString().substring(0,32)),
      { iv: CryptoJS.enc.Hex.parse(k.toString().substring(32,64)) }
    );

    var password = CryptoJS.enc.Latin1.stringify(decrypt);

    console.log(password);
    """
    And I type it in the console on the page [evidence](img3)
    When I submit the script
    Then I receive the decrypted password "<password>"
    And I need to try the username and password in the form
    When I type the username and password in the form
    Then I receive a message with the flag "<flag>" [evidence](img3)
    And I copy and paste the flag in the flag field
