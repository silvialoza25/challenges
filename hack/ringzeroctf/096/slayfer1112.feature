## Version 2.0
## language: en

Feature: CHALLENGE-Ringzer0ctf
  Site:
    Ringzer0ctf
  Category:
    CHALLENGE
  User:
    slayfer1112
  Goal:
    Found the flag making a XSS.

  Background:
  Hacker's software:
    | <Software name>  | <Version>     |
    | Windows OS       | 10            |
    | Chrome           | 79.0.3945.130 |
  Machine information:
    Given I am accessing to the website
    Then I choose a challenge
    And I was redirect to a base64 converter

  Scenario: Fail:Use-generic-XSS-payload
    Given I'm on the challenge page
    And The site say me that type any base64 to generate a random image
    When I type any base64 I didn't see that nothing happen
    Then I try to use the commons xss payload "<script>alert(1)</alert>"
    And I see that nothing happens
    When I think that I can use the payload in base64
    Then I use "btoa()" in the browser to convert my payload to base64
    And I use the base64 payload
    When I press the submit button again
    Then Nothing happen
    And I fail to found the flag.

  Scenario: Fail:Use-src-xss-payload
    Given Inject the script fails
    When I try to figure out how to make a XSS i try to inspect the code
    Then I found a hidden field in the code [evidence 1](evidence-1)
    And I see that this field has a value to decode data image base64
    When I try to send anything with the form
    Then I see a new "<embed>" tag with a "src"
    And I see that these tag has the value of the hidden field and the textarea
    When I see that "src" attribute is used I thin that this can be used
    Then I change the hidden field for "javascript:alert('XSS');//"
    And I try to send the form again
    When I see the response nothing happens
    Then I fail to capture the flag

  Scenario: Fail:Use-src-xss-payload
    Given Use src to inject the xss fails
    When I read about "<embed>" searching any way to scape the filter
    Then I found that "src" in "<embed>" was fixed and can't execute javascript
    And I found too that I can inject html with base64 in "<embed>"
    When I think that I must give a try
    Then Read carefully the tags I found that the back use something like
    """
    `${hiddenfield.value},${textarea.value}`
    """
    And I think that I can't scape the "," with only 1 field payload
    When I figure out how to make it with the 2 fields that I have
    Then I change the hidden field value to "data:text/html;base64"
    And I use the textarea with
    """
    PHNjcmlwdD5hbGVydCgnWFNTJyk8L3NjcmlwdD4=
    """
    When I submit the form
    Then Finally I see the alert [evidence 2](evidence-2)
    And I didn't see any flag
    Then I fail to found the flag

  Scenario: Success:Use-reflected-xss
    Given I success to make a xss
    When I think why I didn't receive any flag
    Then I read again the test and I see this
    """
    Our XSS Bot is only able to get out on ports 80 and 8080 to 8090 don't
    forget this little detail when you gonna build your payload.
    """
    And I think that I must make a reflected-XSS.
    When I was searching about reflected-XSS I found something with "webhook"
    Then I Search for a webhook site online and I found "webhook.site"
    And I add a response body to see if it works
    When I found 2 ways to inject the webhook, I choose the easy way.
    """
    1. how embed tag use src I can put my webhook url
    2. use the html injection to inject a new tag with src with the webhook
    """
    Then I use the url in the value on the hidden field with a "#" at the end
    And I use the "#" to scape the "," and send the form
    When I get the response I see my body [evidence 3](evidence-3)
    Then I go to my webhook and see the registry
    And I found 2 registry
    When I see the registries
    Then I found one of them with the flag [evidence 4](evidence-4)
    When I submit the flag on the site
    And I capture the flag
