## Version 2.0
## language: en

Feature: Crypto-Object
  Site:
    https://ringzer0ctf.com
  Category:
    cryptography
  User:
    AndresCL94
  Goal:
    Get the flag

  Background:
  Hacker's software:
  | <Software name> | <Version>         |
  | Kali Linux      | 5.5.0-kali1-amd64 |
  | Firefox         | 68.7.0esr         |
  | GNU strings     | 2.3.0             |
  | Steghide        | 0.5.1             |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/154
    """
    When I open it in a browser
    Then I see a button to start the challenge
    And I get redirected to a page that only has an image

  Scenario: Fail: Strings-Image
    Given the image the challenge redirected me to
    And that I downloaded it
    When I use the command "strings" to see if there is some hidden data
    Then I do not find anything useful to solve the challenge
    And I do not get the flag

  Scenario: Fail: Steghide
    Given that the image has a rubber band with some characters written on it
    And I write them down "GMODCDOOKCDBIOYDRMKDPQLDPVWYOIVRVSEOV"
    When I use "steghide" with the previous characters as passphrase
    Then I do not get any hidden data
    And I do not get the flag

  Scenario: ROT-Scytale
    Given that the characters in the image are written vertically
    And that it gives the idea of scytale
    And that there are only letters from the alphabet
    When I try several attempts with ROT encryption and column transposition
    Then I get a clear message with ROT16 and a scytale of 3 columns
    """
    W       E       L
    C       O       M
    E       T       O
    T       H       E
    S       C       Y
    T       A       L
    E       T       H
    E       F       L
    A       G       I
    S       B       U
    T       T       E
    R       F       L
    Y
    """
    And I get the flag
