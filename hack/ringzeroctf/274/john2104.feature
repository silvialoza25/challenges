## Version 2.0
## language: en

Feature: youre-drunk-ringerzeroctf
  Site:
    https://ringzer0ctf.com/challenges/274
  Category:
    Cryptography
  User:
    john2104
  Goal:
    Decipher the message

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/274
    """
    When I open the url with Chrome
    Then I see a ciphered message
  Scenario: Fail:Decipher
    Given the ciphered message
    When I try with the CAESAR cipher
    Then I don't get anything legible
    And I don't solve the challenge

   Scenario: Success:Decipher
    Given the ciphered message
    When I go to the page
    """
    https://quipqiup.com/
    """
    And put the string there
    Then I get the message in a legible format
    And the flag
    Then I solve the challenge
