"""
$ pylint dferrans.py #linting
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 dferrans.py #linting

"""
from __future__ import print_function


def decrypt_middle_7_challenge(data):
    """ Function to decrypt """

    size = int(data())
    for _ in range(size):
        encrypted_string = "21052F151200271512413E35101A152F3511"

        dictonary = {"_": "26", ".": "03", ",": "42", ";": "07", ":": "44",
                     "?": "28", "!": "46", " ": "12", "1": "1B", "2": "43",
                     "3": "17", "4": "2B", "5": "01", "6": "2E", "7": "09",
                     "8": "33", "9": "39", "0": "2A", "a": "27", "b": "0B",
                     "c": "41", "d": "45", "e": "0E", "f": "10", "g": "11",
                     "h": "05", "i": "2F", "j": "1C", "k": "16", "l": "18",
                     "m": "04", "n": "35", "o": "3E", "p": "37", "q": "1D",
                     "r": "1F", "s": "15", "t": "21", "u": "1A", "v": "23",
                     "w": "00", "x": "0C", "y": "3B", "z": "30", "A": "1E",
                     "B": "13", "C": "25", "D": "3C", "E": "29", "F": "2C",
                     "G": "31", "H": "22", "I": "0D", "J": "0F", "K": "34",
                     "L": "38", "M": "3A", "N": "3D", "O": "02", "P": "08",
                     "Q": "2D", "R": "36", "S": "24", "T": "32", "U": "20",
                     "V": "0A", "W": "06", "X": "3F", "Y": "19", "Z": "14"}
        inv_map = {v: k for k, v in dictonary.items()}
        result = []
        iterations = len(encrypted_string)

        answer = []
        for pairs_str in range(iterations):
            result.append(encrypted_string[pairs_str:pairs_str+2])

        for str_to_decrypt in result[0::2]:
            answer.append(inv_map[str_to_decrypt])

        print("".join(answer))


decrypt_middle_7_challenge(input)

# $python dferrans.py < DATA.lst
# this was confusing
