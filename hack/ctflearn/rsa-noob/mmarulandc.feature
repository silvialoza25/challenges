## Version 2.0
## language: en

Feature: RSA Noob - Cryptography - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    Cryptography
  User:
    mmarulandc
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 19.10       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A link in the challenge page
    And the challenge has a ".txt" file to download
    And the ".txt" contains a list of mapped integer numbers by letters
    """
    e: 1
    c:9327565722767258308650643213344542404592011161659991421
    n: 245841236512478852752909734912575581815967630033049838269083
    """
  Scenario: Success: Analysing the content of the txt file
    Given The name of the challenge
    And some information at the comments section like "check out on wikipedia"
    When I research about RSA cryptosystem on wikipedia
    Then I feel able to decrypt the message with the ".text" information
    And I realize the list of mapped integer numbers are actually params
    When I see the decryption equation
    """
    c^d <-> (m^e)^d <-> m (mod(n))
    """
    Then I realize I have the "e, c, n" params
    Given "e" equals one, in this case, I can determinade "m = c"
    When the "c" is computed
    Then that "c" must be encoded to hexadecimal and then decoded to ascci
    When I run the next python code, for computing the above
    """
    n = 245841236512478852752909734912575581815967630033049838269083
    c = 9327565722767258308650643213344542404592011161659991421
    e = 1
    res = (c ** e) % n #m (mod(n))
    print ("The flag is ",format(res,"x").decode("hex"))
    """
    Then I get the flag [evidence](evidence.png)
    When I put the value in flag format
    """
    CTFLearn{<FLAG>}
    """
    Then the challenge is solved
