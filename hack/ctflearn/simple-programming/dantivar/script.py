with open('data.dat', 'r') as data:
    text = data.readlines()
    n_lines = 0
    for line in text:
        i = 0
        n_zeros = 0
        n_ones = 0
        while(i < len(line)):
            if line[i] == '0':
                n_zeros += 1
            elif line[i] == '1':
                n_ones += 1
            i += 1
        if n_zeros % 3 == 0:
            n_lines += 1
        elif n_ones % 2 == 0:
            n_lines += 1
    print(n_lines)
