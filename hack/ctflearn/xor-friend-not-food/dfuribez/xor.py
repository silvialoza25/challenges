#!/usr/bin/env python

import itertools

ENC = b"\t\x1b\x11\x00\x16\x0b\x1d\x19\x17\x0b\x05\x1d(\x05\x005\x1b\x1f\t,\r"\
    b"\x00\x18\x1c\x0e"


def find_key(enc: int, clear: int) -> int:
    return enc ^ clear


def main() -> None:
    clear_text = "ctflearn{x"
    key = []

    enc_slice = ENC[len(clear_text):]

    for index, byte in enumerate(clear_text):
        key.append(find_key(ENC[index], ord(byte)))

    while len(enc_slice) > 0:
        for c, k in zip(enc_slice, itertools.cycle(key)):
            print(chr(c ^ k), end="")
        print()
        enc_slice = enc_slice[1:]


main()
