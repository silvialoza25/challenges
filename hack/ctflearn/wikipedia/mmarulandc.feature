## Version 2.0
## language: en

Feature: Wikipedia - Miscellaneous - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    Miscellaneous
  User:
    mmarulandc
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 19.10       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given An IP address: 128.125.52.138
    And the challenge advises me to use Wikipedia

  Scenario: Success: Analysing the content of the search in wikipedia
    Given I enter to Wikipedia
    When I search the IP address in Wikipedia
    Then I see the user contributions
    And there is only one user contribution titled "flag"
    When I see the the difference between revisions of the contribution, here:
    """
    https://en.wikipedia.org/w/index.php?title=Flag&diff=prev&oldid=676540540
    """
    Then I realize the site has a text with the flag [evidence](evidence.png)
    When I put the value in flag format
    """
    CTFLearn{<FLAG>}
    """
    Then the challenge is solved
