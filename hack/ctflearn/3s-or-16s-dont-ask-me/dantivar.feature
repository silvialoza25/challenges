## Version 2.0
## language: en

Feature: 3's or 16's? Don't Ask Me! - Cryptography - ctflearn
  Site:
    https://ctflearn.com/challenge/232
  User:
    dantivar
  Goal:
    Get flag

  Background:
  Hacker's software:
    | <Software name> |   <Version>  |
    | Debian          |   stretch    |
    | Chrome          |      84      |
  Machine information:
    Given A file "3's or 16's.txt"
    And the description of the challenge:
    """
    Simple cryptography. Or, is it?
    https://mega.nz/#!0YMl1S6K!VeHBRSYNbmHRsJ0KpQogPt85CFFGTDGskQ9fzeC5QK8
    """

  Scenario: Success: hex to ascii converter
    Given the file
    Then I check what is inside of it
    And I notice what seems to be hex encoding
    Then I try to convert it to ASCII using the following web page:
    """
    https://www.rapidtables.com/convert/number/hex-to-ascii.html
    """
    And I get a message
    And it says that the operation should be performed 4 more times
    And after I do the "convert to ASCII" operations
    Then I get the flag
