## Version 2.0
## language: en

Feature: Vigenere-cipher - Criptography - ctflearn
  Site:
    https://ctflearn.com/problems/305
  User:
    adrianfcn (ctflearn)
  Goal:
    Generate a secret code

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | buster      |
    | Firefox         | 60.9.0      |
  Machine information:
    Given I have the message "blorpy gwox{RgqssihYspOntqpxs}"

  Scenario: Success: Vigenere decode
    Given I need to decode the message "blorpy gwox{RgqssihYspOntqpxs}"
    And I know that this use Vigenere cipher
    When I look for how the Vigenere cipher works
    Then I found that to decrypt the message a key is necessary
    And I think "blorpy" is the key
    And I found an online decode tool
    """
    https://cryptii.com/pipes/vigenere-cipher
    """
    When I use the online decode tool
    Then I decode the message <flag>
    And I solve the challenge
