## Version 2.0
## language: en

Feature: Corrupted Image - Forensics - ctflearn
  Site:
    https://ctflearn.com/challenge/138
  User:
    dantivar
  Goal:
    Get flag

  Background:
  Hacker's software:
    | <Software name> |   <Version>  |
    | Debian          |   stretch    |
    | Chrome          |      84      |
    | ImageMagick     |    6.9.7     |
    | ghex            |    3.18.3    |
    | base64          |    8.26      |
  Machine information:
    Given A file "unopenable.gif"
    And the description of the challenge:
    """
    Help! I can't open this file. Something to do with the file header…
    Whatever that is.
    https://mega.nz/#!aKwGFARR!rS60DdUh8-jHMac572TSsdsANClqEsl9PD2sGl-SyDk
    """

  Scenario: Success: ghex Forensics
    Given the file
    And that the challenge says there is a problem with the header
    Then I open the file with "ghex"
    And I notice that "GIF8" is missing in the header
    Then I set "ghex" in "Insert mode"
    And add the missing information in the header
    Then I am able to open the gif
    But it is too fast so I try the following line in shell:
    """
    convert -delay 400x100 unopenable.gif unopenable.gif
    """
    Then I am able to read the information inside the gif
    And I write it down
    Then I decode it using "base64"
    And I have the flag
