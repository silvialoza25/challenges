## Version 2.0
## language: en

Feature: milks best friend - forensics - ctflearn
  Code:
    195
  Site:
    ctflearn
  Category:
    forensics
  User:
    Calderonsin
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Ubuntu          | 16.01        |
    | Firefox         | 71.0         |
    | binwalk         | 2.1.1        |
    | ExifTool        | 11.84        |
  Machine information:
    Given I am accessing the challenge site via browser

  Scenario: Failed:Trying to search something in the img
    When I check the challenge
    Then I download the challenges's img
    And  I open the img
    And  I can't find nothing interesting

  Scenario: Failed:Look into the metadata of the img
    When I use Exiftool
    """
    https://exiftool.org/index.html
    """
    Then I look into the metadata of the img
    Then I can't find nothing interesting

  Scenario: Success:Look for hidden files
    When I think about hidden files
    Then I use binwalk
    """
    https://www.refirmlabs.com/binwalk/
    """
    Then I find a zip file
    And  I unzip it
    When I look into the folder
    Then I find another img
    Then I use binwalk to search for hidden files
    And  I find nothing
    Then I use the command hexdump to look all the bytes in the file
    And  I find the flag  in the last 2 block of bytes
    Then I submit it and solve the challenge
