## Version 2.0
## language: en

Feature:
  Site:
    ctflearn
  Category:
    Forensics
  User:
    mr_once
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Foremost        | 1.5.7       |
    | Python          | 3.8.5       |
    | base64          | 8.32        |
  Machine information:
    Given I am downloading an image from
    """
    https://mega.nz/#!vf43RCyC!NNpuYjB3d-gevhsHXefwAAAmzk4tJHxUZr0GnrSDI_c
    """
    And it is a ".jpg" file
    And the description of the challenge is
    """
    You'll need Brute Force to solve this. Knowing Python should help too.
    Oh! And Base64 encryption of course! Find the flag!

    https://mega.nz/#!vf43RCyC!NNpuYjB3d-gevhsHXefwAAAmzk4tJHxUZr0GnrSDI_c
    Hash: e82a4b4a0386d5232d52337f36d2ab73
    """

  Scenario: Success:Find hidden files
    Given I am given an image
    When I use "foremost" on it
    """
    $ foremost legotroopers.jpg
    """
    Then I can see that there is a ".zip" hidden inside [evidence](zip.png)
    And inside the ".zip" file I can see a password-protected file
    And a non protected folder [evidence](files.png)
    When I browse inside the folder I find this note:
    """
    Hmmm... almost!
    The password is: "ctflag*****" where * is a number.
    Encrypt the password using MD5 and compare it to the given hash!
    As I said, you're gonna have to brute force the password!
    Good luck! :)
    """
    Then I need to brute force the flag
    And compare it with the hash given in the description

  Scenario: Success:Write a script to brute force the flag
    Given That now I know I have to brute force the flag
    When I write an run the script "bruteforce.py"
    And the script tries all possible five number combinations
    Then I can see that the flag is
    """
    $ python bruteforce.py
    ctflag48625
    """

  Scenario: Fail:Try the flag to validate the challenge
    Given that now I have the flag
    When I try to use that flag to validate the challenge
    Then I get that the flag is not valid

  Scenario: Success:Find the rigth flag
    Given that the bruteforced flag did not work to validate the challenge
    When I use the same flag to open the password protected zip
    Then I can open it and I can see in a file named "flag.txt"
    """
    RkxBR3ttYXlfdGhlX2JydXRlX2ZvcmNlX2JlX3dpdGhfeW91fQ==
    """
    When I decode it
    """
    $ echo RkxBR3ttYXlfdGhlX2JydXRlX2ZvcmNlX2JlX3dpdGhfeW91fQ== | base64 -d
    """
    Then I get the rigth flag
    """
    FLAG{may_the_brute_force_be_with_you}
    """
    And I solve the challenge
