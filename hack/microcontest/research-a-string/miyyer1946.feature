## Version 2.0
## language: en

Feature: microcontest-research-a-string

  Site:
    http://www.microcontest.com/contest.php?id=16
  Category:
    Hacking
  User:
    miyyer1946
  Goal:
    Send the parameters required by the challenge through a POST request

  Background:
  Hacker's software:
    | <Software name>  | <Version>    |
    | Windows          |      10      |
    | Microsoft Edge   | 84.0.522.63  |
    | Curl             |    7.74      |
  Machine information:
    Given I am accessing the challenge site
    When reading the challenge information, it mentions
    """
    Un nombre décimal s'est glissé dans la chaîne de caractères chaine.
    A vous de le retrouver et de le renvoyer dans la variable nombre.
    """

  Scenario: Fail: POST request sending greater than 4 seconds
    Given the values retrieved by the challenge page [miyyer1946](01.png)
    """
    http://www.microcontest.com/contests/16/contest.php

    Nombre_variables=1
    [chaine]
    Longueur=85
    Valeur=B\G\JNbQSyYNl}e[GOuSr225.31533458689fRqu\Oa{qeAGyZqfavOybd\
    gWQpztIftQzqm]`TvUISIBJRvy
    """
    When I check the answer, a floating number is identified within the string
    Then this value must be sent as a floating variable via POST request
    """
    curl.exe -b cookie.txt -d "nombre=225.31533458689"
    http://www.microcontest.com/contests/16/validation.php

    success:1
    timeout:1
    already:0
    """
    When the response is validated according to the website documentation
    """
    http://www.microcontest.com/interface.php

    success will be worth 1 when the returned settings are correct, 0 if not.
    timeout will be worth 1 if you have exceeded the allotted time of 4 seconds
    0 otherwise.
    already will be worth 1 if you have already validated the test, 0 if not.
    points will only appear when you validate the event for the first time,
    so when you have success:1 timeout:0 already:0.
    """
    Then it is observed that the challenge has failed
    And that its cause is due to a response time greater than 4 seconds

  Scenario: Successful: use a shell script to solve the challenge
    Given a shell script, the execution of the CURL commands is automated
    When I use the script in the terminal [miyyer1946](test.sh)
    Then the content is processed and the response is sent via CURL
    And I solved the challenge [miyyer1946](02.png)(03.png)
