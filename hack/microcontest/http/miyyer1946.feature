## Version 2.0
## language: en

Feature: microcontest-http

  Site:
    http://www.microcontest.com/
  Category:
    Hacking
  User:
    miyyer1946
  Goal:
    Send the response to the server in less than 3 seconds

  Background:
  Hacker's software:
    | <Software name>  | <Version>    |
    | Windows          |      10      |
    | Microsoft Edge   | 84.0.522.63  |
    | Raspberry pi OS  |     5.4      |
    | Python           |    3.8.3     |

  Scenario: Fail: unknown username and password
    Given the values retrieved by the challenge page[miyyer1946](01.png)
    """
    http://www.microcontest.com/contests/15/contest.php

    Nombre_variables=1
    [x]
    Valeur=413
    """
    Then I send the value as a parameter using GET request [miyyer1946](02.png)
    """
    http://www.microcontest.com/contests/15/nombre.php?x=413

    1229.736032983
    """
    And the floating return value send it to the server with the POST request
    Given parameters and POST request cannot be sent using the web browser
    And the session start cookie and shipping less than 3 seconds is required
    Then I can't solve the challenge

  Scenario: Successful: use python script to solve the challenge
    Given the python requests module I write python script
    And I sending HTTP requests to the server [miyyer1946](test.py)
    Then I get the sequence of success [miyyer1946](03.png)
    And I solved the challenge [evidences](04.png)
