import requests
from bs4 import BeautifulSoup

payload = {
    'username': 'miyyer1946',
    'password': '***PASSWORD SHA1 HASH***',
    'ID': '44',
    'contestlogin': '1',
    'version': '2'
}
URL = "http://www.microcontest.com/contests/44/contest.php"
r = requests.post(URL, data=payload)
cookie = r.cookies
match = r.text.find("Valeur=")
username = r.text[match + 7: - 5]
URL = "http://www.wechall.net/en/profile/" + username
r = requests.get(URL)
soup = BeautifulSoup(r.text,"lxml")
mydivs = str(soup.findAll("div", {"class":"fl"}))
value = mydivs.find(username + "#rank_")
selection = (mydivs[value + len(username) + 6:value + len(username) + 16])
limit = selection.find("\">")
rank = selection[:limit]
value = mydivs.find(username + "\">")
selection = (mydivs[int(value) + len(username) + 2:int(value) + 20])
limit = selection.find("</")
score = selection[:limit]
value = mydivs.find("Register Date" + "</th")
selection = (mydivs[int(value) + 22:int(value)+50])
limit = selection.find("</")
register_date = selection[:limit]
value = mydivs.find("Last Activity" + "</th")
selection = (mydivs[int(value) + 22:int(value) + 50])
limit = selection.find("</")
last_activity = selection[:limit]
payload = {
    'score': str(score),
    'rank': str(rank),
    'register_date': str(register_date),
    'last_activity': str(last_activity)
}
URL = "http://www.microcontest.com/contests/44/validation.php"
r = requests.post(URL, data=payload, cookies=cookie)
print(r.text)
