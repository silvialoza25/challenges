## Version 2.0
## language: en

Feature: microcontest-html-parsing

  Site:
    http://www.microcontest.com/contest.php?id=44
  Category:
    Hacking
  User:
    miyyer1946
  Goal:
    Send the parameters required by the challenge through a POST request

  Background:
  Hacker's software:
    | <Software name>  | <Version>    |
    | Windows          |      10      |
    | Microsoft Edge   | 84.0.522.63  |
    | Raspberry pi OS  |     5.4      |
    | Python           |    3.8.3     |
    | Curl             |    7.74      |
    | BeautifulSoup    |    4.8.2     |

  Scenario: Fail: POST request sending greater than 4 seconds
    Given the values retrieved by the challenge page [evidences](01.png)
    """
    http://www.microcontest.com/contests/44/contest.php

    Nombre_variables=1
    [username]
    Longueur=4
    Valeur=tuxy
    """
    Then I send the value as a parameter using GET request [evidences](02.png)
    """
    http://www.wechall.net//profile/tuxy
    """
    When I access the website I capture the required data that are:
    """"
    Score, Global Rank, Register Date, Last Activity
    """
    Then I use Curl to send the values as POST parameters
    """
    curl.exe -b cookie.txt -d
    "score=21297&
    rank=1094&
    register_date=May 26, 2009 - 11:30:11&
    last_activity=Sep 01, 2010 - 20:09:33"
    http://www.microcontest.com/contests/44/validation.php

    success:1
    timeout:1
    already:0
    """
    When the response is validated according to the website documentation
    """
    http://www.microcontest.com/interface.php

    success will be worth 1 when the returned settings are correct, 0 if not.
    timeout will be worth 1 if you have exceeded the allotted time of 4 seconds
    0 otherwise.
    already will be worth 1 if you have already validated the test, 0 if not.
    points will only appear when you validate the event for the first time,
    so when you have success:1 timeout:0 already:0.
    """
    Then it is observed that the challenge has failed
    And that its cause is due to a response time greater than 4 seconds

  Scenario: Successful: use a python script to solve the challenge
    Given the request module, I write python script for handling HTTP requests
    When I use the BeautifulSoup library I can capture the HTML content
    Then I send HTTP requests to the server [evidences](challenge.py)
    And I solved the challenge [evidences](03.png)(04.png)
