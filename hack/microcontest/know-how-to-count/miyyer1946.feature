## Version 2.0
## language: en

Feature: microcontest-know-how-to-count

  Site:
    http://www.microcontest.com/contest.php?id=3
  Category:
    Hacking
  User:
    miyyer1946
  Goal:
    Send the parameters required by the challenge through a POST request

  Background:
  Hacker's software:
    | <Software name>  | <Version>    |
    | Windows          |      10      |
    | Microsoft Edge   | 84.0.522.63  |
    | Raspberry pi OS  |     5.4      |
    | Python           |    3.8.3     |
    | Curl             |    7.74      |

  Scenario: Fail: POST request sending greater than 4 seconds
    Given a text find the repetitions of the variable mot [miyyer1946](01.png)
    """
    http://www.microcontest.com/contests/3/contest.php

    Nombre_variables=2
    [texte]
    Longueur=612
    Dans la plaine rase, sous la nuit sans étoiles, d’une
    obscurité et d’une épaisseur d’encre, un homme suivait seul la
    grande route de Marchiennes à Montsou dix kilomètres de pavé
    coupant tout droit, à travers les champs de betteraves. Devant
    lui, il ne voyait même pas le sol noir, et il n’avait la sensation de
    l’immense horizon plat que par les souffles du vent de mars, des
    rafales larges comme sur une mer, glacées d’avoir balayé des
    lieues de marais et de terres nues. Aucune ombre d’arbre ne
    tachait le ciel, le pavé se déroulait avec la rectitude d’une jetée,
    au milieu de l’embrun aveuglant des ténèbres.
    [mot]
    Longueur=2
    Valeur=la
    """
    When I check the text I find the variable "la" 5 times
    Then this value must be sent as an integer variable "occ" POST parameter
    """
    curl.exe -b cookie.txt -d "occ=5"
    http://www.microcontest.com/contests/3/validation.php

    success:1
    timeout:1
    already:0
    """
    When the response is validated according to the website documentation
    """
    http://www.microcontest.com/interface.php

    success will be worth 1 when the returned settings are correct, 0 if not.
    timeout will be worth 1 if you have exceeded the allotted time of 4 seconds
    0 otherwise.
    already will be worth 1 if you have already validated the test, 0 if not.
    points will only appear when you validate the event for the first time,
    so when you have success:1 timeout:0 already:0.
    """
    Then it is observed that the challenge has failed
    And that its cause is due to a response time greater than 4 seconds

  Scenario: Successful: use a python script to solve the challenge
    Given the request module, I write a python script for handling HTTP requests
    Then I send the POST request to the server [miyyer1946](challenge.py)
    And I solved the challenge [miyyer1946](02.png)(03.png)
