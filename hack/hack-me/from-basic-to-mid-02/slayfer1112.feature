## Version 2.0
## language: en

Feature: CHALLENGE-Hac.me
  Site:
    Hac.me
  Category:
    CHALLENGE
  User:
    slayfer1112
  Goal:
    Found the flag to pass to the next stage

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
  Machine information:
    Given I am accessing to the website
    Then Start the virtual machine on the website
    And The website ask for a password to continue

  Scenario: Fail:Type-a-generic-password
    Given I complete the first stage
    And I'm on the field that ask me the password
    When I try some generics passwords like "admin", "Admin123", "pssword", etc
    Then I receive a popup with a error message [evidence](img1)
    And I fail to access

  Scenario: Fail:Inspect-code-to-see-the-script
    Given I complete the first stage
    And I'm on the field that ask me the password
    When I Inspect the page to see the HTML
    Then I see a function in the bumit button "checkForm();" [evidence](image2)
    And I try to find the script to see the function
    When I found the script I see the function "checkForm()"
    """
    function checkForm() {
      var password = document.getElementById('password');
      if(password.value.length < 8) {
        alert('Error: Password must contain at least eight characters!');
        password.focus();
        return false;
      }
      if(password.value.length >= 10) {
        alert('Error: Password must contain less than ten characters!');
        password.focus();
        return false;
      }
      re = /[0-9]/;
      if(!re.test(password.value)) {
        alert('Error: password must contain at least one number (0-9)!');
        password.focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(password.value)) {
        alert('Error: password must contain at least one lowercase letter
                (a-z)!');
        password.focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(password.value)) {
        alert('Error: password must contain at least one uppercase letter
                (A-Z)!');
        password.focus();
        return false;
      }
      if(password.value == '<password>') {
        form = document.getElementById('form');
        form.action = password.value + '.html'
        form.submit();
      } else {
        alert('You entered a valid password, but not the correct one!
                You entered: ' + password.value);
        password.focus();
        return false;
      }
    }
    """
    Then I inspect that to find any clue for the password
    And I found the password "<password>"
    When I input the "<password>" on the password field
    Then I receive a popup that say that the password failed [evidence](img3)
    And I fail to found the flag

  Scenario: Fail:Inspect-code-to-see-the-script
    Given I complete the first stage
    And I have the script "checkForm"
    Then I found the "<password>" but fail
    And I'm on the field that ask me the password
    When I try to debug the script
    Then I fond that the password fail for this lines
    """
    if(password.value.length >= 10) {
      alert('Error: Password must contain less than ten characters!');
      password.focus();
      return false;
    }
    """
    And I see that password always fail for that reason

  Scenario: success:Change-url-with-the-password
    Given I complete the first stage
    And I have the script "checkForm"
    Then I found the "<password>" but fail
    And I'm on the field that ask me the password
    When I see a line that have the attributte "form.action"
    """
    if(password.value == '<password>') {
      form = document.getElementById('form');
      form.action = password.value + '.html'
      form.submit();
    }
    """
    Then I see a hint that may help to complete the stage
    And I try to use in the URL the value of "password.value + '.html'"
    When I change the page "H4rd3r-X.html" to "<password>.html"
    Then I'm reditected to the next step [evidence](img4)
    And I found the flag for the stage 2
