## Version 2.0
## language: en

Feature: Local File Inclusion
  Site:
    root-me.org
  Category:
    Web-Server
  User:
    caos1986
  Goal:
    Get in the admin section

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2021.1    |
    | Firefox Browser       | 68.7.0esr |

  Machine information:
    Given the challenge url
    """
    https://www.root-me.org/en/Challenges/Web-Server
    /Local-File-Inclusion
    """
    Then I can see the start button
    And This can be seen in [evidence](img1.png)
    And I can access a web page
    """
    http://challenge01.root-me.org/web-serveur/ch16/
    """
    And This can be seen in [evidence](img2.png)

  Scenario: Fail: Wrong Route
    Given I can browse the website
    Then I click the Sysadmin link
    And I can see the files and f URL parameters
    And This can be seen in [evidence](img3.png)
    When I try to change the f page parameter
    Then I first try to read the index.php file
    And The site does not show any error
    And This can be seen in [evidence](img4.png)
    Then I fail the attempt

  Scenario: Success: LFI
    Given I see the admin link
    When I click the admin link
    Then The page prompts a username and password dialog
    And I figure that it reads from the admin folder
    When I first read the "../../index.php" file
    Then The page shows the file contents
    And This can be seen in [evidence](img5.png)
    When I first read the "../../admin/index.php" file
    Then The page shows the file contents
    And This can be seen in [evidence](img6.png)
    And I can see the admin credentials
    When I try using those credentials in the admin dialog
    Then The page says
    """
    You could use this password to validate the challenge !
    """
    And This can be seen in [evidence](img7.png)
    Then I write the password as the solution
    And The challenge is solved
    And This can be seen in [evidence](img8.png)
    And I solved the challenge
    And I caught the flag
