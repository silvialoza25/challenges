## Version 2.0
## language: en

Feature: HTML Source code - Web-server - Root Me

  Site:
    https://www.root-me.org
  Category:
    web server
  User:
    andrewmiy95
  Goal:
    Decrypt the password using the HTML source code

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows          | 10           |
    | Microsoft Edge   | 84.0.522.63  |

  Machine information:
    Given I am registered in root-me
    Given the url "https://www.root-me.org"

  Scenario: Successful: use the web browser's code inspection tool
    Given a web form, the access password is required
    Then you access the html source code inspection
    When you get the source code, you find a comment with the access password
    Then I can actually read the password of access
    And I conclude that the code inspection worked
    And I solved the challenge
    And I can read the password
