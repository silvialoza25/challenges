## Version 2.0
## language: en

Feature:
  Site:
    www.root-me.org
  Category:
    BoF
  User:
    bianfa
  Goal:
    Get the crackme inside ELF executable

  Background:
  Hacker's software:
    | <Software name> | <Version>            |
    | Kali Linux      | 2020.3               |
    | Firefox         | 68.10.0esr           |
    | ghex            | 3.18.4               |
    | radare2         | 5.0.0                |
    | gdb             | 10.1.90.20210103-git |
    | pwndbg          | 1.1.0                |
  Machine information:
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Cracking/ELF-Random-Crackme
    """
    When I open this URL with Firefox
    Then I click on "Start the challenge"
    And I get a compressed file called "ch5.zip"
    When I unzip the file
    Then I get a file called "crackme_wtf"

  Scenario: Fail: I ignore the ptracer of the executable
    Given the file "crackme_wtf" when I unzip the file "ch5.zip"
    When I run "file ./crackme_wtf"
    Then linux doesn't recognize it as an ELF file
    And this can be seen in [evidence](img1.png)
    And I run "ghex ./crackme_wtf" to see and edit the file in hexadecimal
    Then I see more data before of ".ELF" with which all ELF files start
    And this can be seen in [evidence](img2.png)
    Then I think that this data is trash
    And I remove this data as it can be seen in [evidence](img3.png)
    When I run "file ./crackme_wtf"
    Then I can see the information of an ELF file
    And this can be seen in [evidence](img4.png)
    And I try to run the file "crackme_wtf" but get "permission deniend"
    Then I run "chmod +x ./crackme_wtf" to add execution permissions
    And I run "./crackme" again and the file working good
    And this can be seen in [evidence](img5.png)
    Then I have to enter a valid password to get the flag
    And I reverse engineer because I don't have a valid password
    When I run "radare2 ./crackme_wtf"
    Then I try to debug the program whit radare2
    And I run "aaaa" to analyze the program with radare2
    Then I run "pdf @ main" to I get all instructions of the "main" method
    And this can be seen in [evidence](img6.png)
    And the first thing that catches my eye is the call to "strcmp"
    And I think that the password is compared with the input with this call
    And this can be seen in [evidence](img7.png)
    When I run "gdb ./crackme_wtf" to debug the program with pwndbg from gdb
    Then I try to create breakpoints to debug the program in an instruction
    And I run "b *0x08048bde" to create a break in the call to "strcmp"
    And this can be seen in [evidence](img8.png)
    Then I run "run" inside of pwndbg
    And I get an error as it can be seen in [evidence](img9.png)

  Scenario: Fail: I'm considering the ptracer of the executable
    Given the file "crackme_wtf" working like an ELF file executable
    When I run "gdb ./crackme_wtf" to debug the program with pwndbg from gdb
    Then I run "run" inside pwndbg to run the ELF file
    And the same program closes because it detects the pwndbg debugger
    And this can be seen in [evidence](img9.png)
    And I conclude that the program has a instruction for this
    Then I run "pdf @ main" after I run the radare2 with ELF file
    And I check instructions of "main" method
    And I can see a call to "ptrace"
    And this can be seen in [evidence](img10.png)
    And I think that whit these instructions could it detects the debugger
    Then I run "b *0x08048bde" in pwndbg to check the call to "strcmp"
    And I try to check that the password is compared with the input there
    And I run "b *0x08048af2" in pwndbg
    Then I try to skip the instructions they use the call to "ptrace"
    And I run "run" in pwndbg
    When I get to the "ptrace" break
    Then I run "set $eip = 0x08048b2c" to skip the instructions of "ptrace"
    And this can be seen in [evidence](img11.png)
    And I run "c" for that the program is running
    Then the program asks me for the password
    And I enter anything
    And I click "enter" key
    And this can be seen in [evidence](img12.png)
    Then I get to the "strcmp" break
    And I can see the password that is compared the user input
    And this can be seen in [evidence](img13.png)
    Then I run "run" in pwndbg again
    And I get to the "ptrace" break
    Then I run "set $eip = 0x08048b2c" to skip the instructions of "ptrace"
    And I run "c" for that the program is running
    And I enter the password with which it is compared the user input
    And I get an error because the password is a random number more
    """
    _VQLGE_TQPTYD_KJTIV_
    """
    And this can be seen in [evidence](img14.png)

  Scenario: Success: I'm considering the ptracer & random pass of the executable
    Given the file "crackme_wtf" working like an ELF file executable
    When I run "pdf @ main" in radare2
    Then I check the instructions of the "main" method
    And I see the call to "sprintf"
    And this can be seen in [evidence](img15.png)
    And I know that "sprintf" is used to concatenate string with a format
    Then I check previous instructions to the call to "sprintf"
    And I can see that the constant "s" is set to "eax" register
    And this can be seen in [evidence](img16.png)
    Then I check the constant "s" value
    And I see that "s" is the memory address "ebp-0x1007b"
    And I understand that what is set to "eax" is a memory address
    Then I run "b *0x08048a08" in pwndbg to set a break before of "sprintf"
    And I run "b *0x08048a0d" in pwndbg to set a break after of "sprintf"
    And I run "run" in pwndbg to debug the program
    When the program reaches the first break
    Then I can see the memory address of the constant "s" in "eax" register
    And this can be seen in [evidence](img17.png)
    And I run "x/8s 0xfffed1ad" to see the content of this memory address
    And I see that the memory address is empty
    Then I run "c" for that the program is running
    Then the program reaches the second break
    And I run "x/8s 0xfffed19d" to see the content of this memory address again
    And I can see likely a valid password
    And this can be seen in [evidence](img18.png)
    Then I copy the password
    And I run "b *0x08048af2" to create break in the call to "ptracer"
    And I run "c" again
    When the program reaches to new break
    And I run "set $eip = 0x08048b2c" for skip the instructions of "ptracer"
    And this can be seen in [evidence](img19.png)
    And I run "b *0x08048bde" to create a break in the call to "strcmp"
    And I run "c" again
    Then The program asks me for the password
    And I enter the password copied from memory address "0xfffed19d"
    And the program reaches to new break
    And I can see that the password that I copied is equal to compared
    And this can be seen in [evidence](img20.png)
    Then I run "c" again
    And I get the flag as it can be seen in [evidence](img21.png)
