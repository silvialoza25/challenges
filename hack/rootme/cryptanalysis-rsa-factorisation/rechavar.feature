## Version 2.0
## language: en

Feature: RSA factorization - Cryptanalysis - Root Me

  Site:
    https://www.root-me.org
  Category:
    cryptanalysis
  User:
    rechavar
  Goal:
    Decrypt the password using the public key

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows         |     10      |
    | Google-chrome   |84.0.4147.125|
    | Python          |    3.8.5    |
    | Pycryptodome    |    3.9.8    |
    | Jupyter         |    1.0.0    |
    | Sympy           |    1.6.1    |

  Machine information:
    Given The challenge URL
    """
    https://www.root-me.org/es/Challenges/Criptoanalisis/RSA-Factorisation
    """
    And The challenge statement
    """
    The validation password was encrypted using this public key.
    """
    And 10 related sources
    """
    Cryptanalysis of short RSA secret exponents (Cryptographie/Asymétrique).
    DROWN: Breaking TLS using SSLv2 (Cryptographie/Asymétrique).
    Chosen ciphertext attacks against protocols based on the RSA encryption
    standard - Daniel Bleichenbacher (Cryptographie/Asymétrique).
    Cryptographie asymétrique (Cryptographie/Asymétrique).
    Continued Fractions - RSA (Cryptographie/Asymétrique).
    Chiffrement à clef publique, authentification et distribution des clefs
    (Cryptographie/Asymétrique).
    Fractions Continues et Algorithme LLL - RSA (Cryptographie/Asymétrique).
    A new and optimal chosen-message attack on RSA-type cryptosystems
    (Cryptographie/Asymétrique).
    Power Attack on Small RSA Public Exponent (Cryptographie/Asymétrique).
    Asymétrique (Cryptographie).
    """
  Scenario: Fail:Decrypt .pem file using base64
    Given I started the challenge
    And I downloaded a compressed file
    Then I uncompressed it
    And there's a .pem file named pubkey
    Then I searched on Google what is a .pem file
    When I realized that is a container format that it's encoded in base64
    Then I opened jupyter notebook
    And I tried to read the pem file using base64 library
    Then I loaded pubkey.pem file and split the string to have the public key
    When I got the binary string
    And I converted the string to hexadecimal
    When I got the private key 'e'
    """
    e = 82604614372316432124942744109740217522526897765917451489052586460818155
    324154738966632345006582603335636376212459354460842489833740433626007949352
    289280839307228223693008685025771963414976954956772262417319895334829186286
    981419276224745357508609
    """
    When I could not find the modulus 'n'
    Then I realized that I end up in a dead end

  Scenario: Success:Cryptanalysis
    Given I started the challenge
    And I downloaded a compressed file
    Then I uncompressed it
    And there's a .pem file named pubkey
    Then I searched on Google what is a .pem file
    When I realized that is a container format that it's encoded in base64
    Then I searched on Google how to read those files using python
    When I found Pycryptodome
    Then the documentation tells that I can read pubkey.pem
    And I can get the public key 'e' and the modulus 'n'
    """
    f = open("pubkey.pem", "r")
    key = RSA.importKey(f.read())
    key.e= 65537
    key.n= 1881988129206079638386972394616504398071635633794173827007633564
    22988859715234665485319060606504743045317388011303396716199692321205734
    031879550656996221305168759307650257059
    """
    Then I searched on Google about RSA cryptography system
    When I ended up in Wikipedia learning about RSA
    """
    https://es.wikipedia.org/wiki/RSA
    """
    Then I realized that RSA it's a symmetric cryptosystem
    And I learned about the math behind this system
    Then I realized that
    """
    The private key d is the modular multiplicative inverse of e and phi(n)
    where e is the public key and phi(n) is the euler function applied on n:
    d*e = 1 mod phi(n)
    where: phi(n) = (p-1)*(q-1) being p and q prime numbers
    and: n = p*q
    """
    Then I read the paper named "Cryptanalysis of Short RSA Secret Exponents"
    And it says that if I want to know the private key I need to factorized 'n'
    Then I searched on Google if exists any database of factors
    And I found a webpage that is a database of factors
    """
    http://factordb.com/
    """
    When I searched the number 'n'
    And I found
    """
    p = 398075086424064937397125500550386491199064362342526708406385189575946
    388957261768583317
    q =  72772146107435302536223071973048224632914695302097116459852171130520
    711256363590397527
    """
    Then I found 'phi(n)'
    """
    phi(n) = (p-1)(q-1) = 188198812920607963838697239461650439807163563379417
    3827007633564229888597152346654853181897592722115450774546627308732814838
    60342148089408054684419635514838068545682291276216
    """
    And I tried to find 'd'
    When the result was a very low number
    And I did not get satisfied with that number
    Then I searched on Google how to get the modular multiplicative inverse
    When I found a python library named Sympy
    """
    Use sympy.mod_inverse(a, m) to return a number c such that,
    (a * c) = 1 (mod m)
    """
    And the result was
    """
    d = 48318251158920145864930035723053089097690375168562443830554407970661
    009102774592695057202204701378327623682075089028698815133956650193819978
    456750305738325912711134934968241
    """
    Then I decoded the ciphertext using base64 library in python
    """
    c_bytes = base64.b64decode(c_base64)
    """
    And I converted 'c_bytes' to hexadecimal
    """
    c = int.from_bytes(c_bytes, 'big')
    c = 119597105930544804939860850116469838181273811698209889318158329611052
    7471005321301420399097847250216549684904459387347269287231043829453084220
    50675301929768086395211461791808
    """
    Then I tried to find the message 'm' using
    """
    m = (c^d) mod n
    """
    And my computer crashed
    Then I thought that c^d its very expensive computative process
    When I went to rootme forum to search if there's any clue to continue
    Then I found that someone posts a page where I can decrypt the message
    And with the ciphertext and the private key in base64
    """
    https://8gwifi.org/rsafunctions.jsp
    """
    Then I searched in Pycryptodome documentation if I can export d to pem.file
    When I found a function that creates an RSAkey object
    And  this object can export the private key 'd' to .pem file
    """
    rsa_object = RSA.construct((key.n, key.e, d, p, q))
    with open("private.pem",'wb') as f:
        f.write(rsa_object.exportKey())
    f.close()
    """
    When I opened the private.pem file
    And copy the private key in base64
    Then I opened the 8gwifi page and puts the private key and ciphertext
    And I got the flag
    Then I pass the challenge!
