## Version 2.0
## language: en

Feature: forensic-keylogger
  Site:
    https://www.root-me.org/es/Challenges/Forense/Homemade-keylogger
  User:
    alestorm980 (wechall)
  Goal:
    Recover the contents of the file /tmp/flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04       |
    | Python          | 2.7.15+     |
    | GNU coreutils   | 8.28        |
  Machine information:
    Given Some commands in the description
    Then I have a file with some characters
    Then I find the content of the file "/tmp/flag"

  Scenario: Fail: Command strings on the file
    Given The file "ch24.txt" with random characters
    When I run the command "strings"
    """
    strings ch24.txt
    """
    Then I get the content of the file without any information
    And It doesnt look like a good aproach

  Scenario: Success: Base64 decode and python script
    Given The file "ch24.txt" with random characters
    When I read the challenge description
    Then I see that the file is the output of the command
    """
    base64 /tmp/ch24 > ch24.txt
    """
    And I decide to decode the file
    """
    cat ch24.txt | base64 -d > events.bin
    """
    When I decode the file
    Then I get a weird file containing binary information [evidence](image1.png)
    When I read the challenge description again
    Then I read that the file should be the output of the command
    """
    cat /dev/input/event0 > /tmp/ch24
    """
    When I read about that file
    Then I find that the "/dev/input/event0" contains the events of the keyboard
    When I search how to decode it
    Then I find a python script "keylogger.py" that I decide to modify
    """
    https://stackoverflow.com/questions/5060710/format-of-dev-input-eventB
    """
    When I have the python script
    Then I create the file "keylogger.txt"
    When I have that file containing the key and the type of command
    Then I add to my script a way to convert the events into the ascii value
    When I see that the keyboard is changed using the command
    """
    setxkbmap fr
    """
    Then I create a keymap to match the integers values with the ascii
    When I run the python script
    """
    python keylogger.py
    """
    Then I get the commands typed for the user [evidence](image2.png)
    When I see the output
    Then I know that I need the output of the command
    """
    echo c0nGralut4t10n__$(./hihi.py $(date +%s)
    | sha1sum | grep -o '^[0-9a-f]\+')_hoh0 > /tmp/flag
    """
    And I need the "hihi.py" file
    When I find the content of that file in the output of my script
    Then I create the "hihi.py" file [evidence](image3.png)
    When I see that I need the "$(date +%s)"
    Then I take it from my "keylogger.txt" that is created by my python script
    """
    1510683198
    """
    When I run the complete command
    """
    echo c0nGralut4t10n__$(./hihi.py 1510683198
    | sha1sum | grep -o '^[0-9a-f]\+')_hoh0 > /tmp/flag
    """
    Then I get the flag
    """
    c0nGralut4t10n__<sha1sum>_hoh0
    """
    And I solve the challenge
