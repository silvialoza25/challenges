## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  User:
    caos1986
  Goal:
    Get the password stored in the .passwd file

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2021.1    |
    | Firefox Browser       | 68.7.0esr |

  Machine information:
    Given the challenge url
    """
    https://www.root-me.org/en/Challenges/App-Script/Bash-cron
    """
    Then The site gives the ssh credentials "app-script-ch4:app-script-ch4"
    And I can access a remote machine via webssh with the url
    """
    http://webssh.root-me.org/?location=WebSSH_12&
    ssh=ssh://app-script-ch4:app-script-ch4@challenge02
    """
    And This can be seen in [evidence](img1.png)

  Scenario: Fail: First Attempt
    When I attempt for first time to add my own script
    And The cron task deleted my script
    Then I have to repeat the steps to success

  Scenario: Success: Privilege Escalation
    Given I can execute commands via the webssh interface
    Then I can start an ssh connection through webssh link
    And This can be seen in [evidence](img2.png)
    When I execute "ls" command shows the application script "ch4"
    And I the previous command show cron.d symboliclink
    And Also shows the ".passwd" file
    Then I can see the source code of the script with "cat ch4"
    And This can be seen in [evidence](img3.png)
    When I analyse the script
    And I see that it script executes cron tasks
    Then I run the "rm -rf cron.d/*"
    And I deleted all the contents of the "cron.d" folder
    Then I run the "set | grep /dev/pts" command
    And I see that SSH service display information in terminal 0
    And This can be seen in [evidence](img4.png)
    Then I run the "chmod o+w /dev/pts/0"
    And So my script can write on SSH terminal
    Then I created a script to display the content of the .passwd file
    And The content o the script is the net
    """
    #!/bin/bash
    /bin/cat /challenge/app-script/ch4/.passwd > /dev/pts/0
    """
    And This can be seen in [evidence](img5.png)
    Then I change the permissions of the script file
    And I run the "chmod o+rx cron.d/script.sh" to do that
    When The script got executed it displays the ".passwd" content
    And This can be seen in [evidence](img4.png)
    And I solved the challenge
    And I caught the flag
