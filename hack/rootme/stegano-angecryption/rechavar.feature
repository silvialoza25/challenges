# Version 2.0
## language: en

Feature: Angevryption - Steganography - Root Me

  Site:
    https://www.root-me.org
  Category:
    Steganography
  User:
    rechavar
  Goal:
    Get the flag.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          |    20.04    |
    | Google-chrome   |84.0.4147.125|
    | Python          |    3.8.5    |
    | Jupyter         |    1.0.0    |
    | Notepad++       |    7.8.9    |
    | Pycrypto        |    2.6.1    |
    | openCV          |    4.4.0    |

  Machine information:
    Given The challenge URL
    """
    https://www.root-me.org/en/Challenges/Cryptanalysis/ECDSA-Introduction
    """
    And The challenge statement
    """
    One of the root-me admins has gone rogue and we suspect him of exfiltrating
    flags to a known group of cheaters. We intercepted this PDF file, but
    something doesn’t feel quite right about it.

    Can you find anything?
    """
    And 3 related resource:
    """
    watch?v=iIesDpv9F4s (www.youtube.com)
    watch?v=wbHkVZfCNuE (www.youtube.com)
    31C3 - Funky File Formats - Ange Albertini (Stéganographie)
    """
  Scenario: Fail: 3DES encryption algorithm
    Given I start the challenge
    And a pdf file tries to open
    Then I download the pdf
    When I try to open it with Adobe reader I see [evidence](image1.png)
    """
    There was an error opening this document.
    The file is damaged and could not be repaired
    """
    Then I try to open it Document Viewer
    And I see [evidence](image2.png)
    """
    The Project
    Root-Me is a non-profit organization which goal is to promote the
    spread of knowledge related to hacking and information security.
    This page is an introduction to the portal, its projects, his community
    but also to its philosophy. We encourage visitors to take an active part
    in the development of this community and its projects...
    """
    Then I open the related sources
    And I see a 1 hour Youtube video that is a talk by Ange Albertini
    When I finish it I understand what is Angecryption and how it works
    Then I open the pdf related sources
    And I see [evidence](image3.png)
    """
    This is a JPEG picture... if you decrypt it with Triple DES...
    You get a PDF document
    """
    Then I realized that I need to find the key and IV to apply 3DES
    And I can get a JPEG file after that
    When I highlight all document I see [evidence](image4.png)
    Then I zoom in to see [evidence](image5.png)
    """
    key: m8HxQ2jeUEDKUkdF
    iv: c65cf15eefb26d12bdf12f8947111677
    """
    Then I open a jupyter notebook
    And I import all the necessary libraries
    """
    from Crypto.Cipher import AES, DES3
    """
    Then I parse the iv string to bytes from hex
    """
    iv = 'C65CF15EEFB26D12BDF12F8947111677'
    iv = bytes.fromhex(iv)
    """
    And I create an object based on Pycrypto documentation
    """
    Angecryption uses a block cipher, which means that it encrypts all bytes in
    blocks, in 3DES this is reached using CBC mode:

    des3 = DES3.new(key, DES3.MODE_CBC, iv)
    """
    When I execute the cell I get
    """
    ValueError: IV must be 8 bytes long
    """
    Then I realize that iv is 16 bytes long
    And I need to find another algorithm

  Scenario: Fail: XOR bitwise operator
    Given the Pycrypto source code
    Then in AES cipher section I see [evidence](image6.png)
    """
    IV : byte string
      (*Only* `MODE_CBC`, `MODE_CFB`, `MODE_OFB`, `MODE_OPENPGP`).

      The initialization vector to use for encryption or decryption.

      It is ignored for `MODE_ECB` and `MODE_CTR`.

      For `MODE_OPENPGP`, IV must be `block_size` bytes long for encryption
      and `block_size` +2 bytes for decryption (in the latter case, it is
      actually the *encrypted* IV which was prefixed to the ciphertext).
      It is mandatory.

      For all other modes, it must be 16 bytes long.
    """
    Then I import the module
    """
    from Crypto.Cipher import AES
    """
    And I successfully create the object
    """
    aes = AES.new(key,AES.MODE_CBC, iv)
    """
    Then I search on google the beams from Ange's talk
    And I look for the Cipher Block Chaining beam [evidence](image7.png)
    Then I create the Encrypt and decrypt function based on the beam
    """
    def encrypt(msg,iv,key):
        m = [msg[i:i+16] for i in range(0,len(msg),16)]
        r = b''
        for i in m:
            aes = AES.new(key,AES.MODE_CBC, iv)
            result = aes.encrypt(i)
            print(result)
            r += result
            iv = r[-16:]
        return r

    def decrypt(msg,iv,key):
        m = [msg[i:i+16] for i in range(0,len(msg),16)]
        r = b''
        for i in m:
            aes = AES.new(key,AES.MODE_CBC, iv)
            result = aes.decrypt(i)
            print(result)
            r += result
            iv = i
        return r
    """
    When I open the pdf
    And I see the pdf lenght its not multiple of 16
    Then I add some padding to fix this
    """
    with open(file, 'rb') as file:
        img = file.read()

    img += b"\x00" * (16 - len(img) % 16) * (len(img) % 16 != 0)
    """
    When I decrypt the pdf file I see [evidence](image8.png)
    Then I realize that '\xff\xd8\xff' is the file format header for jpg
    And I save it as jpg
    """
    with open('dec_ch14.jpg', 'wb') as ofile:
        ofile.write(img)
    ofile.close()
    """
    When I open it I see [evidence](image9.png)
    Then I search in rootme forums
    And I find that I need to pass from JPG to PNG using the same payload
    When I search on the provided PDF I see [evidence](image10.png)
    """
    This a JPG picture... If you encrypt it with AES... You get a PNG
    """
    Then I start to search for iv and key
    And I create a xor function to get iv with the JPG header and PNG header
    """
    def xor(a,b):
        aux = b''
        i = 0
        for x,y in zip(a,b):
            print(i)
            my_int = x ^ y
            aux += my_int.to_bytes(1,'big')
            i += 1
        return aux
    """
    When I apply this I get
    """
    aes = AES.new(key,AES.MODE_ECB)
    iv = xor(aes.decrypt(img[:16]),targer_header)
    iv = '\xe7\x13\x18\x9e\x90\xb7\x8cO\x94\x1fv\xd6\xca\x0c\xbdJ'
    """
    Then I try to encrypt the jpg file
    And I can see [evidence](image11.png)
    When I look at the first line I see that is not a png file format header
    Then I try decrypting the jpg file
    And I can see a PNG file format header
    """
    b'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR'
    """
    Then I save the file
    When I try to open it I can see [evidence](image12.png)
    """
    Fatal error reading PNG image file: IHDR: CRC error
    """

  Scenario: Fail: Stegsolver
    Given the jpg file
    Then I open it with python opencv to check it shape
    When I see
    """
    img = cv2.imread('dec_ch14.jpg')
    img.shape
    (512,512,3)
    """
    Then I create a for loop to check the value in each RGB channel
    And if those 3 values are the same they will change to 0 to get a black
    """
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i][j][0] != 255:
                if img[i][j][0] == img[i][j][1] == img[i][j][2]:
                    print(img[i][j][0],img[i][j][1], img[i][j][2])
                    img[i][j][0] = 0
                    img[i][j][1] = 0
                    img[i][j][2] = 0
    """
    When I save and look the resulting image I can see [evidence](image13.png)
    """
    key = 'HqeOwrzpKRC2dmUY'
    iv = '0c1f7f62e43def1c4a12b146754fa777'
    """
    Then I use the key and IV to decrypt the jpg image
    And I can see [evidence](image14.PNG)
    Then I save it
    And  I open it to see [evidence](image15.png)
    When I open it with Notepad++ I see [evidence](image16.png)
    """
    tEXtComment an exerSIZE: think OUTSIDE the BOX
    """
    Then I open it with cv2
    And I see a 1 channel image
    Then I search on google for steganography tool
    When I find Stegsolve
    """
    Stegsolve is an immensely useful program for many steganography challenges,
    allowing you to go through dozens of color filters to try to
    uncover hidden text.
    """
    Then I download the .jar file to I open it [evidence](image17.png)
    When I try to inspect the image I can not see something useful

  Scenario: Success: Thinking OUTSIDE the BOX
    Given the hint inside the png file
    When I realize that if the height or width in IHDR
    And the original height  or width are differents
    Then I can hide information
    When I search on google some tool to check those sizes
    Then I find png-unhide
    """
    A simple python program which checks PNG file's size in IHDR is correct.
    Inspired from CTF problem.
    """
    Then I clone the repo
    When I run it I can read
    """
    $ python3 png-unhide/checker.py ch14.png
    [*] Height in the IHDR: 0x200
    [*] Real Height: 0x232
    [!!] Wrong size in the IHDR
    Automatically fix the IHDR? (Y/N)
    """
    Then I type 'y'
    And I read
    """
    y
    [*] Fixed file saved to ch14_fixed.png
    """
    When I open 'ch14_fixed.png' I see [evidence](image18.png)
    """
    key= 'Xt5fMcSghluqf783'
    iv = 'e9d908966ed40b4e6951b2638626eac8'
    """
    When I apply the same payload to decrypt
    Then I can see the flag at the first lines [evidence](image19.png)
    And I summit the flag to pass
