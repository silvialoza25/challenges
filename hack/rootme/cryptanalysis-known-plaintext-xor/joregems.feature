## Version 2.0
## language: en

Feature: Web server-rootme
  Site:
    www.root-me.org
  Category:
    Cryptanalysis
    decoding, BoF...
  User:
    joregems
  Goal:
    revert a image ciphered with xor and a plain text key

  Background:
    Hacker's software:
      | <Software name> | <Version>   |
      | Ubuntu 18.10    | Ubuntu 18.10 |
      | Firefox Quantum | 66.0.2 (64-bit)|
      | xor decipher         | unversioned |

    Machine information:
      Given the image ch3.bmp obtained from:
      """
      https://www.root-me.org/en/Challenges/Cryptanalysis/Known-plaintext-XOR
      Known plaintext - XOR
      """
      And using the tool for xor decryption obtained from
      """
      https://github.com/baz1/XOR
      """
      And using the key I got from analysing the image as plain text
      And decrypting the image with the help from the xor decryption tool
      Then I can see the image contents.

  Scenario: Success: find the length of the key and identify the bytes
    its value is 0. using what must be in a .bmp header
    Given the image I open it with plain text editor
    And I see the header id is corrupted by the xor encryption, probably.
    Then search for the normal header of a .bmp file
    And I learn that the value of a few characters are known
    Then I know the byte 0 and the byte 1 have as value "BM" respectively
    And I also know the value of the bytes 6,7,8,9,16,17 must be 0
    Then if is xor encrypted searching in the file for repeating strings
    Then I could know the length of the key
    Then I observe there are a lot of strings that are repeating with a patron
    And 6 character long, I assume that is the key length

  Scenario: Success: Looking for a suitable key
    Given xor with a value of 0 is the original value
    And because the bytes I know are in all the positions of the key
    And number 6 is the first
    And number 7 is the second
    And basically their positions is residue from dividing by 6
    And 6 is the key length, then, the residue from dividing by 6
    And the known character length.
    And is 0 1 2 3 4 5 the residue for the bytes 6 7 8 9 16 17 respectively
    Then that's how I know what the key is -fallen-

  Scenario: Success: Checking the key
    Given I know the first 2 characters from ch3.bmp
    When I xor them with -fa-
    And get -BM-
    And -fa- is the begin of the key
    And BM is the beginning of the .bmp header
    Then I conclude is a part of the key
    And -fallen- is a very suitable key

  Scenario: Success: decrypt the image using a tool for decrypt
  the image
    Given I know a possible key
    Then I use a tool that encrypt/decrypt with xor algorithms
    Then I make my own key long like the file
    And concatenating the key around 100000 times
    And the weight of the image is 495606 and the key is 6 bytes long, 100000
    And 100000 concatenations are enough for that file
    Then I use the tool tipping
    """
    $./xor ch3.bmp key.lst > decrypted-file.bmp
    """
    And I get the image decrypted [evidence](decrypted-file.png)
    And using that password I go to the challenge's site
    And put the password and verify I had success
