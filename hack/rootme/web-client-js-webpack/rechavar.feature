# Version 2.0
## language: en

Feature: Javascript - Webpack - Web-Client - Root Me

  Site:
    https://www.root-me.org
  Category:
    web-client
  User:
    rechavar
  Goal:
    Find the password

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          |    20.04    |
    | Google-chrome   |84.0.4147.125|

  Machine information:
    Given The challenge URL:
    """
    https://www.root-me.org/en/Challenges/Web-Client/Javascript-Webpack
    """
    And The challenge statement:
    """
    Find the password.
    """
    And 1 related resource:
    """
    Webpackjs - Devtool (Exploitation - Web)
    """
  Scenario: Fail: Inspect elements
    Given I start the challenge
    And a webpage is open
    Then I read a message:
    """
    Welcome!
    If you are here, it means that you don't know the difference
    between a duck and a mandarin duck.
    Shame on you!
    """
    When I see two buttons in the upper left corner
    Then I click on the "Duck" button
    When I see a message and an image[evidence](image1.PNG)
    And I click on the "Mandarine duck" button
    Then it displays a different image and message[evidence](image2.PNG)
    When I open Google DevTools in Elements tap
    And I can't find any flag

  Scenario: Success: Google DevTools
    Given I start the challenge
    And a webpage is open
    Then I read a message:
    """
    Welcome!
    If you are here, it means that you don't know the difference
    between a duck and a mandarin duck.
    Shame on you!
    """
    When I see two buttons in the upper left corner
    And I click on the "Duck" button
    Then I see a message and an image[evidence](image1.PNG)
    When I click on the "Mandarine duck" button
    And it displays a different imagen and message[evidence](image2.PNG)
    Then I open the related resource
    When it shows documentation about webpack
    Then I open the official repository on GitHub
    And I see
    """
    webpack is a module bundler.
    Its main purpose is to bundle JavaScript files for usage in a browser,
    yet it is also capable of transforming, bundling,
    or packaging just about any resource or asset.
    """
    When I use google inspect to see which files can I see
    And I click on the Sources tap to see which files the page loaded
    When I see 3 main folders[evidence](image3.PNG)
    Then I open webpack folder
    And I navigate through it so finally, I ended up in:
    """
    webpack:// > src
    > components > YouWillNotFindThisRouteBecauseItIsHidden.vue
    """
    Then I open the file
    And I scroll down
    When I see a wise advice
    """
    // Did you know that comments are readable by the end-user ?
    // Well, this because I build the application with the source maps enabled!
    // So please, disable source map when you build for production
    """
    Then I see the flag[evidence](image4.PNG)
    And I pass the challenge
