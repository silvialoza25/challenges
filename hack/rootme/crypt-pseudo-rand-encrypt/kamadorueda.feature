## Version 2.0
## language: en

Feature: Root Me - Crypto - Pseudo Random Based Encryption
  Site:
    Root Me
  Category:
    Crypto
  Challenge:
    Pseudo Random Based Encryption
  User:
    kedavamaru

  Background:
  Hacker's software:
    | <Software name> | <Version>                               |
    | Ubuntu          | 18.04.1 LTS (amd64)                     |
    | GNU bash        | 4.4.19(1)-release (x86_64-pc-linux-gnu) |
    | Google Chrome   | 70.0.3538.77 (64-bit)                   |

  Machine information:
    Given I'm accesing the challenge page
    And the problem statement is provided
    """
    Here is an archive containing an encrypted file,
    and the program which has been used for encryption.
    Your goal is to get back the file content.
    """
    And a Clang source code is provided
    # non important lines has been removed
    """
    --   ...
    09 unsigned int holdrand = 0;
    10
    11 static void Srand (unsigned int seed) {
    12   holdrand = seed;
    13 }
    14
    15 static int Rand (void) {
    16   return(((holdrand = holdrand * 214013L + 2531011L) >> 16) & 0x7fff);
    17 }
    18
    19 char* genere_key(void) {
    --   ...
    27   for(i = 0; i < KEY_SIZE; i++) {
    28     key[i] = charset[Rand() % (sizeof(charset) - 1)];
    29   }
    --   ...
    33 }
    34
    35 void crypt_buffer(unsigned char *buffer, size_t size, char *key) {
    --   ...
    39   j = 0;
    40   for(i = 0; i < size; i++) {
    41     if(j >= KEY_SIZE)
    42       j = 0;
    43     buffer[i] ^= key[j];
    44     j++;
    45   }
    46 }
    --   ...
    65 int main(int argc, char **argv) {
    --   ...
    69   Srand(time(NULL));
    --   ...
    93 }
    94
    """
    And an some files are provided
    """
    |__ ch16.tgz
      |__ crypt.c
      |__ oDjbNkIoLpaMo.bz2.crypt
    """

  Scenario: Fail: recoinaisance, seeding the generator
    Given I see the random generation algorithm is seed based on current time
    """
    69   Srand(time(NULL));
    """
    And Srand function set a global variable to the argument passed
    """
    09 unsigned int holdrand = 0;
    10
    11 static void Srand (unsigned int seed) {
    12   holdrand = seed;
    13 }
    """
    And the Rand function is deterministic
    """
    15 static int Rand (void) {
    16   return(((holdrand = holdrand * 214013L + 2531011L) >> 16) & 0x7fff);
    17 }
    """
    And the key is genereated based on Rand
    """
    19 char* genere_key(void) {
    --   ...
    27   for(i = 0; i < KEY_SIZE; i++) {
    28     key[i] = charset[Rand() % (sizeof(charset) - 1)];
    29   }
    --   ...
    33 }
    """
    Then I conclude that I can outsmart this encryption

  Scenario: Fail: recoinaisance, encryption logic
    Given I see the encryption logic is based on bit-bit XOR
    """
    40   for(i = 0; i < size; i++) {
    41     if(j >= KEY_SIZE)
    42       j = 0;
    43     buffer[i] ^= key[j];
    44     j++;
    45   }
    """
    Then I conclude that I can decrypt by taking the XOR of the file again
    And in short, I can use the same encryption program to decrypt
    And all I need is to seed properly the algorithm

  Scenario: Fail: recoinaisance, original file
    Given I see the name of the encrypted file
    """
    oDjbNkIoLpaMo.bz2.crypt
    """
    Then The encryption was run against against the file
    """
    oDjbNkIoLpaMo.bz2
    """
    And I conclude that this is a bzip2 file

  Scenario: Fail: getting the correct seed
    Given I know the algorithm is seed based on the current time
    When I scan the properties of the encrypted file
    """
    $ stat ./oDjbNkIoLpaMo.bz2.crypt
      ...
      Modify: 2012-12-05 07:05:36.000000000 -0500
      ...
    $ stat --format="Modify: %Y unix epoch time" ./oDjbNkIoLpaMo.bz2.crypt
      Modify: 1354709136 unix epoch time
    """
    Then I conclude the file was created at december the fifth, 2012
    And the UNIX epoch time for this date and hour is "1354709136"

  Scenario: Success: assembling the attack vector
    Given I modify the encryption code
    """
    -- 69   Srand(time(NULL));
    ++ 69   Srand(1354709136);
    """
    When I compile and run the algorithm over the encrypted file
    """
    $ gcc -o crypt crypt.c
    $ ./crypt ./oDjbNkIoLpaMo.bz2.crypt
      [+] Using key : aM5IkP4AdQzi48qtlAjCDFYn76xLD4NN
      [+] File ./oDjbNkIoLpaMo.bz2.crypt.crypt crypted !
      [+] DONE.
    """
    Then I get a bzip file
    When I extract the bzip2 file
    """
    $ bzip2 -d ./oDjbNkIoLpaMo.bz2.crypt.crypt
      bzip2: Output file ./oDjbNkIoLpaMo.bz2.crypt.crypt.out
    """
    Then I get a new file that I can read
    """
    Good job man !!!
    You can now validate the challenge with the key :
    /
    Bien joué !
    Tu peux maintenant valider le challenge avec la clef :
    xXxootHiSiStH3K3yooxXx
    """
    When I enter "xXxootHiSiStH3K3yooxXx"
    Then I solve the challenge
