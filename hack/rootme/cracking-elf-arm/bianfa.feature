## Version 2.0
## language: en

Feature:
  Site:
    www.root-me.org
  Category:
    BoF
  User:
    bianfa
  Goal:
    Get the password inside ELF executable

  Background:
  Hacker's software:
    | <Software name> | <Version>            |
    | Kali Linux      | 2020.3               |
    | Firefox         | 68.10.0esr           |
    | qemu-arm        | 5.2.0                |
    | radare2         | 5.0.0                |
    | gdb             | 10.1.90.20210103-git |
    | gdb-multiarch   | 3.18.4               |
    | pwndbg          | 1.1.0                |
  Machine information:
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Cracking/ELF-ARM-crackme-1337
    """
    When I open this URL with Firefox
    Then I can see the statement that says
    """
    If the binary file sends you 1337 you got the right password.
    """
    And I conclude that I should looking for password inside the same file
    Then I click on "Start the challenge"
    And I get a compressed file called "ch9.zip"
    When I unzip the file
    Then I get a file called "chall9.bin"

  Scenario: Fail: I try to use gdb for debug the ELF file
    Given the file "chall9.bin" when I unzip the file "ch5.zip"
    When I run "file ./chall9.bin"
    Then I can see the information of an ELF file
    And I see that the file is an ARM executable
    And this can be seen in [evidence](img1.png)
    Then I use qemu-arm to run the file with some settings for debug it in gdb
    And I run
    """
    qemu-arm-static -g 12345 -L /usr/arm-linux-gnueabi/ ./chall9.bin
     stringTest &
    """
    And I try to test the password "stringTest" on the program
    And I try to open the port "12345" for debug remotely from gdb
    Then I get an error of denial of permission
    And this can be seen in [evidence](img2.png)
    And I run "chmod +x ./chall9.bin" to add execution permissions
    And I run the executable in the same way above
    """
    qemu-arm-static -g 12345 -L /usr/arm-linux-gnueabi/ ./chall9.bin
     stringTest &
    """
    Then I open gdb running "gdb"
    And I try to connect to the port "12345" for debug the program running
    """
    target remote :12345
    """
    And I get an error as it can be seen in [evidence](img3.png)

  Scenario: Success: I try to use gdb-multiarch for debug the ELF file
    Given the file "chall9.bin" when I unzip the file "ch5.zip"
    When I run the executable
    """
    qemu-arm-static -g 12345 -L /usr/arm-linux-gnueabi/ ./chall9.bin
     stringTest &
    """
    Then I try to test the password "stringTest" on the program
    And I try to open the port "12345" for debug remotely from gdb
    And I open gdb running "gdb-multiarch"
    Then I try to connect to the port "12345" for debug the program running
    """
    target remote :12345
    """
    And I can debug the program as it can be seen in [evidence](img4.png)
    Then I try to debug the program with radare2 too to see the code
    And I run "radare2 ./chall9.bin"
    And I run "aaaa" on radare2 to analyze all code
    And I run "pdf @ main" on radare2 to see the code of main function
    And this can be seen in [evidence](img5.png)
    Then I try to find the instruction that compare my password with the real
    And I see the instruction that is probably doing that in "0x00008400"
    And this can be seen in [evidence](img6.png)
    And I run "b *0x00008400" on gdb-multiarch to create a break here
    And I try to find the instruction that load the answer "1337"
    Then I see the instruction that is probably doing that in "0x00008440"
    And this can be seen in [evidence](img7.png)
    And I run "b *0x00008440+4" on gdb-multiarch to create a break here
    Then I run "c" on gdb-multiarch to go first breakpoint
    And I can see that the program is comparing my password with the real
    And I can see that the program is comparing byte by byte within in a loop
    And this can be seen in [evidence](img8.png)
    Then I conclude that I can build the real password byte by byte
    And I see a string which seemed to follow the sequence of the real password
    And this can be seen in [evidence](img9.png)
    Then I try to run the program with that string
    """
    qemu-arm-static -g 12345 -L /usr/arm-linux-gnueabi/ ./chall9.bin
     <flag> &
    """
    And I debug the program again with gdb-multiarch
    And although the program can't print "1337" I can see that it loads in "r3"
    And this can be seen in [evidence](img10.png)
    Then I conclude that the password is correct
    And I get the flag
