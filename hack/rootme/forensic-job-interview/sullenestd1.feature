## Version 2.0
## language: en

Feature: Forensic-rootme
  Site:
    Rootme
  Category:
    Forensic
  User:
    sullenestdust1
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>      |
    | Arch Linux      | 5.7.11-arch1-1 |
    | Chrome          | 86.0.4214.2    |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge statement, it says
    """
    You are invited to an interview for a forensics investigator position at
    the NSA. For your first technical evaluation they ask you to analyze this
    file. Prove to them that you’re a fitting candidate for this job.
    """
    Then I click the link to start the challenge
    And downloads the following file
    """
    ch16.zip
    """

  Scenario: Success: Mouting the file
    Given The downloaded file
    When I extract it, I get the following file
    """
    image_forensic.e01
    """
    Then I inspect it with
    """
    exiftool
    """
    And I get the following output
    """
    ExifTool Version Number         : 12.00
    File Name                       : image_forensic.e01
    Directory                       : .
    File Size                       : 237 kB
    File Modification Date/Time     : 2016:07:02 16:19:10-05:00
    File Access Date/Time           : 2020:08:05 11:19:57-05:00
    File Inode Change Date/Time     : 2020:08:05 11:19:55-05:00
    File Permissions                : rw-r--r--
    Error                           : Unknown file type
    """
    When I check about E0X files, I found
    """
    https://www.andreafortuna.org/2018/04/11/how-to-mount-an-
    ewf-image-file-e01-on-linux/
    """
    Then I mount the image using
    """
    $ ewfmount image_forensic.e01 ./image/
    ewfmount 20140608
    """
    And I check the folder to find only one file
    """
    efw
    """
    And I check what type of file is with
    """
    $ exiftool ewf1
    ExifTool Version Number         : 12.00
    File Name                       : ewf1
    Directory                       : .
    File Size                       : 9.0 MB
    File Modification Date/Time     : 2020:08:05 11:06:18-05:00
    File Access Date/Time           : 2020:08:05 11:19:57-05:00
    File Inode Change Date/Time     : 2020:08:05 11:19:55-05:00
    File Permissions                : r--r--r--
    File Type                       : TAR
    File Type Extension             : tar
    MIME Type                       : application/x-tar
    Warning                         : Unsupported file type
    """
    Given The file is a TAR
    When I extract it with
    """
    $ tar -xvf ewf1
    """
    Then I get the file
    """
    bcache24.bmc
    """
    And I check online how to process bmc files and I found
    """
    https://github.com/ANSSI-FR/bmc-tools
    """
    When I use the programm to process the file, with the following command
    """
    $ python2.7 bmc-tools.py -s ../bcache24.bmc -d ../results/
    """
    Then I get the following output
    """
    [+++] Processing a single file: '../bcache24.bmc'.
    [===] 575 tiles successfully extracted in the end.
    [===] Successfully exported 575 files.
    """
    When I check the output folder from the previous command
    Then I found 575 "bmp" files called
    """
    bcache24.bmc_0000.bmp bcache24.bmc_0001.bmp ... bcache24.bmc_0574
    """
    When I look to the file before the last one
    """
    bcache24.bmc_0573
    """
    Then I notice an image with something written and open it
    And I see [evidence](evidence.png)
    And I found the flag
