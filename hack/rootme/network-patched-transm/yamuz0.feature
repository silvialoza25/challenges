## Version 2.0
## language: en

Feature: Network-Root Me
  Site:
    www.root-me.org
  Category:
    Network
  User:
    yamuz0
  Goal:
    To reconstitute ten lost bytes (twenty hex. values) from a frame sample.

  Background:
  Hacker's software:
    | <Software name> | <Version>                 |
    | Ubuntu          | 16.04.5 LTS (amd64)       |
    | Chromium        | 66.0.3359.139             |
    | Wireshark       | 2.2.6                     |
  Relevant information:
    Given the challenge webpage indicates with '?' the lost hexadecimal values
    And the frame with these lost bytes is called 'egress'
    Then It is conclude that this frame is a kind of response or reply
    And this reply must be as answer to at least one of the 'ingress' frames

  Scenario: Success:identifying request frame
    Given I can import the frame samples on Wireshark to know more about them
    And I find a 0x8100 value, so the communication is performed through VLANs
    And I also find ICMPv6 messages
    Then I only need to compare among the 'ingress' frames
    And find out those that match VLAN ID and ICMPv6 ID of 'egress' frame
    And that comparission is showed at [evidence](frames.png)
    Given the comparission, I find out the only 'ingress' frame that match VLAN
    And ICMPv6 ID's, this is the second one, so this is the request frame.
    Then I can reconstitute the lost bytes as shown in next table
    | <Lost byte> | <value> | <explanation>                                    |
    | 1           | f9      | Last Byte of source MAC address                  |
    | 2           | f7      | Last Byte of destination MAC address             |
    | 3 - 4       | 8100    | Communication performed through VLANs            |
    | 5           | 3a      | The same at all frames. ICMPv6 parameter         |
    | 6 - 7       | fada    | Last two destination IP address bytes            |
    | 8 - 9       | b00b    | Last two source IP address bytes                 |
    | 10          | 81      | Ping reply message                               |
    And I discover that the password is:
    """"""
    f9f781003afadab00b81
    """"""
    Then I type it as solution on the webpage challenge
    And I solve the challenge
