## Version 2.0
## language: en

Feature: SQL injection - String
  Site:
    root-me.org
  Category:
    Web-Server
  User:
    caos1986
  Goal:
    Get the administrator password from the database

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2021.1    |
    | Firefox Browser       | 68.7.0esr |

  Machine information:
    Given the challenge url
    """
    https://www.root-me.org/es/Challenges/Web-Servidor
    /SQL-injection-string
    """
    Then I can see the start button
    And This can be seen in [evidence](img1.png)
    And I can access a web page
    """
    http://challenge01.root-me.org/web-serveur/ch19/
    """
    And This can be seen in [evidence](img2.png)

  Scenario: Fail: Wrong Field
    Given I can browse the website
    Then I click the Home link
    And I can see the action URL parameter
    And This can be seen in [evidence](img3.png)
    When I try to change the page parameter
    And The site does not show any error
    And This can be seen in [evidence](img4.png)
    Then I fail the attempt

  Scenario: Success: LFI
    Given I can use the search functionality
    And I am using that field for the SQL Injection
    When I put a single quote character with an OR instruction
    Then I verify the SQL Injection in that field
    And This can be seen in [evidence](img5.png)
    And I can build the right SQL query for the user's table
    Then The page shows the user's table contents
    And This can be seen in [evidence](img6.png)
    When I use the admin password as the solution
    Then The challenge is solved
    And This can be seen in [evidence](img7.png)
    And I solved the challenge
    And I caught the flag
