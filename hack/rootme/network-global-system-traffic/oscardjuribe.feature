## Version 2.0
## language: en

Feature: global-system-traffic
  Site:
    https://www.root-me.org/en/Challenges/Network/
    Global-System-Traffic-for-Mobile-communication-61
  User:
    alestorm980 (wechall)
  Goal:
    Find the password inside the "pcap" file

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali            | 4.19.0      |
    | Wireshark       | 2.6.8       |
    | Firefox         | 60.6.2      |
  Machine information:
    Given The challenge description
    And A ".pcap" file
    Then I have to find the password inside it

  Scenario: Fail: Using Wireshark
    Given The file
    When I open it with Wireshark
    Then I see some ISI packets
    When I try to find some information
    Then I only see random values

  Scenario: Succcess: Using online decoder
    Given The file
    When I search in Google about SMS decoding
    Then I find a page to decode SMS messages
    """
    http://smstools3.kekekasvi.com/topic.php?id=288
    """
    When I analyse the "pcap" file
    Then I see a packet larger than the others
    When I take the hex data from the packet
    """
    000003340001a00020202020202000f5a00001006900006700ff9c0402030201ffff0b5a0791
    233010210068040b917120336603f800002140206165028047c7f79b0c52bfc52c101d5d0699
    d9e133283d0785e764f87b6da7956bb7f82d2c8b
    """
    Then I paste it into the converter
    And I get the flag [evidence](image1.png)
