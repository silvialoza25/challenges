## Version 2.0
## language: en

Feature: Root Me - Crypto - Shift cipher
  Site:
    Root Me
  Category:
    Crypto
  Challenge:
    Shift cipher
  User:
    kedavamaru

  Background:
  Hacker's software:
    | <Software name> | <Version>               |
    | Ubuntu          | 18.04.1 LTS (amd64)     |
    | Google Chrome   | 70.0.3538.77 (64-bit)   |
    | GNU bash        | version 4.4.19 (x86_64) |

  Machine information:
    Given I'm accesing the challenge page
    And the tittle is
    """
    Shift cipher
    """
    And the subtitle is
    """
    Recover the password
    """
    And the statement is
    """
    keep on turning.
    """
    And a binary file called "ch7.bin" is provided

  Scenario: Fail: Reconaissance 1; is it a binary?
    Given I have a file called ch7.bin
    Then I want to discover if it is an executable
    When I run the following commands
    """
    $ gdb ./ch7.bin
      GNU gdb (Ubuntu 8.1-0ubuntu3) 8.1.0.20180409-git
      Copyright (C) 2018 Free Software Foundation, Inc.
      ...
      (gdb) disass main
      No symbol table is loaded.  Use the "file" command.
    """
    Then I conclude the file is not a program

  Scenario: Fail: Reconaissance 2; dump it to hex
    Given I have a file called ch7.bin
    Then I want to see its contents
    When I run the following command
    """
    $ xxd -b ./ch7.bin'
      00000000: 4c7c 6b80 792b 2a5e 7f2a 7a6f 7f82 2a80  L|k.y+*^.*zo..*.
      00000010: 6b76 736e 6f7c 2a6b 806f 6d2a 766f 2a7a  kvsno|*k.om*vo*z
      00000020: 6b7d 7d2a 6379 766b 7372 7f14 0a         k}}*cyvksr...
    """
    Then I conclude the file is not readable

  Scenario: Success: Write a software that shift it
    Given I create a file called "kamadoatfluid.cpp"
    And this programm takes the binary file and shift some delta 'd' every char
    """
    -- ...
    11 using uchar = unsigned char;
    -- ...
    29 char nc = char((uchar(c) + d) % 0x100);
    -- ...
    """
    When I run it
    """
    $ ./kamadoatfluid
      ... non important lines
      shift: 234 [6fUjc..Hi.dYil.jU`]XYf.UjYW.`Y.dUgg.Mc`U]\i..]
      shift: 235 [7gVkd..Ij.eZjm.kVa^YZg.VkZX.aZ.eVhh.NdaV^]j..]
      shift: 236 [8hWle..Jk.f[kn.lWb_Z[h.Wl[Y.b[.fWii.OebW_^k..]
      shift: 237 [9iXmf..Kl.g\lo.mXc`[\i.Xm\Z.c\.gXjj.PfcX`_l..]
      shift: 238 [:jYng..Lm.h]mp.nYda\]j.Yn][.d].hYkk.QgdYa`m..]
      shift: 239 [;kZoh..Mn.i^nq.oZeb]^k.Zo^\.e^.iZll.RheZban..]
      shift: 240 [<l[pi..No.j_or.p[fc^_l.[p_].f_.j[mm.Sif[cbo..]
      shift: 241 [=m\qj..Op.k`ps.q\gd_`m.\q`^.g`.k\nn.Tjg\dcp..]
      shift: 242 [>n]rk..Pq.laqt.r]he`an.]ra_.ha.l]oo.Ukh]edq..]
      shift: 243 [?o^sl..Qr.mbru.s^ifabo.^sb`.ib.m^pp.Vli^fer..]
      shift: 244 [@p_tm..Rs.ncsv.t_jgbcp._tca.jc.n_qq.Wmj_gfs..]
      shift: 245 [Aq`un .St.odtw.u`khcdq.`udb.kd.o`rr.Xnk`hgt..]
      shift: 246 [Bravo! Tu peux valider avec le pass Yolaihu..]
      shift: 247 [Csbwp"!Uv!qfvy!wbmjefs!bwfd!mf!qbtt!Zpmbjiv..]
      shift: 248 [Dtcxq#"Vw"rgwz"xcnkfgt"cxge"ng"rcuu"[qnckjw..]
      shift: 249 [Eudyr$#Wx#shx{#ydolghu#dyhf#oh#sdvv#\rodlkx..]
      shift: 250 [Fvezs%$Xy$tiy|$zepmhiv$ezig$pi$teww$]spemly..]
      shift: 251 [Gwf{t&%Yz%ujz}%{fqnijw%f{jh%qj%ufxx%^tqfnmz..]
      shift: 252 [Hxg|u'&Z{&vk{~&|grojkx&g|ki&rk&vgyy&_urgon{..]
      shift: 253 [Iyh}v('[|'wl|.'}hspkly'h}lj'sl'whzz'`vshpo|..]
      shift: 254 [Jzi~w)(\}(xm}.(~itqlmz(i~mk(tm(xi{{(awtiqp}..]
      shift: 255 [K{j.x*)]~)yn~.).jurmn{)j.nl)un)yj||)bxujrq~..]
    """
    Then I see that the correct shift is 246
    """
      shift: 246 [Bravo! Tu peux valider avec le pass Yolaihu..]
    """
    When I use "Yolaihu" as solution
    Then I solve the challenge
