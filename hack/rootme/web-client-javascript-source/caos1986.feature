## Version 2.0
## language: en

Feature: Javascript - Source
  Site:
    root-me.org
  Category:
    Web - Client
  User:
    caos1986
  Goal:
    Authenticate on the site

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2021.1    |
    | Firefox Browser       | 68.7.0esr |

  Machine information:
    Given the challenge url
    """
    https://www.root-me.org/en/Challenges/Web-Client/Javascript-Source
    """
    Then I can see the start button
    And This can be seen in [evidence](img1.png)
    And I can access a web page
    """
    http://challenge01.root-me.org/web-client/ch1/
    """
    And This can be seen in [evidence](img2.png)

  Scenario: Success: Password Obtained
    Given I see the login prompt
    When I go to the source code via URL
    """
    view-source:http://challenge01.root-me.org/web-client/ch1/
    """
    Then The browser shows the page source code
    When The source code of the page loads
    Then I see a line containing the password
    And I try that username and password on the page
    And This can be seen in [evidence](img4.png)
    When I try using that password in the login
    Then The page says
    """
    You can validate the challenge using this password.
    """
    And I write the password as the challenge solution
    And The challenge is solved
    And This can be seen in [evidence](img5.png)
    And I solved the challenge
    And I caught the flag
