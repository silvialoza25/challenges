## Version 2.0
## language: en

Feature: Command & Control - level 2 - rootme
  Site:
    root-me.org
  Category:
    Forensic
  User:
    caos1986
  Goal:
    The validation flag is the workstation’s hostname.

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2021.1    |
    | Firefox Browser       | 68.7.0esr |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://www.root-me.org/es/Challenges/Forense/Command-Control-level-2
    """
    And it prompts
    """
    Felicitaciones Berthier, gracias a tu ayuda la computadora ha sido
    identificada. Ha solicitado un volcado de memoria, pero antes de
    iniciar el análisis quería echar un vistazo a los registros del
    antivirus. Desafortunadamente, olvidaste anotar el nombre del host
    de la estación de trabajo. Pero ya que tienes su volcado de memoria
    deberías ser capaz de recuperarlo!

    La bandera de validación es el nombre de la estación de trabajo.

    El volcado de memoria sin comprimir md5 hash es
    e3a902d4d44e0f7bd9cb29865e0a15de
    """
    And This can be seen in [evidence](img1.png)
    When I click start it shows a compressed file to download("ch2.tbz2")
    And This can be seen in [evidence](img2.png)
    When I extract it
    Then I get a file called "ch2.dmp"
    And This can be seen in [evidence](img3.png)
    Then I decided to use volatility

  Scenario: Fail: try-find-the-flag-wiht-volatility
    When I try to install volatility it fails
    Then I try again installing dependencies
    And The program doesn't work

  Scenario: Succes: insert-the-flag
    When I try finding the name with the commands strings and grep
    """
    strings ch2.dmp| grep -i computername
    """
    Then I can see the host name a few lines after the command
    And This can be seen in [evidence](img4.png)
    Then I entered that name in the answer box
    And This can be seen in [evidence](img5.png)
    And I solve the challenge.
