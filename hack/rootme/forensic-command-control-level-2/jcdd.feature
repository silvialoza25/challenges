## Version 2.0
## language: en

Feature: Command & Control - level 2 - rootme
  Site:
    root-me.org
  Category:
    Forensic
  User:
    jcdd
  Goal:
    The validation flag is the workstation’s hostname.

  Background:
  Hacker's software:
    | <Software name>       |    <Version>  |
    | Windows 10 Home Single|     1903      |
    | Google Chrome         | 78.0.3904.97  |
    | WinDbg                | 1.0.1910.03003|

  Scenario: Success:data-carving
    Given I start the level
    And it prompts
    """
    Congratulations Berthier, thanks to your help the computer has been
    identified. You have requested a memory dump but before starting
    your analysis you wanted to take a look at the antivirus’ logs.
    Unfortunately, you forgot to write down the workstation’s hostname.
    But since you have its memory dump you should be able to get it back!

    The validation flag is the workstation’s hostname.

    The uncompressed memory dump md5 hash is
    e3a902d4d44e0f7bd9cb29865e0a15de
    """
    And there's a compressed file to download("ch2.tbz2")
    When I extract it
    Then I get a file called "ch2.dmp"
    When I see its extension to know the correct tools
    Then I decided to use WinDbg

  Scenario: Fail:try-find-the-flag-wihtout-filter
    Then I load the archive "ch2.dmp" to WinDbg
    Then I look the interface quickly
    And Nothing

  Scenario: Fail:try-insert-the-posible-flag
    Then I searched with the main words
    And I founded two options
    Then I submit the first options
    And It was incorrect

  Scenario: Succes:insert-the-flag
    Then I submit the second options as answer
    And I solve the challenge.
