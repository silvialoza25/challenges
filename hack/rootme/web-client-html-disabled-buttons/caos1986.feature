## Version 2.0
## language: en

Feature: HTML - disabled buttons - rootme
  Site:
    root-me.org
  Category:
    Web
  User:
    caos1986
  Goal:
    Enable the buttons to find the password

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2021.1    |
    | Firefox Browser       | 68.7.0esr |

  Machine information:
    Given I am accessing the website through the URL
    """
    https://www.root-me.org/es/Challenges/Web-Cliente/HTML-disabled-buttons
    """
    And it prompts
    """
    Este formulario está desactivado y no puede ser utilizado.
    Depende de ti encontrar la manera de usarlo.
    """
    And This can be seen in [evidence](img1.png)
    When I click start it show a form
    And The buttons are disabled
    And This can be seen in [evidence](img2.png)

  Scenario: Fail: try-to-use-the-form
    When I try to click the button
    Then I figure that the button is disabled
    And The form does not work

  Scenario: Success: enabling-the-buttons
    Then I try using the "inspect element" functionality of Firefox
    And This can be seen in [evidence](img3.png)
    Then I removed the disabled parameter from the buttons
    And This can be seen in [evidence](img4.png)
    When I write admin and press the button
    Then The challenge is solved
    And This can be seen in [evidence](img5.png)
    And I can see the password to solve the challenge
    And This can be seen in [evidence](img6.png)
    And I solve the challenge.
