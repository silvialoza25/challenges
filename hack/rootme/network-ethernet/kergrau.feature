# lenguage: en

Feature: Solve ethernet-frame
  From root-me site
  Category Network
  With my username synapkg

  Background:
    Given I am running Ubuntu Xenial Xerus 16.04 (amd64)
    And I am using Mozilla Firefox Quantum 63.0 (64-bit)

  Scenario: Successful scenario
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Network/ETHERNET-frame
    """
    Then I opened that URL with Mozilla Firefox
    And I read the problem statement
    """
    Find the (supposed to be) confidential data in this ethernet frame
    """
    Given the ethernet frame in hexadecimal format
    And I passed it for a ethernet frame decoder
    """
    https://hpd.gasmi.net/
    """
    Then I saw a subfield titled Credentials which value is
    """
    confi:dential
    """
    Then I put the value as the answer and I was right.
