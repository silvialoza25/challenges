# Version 2.0
## language: en

Feature: Base - Jumper - Steganography - Root Me

  Site:
    https://www.root-me.org
  Category:
    Steganography
  User:
    rechavar
  Goal:
    Get the flag.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          |    20.04    |
    | Google-chrome   |84.0.4147.125|
    | Python          |    3.8.5    |
    | Jupyter         |    1.0.0    |
    | Notepad++       |    7.8.9    |
    | Binwalk         |    11.88    |

  Machine information:
    Given The challenge URL
    """
    https://www.root-me.org/en/Challenges/Steganography/Base-Jumper
    """
    And The challenge statement
    """
    It seems steganography is all the rage with attackers
    exfiltrating data these days.
    Look at this example I found, I think it has a flag inside.
    """
  Scenario: Fail: Binwalk
    Given I start the challenge
    And an image is open [evidence](image1.png)
    Then I download it
    When I open a terminal to start with some steganography techniques
    And I begin with searching images inside the file
    """
    $ binwalk -Me cha.jpg
    Scan Time:     2020-10-09 11:35:00
    Target File:   /home/rechavar/Desktop/Fluid_attacks/Envio/Hack
                        /base jumper/ch15.jpg
    MD5 Checksum:  89e1b135e9f4c6facf4dc6104d169437
    Signatures:    391

    DECIMAL       HEXADECIMAL     DESCRIPTION
    ---------------------------------------------------------------------------
    0             0x0             JPEG image data, JFIF standard 1.01
    30            0x1E            TIFF image data, big-endian, offset of first
                                  image directory: 8
    1018          0x3FA           JPEG image data, JFIF standard 1.01
    67930         0x1095A         Copyright string: "Copyright (c) 1998
                                  Hewlett-Packard Company"
    """
    Then I see that inside of the image there's a .tiff image and a .jpg image
    And I extract those images
    """
    binwalk -D='.*' ch15.jpg
    """
    When I open the first image I can see [evidence](image2.png)
    And I conclude that its the same given image
    Then I open the second image [evidence](image3.png)
    And I see that is a broken image
    Then I try with the last image [evidence](image4.png)
    And I don't see anything useful
    When I don't see anything useful inside the image using Notepad++
    Then I conclude that I'm on the wrong track

  Scenario: Fail: Levenshtein distance and similarity
    Given The exiftool
    Then I open a new terminal
    And I see the exif information of the given image [evidence](image4.png)
    """
    $ exiftool ch15.jpg
    """
    When I scroll down I see a base64 in the comments[evidence](image5.png)
    Then I open Notepad++ to search the base64 message
    And I copy and paste in a new file to work with it
    When I open a new jupyter notebook
    And I create a simple code to store the decrypted message
    """
    with open('DATA.lst', 'r') as data:
        array = data.readlines()

    array_decoded = [base64.b64decode(x).decode() for x in array]

    array_get = ''.join(array_decoded)

    with open('text', 'w') as file:
        file.write(''.join(array_decoded))
    file.close()
    """
    Then I open it
    And I see [evidence](image6.png)
    When I search for it in google I find that is 'RFC3548'
    Then I think that there should be a clue inside the given RFC
    And I compare the given message with the original document
    When I look for the Levenshtein distance I see [evidence](image7.png)
    """
    distance = textdistance.levenshtein.similarity(string, array_get)
    Being: string(original text), array_get(given text)
    """
    Then I conclude that are the same text

  Scenario: Success: Paddinganography
    Given The base64 text
    Then I decode it line per line
    And I encode it again
    """
    with open('DATA.lst', 'rb') as f:
        file_lines = f.readlines()
    norm_line = [base64.b64encode(base64.b64decode(x.replace(b"\n",b"")))
                  .replace(b"\n",b"") for x in file_lines]
    """
    When I see that the original base64 is different from the new
    """
    re_encoded_line = b'CgoKTmU='
    original_line = b'CgoKTmV='
    """
    When I search this on google I find this
    """
    In short, Base64 encoding is
    1. 3 chars in the original text (3x8=24bits) are transformed to 4 of 6bit
    encoded chars.
    2. long text is divided into groups of 3chars and rule 1 is applied to
    each group.
    3. if the last group contains 2 chars,
    2 of 8bit chars (2x8=16) is expressed by 3 of 6bit encoded chars (3x6=18)
    and add an '=' char
    encoded 18 bit is enough for the original 16bit with empty 2 bits.

    The 2 bits are the room for steganography!!!!!!!!!!
    """
    Then I search on google a way to get the hidden bits in the padding
    And I found an explanation of how to do this [evidence](image8.png)
    When I found en github a python script to do this [evidence](image9.png)
    Then I have problems installing gmpy library
    And I create the lcm and gcd functions to apply the found function
    """
    def gcd(a,b):
        while b > 0:
            a, b = b, a % b
        return a

    def lcm(a, b):
        return a * b / gcd(a, b)
    """
    When I decrypt it I see [evidence](image10.png)
    Then I convert it to string
    And I save each line in an array [evidence](image11.png)
    Then looking closely at the first value I realize that is base32
    """
    FJBECU2FFIQD====
    base32_alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
    """
    Then I apply the same payload but with base32 char set
    """
    base32_alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
    result2 = extract_data_from_message(base_msg2,
                          base_charset=base32_alphabet)
    """
    When I read the binary I see the flag [evidence](image12.png)
    Then I summit the flag
    And I pass the challenge
