## Version 2.0
## language: en

Feature: network-ftp-auth-www.root-me.org
  Site:
    www.root-me.org
  Category:
    network
  User:
    dgomez0
  Goal:
    Recover the password used by the user

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      |  2021.1     |
    | Mozilla Firefox |  78.7.0 esr |
    | tshark          |  3.4.3      |

  Machine information:
    Given A pcap file
    Then I have to find a password on it

  Scenario: Fail:First analysis
    Given An authenticated file exchange achieved through FTP
    And The request for this challenge is recover the ftp password
    Then I need to analyze it with some Linux commands
    Then I did it with file command to see its description
    And The result of this analysis is the following
    """
    file -i ch1.pcap
    ch1.pcap: text/plain; charset=utf-16le
    """
    And Based on the output with the i option
    Then Apparently it is a valid pcap file
    And In a plain text format

  Scenario: Success:File and packet analysis
    Given The pcap file named ch1 and the previous analysis
    Then I read it with tshark command and r option
    And This option helps us to read the content of pcap files
    """
    tshark -r ch1.pcap
    """
    Then It shows 105 lines of communication messages
    And Considering that FTP protocol sends credentials in plain text
    And Based on its functionality explained in RFC 959
    Then I passed that output to grep command to find the PASS string
    """
    grep PASS
    """
    Then In line 11 I found the target string with value cdts3500
    """
    FTP 81 Request:PASS cdts3500
    """
    And The communication was established between these hosts
    """
    10.20.144.150 → 10.20.144.151
    """
    And I could solve the challenge
