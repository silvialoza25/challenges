## Version 2.0
## language: en

Feature: 10-Web Server-www.root-me.org
  Code:
    10
  Site:
    www.root-me.org
  Category:
    Web Server
  User:
    edwinhaq
  Goal:
    Retrieve the administrator password.

Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | MS Windows      | 10          |
    | Brave           | 1.17.75     |
  Machine information:
    Given I start the challenge with following URL
    """
    http://challenge01.root-me.org/web-serveur/ch10/
    """
    And I see login page

  Scenario: Fail:Try common injection payloads
    Given I put payload inside input username
    Then Web application returns <<no such user/password>>
    And I test common characters for SQL in password input <<;"'>>
    Then Web application shows following warning message
    """
    Warning: SQLite3::query(): Unable to prepare statement: 1, ...
    """
    Then I know that Web application uses SQLite

  Scenario: Fail:Try use single username payload
    Given I send following payload in username input
    """
    admin';
    """
    Then Web application returns success message [evidence](evidence1.png)
    Then I notice that <<;>> ends the query
    And I try use <<UNION>> using following payload
    """
    admin' UNION select 1;
    """
    Then Web application detects injection [evidence](evidence2.png)

  Scenario: Success:Try to find password using substring function
    Given I know web application uses SQLite
    Then I research SQLite functions
    And I notice that <<length>> and <<substr>> functions can be useful
    Then I check password column length with following payload
    """
    admin' and length(password)=%L;
    """
    And I try values for %L = 1,2,3,4,5,6,7,8
    Then I get eight is the length of password
    Then I define following payload
    """
    admin' and substr(password,%X,1)='%Y';
    """
    And I create script to loop %X and %Y [evidence](sqlinjblind.py)
    And I get password [evidence](evidence3.png)
    And I test the password in login page
    Then I log in successfully
    And I solve challenge
