## Version 2.0
## language: en

Feature: network-twitter-auth-www.root-me.org
  Site:
    www.root-me.org
  Category:
    network
  User:
    dgomez0
  Goal:
    Retrieve the password from a Twitter account

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      |  2021.1     |
    | Mozilla Firefox |  78.7.0 esr |
    | tshark          |  3.4.3      |
    | base64          |  8.3.2      |

  Machine information:
    Given A pcap file captured from a http session
    Then Find a password on it from a Twitter account

  Scenario: Fail: First look
    Given The file downloaded from rootme
    And Twitter authentication challenge from network
    Then The statement is that based on a authentication
    And Session Captured, you have to retrieve the password
    Then I analyze it with file linux command
    """
    file -i ch3.pcap
    ch3.pcap: application/vnd.tcpdump.pcap; charset=binary
    """
    And The output indicates it is a pcap file
    And Maybe captured with some packet capture program
    And Like wireshark or tcpdump

  Scenario: Success: packet analysis and decode id session
    Given The pcap file named ch3.pcap
    Then I read it with tshark command and r option
    And It gives me the following output
    """
    HTTP 518 GET /statuses/replies.xml HTTP/1.1
    """
    And There is only one packet captured, a http get message
    Then I read the content of the pcap file with cat command
    """
    cat ch3.pcap
    """
    Then I saw what it contains
    Then Based on rfc7617 I could see that the credentials
    And Are stored in base64 code
    Then The following is the authentication information
    """
    Authorization: Basic dXNlcnRlc3Q6cGFzc3dvcmQ=
    """
    Then I used base64 command with d option to decode it
    """
    echo -n "dXNlcnRlc3Q6cGFzc3dvcmQ=" | base64 -d
    """
    And User is usertest and the password is password
    And I could solve the challenge
