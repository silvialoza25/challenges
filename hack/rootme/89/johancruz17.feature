## Version 2.0
## language: en

Feature:89-cracking-Rootme
  Code:
    89
  Site:
    www.root-me.org
  Category:
    Cracking
  User:
    johancruz17
  Goal:
    Find the password in the "Crack" file

  Background:
  Hacker's software:
  | <Software name> | <Version>  |
  | Windows         | 10 Pro     |
  | Firefox         | 80.0.1     |
  | Microsoft word  | 2016       |

   Scenario: Fail:i use Microsoft word to display and
   decode from a suspicious string in the binary
    Given a binary file i look for the required flag
    When i opened the binary file in microsoft word
    Then i did not find anything related with thirteen thirty seven

   Scenario: Success:i use Firefox 80.0.1 to display and
   decode from a suspicious string in the binary
    Given a binary file i look for the required flag
    When i load the binary file in the elf web viewer
    Then i look for line 1337 to find the hexadecimal is 40
    And i find @ passing from hexadecimal to ascii code
    And by trial and error
    And I can show it in files one point png
    And two point png
    And try what the password can be that should be after the @
    And copying and pasting the password in the form
    And I can show it in file three point png
    And I got the correct password confirmation on the challenge page
    And i conclude that the password worked correctly
    And i solved the challenge
    And i can read the flag
