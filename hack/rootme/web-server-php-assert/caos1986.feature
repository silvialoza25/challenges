## Version 2.0
## language: en

Feature: PHP - assert
  Site:
    root-me.org
  Category:
    Web-Server
  User:
    caos1986
  Goal:
    Get the password stored in the .passwd file

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2021.1    |
    | Firefox Browser       | 68.7.0esr |

  Machine information:
    Given the challenge url
    """
    https://www.root-me.org/es/Challenges/Web-Servidor/PHP-assert
    """
    Then I can see the start button
    And This can be seen in [evidence](img1.png)
    And I can access a web page
    """
    http://challenge01.root-me.org/web-serveur/ch47/
    """
    And This can be seen in [evidence](img2.png)

  Scenario: Fail: String validation
    Given I can browse the website
    When I click the Home link
    Then I can see the page URL parameter
    And This can be seen in [evidence](img3.png)
    When I try to change the page parameter
    And The site shows an error
    And This can be seen in [evidence](img4.png)
    Then I fail the attempt

  Scenario: Success: LFI
    Given I can change the page parameter
    Then I try encoding the input
    And This can be seen in [evidence](img5.png)
    And I used the encoded string as input
    Then the website shows the .passwd file content
    And This can be seen in [evidence](img6.png)
    And I solved the challenge
    And I caught the flag
