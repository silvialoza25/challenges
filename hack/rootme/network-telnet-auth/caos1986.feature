## Version 2.0
## language: en

Feature: TELNET-authentication
  Site:
    root-me.org
  Category:
    Network
  User:
    caos1986
  Goal:
    Find the user password in this TELNET session capture

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2021.1    |
    | Firefox Browser       | 68.7.0esr |
    | Wireshark             | 3.4.4     |

  Machine information:
    Given the challenge url
    """
    https://www.root-me.org/en/Challenges/Network/TELNET-authentication
    """
    Then I can see the start button
    And This can be seen in [evidence](img1.png)
    When I click on the start button
    Then I can download the capture file "ch2.pcap"
    And This can be seen in [evidence](img2.png)

  Scenario: Success: Password Obtained
    Given I downloaded the capture file
    When I open the capture file with wireshark
    Then This can be seen in [evidence](img3.png)
    When I click on a TELNET packet
    Then I press Ctrl+Alt+Shif+T
    And I can trace the communication
    And I can see the password submitted
    And This can be seen in [evidence](img4.png)
    When I write the password as the solution
    Then The challenge is solved
    And This can be seen in [evidence](img5.png)
    And I solved the challenge
    And I caught the flag
