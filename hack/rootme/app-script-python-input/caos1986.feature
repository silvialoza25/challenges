## Version 2.0
## language: en

Feature:
  Site:
    root-me.org
  User:
    caos1986
  Goal:
    Get the password stored in the .passwd file

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2021.1    |
    | Firefox Browser       | 68.7.0esr |
  Machine information:
    Given the challenge url
    """
    https://www.root-me.org/es/Challenges/App-Script
    /Python-input
    """
    Then I can see the source code from the application
    And The site gives the ssh credentials "app-script-ch6:app-script-ch6"
    And I can access a remote machine via webssh with the url
    """
    http://webssh.root-me.org/?location=WebSSH_1224&ssh=
    ssh://app-script-ch6:app-script-ch6@challenge02
    """
    And This can be seen in [evidence](img1.png)
  Scenario: Success: rce
    Given I can execute commands via the webssh interface
    Then I can start an ssh connection through webssh link
    And This can be seen in [evidence](img2.png)
    When Executing ls command shows the application executable
    Then I run the application by the ".\setuid-wraper"
    And The application asks for the user input
    When I input "sys.stdout.write(open(".passwd").readline()"
    Then The content of the .passwd file is printed
    And This can be seen in [evidence](img3.png)
    And I solved the challenge
    And I caught the flag
