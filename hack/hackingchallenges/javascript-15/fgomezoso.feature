## Version 2.0
## language: en

Feature: javascript-15 - javascript - hacking-challenges
  Site:
    hacking-challenges
  Category:
    javascript
  User:
    fgomezoso
  Goal:
    Enter the correct password

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 7            |
     | Chrome          | 78.0.3904.70 |
  Machine information:
    Given I am accessing the challenge site via browser
    """
    http://www.hacking-challenges.de/index.php?page=
    hackits&kategorie=javascript&id=15
    """
    And the site has four password fields
    And all passwords are sent at the same time

  Scenario: Success:Finding values in javascript
    Given the challenge page is displayed
    When I check the source code
    Then I find a script with a password function
    """
    function pwd(){
    var a1="The";
    var a=document.login.a.value;
    var b1= new Array("1","2","3","4","5","6","7","8","9","0") ;
    var b=document.login.b.value;
    var c1="%65%73%63%61%70%65";
    var c=document.login.c.value;
    var d0="BwHbtaSfrcCqvfkasswaHbgiemkotaw?";
    var d=document.login.d.value;
    var d1= d0.substring(2,3)+ d0.substring(5,6)+ d0.substring(9,10)+
    d0.substring(14,15)+ d0.substring(23,24)+ d0.substring(28,29);
      if (a==a1 && b==b1[9] + b1[7] + b1[9] + b1[9]  && c== unescape(c1)
       && d==d1) {
        alert ("Passwörter sind Richtig!\nBitte zur Bestätigung alle
        Passwörter ohne Leerzeichen bei Lösung eingeben!");
      }
      else {
        alert ("Es ist ein Fehler aufgetreten!\nEines der Passwörter
         scheint falsch zu sein!");
      }
    }
    """
    And I find out that there is a conditional blocking the access
    And I need to find the right values for 4 variables
    When I modify the source code with my own script "fgomezoso.js"
    Then I find the values for every password
    """
    +------------+----------------+
    | Field Name | Values         |
    +-----------------------------+
    | Passwort 1 | The            |
    | Passwort 2 | 0800           |
    | Passwort 3 | escape         |
    | Passwort 4 | Hackit         |
    +------------+----------------+
    """
    When I enter the 4 passwords
    And the form is submitted
    Then a success message is displayed [evidence](img.png)
    And I have to enter the 4 passwords without spaces in the solution
    When I enter the final password
    """
    The0800escapeHackit
    """
    Then I solve the challenge
