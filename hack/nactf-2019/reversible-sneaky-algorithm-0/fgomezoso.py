# $ pylint fgomezoso.py
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
This program returns the flag with the format "nactf{<FLAG>}" when 3 RSA
variables are known
"""
from __future__ import print_function
import binascii


def main():

    """
    The pow function with 3 parameters deciphers a number.
    This number must be decoded to ascii through functions
    """

    data = open("DATA.lst", "r")

    public_n = int(data.readline().rstrip('\n'))
    public_key = int(data.readline().rstrip('\n'))
    cipher = int(data.readline().rstrip('\n'))

    message = pow(cipher, public_key, public_n)

    m_hex = hex(message).lstrip('0x')

    b_flag = binascii.unhexlify(m_hex)

    flag = b_flag.decode('ascii')

    print(flag)

    data.close()


main()

# $ python fgomezoso.py
# nactf{<FLAG>}
