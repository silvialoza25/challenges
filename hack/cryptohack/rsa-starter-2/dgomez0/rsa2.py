#!/usr/bin/python

#Prime numbers
p = 17
q = 23

#Most common value for "e" is 0x10001 or 65537
#for RSA "public key"
e = 65537

#Number to "Encrypt"
base = 12

#Modulus for RSA key
N = p * q

result = pow(base,e,N)

print("The result is: ")
print(result)
