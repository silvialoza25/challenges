## Version 2.0
## language: en

Feature: rsa-starter-2-www.cryptohack.org
  Site:
    www.cryptohack.org
  Category:
    rsa
  User:
    dgomez0
  Goal:
    Find the result of an operation

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      |  2021.1     |
    | Mozilla Firefox |  78.7.0 esr |
    | Python          |  2.7.18     |

  Machine information:
    Given a rsa starter 2 challenge in cryptohack
    And the statement described in the next web page
    """
    https://cryptohack.org/challenges/rsa/
    """
    Then I have to solve the given operation to find the "ciphertext"

  Scenario: Fail: Wrong formula
    Given the next statement for the challenge
    """
    What number do you get as the ciphertext?
    """
    And the RSA "public key" explained in the challenge's text
    Then I found the equation needed for this problem
    """
    Base^Exponencial mod Modulus
    """
    When I was coding it in python I made a mistake in the formula
    And the error was in the position of the values
    And as shown in [evidence](rsawrong.png) the flag didn't work
    Then I had to correct my code in order to obtain the right answer
    And I corrected the order of the variables in python's pow operator
    """
    pow(base, exponent, modulus)
    """

  Scenario: Success: Making a simple python program
    Given the request shown in the cryptohack web page
    """
    "Encrypt" the number 12 using the exponent e = 65537 and
    the primes p = 17 and q = 23.
    What number do you get as the ciphertext?
    """
    Then I have to find the flag
    Then I started to code the solution considering RSA theory
    And the statement of the challenge
    Then I put the variables in the right order as follows
    """
    base=12, exponent=e, modulus=N
    where 12 is the number in the challenge request
    and e=65537 and N=p*q, being p and q prime numbers (17 and 23)
    """
    And the formula in python looks like the following
    """
    pow(base,e,N)
    """
    And after the code [evidence](rsa2.py) was executed
    Then I submitted the answer [evidence](pyresult2.png)
    And I solved the challenge
