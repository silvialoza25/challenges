## Version 2.0
## language: en

Feature: introduction-great-snakes-cryptohack
  Site:
    https://cryptohack.org/challenges/introduction/
  Category:
    Crypto
  User:
    andrewmi95
  Goal:
    Run the attached Python script and it will output your flag

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | Windows         | 10 Pro              |
  | Python         | 3.8.3     |

   Scenario: Successful: use console python 3.8 for windows
    Given a python script find the required flag
    Then downloaded the file run the python script on the power shell console
    When running the python script the console throws the required flag
    Then I can actually read the password of access
    And I conclude that the execution of the python script worked correctly
    And I solved the challenge
    And I can read the flag
