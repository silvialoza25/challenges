## Version 2.0
## language: en

Feature: crypto-on-the-web-token-appreciation
  Site:
    https://cryptohack.org/challenges/web/
  Category:
    Crypto
  User:
    miyyer1946
  Goal:
    Decode the JWT token

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Windows         |      10     |
    | Raspberry pi OS |     5.4     |
    | Python          |    3.8.3    |
    | Pip             |     18.1    |

  Scenario: Fail: flag encoded in the JWT token
    Given the jwt token on the site
    """
    eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmbGFnIjoiY3J5cHRve2p3dF9jb250ZW5
    0c19jYW5fYmVfZWFzaWx5X3ZpZXdlZH0iLCJ1c2VyIjoiQ3J5cHRvIE1jSGFjayIsImV4cCI
    6MjAwNTAzMzQ5M30.shKSmZfgGVvd2OSB2CGezzJ3N6WAULo3w9zCl_T47KQ
    """
    Then the parts of the JWT token are investigated
    And which are the header, the payload and the signature
    Given the decoding is unknown you can't get the flag

  Scenario: Success: decode the JTW token and get the flag
    Given the jwt library of python
    Then it is installed by pip
    """
    pip install pyjwt
    """
    When the library is installed it proceeds to encode a python script
    Then I run the python script [evidences](jwt-token-appreciation.py)
    Then I can actually read the flag [evidences](01.png)
    And I conclude that the execution of the python script worked correctly
    And I solved the challenge [evidences](02.png)
