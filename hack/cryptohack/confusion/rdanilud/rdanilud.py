from typing import List, Tuple
def sub_bytes(state:List[List[int]], sbox:Tuple[str, ...])->str:
    result = ""
    for rowState in state:
      for item in rowState:
        indexItem = int(hex(item)[2],16)*16+int(hex(item)[3],16)
        result += chr(int(sbox[indexItem], 16))
    return result

dataInput = open('DATA.lst', 'r').readlines()
sbox=tuple(""*256)
invSbox=tuple(""*256)
state = []
for i in range(len(dataInput)):
  temp = dataInput[i].strip('()= inv_s_boxtate[]').rstrip("],)\n").split(',')
  if i==1:
    invSbox = tuple(temp)
  elif i==0:
    sbox = tuple(temp)
  else:
    state.append([int(x) for x in temp])

print(sub_bytes(state, invSbox))
