def iroot(k:int, n:int)->int:
    u, s = n, n+1
    while u < s:
        s = u
        t = (k-1) * s + n // pow(s, k-1)
        u = t // k
    return s

dataInput=open('DATA.lst', 'r').readlines()
module = int(dataInput[0].strip('n= ').rstrip("\n"))
publicKey =int(dataInput[1].strip('e= ').rstrip("\n"))
cipheredMessage =int(dataInput[2].strip('ct= ').rstrip("\n"))
decipheredMessage= iroot(3, cipheredMessage)
decipheredMessagehex=hex(decipheredMessage)
print(bytes.fromhex(decipheredMessagehex[2:]))
