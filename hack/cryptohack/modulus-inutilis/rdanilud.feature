## Version 2.0
## language: en

Feature: modulus-inutilis-crypto-cryptohack
  Site:
    https://www.cryptohack.org/
  Category:
    crypto
  User:
    rdanilud
  Goal:
    Decipher the message and get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |
    | Python          | 3.8.6     |

  Machine information:
    Given I am accessing the challenge site
    When I read the challenge information, it mentions
    """
    My primes should be more than large enough now!
    """
    Then I download the challenge files
    """
    modulus_inutilis.py
    output.txt
    """
  Scenario: Fail: decipher the message with the traditional cube root
    Given The downloaded files
    When I inspected the modulus_inutilis.py file
    Then I found the following information
    """
    e = 3
    d = -1
    while d == -1:
    p = getPrime(1024)
    q = getPrime(1024)
    phi = (p - 1) * (q - 1)
    d = inverse(e, phi)
    n = p * q
    flag = b"XXXXXXXXXXXXXXXXXXXXXXX"
    pt = bytes_to_long(flag)
    ct = pow(pt, e, n)
    """
    When I examined the previous information
    Then I noted that the program was generating two prime numbers of 1024 bits
    And it was using them to calculate the module and the private key
    And after that it ciphered the message
    Then I revised the output.txt file
    And I saw the numbers below
    """
    n = 17258212916191948536348548470938004244269544560039009244721959293554822
    498047075403658429865201816363311805874117705688359853941515579440852166618
    074161313773416434156467811969628473425365608002907061241714688204565170146
    117869742910273064909154666642642308154422770994836108669814632309362483307
    560217924183202838588431342622551598499747369771295105890359290073146330677
    383341121242366368309126850094371525078749496850520075015636716490087482193
    603562501577348571256210991732071282478547626856068209192987351212490642903
    450263288650415552403935705444809043563866466823492258216747445926536608548
    665086042098252335883
    e = 3
    ct = 2432510536179037603099418448354112923733506559730754802640013529198651
    801512221898204733584110377593813286429573248895191923371523553028084006380
    52620580409813222660643570085177957
    """
    Then I identified that the module was the next number
    """
    n = 17258212916191948536348548470938004244269544560039009244721959293554822
    498047075403658429865201816363311805874117705688359853941515579440852166618
    074161313773416434156467811969628473425365608002907061241714688204565170146
    117869742910273064909154666642642308154422770994836108669814632309362483307
    560217924183202838588431342622551598499747369771295105890359290073146330677
    383341121242366368309126850094371525078749496850520075015636716490087482193
    603562501577348571256210991732071282478547626856068209192987351212490642903
    450263288650415552403935705444809043563866466823492258216747445926536608548
    665086042098252335883
    """
    Then the ciphered message was the number below
    """
    ct = 2432510536179037603099418448354112923733506559730754802640013529198651
    801512221898204733584110377593813286429573248895191923371523553028084006380
    52620580409813222660643570085177957
    """
    And the public key was the following number
    """
    e = 3
    """
    When I analyzed the flag length
    Then I noted that it could be a number of about two hundred bits
    Then the algorithm ciphered the message using the following formula
    """
    cipheredtext=flag^publickey mod module
    """
    When I looked the above equation
    Then I analyzed the first part of the formula
    """
    flag^publickey
    """
    Then I replace the public key value in that equation
    And thus I got the raised below
    """
    flag^3
    """
    Then I noted that the flag raised to cube was a number for about 600 bits
    Then I replaced that in the original formula
    """
    cipheredtext = number_of_600_bits mod module
    """
    When I analyzed the previous equation
    Then I noted that module was a number of two thousand and forty eight bits
    """
    cipheredtext = number_of_600_bits mod number_of_2048_bits
    """
    And the result for that was the next
    """
    cipheredtext = number_of_600_bits
    cipheredtext = flag^3
    """
    Then I raised the formula below to get the deciphered text
    """
    decipheredtext=cipheredtext^(1/3)
    """
    Then I executed the following function to get the deciphered text
    """
    decipheredtext=int(cipheredtext**(1/3))
    """
    Then I converted that number to its hexadecimal representation
    Then I converted form hexadecimal to ASCII format
    And I got the next result
    """
    cryptn\xa4\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00
    \x00\x00\x00\x00\x00\x00\x00\x00
    """
    Then I noted that something wasn't working correctly

  Scenario: Success: decipher the flag using the cube root with Newton's method
    Given that the previous function didn't work to decipher the message
    Then I decided to apply the Newton's method to get the cube root
    """
    m^(1/n)=(a^n+b)^(1/n)
    m^(1/n)=a+(b/(n*a^n-1))
    """
    Then I implemented the previous function with the next code
    """
    def iroot(k:int, n:int)->int:
    u, s = n, n+1
    while u < s:
    s = u
    t = (k-1) * s + n // pow(s, k-1)
    u = t // k
    return s
    """
    And thus I got the correct result for the following operation
    """
    decipheredtext=cipheredtext^(1/3)=iroot(3,cipheredtext)
    """
    Then I calculated the deciphered message with the above formula
    Then I converted the previous result to its hexadecimal representation
    Then I executed my python script [evidence](rdanilud.py)
    And I got the deciphered message in its ASCII format [evidence](01.png)
