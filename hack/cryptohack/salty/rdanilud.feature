## Version 2.0
## language: en

Feature: salty-crypto-cryptohack
  Site:
    https://www.cryptohack.org/
  Category:
    crypto
  User:
    rdanilud
  Goal:
    Decipher the message and get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |
    | Python          | 3.8.6     |

  Machine information:
    Given I am accessing the challenge site
    When I read the challenge information, it mentions
    """
    Smallest exponent should be fastest, right?
    """
    Then I download the challenge files
    """
    salty.py
    output.txt
    """

  Scenario: Fail: decipher the flag with the modular inverse operation
    Given The downloaded files
    When I inspected the salty.py file
    Then I found the following information
    """
    e = 1
    d = -1
    while d == -1:
    p = getPrime(512)
    q = getPrime(512)
    phi = (p - 1) * (q - 1)
    d = inverse(e, phi)
    n = p * q
    flag = b"XXXXXXXXXXXXXXXXXXXXXXX"
    pt = bytes_to_long(flag)
    ct = pow(pt, e, n)
    """
    When I examined the previous information
    Then I noted that the program was generating two prime numbers of 512 bits
    And it was using them to calculate the module and the private key
    And after that it ciphered the message
    Then I revised the output.txt file
    And I saw the numbers below
    """
    n = 11058179571595856620660039216136021257966963739143709770368515423701735
    157046476772532418205119990192031821129040477725972892361491721129156255586
    475300517932610189042766981983464200792440686248234361448876825695161608628
    704472503441280217631227308132219586604609859530626178178827657092046784017
    2004530873767
    e = 1
    ct = 4498123071821218360427478592579314544265546502526455404602825131116449
    4127485
    """
    Then I identified that the module was the next number
    """
    n = 11058179571595856620660039216136021257966963739143709770368515423701735
    157046476772532418205119990192031821129040477725972892361491721129156255586
    475300517932610189042766981983464200792440686248234361448876825695161608628
    704472503441280217631227308132219586604609859530626178178827657092046784017
    2004530873767
    """
    Then the ciphered message was the number below
    """
    ct = 4498123071821218360427478592579314544265546502526455404602825131116449
    4127485
    """
    And the public key was the following number
    """
    e = 1
    """
    When I noted that the public key was one
    Then I noted that something wasn't correct
    Then I raised the next formula
    """
    cipheredtext=text^publickey mod module
    """
    Then I replace the public key value in that equation
    And thus I got the raised below
    """
    cipheredtext=text^1 mod module
    cipheredtext=text mod module
    """
    Then I used the modular inverse multiplication in the following way
    """
    decipheredtext=inverse_modular(cipheredtext,module)
    """
    And thus I got the deciphered message
    Then I converted that number to its hexadecimal representation
    Then I converted form hexadecimal to ASCII format
    And I got the next result
    """
    \x84\x9c\xa8DD\xb2\xeb"\x1d|_\xef\x8c\xb9\x9d\xd0\xe5Y\xc1\xc2[\xbd\xb5\x0b
    \x0eZ\x95\xb7\xd9h\xd85\xf8\xd1\xf9\x9b\xd4\xae\xf0\x08$\xb3.e\x92\x10K2
    \x80\xa6\x92TC^\xd7\x0c\x1ap\x9b\xc9&\x12#\x1d\xc8\x00\xf5\x86\xf2\xaa\xc2
    \x95\x0e\xde\x15,\xefC\xa7\xcd\x8f{\xa0\x92\x9f\xb5\xa39\x99\xe8\x020\x15
    \x10\xa8\xc0\xf1\x8d\x8d9\xdf\x7f\x18\xe3\x8f\xfa<\x9e\xb6\xd9I\xa5\x9ar5v
    \xd2R\xee\xf7\xa8\x0c\x97\xd8\xc5\xda.{
    """
    Then I noted that I'd committed a mistake

  Scenario: Success: decipher the message using the module operation
    Given that the previous raising didn't work to decipher the message
    When I analyzed the private key equation
    Then I decided to raised the next idea
    """
    inverse_modular(1,n)=1
    """
    Then I took into account that as the public key was one
    Then I could calculate the private key in the next way
    """
    privatekey=inverse_modular(1,module)=1
    """
    Then I raised the following equation to decipher the message
    """
    decipheredtext=cipheredtext^privatekey mod module
    decipheredtext=cipheredtext^1 mod module
    decipheredtext=cipheredtext mod module
    """
    Then I calculated the deciphered message with the above formula
    Then I converted the previous result to its hexadecimal representation
    Then I executed my python script [evidence](rdanilud.py)
    And I got the deciphered message in its ASCII format [evidence](01.png)
