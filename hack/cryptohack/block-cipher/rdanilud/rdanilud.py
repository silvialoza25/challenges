import requests
getFlag='https://aes.cryptohack.org/block_cipher_starter/encrypt_flag/'
r = requests.get(url=getFlag)
received = r.json()['ciphertext']
flag='https://aes.cryptohack.org/block_cipher_starter/decrypt/'+received
r = requests.get(url=flag)
received = r.json()
print(bytes.fromhex(received['plaintext']))
