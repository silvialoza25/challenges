## Version 2.0
## language: en

Feature: block-cipher-crypto-cryptohack
  Site:
    https://www.cryptohack.org/
  Category:
    crypto
  User:
    rdanilud
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |
    | Python          | 3.8.6     |
    | curl            | 7.74.0    |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge information, it mentions
    """
    All block cipher modes have serious weaknesses when used incorrectly.
    The challenges in this category take you to a different section of the
    website where you can interact with APIs and exploit those weaknesses.
    Get yourself acquainted with the interface and use it to take your
    next flag!
    Play at http://aes.cryptohack.org/block_cipher_starter
    """
    Then I accessed to that web site [evidence](01.png)

  Scenario: Fail: Decipher the message generating aleatory keys
    Given I'd accessed to the challenge website
    When I analyzed the code used to cipher the flag
    Then I noted that it was using AES ECB to encrypt the message
    And for that it was using the following code
    """
    def encrypt_flag():
    cipher = AES.new(KEY, AES.MODE_ECB)
    encrypted = cipher.encrypt(FLAG.encode())
    return {"ciphertext": encrypted.hex()}
    """
    Then I did a request to get the ciphered flag with the command
    """
    curl http://aes.cryptohack.org//block_cipher_starter/encrypt_flag/
    """
    Then I received the json below
    """
    {"ciphertext":
    "2fb76b63a7e6421271099bb31997065167852ca9f72928d4f38f967dec8c8b78"}
    """
    Then I tried generating aleatory keys of 16 bytes to decipher it
    Then it didn't work
    And I knew that I had to implement a different raised

  Scenario: Success: Decipher the message doing a get request
    Given that previously I couldn't decipher the encrypted message
    Then I analyzed the code used to decipher the message
    """
    @chal.route('/block_cipher_starter/decrypt/<ciphertext>/'
    def decrypt(ciphertext):
    ciphertext = bytes.fromhex(ciphertext)
    cipher = AES.new(KEY, AES.MODE_ECB)
    try:
    decrypted = cipher.decrypt(ciphertext)
    except ValueError as e:
    return {"error": str(e)}
    return {"plaintext": decrypted.hex()}
    """
    Then I noted that it suggested sending the get request to the next route
    """
    /block_cipher_starter/decrypt/<ciphertext>/
    """
    Then I replaced the ciphered message gotten previously in that route
    """
    /block_cipher_starter/
    decrypt/2fb76b63a7e6421271099bb31997065167852ca9f72928d4f38f967dec8c8b78/
    """
    Then I did a request to the previous route using curl
    """
    curl http://aes.cryptohack.org//block_cipher_starter/decrypt
    /2fb76b63a7e6421271099bb31997065167852ca9f72928d4f38f967dec8c8b78/
    """
    And thus I got the following response
    """
    {"plaintext":
    "63727970746f7b626c30636b5f633170683372355f3472335f663435375f217d"}
    """
    Then I executed my python script [evidence](rdanilud.py)
    And thus I converted the gotten number to its ASCII representation
    And thus I deciphered the message and got the flag [evidence](02.png)
