## Version 2.0
## language: en

Feature: rsa-starter-1-www.cryptohack.org
  Site:
    www.cryptohack.org
  Category:
    rsa
  User:
    dgomez0
  Goal:
    Find the result of an operation

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      |  2021.1     |
    | Mozilla Firefox |  78.7.0 esr |
    | Python          |  2.7.18     |

  Machine information:
    Given a rsa challenge in cryptohack
    And the statement described in the web page below
    """
    https://cryptohack.org/challenges/rsa/
    """
    Then I have to solve the given operation to find the flag

  Scenario: Success:Making a simple python program
    Given the following statement
    """
    Find the solution to 101^17 mod 22663
    """
    Then I have to find the result
    When I read the information in the challenge [evidence](rsaweb.png)
    And it mentions a built in python operator
    Then I thought to use it to find the answer
    When I go the challenge web page I found the operator mentioned
    """
    pow(base, exponent, modulus)
    """
    Then replacing the values in the program I could obtain the result
    """
    x = pow(101, 17, 22663)
    print(x)
    """
    And the result can be seen in the picture [evidence](pyresult.png)
    And like it's shown in [evidence](rsasol.png)
    And I solved the challenge
