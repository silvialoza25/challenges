dataInput=open('DATA.lst', 'r').readlines()
chiperedMessage =int(dataInput[0].strip('ct=').rstrip("\n"))
n=int(dataInput[1].strip('n=').rstrip("\n"))
publicKey = 65537
eulerTotient=(n-1)*(n-1)
privateKey=pow(publicKey,-1,eulerTotient)
dechiperedMessage=pow(chiperedMessage,privateKey,n)
print(bytes.fromhex(hex(dechiperedMessage)[2:]))
