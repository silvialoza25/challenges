## Version 2.0
## language: en

Feature: monoprime-crypto-cryptohack
  Site:
    https://www.cryptohack.org/
  Category:
    crypto
  User:
    rdanilud
  Goal:
    Decipher the message and get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |
    | Python          | 3.8.6     |

  Machine information:
    Given I am accessing the challenge site
    When I read the challenge information, it mentions
    """
    Why is everyone so obsessed with multiplying two primes for RSA. Why not
    just use one?
    """
    Then I download the challenge file
    """
    output.txt
    """
  Scenario: Fail: decipher the flag with the multiplicative inverse
    Given The downloaded file
    When I analyzed its content
    Then I identificated the next information
    """
    ct=161367550346730604451454756189028938964941280347662098798775466019463375
    610700074840105776873791605070092554650190486030367121011578171525759600774
    739890458414593857709994072516290998135846956596662071379067305011746842247
    628316996977338024343628757374524136260758515864509435302781735938531030576
    289086798942
    n=1717313712180654441254825363022459154156033183802803923852918364722997527
    479346072464775085078272840757639102649953260102512684936305019898108554184
    166433526311024343179000286979932248686299356572730624725446756933659309433
    080866342919368465058612039144493380077609900517889804854625928234464696068
    24421932591
    """
    When I examined each of one numbers
    Then I concluded that the ciphered message was the next
    """
    ct=161367550346730604451454756189028938964941280347662098798775466019463375
    610700074840105776873791605070092554650190486030367121011578171525759600774
    739890458414593857709994072516290998135846956596662071379067305011746842247
    628316996977338024343628757374524136260758515864509435302781735938531030576
    289086798942
    """
    And the next number would be the module
    """
    n=1717313712180654441254825363022459154156033183802803923852918364722997527
    479346072464775085078272840757639102649953260102512684936305019898108554184
    166433526311024343179000286979932248686299356572730624725446756933659309433
    080866342919368465058612039144493380077609900517889804854625928234464696068
    24421932591
    """
    Then I analyzed the euler totient formula as it can see below
    """
    et(mn) = et(m)et(n)
    et(mn)= (m-1)(n-1)
    """
    Then I replaced the last number in the formula in the next way
    """
    et(n)= n^2-2
    """
    And thus I calculated the totient of n
    Then I used the multiplicative inverse function shown below
    """
    pow(publicKey,-1,eulerTotient)
    """
    And I got the private key
    Then I used that key to decipher the message
    Then I got the next result
    """
    \xe7)3\x05\xebT\x89\x83\xa1\xea\x04\xeb\xb3b\xcf\xf8G\xf9?\x06\x0f\xc0\x9f"
    n\xaal\x89\x85jVY\xcbX\x0c\xf34gX|\xfd=/\x17\xbdu\xfe\x8a&!u\x98\xe7p\xad
    \xc2^\x1aC\xd6\x11\xe4\xa3\xc3\xd9m\xbfs\x81k\x0e\x03\xea\xdbf\x19\t\x96
    \x8f\x8e\xbcJa\x94\x989k+\xc6!\x8d\xb9\x92\xb6\x17\x89\xee\xd0G\xad6\xb7\
    x12\x02\x16\x99?l\x83P\xb0\x14\xf1s\x95\x9a\xa4\xbf]\x1c\xaf@\xba \xf8L\x19
    \x90
    """
    Then I concluded that something was wrong

  Scenario: Success: decipher the flag with the correct euler totient
    Given the euler totient formula
    When I analyzed its structure one more time
    Then I calculated the euler totient in a different way as it can see below
    """
    et(nn)= (n-1)^2
    """
    Then I calculated the private key with the function previously shown again
    And I used it to calculate the deciphered message with the next formula
    """
    dechiperedMessage=chiperedMessage^privateKey mod(n)
    """
    Then I got the deciphered message in decimal format
    Then I converted the deciphered message from decimal to hexadecimal format
    Then I executed my python script [evidence](rdanilud.py)
    And I got the deciphered flag in ASCII format [evidence](01.png)
