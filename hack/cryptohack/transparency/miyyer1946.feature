## Version 2.0
## language: en

Feature: transparency
  Site:
    https://cryptohack.org/challenges
  Category:
    General
  User:
    miyyer1946
  Goal:
    Find the subdomain of cryptohack.org which uses these parameters in its
    TLS certificate, and visit that subdomain to obtain the flag.

  Background:
  Hacker's software:
    | <Software name> |      <Version>      |
    | Windows         |          10         |
    | VirtualBox      |         6.1         |
    | Kali Linux      |        2020.3       |
    | OpenSSL         |       1.1.1.g       |
    | Sublist3r       |         1.0         |
  Machine information:
    Given I am accessing the challenge site
    When I read the challenge information, mention and submit a .pem file
    Then it is specified that it is a public key
    And the subdomain to which it belongs must be found

  Scenario: Fail: modulus encoded in values hexadecimals
    Given the file transparency.pem
    When I try to read the certificate using OpenSSL
    """
    $ openssl x509 -in transparency_afff0345c6f99bf80eab5895458d8eab.pem
    -text -noout

    unable to load certificate
    140649542255936:error:0909006C:PEM routines:get_name:no start
    line:../crypto/pem/pem_lib.c:745:Expecting: TRUSTED CERTIFICATE
    """
    Then an error is displayed when opening the certificate
    And I try to open the certificate as plain text and review its contents
    """
    -----BEGIN PUBLIC KEY-----
    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuYj06m5q4M8SsEQwKX+5
    NPs2lyB2k7geZw4rP68eUZmqODeqxDjv5mlLY2nz/RJsPdks4J+y5t96KAyo3S5g
    mDqEOMG7JgoJ9KU+4HPQFzP9C8Gy+hisChdo9eF6UeWGTioazFDIdRUK+gZm81c1
    iPEhOBIYu3Cau32LRtv+L9vzqre0Ollf7oeHqcbcMBIKL6MpsJMG+neJPnICI36B
    ZZEMu6v6f8zIKuB7VUHAbDdQ6tsBzLpXz7XPBUeKPa1Fk8d22EI99peHwWt0RuJP
    0QsJnsa4oj6C6lE+c5+vVHa6jVsZkpl2PuXZ05a69xORZ4oq+nwzK8O/St1hbNBX
    sQIDAQAB
    -----END PUBLIC KEY-----
    """
    When I check the file it only contains the public key
    Then the certificate is incomplete and I cannot know your subdomain

  Scenario: Success: use Sublist3r to list subdomains of cryptohack.org
    Given the provenance of the public key is unknown
    Then the idea is to look for the subdomains of cryptohack.org
    And I use Sublist3r as a hack tool [evidences](01.png)
    """
    $ python3 sublist3r.py -d cryptohack.org


    / ___| _   _| |__ | (_)___| |_|___ / _ __
    \___ \| | | | '_ \| | / __| __| |_ \| '__|
    ___) | |_| | |_) | | \__ \ |_ ___) | |
    |____/ \__,_|_.__/|_|_|___/\__|____/|_|

    # Coded By Ahmed Aboul-Ela - @aboul3la

    [-] Enumerating subdomains now for cryptohack.org
    [-] Searching now in Baidu..
    [-] Searching now in Yahoo..
    [-] Searching now in Google..
    [-] Searching now in Bing..
    [-] Searching now in Ask..
    [-] Searching now in Netcraft..
    [-] Searching now in DNSdumpster..
    [-] Searching now in Virustotal..
    [-] Searching now in ThreatCrowd..
    [-] Searching now in SSL Certificates..
    [-] Searching now in PassiveDNS..
    [!] Error: Virustotal probably now is blocking our requests
    [-] Total Unique Subdomains Found: 6
    www.cryptohack.org
    aes.cryptohack.org
    blog.cryptohack.org
    nc.cryptohack.org
    xxxxxxxxxxxxxxxxxxxxx.cryptohack.org
    web.cryptohack.org
    """
    When the results are listed the subdomain is found
    Then I access the subdomain from the web browser [evidences](02.png)
    And I find the flag and solve the challenge [evidences](03.png)
