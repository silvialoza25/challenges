## Version 2.0
## language: en

Feature: bringing-it-all-crypto-cryptohack
  Site:
    https://www.cryptohack.org/
  Category:
    crypto
  User:
    rdanilud
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |
    | Python          | 3.8.6     |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge information, it mentions
    """
    complete the decrypt function that implements the steps shown in the
    diagram. The decrypted plaintext is the flag.
    """
    Then I noted that it suggested a diagram to implement it [evidence](01.png)
    Then I downloaded the challenge file
    """
    aes_decrypt.py
    """
  Scenario: Fail: Decipher the message with the suggesting diagram
    Given The diagram previously suggested
    When I analyzed that scheme
    Then I started to implement the expand_key function
    And for that I generated the following code
    """
    def expand_key(key:List[int],sBox:List[List[int]])->List[List[List[int]]]:
    rounCons = [0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40,0x80, 0x1B, 0x36]
    keyExpand = []
    keyExpand.append(transpouse(key))
    for i in range(10):
    keyTem = transpousem(keyExpand[len(keyExpand)-1])[3]
    keyTem.append(keyTem.pop(0))
    for j in range(len(keyTem)):
    tem=0
    if keyTem[j] < 16 :
    tem=sBox[0][int(hex(keyTem[j])[2],16)]
    else:
    tem=sBox[int(hex(keyTem[j])[2],16)][int(hex(keyTem[j])[3],16)]
    keyTem[j]=tem
    keyTem[0]^=rounCons[i]
    resulkey=[[],[],[],[]]
    for j in range(4):
    tem=transpousem(keyExpand[len(keyExpand)-1])[0][j]^keyTem[j]
    resulkey[0].append(tem)
    for j in range(3):
    tem=transpousem(keyExpand[len(keyExpand)-1])[j+1]
    for l in range(4):
    resulkey[j+1].append(tem[l]^resulkey[j][l])
    keyExpand.append(transpousem(resulkey))
    return keyExpand
    """
    And thus I created eleven keys to use during the decryption process
    Then I programmed the addroundkey function which work in the next way
    """
    result_state[i][j]=state[i][j]^key[i][j]
    """
    Then I generate the inv_mix_col function
    Then I had to take into account that it was necessary to do the next step
    """
    m=[[14,11,13,9],
       [9,14,11,13],
       [13,9,14,11],
       [11,13,9,14]]
    result_state=m*state
    """
    When I analyzed the previous operation
    Then I noted that the matrix multiplication had to be over Galois field
    Then I took in consideration the following rule
    """
    if m[i][j]*s[i][j]==m[i][j] or m[i][j]*s[i][j]==s[i][j]
    m[i][j]*s[i][j]==m[i][j]*s[i][j]
    if m[i][j]*s[i][j]==0
    m[i][j]*s[i][j]==m[i][j]*s[i][j]
    else
    e(l(m[i][j])+l(s[i][j]))
    """
    Then I knew that I had to use the e and l matrix
    """
    e=[[0X01,0X03,0X05,0X0F,0X11,0X33,0X55,0XFF,0X1A,0X2E,0X72,0X96,0XA1,0XF8,
    0X13,0X35],[0X5F, 0XE1, 0X38, 0X48, 0XD8, 0X73, 0X95, 0XA4, 0XF7, 0X02,
    0X06, 0X0A, 0X1E, 0X22, 0X66, 0XAA],[0XE5, 0X34, 0X5C, 0XE4, 0X37, 0X59,
    0XEB, 0X26, 0X6A, 0XBE, 0XD9, 0X70, 0X90, 0XAB, 0XE6, 0X31],[0X53, 0XF5,
    0X04, 0X0C, 0X14, 0X3C, 0X44, 0XCC, 0X4F, 0XD1, 0X68, 0XB8, 0XD3, 0X6E,
    0XB2, 0XCD],[0X4C, 0XD4, 0X67, 0XA9, 0XE0, 0X3B, 0X4D, 0XD7, 0X62, 0XA6,
    0XF1, 0X08, 0X18, 0X28, 0X78, 0X88],[0X83, 0X9E, 0XB9, 0XD0, 0X6B, 0XBD,
    0XDC, 0X7F, 0X81, 0X98, 0XB3, 0XCE, 0X49, 0XDB, 0X76, 0X9A],[0XB5, 0XC4,
    0X57, 0XF9, 0X10, 0X30, 0X50, 0XF0, 0X0B, 0X1D, 0X27, 0X69, 0XBB, 0XD6,
    0X61, 0XA3],[0XFE, 0X19, 0X2B, 0X7D, 0X87, 0X92, 0XAD, 0XEC, 0X2F, 0X71,
    0X93, 0XAE, 0XE9, 0X20, 0X60, 0XA0],[0XFB, 0X16, 0X3A, 0X4E, 0XD2, 0X6D,
    0XB7, 0XC2, 0X5D, 0XE7, 0X32, 0X56, 0XFA, 0X15, 0X3F, 0X41],[0XC3, 0X5E,
    0XE2, 0X3D, 0X47, 0XC9, 0X40, 0XC0, 0X5B, 0XED, 0X2C, 0X74, 0X9C, 0XBF,
    0XDA, 0X75],[0X9F, 0XBA, 0XD5, 0X64, 0XAC, 0XEF, 0X2A, 0X7E, 0X82, 0X9D,
    0XBC, 0XDF, 0X7A, 0X8E, 0X89, 0X80],[0X9B, 0XB6, 0XC1, 0X58, 0XE8, 0X23,
    0X65, 0XAF, 0XEA, 0X25, 0X6F, 0XB1, 0XC8, 0X43, 0XC5, 0X54],[0XFC, 0X1F,
    0X21, 0X63, 0XA5, 0XF4, 0X07, 0X09, 0X1B, 0X2D, 0X77, 0X99, 0XB0, 0XCB,
    0X46, 0XCA],[0X45, 0XCF, 0X4A, 0XDE, 0X79, 0X8B, 0X86, 0X91, 0XA8, 0XE3,
    0X3E, 0X42, 0XC6, 0X51, 0XF3, 0X0E],[0X12, 0X36, 0X5A, 0XEE, 0X29, 0X7B,
    0X8D, 0X8C, 0X8F, 0X8A, 0X85, 0X94, 0XA7, 0XF2, 0X0D, 0X17],[0X39, 0X4B,
    0XDD, 0X7C, 0X84, 0X97, 0XA2, 0XFD, 0X1C, 0X24, 0X6C, 0XB4, 0XC7, 0X52,
    0XF6, 0X01]]
    l=[[0X00,0X00,0X19,0X01,0X32,0X02,0X1A,0XC6,0X4B,0XC7,0X1B,0X68,0X33,0XEE,
    0XDF,0X03],[0X64, 0X04, 0XE0, 0X0E, 0X34, 0X8D, 0X81, 0XEF, 0X4C, 0X71,
    0X08, 0XC8, 0XF8, 0X69, 0X1C, 0XC1],[0X7D, 0XC2, 0X1D, 0XB5, 0XF9, 0XB9,
    0X27, 0X6A, 0X4D, 0XE4, 0XA6, 0X72, 0X9A, 0XC9, 0X09, 0X78],[0X65, 0X2F,
    0X8A, 0X05, 0X21, 0X0F, 0XE1, 0X24, 0X12, 0XF0, 0X82, 0X45, 0X35, 0X93,
    0XDA, 0X8E],[0X96, 0X8F, 0XDB, 0XBD, 0X36, 0XD0, 0XCE, 0X94, 0X13, 0X5C,
    0XD2, 0XF1, 0X40, 0X46, 0X83, 0X38],[0X66, 0XDD, 0XFD, 0X30, 0XBF, 0X06,
    0X8B, 0X62, 0XB3, 0X25, 0XE2, 0X98, 0X22, 0X88, 0X91, 0X10],[0X7E, 0X6E,
    0X48, 0XC3, 0XA3, 0XB6, 0X1E, 0X42, 0X3A, 0X6B, 0X28, 0X54, 0XFA, 0X85,
    0X3D, 0XBA],[0X2B, 0X79, 0X0A, 0X15, 0X9B, 0X9F, 0X5E, 0XCA, 0X4E, 0XD4,
    0XAC, 0XE5, 0XF3, 0X73, 0XA7, 0X57],[0XAF, 0X58, 0XA8, 0X50, 0XF4, 0XEA,
    0XD6, 0X74, 0X4F, 0XAE, 0XE9, 0XD5, 0XE7, 0XE6, 0XAD, 0XE8],[0X2C, 0XD7,
    0X75, 0X7A, 0XEB, 0X16, 0X0B, 0XF5, 0X59, 0XCB, 0X5F, 0XB0, 0X9C, 0XA9,
    0X51, 0XA0],[0X7F, 0X0C, 0XF6, 0X6F, 0X17, 0XC4, 0X49, 0XEC, 0XD8, 0X43,
    0X1F, 0X2D, 0XA4, 0X76, 0X7B, 0XB7],[0XCC, 0XBB, 0X3E, 0X5A, 0XFB, 0X60,
    0XB1, 0X86, 0X3B, 0X52, 0XA1, 0X6C, 0XAA, 0X55, 0X29, 0X9D],
    [0X97, 0XB2, 0X87, 0X90, 0X61, 0XBE, 0XDC, 0XFC, 0XBC, 0X95, 0XCF, 0XCD,
    0X37, 0X3F, 0X5B, 0XD1],[0X53, 0X39, 0X84, 0X3C, 0X41, 0XA2, 0X6D, 0X47,
    0X14, 0X2A, 0X9E, 0X5D, 0X56, 0XF2, 0XD3, 0XAB],[0X44, 0X11, 0X92, 0XD9,
    0X23, 0X20, 0X2E, 0X89, 0XB4, 0X7C, 0XB8, 0X26, 0X77, 0X99, 0XE3, 0XA5],
    [0X67, 0X4A, 0XED, 0XDE, 0XC5, 0X31, 0XFE, 0X18, 0X0D, 0X63, 0X8C, 0X80,
    0XC0, 0XF7, 0X70, 0X07]]
    """
    Then I executed the xor operation to complete the resulting matrix
    """
    result_state[i][j]^=m[i][k]*state[k][j]
    result_state[0][0]=m[0][0]*state[0][0]^m[0][1]*state[1][0]^
    m[0][2]*state[2][0]^m[0][3]*state[3][0]
    """
    Then I started to generated the inv_shift_rows function
    And there I did the operation below on the state matrix
    """
    result.append([matrix[0][0],matrix[0][1],matrix[0][2],matrix[0][3]])
    result.append([matrix[1][3],matrix[1][0],matrix[1][1],matrix[1][2]])
    result.append([matrix[2][2],matrix[2][3],matrix[2][0],matrix[2][1]])
    result.append([matrix[3][1],matrix[3][2],matrix[3][3],matrix[3][0]])
    """
    Then I programmed the sub_bytes function but I used the invSbox for that
    """
    invSbox = (0x52, 0x09, 0x6A,0xD5,0x30,0x36,0xA5,0x38,0xBF,0x40,0xA3,0x9E,
    0x81, 0xF3, 0xD7, 0xFB,0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87,0x34,
    0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,0x54, 0x7B, 0x94, 0x32, 0xA6,0xC2,
    0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,0x08, 0x2E,0xA1,
    0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B,0xD1,0x25,
    0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C,0xCC,0x5D,
    0x65, 0xB6, 0x92,0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E,0x15,
    0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC,0xD3,
    0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,0xD0, 0x2C, 0x1E,0x8F,
    0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A,0x6B,0x3A,
    0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE,0xF0,0xB4,
    0xE6, 0x73,0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9,0x37,
    0xE8, 0x1C, 0x75, 0xDF, 0x6E,0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5,0x89,
    0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,0xFC, 0x56, 0x3E,0x4B, 0xC6,
    0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,0x1F,0xDD,
    0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27,0x80,0xEC,
    0x5F,0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A,0x9F,
    0x93, 0xC9, 0x9C, 0xEF,0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0,0xC8,
    0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,0x17, 0x2B, 0x04, 0x7E, 0xBA,0x77,
    0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D)
    """
    Then I inspected the suggesting diagram
    And I implemented it
    Then in first place I did a addroundkey with the first key
    Then I executed nine rounds with the inv_mix_columns function
    And the addroundkey function with the keys in regressive mode
    And the inv_shift_rows function
    And the sub_bytes function
    Then for the last round I executed the addroundkey function
    And the inv_shift_rows function
    And the sub_bytes function
    Then I transformed the state matrix to its ASCII representation
    And thus I got the next decrypted message
    """
    *ñCýØA¡Ý-÷B®YÒ^
    """
    Then I noted that something was wrong

  Scenario: Success: Decipher the message using a different diagram
    Given that previously I couldn't decipher the encrypted message
    Then I decided to search another diagram
    Then I found the following diagram [evidence](02.png)
    And in the first round I executed the addroundkey function with the last key
    And the inv_shift_rows function
    And the sub_bytes function
    Then I executed nine rounds with the inv_mix_columns function
    And the addroundkey function with the keys in regressive mode
    And the inv_shift_rows function
    And the sub_bytes function
    Then I executed addroundkey with the first key
    Then I took the resulting matrix
    Then I executed my python script [evidence](rdanilud.py)
    And thus I converted each element of that matrix to its ASCII character
    And thus I deciphered the message and got the flag [evidence](03.png)
