from typing import List

def string_flag(matrix:List[List[int]])->str:
  flag=""
  for row in matrix:
    for item in row:
      flag += chr(item)
  return flag

def transpouse(vec:bytearray)->List[List[int]]:
  transpouse=[[],[],[],[]] # type: List[List[int]]
  for i in range(len(vec)):
    transpouse[i-4*(i//4)].append(vec[i])
  return transpouse

def transpousem(matrix:List[List[int]])->List[List[int]]:
  transpouse=[[],[],[],[]] # type: List[List[int]]
  for i in range(len(matrix)):
    for j in range(len(matrix[i])):
      transpouse[j].append(matrix[i][j])
  return transpouse

def addroundkey(m1:List[List[int]],m2:List[List[int]])->List[List[int]]:
  matrix=[[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
  for i in range(len(m1)):
    for j in range(len(m2)):
      matrix[i][j]=m1[i][j]^m2[i][j]
  return matrix

def sub_bytes(matrix:List[List[int]],invSbox:List[List[int]])->List[List[int]]:
  result=[[],[],[],[]] # type: List[List[int]]
  for i in range(len(matrix)):
    for item in matrix[i]:
      tem=0
      if item < 16 :
        tem=invSbox[0][int(hex(item)[2],16)]
      else:
        tem=invSbox[int(hex(item)[2],16)][int(hex(item)[3],16)]
      result[i].append(tem)
  return result

def inv_shift_rows(matrix:List[List[int]])->List[List[int]]:
  result=[]
  result.append([matrix[0][0],matrix[0][1],matrix[0][2],matrix[0][3]])
  result.append([matrix[1][3],matrix[1][0],matrix[1][1],matrix[1][2]])
  result.append([matrix[2][2],matrix[2][3],matrix[2][0],matrix[2][1]])
  result.append([matrix[3][1],matrix[3][2],matrix[3][3],matrix[3][0]])
  return result

def galois(a:int,b:int,e:List[List[int]],l:List[List[int]])->int:
  tem=[0,0,0]
  if a<16:
    tem[0]=l[0][a]
  else:
    tem[0]=l[int(hex(a)[2],16)][int(hex(a)[3],16)]
  if b<16:
    tem[1]=l[0][b]
  else:
    tem[1]=l[int(hex(b)[2],16)][int(hex(b)[3],16)]
  tem[2]=tem[0]+tem[1]
  if tem[2]>255:
    tem[2]-=255
  if tem[2]<16:
    return e[0][tem[2]]
  else:
    return e[int(hex(tem[2])[2],16)][int(hex(tem[2])[3],16)]

def ic(s:List[List[int]],e:List[List[int]],l:List[List[int]])->List[List[int]]:
  m=[[14,11,13,9],[9,14,11,13],[13,9,14,11],[11,13,9,14]]
  res=[[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
  for i in range(4):
    for j in range(4):
      for k in range(4):
        if m[i][k]*s[k][j]==s[k][j]or m[i][k]*s[k][j]==m[i][k]:
          res[i][j]^=m[i][k]*s[k][j]
        elif m[i][k]*s[k][j]==0:
          res[i][j]^=m[i][k]*s[k][j]
        else:
          res[i][j]^=galois(m[i][k],s[k][j],e,l)
  return res

def expand_key(key:bytearray,sBox:List[List[int]])->List[List[List[int]]]:
  rounCons = [0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40,0x80, 0x1B, 0x36]
  keyExpand = []
  keyExpand.append(transpouse(key))
  for i in range(10):
    keyTem = transpousem(keyExpand[len(keyExpand)-1])[3]
    keyTem.append(keyTem.pop(0))
    for j in range(len(keyTem)):
      tem=0
      if keyTem[j] < 16 :
        tem=sBox[0][int(hex(keyTem[j])[2],16)]
      else:
        tem=sBox[int(hex(keyTem[j])[2],16)][int(hex(keyTem[j])[3],16)]
      keyTem[j]=tem
    keyTem[0]^=rounCons[i]
    resulkey=[[],[],[],[]] # type: List[List[int]]
    for j in range(4):
      tem=transpousem(keyExpand[len(keyExpand)-1])[0][j]^keyTem[j]
      resulkey[0].append(tem)
    for j in range(3):
      tem1=transpousem(keyExpand[len(keyExpand)-1])[j+1]
      for l in range(4):
        resulkey[j+1].append(tem1[l]^resulkey[j][l])
    keyExpand.append(transpousem(resulkey))
  return keyExpand

dataIn = open('DATA.lst', 'r').readlines()
key = ''
ciphertext = ''
invSbox = []
for i in range(len(dataIn)):
  tem = dataIn[i].strip(" keytx=[]").rstrip("],\n")
  if i==0:
    key = tem.strip(' ')
  elif i==1:
    ciphertext = tem
  else:
    invSbox.append([int(x, 16) for x in tem.split(',')])
e=invSbox[16:32]
l=invSbox[32:48]
sBox=invSbox[48:]
invSbox=invSbox[:16]
keyExpand=expand_key(bytearray.fromhex(key),sBox)
state=transpouse(bytearray.fromhex(ciphertext))
for i in range(10, 0, -1):
  state=addroundkey(state,keyExpand[i])
  if i!=10:
    state=ic(state,e,l)
  state=inv_shift_rows(state)
  state=sub_bytes(state,invSbox)
state=addroundkey(state,keyExpand[0])
print(string_flag(transpousem(state)))
