## Version 2.0
## language: en

Feature: general-hex-cryptohack
  Site:
    https://cryptohack.org/challenges/general/
  Category:
    Crypto
  User:
    john2104
  Goal:
    Decrypt the text

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | Windows         | 10 Pro              |
  | Firefox         | 74.0 (64-bit)       |

  Machine information:
    Given the challenge URL
    """
    https://cryptohack.org/challenges/general/
    """
    When I open the url with Chrome
    Then I see a string of numbers
    """
    63727970746f7b596f755f77696c6c5f62655f776f726b696e675f776974685f6865785f737
    472696e67735f615f6c6f747d
    """
  Scenario: Fail:decrypt
    Given the array
    Then I try to decypher it using base64
    When I put the string I get:
    """
      ~}ޜ_n龽_ι_~n~_^_Ο
    """
    And I didn't solve it

   Scenario: Success:Decrypt
    Given the array
    Then I go to the webpage
    """
      https://codebeautify.org/hex-string-converter
    """
    And put the string
    Then I get the flag
    Then I solve the challenge
