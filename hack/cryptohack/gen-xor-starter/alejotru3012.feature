## Version 2.0
## language: en

Feature: xor-starter - crypto - cryptohack
  Site:
    https://cryptohack.org/challenges/
  User:
    AlejandroTL (wechall)
  Goal:
    XOR a string with the number 13

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 20.04.1     |
    | Firefox         | 84.0.2      |
  Machine information:
    Given a string "label" in the challenge page
    And a number "13"

  Scenario: Fail: XOR with hex input
    Given the string "label"
    When I visited "https://gchq.github.io/CyberChef/"
    Then I put the string in the input textbox
    And I dragged XOR from the operations list to the recipe
    And I put the number "13" as XOR's parameter
    And I let the default option "Hex" [evidence](01.png)
    Then I couldn't solve the challenge

  Scenario: Success: XOR with decimal input
    Given the string "label"
    When I visited "https://gchq.github.io/CyberChef/"
    Then I put the string in the input textbox
    And I dragged XOR from the operations list to the recipe
    And I put the number "13" as XOR's parameter
    And I selected "Decimal" indicating the input type [evidence](02.png)
    Then I got the solution from the output textbox
