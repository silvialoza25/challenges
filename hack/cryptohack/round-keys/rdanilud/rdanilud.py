from typing import List
def aroundkey(state:List[List[int]],roundKey:List[List[int]])->List[List[int]]:
    addRoundKey = []
    for i in range(len(state)):
      row = []
      for j in range(len(state[i])):
        row.append(state[i][j]^roundKey[i][j])
        if len(row) == 4:
          addRoundKey.append(row)
    return addRoundKey

def matrix2bytes(matrix : List[List[int]]) -> str:
    result =""
    for row in matrix:
      for element in row:
        result += str(chr(element))
    return result

dataInput=open('DATA.lst', 'r').readlines()
state=[]
round_key=[]
for i in range(len(dataInput)):
  tem = dataInput[i].strip("[=stateround_ky").rstrip("],\n").split(',')
  if i<4:
    state.append([int(x) for x in tem])
  else:
    round_key.append([int(x) for x in tem])

print(matrix2bytes(aroundkey(state, round_key)))
