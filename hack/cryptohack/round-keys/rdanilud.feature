## Version 2.0
## language: en

Feature: round-keys-crypto-cryptohack
  Site:
    https://www.cryptohack.org/
  Category:
    crypto
  User:
    rdanilud
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |
    | Python          | 3.8.6     |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge information, it mentions
    """
    Complete the add_round_key function, then use the matrix2bytes function
    to get your next flag.
    """
    Then I download the challenge file
    """
    add_round_key.py
    """
  Scenario: Fail: Apply shiftRows and addRoundKey functions to get the flag
    Given The downloaded file
    When I analyzed its content
    Then I found the next information
    """
    state = [[206, 243, 61, 34],
    [171, 11, 93, 31],
    [16, 200, 91, 108],
    [150, 3, 194, 51],]
    round_key = [[173, 129, 68, 82],
    [223, 100, 38, 109],
    [32, 189, 53, 8],
    [253, 48, 187, 78],]
    """
    When I examined each matrix
    Then it was clear that the state matrix was
    """
    state = [[206, 243, 61, 34],
    [171, 11, 93, 31],
    [16, 200, 91, 108],
    [150, 3, 194, 51],]
    """
    And the round key matrix was
    """
    round_key = [[173, 129, 68, 82],
    [223, 100, 38, 109],
    [32, 189, 53, 8],
    [253, 48, 187, 78],]
    """
    Then I used the shiftRows function in the state matrix
    And thus I got the next matrix
    """
    state = [[206, 243, 61, 34],
    [11, 93, 31, 171],
    [91, 108,16, 200,],
    [51 ,150, 3, 194],]
    """
    Then I analyzed the AddRoundKey function which use the next operation
    """
    addRoundKey=state[m][n]⊕round_key[m][n]
    """
    Then I operate each element of the state and round_key
    Then I applied xor operation in each of them as the above formula show
    And thus I got a new matrix with size 4 x 4
    Then I converted each element of that matrix to its ASCII character
    Then I got the next message
    """
    VB´K¤K¤¥Áñ
    """
    Then I knew that I'd had a mistake

  Scenario: Success: Apply only addRoundKey function to get the flag
    Given the addRoundKey function
    Then I decided to omit the shiftRows function on state matrix
    Then I applied the addRoundKey function one more time
    Then I used xor operation in each element of the state and round_key again
    Then I got a new matrix with size 4 x 4
    Then I executed my python script [evidence](rdanilud.py)
    And I converted each element of the resulting matrix to its ASCII character
    And thus I got the flag [evidence](01.png)
