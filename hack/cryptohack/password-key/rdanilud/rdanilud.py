from Crypto.Cipher import AES
import hashlib
import requests
getFlag='https://aes.cryptohack.org/passwords_as_keys/encrypt_flag/'
r = requests.get(url=getFlag)
received = r.json()['ciphertext']
with open("DATA.lst") as f:
  word=[hashlib.md5(w.strip().encode()).digest() for w in f.readlines()]
for row in word:
  cipher=AES.new(row,AES.MODE_ECB)
  flag=cipher.decrypt(bytes.fromhex(received))
  hint=b'crypto'
  if hint in flag:
    print(flag)
    break
