dataIn=open('DATA.lst', 'r').readlines()
module = int(dataIn[0].strip('n= ').rstrip("\n"))
privKey =int(dataIn[1].strip('d= ').rstrip("\n"))
publicKey =int(dataIn[2].strip('e= ').rstrip("\n"))
ep=[int (i.strip('ea= ').rstrip("\n"))for i in dataIn[3:8]]
ciphertext=int(dataIn[8].strip('c= ').rstrip("\n"))
ed=privKey*publicKey-1
i=1
for i in reversed(range(len(ep))):
  d=pow(ep[i],-1,ed)
  ciphertext=pow(ciphertext,d,module)
deciphertext=bytes.fromhex(hex(ciphertext)[2:])
print(deciphertext)
