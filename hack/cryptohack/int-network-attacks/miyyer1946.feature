## Version 2.0
## language: en

Feature: introduction-network-attacks-cryptohack
  Site:
    https://cryptohack.org/challenges/introduction/
  Category:
    Crypto
  User:
    andrewmi95
  Goal:
    Connect to on port. Send a JSON object with the key and value.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Python          |    3.8.3    |

   Scenario: Fail: download and run the python script
    Given a python script send to the required host the correct JSON format
    When the python script asks the api-rest for the request to buy clothes
    Then shows in [evidences](03.png)
    And the script run, the server api-rest returns error [evidences](05.png)

   Scenario: Successful: modify the script and run the powershell console
    When the api-rest request parameters that meet the challenge are modified
    And are exchanged for buy flag [evidences](04.png)
    Then you run the python script returns the flag [evidences)(01.png)
    Then I can actually read the flag
    And I conclude that the execution of the python script worked correctly
    And I solved the challenge [evidences](02.png)
