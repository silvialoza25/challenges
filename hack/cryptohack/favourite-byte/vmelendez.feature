## Version 2.0
## language: en

Feature: favourite-byte
  Site:
    Cryptohack
  Category:
    General
  User:
    Mbaku
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Python          | 3.8.5     |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its section
    When I open the challenge section
    Then I see a description with the following sentence
    """
    I've hidden my data using XOR with a single byte.
    Don't forget to decode from hex first.
    """
    And a hexadecimal string is provided
    """
    73626960647f6b206821204f21254f7d694f7624662065622127234f726927756d
    """

  Scenario: Success: Find the flag
    Given the data was encoded using a single byte with the XOR operation
    When I decode the string with Python
    """
    decoded_string = [i for i in bytes.fromhex(encoded_string)]
    """
    And I realize that it is XOR with a single byte
    Then knowing that the range of a byte is [evidences](rangebyte.png)
    """
    signed char   (1 byte) = -128 to 127
    unsigned char (1 byte) = 0 to 255
    """
    Then I could use all values of a byte and brute-force the XORed data
    """
    for i in range(128):
        for j in range(len(decoded_string)):
            print (chr(decoded_string[j] ^ i), end="")
    """
    When I executed the Python script I redirected STDOUT to a text file
    """
    $ python solver.py > text_file
    """
    Then reading this file I was able to find the flag: [evidences](flag.png)
