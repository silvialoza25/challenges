## Version 2.0
## language: en

Feature: web-jwt-sessions-www.cryptohack.org
  Site:
    www.cryptohack.org
  Category:
    web
  User:
    dgomez0
  Goal:
    Find the name of the http header

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      |  2021.1     |
    | Mozilla Firefox |  78.7.0 esr |

  Machine information:
    Given a jwt session challenge in cryptohack
    And described with the following statement
    """
    the flag is the name of the HTTP header used
    by the JWTs to the sever
    """
    Then I need to find the http header mentioned

  Scenario: Fail:First analysis
    Given the challenge request shown in the previous lines
    Then I have to find what is the name of http header
    Then my first idea was to analyze the page source
    When I used my web browser I searched for jwt in the code
    But there are only references to the same web page
    And about the challenge statement and text with jwt words
    And the following is an example line
    """
    data-challenge="jwt-sessions" class="collapsible-header"
    """
    Then I thought to intercept traffic with a web proxy
    But this challenge does not have an specific website

  Scenario: Success:jwt analysis with online sources
    Given a web page with a blank text box
    And a submit button to send the solution
    Then I decided to search for information about jwt
    Then based on the rfc 7519 I saw how jwt is sent
    And like it's shown in [evidence](rfcjwt.png)
    """
    https://tools.ietf.org/html/rfc7519
    """
    Then using the following web page
    """
    https://jwt.io/introduction/
    """
    And the specific information shown in [evidence](webjwt.png)
    Then I could realized important things about the token
    And the word Bearer is a type of schema
    And that precedes the jwt token
    When the browser sends a request
    Then the format used is the following
    """
    Authorization: Bearer <token>
    """
    And authorization is the name of the header
    And it is used to send jwt token to a server
    And I could solve the challenge
