def intsqr(intNum:int)->int:
  r = 1 << ((intNum.bit_length() + 1) >> 1)
  while True:
    newRap = (r + intNum // r) >> 1
    if newRap >= r:
      return r
    r = newRap

dataInput=open('DATA.lst', 'r').readlines()
integerN = int(dataInput[0].strip('N= ').rstrip("\n"))
publicKey =int(dataInput[1].strip('e= ').rstrip("\n"))
cipheredMessage =int(dataInput[2].strip('c= ').rstrip("\n"))
sqrtN=intsqr(integerN)
eTotient = integerN -2*sqrtN+1
privateKey = pow(publicKey,-1,eTotient)
decipheredMessage = pow(cipheredMessage, privateKey, sqrtN)
decipheredMessageHex = hex(decipheredMessage)[2:]
print(bytes.fromhex(decipheredMessageHex))
