## Version 2.0
## language: en

Feature: diffusion-crypto-cryptohack
  Site:
    https://www.cryptohack.org/
  Category:
    crypto
  User:
    rdanilud
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |
    | Python          | 3.8.6     |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge information, it mentions
    """
    We've provided code to perform MixColumns and the forward ShiftRows
    operation. After implementing inv_shift_rows, take the state, run
    inv_mix_columns on it, then inv_shift_rows, convert to bytes and
    you will have your flag.
    """
    Then I downloaded the challenge file
    """
    diffusion.py
    """
  Scenario: Fail: Find the flag doing shiftRows function without transposition
    Given The downloaded file
    When I analyzed its content
    Then I found the next information
    """
    state = [[108, 106, 71, 86],
    [96, 62, 38, 72],
    [42, 184, 92, 209],
    [94, 79, 8, 54]]
    def mix_single_column(col:List[int])->List[int]:
    colmix = col
    xtime = lambda a: (((a << 1) ^ 0x1B) & 0xFF) if (a & 0x80) else (a << 1)
    t = colmix[0] ^ colmix[1] ^ colmix[2] ^ colmix[3]
    u = colmix[0]
    colmix[0] ^= t ^ xtime(colmix[0] ^ colmix[1])
    colmix[1] ^= t ^ xtime(colmix[1] ^ colmix[2])
    colmix[2] ^= t ^ xtime(colmix[2] ^ colmix[3])
    colmix[3] ^= t ^ xtime(colmix[3] ^ u)
    return colmix
    def mix_columns(matrix:List[List[int]])->List[List[int]]:
    result = []
    for i in range(4):
    result.append(mix_single_column(matrix[i]))
    return result
    def inv_mix_columns(matrix:List[List[int]])->List[List[int]]:
    mixMatrix = matrix
    xtime = lambda a: (((a << 1) ^ 0x1B) & 0xFF) if (a & 0x80) else (a << 1)
    for i in range(4):
    u = xtime(xtime(mixMatrix[i][0] ^ mixMatrix[i][2]))
    v = xtime(xtime(mixMatrix[i][1] ^ mixMatrix[i][3]))
    mixMatrix[i][0] ^= u
    mixMatrix[i][1] ^= v
    mixMatrix[i][2] ^= u
    mixMatrix[i][3] ^= v
    return mix_columns(mixMatrix)
    """
    When I examined the above information
    Then I noted that the previous matrix was the state matrix
    And the mix_single_column function was used by mix_columns function
    And the mix_columns function was used by inv_mix_columns function
    Then I raised the following function as my inv_shift_rows function
    """
    result = []
    result.append([matrix[0][0],matrix[0][1],matrix[0][2],matrix[0][3]])
    result.append([matrix[1][3],matrix[1][0],matrix[1][1],matrix[1][2]])
    result.append([matrix[2][2],matrix[2][3],matrix[2][0],matrix[2][1]])
    result.append([matrix[3][1],matrix[3][2],matrix[3][3],matrix[3][0]])
    """
    Then I executed inv_mix_columns on the state matrix
    And I executed the raised function previously on the resulting matrix
    And thus I got the flag
    """
    cUd}fUtyoR{psr
    """
    Then I noted that something was wrong

  Scenario: Success: Find the flag doing shiftRows function with transposition
    Given the function raised previously didn't work well
    Then I decided to  modify it
    Then I raised the inv_shift_rows function in the next way
    """
    result.append([matrix[0][0],matrix[3][1],matrix[2][2],matrix[1][3]])
    result.append([matrix[1][0],matrix[0][1],matrix[3][2],matrix[2][3]])
    result.append([matrix[2][0],matrix[1][1],matrix[0][2],matrix[3][3]])
    result.append([matrix[3][0],matrix[2][1],matrix[1][2],matrix[0][3]])
    """
    And thus I executed the transposition at the same time
    Then I executed the function inv_mix_columns on state matrix
    Then I took the resulting matrix
    And I executed the inv_shift_rows function on that matrix
    Then I executed my python script [evidence](rdanilud.py)
    And thus I converted each element of that matrix to its ASCII character
    And thus I got the flag [evidence](01.png)
