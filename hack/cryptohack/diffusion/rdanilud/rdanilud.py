from typing import List, Callable

def string_flag(matrix:List[List[int]])->str:
  flag=""
  for row in matrix:
    for item in row:
      flag += chr(item)
  return flag

def inv_shift_rows(matrix:List[List[int]])->List[List[int]]:
  result = []
  result.append([matrix[0][0],matrix[3][1],matrix[2][2],matrix[1][3]])
  result.append([matrix[1][0],matrix[0][1],matrix[3][2],matrix[2][3]])
  result.append([matrix[2][0],matrix[1][1],matrix[0][2],matrix[3][3]])
  result.append([matrix[3][0],matrix[2][1],matrix[1][2],matrix[0][3]])
  return result

def xtime (a:int)->int:
  if (a & 0X80):
    return (((a << 1)^0X1B) & 0XFF)
  else :
    return a << 1

def mix_single_column(col:List[int])->List[int]:
  colmix = col
  t = colmix[0] ^ colmix[1] ^ colmix[2] ^ colmix[3]
  u = colmix[0]
  colmix[0] ^= t ^ xtime(colmix[0] ^ colmix[1])
  colmix[1] ^= t ^ xtime(colmix[1] ^ colmix[2])
  colmix[2] ^= t ^ xtime(colmix[2] ^ colmix[3])
  colmix[3] ^= t ^ xtime(colmix[3] ^ u)
  return colmix

def mix_columns(matrix:List[List[int]])->List[List[int]]:
  result = []
  for i in range(4):
    result.append(mix_single_column(matrix[i]))
  return result

def inv_mix_columns(matrix:List[List[int]])->List[List[int]]:
  mixMatrix = matrix
  for i in range(4):
    u = xtime(xtime(mixMatrix[i][0] ^ mixMatrix[i][2]))
    v = xtime(xtime(mixMatrix[i][1] ^ mixMatrix[i][3]))
    mixMatrix[i][0] ^= u
    mixMatrix[i][1] ^= v
    mixMatrix[i][2] ^= u
    mixMatrix[i][3] ^= v

  return mix_columns(mixMatrix)

dataInput = open('DATA.lst', 'r').readlines()
state = []
for row in dataInput:
  tem = row.strip(' state=[]').rstrip("],\n").split(',')
  state.append([int(x) for x in tem])
shifRows= inv_shift_rows(inv_mix_columns(state))
print(string_flag(shifRows))
