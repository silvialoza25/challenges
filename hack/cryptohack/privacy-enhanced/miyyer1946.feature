## Version 2.0
## language: en

Feature: data-formats-priveacy-enhanced-mail
  Site:
    https://cryptohack.org/challenges/web/

  Category:
    Crypto

  User:
    miyyer1946

  Goal:
    Extract the private key a decimal integer from this PEM-formatted RSA key

  Background:
  Hacker's software:
    | <Software name> |      <Version>      |
    | Windows         |          10         |
    | Microsoft-Edge  | 86.0.622.69 (64-bit)|

  Scenario: Fail: private key encoded in file .pem
    Given the file privacy_enhanced_mail.pem
    Then I get the private key RSA
    """
    MIIEowIBAAKCAQEAzvKDt+EO+A6oE1LItSunkWJ8vN6Tgcu8Ck077joGDfG2NtxD
    4vyQxGTQngr6jEKJuVz2MIwDcdXtFLIF+ISX9HfALQ3yiedNS80n/TR1BNcJSlzI
    uqLmFxddmjmfUvHFuFLvxgXRga3mg3r7olTW+1fxOS0ZVeDJqFCaORRvoAYOgLgu
    d2/E0aaaJi9cN7CjmdJ7Q3m6ryGuCwqEvZ1KgVWWa7fKcFopnl/fcsSecwbDV5hW
    fmvxiAUJy1mNSPwkf5YhGQ+83g9N588RpLLMXmgt6KimtiWnJsqtDPRlY4Bjxdpu
    V3QyUdo2ymqnquZnE/vlU/hn6/s8+ctdTqfSCwIDAQABAoIBAHw7HVNPKZtDwSYI
    djA8CpW+F7+Rpd8vHKzafHWgI25PgeEhDSfAEm+zTYDyekGk1+SMp8Ww54h4sZ/Q
    1sC/aDD7ikQBsW2TitVMTQs1aGIFbLBVTrKrg5CtGCWzHa+/L8BdGU84wvIkINMh
    CtoCMCQmQMrgBeuFy8jcyhgl6nSW2bFwxcv+NU/hmmMQK4LzjV18JRc1IIuDpUJA
    kn+JmEjBal/nDOlQ2v97+fS3G1mBAaUgSM0wwWy5lDMLEFktLJXU0OV59Sh/90qI
    Jo0DiWmMj3ua6BPzkkaJPQJmHPCNnLzsn3Is920OlvHhdzfins6GdnZ8tuHfDb0t
    cx7YSLECgYEA7ftHFeupO8TCy+cSyAgQJ8yGqNKNLHjJcg5t5vaAMeDjT/pe7w/R
    0IWuScCoADiL9+6YqUp34RgeYDkks7O7nc6XuABi8oMMjxGYPfrdVfH5zlNimS4U
    wl93bvfazutxnhz58vYvS6bQA95NQn7rWk2YFWRPzhJVkxvfK6N/x6cCgYEA3p21
    w10lYvHNNiI0KBjHvroDMyB+39vD8mSObRQQuJFJdKWuMq+o5OrkC0KtpYZ+Gw4z
    L9DQosip3hrb7b2B+bq0yP7Izj5mAVXizQTGkluT/YivvgXcxVKoNuNTqTEgmyOh
    Pn6w+PqRnESsSFzjfWrahTCrVomcZmnUTFh0rv0CgYBETN68+tKqNbFWhe4M/Mtu
    MLPhFfSwc8YU9vEx3UMzjYCPvqKqZ9bmyscXobRVw+Tf9llYFOhM8Pge06el74qE
    IvvGMk4zncrn8LvJ5grKFNWGEsZ0ghYxJucHMRlaU5ZbM6PEyEUQqEKBKbbww65W
    T3i7gvuof/iRbOljA9yzdwKBgQDT9Pc+Fu7k4XNRCon8b3OnnjYztMn4XKeZn7KY
    GtW81eBJpwJQEj5OD3OnYQoyovZozkFgUoKDq2lJJuul1ZzuaJ1/Dk+lR3YZ6Wtz
    ZwumCHnEmSMzWyOT4Rp2gEWEv1jbPbZl6XyY4wJG9n/OulqDbHy4+dj5ITb/r93J
    /yLCBQKBgHa8XYMLzH63Ieh69VZF/7jO3d3lZ4LlMEYT0BF7synfe9q6x7s0ia9b
    f6/QCkmOxPC868qhOMgSS48L+TMKmQNQSm9b9oy2ILlLA0KDsX5O/Foyiz1scwr7
    nh6tZ+tVQCRvFviIEGkaXdEiBN4eTbcjfc5md/u9eA5N21Pzgd/G
    """
    Given the base64 encoding
    Then a hex format is decoded [evidences](01.png)
    Then I perform a search that determines the format of the message is ASN.1
    Given the decoding is unknown you can't get the flag

  Scenario: Success: use HEX decoder to ASN.1 online
    Given the sequence of hexadecimal values
    Then I read the ASN.1 format encoding
    And I find the syntax of the ANS.1 private-key format
    """
    An RSA private key shall have ASN.1 type RSAPrivateKey:

    RSAPrivateKey ::= SEQUENCE {
    version Version,
    modulus INTEGER, -- n
    publicExponent INTEGER, -- e
    privateExponent INTEGER, -- d
    prime1 INTEGER, -- p
    prime2 INTEGER, -- q
    exponent1 INTEGER, -- d mod (p-1)
    exponent2 INTEGER, -- d mod (q-1)
    coefficient INTEGER -- (inverse of q) mod p }
    """
    Given the syntax above, the value privateExponent INTEGER is searched, -- d
    Then I make use of the ASN.1 JavaScript decoder online tool
    And I find the value of the private key as a value of 2047 bits in length
    """
    1568270028805633136478717104581997365499114994919795992986086122
    8180021707316851924456205543665565810892674190059831330231436970
    9144747745627149456205191443897851589089941819513488460174325064
    6416356496099378425415339540679910131476003344506519342959251234
    9952020982932218524462341002102063435489318813316464511621736943
    9384407104706949123362376802197462045951289591618005952163662375
    3829644733537581887195252002699310214832889708354718428649324119
    1505953601668858941129790966909236941127851370202421135897091086
    7635698847600991122910720569706363804173490195797687480547601048
    38790424708988260443926906673795975104689
    """
    And I find the private key in int format [evidences](02.png)
    Given the value of the private key
    Then the value is confirmed as correct
    And I solved the challenge
