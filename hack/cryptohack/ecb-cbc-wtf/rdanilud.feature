## Version 2.0
## language: en

Feature: ecb-cbc-wtf-crypto-cryptohack
  Site:
    https://www.cryptohack.org/
  Category:
    crypto
  User:
    rdanilud
  Goal:
    Decipher the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |
    | Python          | 3.8.6     |
    | curl            | 7.74.0    |

  Machine information:
    Given I accessed to the challenge site
    When reading the challenge information, it mentioned
    """
    Here you can encrypt in CBC but only decrypt in ECB. That shouldn't be a
    weakness because they're different modes... right?
    Play at http://aes.cryptohack.org/ecbcbcwtf
    """
    Then I accessed to that web site [evidence](01.png)

  Scenario: Fail: Find the flag with the first 16 bytes of the ciphered message
    Given I'd accessed to the challenge website
    When I analyzed the code used to cipher the flag
    Then I noted that it was using AES CBC to encrypt the message
    And for that it was using the following code
    """
    def encrypt_flag():
    iv = os.urandom(16)
    cipher = AES.new(KEY, AES.MODE_CBC, iv)
    encrypted = cipher.encrypt(FLAG.encode())
    ciphertext = iv.hex() + encrypted.hex()
    return {"ciphertext": ciphertext}
    """
    When I analyzed the code above
    Then I noted that the resulting number was composed by iv and ciphered text
    And I could visualized that iv vector had 16 bytes
    Then I did a GET request to get the ciphered flag with the command
    """
    curl http://aes.cryptohack.org/ecbcbcwtf/encrypt_flag/
    """
    Then I received the json below
    """
    {"ciphertext":
    "c90a4ab81383d85a179fccf4aeab206ba94b582c141cb99b565dbd4379f7073077859cdb40
    ed44b5e4ce7a5b39d5557a"}
    """
    Then I took the first 16 bytes of that number as the iv vector
    And the rest of the bytes as the encrypted message
    """
    iv=c90a4ab81383d85a179fccf4aeab206b
    ciphertext=a94b582c141cb99b565dbd4379f7073077859cdb40ed44b5e4ce7a5b39d5557a
    """
    Then I analyzed the code used to decipher the message
    """
    @chal.route('/ecbcbcwtf/decrypt/<ciphertext>/')
    def decrypt(ciphertext):
    ciphertext = bytes.fromhex(ciphertext)
    cipher = AES.new(KEY, AES.MODE_ECB)
    try:
    decrypted = cipher.decrypt(ciphertext)
    except ValueError as e:
    return {"error": str(e)}
    return {"plaintext": decrypted.hex()}
    """
    Then I noted that it suggested sending the get request to the next address
    """
    http://aes.cryptohack.org/ecbcbcwtf//ecbcbcwtf/decrypt/<ciphertext>/
    """
    Then I did a GET request to that address
    And thus I sent the encrypted message using a curl
    """
    curl http://aes.cryptohack.org/ecbcbcwtf//ecbcbcwtf/decrypt/a94b582c141cb99
    b565dbd4379f7073077859cdb40ed44b5e4ce7a5b39d5557a
    """
    Then I got the json below
    """
    {"plaintext":
    "aa7833c867eca36974fd93c1dbc84b5ef67f2e1c2578e6aa61029c6258d6264d"}
    """
    Then I noted that it had 32 bytes
    Then I converted that number to a byte array
    """
    plaintext=bytearray.fromhex('aa7833c867eca36974fd93c1dbc84b5ef67f2e1c2578e
    6aa61029c6258d6264d')
    """
    Then I did  a xor operation among that vector and iv vector
    Then I converted the resulting number to its ASCII representation
    And for that I used the following code
    """
    for i in range(len(plaintext)):
    result+=chr(plaintext[i]^vi[i%len(vi)])
    """
    Then I got the next message
    """
    crypto{3cb_5uck5?ud¤6û>ðvö}&
    """
    Then I noted that the first 16 bytes had been deciphered correctly
    And the next 16 bytes had a mistake since the last character had to be }

  Scenario: Success:Find the flag with the first 32 bytes of the encrypted text
    Given that previously I couldn't decipher the encrypted message totally
    Then I analyzed the plaintext number one more time
    """
    plaintext=aa7833c867eca36974fd93c1dbc84b5ef67f2e1c2578e6aa61029c6258d6264d
    """
    Then I took the last byte of that number
    And I compared with the expected result when xor operation was realized
    """
    }=0x7d
    0x4d⊕?=0x7d
    0x4d⊕0x7d=0x30
    """
    And thus I knew that I need 0x030 to get the expected result
    Then I analyzed again the ciphered message
    """
    ciphertext=c90a4ab81383d85a179fccf4aeab206ba94b582c141cb99b565dbd4379f70730
    77859cdb40ed44b5e4ce7a5b39d5557a
    """
    Then I realized that the byte 32 in that number was 30
    Then I took the first 32 bytes from the ciphertext as the iv vector
    """
    iv=c90a4ab81383d85a179fccf4aeab206ba94b582c141cb99b565dbd4379f70730
    """
    Then I converted that number and plaintext number to a byte array
    """
    iv=bytearray.fromhex('c90a4ab81383d85a179fccf4aeab206ba94b582c141cb99b565db
    d4379f70730')
    plaintext=bytearray.fromhex('aa7833c867eca36974fd93c1dbc84b5ef67f2e1c2578e
    6aa61029c6258d6264d')
    """
    Then I did  a xor operation among that vector and iv vector
    Then I converted the resulting number to its ASCII representation
    And I used the following code
    """
    for i in range(len(plaintext)):
    result+=chr(plaintext[i]^vi[i])
    """
    Then I executed my python script [evidence](rdanilud.py)
    And thus I deciphered the message and got the flag [evidence](02.png)
