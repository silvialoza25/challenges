import requests
getflag='https://aes.cryptohack.org/ecbcbcwtf/encrypt_flag/'
r=requests.get(url=getflag)
received = r.json()['ciphertext']
vi=bytearray.fromhex(received[:64])
flag='https://aes.cryptohack.org/ecbcbcwtf/decrypt/'+received[32:]
r = requests.get(url=flag)
received = r.json()['plaintext']
receivedArray=bytearray.fromhex(received)
result=''
for i in range(len(receivedArray)):
  result+=chr(receivedArray[i]^vi[i])
print(result)
