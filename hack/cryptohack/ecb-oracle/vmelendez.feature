## Version 2.0
## language: en

Feature: ecb_oracle
  Site:
    Cryptohack
  Category:
    Block ciphers
  User:
    Mbaku
  Goal:
    Exploit the weakness of the cipher

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Python          | 3.8.5     |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its section
    When I open the challenge section
    Then I see the following link:
    """
    Play at http://aes.cryptohack.org/ecb_oracle
    """
    When I access the link I see an interactive site for the challenge
    And a way to send data using the GET method [evidences](interact.png)
    """
    $ curl http://aes.cryptohack.org/ecb_oracle/encrypt/{data}/
    {"ciphertext":"722f80253120ab21da890037bbbcc054419106657728a2d4d368f6e"}
    """
    Then on the site I can see the algorithm of the cipher
    And interact with it [evidences](cipheralgorithm.png)

  Scenario: Sucess:Analyzing the cipher
    Given the algorithm of the cipher is provided
    When I analyze it I find some weaknesses in the implementation
    """
    from Crypto.Cipher import AES
    from Crypto.Util.Padding import pad, unpad

    KEY = ?
    FLAG = ?

    @chal.route('/ecb_oracle/encrypt/<plaintext>/')
    def encrypt(plaintext):
        plaintext = bytes.fromhex(plaintext)

        padded = pad(plaintext + FLAG.encode(), 16)
        cipher = AES.new(KEY, AES.MODE_ECB)
        try:
            encrypted = cipher.encrypt(padded)
        except ValueError as e:
            return {"error": str(e)}

        return {"ciphertext": encrypted.hex()}
    """
    And the first thing the algorithm does is take a plaintext in hexadecimal
    Then it is concatenated with the flag and padded in multiples of 16
    """
    plaintext = bytes.fromhex(plaintext)

    padded = pad(plaintext + FLAG.encode(), 16)
    """
    And use the ECB mode to encrypt the value padded with the block cipher: AES
    Then the ciphertext is returned
    """
    cipher = AES.new(KEY, AES.MODE_ECB)
        try:
            encrypted = cipher.encrypt(padded)
        except ValueError as e:
            return {"error": str(e)}

        return {"ciphertext": encrypted.hex()}
    """
    When I learn about ECB mode encryption [evidences](ecbmode.png)
    Then I realize that it is an unsafe mode [evidences](weakmode.png)
    """
    The disadvantage of this method is a lack of diffusion.
    Because ECB encrypts identical plaintext blocks into identical
    ciphertext blocks, it does not hide data patterns well.
    ECB is not recommended for use in cryptographic protocols.
    """
    And knowing that AES encrypts blocks of 128 bits independently
    Then I can take advantage of the padding to leak information from the flag

  Scenario: Success: Exploiting the weakness
    Given the cipher uses the ECB mode
    And the plaintext is padded with the flag
    Then I can perform a Chosen Plaintext Attack
    """
    A chosen-plaintext attack (CPA) is a model for cryptanalysis
    which assumes that the attacker can choose random plaintexts
    to be encrypted and obtain the corresponding ciphertexts.
    """
    And for that I can use the padding with the plaintext
    Given AES encrypts in 128-bit blocks I can leak a byte from the flag
    """
    +---------+---------+---------+
    | block 0 | block 1 | ...     |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    | | | | | | | | | | | | | | | |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    """
    And I can send a 120 bits (15 bytes) plaintext
    When AES encrypts the block
    Then it will encrypt 120 bits (plaintext) + 8 bits (flag)
    """
    +-----------------------------------------+
    |              block                      |
    +---+---+---+-----+----+------------------+
    | 0 | 1 | 2 | ... | 14 | one byte of flag |
    +---+---+---+-----+----+------------------+
    """
    And I did an implementation for this stage
    """
    payload = 'http://aes.cryptohack.org/ecb_oracle/encrypt/'
               + binascii.hexlify(b'A' * (16 + 15 - j)).decode() + '/'

    r = requests.get(payload)
    d = eval(r.text)
    block_a = blockify(d['ciphertext'])[nblock]
    r.close()
    """
    And encrypting 120 bits (padding) + all possible ASCII values (8 bits)
    When comparing it with the ciphertext if it matches
    Then I can leak a byte from the flag
    """
    for i in range(len(printable)):
        payload = 'http://aes.cryptohack.org/ecb_oracle/encrypt/'
                   + binascii.hexlify(b'A' * (16 + 15 - j) + flag +
                   printable[i].encode()).decode() + '/'

        r = requests.get(payload)
        block = blockify(eval(r.text)['ciphertext'])[nblock]
        r.close()
        if block == block_a:
            flag += printable[i].encode()
            print (flag)
    """
    And once leaked one byte I do the same for the rest of the bytes
    When I have leaked a whole block
    Then I simply padding the 16 bytes (128 bits) of the flag
    And repeat the process
    """
    +--------------------------------------------------------+
    |              block                                     |
    +---+---+---+-----+----+----------------+----------------+
    | 0 | 1 | 2 | ... | 13 | byte 1 of flag | byte 2 of flag |
    +---+---+---+-----+----+----------------+----------------+
    """
    Then this way I got the flag: [evidences](flag.png)
