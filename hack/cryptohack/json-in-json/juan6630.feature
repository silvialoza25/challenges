## Version 2.0
## language: en
Feature: JSON in JSON-Crypto on the web-www.cryptohack.org
  Site:
    www.cryptohack.org
  Category:
    Crypto on the web
  User:
    juan6630
  Goal:
    Create session as administrator.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows 10      | 20H2        |
    | Mozilla Firefox | 85.0.2      |
  Machine information:
    Given I start the challenge with the following URL
      """
      http://web.cryptohack.org/json-in-json/
      """
    And I see the source code

  Scenario: Fail: JSON web token edit
    Given The source code of the challenge
    When I create a session with the username <<juan>>
    Then I get a JSON encoded output
    And I edit the JSON token with
    """
    https://jwt.io/
    """
    When I use the new token
    Then I get an error [evidence](error.png)

  Scenario: Success: Code injection
    Given The source code for the challenge
    When I read the function used to create the session
    """
    body = '{' \
          + '"admin": "' + "False" \
          + '", "username": "' + str(username) \
          + '"}'
    """
    Then I use a payload to modify the <<admin>> value [evidence](payload.png)
    """
    aaa","admin":"True
    """
    And I get the output
    """
    {"session":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhZG1pbiI6IlRydWUiLCJ1c
    2VybmFtZSI6ImFhYSJ9.Jt5plyb9oK_xN9gl_8t687u-r5DMZbZ8DvfKjero-T8"}
    """
    When I send the output as <<authorise token>>
    Then I get the admin flag [evidence](admin.png)
    And I solve the challenge
