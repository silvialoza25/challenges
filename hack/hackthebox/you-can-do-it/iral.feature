## Version 2.0
## language: en

Feature: you_can_do_it-crypto-hackthebox
  Site:
    https://www.hackthebox.eu
  Category:
    web
  User:
     iral
  Goal:
    Find the flag on the given zip

  Background:
  Hacker's software:
    | <Software name> | <Version>           |
    | Linux           | 4.19.0-kali1-amd64  |
    | Firefox esr     | 60.8.0esr           |

  Scenario: Success: ctf
    Given I download the file called you_can_do_it.zip
    Then I proceed to decompress the file you_can_do_it.zip
    And throw a txt file called you_can_do_it
    And I find within the file the following message
    """
    YHAOANUTDSYOEOIEUTTC!
    """
    Then I go to the next web page
    """
    https://www.dcode.fr/caesar-box-cipher
    """
    When I enter the code I throw the following result
    """
    YOUSEETHATYOUCANDOIT!
    YOUSEETHATYOUCANDOIT
    YOUSEETHATYOUCANDOIT
    YNYEHUOUATETODOTASIC
    YADEUHNSOTAUYITOTOEC
    YTIHDEASUOYTAOTNECUO
    YYHOAEOOAINEUUTTDTSC
    YADEUHNSOTAUYITOTOEC
    """
    Then I realized that it was cesar code with key 7
    And I get the flag :)

  Scenario: Fail: ctf
    Given I download the file called you_can_do_it.zip
    Then I proceed to decompress the file you_can_do_it.zip
    And throw a txt file called you_can_do_it
    And I find within the file the following message
    """
    YHAOANUTDSYOEOIEUTTC!
    """
    Then I go to the next web page
    """
    https://www.dcode.fr/index-coincidence
    """
    And gave me the following result
    """
    0.05263
    """
    And I don't get the flag :(
