## Version 2.0
## language: en

Feature: Freelancer - HackTheBox
  Site:
    www.hackthebox.eu
  User:
    williamgiraldo
  Goal:
    Get to the admin userpage and retrieve the flag.

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Google Chrome     |  76.0.3809 |
    | PHP               |    7.1.1   |
  Machine Information:
    Given this is a web-based challenge,
    When I get to the challenge page
    Then I will inspect its source and resources

  Scenario: Fail:load-challenge
    Given I start the challenge
    And it prompts
    """
    Can you test how secure my website is?
    Prove me wrong and capture the flag!
    """
    When I load the challenge site,
    Then the site looks very well designed.
    And its source does not reveal nothing interesting
    But the facts the site is running PHP with Apache Web Server.

  Scenario: Fail:dir
    Given I tried GoBuster directory listing,
    And some interesting paths appear
    When I go to one of these paths.
    Then I notice I've found the admin login page (evidence)[img1.png].
    Then I try to play SQLi a little
    But the input looks like being sanitized.

  Scenario: Fail:inspect-source-better
    Given I inspect the network requests.
    And nothing interesting shows,
    When I inspect the source again,
    Then I notice some <a> tags are commented out
    And their href is a hidden page, portfolio.php?id=1
    When I get there and play a while with the id param
    Then I notice the query string is injectable.
    Then I proceed with retrieval of known columns:
    """
    id=0 UNION SELECT 1,2,3
    """
    Then I proceed with retrieval of databases:
    """
    id=0 UNION SELECT 1,TABLE_NAME,TABLE_SCHEMA FROM INFORMATION_SCHEMA.TABLES
    (This also show us the server runs a MariaDB MySQL server)
    """
    And I assume the selected DB is the one I found
    Then I proceed with retrieval of table information
    """
    id=0 UNION SELECT COLUMN_NAME,TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS
    """
    And I get the fields of the relevant tables.
    Then I go for the obvious
    """
    id=0 UNION SELECT id, username, password FROM safeadmin
    """
    And I get the admin username and hashed password (evidence)[img2.png]
    And I try to put this directly on the login page.
    But it fails.

  Scenario: Fail:decrypt
    When I realize the hash structure is the output of a PHP password_hash
    Then I devise a script to read a password list
    And use PHP password_verify to compare the raw and the hashed passwords.
    But until the moment I write this feature, the process has not finished.

  Scenario: Success:sql-injection-load-file
    Given I now know which DBMS is the server running
    And thanks to that I could get also the OS of the server.
    When I assume the DBMS and the Web Server lie on the same host,
    Then it might be possible to use LOAD_FILE inside a SELECT statement.
    And I try a lot of common paths.
    Then the query looks like
    """
    id=0 UNION SELECT 1,2,LOAD_FILE('/var/www/html/administrat/index.php')
    """
    Then I get the desired file,
    And its logic points to panel.php on successful login
    Then I get the source of said file
        """
    id=0 UNION SELECT 1,2,LOAD_FILE('/var/www/html/administrat/panel.php')
    """
    And there is the flag (evidence)[img3.png]
