## Version 2.0
## language: en

Feature: eternal-loop-misc-hackthebox
  Site:
    https://www.hackthebox.eu
  Category:
    misc
  User:
     martialwolf
  Goal:
    Find the flag on the given zip

  Background:
  Hacker's software:
    | <Software name> | <Version>           |
    | NixOS           | 19.09.2260          |
    | Chromium        | 78.0.3904.87        |

  Scenario: Success: ctf
    Given I downloaded the file called Eternal_Loop.zip
    When I proceeded to decompress it
    Then it showed another ZIP file called 37366.zip
    And I find within the file there is another zip file
    When I realized that the password for 37366 is the name of the file inside
    Then I decided to develop a tool to unzip all files
    """
    from zipfile import ZipFile
    import os

    zip_file = '37366.zip'

    try:
        while ".zip" in zip_file:
            with ZipFile(zip_file) as zf:
              before = zip_file
              zip_file = zf.infolist()[0].filename
              passwd = zip_file.split(".")[0]
              zf.extractall(pwd=bytes(passwd,'utf-8'))
              os.system("rm "+before)
    except Exception as e:
        print(e)

    """
    And I got a final ZIP file without the password
    When I executed a tool to break the password
    """
    fcrackzip -u -D -p "rockyou.txt" 6969.zip
    """
    Then I got the ZIP password
    And unziped the file
    Then I got a SQLite 3.x database file
    When I executed the following
    """
    strings DoNotTouch | grep 'HTB'
    """
    Then I got the flag
