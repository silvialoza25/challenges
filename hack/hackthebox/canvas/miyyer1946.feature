## Version 2.0
## language: en

Feature: canvas
  Site:
    https://www.hackthebox.eu/
  Category:
    Misc
  User:
    miyyer1946
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> |      <Version>      |
    | Windows         |          10         |
    | Microsoft-Edge  | 86.0.622.69 (64-bit)|

  Scenario: Fail: flag encoded in the challenge files
    Given the challenge has necessary files to download
    Then I find files from a web page .js, .html and .css
    Then I validate the contents of the index.html
    """
    <html>
    <head>
    <title>canvas</title>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="js/login.js"></script>
    </head>
    <body>
    <div class="container">
    <div class="main">
    <h2>Canvas login</h2>
    <form id="form_id" method="post" name="myform">
    <label>User Name :</label>
    <input type="text" name="username" id="username"/>
    <label>Password :</label>
    <input type="password" name="password" id="password"/>
    <input type="button" value="Login" id="submit" onclick="validate()"/>
    </form>
    </div>
    </div>
    </body>
    </html>
    """
    Given the html code the use of the file is evident login.js
    Then I analyze the content and you see a javascript file encoded in hex
    And I use Javascript Decoder online [evidences](01.png)
    """
    https://malwaredecoder.com/
    """
    Then I don't observe any value to get the flag

  Scenario: Success: use HEX decoder to string
    Given the format of the flag to solve the challenge [evidences](02.png)
    Then convert the first characters to HEX format [evidences](03.png)
    """
    http://string-functions.com/string-hex.aspx
    """
    And I add the prefixes and do the search in the file .js[evidences](04.png)
    Then I find the sequence HEX that contains the flag
    Given the format I use HEX to String converter
    """
    http://string-functions.com/hex-string.aspx
    """
    And I find the flag [evidences](05.png)
