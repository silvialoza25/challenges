## Version 2.0
## language: en

Feature: two-for-one-crypto-hackthebox
  Site:
    https://www.hackthebox.eu/
  Category:
    crypto
  User:
    rdanilu
  Goal:
    Decipher the message and find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |
    | Python          | 3.8.6     |
    | Openssl         | 1.1.1     |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge information, it mentions
    """
    Alice sent two times the same message to Bob.
    """
    Then I downloaded the challenge file
    """
    TwoForOne.zip
    """
  Scenario: Fail: Use public keys to generate a private key and find the flag
    Given The downloaded file
    When seeing the file extension
    Then I extracted its content
    """
    key1.pem
    key2.pem
    message1.log
    message2.log
    """
    Then I opened the key1.pem file using Openssl
    And there I found the following information
    """
    RSA Public-Key: (2048 bit)
    Modulus:
    00:c6:ac:b8:df:48:6e:66:71:d4:a5:56:48:03:e1:
    c3:21:4a:8e:27:4d:e0:ac:00:43:ec:28:c8:58:9f:
    37:7c:7e:8d:30:8b:c3:e3:02:85:03:84:34:4b:a7:
    98:88:85:62:0a:41:8e:6a:d9:55:57:82:84:fc:04:
    f2:89:f1:26:b3:8a:01:81:62:51:ce:f9:a1:4f:d4:
    c2:49:d9:6b:69:08:7f:a9:1b:2e:1a:db:dc:80:cb:
    96:ff:0c:cb:61:29:d8:f6:73:7d:a8:50:c4:51:f2:
    ed:3f:6c:b6:1c:36:89:1d:c9:24:d0:ab:28:f2:6a:
    df:0e:d3:57:ce:84:8d:02:ff:e0:09:12:71:4c:cf:
    63:72:c1:f4:10:80:e8:67:47:a0:30:3e:b5:cd:f6:
    ce:91:2f:11:44:fd:4f:55:74:3c:79:68:75:a1:4f:
    df:f8:f8:b6:62:15:0c:56:be:58:b0:92:39:77:1d:
    c4:4d:96:90:79:c4:ad:8f:d9:93:bc:63:0b:78:55:
    d2:e0:2e:8b:e1:68:24:dc:d5:ab:38:13:23:1c:17:
    31:11:0a:8b:d0:28:d7:a1:df:ab:89:2e:75:29:45:
    57:ba:fc:71:ae:af:5e:48:db:02:67:a6:db:63:d3:
    50:f9:95:06:8e:e1:ca:d6:d3:2d:f1:1a:49:bd:24:
    ba:97
    Exponent: 65537 (0x10001)
    """
    Then I opened the key2.pem file with Openssl
    And thus I saw the following information
    """
    RSA Public-Key: (2048 bit)
    Modulus:
    00:c6:ac:b8:df:48:6e:66:71:d4:a5:56:48:03:e1:
    c3:21:4a:8e:27:4d:e0:ac:00:43:ec:28:c8:58:9f:
    37:7c:7e:8d:30:8b:c3:e3:02:85:03:84:34:4b:a7:
    98:88:85:62:0a:41:8e:6a:d9:55:57:82:84:fc:04:
    f2:89:f1:26:b3:8a:01:81:62:51:ce:f9:a1:4f:d4:
    c2:49:d9:6b:69:08:7f:a9:1b:2e:1a:db:dc:80:cb:
    96:ff:0c:cb:61:29:d8:f6:73:7d:a8:50:c4:51:f2:
    ed:3f:6c:b6:1c:36:89:1d:c9:24:d0:ab:28:f2:6a:
    df:0e:d3:57:ce:84:8d:02:ff:e0:09:12:71:4c:cf:
    63:72:c1:f4:10:80:e8:67:47:a0:30:3e:b5:cd:f6:
    ce:91:2f:11:44:fd:4f:55:74:3c:79:68:75:a1:4f:
    df:f8:f8:b6:62:15:0c:56:be:58:b0:92:39:77:1d:
    c4:4d:96:90:79:c4:ad:8f:d9:93:bc:63:0b:78:55:
    d2:e0:2e:8b:e1:68:24:dc:d5:ab:38:13:23:1c:17:
    31:11:0a:8b:d0:28:d7:a1:df:ab:89:2e:75:29:45:
    57:ba:fc:71:ae:af:5e:48:db:02:67:a6:db:63:d3:
    50:f9:95:06:8e:e1:ca:d6:d3:2d:f1:1a:49:bd:24:
    ba:97
    Exponent: 343223 (0x53cb7)
    """
    When I compared the previous keys
    Then I noted that both had the same modulus
    Then I opened the message1.log file
    And There I saw the message below
    """
    RBVdQw7Pllwb42GDYyRa6ByVOfzRrZHmxBkUPD393zxOcrNRZgfub1mqcrAgX4PAsvAOWptJSHb
    rHctFm6rJLzhBi/rAsKGboWqPAWYIu49Rt7Sc/5+LE2dvy5zriAKclchv9d+uUJ4/kU/
    vcpg2qlfTnyor6naBsZQvRze0VCMkPvqWPuE6iL6YEAjZmLWmb+bqO+unTLF4YtM1MkKTtiOEy
    +Bbd4LxlXIO1KSFVOoGjyLW2pVIgKzotB1/9BwJMKJV14+MUEiP40ehH0U2zr8BeueeXp6NIZwS
    /9svmvmVi06Np74EbL+aeB4meaXH22fJU0eyL2FppeyvbVaYQ==
    """
    Then I inspected the message2.log file
    And thus I looked the following message
    """
    TSHSOfFBkK/sSE4vWxy00EAnZXrIsBI/Y6mGv466baOsST+qyYXHdPsI33Kr6ovucDjgDw/
    VvQtsAuGhthLbLVdldt9OWDhK5lbM6e0CuhKSoJntnvCz7GtZvjgPM7JDHQkAU7Pcyall9UEqL
    +W6ZCkiSQnK+j6QB7ynwCsW1wAmnCM68fY2HaBvd8RP2+rPgWv9grcEBkXf7ewA
    +sxSw7hahMaW0LYhsMYUggrcKqhofGgl+4UR5pdSiFg4YKUSgdSw1Ic/tug9vfH
    uLSiiuhrtP38yVzazqOZPXGxG4tQ6btc1helH0cLfw1SCdua1ejyan9l1GLXsAyGOKSFdKw==
    """
    Then I converted the first message to its decimal representation
    Then I got the following number
    """
    873961331782995910091449646847759795543392518345294851177526346683903185874
    441480436813149957402733022186663617169480360522003040418846730425797397389
    019242083324149861224881837648827733065912009282159797086113453810648397800
    807260005896085755695316369539814016145820385526373437011479632066926340672
    536934389323937143990206299577026365877797291758239395472312296757614369580
    752686426670693235050217325701123469157590143131777555504346042029577875228
    242214326925541298943593867221827186726423827500795707382713440078033314653
    272055827489856063627124193667353951475811064374746353751368335130972579203
    721246276744488939944465765448413401407668027548318477504594854150481029698
    862204679242717032386753781156742107462311580316160677231950032550084265078
    787902715618524069842632254977185705834313053732512790485211836467626082647
    357
    """
    Then I noted that the number was so big compared with the modulus
    Then I realized that the message was encoded in base64
    Then I decoded the messages using the base64 decoder in the following link
    """
    https://cryptii.com/pipes/base64-to-hex
    """
    And after that I got the following number for the message1
    """
    859473986887668070767565901615909156327358405036937595024219577099198575172
    935694309189976147334129154666153414725865687782513908650009104061765209485
    502159139849253857221646583015188515086669868781360975724166113485213323664
    773644884679522883704166398010538863039925462340029237476408887977177625876
    988095813802356769512375147578351230592032187314843752986012972151491101166
    496040817707331627584864510355711154631993055109759365518700576961119697882
    231652456418695663393705270922315325358388806239210570172035446410005693607
    977312222464044766635168495649940947953012190422995087330182585823977646508
    6336263230151265
    """
    And the next number for the message2
    """
    973702733603735905918924865367947942776018870720330428770563223499366450662
    612320051640702632017308852604836377567213620194448606890096128928151175912
    470318552578190702371859153714049708948651635716335827483097498513345420730
    828910476203115698745267890726494441212080656201391902421731673400338414605
    398520451794647995654497242119684536129357651770314963115704833581232759543
    768579666799630450786920429993325374258796618932825249708785906790209668743
    520769729628796822533518916980826560602373012635114271053701819009097235551
    163652476852156411364645680764591156921955747239415470912669153383810690914
    6132258511478059
    """
    Then I raised the following equation to decipher the message
    """
    privateKey=exp_publicKey1*exp_publicKey2
    deciphertext=message1^privatekey mod modulus
    """
    Then I did those operations
    And I converted the resulting number to its ASCII representation
    And I got the following result
    """
    <u\xa92\x992\\\xf3\xc9\xd4\x05&\xf4\xf5\x9d<`\xd9Uc[\x84\xf05\x86\x15B\x1f
    \xf8\xc1\xd6\xa9u9\x1f\xaa\xdf\xf3\xaeJ\xab\xe7Zf\xd4\x07\x87j\xe1\xd8\xde\
    x993v#\xe5\xb0~1\t7\xe3\x94 \x9d*\xef\xaf\xc9a\x8c\x83bx\xa9\xa6\xc1C\xc9<}
    \x85\xdayh>\x92\xc9\xdf%\xfd2\xadA\xd8\x18\xc6:f"\x08f\xf5Vq4\x97\xb2\x84\x
    d1c\xb8\xe4\x9d\x05\x86\x16f>$\xa6\x1b\x01RI\xfa\x17\xee!\x84\x06\x8d\xfad_
    B\x1b\x95W\t\xbfyB\xad\x18\x87\tyC\x9f\xa3%-<\xe2\x89\xcbO\xcb\x81\x7fb(\xa
    a\xd5A\xde\xa1M\xe2\xfd\xce\xb6U\xf9;\xa7\x9e\x8e\x1b)~\xd0\x02O\xff\x9e\xd
    6*\x13&\xfaKCB\xbe\x89\xa3\xc8\x19\xd7\xb4C\xb7q\xa6\x01u\xdb\x99\xb4\x87\x
    aaw\x05\xc9F\xc9\xdf\xc7\xf8\x0e`\xae\x8c!\xc7\xb2\x9f\x17\xf0_\x9e\xa4p1\x
    b3S\xdc\xeee\xd4\xd2H6~\x89\x19\xa7-xx\xe2\x08
    """
    Then I knew that the previous approach was wrong

  Scenario: Success: use extended euclidean algorithm to decipher the message
    Given that previously I couldn't decipher the encrypted message
    Then I decided to use the extended euclidean algorithm in the way below
    """
    exp_publicKey1*a+exp_publicKey2*b=GCD(exp_publicKey1,exp_publicKey2)
    but if exp_publicKey1 and exp_publicKey2 are prime numbers
    Greatest_common_divisor(exp_publicKey1,exp_publicKey2)=1
    """
    Then I calculated a and b with the code below
    """
    def eucext(num1:int, num2:int)->List[int]:
    s=[1,0]
    t=[0,1]
    vint=[num1,num2]
    while num1%num2!=0:
    quotien = num1//num2
    mod = num1%num2
    num1 = num2
    num2=mod
    s.append(s[0]-quotien*s[1])
    s.pop(0)
    t.append(t[0]-quotien*t[1])
    t.pop(0)
    return [s[1],t[1]]
    """
    Then I used the following formula to decipher the message
    """
    message1^a*message2^b mod modulus=decipher_message
    message2^b=(message2^-1 mod n)-n
    """
    Then I executed the previous operations
    Then I took the resulting number
    And I executed my python script [evidence](rdanilud.py)
    Then I converted the resulting number to its ASCII representation
    And thus I deciphered the message and got the flag [evidence](01.png)
