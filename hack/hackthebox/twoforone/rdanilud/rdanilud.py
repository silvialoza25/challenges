from typing import List
def eucext(num1:int, num2:int)->List[int]:
  s=[1,0]
  t=[0,1]
  vint=[num1,num2]
  while num1%num2!=0:
    quotien = num1//num2
    mod = num1%num2
    num1 = num2
    num2=mod
    s.append(s[0]-quotien*s[1])
    s.pop(0)
    t.append(t[0]-quotien*t[1])
    t.pop(0)
  return [s[1],t[1]]


dataIn=open('DATA.lst', 'r').readlines()
m1=int(dataIn[0].strip('ma='))
m2=int(dataIn[1].strip('mb='))
n=int(dataIn[2].strip('n='))
e1=int(dataIn[3].strip('ea='))
e2=int(dataIn[4].strip('eb='))
a,b= eucext(e1,e2)
i=pow(m2,-1,n)-n
sub1=pow(m1,a,n)
sub2=pow(i,abs(b),n)
deciphertxt=(sub1*sub2)%n
deciphertxthex=hex(deciphertxt)[2:]
print(bytes.fromhex(deciphertxthex))
