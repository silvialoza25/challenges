#!/usr/bin/env python
# Python lope391.py
"""
Replaces pure black pixels in the original image to white pixels
"""

from PIL import Image
PICTURE = Image.open("blackSquare.png")

"""
Get the size of the image
"""
WIDTH, HEIGHT = PICTURE.size
print("Width:", WIDTH, "Height:", HEIGHT)

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
CONT = 0

"""
Process every pixel
"""
for x in range(WIDTH):
    for y in range(HEIGHT):
        current_color = PICTURE.getpixel((x, y))
        """
        Changing only pure black pixels to white
        """
        if current_color[0] == BLACK:
            PICTURE.putpixel((x, y), WHITE)
            CONT += 1

print("PIX COUNTED: ", CONT)
PICTURE.save('result.png')
