## Version 2.0
## language: en

Feature: blackhole-misc-hackthebox
  Site:
    https://www.hackthebox.eu
  Category:
    misc
  User:
     martialwolf
  Goal:
    Find the flag on the given zip

  Background:
  Hacker's software:
    | <Software name> | <Version>           |
    | NixOS           | 19.09.2260          |
    | Chromium        | 78.0.3904.87        |

  Scenario: Success: ctf
    Given I downloaded the file called Blackhole.zip
    When I proceeded to decompress it
    Then it showed a file named hawking
    And I used the command file to know what type of file is
    When I converted it to JPG
    """
    cp hawking hawking.jpg
    """
    Then it shows a image of Stephen Hawking
    When I used steghide to search for things in the file
    """
    steghide extract -sf hawking.jpg
    """
    Then it promped a passphrase input
    And I put "hawking" as the password
    Then it shows a flag.txt file with a base64 string
    When I decoded it two times it showed some disordered characters
    Then I searched for a caesar chipher decoder
    And I modified the shift until it is readable
    Then I got the flag
