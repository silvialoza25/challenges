## Version 2.0
## language: en

Feature: iknowmag1c - HackTheBox
  Site:
    www.hackthebox.eu
  User:
    williamgiraldo
  Goal:
    Get to the admin userpage

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Google Chrome     |  76.0.3809 |
    | PHP               |    7.1.1   |
  Machine Information:
    Given this is a web-based challenge,
    When I get to the challenge page
    Then I will inspect its source and resources

  Scenario: Fail:load-challenge
    Given I start the challenge
    And it prompts
    """
    Can you get to the profile page of the admin?
    """
    And a page loads, where I register and login.

  Scenario: Fail: inspect-source-code
    Given I get to the web page
    When I see its souce
    Then I can not find anything useful.

  Scenario: Fail: cookies
    When I see the cookies
    Then I find one of interest.
    """
    iknowmag1c=C2J1d%2BQldpssIbjSfd3hjwrW%2FyXguz1OxCMqo%2Bbda
    OGAwnih4fgaNvm8GYmiRMjtCylHUJbe1xc%3D
    """
    And this one looks like url-encoded base64-encoded string.
    When I try to decode it,
    Then it succeeds, but leaves me with a lot of random bytes.

  Scenario: Fail: decryption
    Given I know that encrypted client-side data could be decrypted.
    And this bytestring has exactly 40 bytes (which is a multiple of 8)
    When I play a little with this bytestring
    Then I find a Padding Oracle attack is possible.
    And I try to understand the Padding Oracle attack basis
    And it is very clever.
    Then I developed with a script for decoding any number of cipher blocks
    And it worked [evidence](img1.png),
    Then we are sure we can follow this approach longer.

  Scenario: Success: decrypt-and-encrypt
    Given we now know about our encrypted data (a JSON string),
    And we've successfully decrypted it.
    When analyzing the patterns and mathematical ways to encrypt
    Then we found out encrypting plaintext is possible
    And it could be done using our already developed functionality.
    Then we developed the required logic
    And devised a new function for encrypting arbitrary data.
    And after a lot of time, the script halted (evidence)[img2.png]
    And after putting the cookie using document.cookie = ...
    Then we reloaded the page, and got the flag (evidence)[img3.png]

  Scenario: references
    """
    [1] (Vaudenay, Serge. 2002) CBC Padding: Security Flaws
    in SSL, IPSEC, WTLS.
    [2] (Rizzo et. al. 2010) Practical Padding Oracle Attacks.
    [3] (Paterson et. al, 2005) Padding Oracle Attacks on
    CBC-Mode Encryption with Secret and Random IVs.
    """
