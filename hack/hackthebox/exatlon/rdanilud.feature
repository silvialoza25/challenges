## Version 2.0
## language: en

Feature: exatlon-reversing-hackthebox
  Site:
    https://www.hackthebox.eu/
  Category:
    reversing
  User:
    rdanilu
  Goal:
    Crack the password and get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |
    | Clojure         | 1.10.1    |
    | strings         | 2.35.1    |
    | upx             | 3.96      |

  Machine information:
    Given I am accessing the challenge site
    When I read the challenge information, it mentions
    """
    Can you find the password?
    """
    Then I downloaded the challenge file
    """
    Exatlon.zip
    """
  Scenario: Fail: search password in the file given
    Given The downloaded file
    When I saw the file extension
    Then I extracted its content
    """
    exatlon_v1.bin
    """
    And I executed the program with the next command
    """
    (kali㉿kali)-[~]$ ./exatlon_v1.bin
    """
    And I tried a password [evidence] (01.png)
    And thus I deduced that if a wrong password was introduced
    Then The program showed the next message
    """
    [-] ;(
    """
    Then I used the next command to see the program content
    """
    strings -a exatlon_v1.bin | more
    """
    Then I got the next output [evidence] (02.png)
    Then I inferred that it was compressed using upx
    Then I used the next command to uncompress the program
    """
    upx -d exatlon_v1.bin
    """
    And I got the next output
    """
    Ultimate Packer for eXecutables
    Copyright (C) 1996 - 2020
    UPX 3.96    Markus Oberhumer, Laszlo Molnar & John Reiser   Jan 23rd 2020
    File size           Ratio      Format      Name
    -----------------   ------   -----------   -----------
    2202568 <- 709524   32.21%   linux/amd64   exatlon_v1
    Unpacked 1 file.
    """
    When I got the uncompressed file
    Then I executed the next command again
    """
    strings -a exatlon_v1.bin | more
    """
    When I analyzed the output
    Then I found something special as it can see in [evidence] (03.png)
    And as I'd deduced previously the output shown with a wrong password is
    """
    [-] ;(
    """
    Then the message shown with a correct password is
    """
    [+] Looks Good ^_^
    """
    And therefore the next numbers must be the password
    """
    1152 1344 1056 1968 1728 816 1648 784 1584 816 1728 1520 1840 1664 784 1632
    1856 1520 1728 816 1632 1856 1520 784 1760 1840 1824 816 1584 1856 784 1776
    1760 528 528 2000
    """
    Then I tried to type those numbers as a password
    And it didn't work [evidence] (04.png)

  Scenario: Success:find out the hidden password analyzing the flag format
    Then I converted the numbers found previously to their hexadecimal format
    And after executing the clojurescript program [evidence] (rdanilud.cljs)
    Then I got the next output
    """
    [480 540 420 7b0 6c0 330 670 310 630 330 6c0 5f0 730 680 310 660 740 5f0
    6c0 330 660 740 5f0 310 6e0 730 720 330 630 740 310 6f0 6e0 210 210 7d0]
    """
    When I analyzed the result
    Then I found out that all the numbers end in zero
    Then I decided to remove the last number of all of them
    And I got the next output
    """
    [48 54 42 7b 6c 33 67 31 63 33 6c 5f 73 68 31 66 74 5f 6c 33 66 74 5f 31 6e
    73 72 33 63 74 31 6f 6e 21 21 7d]
    """
    Then I did a conversion from hexadecimal to ASCII as it can see below
    """
    48->H 54->T 42->B 7B->{
    """
    And thus I could determine that the first four ASCII characters
    And the first four flag format characters [evidence] (05.png) were the same
    Then I executed the clojurescript program [evidence] (rdanilud.cljs)
    And I got the next output
    """
    HTB{l3g1c3l_sh1ft_l3ft_1nsr3ct1on!!}
    """
    Then I tried the previous output as a password
    And thus I comprobate that my answer was correct [evidence](06.png)
