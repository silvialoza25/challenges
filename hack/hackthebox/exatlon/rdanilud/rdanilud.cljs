(ns rdanilud
(:gen-class)
(:require [clojure.core :as core])
(:require [clojure.string :as str]))

(defn hex-format [data]
  (let [vec-hex (atom [])]
    (doseq [item data]
      (swap! vec-hex #(conj % (core/format "%x" (Integer. item)))))@vec-hex))

(defn rm-last-item [vec-hex]
  (let [vec-firstitems (atom [])]
    (doseq [item vec-hex]
      (swap! vec-firstitems #(conj % (subs item 0 2))))@vec-firstitems))

(defn ascci-format [vec-firstitems]
  (let [vascii (atom [])]
    (doseq [item vec-firstitems]
      (swap! vascii #(conj %(char (BigInteger. item 16)))))(str/join @vascii)))

(defn main []
  (let [data (str/split (core/read-line) #" ")]
    (println (hex-format data))
    (println (rm-last-item (hex-format data)))
    (println (ascci-format (rm-last-item (hex-format data))))))
(main)
