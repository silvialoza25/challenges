## Version 2.0
## language: en

Feature: Keys - Crypto - Hack the Box
  Site:
    https://www.hackthebox.eu
  Category:
    Crypto
  User:
    arguoK
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A text file with two ciphered text:
    """
    hBU9lesroX_veFoHz-xUcaz4_ymH-D8p28IP_4rtjq0=

    gAAAAABaDDCRPXCPdGDcBKFqEFz9zvnaiLUbWHqxXqScTTYWfZJcz-WhH7rf_
    fYHo67zGzJAdkrwATuMptY-nJmU-eYG3HKLO9WDLmO27sex1-R85CZEFCU=
    """
    And A flag format of
    """
    HTB{$FLAG}
    """
    And Author clue "Can you decrypt the message? "

  Scenario: Fail: Trivial decryption methods
    Given The clue and the ciphered texts
    When I scan the ciphered texts in CyberChef and CipherIdentifier
    Then I get no analysis result
    When I try XOR between both texts
    Then I get no result
    And I fail to find the flag

  Scenario: Success: Fernet decode
    Given The ciphered texts
    And No trivial cipher/encode is being used
    When I look at htb forum
    """
    Hint : this type of bitter is usually served as a digestif after a meal
    but may also be served with coffee and espresso or mixed into coffee
    and espresso drinks
    """
    When I search for this quote in google
    Then I find Fernet wine
    When I search "Fernet Crypto" in google
    Then I find "Fernet symmetric encryption" with a Python library
    And read about it syntax
    When I execute the script to decode the key using the token
    Then I can see the "HTB{...}" pattern
    And I caught the flag [evidence](img1.jpg)
