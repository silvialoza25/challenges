## Version 2.0
## language: en

Feature: Reminiscent-Hack_The_Box
  Site:
    https://www.hackthebox.eu
  Category:
    Forensic
  User:
    lope391
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Volatility      | 2.6         |
    | Linux Mint      | 19.3(Tricia)|
    | Mozilla Firefox | 72.0.2      |
  Machine information:
    Given A suspicious email file
    """
    <html>
      <head>
        <meta http-equiv=3D"Content-Type"
        content=3D"text/html; charset==3DUTF-8" />
      </head>
      <body style=3D'font-size: 10pt;
      font-family: Verdana,Gen= eva,sans-serif'>
        <div class=3D"pre" style=3D"margin:
        0; padding: 0; font-family: monospace">=
          <br /> Hi Frank, someone told me you would
          be great to review my resume.. cuold you have a look?
        <br /> <br />
        <a href=3D"http://10.10.99.55:8080/resume=
         =2Ezip">resume.zip</a></div>
      </body>
    </html>
    """
    And A memory dump of a VM
    And the basic information of the memory image
    """
    Suggested Profile(s) :
    Win7SP1x64, Win7SP0x64, Win2008R2SP0x64, Win2008R2SP1x64_23418,
    Win2008R2SP1x64, Win7SP1x64_23418
                     AS Layer1 : WindowsAMD64PagedMemory (Kernel AS)
                     AS Layer2 : VirtualBoxCoreDumpElf64 (Unnamed AS)
                     AS Layer3 : FileAddressSpace
                     (/home/infosec/dumps/mem_dumps/01/flounder-pc-memdump.elf)
                      PAE type : No PAE
                           DTB : 0x187000L
                          KDBG : 0xf800027fe0a0L
          Number of Processors : 2
     Image Type (Service Pack) : 1
                KPCR for CPU 0 : 0xfffff800027ffd00L
                KPCR for CPU 1 : 0xfffff880009eb000L
             KUSER_SHARED_DATA : 0xfffff78000000000L
           Image date and time : 2017-10-04 18:07:30 UTC+0000
     Image local date and time : 2017-10-04 11:07:30 -0700

    """
    And The instructions:
    """
    Find and decode the source of the malware to find the flag.
    """
    And the flag format
    """
    HTB{flag}
    """
    And a memory dump analysis application in Volatility
    And its running on Mint 19.3 with kernel 18.04.3

  Scenario: Fail: Did not find the flag
    Given The memory dump file
    When I run the command:
    """
    volatility -f flounder-pc-memdump.elf --profile=Win7SP1x64 pstree
    """
    Then I get the hierarchy of processes active before the memory dump
    When I look at the active processes
    Then I can see there was an explorer process active
    And It had spawned several child processes [evidence](1.png)
    When I look at the child processes
    Then I see one of them is called "VBoxTray.exe"
    When I search fot the origin of this process
    Then I find it is a system initialization process
    And It is only called legitimately by System32
    When I run the command:
    """
    volatility -f flounder-pc-memdump.elf --profile=Win7SP1x64 \
    memdump -p 2044 -D tmp
    """
    Then I get a dump of the memory region for the explorer process
    When I run the command:
    """
    strings * /tmp
    """
    Then I get a list of all the strings within the explorer process dump
    When I read through all the strings
    But I do not find any string that could resemble the flag

  Scenario: Success: Found the flag
    Given The memory dump file
    When I run the command:
    """
    volatility -f flounder-pc-memdump.elf --profile=Win7SP1x64 \
    filescan | grep -i "resume"
    """
    Then I get a list of the files named "resume" [evidence](2.png)
    And I might have found the source of the malware
    When I run the command:
    """
    volatility -f flounder-pc-memdump.elf --profile=Win7SP1x64 \
    dumpfiles -Q 0x000000001e8feb70 -D filedump
    """
    Then I get the file contents dumped into the "filedump folder"
    When I look at the contents of this file
    Then I find it looks like a base64 encoded message
    When I try to decode the message using UTF-8 encoding
    Then I get a half decoded message with invalid characters [evidence](3.png)
    And I assume I am using the wrong encoding
    When I try to decode the message using UTF-16 LE
    Then I get a power shell script
    And The script is encoding another base64 string [evidence](4.png)
    When I decode the final base64 string
    Then I get the source code of a trojan virus
    And Within the source code I find the formated flag [evidence](5.png)
    When I enter the found flag as the answer
    Then I solve the challenge
