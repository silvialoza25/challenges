## Version 2.0
## language: en

Feature: Baby RE - Reversing - Hack the Box
  Site:
    https://www.hackthebox.eu
  Category:
    Reversing
  User:
    arguoK
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A ZIP file
    And it contains a "baby" file
    And A text "Show us your basic skills!"
    And the flag format "HTB{flag}"
    And challenge category Reversing

  Scenario: Fail: Strings in the file
    Given The information text
    And Category Reversing
    When I use command "strings baby"
    Then I can see a readable sentence [evidence](img1.png)
    """
    Dont run `strings` on this challenge, that is not the way!!!!
    """
    And I don't find anything useful

  Scenario: Success: Executable file
    Given The file "baby" inside the ZIP file
    And category Reversing
    When I use the command "file baby"
    Then I see the file information
    """
    baby: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV),
    dynamically linked, interpreter /lib64/l,
    BuildID[sha1]=25adc53b89f781335a27bf1b81f5c4cb74581022,
    for GNU/Linux 3.2.0, not stripped
    """
    And I know "ELF 64-bit LSB shared object" is a type of Executable
    When I change the access permission of the file using "chmod"
    Then I can execute the file
    When I execute the file
    Then the executable asks for a key
    But I remember the information of my first try
    And there is the key [evidence](img1.png)
    When I enter the key
    Then I can see the "HTB{flag}" format [evidence](img2.png)
    And I caught the flag
