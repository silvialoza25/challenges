## Version 2.0
## language: en

Feature: bypass
  Site:
    Hackthebox
  Category:
    Reversing
  User:
    lkkpp
  Goal:
    Get the flag

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Windows         | 10        |
    | Dnspy           | 6.1.7     |
    | Ghidra          | 9.1.2     |
    | DIE             | 3.0       |

  Machine information:
    Given I am accessing the challenge through its section
    When I open the challenge section
    And I see the following sentence
    """
    The Client is in full control.
    Bypass the authentication and read the key to get the Flag.
    """
    Then it is possible to download the challenge

  Scenario: Success: Analyze the binary statically
    Given a binary is provided
    When I analyze it with DIE (Detect It Easy) [evidences](die.png)
    And I realize that the binary is a .NET
    Then the idea is to debug it dynamically

  Scenario: Fail: Debug the binary with Ghidra
    Given the idea is to debug the binary
    When I use Ghidra [evidences](ghidra.png)
    Then I realize that it is not able to correctly parse the binary
    """
    uVar20 = (uint)CONCAT11((char)(unaff_EBX >> 8) + *(char *)(unaff_EBX + 0x15
    puVar22 = (uint *)(unaff_EBX & 0xffff0000 | uVar20);
    bVar6 = (byte)in_EAX;
    *in_EAX = *in_EAX + bVar6;
    piVar14 = (int *)((uint)in_EAX & 0xffffff00 | (uint)(byte)(bVar6 + 0x6f));
    pbVar18 = (byte *)((int)piVar14 + (uint)(0x90 < bVar6) + *piVar14);
    *param_2 = *param_2 + param_1;
    puVar24 = (undefined4 *)
      ((unaff_ESI -
      *(int *)((uint)pbVar18 & 0xffffff00 | (uint)(byte)((byte)pbVar18
      (uint)((byte)pbVar18 < *pbVar18));
    """

  Scenario: Success: Analyze the binary dynamically with Dnspy
    Given the binary is a .NET so the idea to debug it with Dnspy
    """
    dnSpy is a debugger and .NET assembly editor.
    """
    When I open the binary in the debugger
    And it stops at the entry point, I see what I could call the main function
    """
    // Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000258
    public static void 0()
    {
        bool flag = global::0.1();
        bool flag2 = flag;
        if (flag2)
        {
            global::0.2();
        }
        else
        {
            Console.WriteLine(5.0);
            global::0.0();
        }
    }
    """
    And this function calls the sub function "global::0.1();"
    Then this sub function asks for a username and password
    """
    public static bool 1()
    {
        Console.Write(5.1);
        string text = Console.ReadLine();
        Console.Write(5.2);
        string text2 = Console.ReadLine();
        return false;
    }
    """
    When the data has been entered it will return false (always)
    And it will print "Wrong username and/or password"
    And if for some reason it doesn't return false
    Then it could print something else (maybe the flag)
    """
    public static void 2()
    {
        string <<EMPTY_NAME>> = 5.3;
        Console.Write(5.4);
        string b = Console.ReadLine();
        bool flag = <<EMPTY_NAME>> == b;
        if (flag)
        {
            Console.Write(5.5 + global::0.2 + 5.6);
        }
        else
        {
            Console.WriteLine(5.7);
            global::0.2();
        }
    }
    """

  Scenario: Success: Bypassing the if condition
    Given in a normal case it will always print error "Wrong ..."
    And this obviously is not going to give me the flag
    Then the idea is to modify the condition or the return
    When I try to do it it's very easy [evidences](modifyif.png)
    And this is because Dnspy allows me to modify return values
    """
    -------------------+-----------------+-------------------
    Name               | Value           | Type
    -------------------+-----------------+-------------------
    flag               | false           | bool
    flag2              | true            | bool
    -------------------+-----------------+-------------------
    """
    Then I continue the debugger and get the flag [evidences](flag.png)
