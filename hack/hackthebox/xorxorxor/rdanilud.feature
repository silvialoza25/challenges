## Version 2.0
## language: en

Feature: xorxorxor-crypto-hackthebox
  Site:
    https://www.hackthebox.eu/
  Category:
    crypto
  User:
    rdanilu
  Goal:
    Decipher the flag

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 2020.4    |
    | Mozilla Firefox | 78.5.0    |
    | Python          | 3.8.6     |
    | pip             | 20.1.1    |
  Machine information:
    Given I am accessing the challenge site
    When reading the challenge information, it mentions
    """
    Who needs AES when you have XOR?
    """
    Then I download the challenge file
    """
    xorxorxor.zip
    """
  Scenario: Fail: search flag in challenge files
    Given The downloaded file
    Then I extracted the content
    And I analyzed the file
    """
    output.txt
    """
    And I found the ciprathed flag
    """
    Flag: 134af6e1297bc4a96f6a87fe046684e8047084ee046d84c5282dd7ef292dc9
    """
    Then I analyzed the another extracted file
    """
    challenge.py
    """
    Then I validated the file content
    And I found the next code
    """
    import os
    flag = open('flag.txt', 'r').read().strip().encode()
    class XOR:
    def __init__(self):
    self.key = os.urandom(4)
    def encrypt(self, data: bytes) -> bytes:
    xored = b''
    for i in range(len(data)):
    xored += bytes([data[i] ^ self.key[i % len(self.key)]])
    return xored
    def decrypt(self, data: bytes) -> bytes:
    return self.encrypt(data)
    def main():
    global flag
    crypto = XOR()
    print ('Flag:', crypto.encrypt(flag).hex())
    if __name__ == '__main__':
    main()
    """
    Then I noted that it was the code to cipher the flag
    Then I analzed the code, I found that the key size was 4 bytes
    And I tried to search the key in the code but I didn't find it

  Scenario: Success:analyze the flag format and use the xor to find the key
    Given the flag format [evidence](01.png)
    And taking into account that the key size was 4 bytes
    And the inverse of xor is xor
    Given pwn library of python
    Then I installed the library by pip
    """
    pip install pwn
    """
    When the library is installed
    Then I used xor function with the first four bytes of the ciprathed flag
    And with the first four bytes of the flag format respectively
    Then I executed the python script [evidence](rdanilud.py)
    Then I got each of one of the key bytes [evidence](02.png)
    Then I operated with xor the gotten key with the ciprathed flag
    And I got the deciprathed flag  [evidence](03.png)
