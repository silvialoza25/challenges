from pwn import xor
cipherText = bytes.fromhex(open('DATA.lst', 'r').read().strip('Flag: '))
key = b''
for i in range(4):
 form = b'HTB{'
 for x in range(256):
   if xor(cipherText[i], x.to_bytes(1, 'big')).find(form[i]) != -1:
     key += x.to_bytes(1, 'big')
     break
print(xor(cipherText, key))
