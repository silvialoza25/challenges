## Version 2.0
## language: en

Feature: Took the Byte - Forensics - Hack the Box
  Site:
    https://www.hackthebox.eu
  Category:
    OSINT
  User:
    arguoK
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A ZIP file
    And it contains a "password" file
    And A text "Someone took my bytes! Can you recover my password for me?"
    And the flag format "HTB{flag}"
    And challenge category Forensics

  Scenario: Fail: Wrong operation in binary
    Given The "password" file
    And category Forensics
    When I check the file binary
    """
    10101111 10110100 11111100 11111011 11101011 11111111 11110111 11111111
    11110111 11111111 00101000 01100011 10101010 10110001 11111111 11111111
    11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111111
    11111111 11111111 11110011 11111111 11101111 11111111 10001111 10011110
    10001100 10001100 10001000 10010000 10001101 10011011 11010001 10001011
    10000111 10001011 10101010 10100111 11110011 11111111 00100100 10111011
    10010000 10100011 01101010 10111011 10010000 10100011 00001010 11111110
    11101011 11111111 00001100 11110111 10001110 01010101 11001001 11001101
    10001000 00110011 10001101 10001100 00110110 11010001 00110011 11001011
    11010011 00001000 01010101 00011010 11111101 11111111 10101111 10110100
    11111000 11110111 10110010 00110111 00000001 11000001 11101011 11111111
    11111111 11111111 11101101 11111111 11111111 11111111 10101111 10110100
    11111110 11111101 11101010 11111100 11101011 11111111 11110111 11111111
    11110111 11111111 00101000 01100011 10101010 10110001 10110010 00110111
    00000001 11000001 11101011 11111111 11111111 11111111 11101101 11111111
    11111111 11111111 11110011 11111111 11110011 11111111 11111111 11111111
    11111111 11111111 11111111 11111111 11111111 10111111 01011011 01111110
    11111111 11111111 11111111 11111111 10001111 10011110 10001100 10001100
    10001000 10010000 10001101 10011011 11010001 10001011 10000111 10001011
    10101010 10100111 11110111 11111111 00100100 10111011 10010000 10100011
    01101010 10111011 10010000 10100011 10101111 10110100 11111010 11111001
    11111111 11111111 11111111 11111111 11111110 11111111 11111110 11111111
    10111001 11111111 11111111 11111111 10100001 11111111 11111111 11111111
    11111111 11111111
    """
    Then I can see the file binary so i can search for the "took byte"
    When I search for any missing byte to complete a magic number header
    Then I can't find anything useful
    And I failed to find the flag

  Scenario: Success: Flag in Hex XOR
    Given The "password" file
    And category Forensics
    When I use "xxd password" command
    """
    afb4 fcfb ebff f7ff f7ff 2863 aab1 ffff  ..........(c....
    ffff ffff ffff ffff ffff f3ff efff 8f9e  ................
    8c8c 8890 8d9b d18b 878b aaa7 f3ff 24bb  ..............$.
    90a3 6abb 90a3 0afe ebff 0cf7 8e55 c9cd  ..j..........U..
    8833 8d8c 36d1 33cb d308 551a fdff afb4  .3..6.3...U.....
    f8f7 b237 01c1 ebff ffff edff ffff afb4  ...7............
    fefd eafc ebff f7ff f7ff 2863 aab1 b237  ..........(c...7
    01c1 ebff ffff edff ffff f3ff f3ff ffff  ................
    ffff ffff ffbf 5b7e ffff ffff 8f9e 8c8c  ......[~........
    8890 8d9b d18b 878b aaa7 f7ff 24bb 90a3  ............$...
    6abb 90a3 afb4 faf9 ffff ffff feff feff  j...............
    b9ff ffff a1ff ffff ffff
    """
    Then I can see most of its bytes value is "ff"
    When I use XOR with "ff" hex key
    """
    PK........×.UN................password.txtUX..ÛDo\.Do\õ...ó.qª62w
    ÌrsÉ.Ì4,÷ªå..PK..MÈþ>........PK..........×.UNMÈþ>................
    ...@¤.....password.txtUX..ÛDo\.Do\PK..........F...^.....
    """
    Then I can see the file magic numbers for a zipped file
    When I unzip the file
    Then I can see the flag [evidence](img1.png)
    And I caught the flag
