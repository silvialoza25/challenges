## Version 2.0
## language: en

Feature: unix 101 - level 1 - rankk
  Site:
    Rankk
  Category:
    level-1
  User:
    andres568
  Goal:
    Change permissions on a file

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  Machine information:
    Given I am accessing the challenge page

  Scenario: Fail:use-symbolic-method
    Given I am on the main page
    And I can see the message
    """
    help Genius change the permission of the file as follows:

    Owner: full access
    Group: read access
    Other: no access

    $ ls -l restricted
    -rwxr-xr-x   1   Genius   220   Feb 07 15:36   restricted
    """
    When I search for the "ls -l" command
    Then I find that the command shows the information about files
    And  I realized that the file is called "restricted"
    When I search for a command to change the permissions
    Then I find the command "chmod"
    When I use the command to change the permission
    """
    chmod go-rwx restricted; chmod g+r restricted
    """
    Then The challenge page show a message
    """
    Wrong
    """
    And I can not capture the flag

  Scenario: Success:use-absolute-form
    Given I am on the main page
    When I search for another way to change permissions on a file
    Then I find that I can use the "chmod" command with the absolute form
    And There are numbers which are asigned to each permission
    When I use the absolute form to change permissions
    """
    chmod 740 restricted
    """
    Then I capture the flag
