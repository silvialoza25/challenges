# pylint oscardjuribe.py
# No config file found, using default configuration
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
Script to solve the problem asm-ii from rankk.org
"""

import re


def parse_value(current_register, current_value, current_registers_array):
    """
    Method to convert input values to numbers
    """

    # check if value is numeric or a register
    if re.match(r"[A-C][X|H|L]", current_value) is None:
        # check if value was passed as hexadecimal or not
        if "h" in current_value:
            # cast from hex string to int
            final_value = int(current_value[:-1], base=16)
        else:
            # cast from string to int
            final_value = int(current_value)
            # check if needs only the lower bits of the register
        if "L" in current_register:
            # and with 11111111 to make zero the more significant bits
            final_value = int(final_value & 255)
    else:
        if "A" in current_value:
            final_value = current_registers_array[0]
        if "B" in value:
            final_value = current_registers_array[1]
        if "C" in value:
            final_value = current_registers_array[2]

    return final_value


def add(add_register, add_value, add_registers_array):
    """
    Method to simulate the add instruction in assembler
    """

    # calculate the value to add
    add_value = parse_value(add_register, add_value, add_registers_array)

    # check where to store the result
    if "A" in add_register:
        add_registers_array[0] += add_value
    if "B" in add_register:
        add_registers_array[1] += add_value
    if "C" in add_register:
        add_registers_array[2] += add_value

    # return current registers_array
    return add_registers_array


def mov(mov_register, mov_value, mov_registers_array):
    """
    Method to simulate the mov instruction in assembler
    """

    # calculate the value to mov
    mov_value = parse_value(mov_register, mov_value, mov_registers_array)

    # check where to move
    if "A" in mov_register:
        mov_registers_array[0] = mov_value
    if "B" in mov_register:
        mov_registers_array[1] = mov_value
    if "C" in mov_register:
        mov_registers_array[2] = mov_value

    # return current registers_array
    return mov_registers_array


def sub(sub_register, sub_value, sub_registers_array):
    """
    Method to simulate the sub instruction in assembler
    """

    # calculate the value to sub
    sub_value = parse_value(sub_register, sub_value, sub_registers_array)

    # check where to store the result
    if "A" in sub_register:
        sub_registers_array[0] -= sub_value
    if "B" in sub_register:
        sub_registers_array[1] -= sub_value
    if "C" in sub_register:
        sub_registers_array[2] -= sub_value

    # return current registers_array
    return sub_registers_array


def shl(shl_register, shl_value, shl_registers_array):
    """
    Method to simulate the shift left instruction in assembler
    """

    # calculate the value to shl
    shl_value = parse_value(shl_register, shl_value, shl_registers_array)

    # check where to store the result
    if "A" in shl_register:
        shl_registers_array[0] = shl_registers_array[0] << shl_value
    if "B" in shl_register:
        shl_registers_array[1] = shl_registers_array[1] << shl_value
    if "C" in register:
        shl_registers_array[2] = shl_registers_array[2] << shl_value

    # return current registers_array
    return shl_registers_array


def shr(shr_register, shr_value, shr_registers_array):
    """
    Method to simulate the shift right instruction in assembler
    """

    # calculate the value to shr
    shr_value = parse_value(shr_register, shr_value, shr_registers_array)

    # check where to store the result
    if "A" in register:
        shr_registers_array[0] = shr_registers_array[0] >> shr_value
    if "B" in register:
        shr_registers_array[1] = shr_registers_array[1] >> shr_value
    if "C" in register:
        shr_registers_array[2] = shr_registers_array[2] >> shr_value

    # return current registers_array
    return shr_registers_array


def mul(mul_register, mul_value, mul_registers_array):
    """
    Method to simulate the mul instruction in assembler
    """

    # calculate the value to shr
    mul_value = parse_value(mul_register, mul_value, mul_registers_array)

    # result is stored in AX
    mul_registers_array[0] *= mul_value

    # return current registers_array
    return mul_registers_array


def idiv(div_register, div_value, div_registers_array):
    """
    Method to simulate the idiv instruction in assembler
    """

    # calculate the value to shr
    div_value = parse_value(div_register, div_value, div_registers_array)

    # result is stored in AX
    div_registers_array[0] /= div_value

    # return current registers_array
    return div_registers_array


def get_bl(current_registers):
    """
    Method to get the value in the BL register as a char
    """

    # return first 8 lower bits
    return chr(current_registers[1] & 255)


# asm code
F = """
mov AX,171
sub AX,154
mov CX,AX
shl AX,2
sub AX,CX
mov BL,AL

mov BX,101
mov CL,42h
sub BX,13h
shr CX,1
sub BL,CL

mov AX,13h
mov BX,3
mul BL
mov BL,AL

mov CX,7
mov AX,0C8h
idiv CL
add AL,01Bh
mov BL,AL

mov CX,12h
sub CX,0Ah
shl CX,1
mov BX,CX
shl CX,2
add CX,BX
sub CL,5
mov BL,CL
"""

# array containing values on each register, AX,BX,CX
REGISTERS_ARRAY = [0, 0, 0]

# empty string to store the answer
SOL = ""

# iterate over lines with text
for line in F.split("\n"):
    # avoid blank lines
    if len(line) > 1:
        # get operation and the other part of the instruction
        operation, values = line.split(" ")

        # if instruction is not mul or idiv then split
        if operation != "mul" and operation != "idiv":
            # split other part to get
            register, value = values.split(",")
        # because mul and idiv only have one parameter and the
        # other is always AX
        else:
            # operation always with AX register
            register = "AX"
            # get the value
            value = values

        # if operation == mov then call mov method
        if operation == "mov":
            REGISTERS_ARRAY = mov(register, value, REGISTERS_ARRAY)
        # if operation == sub then call sub method
        elif operation == "sub":
            REGISTERS_ARRAY = sub(register, value, REGISTERS_ARRAY)
        # if operation == shl then call shl method
        elif operation == "shl":
            REGISTERS_ARRAY = shl(register, value, REGISTERS_ARRAY)
        # if operation == shr then call shr method
        elif operation == "shr":
            REGISTERS_ARRAY = shr(register, value, REGISTERS_ARRAY)
        # if operation == add then call add method
        elif operation == "add":
            REGISTERS_ARRAY = add(register, value, REGISTERS_ARRAY)
        # if operation == mul then call mul method
        elif operation == "mul":
            REGISTERS_ARRAY = mul(register, value, REGISTERS_ARRAY)
        # if operation == idiv then call idiv method
        elif operation == "idiv":
            REGISTERS_ARRAY = idiv(register, value, REGISTERS_ARRAY)
    else:
        SOL += get_bl(REGISTERS_ARRAY)
        REGISTERS_ARRAY = [0, 0, 0]

SOL += get_bl(REGISTERS_ARRAY)
print "The solution is: " + SOL
