## Version 2.0
## language: en

Feature: Mini challenge - miscellaneous - rankk
  Site:
    https://www.rankk.org/challenges/mini-challenge.py
  User:
    pperez7 (wechall)
  Goal:
    Solve the challenge

  Background:
  Hacker's software:
    | Windows      | 1809 |
    | Google Chrome     | 79.0.3945.130  |
  Machine information:
    Given a link in the challenge page
    When I access it
    Then I can see a text with a hint about the challenge
    """
      [evidence](hint.png)
    """

Scenario: Fail: Wrong browser
    Given The hint of the challenge
    When I search in Chrome DevTools the option "Network conditions"
    Then I uncheck the option "Select automatically" in "User Agent" section
    """
      [evidence](useragent.png)
    """
    And  I look for a browser from a small device like Android
    Then I select the browser "Android (2.3) browser"
    Given The User agent I selected
    When I refresh the page
    Then nothing changes in the challenge's page

  Scenario: Success: Solving challenge
    Given The hint of the challenge
    When I search in Chrome DevTools the option "Network conditions"
    Then I uncheck the option "Select automatically" in "User Agent" section
    """
      [evidence](useragent.png)
    """
    And  I look for a browser called "UC Browser - Windows Phone" at the list
    Then I select that Browser
    Given The User agent I selected
    When I refresh the page
    Then I solve the challenge
