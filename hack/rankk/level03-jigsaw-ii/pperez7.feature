## Version 2.0
## language: en

Feature: Jigsaw II - miscellaneous - rankk
  Site:
    Rankk
  User:
    pperez7 (wechall)
  Goal:
    Solve the challenge

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    | Windows         |     1809      |
    | Google Chrome   | 79.0.3945.130 |
  Machine information:
    Given a link in the challenge's page
    When I access it
    Then I can see a picture with the following puzzle
    """
    [evidence](puzzle.png)
    """

  Scenario: Fail: Decrypting the first solution a get
    Given The challenge's puzzle
    When I open the picture using the Paint 3D tool at my computer
    Then I crop each piece of the puzzle
    And I finally get the resultant image of the puzzle
     """
    [evidence](result.png)
    """
    When I get all the lines
    Then I notice that they all have 8 digits
    And that they represent binary numbers
    When I go to the following page
    """
    https://www.rapidtables.com/convert/number/binary-to-ascii.html
    """
    Then I convert the resultant binary numbers
    And I get an illegible text

  Scenario: Reversing the binary numbers
    Given The numbers I get from the resultant image of the puzzle
    When I notice that all the binary numbers has '0' as the last digit
    Then I reverse all the numbers
    When I go to the following page
    """
    https://www.rapidtables.com/convert/number/binary-to-ascii.html
    """
    Then I convert the resultant binary numbers
    And I get 5 letters
    And I reverse the resultant text
    And I get the <FLAG>
    When I write the <FLAG> at the challenge's input text
    Then I press the submit button
    And I solve the challenge
