## Version 2.0
## language: en

Feature: Newbie PHP coder - miscellaneous - rankk
  Site:
    Rankk
  User:
    pperez7 (wechall)
  Goal:
    Solve the challenge

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    | Windows         |     1809      |
    | Google Chrome   | 79.0.3945.130 |
  Machine information:
    Given a link in the challenge page
    When I access it

  Scenario: Fail: Wrong code analysis
    Given The following PHP code
    """
    function transform($elem) {
        if ($elem %2 == 1) {
            return ++$elem;
        }
        else {
            return --$elem;
        }
    }
    $info = $_COOKIE["info"];
    preg_match_all("/\d/", $info, $out);
    $array = $out[0];
    if (sizeof($array) < 4) {
        $solution = substr($info, 0, 8);
    }
    else {
        $array = array_map("transform", $array);
        $solution = implode("", $array);
    };
    """
    When I get the value of the first 8 characters of "$info"
    And I try that value as the solution for the challenge
    Then I see a "Wrong answer" message printed at the page

  Scenario: Success: Solving challenge
    Given The PHP code
    When I realize that I need the value of "$_COOKIE["info"]"
    Then I go to my browser's cookie list at that page
    When I see the cookie "info"
    Then I save it's value in a file text
    When I go to "paiza.io" where I can find a PHP online compiler
    Then I paste the code in the editor
    And change the value of "$info" to be the one I saved in the file text
    And I use the var_dump function to print the value of "$solution"
    When I try that value in the challenge's input
    Then I solve the challenge
