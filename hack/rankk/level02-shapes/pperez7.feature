## Version 2.0
## language: en

Feature: Shapes - miscellaneous - rankk
  Site:
    Rankk
  User:
    pperez7 (wechall)
  Goal:
    Solve the challenge

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    | Windows         |     1809      |
    | Google Chrome   | 79.0.3945.130 |
  Machine information:
    Given a link in the challenge's page
    When I access it
    Then I can see and input text with the following hint above
    """
    Only your keyboard can save you this time.

    TGBUHM WSX RFVGYJM UYTGBNMJH YUIUJM QAZXCDE ERTRFV
    """

  Scenario: Fail: Drawing all the shapes together
    Given The challenge's information
    When I get a picture from internet of a keyboard
    Then I use the Paint tool from my computer
    And I try to draw all the patterns at without erasing the previous one
    And I can't identify anything from the final shape that could be the flag

  Scenario: Success: Drawing the shapes one by one
    Given The challenge's information
    When I get a picture from internet of a keyboard
    Then I use the Paint tool from my computer
    And I draw all the patterns one by one
    And I can see that all the patterns representing a letter
    When I write all the letters that i got
    Then I press the submit button
    And I solve the challenge
