# shellcheck shell=bash

function lint {
  local path="${1}"
  local lizard_max_warns='0'
  local lizard_max_func_length='30'
  local lizard_max_ccn='10'
  local cppcheck_args=(
    --error-exitcode=1
  )
  local splint_args=(
    -strict
    -internalglobs
    -modfilesys
    -boundsread
  )
  local lizard_args=(
    --ignore_warnings "${lizard_max_warns}"
    --length "${lizard_max_func_length}"
    --CCN "${lizard_max_ccn}"
  )

  cppcheck "${cppcheck_args[@]}" "${path}" \
    && splint "${splint_args[@]}" "${path}" \
    && lizard "${lizard_args[@]}" "${path}"
}

function compile {
  local path="${1}"
  local args=(
    -Wall
    -Wextra
    -Winit-self
    -Wuninitialized
    -Wmissing-declarations
    -Winit-self
    -ansi
    -pedantic
    -Werror
  )
  chmod -R +w "$(dirname "${path}")" \
    && gcc "${args[@]}" "${path}" -o "${path%.*}" -lm
}

function main {
  local tmp
  local path

  info Compiling C code \
    && path="${envTargets}" \
    && tmp="$(mktemp)" \
    && find "${path}" -wholename '*.c' -type f \
    | sort --ignore-case > "${tmp}" \
    && while read -r path; do
      info Linting "${path}" \
        && lint "${path}" \
        && info Compiling "${path}" \
        && compile "${path}" \
        || critical Unsuccessful compilation
    done < "${tmp}" \
    && touch "${out}"
}

main "${@}"
