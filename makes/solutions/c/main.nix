{ makeDerivation
, makePythonEnvironment
, inputs
, path
, ...
}:
makeDerivation {
  name = "build_solutions_c";
  env = {
    envTargets = path "/code";
  };
  searchPaths = {
    bin = [
      inputs.nixpkgs.gcc
      inputs.nixpkgs.cppcheck
      inputs.nixpkgs.splint
      inputs.nixpkgs.findutils
    ];
    source = [
      (makePythonEnvironment {
        dependencies = [
          "lizard==1.17.3"
        ];
        name = "lizard";
        python = "3.7";
      })
    ];
  };
  builder = ./entrypoint.sh;
}
