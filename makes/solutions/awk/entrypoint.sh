# shellcheck shell=bash

function main {
  local args=(
    --source 'BEGIN { exit(0) } END { exit(0) }'
    --lint=fatal
    --lint=invalid
    --posix
    -f
  )
  local tmp
  local path

  info Compiling AWK code \
    && path="${envTargets}" \
    && tmp="$(mktemp)" \
    && find "${path}" -wholename '*.awk' -type f \
    | sort --ignore-case > "${tmp}" \
    && while read -r path; do
      info Compiling "${path}" \
        && gawk "${args[@]}" "${path}" \
        || critical Unsuccessful compilation
    done < "${tmp}" \
    && touch "${out}"
}

main "${@}"
