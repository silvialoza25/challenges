{ makeDerivation
, inputs
, path
, ...
}:
makeDerivation {
  name = "build_solutions_awk";
  env = {
    envTargets = path "/code";
  };
  searchPaths = {
    bin = [
      inputs.nixpkgs.gawk
      inputs.nixpkgs.findutils
    ];
  };
  builder = ./entrypoint.sh;
}
