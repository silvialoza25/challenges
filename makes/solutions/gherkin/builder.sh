# shellcheck shell=bash

function main {
  local HOME="."
  local args=(
    --format
  )

  info Compiling Gherkin solution \
    && cucumber "${args[@]}" rerun "${envTarget}" \
    || critical Unsuccessful compilation \
    && touch "${out}"
}

main "${@}"
