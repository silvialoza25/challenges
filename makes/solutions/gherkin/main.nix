{ makeDerivation
, makeDerivationParallel
, pathCopy
, inputs
, pathsMatching
, ...
}:
let
  solutionsPaths = pathsMatching {
    regex = ".*\\.feature";
    targets = [
      "/hack"
      "/vbd"
    ];
  };

  makeSolution = src: makeDerivation {
    env = {
      envTarget = pathCopy src;
    };
    name = "build-solution-gherkin${src}";
    searchPaths = {
      bin = [
        inputs.nixpkgs.cucumber
      ];
    };
    builder = ./builder.sh;
  };
in
makeDerivationParallel {
  dependencies = builtins.map makeSolution solutionsPaths;
  name = "build-solutions-for-gherkin";
}
