{ makeDerivation
, makeDerivationParallel
, pathCopy
, inputs
, pathsMatching
, ...
}:
let
  solutionsPaths = pathsMatching {
    regex = ".*\\.lobster";
    targets = [ "/code" ];
  };

  makeSolution = src: makeDerivation {
    env = {
      envTarget = pathCopy src;
    };
    name = "build-solution-lobster${src}";
    searchPaths = {
      bin = [
        inputs.nixpkgs.lobster
      ];
    };
    builder = ./builder.sh;
  };
in
makeDerivationParallel {
  dependencies = builtins.map makeSolution solutionsPaths;
  name = "build-solutions-for-lobster";
}
