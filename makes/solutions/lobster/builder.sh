# shellcheck shell=bash

function main {
  local args=(
    --pak
  )

  info Compiling lobster solution \
    && lobster "${args[@]}" "${envTarget}" \
    || critical Unsuccessful compilation \
    && touch "${out}"
}

main "${@}"
