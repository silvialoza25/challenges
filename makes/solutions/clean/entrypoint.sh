# shellcheck shell=bash

function main {
  local tmp
  local path

  info Compiling Clean code \
    && path="${envTargets}" \
    && tmp="$(mktemp)" \
    && find "${path}" -wholename '*.icl' -type f \
    | sort --ignore-case > "${tmp}" \
    && while read -r path; do
      info Compiling "${path}" \
        && clm "${path%.*}" -o "${path%.*}" \
        || critical Unsuccessful compilation
    done < "${tmp}" \
    && touch "${out}"
}

main "${@}"
