{ makeDerivation
, inputs
, path
, ...
}:
makeDerivation {
  name = "build_solutions_clean";
  env = {
    envTargets = path "/code";
  };
  searchPaths = {
    bin = [
      inputs.nixpkgs.clean
      inputs.nixpkgs.findutils
    ];
  };
  builder = ./entrypoint.sh;
}
