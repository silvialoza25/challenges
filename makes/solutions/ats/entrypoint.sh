# shellcheck shell=bash

function main {
  local args=(
    -Werror
    -Wextra
    -c
  )
  local tmp
  local path

  info Compiling ATS code \
    && path="${envTargets}" \
    && tmp="$(mktemp)" \
    && find "${path}" -wholename '*.dats' -type f \
    | sort --ignore-case > "${tmp}" \
    && while read -r path; do
      info Compiling "${path}" \
        && atscc "${args[@]}" "${path}" \
        || critical Unsuccessful compilation
    done < "${tmp}" \
    && touch "${out}"
}

main "${@}"
