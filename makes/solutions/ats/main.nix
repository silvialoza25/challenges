{ makeDerivation
, inputs
, path
, ...
}:
makeDerivation {
  name = "build_solutions_ats";
  env = {
    envTargets = path "/code";
  };
  searchPaths = {
    bin = [
      inputs.nixpkgs.ats
      inputs.nixpkgs.findutils
    ];
  };
  builder = ./entrypoint.sh;
}
