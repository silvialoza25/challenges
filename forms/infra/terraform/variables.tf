variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "cloudflare_api_key" {}
variable "cloudflare_account_id" {}

data "cloudflare_ip_ranges" "cloudflare" {}
data "cloudflare_zones" "autonomicjump_com" {
  filter {
    name = "autonomicjump.com"
  }
}

variable "region" {
  default = "us-east-1"
}
