# Production

resource "cloudflare_record" "prod" {
  zone_id = lookup(data.cloudflare_zones.autonomicjump_com.zones[0], "id")
  name    = "forms.${lookup(data.cloudflare_zones.autonomicjump_com.zones[0], "name")}"
  type    = "CNAME"
  value   = aws_s3_bucket.prod.website_endpoint
  proxied = true
  ttl     = 1
}

# Development

resource "cloudflare_record" "dev" {
  zone_id = lookup(data.cloudflare_zones.autonomicjump_com.zones[0], "id")
  name    = "forms-dev.${lookup(data.cloudflare_zones.autonomicjump_com.zones[0], "name")}"
  type    = "CNAME"
  value   = aws_s3_bucket.dev.website_endpoint
  proxied = true
  ttl     = 1
}
