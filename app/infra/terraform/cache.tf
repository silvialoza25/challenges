# Production

resource "cloudflare_page_rule" "app_cache_prod" {
  zone_id  = lookup(data.cloudflare_zones.autonomicjump_com.zones[0], "id")
  target   = "app.${lookup(data.cloudflare_zones.autonomicjump_com.zones[0], "name")}/*"
  status   = "active"
  priority = 1

  actions {
    cache_level            = "cache_everything"
    edge_cache_ttl         = 3600
    browser_cache_ttl      = 1800
  }
}

# Development

resource "cloudflare_page_rule" "app_cache_dev" {
  zone_id  = lookup(data.cloudflare_zones.autonomicjump_com.zones[0], "id")
  target   = "app-dev.${lookup(data.cloudflare_zones.autonomicjump_com.zones[0], "name")}/*"
  status   = "active"
  priority = 1

  actions {
    cache_level            = "cache_everything"
    edge_cache_ttl         = 3600
    browser_cache_ttl      = 1800
  }
}
