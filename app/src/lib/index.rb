# frozen_string_literal: true

# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Metrics/MethodLength

require 'yaml'

def langs_dropped_table
  lang_dropped_info = YAML.safe_load(File.read(@config[:const][
    :lang_dropped_data]))
  taxonomy = YAML.safe_load(File.read(@config[:const][:taxonomy]))
  add_description(lang_dropped_info, taxonomy)
  reasons_counter = add_counter(lang_dropped_info, taxonomy)
  [lang_dropped_info, lang_dropped_info.length, reasons_counter]
end

def add_description(lang_dropped_info, taxonomy)
  lang_dropped_info.each_value do |lang|
    lang['description'] = make_reason_str(lang['reasons'], taxonomy, [])
  end
end

def make_reason_str(reason, taxonomy, str)
  if reason.nil?
    str.join('<br/>')
  else
    make_reason_str(reason.slice(1, reason.length), taxonomy,
                    str + [taxonomy[reason.first]])
  end
end

def add_counter(lang_dropped_info, taxonomy)
  reasons_counter = taxonomy_hash(taxonomy)
  lang_dropped_info.each_value do |lang|
    lang['reasons'].each do |reason|
      reasons_counter[reason] += 1
    end
  end
  reasons_counter
end

def taxonomy_hash(taxonomy)
  taxonomy.keys.each_with_object(Hash.new(0)) do |value, collection|
    collection[value] = 0
  end
end

def langs_table
  folder = @config[:const][:code_dir]
  lang_totals = {
    'sltns' => 0, 'unique-sltns' => 0, 'int-sltns' => 0, 'ext-sltns' => 0
  }
  lang_info = YAML.safe_load(File.read(@config[:const][:lang_data]))
  init_langs(lang_info)
  langs_table_int_sltns(lang_info, lang_totals, folder)
  langs_table_ext_sltns(lang_info, lang_totals, folder)
  lang_info, lang_totals = calculate_sltns(lang_info, lang_totals)
  [lang_info, lang_totals]
end

def langs_table_int_sltns(lang_info, lang_totals, folder)
  all_files = Dir.glob("#{folder}/**/*.*")
  all_files.each do |sltn|
    sltn_ext = sltn.split('.')[-1].strip
    sltn_lang = search_lang(sltn_ext, lang_info)
    next if sltn_lang.nil?

    lang_info[sltn_lang]['int-sltns'] += 1
    lang_totals['int-sltns'] += 1
    if check_sltn_unique_code(sltn)
      lang_info[sltn_lang]['unique-sltns'] += 1
      lang_totals['unique-sltns'] += 1
    end
  end
end

def langs_table_ext_sltns(lang_info, lang_totals, folder)
  other_files = Dir.glob("#{folder}/**/OTHERS.lst")
  other_files.each do |other|
    others = File.readlines(other)
    others.each do |other_line|
      other_ext = other_line.split('.')[-1].strip
      other_lang = search_lang(other_ext, lang_info)
      next if other_lang.nil?

      lang_info[other_lang]['ext-sltns'] += 1
      lang_totals['ext-sltns'] += 1
    end
  end
end

def init_langs(lang_info)
  lang_info.each_value do |lang|
    lang['sltns'] = 0
    lang['unique-sltns'] = 0
    lang['int-sltns'] = 0
    lang['ext-sltns'] = 0
  end
end

def calculate_sltns(lang_info, lang_totals)
  lang_info.each_value do |value|
    value['sltns'] = value['int-sltns'] + value['ext-sltns']
    lang_totals['sltns'] += value['sltns']
  end
  [lang_info, lang_totals]
end
