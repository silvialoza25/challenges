# frozen_string_literal: true

# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/CyclomaticComplexity
# rubocop:disable Metrics/PerceivedComplexity

require 'etc'
require 'yaml'
require 'open3'
require 'time'

def git_user_stats_users_statistics
  users = { code_users: {}, hack_users: {}, vbd_users: {} }
  code_dir = @config[:const][:code_dir]
  hack_dir = @config[:const][:hack_dir]
  vbd_dir = @config[:const][:vbd_dir]
  yml_path = @config[:const][:lang_data]
  lang_info = YAML.safe_load(File.read(yml_path))
  code_sltns = Dir.glob("#{code_dir}/**/*.*")
  hack_sltns = Dir.glob("#{hack_dir}/**/*.feature")
  vbd_sltns = Dir.glob("#{vbd_dir}/**/*.feature")
  code_sltns = code_sltns.reject { |path| path.end_with? 'DATA.lst' }
  code_sltns = code_sltns.reject { |path| path.end_with? 'LINK.lst' }
  code_sltns = code_sltns.reject { |path| path.end_with? 'OTHERS.lst' }

  code_sltns.each do |code_sltn|
    git_user_stats_users_time(users[:code_users], code_sltn, lang_info,
                              code_dir)
  end

  hack_sltns.each do |hack_sltn|
    git_user_stats_users_time(users[:hack_users], hack_sltn, lang_info,
                              hack_dir)
  end

  vbd_sltns.each do |vbd_sltn|
    git_user_stats_users_time(users[:vbd_users], vbd_sltn, lang_info,
                              vbd_dir)
  end
  git_user_stats_postprocess(users)
  users
end

def git_user_stats_postprocess(users)
  users[:general_users] = add_hashes(users[:code_users], users[:hack_users])
  users[:general_users] = add_hashes(users[:general_users], users[:vbd_users])
  git_user_stats_totals(users)
  users[:general_users].each_key do |user|
    unless users[:general_users][user][:time].zero?
      users[:general_users][user][:avg] =
        users[:general_users][user][:sltns] / users[:general_users][user][:time]
    end
    users[:general_users][user] =
      git_user_stats_postprocess_user(user, 'Total', :general_users, users)
    users[:code_users][user] =
      git_user_stats_postprocess_user(user, 'Code', :code_users, users)
    users[:hack_users][user] =
      git_user_stats_postprocess_user(user, 'Hack', :hack_users, users)
    users[:vbd_users][user] =
      git_user_stats_postprocess_user(user, 'Vbd', :vbd_users, users)
  end
  users[:totals].each_value do |hash|
    hash[:avg] = format('%.2f', hash[:avg])
  end
end

def git_user_stats_totals(users)
  users[:totals] = {
    code_users: { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 },
    hack_users: { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 },
    vbd_users: { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 },
    general_users: { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 }
  }
  users.each_key do |hash|
    next if hash == :totals

    users[hash].each_value do |user|
      users[:totals][hash][:time] += user[:time]
      users[:totals][hash][:sltns] += user[:sltns]
      users[:totals][hash][:score] += user[:score]
    end
    next if users[:totals][hash][:sltns].zero?

    users[:totals][hash][:avg] =
      users[:totals][hash][:sltns] / users[:totals][hash][:time]
    users[:totals][hash][:score] =
      users[:totals][hash][:score] / users[:totals][hash][:sltns]
  end
end

def git_user_stats_postprocess_user(user, hash_name, type, users)
  if users[type][user].nil? || users[type][user][:sltns].zero?
    users[type][user] = {
      time: 0.0, sltns: 0, avg: 0.0, score: 0.0,
      first_sltn_date: '-', last_sltn_date: '-'
    }
  end
  users[type][user][:name] = hash_name
  users[type][user][:avg] = format('%.2f', users[type][user][:avg])
  users[type][user][:score] = format('%.2f', users[type][user][:score])
  users[type][user]
end

def git_user_stats_users_time(users, sltn, lang_info, type)
  score_multiplier = 0.5 if code_dir?(type)
  score_multiplier = 0.7 if hack_dir?(type) || vbd_dir?(type)
  return if code_dir?(type) && !check_valid_sltn_code(sltn, lang_info)

  user = sltn.split('/')[-1].split('.')[0]
  path = sltn.split('/')[0, 3].join('/')

  users[user] = { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 } if
    users[user].nil?

  if File.exist?("#{path}/#{user}.yml")
    stdout = YAML.safe_load(File.read("#{path}/#{user}.yml"))
  else
    date = '-'
    effort = 0.0
    if users[user][:first_sltn_date].nil?
      users[user][:first_sltn_date] = date
      users[user][:last_sltn_date] = date
    end
    users[user][:sltns] += 1
    users[user][:time] += effort
    users[user][:avg] = users[user][:sltns] / users[user][:time] unless
      users[user][:time].zero?
    users[user][:score] = users[user][:avg] * score_multiplier
    return
  end

  date = stdout['date']
  effort = stdout['effort']
  if users[user][:first_sltn_date].nil? || users[user][:first_sltn_date] == '-'
    users[user][:first_sltn_date] = date
    users[user][:last_sltn_date] = date
  end
  users[user][:first_sltn_date] = [users[user][:first_sltn_date], date].min
  users[user][:last_sltn_date] = [users[user][:last_sltn_date], date].max
  users[user][:sltns] += 1
  users[user][:time] += effort
  users[user][:avg] = users[user][:sltns] / users[user][:time] unless
    users[user][:time].zero?
  users[user][:score] = users[user][:avg] * score_multiplier
end
