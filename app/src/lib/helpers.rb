# frozen_string_literal: true

# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Style/ConditionalAssignment
# rubocop:disable Style/MixinUsage
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength

include Nanoc::Helpers::Capturing
include Nanoc::Helpers::Rendering
include Nanoc::Helpers::LinkTo

# General Methods

def split_array(array, parts)
  array.group_by.with_index { |_, i| i % parts }.values
end

def deep_copy(object)
  Marshal.load(Marshal.dump(object))
end

def calc_external_solution_totals(totals)
  avg_external_solutions =
    totals['total-external-sltns'] / totals['potential-chgs'].to_f
  format('%<prcntg>.2f', prcntg: avg_external_solutions)
end

def add_hashes(hash1, hash2)
  hash3 = deep_copy(hash1)
  hash2c = deep_copy(hash2)
  hash3.merge!(hash2c) do |key, old, new|
    if old.is_a?(Hash)
      add_hashes(old, new)
    elsif old.is_a?(String)
      old
    elsif key == :first_sltn_date
      [old, new].min
    elsif key == :last_sltn_date
      [old, new].max
    else
      old + new
    end
  end
  hash3
end

def user_ranking_init_hashes(folder, lang_info)
  users = {}
  totals = {
    'sltns' => 0, 'unique-sltns' => 0, 'npages' => 0, 'pages' => []
  }
  if hack_dir?(folder) || vbd_dir?(folder)
    sltns = Dir.glob("#{folder}/**/*.feature")
  else
    sltns = Dir.glob("#{folder}/**/**")
  end
  sltns.each do |sltn|
    next if code_dir?(folder) && !check_valid_sltn_code(sltn, lang_info)

    user = sltn.split('/')[-1].split('.')[0]
    next unless users[user].nil?

    users[user] = {
      'sltns' => 0,
      'unique-sltns' => 0,
      'npages' => 0,
      'pages' => []
    }
  end
  [users, totals]
end

def add_user(user, users)
  unless users.key?(user)
    user_hash = {
      'sltns' => 0, 'unique-sltns' => 0, 'unique-sltns-prcntg' => 0.0,
      'npages' => 0, 'pages' => []
    }
    users[user] = user_hash
    users
  end
  users
end

def add_percentage(users)
  users.each_key do |key|
    users[key]['unique-sltns-prcntg'] =
      (100.0 / users[key]['sltns'] * users[key]['unique-sltns']).round
  end
  users
end

def search_lang(ext, lang_info)
  lang_info.each do |key, lang|
    return key if lang['ext'].include? ext
  end
  nil
end

def search_extension(ext, others)
  others.each do |other|
    other_ext = other.split('.')[-1]
    return true if ext == other_ext
  end
end

def file_path(sltn_path, file)
  parts = sltn_path.split('/')
  "#{parts.first(parts.size - 1).join('/')}/#{file}"
end

def sltn_repo(sltn_path)
  sltn_path.split('/')[0]
end

def calc_other_fields(sites, totals)
  sites.each_value do |val|
    val['pending-chgs'] = val['challenges'] - val['confirmed-chgs']
    totals['pending-chgs'] += val['pending-chgs']
    val['ext-sltns-per-chg'] =
      val['total-external-sltns'] / val['challenges'].to_f
    val['ext-sltns-per-chg'] =
      format('%<prcntg>.2f', prcntg: val['ext-sltns-per-chg'])
  end
  [sites, totals]
end

def check_solved_with_ext_chg(chg_path)
  others_path = "#{chg_path}/OTHERS.lst"
  others_exist = File.file?(others_path)
  others_empty = File.zero?(others_path)
  others_exist && !others_empty
end

def init_site(sites, site_name, site_path, file_name)
  sites[site_name] = YAML.safe_load(File.read(site_path + file_name))
  sites[site_name]['confirmed-chgs'] = 0
  sites[site_name]['internal-chgs'] = 0
  sites[site_name]['external-chgs'] = 0
  sites[site_name]['unique-sltns'] = 0
  sites[site_name]['total-internal-sltns'] = 0
  sites[site_name]['total-external-sltns'] = 0
  sites
end

def code_dir?(folder)
  folder == @config[:const][:code_dir]
end

def hack_dir?(folder)
  folder == @config[:const][:hack_dir]
end

def vbd_dir?(folder)
  folder == @config[:const][:vbd_dir]
end

def get_chg_sltns_epoch(sltns)
  dates = {}
  sltns.each do |sltn|
    user = sltn.split('/')[-1].split('.')[0]
    path = sltn.split('/')[0, 3].join('/')
    if File.exist?("#{path}/#{user}.yml")
      stdout = YAML.safe_load(File.read("#{path}/#{user}.yml"))
      dates[sltn] = Date.parse(stdout['date']).mjd.to_i
    else
      dates[sltn] = '-'
    end
  end
  dates.sort_by { |_key, value| value }.to_h
end

def same_ext_not_in_path(sltn)
  sltn_ext = sltn.split('.')[-1].strip
  chg_path = sltn.split('/')[0..-2].join('/')
  same_ext_sltns = Dir.glob("#{chg_path}/*.#{sltn_ext}")
  same_ext_sltns.length == 1
end

def same_ext_not_in_others(sltn)
  success = true
  sltn_ext = sltn.split('.')[-1].strip
  others_path = file_path(sltn, 'OTHERS.lst')
  if File.file?(others_path)
    others = File.readlines(others_path)
    others.each do |other|
      other_ext = other.split('.')[-1].strip
      if sltn_ext == other_ext
        success = false
        break
      end
    end
  end
  success
end

# Code Methods

def check_valid_sltn_code(sltn, lang_info)
  sltn_ext = sltn.split('.')[-1].strip
  sltn_lang = search_lang(sltn_ext, lang_info)
  !sltn_lang.nil?
end

def check_solved_chg_code(chg_path, lang_info)
  others_path = "#{chg_path}/OTHERS.lst"
  others_exist = File.file?(others_path)
  others_empty = File.zero?(others_path)
  return true if others_exist && !others_empty

  sltns = Dir.glob("#{chg_path}/*.*")
  sltns.each do |sltn|
    return true if check_valid_sltn_code(sltn, lang_info)
  end
  false
end

def check_sltn_unique_code(sltn)
  success = true
  chg_path = sltn.split('/')[0..-2].join('/')
  sltn_name = sltn.split('/')[-1].strip
  sltn_user = sltn_name.split('.')[0].strip
  user_chg_sltns = Dir.glob("#{chg_path}/#{sltn_user}.*")
  chg_sltns_epoch = get_chg_sltns_epoch(user_chg_sltns)

  chg_sltns_epoch.each do |chg_sltn, _|
    if same_ext_not_in_path(chg_sltn) && same_ext_not_in_others(chg_sltn)
      success = false if sltn != chg_sltn
      break
    end
  end
  success
end

def check_solved_with_int_chg_code(chg_path, lang_info)
  sltns = Dir.glob("#{chg_path}/*.*")
  sltns.each do |sltn|
    return true if check_valid_sltn_code(sltn, lang_info)
  end
  false
end

# Hack and VbD Methods

def check_sltn_unique_hack_vbd(sltn)
  others_path = file_path(sltn, 'OTHERS.lst')
  others_exist = File.file?(others_path)
  others_empty = File.zero?(others_path)
  return true if !others_exist || others_empty

  false
end

def check_solved_chg_hack_vbd(chg_path)
  others_path = "#{chg_path}/OTHERS.lst"
  others_exist = File.file?(others_path)
  others_empty = File.zero?(others_path)
  return true if others_exist && !others_empty

  sltns = Dir.glob("#{chg_path}/*.feature")
  return true if sltns.any?

  false
end

def check_solved_with_int_chg_hack_vbd(chg_path)
  sltns = Dir.glob("#{chg_path}/*.feature")
  return true if sltns.any?

  false
end

def get_ranking(users)
  sort_arrary = users.sort_by do |_user, value|
    [value['unique-sltns'], value['unique-sltns-prcntg']]
  end
  sort_arrary.reverse
end

def get_position(user, position)
  user_pos = position.index { |item| item.first == user }
  if user_pos.nil?
    '-'
  else
    user_pos + 1
  end
end

def calc_weeks(first_commit, last_commit)
  if first_commit == '-' || last_commit == '-'
    '-'
  else
    (Date.parse(last_commit).mjd - Date.parse(first_commit).mjd) / 7
  end
end

def calc_days(last_commit)
  if last_commit == '-'
    '-'
  else
    Date.parse(Time.now.utc.to_s).mjd - Date.parse(last_commit).mjd
  end
end

def get_date(sltn, users, user)
  path = sltn.split('/')[0, 3].join('/')

  if File.exist?("#{path}/#{user}.yml")
    stdout = YAML.safe_load(File.read("#{path}/#{user}.yml"))
  else
    date = '-'
    if users[user]['first-sltn-date'].nil?
      users[user]['first-sltn-date'] = date
      users[user]['last-sltn-date'] = date
    end
    return
  end

  date = stdout['date']

  if users[user]['first-sltn-date'].nil? ||
     users[user]['first-sltn-date'] == '-'
    users[user]['first-sltn-date'] = date
    users[user]['last-sltn-date'] = date
  end
  users[user]['first-sltn-date'] = [users[user]['first-sltn-date'], date].min
  users[user]['last-sltn-date'] = [users[user]['last-sltn-date'], date].max
end
